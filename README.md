# Description
This repo contains code to perform a number of SNOMED CT inspection and analysis tasks (as a 'hobbyist', this code must **not** be used for production level/safety critical activities). The tasks naturally fall into four categories, each of which is controlled by a distinct command-line interface:

- Snapshot data browser
- Snapshot ECL and set browser (_set focussed_)
- Full data browser
- MRCM diagram configurations

The underlying reference data files require the [SNOMED CT International Edition RF2 release](https://www.snomed.org/snomed-ct/get-snomed) files or [UK Release files](https://isd.digital.nhs.uk/trud3/user/guest/group/0/pack/26) acquired, installed and used in accordance with their associated licenses.

The underlying scripts have largely been developed in a Linux context (the data build scripts in particular assume Linux both in terms of paths and utilities e.g. sed), however the functional/browser scripts have all been tested and work on Windows. The Linux capability means that the Snapshot scripts work on Android.

Each command line application comes with its own help files, but in brief:

## Snapshot data browser

**This is a command line browser for searching, viewing and manipulating the SNOMED CT Snapshot data.**

![Snapshot browser landing page](RF2/buildInstructions/images/Snapshot2.png)

**Functions include:**

**Search:** Basic 'starts with' search tokens (or identifiers) can be entered at the 'Input selection or search text:' prompt.

**Concept details: ** Further concept details can be selected by entering a letter and the index number (e.g. 'e6' will display the terms of the concept associated with index number [6]).

Concept details that can be retrieved are:
[e], [g] Terms
[f], [h] Relationship details
[i] Concept metadata
[k], [l], [o] Refset membership for selected concept
[m], [n] Refset members if concept is a refset
[q], [r] Ancestors and descendants
[s], [t] Historical origins of active concepts
[x] Calculated proximal primitive supertypes
[H] to [J] Refset and top level chapter member comparison
[K] Emulates concrete values where codes in data (will eventually be deprecated)

**Other control functions are:**
[b] Search by identifier
[c] Search on text (includes inactives, exact matches)
[d] Read back latest search list
[j] Show list of last 25 distinct concepts selected ('breadcrumbs')
[p] Read in concept from the Full/state valid window (if this is used)
[v] Reload the focus concept (useful if interacting with the ECL browser window)
[u] This basic help page (or '--'). Letter-specific help pages can be shown by '-' then the letter (e.g. '-e' will show help for 'e' - if it's been written)
[w] A selection of 'useful' codes (e.g. shortcut to refset root and other metadata concepts)
[y] Toggles pretty printing of SCG and OWL strings

**Current configuration values:**
Where an action modifies a configuration setting, the current value will be shown at the top of the help file returned.
For example '-A', in addition to providing help on setting up which pictures/diagrams are refreshed with each concept change, will show the current value.

**Picture generation:**
Each time a concept is selected a dot-notation diagram of the concept definition and its ancestry is produced. This can be viewed using a suitable image browser. Configurtation is controlled by:
[A] Sets the type and nature of diagram(s) to be produced with each concept change
[B] Sets the way that value concepts are organised.
[C] Shows/hides supertypes, uses soft or angled edges.

UK-specific:
[z] Rotates between latest UK and International data (snapshot only)

*Example showing focus, ancestor and descendant (treemap) diagrams:*
![Snapshot browser with diagrams](RF2/buildInstructions/images/EndoProcDiag.png)

## Snapshot ECL and set browser

**These are companion scripts for SCT Snapshot Data Browser, providing a mechanism for editing ECL-based predicates and then manipulating and processing the resulting sets.**

![Snapshot ECL and Sets example](RF2/buildInstructions/images/ECL_Set1.png)

**Functions include:**

Building and manipulating single depth (i.e. no clause nesting) ECL constraints, or sets of such constraints.
Single constraints use AND logic between clauses; sets use OR logic between constraints.

Individual constraints can be generated from importing Snapshot browser focus definitions ([a] and/or [v] actions), or from a number of basic templates ([s]).
Constraints can then be modified:
[c], [d], [e], [f] Add or remove entire rows
[j], [k], [l] modify operators.
[i], [z] and [L] change coded values (including concrete values).

Constraints are then run against the reference data by entering [q].

Sets of concepts returned can then be given temporary names ('A', 'B', 'C'...) using [h] and can then be included in subsequent constraints as if they were reference sets in place of concepts ([z]).

Sets can also be rapidly populated from a snapshot search report ([g]) or refset membership/TC result ([t]). Named sets can be read back ([u]) as can the result of constraint processing ([r]).

**Set combinations:** A small nunber of AND/OR/NOT shortcuts to set combination are offered ([m], [n], [o])

**Constraint combination:** Constraint sets can be built up, modified, stored (with names), retrieved and deleted ([C], [D], [E], [G], [H], [I]).
After processing ([F]) resulting sets can be given temporary names and manipulated as above.

**Set compression/refactoring:** By various [J] actions, sets of concepts can be directly converted into constraint sets (multiple include clauses), or can be converted to more complex sets of include/exclude constraints that correspond to the original set.
Since constraint sets can be saved, this allows temporary sets to be saved (as constraint sets) - although this may introduce version dependencies.
[J] actions also include some experimental variants that try to identify navigational ancestors (which can be saved and manipulated with [S] and [T])

**Visualisation:** [w] draws a subgraph of set members (also producing a navigable subgraph), [B] draws the combined definitions of a set of codes, and [m], [n] and [o] can produce diagrammatic representations of two set mergers/comparisons.
More complex [w] options appear as diagram variants - in particular a treemap of descendants.

[U] groups set members by module.

**Current configuration values:**
Where an action modifies a configuration setting, the current value will be shown at the top of the help file returned.
For example '-O', in addition to providing help on specifying whether calculated supertypes should only be defined or not, will show the current value.

*Example showing focus, ancestor and descendant (treemap) set manipulation diagrams:*
![Sets example with diagrams](RF2/buildInstructions/images/SetNavigation.png)

*Example showing snapshot and set browsers running in QPython 3L on Android:*
![Snapshot & sets in QPython 3L](RF2/buildInstructions/images/AndroidSnapSet.png)

## Full data browser

**This is a command line browser for searching, viewing and manipulating the SNOMED CT Full data.**

![Full browser landing page](RF2/buildInstructions/images/Full1.png)

The full browser depends/relies upon the snapshot browser for search-based and id input-based component access (only the snapshot browser has these features - plus the ECL and set browser supports the import of sets of codes using [M]). Otherwise much of the experience is similar to the snapshot browser, except that the data being browsed is determined by the effectiveTime set by the user.

As currently used (and thus configured and coded) the assumption is that the full data build corresponds to the UK snapshot data. Consequently the snapshot [z] function only rotates the snapshot data (there is only one Full database but two Snapshots). This arrangement meets personal requirements (and uses less disk space) but could be modified in the future.

_Note - whilst in some ways the full browser has an easier build (most of its lookups are from a plain import of the full files), it is also dependent on (a) a snapshot relational build and (b) a neo4j build of the basic graph. The former is needed for search indexing, and the latter for any functions (of which there are many) that require testing  state-valid ancestry._ 

**Functions include:**

**Initialisation:** Initialisation includes [a] browse from root, [b] import the snapshot focus (for example after searching) and [y] setting the effectiveTime.

**Hierarchy/graph navigation:** As with the snapshot browser and in other settings, selecting the index number of a concept will assign this focus and indicate immediate parents and children. As indicated above, a common workflow is to locate a concept of interest in the snapshot browser and then pull this into the full browser with [b].

**Set effectiveTime functions:**
Concept details at a given effectiveTime that can be viewed are:
[q] Terms
[r] Relationship details
[x], [B] Refset membership for selected concept
[z], [s] Refset members if concept is a refset
[t], [u] Ancestors and descendants

Visualisations at a set effectiveTime are:
[E] ,[H] and [J] display a diagram of the ancestors, descendants or focus concept definition

**Through time functions:**
As well as viewing the data at specific effectiveTimes, it is also possible to consider how the data has changed over time. The following functions support through time data display.

Concept details through time:
[v], [w] and [A] generate a matrix report of a concept's ancestors, descendants or members (if a refset) through time. The matrix is temporarily displayed in the terminal, and a permanent record is stored as an xlsx file in the files/xlsx folder.

Visualisations through time:
[D] ,[G] and [I] combine the D3 library with Graphviz to produce animated 'through time diagrams' of the ancestors, descendants or focus concept definition.

**Concept metadata:** Unlike the snapshot browser where different function codes display or hide metadata, in the full browser metadata can be turned on or off for each function ([k] and [l]). 

**Setting effectiveTimes plus showing active and inactive data:** the effectiveTime of interest is set using [y] and then entering a short/long time or 'now' - this persists between sessions. How the data viewed is then calculated is determined by setting [e] (simple snapshot) or [f] (MDR snapshot - slower (at times unworkably) but important for data where there is a mismatch between extension and international release frequencies). It is also possible to display active or inactive rows for a given effectiveTime ([g], [h] and [i]) or simply show all related rows irrespective of the effectiveTime ([d]).

Terms displayed when browsing can either be from those cached in latest snapshot build [m] (fast) or actually calculated based on the effectiveTime [n] (slower).

**Other control functions are:**
[o], [p] showing and hiding 'further subtype' crosses
[c], [j] give access to breadcrumbs from the snapshot and full browsers
[C] and [K] configure syntax and diagram features.

*Example showing through time descendant report (screen and xlsx):*
![Example full descendants thro time](RF2/buildInstructions/images/FullDescThroTime.png)

## MRCM diagram configuration and generation

**This code controls the configuration and generation of MRCM diagrams.**

![Snapshot MRCM landing page](RF2/buildInstructions/images/MRCM1.png)

Launching the code and/or selecting [a] provides a list of the MRCM domains.

**Selecting domains:** Domains for diagram inclusion are selected by the [b] and [c] functions. After domains are selected and other configuration options set, diagrams can be generated with [d].

**Caption and key:** Depending on selections made with [e], [o] and [p] the caption and/or key can be hidden or included in diagrams.

**Layout and feature setting:** Various aspects of layout and diagram features can be set, including attribute size ([g], [A], [B]), domain and range size ([h]), inclusion or exclusion of constraint operators ([j]) and tooltips ([k]), edge direction ([l]), parent domains ([m]), angled or curved edges ([r]) and font size([s]).

**Diagram generation setting:** Diagram generation can end with dot file production or can also include svg generation ([i]). Pre- or post-coordinated MRCM data can be used ([y] and [z]), experimentally an animated diagram using the D3 framework can be generated ([n]) and if UK and international data are separately loaded then an attempt at indicating what has changed in the MRCM can be shown [w].

**Data build:** As a final stage in the snapshot data build sequence, it is necessary to generate a small file of model-referenced attributes ([x]).

*Example showing pre- and post-coordinated MRCM pharmacy domain diagrams:*
![Example MRCM with diagrams](RF2/buildInstructions/images/MRCMWithPics.png)

### How do I get set up? ###

* Build guidance in `buildinstructions/buildguide.md`.
* Dependencies (pip install...):
    * Snap and ECL/Sets (`SCTRF2.py` and `SCTRF2R.py` plus dependent scripts) both use unidecode
    * Full (`SCTRF2SV.py` plus dependent scripts) uses `python-dateutil`, `neo4j` & `xlsxwriter`

### Who do I talk to? ###

* Repo owner or admin: edcheetham@protonmail.com

### License ###

* Apache 2.0
* See the included [LICENSE](LICENSE) file for details.