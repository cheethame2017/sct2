#!/usr/bin/python
from __future__ import print_function, unicode_literals
import sqlite3
from unidecode import unidecode


def buildDescIndex(snapPath, outPath):
    # this code fills descriptionsdx (includes a de-unicoded column for the term in RF2Out)

    conn = sqlite3.connect(outPath)
    cur = conn.cursor()
    cur2 = conn.cursor()
    cur.execute("attach '" + snapPath + "' as RF2Snap")
    # cur.execute("select * from RF2Snap.descriptions where active = 1")
    cur.execute("select * from RF2Snap.descriptions")
    # cur.execute("select * from RF2Snap.descriptions, rdsrows where RF2Snap.descriptions.id=rdsrows.id")

    progress = 1

    ##############################
    recordsetIn1 = cur.fetchall()
    tot = len(recordsetIn1)
    print(tot)
    for row in recordsetIn1:
        templist = []
        templist = [item for item in row]
        templist.insert(8, unidecode(templist[7].upper()))
        if not row:
            break
        cur2.execute("insert into descriptionsdx values (? , ? , ? , ? , ? , ? , ? , ? , ?, ?)", templist)
        if (progress % 500 == 0):
            print(progress, "/", tot)
            conn.commit()
        progress += 1
    # cleanup commit
    conn.commit()

    # definitions
    cur.execute("select * from RF2Snap.definitions")
    progress = 1
    ##############################
    recordsetIn1 = cur.fetchall()
    tot = len(recordsetIn1)
    print(tot)
    for row in recordsetIn1:
        templist = []
        templist = [item for item in row]
        templist.insert(8, unidecode(templist[7].upper()))
        if not row:
            break
        cur2.execute("insert into descriptionsdx values (? , ? , ? , ? , ? , ? , ? , ? , ?, ?)", templist)
        if (progress % 500 == 0):
            print(progress, "/", tot)
            conn.commit()
        progress += 1
    # cleanup commit
    conn.commit()

    cur2.close()
    cur.close()
    conn.close()

if __name__ == '__main__':
    buildDescIndex("/home/ed/D/RF2Snap.db", "/home/ed/D/RF2Out.db")
