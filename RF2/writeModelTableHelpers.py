from __future__ import print_function, unicode_literals
import io
from datetime import date
import re
import math
from random import random
import textwrap
from unidecode import unidecode
from SCTRF2Helpers import TCCompare_IN, refSetTypeFromConId_IN, refSetMemberCountFromConId_IN, \
                            refSetMembersFromConId_IN, refSetFieldsFromConId_IN, refSetDetailsFromMemIdandSetId_IN, conMetaFromConId_IN, \
                            pterm, descendantCountFromConId_IN, attUseCount, defnFromConId_IN, attUseInDomCount
from SCTRF2Subgraph import setToGraph, adjacencyBuild
from SCTRF2Funcs import snapHeaderRead, sorted_cfg, hasDef
# CONC - header
from SCTRF2Concrete import stringDecimalInteger, concreteValues
from SCTRF2config import tok, cfg, cfg_MRCM as cfg_local
from platformSpecific import cfgPath, cfgPathInstall, copyPreModelCycleD3, backupMRCMOutputs, newMRCMOutputs, \
                            modelToSVGLocal, modelToSVGWinHost, backupOldModelFile, backupNewModelFile, sep, backupAllDomainMRCMOutputsPre, \
                            backupAllDomainMRCMOutputsPost, renameDiagramFile


# creates graphviz dot file image outputs of international MRCM files.
# set configuration variables in files/MRCMconfig.cfg

# main difference is D3 0 or 1 (1 generates a sequence of small models, rendered in pre-model-cycle.html), 0 generates statid file restricted to
# domains set in files/c

# If the MRCM domains change (i.e. new domains added) then modelLimitID.txt will be updated automatically during data build. However the
# D3 modelLimiterList groupings will need manual updating for now. Currently UK data is domain consistent with international
# but if it falls behind then may need different list conditional on getVersion() result)

# invoked from SCTMRCM.py control script. Uses SCTRF2.py conrolled snapshot methods and configuration.
# data set (UK or latest international) set by running snapshot browser and selecting relevant dataset (by pressing 'z')

# if wishing only to fill the files/attLimit.txt table for limiting attribute visibility in diagrams, set 'x1'.
# whilst the other tables (and unrendered dot files) are still generated.


def pterm_unidec(inId):
    return unidecode(str(pterm(inId)))

def definition_unidec(inId):
    try:
        mydef = defnFromConId_IN(inId)
        mydefstring = mydef[1]
        mydefstring = mydefstring.replace("\"", "\'")
        if len(mydef) > 0:
            return "\\n" + toString(textwrap.wrap(unidecode(str(mydefstring)), 60)) + "\\n"
    except:
        return ""
    else:
        return ""

def cfgSet():
    tempLineList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        tempLine = line
        tempLine = tempLine.replace("SHOW_KEY|1", "SHOW_KEY|0")
        tempLineList.append(tempLine)
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'w', encoding="UTF-8")
    for line in tempLineList:
        f.write(line)
    f.close()

def cfgReSet(inKeySetting):
    tempLineList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        tempLine = line
        tempLine = tempLine.replace("SHOW_KEY|0", "SHOW_KEY|" + inKeySetting)
        tempLineList.append(tempLine)
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'w', encoding="UTF-8")
    for line in tempLineList:
        f.write(line)
    f.close()

def limitDomainsByID(inValueList):
    returnValueList = []
    modelLimitList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":
            modelLimitList.append(line.strip())
    f.close()
    returnValueList = [row for row in inValueList if row[0] not in modelLimitList]
    if len(returnValueList) > 0:
        return returnValueList
    else:
        return inValueList

def limitDomainsID():
    fullList = []
    modelLimitList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'r', encoding="UTF-8")
    for line in f:
        if line[0] == "#":
            modelLimitList.append(line[2:].strip())
        fullList.append(line.strip())
    f.close()
    if len(modelLimitList) > 0:
        return modelLimitList
    else:
        return fullList

def getKeyLines(cardOp):
    keyLines = []
    # CONC - modify key file if concrete data
    if cardOp:
        if cfg("CONC_DATA") == 1:
            with io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'keyfileConc.dot', encoding="UTF-8") as f:
                keyLines = f.readlines()
        else:
            with io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'keyfile.dot', encoding="UTF-8") as f:
                keyLines = f.readlines()
    else:
        if cfg("CONC_DATA") == 1:
            with io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'keyfileNoOpConc.dot', encoding="UTF-8") as f:
                keyLines = f.readlines()
        else:
            with io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'keyfileNoOp.dot', encoding="UTF-8") as f:
                keyLines = f.readlines()
    return keyLines

def getVersion(inInt):
    # assumes UK version will be older than latest international (so versionListLen longer for older data).
    versionList = snapHeaderRead().split(" ")
    versionString = versionList[0]
    versionListLen = len(versionList)
    if inInt == 1:
        return versionString
    elif inInt == 2:
        return versionListLen
    else:
        return versionListLen,  versionString

def rankdir(inInt):
    rankDict = {1:"BT",
                2:"TB",
                3:"LR",
                4:"RL"}
    return rankDict[inInt]

def generateMRCMDotFile(domainRefsetId, attDomainRefsetId, prePost):
    if cfg_local("D_3") == 1:
        lineOpen = "'\""
        lineEnd = "]',\n"
    else:
        lineOpen = "\""
        lineEnd = "];\n"
    if cfg_local("ONLY_ATT") == 1:
        modelTableFolder = "atts"
    else:
        modelTableFolder = "dot"
    if cfg_local("CONSTR_OP") == 1:
        f = io.open(cfgPathInstall() + 'files' + sep() + modelTableFolder + sep() + 'modelTableCO.txt', 'w', encoding="UTF-8")
    else:
        f = io.open(cfgPathInstall() + 'files' + sep() + modelTableFolder + sep() + 'modelTable.txt', 'w', encoding="UTF-8")
    if prePost == "pre":
        f4 = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-pre_temp.dot', 'w', encoding="UTF-8")
    else:
        f4 = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-post_temp.dot', 'w', encoding="UTF-8")
    if cfg_local("ANGLED_EDGES") == 1:
        splineText = "; splines=ortho;"
    else:
        splineText = ";"
    if cfg_local("D_3") == 0:
        f4.write("digraph RF2_MRCM { rankdir=" + rankdir(cfg_local("RANK_DIRECTION")) + splineText + " node [shape=box, peripheries=1, style=filled, fontsize=9, fontname=Helvetica, tooltip=\" \", fillcolor=palegreen];\n")
    else:
        f4.write("'digraph RF2_MRCM { rankdir=" + rankdir(cfg_local("RANK_DIRECTION")) + splineText + " node [shape=box, peripheries=1, style=filled, fontsize=9, fontname=Helvetica, tooltip=\" \", fillcolor=palegreen]',\n")
    f4List = []
    attList = []
    attDomDict = {}
    domList = []
    domTot = 0
    allDomTot = 0
    rangeList = []
    DARList = []
    parentDomList = []
    domAndRangeList = []
    domRangeCollisionList = []
    attRangeCollisionList = []
    attRangeCollisionListTerm = []
    mondrianList = []
    mondrianPatchworkList = []
    domAndRangeDict = {}
    attRangeDict = {}
    domainTTDict = {}
    rangeTTDict = {}
    attTTDomDict = {}
    attTTRangeDict = {}
    myRefset = refSetTypeFromConId_IN(domainRefsetId)
    for refset in myRefset:
        refsetType = refset[0]
    myCount = refSetMemberCountFromConId_IN(domainRefsetId, refsetType)
    myfilter = 0
    myValueRows = refSetMembersFromConId_IN(domainRefsetId, refsetType, myfilter, myCount)
    myDescRows = refSetFieldsFromConId_IN(domainRefsetId)
    myRefsetType = 'c' + refSetTypeFromConId_IN(domainRefsetId)[0][0]  # if component datatype (can't pick up which one directly)
    myValueRowsLimit = limitDomainsByID(myValueRows)
    keySetting = str(cfg_local("SHOW_KEY"))
    attLimitBool = False
    if myValueRowsLimit == myValueRows:
        attLimitBool = True
    if (myValueRowsLimit != myValueRows) and cfg_local("HIDE_KEY"):
        cfgSet()
    for value in myValueRowsLimit:
        attRangeDictLocal = {}
        counter = 0
        domainTemp = ""
        domainConTemp = ""
        for row in myDescRows:
            tempRows = []
            if (myRefsetType[counter] == 'c') and (row[0] == "Referenced component"):
                domainTemp = value[counter]
                domainConTemp = value[counter + 1].split(" ")[0]
            else:
                if (row[1] == tok("PARS_STR") and (row[0] == "Domain template for precoordination") and prePost == "pre") or (row[1] == tok("PARS_STR") and (row[0] == "Domain template for postcoordination") and prePost == "post"):

                    tempRows = cleanForModelTableCardOp(value[row[2]], domainTemp)
                    lastAtt = ""
                    if cfg_local("CONSTR_OP") == 1:
                        lastCard = ""
                    newAtt = False # marker to show if attribute is new or old
                    for row in tempRows[1:]:
                        # print(row)
                        splitRow = []
                        f4Line = ""
                        if cfg_local("CONSTR_OP") == 1:
                            collision = False
                        splitRow = row.split("\t")
                        if (len(splitRow) == 5):
                            domId = splitRow[0]
                            domTerm = pterm_unidec(domId)
                            card = splitRow[1]
                            if not(card == "REPLACEATT"):
                                attId = splitRow[2].split("|")[0].strip()
                                attTerm = pterm_unidec(attId)
                            if cfg_local("CONSTR_OP") == 1:
                                rangeOp = splitRow[3]
                            rangeId = splitRow[4].split("|")[0].strip()
                            # CONC - deal with rangeIds
                            if "[[+" in rangeId:
                                # CONC - convert SDI ranges into 'corresponding' codes (saves rewriting a lot later)
                                rangeId = stringDecimalInteger(rangeId)
                            rangeTerm = pterm_unidec(rangeId)
                            if not domId in domAndRangeList:
                                domAndRangeList.append(domId)
                            domAndRangeList.append(rangeId)
                            if not domId in domList:
                                domList.append(domId)
                                # misses refset-based domain for calculations
                                domTot += descendantCountFromConId_IN(domId)
                                allDomTot += domTot
                            if not rangeId in rangeList:
                                rangeList.append(rangeId)
                                # misses refset-based range for calculations
                                allDomTot += descendantCountFromConId_IN(rangeId)
                            if cfg_local("CONSTR_OP") == 1:
                                if not domId in domAndRangeDict:
                                    domAndRangeDict.update({domId: domainConTemp + " " + domId})
                                if not rangeId in domAndRangeDict:
                                    domAndRangeDict.update({rangeId: rangeOp + " " + rangeId})

                                if rangeId in domAndRangeDict:
                                    if not domAndRangeDict[rangeId] == rangeOp + " " + rangeId:
                                        collision = True
                                        if not rangeId in domRangeCollisionList:
                                            domRangeCollisionList.append(rangeId)

                            if not(card == "REPLACEATT"):
                                lastAtt = attId
                                lastAttTerm = pterm_unidec(lastAtt.strip())
                                lastCard = card
                                # add to, or extend, local dictionary for this set of temprows
                                if not attId in attRangeDictLocal:
                                    attRangeDictLocal.update({attId:[rangeId, ]}) #start an entry in the dictionary
                                else:
                                    if not rangeId in attRangeDictLocal[attId]:
                                        attRangeDictLocal[attId].append(rangeId) # add range to list for this attribute
                                # add to main dictionary if not encountered before
                                if not attId in attRangeDict: # if this isn't an attribute we've seen before
                                    newAtt = True
                                    attRangeDict.update({attId:[rangeId, ]}) #start an entry in the dictionary
                                else:
                                    if not rangeId in attRangeDict[attId]:
                                        attRangeDict[attId].append(rangeId) # add range to list for this attribute
                                if cfg_local("CONSTR_OP") == 1:
                                    print(domainConTemp, domId, card, splitRow[2], rangeOp, splitRow[4])
                                else:
                                    print(domId, domTerm, attId, attTerm, rangeId, rangeTerm)
                                if not attId in attList:
                                    attList.append(attId)
                                if not attId in attDomDict:
                                    attDomDict.update({attId: [domId]})
                                else:
                                    if not domId in attDomDict[attId]:
                                        attDomDict[attId].append(domId)
                                if cfg_local("CONSTR_OP") == 1:
                                    f.write("\t".join([domainConTemp, domId, domTerm, card, attId, attTerm, rangeOp, splitRow[4].split("|")[0], rangeTerm]) + '\n')
                                else:
                                    f.write("\t".join([domId, domTerm, attId, attTerm, rangeId, rangeTerm]) + '\n')
                                if [domId, domTerm, attId, attTerm, rangeId, rangeTerm] not in DARList:
                                    DARList.append([domId, domTerm, attId, attTerm, rangeId, rangeTerm])
                                if conMetaFromConId_IN(attId, False)[1] == 1:
                                    if refSetDetailsFromMemIdandSetId_IN(attDomainRefsetId, attId)[0][2] == 0: # if attribute treated as ungrouped for DL
                                        blueEdgeString = "arrowhead=open, style=dashed, color=blue"
                                        redEdgeString = "arrowhead=open, style=dashed, color=red"
                                    else:
                                        blueEdgeString = "arrowhead=open, color=blue"
                                        redEdgeString = "arrowhead=open, color=red"
                                if cfg_local("SHOW_CARD") == 1 and cfg_local("D_3") == 0:
                                    redToolTip = ", tooltip=\"" + domTerm + " -> " +  attTerm + " [" + card + "]\""
                                else:
                                    redToolTip = ", tooltip=\"" + domTerm + " -> " +  attTerm + "\""
                                if cfg_local("CONSTR_OP") == 1:
                                    f4Line = lineOpen + domainConTemp + " " + domId.strip() + "\\n" + domTerm + "\"->\"" + attId + "\\n" + attTerm + "\" [" + redEdgeString + redToolTip + lineEnd
                                else:
                                    f4Line = lineOpen + domId.strip() + "\\n" + domTerm + "\"->\"" + attId + "\\n" + attTerm + "\" [" + redEdgeString + redToolTip + lineEnd
                                if f4Line not in f4List:
                                    f4List.append(f4Line)
                                blueToolTip = ", tooltip=\"" + attTerm + " -> " +  rangeTerm + "\""

                                if not domTerm in domainTTDict:
                                    domainTTDict.update({domTerm: [attTerm]})
                                else:
                                    if not attTerm in domainTTDict[domTerm]:
                                        domainTTDict[domTerm].append(attTerm)

                                if not rangeTerm in rangeTTDict:
                                    rangeTTDict.update({rangeTerm: [attTerm]})
                                else:
                                    if not attTerm in rangeTTDict[rangeTerm]:
                                        rangeTTDict[rangeTerm].append(attTerm)

                                if not attTerm in attTTRangeDict:
                                    attTTRangeDict.update({attTerm: [rangeTerm]})
                                else:
                                    if not rangeTerm in attTTRangeDict[attTerm]:
                                        attTTRangeDict[attTerm].append(rangeTerm)

                                if not attTerm in attTTDomDict:
                                    attTTDomDict.update({attTerm: [domTerm]})
                                else:
                                    if not domTerm in attTTDomDict[attTerm]:
                                        attTTDomDict[attTerm].append(domTerm)
                                # CONC - simplify model edges 1 - lose ids
                                if rangeId in concreteValues():
                                    f4Line = lineOpen + attId + "\\n" + attTerm + "\"->\"" + rangeTerm + "\" [" + blueEdgeString + blueToolTip + lineEnd
                                else:
                                    if cfg_local("CONSTR_OP") == 1:
                                        if collision:
                                            f4Line = lineOpen + attId + "\\n" + attTerm + "\"->\"" + domAndRangeDict[rangeId] + "\\n" + rangeTerm + "\" [" + blueEdgeString + blueToolTip + lineEnd
                                        else:
                                            f4Line = lineOpen + attId + "\\n" + attTerm + "\"->\"" + rangeOp + " " + rangeId + "\\n" + rangeTerm + "\" [" + blueEdgeString + blueToolTip + lineEnd
                                    else:
                                        f4Line = lineOpen + attId + "\\n" + attTerm + "\"->\"" + rangeId + "\\n" + rangeTerm + "\" [" + blueEdgeString + blueToolTip + lineEnd
                                if f4Line not in f4List:
                                    f4List.append(f4Line)
                            else:
                                if newAtt: # extend list of main dictionary if new one
                                    if not rangeId in attRangeDict[lastAtt]:
                                        attRangeDict[lastAtt].append(rangeId) # add range to list for this attribute
                                # extend list of local dictionary
                                if not rangeId in attRangeDictLocal[lastAtt]:
                                    attRangeDictLocal[lastAtt].append(rangeId)
                                if cfg_local("CONSTR_OP") == 1:
                                    print(domainConTemp, domId, lastCard, lastAtt, rangeOp, splitRow[4])
                                    f.write("\t".join([domainConTemp, domId, domTerm, lastCard, lastAtt, lastAttTerm, rangeOp, splitRow[4].split("|")[0], rangeTerm]) + '\n')
                                else:
                                    print(domId, domTerm, lastAtt, lastAttTerm, rangeId, rangeTerm)
                                    f.write("\t".join([domId, domTerm, lastAtt, lastAttTerm, rangeId, rangeTerm]) + '\n')
                                if conMetaFromConId_IN(lastAtt, False)[1] == 1:
                                    if refSetDetailsFromMemIdandSetId_IN(attDomainRefsetId, lastAtt)[0][2] == 0: # if attribute treated as ungrouped for DL
                                        blueEdgeString = "arrowhead=open, style=dashed, color=blue"
                                        redEdgeString = "arrowhead=open, style=dashed, color=red"
                                    else:
                                        blueEdgeString = "arrowhead=open, color=blue"
                                        redEdgeString = "arrowhead=open, color=red"
                                if cfg_local("SHOW_CARD") == 1 and cfg_local("D_3") == 0:
                                    redToolTip = ", tooltip=\"" + domTerm + " -> " +  lastAttTerm + " [" + lastCard + "]\""
                                else:
                                    redToolTip = ", tooltip=\"" + domTerm + " -> " +  lastAttTerm + "\""
                                if cfg_local("CONSTR_OP") == 1:
                                    f4Line = lineOpen + domainConTemp + " " + domId.strip() + "\\n" + domTerm + "\"->\""  + lastAtt + "\\n" + lastAttTerm + "\" [" + redEdgeString + redToolTip + lineEnd
                                else:
                                    f4Line = lineOpen + domId.strip() + "\\n" + domTerm + "\"->\"" + lastAtt + "\\n" + lastAttTerm + "\" [" + redEdgeString + redToolTip + lineEnd
                                if f4Line not in f4List:
                                    f4List.append(f4Line)
                                blueToolTip = ", tooltip=\"" + lastAttTerm + " -> " +  rangeTerm + "\""

                                if not lastAttTerm in attTTRangeDict:
                                    attTTRangeDict.update({lastAttTerm: [rangeTerm]})
                                else:
                                    if not rangeTerm in attTTRangeDict[lastAttTerm]:
                                        attTTRangeDict[lastAttTerm].append(rangeTerm)

                                if not rangeTerm in rangeTTDict:
                                    rangeTTDict.update({rangeTerm: [lastAttTerm]})
                                else:
                                    if not lastAttTerm in rangeTTDict[rangeTerm]:
                                        rangeTTDict[rangeTerm].append(lastAttTerm)
                                # CONC - simplify model edges 2 - lose ids
                                if rangeId in concreteValues():
                                    f4Line = lineOpen + lastAtt + "\\n" + lastAttTerm + "\"->\"" + rangeTerm + "\" [" + blueEdgeString + blueToolTip + lineEnd
                                else:
                                    if cfg_local("CONSTR_OP") == 1:
                                        f4Line = lineOpen + lastAtt + "\\n" + lastAttTerm + "\"->\"" + rangeOp + " " + rangeId + "\\n" + rangeTerm + "\" [" + blueEdgeString + blueToolTip + lineEnd
                                    else:
                                        f4Line = lineOpen + lastAtt + "\\n" + lastAttTerm + "\"->\"" + rangeId + "\\n" + rangeTerm + "\" [" + blueEdgeString + blueToolTip + lineEnd
                                if f4Line not in f4List:
                                    f4List.append(f4Line)

                    # compare main attribute range dict with local one
                    for attMain in attRangeDictLocal:
                        if attMain in attRangeDict:
                            if not(sorted(attRangeDict[attMain]) == sorted(attRangeDictLocal[attMain])):
                                if not attMain in attRangeCollisionList: # if not already in the attCollisionList
                                    attRangeCollisionList.append(attMain) # add it to the attCollisionList
                                    attRangeCollisionListTerm.append(pterm(attMain))
                                    # T0D0 - pickup A-D-V correspondences for use in att and range tooltips

            counter += 1
    f.close()

    # add all the relationship rows to f4
    if cfg_local("D_3") == 0:
        f4.write("\n/* *** Model rows *** */\n")
    if cfg_local("SHOW_MODEL") == 1:
        for line in sorted_cfg(f4List):
            f4.write(line)

    # fill parentDomainList
    for value in myValueRowsLimit:
        if len(value[2].split(" |")[0]) > 0:
            parentDomId = value[2].split(" |")[0]
            parentDomList.append((value[0], parentDomId))
            #add any novel parent domains to domain-containing lists:
            if not parentDomId in domAndRangeList:
                domAndRangeList.append(parentDomId)
            if not parentDomId in domList:
                domList.append(parentDomId)
            if cfg_local("CONSTR_OP") == 1:
                if not parentDomId in domAndRangeDict:
                    domAndRangeDict.update({parentDomId: parentDomId})

    # build subgraph of domain/range class relationships
    domAndRangeList = list(set(domAndRangeList))
    myAdjacencyList = adjacencyBuild(setToGraph(domAndRangeList, False, []), False)
    # CONC - test myAdjacencyList - if contains model component identiffier as target and only sources are SDI then set model component (and edge to root) invis.
    # showModelComponent = setShowModelComponent(myAdjacencyList)
    showModelComponent, myAdjacencyList = setShowModelComponent2(myAdjacencyList)
    if cfg_local("CONSTR_OP") == 1:
        for adj in myAdjacencyList:
            if not adj[0] in domAndRangeDict:
                domAndRangeDict.update({adj[0]:adj[0]})
            if not adj[1] in domAndRangeDict:
                domAndRangeDict.update({adj[1]:adj[1]})

    # draw domain/range adjacencies
    if cfg_local("D_3") == 0:
        f4.write("\n/* *** Domain and range/parent domain graph *** */\n")
    for adj in sorted_cfg(myAdjacencyList):
        print(adj[0] + "\t" + adj[1])
        if (adj in parentDomList) and (cfg_local("SHOW_PARENT_DOMAIN") == 1):
            adjToolTip = ", tooltip=\"" + pterm_unidec(adj[0]) + " is_a/has_parent_domain " +  pterm_unidec(adj[1]) + "\""
            arrowhead = "normal"
        else:
            adjToolTip = ", tooltip=\"" + pterm_unidec(adj[0]) + " is_a " +  pterm_unidec(adj[1]) + "\""
            arrowhead = "onormal"
        # CONC - hidden is_a relationships to model component
        if adj[0] in concreteValues():
            line = lineOpen + pterm_unidec(adj[0]) + "\"->\"" + domAndRangeDict[adj[1]]  + "\\n" + pterm_unidec(adj[1]) + "\" [style=invis" + adjToolTip + lineEnd
        else:
            # CONC - hide metadata concept if only present for concrete value 'codes'
            if (adj[0] == tok("MOD_COMP") and not showModelComponent):
                line = lineOpen + adj[0]  + "\\n" + pterm_unidec(adj[0]) + "\"->\"" + adj[1]  + "\\n" + pterm_unidec(adj[1]) + "\" [style=invis, arrowhead=" + arrowhead + ", color=black" + adjToolTip + lineEnd
            else:
                if cfg_local("CONSTR_OP") == 1:
                    line = lineOpen + domAndRangeDict[adj[0]]  + "\\n" + pterm_unidec(adj[0]) + "\"->\"" + domAndRangeDict[adj[1]]  + "\\n" + pterm_unidec(adj[1]) + "\" [arrowhead=" + arrowhead + ", color=black" + adjToolTip + lineEnd
                else:
                    line = lineOpen + adj[0]  + "\\n" + pterm_unidec(adj[0]) + "\"->\"" + adj[1]  + "\\n" + pterm_unidec(adj[1]) + "\" [arrowhead=" + arrowhead + ", color=black" + adjToolTip + lineEnd
        f4.write(line)

    # draw any other parent domain relationships
    if cfg_local("SHOW_PARENT_DOMAIN") == 1:
        for parentDom in sorted_cfg(parentDomList):
            if not parentDom in myAdjacencyList:
                adjToolTip = ", tooltip=\"" + pterm_unidec(parentDom[0]) + " has_parent_domain " +  pterm_unidec(parentDom[1]) + "\""
                arrowhead = "diamond"
                if cfg_local("CONSTR_OP") == 1:
                    line = lineOpen + domAndRangeDict[parentDom[0]]  + "\\n" + pterm_unidec(parentDom[0]) + "\"->\"" + domAndRangeDict[parentDom[1]]  + "\\n" + pterm_unidec(parentDom[1]) + "\" [arrowhead=" + arrowhead + ", color=black" + adjToolTip + lineEnd
                else:
                    line = lineOpen + parentDom[0]  + "\\n" + pterm_unidec(parentDom[0]) + "\"->\"" + parentDom[1]  + "\\n" + pterm_unidec(parentDom[1]) + "\" [arrowhead=" + arrowhead + ", color=black];\n"
                f4.write(line)

    # add domain and range classes
    if cfg_local("D_3") == 0:
        f4.write("\n/* *** Domain and range classes *** */\n")
    for domAndRange in sorted_cfg(domAndRangeList):
        if conMetaFromConId_IN(domAndRange, False)[3] == tok("DEFINED"):
            periph = "2, color=orange" if hasDef(domAndRange) else "2"
        else:
            periph = "1, color=orange" if hasDef(domAndRange) else "1"
        if conMetaFromConId_IN(domAndRange, False)[1] == 0:
            fillcolor = "orange"
        elif (domAndRange in domList) and (domAndRange in rangeList) and (domAndRange in [dom[0] for dom in myValueRowsLimit]):
            fillcolor = "violet"
        elif (domAndRange in domList) and not (domAndRange in rangeList):
            fillcolor = "lightpink"
        # CONC - identify SDI classes
        elif domAndRange in concreteValues():
            fillcolor = "palegreen, style=\"diagonals,filled\""
        else:
            fillcolor = "lightblue1"
        if cfg_local("CONSTR_OP") == 1:
            if domAndRange in domRangeCollisionList and fillcolor == "violet":
                fontcolor = "white"
            else:
                fontcolor = "black"
        nodeSizeString = ""
        if cfg_local("DOM_RANGE_SIZE") > 0 and (prePost == "pre"):
            # if a refset, check membership size
            if TCCompare_IN(domAndRange, tok('REFSETS')):
                testableRefsetType = refSetTypeFromConId_IN(domAndRange)
                for refset in testableRefsetType:
                    testRefset = refset[0]
                if cfg_local("DOM_RANGE_SIZE") == 1:
                    fontSize = str(int(refSetMemberCountFromConId_IN(domAndRange, testRefset) / 1000) + cfg_local("FONT_SIZE"))
                elif cfg_local("DOM_RANGE_SIZE") == 2 or cfg_local("DOM_RANGE_SIZE") > 3:
                    domMem = refSetMemberCountFromConId_IN(domAndRange, testRefset)
                    print(domAndRange + "\t" + str(domMem))
                    dimensionString = str(int(math.sqrt(cfg_local("DOM_DIM_MULT") * len(domList) * (domMem /allDomTot))))
                    dimensionStringMon = str(int(math.sqrt((cfg_local("DOM_DIM_MULT") * 1000) * (domMem / allDomTot))))
                    nodeSizeString = ', height = ' + dimensionString + ', width = ' + dimensionString
                    nodeSizeStringMon = ', height = ' + dimensionStringMon + ', width = ' + dimensionStringMon
                    nodeSizeStringPatchworkMon = ', area = ' + dimensionStringMon
                    fontSize = str((int(math.sqrt(cfg_local("DOM_FONT_MULT") * len(domList) * (domMem / allDomTot)))) + cfg_local("FONT_SIZE"))
                    fontSizeMon = str((int(math.sqrt((cfg_local("DOM_FONT_MULT") * 3000) * (domMem / allDomTot)))) + cfg_local("FONT_SIZE"))
                    if int(dimensionStringMon) < 4:
                        fontSizePatchworkMon = "4"
                    else:
                        fontSizePatchworkMon = str((int(math.sqrt((cfg_local("DOM_FONT_MULT") * 1) * (domMem / allDomTot)))) + cfg_local("FONT_SIZE"))
            # otherwise check descendant count
            else:
                if cfg_local("DOM_RANGE_SIZE") == 1:
                    fontSize = str(int(descendantCountFromConId_IN(domAndRange) / 1000) + cfg_local("FONT_SIZE"))
                elif cfg_local("DOM_RANGE_SIZE") == 2 or cfg_local("DOM_RANGE_SIZE") > 3:
                    domMem = descendantCountFromConId_IN(domAndRange)
                    print(domAndRange + "\t" + str(domMem))
                    dimensionString = str(int(math.sqrt(cfg_local("DOM_DIM_MULT") * len(domList) * (domMem / allDomTot))))
                    dimensionStringMon = str(int(math.sqrt((cfg_local("DOM_DIM_MULT") * 1000) * (domMem / allDomTot))))
                    nodeSizeString = ', height=' + dimensionString + ', width=' + dimensionString
                    nodeSizeStringMon = ', height=' + dimensionStringMon + ', width=' + dimensionStringMon
                    nodeSizeStringPatchworkMon = ', area=' + dimensionStringMon
                    fontSize = str((int(math.sqrt(cfg_local("DOM_FONT_MULT") * len(domList) * (domMem / allDomTot)))) + cfg_local("FONT_SIZE"))
                    fontSizeMon = str((int(math.sqrt((cfg_local("DOM_FONT_MULT") * 3000) * (domMem / allDomTot)))) + cfg_local("FONT_SIZE"))
                    if int(dimensionStringMon) < 4:
                        fontSizePatchworkMon = "4"
                    else:
                        fontSizePatchworkMon = str((int(math.sqrt((cfg_local("DOM_FONT_MULT") * 1) * (domMem / allDomTot)))) + cfg_local("FONT_SIZE"))
        else:
            fontSize = "9"
        if cfg_local("CONSTR_OP") == 1:
            # CONC - separate out the concrete ranges for tooltip
            if domAndRange in concreteValues():
                domAndRangeToolTip = pterm_unidec(domAndRange) + "\\n"
            else:
                domAndRangeToolTip = domAndRangeDict[domAndRange] + " " + pterm_unidec(domAndRange) + "\\n"
        else:
            domAndRangeToolTip = domAndRange + " " + pterm_unidec(domAndRange) + "\\n"
        if cfg_local("D_3") == 0:
            domAndRangeToolTip += definition_unidec(domAndRange)

        if cfg_local("CLASS_TOOLTIP") == 1 and cfg_local("D_3") == 0:
            tempHasDomain = []
            tempHasDomainNameDict = {}
            tempHasDomainCounter = 1
            tempHasDomainIdDict = {}
            if pterm_unidec(domAndRange) in domainTTDict:
                domAndRangeToolTip += "\\nIN DOMAIN OF:\\n"
                for att in sorted(domainTTDict[pterm_unidec(domAndRange)]):
                    domAndRangeToolTip += att + "\\n"
            if pterm_unidec(domAndRange) in rangeTTDict:
                for att in sorted(rangeTTDict[pterm_unidec(domAndRange)]):
                    for domainName in attTTDomDict[att]:
                        if not domainName in tempHasDomain:
                            tempHasDomain.append(domainName)
                        if not domainName in tempHasDomainIdDict:
                            tempHasDomainIdDict[domainName] = tempHasDomainCounter
                        if not domainName in tempHasDomainNameDict:
                            tempHasDomainNameDict[domainName] = [att]
                            tempHasDomainCounter += 1
                        else:
                            tempHasDomainNameDict[domainName].append(att)
            if pterm_unidec(domAndRange) in rangeTTDict:
                domAndRangeToolTip += "\\nIN RANGE OF:\\n"
                for att in sorted(rangeTTDict[pterm_unidec(domAndRange)]):
                    tempDomList = []
                    for domName in tempHasDomainNameDict:
                        if att in tempHasDomainNameDict[domName]:
                            tempDomList.append(str(tempHasDomainIdDict[domName]))
                    tempString = ",".join(sorted(tempDomList))
                    if len(tempHasDomain) == 1:
                        domAndRangeToolTip += att + "\\n"
                    else:
                        if att in attRangeCollisionListTerm:
                            DARTempList = [DAR for DAR in DARList if DAR[3] == att]
                            domAndRangeToolTip += att + " [" + tempString + "]*\\n" # currently just highlight. Should do something more (look at myValueRowsLimit.../prox prim refinement?)
                        else:
                            domAndRangeToolTip += att + " [" + tempString + "]\\n"
            if len(tempHasDomain) > 0:
                if len(tempHasDomain) == 1:
                    domAndRangeToolTip += "\\nFROM DOMAIN:\\n"
                    for domainName in tempHasDomainIdDict.items():
                        domAndRangeToolTip += domainName[0] + "\\n"
                else:
                    domAndRangeToolTip += "\\nFROM DOMAINS:\\n"
                    for domainName in sorted(tempHasDomainIdDict.items(), key=lambda x: x[1]):
                        if att in attRangeCollisionListTerm:
                            matchDAR = False
                            for DAR in DARTempList:
                                if (domAndRange == DAR[4]) and (domainName[0] == DAR[1]):
                                    matchDAR = True
                            if matchDAR:
                                domAndRangeToolTip += str(domainName[1]) + ". " + domainName[0] + "*\\n"
                            else:
                                domAndRangeToolTip += str(domainName[1]) + ". " + domainName[0] + "\\n"
                        else:
                            domAndRangeToolTip += str(domainName[1]) + ". " + domainName[0] + "\\n"
        # CONC - simplify range terms and apply correct formatting
        if domAndRange in concreteValues():
            f4.write(lineOpen + pterm_unidec(domAndRange) + "\" [shape=box, peripheries=" + periph + ", style=filled, fontsize=" + fontSize + nodeSizeString + ", fontname=Helvetica, fontcolor=black, tooltip=\"" + domAndRangeToolTip + "\", fillcolor=" + fillcolor + lineEnd)
            if cfg_local("DOM_RANGE_SIZE") > 3:
                mondrianList.append(lineOpen + pterm_unidec(domAndRange) + "\" [shape=box, peripheries=" + periph + ", style=filled, fontsize=" + fontSizeMon + nodeSizeStringMon + ", fontname=Helvetica, fontcolor=black, tooltip=\"" + domAndRangeToolTip + "\", fillcolor=" + fillcolor + lineEnd)
                mondrianPatchworkList.append(lineOpen + toString(textwrap.wrap(pterm_unidec(domAndRange), 25)) + "\" [shape=box, peripheries=1, style=filled, fontsize=" + fontSizePatchworkMon + nodeSizeStringPatchworkMon + ", fontname=Helvetica, fontcolor=black, tooltip=\"" + domAndRangeToolTip + "\", fillcolor=" + fillcolor + lineEnd)
        else:
            # CONC - hide metadata concept if only present for concrete value 'codes'
            if (domAndRange != tok("MOD_COMP")) or (domAndRange == tok("MOD_COMP") and showModelComponent):
                if cfg_local("CONSTR_OP") == 1:
                    f4.write(lineOpen + domAndRangeDict[domAndRange] + "\\n" + pterm_unidec(domAndRange) + "\" [shape=box, peripheries=" + periph + ", style=filled, fontsize=" + fontSize + nodeSizeString + ", fontcolor=" + fontcolor + ", fontname=Helvetica, tooltip=\"" + domAndRangeToolTip + "\", URL=\"http://snomed.info/id/" + domAndRange + "\", fillcolor=" + fillcolor + lineEnd)
                    if cfg_local("DOM_RANGE_SIZE") > 3:
                        mondrianList.append(lineOpen + domAndRangeDict[domAndRange] + "\\n" + pterm_unidec(domAndRange) + "\" [shape=box, peripheries=" + periph + ", style=filled, fontsize=" + fontSizeMon + nodeSizeStringMon + ", fontcolor=" + fontcolor + ", fontname=Helvetica, tooltip=\"" + domAndRangeToolTip + "\", URL=\"http://snomed.info/id/" + domAndRange + "\", fillcolor=" + fillcolor + lineEnd)
                        mondrianPatchworkList.append(lineOpen + domAndRangeDict[domAndRange] + "\\n" + toString(textwrap.wrap(pterm_unidec(domAndRange), 25)) + "\" [shape=box, peripheries=1, style=filled, fontsize=" + fontSizePatchworkMon + nodeSizeStringPatchworkMon + ", fontname=Helvetica, fontcolor=black, tooltip=\"" + domAndRangeToolTip + "\", URL=\"http://snomed.info/id/" + domAndRange + "\", fillcolor=" + fillcolor + lineEnd)
                else:
                    f4.write(lineOpen + domAndRange + "\\n" + pterm_unidec(domAndRange) + "\" [shape=box, peripheries=" + periph + ", style=filled, fontsize=" + fontSize + nodeSizeString + ", fontname=Helvetica, tooltip=\"" + domAndRangeToolTip + "\", URL=\"http://snomed.info/id/" + domAndRange + "\", fillcolor=" + fillcolor + lineEnd)
                    if cfg_local("DOM_RANGE_SIZE") > 3:
                        mondrianList.append(lineOpen + domAndRange + "\\n" + pterm_unidec(domAndRange) + "\" [shape=box, peripheries=" + periph + ", style=filled, fontsize=" + fontSizeMon + nodeSizeStringMon + ", fontname=Helvetica, tooltip=\"" + domAndRangeToolTip + "\", URL=\"http://snomed.info/id/" + domAndRange + "\", fillcolor=" + fillcolor + lineEnd)
                        mondrianPatchworkList.append(lineOpen + domAndRange + "\\n" + toString(textwrap.wrap(pterm_unidec(domAndRange), 25)) + "\" [shape=box, peripheries=1, style=filled, fontsize=" + fontSizePatchworkMon + nodeSizeStringPatchworkMon + ", fontname=Helvetica, tooltip=\"" + domAndRangeToolTip + "\", URL=\"http://snomed.info/id/" + domAndRange + "\", fillcolor=" + fillcolor + lineEnd)
    # CONC - hide metadata concept if only present for concrete value 'codes'
    if not showModelComponent:
        f4.write(lineOpen + tok("MOD_COMP") + "\\n" + pterm_unidec(tok("MOD_COMP")) + "\" [style=invis" + lineEnd)
    if cfg_local("SHOW_MODEL") == 1:
        # identify and add rows for role hierarchy relationships
        if cfg_local("D_3") == 0:
            f4.write("\n/* *** Role hierarchy relationships *** */\n")
        attList = list(set(attList))
        # CONC - only generate when needed - overwrites 'old' attributes TODO
        if attLimitBool and prePost == "pre" and cfg_local("ONLY_ATT") == 1:
            attNameList = [[pterm(att), att] for att in attList]
            attNameList = sorted(attNameList)
            f_att = io.open(cfgPathInstall() + 'files' + sep() + 'atts' + sep() + 'attLimit.txt', 'w', encoding="UTF-8")
            for att in attNameList:
                f_att.write("\t".join([att[1], att[0]]) + '\n')
            f_att.close()
        myAdjacencyList = adjacencyBuild(setToGraph(attList, False, []), False)
        for adj in sorted_cfg(myAdjacencyList):
            # print(adj)
            adjToolTip = ", tooltip=\"" + pterm_unidec(adj[0]) + " is_a " +  pterm_unidec(adj[1]) + "\""
            line = lineOpen + adj[0]  + "\\n" + pterm_unidec(adj[0]) + "\"->\"" + adj[1]  + "\\n" + pterm_unidec(adj[1]) + "\" [arrowhead=onormal, color=black" + adjToolTip + lineEnd
            if not tok('MOD_COMP') in line:
                f4.write(line)
        # ...and add rows for attribute classes
        if cfg_local("D_3") == 0:
            f4.write("\n/* *** Attribute classes *** */\n")
        for att in sorted_cfg(attList):
            nodeSizeString = ""
            if cfg_local("ATT_SIZE") > 0 and (prePost == "pre"):
                if cfg_local("ATT_SIZE") == 1:
                    attUse = attUseCount(att)
                    fontSize = str(int(attUse / 1000) + cfg_local("FONT_SIZE"))
                elif cfg_local("ATT_SIZE") == 2 or cfg_local("ATT_SIZE") > 3:
                    attUse = attUseInDomCount(att, attDomDict[att])
                    print(att + "\t" + str(attUse))
                    dimensionString = str(int(math.sqrt(cfg_local("ATT_DIM_MULT") * (attUse / domTot))))
                    dimensionStringMon = str(int(math.sqrt((cfg_local("ATT_DIM_MULT") * 1000) * (attUse / domTot))))
                    nodeSizeString = ', height=' + dimensionString + ', width=' + dimensionString
                    nodeSizeStringMon = ', height=' + dimensionStringMon + ', width=' + dimensionStringMon
                    nodeSizeStringPatchworkMon = ', area=' + dimensionStringMon
                    fontSize = str((int(math.sqrt(cfg_local("ATT_FONT_MULT") * (attUse / domTot)))) + cfg_local("FONT_SIZE"))
                    fontSizeMon = str((int(math.sqrt((cfg_local("ATT_FONT_MULT") * 2000) * (attUse / domTot)))) + cfg_local("FONT_SIZE"))
                    if int(dimensionStringMon) < 4:
                        fontSizePatchworkMon = "4"
                    else:
                        fontSizePatchworkMon = str((int(math.sqrt((cfg_local("ATT_FONT_MULT") * 1) * (attUse / domTot)))) + cfg_local("FONT_SIZE"))
                elif cfg_local("ATT_SIZE") == 3:
                    attUse = attUseInDomCount(att, attDomDict[att])
                    fontSize = str(int((attUse ** (1 + (cfg_local("ATT_POWER") / 10))) / (domTot * cfg_local("ATT_MULT"))) + cfg_local("FONT_SIZE"))
                if attUse == 0:
                    attFontColor = "red"
                else:
                    attFontColor = "black"
            else:
                fontSize = "9"
                attFontColor = "black"

            attToolTip = att + " " +pterm_unidec(att) + "\\n"
            if cfg_local("D_3") == 0:
                attToolTip += definition_unidec(att)

            if cfg_local("CLASS_TOOLTIP") == 1 and cfg_local("D_3") == 0:
                DARTempList = []
                DARCounter = 1
                DARDomDict = {}
                DARRangeDict = {}
                if att in attRangeCollisionList:
                    collisionNote = "*"
                    DARTempList = [DAR for DAR in DARList if DAR[2] == att]
                else:
                    collisionNote = ""
                if pterm_unidec(att) in attTTDomDict:
                    if len(attTTDomDict[pterm_unidec(att)]) == 1:
                        attToolTip += "\\nDOMAIN:\\n"
                    else:
                        attToolTip += "\\nDOMAINS" + collisionNote + ":\\n"
                    for dom in sorted(attTTDomDict[pterm_unidec(att)]):
                        if att in attRangeCollisionList:
                            attToolTip += str(DARCounter) + ". " + dom + "\\n"
                            DARDomDict.update({dom: DARCounter})
                            for DAR in DARTempList:
                                if dom == DAR[1]:
                                    if not DAR[5] in DARRangeDict:
                                        DARRangeDict.update({DAR[5]: [str(DARCounter)]})
                                    else:
                                        if not DARCounter in DARRangeDict[DAR[5]]:
                                            DARRangeDict[DAR[5]].append(str(DARCounter))
                            DARCounter += 1
                        else:
                            attToolTip += dom + "\\n"
                if pterm_unidec(att) in attTTRangeDict:
                    if len(attTTRangeDict[pterm_unidec(att)]) == 1:
                        attToolTip += "\\nRANGE:\\n"
                    else:
                        attToolTip += "\\nRANGES" + collisionNote + ":\\n"
                    for Range in sorted(attTTRangeDict[pterm_unidec(att)]):
                        if att in attRangeCollisionList:
                            tempString = ",".join(sorted(DARRangeDict[Range]))
                            attToolTip += Range + " [" + tempString + "]\\n"
                        else:
                            attToolTip += Range + "\\n"

            if conMetaFromConId_IN(att, False)[1] == 0:
                f4.write(lineOpen + att + "\\n" + pterm_unidec(att) + "\" [shape=box, peripheries=" + ("2, color=orange" if hasDef(att) else "2") + ", style=filled, fontsize=" + fontSize + nodeSizeString + ", fontname=Helvetica, fontcolor=" + attFontColor + ", tooltip=\"" + attToolTip + "\", URL=\"http://snomed.info/id/" + att + "\", fillcolor=orange" + lineEnd)
            elif att in attRangeCollisionList:
                f4.write(lineOpen + att + "\\n" + pterm_unidec(att) + "\" [shape=box, peripheries=" + ("2, color=orange" if hasDef(att) else "2") + ", style=filled, fontsize=" + fontSize + nodeSizeString + ", fontname=Helvetica, fontcolor=" + attFontColor + ", tooltip=\"" + attToolTip + "\", URL=\"http://snomed.info/id/" + att + "\", fillcolor=yellow" + lineEnd)
            else:
                f4.write(lineOpen + att + "\\n" + pterm_unidec(att) + "\" [shape=box, peripheries=" + ("2, color=orange" if hasDef(att) else "2") + ", style=filled, fontsize=" + fontSize + nodeSizeString + ", fontname=Helvetica, fontcolor=" + attFontColor + ", tooltip=\"" + attToolTip + "\", URL=\"http://snomed.info/id/" + att + "\", fillcolor=lemonchiffon1" + lineEnd)
            if cfg_local("ATT_SIZE") > 3:
                mondrianList.append(lineOpen + att + "\\n" + pterm_unidec(att) + "\" [shape=box, peripheries=2, style=filled, fontsize=" + fontSizeMon + nodeSizeStringMon + ", fontname=Helvetica, fontcolor=" + attFontColor + ", tooltip=\"" + attToolTip + "\", URL=\"http://snomed.info/id/" + att + "\", fillcolor=lemonchiffon1" + lineEnd)
                mondrianPatchworkList.append(lineOpen + att + "\\n" + toString(textwrap.wrap(pterm_unidec(att), 25)) + "\" [shape=box, peripheries=1, style=filled, fontsize=" + fontSizePatchworkMon + nodeSizeStringPatchworkMon + ", fontname=Helvetica, fontcolor=" + attFontColor + ", tooltip=\"" + attToolTip + "\", URL=\"http://snomed.info/id/" + att + "\", fillcolor=lemonchiffon1" + lineEnd)

    # send off mondrianList
    if cfg_local("ATT_SIZE") > 3 or cfg_local("DOM_RANGE_SIZE") > 3:
        mondrianDraw(mondrianList, mondrianPatchworkList)

    # add caption
    if cfg_local("SHOW_CAPTION") == 1 and cfg_local("D_3") == 0:
        f4.write("\n/* *** Caption *** */\n")
        today = date.today()
        dateLine = today.strftime("%B %d, %Y")
        versionString = getVersion(1)
        if myValueRows == myValueRowsLimit:
            domainString = "Showing all domains|"
        else:
            preDomainString = "Showing domains:\\n"
            for item in myValueRowsLimit:
                preDomainString += item[0] + " " + pterm_unidec(item[0]) + "\\n"
            domainString = preDomainString[:-2] + "|"

        if prePost == "pre":
            modelViewString = "Simplified diagram of SNOMED CT\\nMRCM Domain and Range constraints|Pre-coordination view |"
        else:
            modelViewString = "Simplified diagram of SNOMED CT\\nMRCM Domain and Range constraints|Post-coordination view |"

        if cfg_local("RANK_DIRECTION") == 1 or cfg_local("RANK_DIRECTION") == 2:
            f4.write("\"diagramLabel\" [label=\"{" + modelViewString + domainString + versionString + " International data|Drawn " + dateLine + "}\", ")
        else:
            f4.write("\"diagramLabel\" [label=\"" + modelViewString + domainString + versionString + " International data|Drawn " + dateLine + "\", ")
        f4.write("shape=record, fontsize=12];\n")

    # add key
    if cfg_local("SHOW_KEY") == 1 and cfg_local("D_3") == 0:
        f4.write("\n/* *** Key *** */\n")
        if cfg_local("CONSTR_OP") == 0:
            keyLineList = getKeyLines(False)
        else:
            keyLineList = getKeyLines(True)
        for line in keyLineList:
            f4.write(line)

    # close f4
    if cfg_local("D_3") == 1:
        f4.write("'}'")
    else:
        f4.write("}")
    f4.close()

    # reveal files to xdot
    renameDiagramFile("MRCM-pre_temp.dot", "MRCM-pre.dot")
    renameDiagramFile("MRCM-post_temp.dot", "MRCM-post.dot")

    # reset configuration
    if str(cfg_local("SHOW_KEY")) != keySetting:
        cfgReSet(keySetting)

    return myValueRows, myValueRowsLimit

def toString(inList):
    outString = ""
    if len(inList) == 1:
        outString =  inList[0]
    else:
        for item in inList[0:-1]:
            outString += item + "\\n"
        outString += inList[-1]
    return outString

def mondrianDraw(mondrianList, mondrianPatchworkList):
    # set colours 1 - normalize background text
    outList1 = []
    outList3 = []
    fillColorSwapDict = {"fillcolor=orange": "fillcolor=$$$$$",
                        "fillcolor=violet": "fillcolor=$$$$$",
                        "fillcolor=lightpink": "fillcolor=$$$$$",
                        "fillcolor=palegreen, style=\"diagonals,filled\"": "fillcolor=$$$$$",
                        "fillcolor=lightblue1": "fillcolor=$$$$$",
                        "fillcolor=lemonchiffon1": "fillcolor=$$$$$",
                        "fontcolor=black": "fontcolor=$$$$$",
                        "fontcolor=red": "fontcolor=$$$$$"}
    key_list = list(fillColorSwapDict.keys())
    for row, rowPatch in zip(mondrianList, mondrianPatchworkList):
        tempString = row
        tempStringPatch = rowPatch
        for key in key_list:
            tempString = tempString.replace(key, fillColorSwapDict[key])
            tempStringPatch = tempStringPatch.replace(key, fillColorSwapDict[key])
        outList1.append(tempString)
        outList3.append(tempStringPatch)
    # set colours 2 - assign suitable random colours to background and font
    outList2 = []
    outList4 = []
    for row, rowPatch in zip(outList1, outList3):
        colorNumber = int(random() * 100)
        if colorNumber < 60:
            colorString2 = "white"
        elif colorNumber < 70:
            colorString2 = "red"
        elif colorNumber < 80:
            colorString2 = "yellow"
        elif colorNumber < 90:
            colorString2 = "lightgray"
        else:
            colorString2 = "blue"
        tempString = row.replace("$$$$$", colorString2)
        tempStringPatch = rowPatch.replace("$$$$$", colorString2)
        outList2.append(tempString)
        outList4.append(tempStringPatch)
    # set peripheries/penwidth
    # write top (including background grouper)
    if cfg_local("ATT_SIZE") > 4 or cfg_local("DOM_RANGE_SIZE") > 4:
        outString = "digraph { rankdir=BT; ranksep=.3; overlap=false; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\nsubgraph cluster_0 { style=filled; color=black;  penwidth=" + str(cfg_local("PEN_WIDTH")) + ";\n"
    else:
        outString = "digraph { rankdir=BT; ranksep=.3; overlap=false; node [shape=box, fontsize=9, penwidth=5, fontname=Helvetica, style=filled, fillcolor=white];\n"
    f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'treemapIsh_temp.dot', 'w', encoding="UTF-8")
    f.write(outString)
    # write rows
    if cfg_local("ATT_SIZE") > 4 or cfg_local("DOM_RANGE_SIZE") > 4:
        for row in outList2:
            f.write(row)
    else:
        for row in mondrianList:
            f.write(row)
    # write tail
    if cfg_local("ATT_SIZE") > 4 or cfg_local("DOM_RANGE_SIZE") > 4:
        f.write("}\n}")
    else:
        f.write("}")
    f.close()
    renameDiagramFile("treemapIsh_temp.dot", "treemapIsh.dot")
    # draw patchwork output
    if cfg_local("ATT_SIZE") > 4 or cfg_local("DOM_RANGE_SIZE") > 4:
        outString = "digraph { layout=\"patchwork\"; ranksep=.3; overlap=false; node [shape=box, fontsize=9, penwidth=10, fontname=Helvetica, style=filled, fillcolor=white];\nsubgraph cluster_0 { style=filled; color=black;  penwidth=" + str(cfg_local("PEN_WIDTH")) + ";\n"
    else:
        outString = "digraph { layout=\"patchwork\"; ranksep=.3; overlap=false; node [shape=box, fontsize=9, penwidth=5, fontname=Helvetica, style=filled, fillcolor=white];\n"
    f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'patchwork_temp.dot', 'w', encoding="UTF-8")
    f.write(outString)
    # write rows
    if cfg_local("ATT_SIZE") > 4 or cfg_local("DOM_RANGE_SIZE") > 4:
        for row in outList4:
            f.write(row)
    else:
        for row in mondrianPatchworkList:
            f.write(row)
    # write tail
    if cfg_local("ATT_SIZE") > 4 or cfg_local("DOM_RANGE_SIZE") > 4:
        f.write("}\n}")
    else:
        f.write("}")
    f.close()
    renameDiagramFile("patchwork_temp.dot", "patchwork.dot")

# CONC - model component visible or not
def setShowModelComponent(myAdjacencyList):
    showModelComponent = False
    for adj in myAdjacencyList:
        if adj[1] == tok("MOD_COMP") and adj[0] not in concreteValues():
            showModelComponent = True
    return showModelComponent

# CONC - model component visible or not
def setShowModelComponent2(myAdjacencyList):
    showModelComponent = False
    returnList = []
    for adj in myAdjacencyList:
        if adj[1] == tok("MOD_COMP") and adj[0] not in concreteValues():
            showModelComponent = True
    if not showModelComponent:
        for adj in myAdjacencyList:
            if adj[1] == tok("MOD_COMP"):
                returnList.append((adj[1], tok("ROOT")))
            elif adj[0] != tok("MOD_COMP"):
                returnList.append(adj)
        return showModelComponent, returnList
    else:
        for adj in myAdjacencyList:
            if adj[1] == tok("MOD_COMP") and adj[0] in concreteValues():
                if (adj[1], tok("ROOT")) not in returnList:
                    returnList.append((adj[1], tok("ROOT")))
            else:
                if adj not in returnList:
                    returnList.append(adj)
        return showModelComponent, returnList

def cleanForModelTableCardOp(inString, inDomain):
    outList = []
    # break templates on cardinality
    outString = re.sub(r"(\[\[[0|1|*]\.\.[0|1|*]\]\] )", r"\nREPLACEME\t\1", inString)
    # break lists of concept constraints at OR clauses
    outString = re.sub(r"(OR [<<|<|\d])", r"\nREPLACEME\tREPLACEATT\t\1", outString)
    # break lists of concept constraints at cardinality
    outString = re.sub(r"(\[[0|1|*]..[0|1|*]\] )", r"\nREPLACEME\t\1", outString)
    # insert domain
    outString = outString.replace(r"REPLACEME", inDomain)
    # remove [[0..1]]
    outString = outString.replace(r"[[0..1]] ", "0..1\t")
    # remove [[0..*]]
    outString = outString.replace(r"[[0..*]] ", "0..*\t")
    # remove [[1..*]]
    outString = outString.replace(r"[[1..*]] ", "1..*\t")
    # remove [[1..1]]
    outString = outString.replace(r"[[1..1]] ", "1..1\t")
    # remove  = [[+id(<
    outString = outString.replace(r" = [[+id(< ", "\t<\t")
    # remove  = [[+id(<<
    outString = outString.replace(r" = [[+id(<< ", "\t<<\t")
    # # remove  = [+id(<< SPECIAL CASE for evaluation procedure error
    outString = outString.replace(r" = [+id(<< ", "\t<<\t")
    # remove  = [[+id(<
    outString = outString.replace(r" = [[+id(<", "\t<\t")
    # remove  = [[+id(
    outString = outString.replace(r" = [[+id(", "\t=\t")
    # remove  = [[+scg(<
    outString = outString.replace(r" = [[+scg(< ", "\t<\t")
    # remove  = [[+scg(<<
    outString = outString.replace(r" = [[+scg(<< ", "\t<<\t")
    # remove  = [[+scg(<
    outString = outString.replace(r" = [[+scg(<", "\t<\t")
    # remove  = [[+scg(
    outString = outString.replace(r" = [[+scg(", "\t=\t")
    # remove  = )]],
    outString = outString.replace(r")]],", "")
    # remove  = )]] }
    outString = outString.replace(r")]] }", "")
    # remove  = ))]]
    outString = outString.replace(r"))]]", "")
    # remove  = |)
    outString = outString.replace(r"|)", "|")
    # CONC - reinstate brackets/braces for concrete values:
    outString = outString.replace(" = [[+int(>#0.. ", "\t=\t[[+int(>#0..)]] ")
    outString = outString.replace(" = [[+dec(>#0.. ", "\t=\t[[+dec(>#0..)]] ")
    outString = outString.replace(" = [[+str(\"*\" ", "\t=\t[[+str(\"*\")]] ")
    # remove  = OR <<
    outString = outString.replace(r"OR << ", "\t<<\t")
    # remove  = OR <<
    outString = outString.replace(r"OR < ", "\t<\t")
    # remove  = OR
    outString = outString.replace(r"OR ", "\t=\t")
    # remove [0..0] 408729009 |Finding context| = *,
    outString = outString.replace(r"[0..0] 408729009 |Finding context| = *,", "")
    # remove [0..0] 246090004 |Associated finding| = *):
    outString = outString.replace(r"[0..0] 246090004 |Associated finding| = *):", "")
    # remove [0..0] 408730004 |Procedure context| = *,
    outString = outString.replace(r"[0..0] 408730004 |Procedure context| = *,", "")
    # remove [0..0] 363589002 |Associated procedure| = *):
    outString = outString.replace(r"[0..0] 363589002 |Associated procedure| = *):", "")
    outList = outString.split('\n')
    return outList

def modelLimiterSelect():
    vStringLen, vString = getVersion(3)
    if vStringLen > 3: # UK data (currently in step with international)
        modelLimiterList = [[tok("OBS_ENT")], \
                [tok("EVAL_PROC")], \
                [tok("PROCEDURE")], \
                [tok("SURG_PROC")], \
                [tok("CLIN_FIND")], \
                [tok("EVENT")], \
                [tok("SWEC"), tok("PWEC"), tok("FWEC")], \
                [tok("ADMIN_SUB_ROUTE")], \
                [tok("PHARM_PROD"), tok("MED_PROD_PACK"), tok("BASIC_DOSE"), tok("PHAR_DOSE_FORM")], \
                [tok("BODY_STRUC"), tok("ANAT_STRUC"), tok("LAT_STRUC_RS")], \
                [tok("SPECIMEN"), tok("SUBSTANCE")], \
                [tok("PHYS_OBJ")]]
    elif vString >= '20200731': # post-physical object international
        modelLimiterList = [[tok("OBS_ENT")], \
                [tok("EVAL_PROC")], \
                [tok("PROCEDURE")], \
                [tok("SURG_PROC")], \
                [tok("CLIN_FIND")], \
                [tok("EVENT")], \
                [tok("SWEC"), tok("PWEC"), tok("FWEC")], \
                [tok("ADMIN_SUB_ROUTE")], \
                [tok("PHARM_PROD"), tok("MED_PROD_PACK"), tok("BASIC_DOSE"), tok("PHAR_DOSE_FORM")], \
                [tok("BODY_STRUC"), tok("ANAT_STRUC"), tok("LAT_STRUC_RS")], \
                [tok("SPECIMEN"), tok("SUBSTANCE")], \
                [tok("PHYS_OBJ")]]
    else: # old pre-physical object international
        modelLimiterList = [[tok("OBS_ENT")], \
                [tok("EVAL_PROC")], \
                [tok("PROCEDURE")], \
                [tok("SURG_PROC")], \
                [tok("CLIN_FIND")], \
                [tok("EVENT")], \
                [tok("SWEC"), tok("PWEC"), tok("FWEC")], \
                [tok("ADMIN_SUB_ROUTE")], \
                [tok("PHARM_PROD"), tok("MED_PROD_PACK"), tok("BASIC_DOSE"), tok("PHAR_DOSE_FORM")], \
                [tok("BODY_STRUC"), tok("ANAT_STRUC"), tok("LAT_STRUC_RS")], \
                [tok("SPECIMEN"), tok("SUBSTANCE")]]
    return modelLimiterList

def generateDiagram():
    # if generating d3 output
    if cfg_local("D_3") == 1:
        # write opening 'var dots = [' to output file
        with io.open(cfgPathInstall() + "files/d3/pre-model-cycle.html", "w", encoding="utf-8") as fout:
            readbackList = []
            with io.open(cfgPathInstall() + "files/d3/opening.txt", "r", encoding="utf-8") as fheader:
                for line in fheader:
                    readbackList.append(line)
            for line in readbackList:
                fout.write(line)
            fout.write("\n\nvar dots = [\n")
            # set of domain 'groups' used for D3 animation generation.
            # needs to be kept in step with modelLimitID.txt rows
            # but also may just need tweaking anyway (not all model groups will render with splines=ortho setting)
            modelLimiterList = modelLimiterSelect()
            # work through a list of identifier sets, progressively rewriting the # characters in modelLimit
            itemListCounter = 0
            for itemList in modelLimiterList:
                # clean modelLimit then refill with new limited set
                readbackList = []
                with io.open(cfgPathInstall() + "files" + sep() + 'mrcm' + sep() + "modelLimitID.txt", "r", encoding="utf-") as f:
                    for line in f:
                        readbackList.append(line)
                with io.open(cfgPathInstall() + "files" + sep() + 'mrcm' + sep() + "modelLimitID.txt", "w", encoding="utf-") as f:
                    for line in readbackList:
                        line = line.replace("# ", "")
                        for item in itemList:
                            if item in line:
                                line = "# " + line
                        f.write(line)
                # run generateMRCMDotFile (pre) against each modelLimit setting
                generateMRCMDotFile(tok("MRCM_DIR"), tok("MRCM_ADIR"), "pre")
                # open MRCM-pre.dot and readback its rows
                readbackList = []
                with io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-pre.dot', "r", encoding="utf-") as fin:
                    for line in fin:
                        readbackList.append(line)
                fout.write("\t[\n")
                for line in readbackList:
                    fout.write(line)
                itemListCounter += 1
                if itemListCounter < len(modelLimiterList):
                    fout.write("\n\t],\n")
                else:
                    fout.write("\n\t]\n")
            fout.write("];\n</script>\n</body>\n")

        if cfg_local("WIN_HOST_FILES") == 1:
            copyPreModelCycleD3()
    # or 'normal' output
    else:
        # backup outupt files in windows host folder
        if cfg_local("WIN_HOST_FILES") == 1:
            backupMRCMOutputs()
        if cfg_local("RUN_PRE") == 1:
            # pre-coordination (modelTableAtts2)
            myValueRows, myValueRowsLimit = generateMRCMDotFile(tok("MRCM_DIR"), tok("MRCM_ADIR"), "pre")
        if cfg_local("RUN_POST") == 1:
            # post-coordination (modelTableAtts3)
            myValueRows, myValueRowsLimit = generateMRCMDotFile(tok("MRCM_DIR"), tok("MRCM_ADIR"), "post")
        # copy files to windows host folder
        if cfg_local("WIN_HOST_FILES") == 1:
            newMRCMOutputs()

    vStringLen = getVersion(2)
    if vStringLen > 3:
        backupOldModelFile()
    else:
        backupNewModelFile()

    # generate SVG files if not D-3
    if cfg_local("GENERATE_SVG") == 1 and cfg_local("D_3") == 0 and cfg_local("ONLY_ATT") == 0:
        svgInt = 0
        copyBool = False
        if cfg_local("RUN_PRE") == 1 and cfg_local("RUN_POST") == 1:
            svgInt = 2
        elif cfg_local("RUN_PRE") == 1 and cfg_local("RUN_POST") == 0:
            svgInt = 0
        elif cfg_local("RUN_PRE") == 0 and cfg_local("RUN_POST") == 1:
            svgInt = 1
        if cfg_local("LOCAL_FILES") == 1:
            modelToSVGLocal(svgInt)
            copyBool = True
        if cfg_local("WIN_HOST_FILES") == 1:
            modelToSVGWinHost(svgInt, copyBool)

    # replicate all domains
    if cfg_local("D_3") == 0:
        if myValueRows == myValueRowsLimit:
            if cfg_local("RUN_PRE") == 1:
                backupAllDomainMRCMOutputsPre()
            if cfg_local("RUN_POST") == 1:
                backupAllDomainMRCMOutputsPost()

        if cfg_local("RUN_PRE") == 1:
            return myValueRows, myValueRowsLimit

if __name__ == '__main__':
    generateDiagram()
