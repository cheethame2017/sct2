import sqlite3
from SCTRF2Helpers import pterm
from SCTRF2Funcs import concCheck
from SCTRF2Concrete import toNumber, concreteTakingRoles
from SCTRF2config import pth
# CONC - entire script

def concRows(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("SELECT DISTINCT destinationId, typeId FROM relationships where active=1")
    recordset = cur.fetchall()
    numberDict1 = {}
    numberDict2 = {}
    concreteTakingRolesList = concreteTakingRoles()
    for item in recordset:
        if item[1] in concreteTakingRolesList:
            print(item[0], str(toNumber(pterm(item[0]))))
            if not item[0] in numberDict1:
                numberDict1[item[0]] = toNumber(pterm(item[0]))
            if "#" + str(toNumber(pterm(item[0]))) not in numberDict2:
                numberDict2["#" + str(toNumber(pterm(item[0])))] = toNumber(pterm(item[0]))

    cur.close
    conn.close

    return numberDict1, numberDict2

def fillConc(inTable, dbPath1, inDict):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    for item in inDict:
        if inTable == 1:
            cur.execute("insert into conc1 values (? , ?)", (item, inDict[item]))
        else:
            cur.execute("insert into conc2 values (? , ?)", (item, inDict[item]))
    conn.commit()
    cur.close
    conn.close

def concBuild(snapPath, concPath):
    concCheck()
    # get all the numbers used
    myDict1, myDict2 = concRows(snapPath)
    print(myDict1)
    print()
    print(len(myDict1))
    print()
    for item in sorted(myDict1.items(), key=lambda x: x[1]):
        print(item[0], str(myDict1[item[0]]))

    print(myDict2)
    print()
    print(len(myDict2))
    print()
    for item in sorted(myDict2):
        print(item, str(myDict2[item]))

    # put numbers into the tables
    fillConc(1, concPath, myDict1)
    fillConc(2, concPath, myDict2)

if __name__ == '__main__':
    snapPath = pth('RF2_SNAP')
    concPath = pth('RF2_OUT')
    concBuild(snapPath, concPath)
