import subprocess
from SCTRF2config import pth
from platformSpecific import cfgPathInstall

dataPath = pth('BASIC_DATA_BUILD') # location of data files during construction
codePath = cfgPathInstall()

# run importAndBuildPrelims.sh
subprocess.call([codePath + "scripts/importAndBuildTidy.sh", dataPath])
