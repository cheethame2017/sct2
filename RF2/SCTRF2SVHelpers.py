from __future__ import print_function, unicode_literals
import io
import sqlite3
import random
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
from neo4j import GraphDatabase
from platformSpecific import clearScreen, progressTrackCmd, startNeoPlatform, stopNeoPlatform, cfgPathInstall, sep
from SCTRF2Helpers import fsnFromConId_IN, tuplify, refSetTypeFromConId_IN, refsetPterm, listify
from SCTRF2SVconfig import pth, tokList, cfg, tok
from SCTRF2Refset import findRefsetMetaFields, refsetRecordsetMetaDecomp
# from neo4j.v1 import GraphDatabase

# note: strictly speaking should not be any print() calls from this layer.
#       However used in three methods:
#       - neoQuery
#       - SV_SIMcheckIsARefset
#       - SV_SIMC1_IsA_C2_Compare
#       Can be controlled by setting helperDisplayBool when methods called.

# fullPath = "/home/ed/Code/RF2Full.db"
fullPath = pth('RF2_FULL')
modsetPath = pth('MODSET')
outPath = pth('RF2_OUT')
snapPath = pth('RF2_SNAP')
neoPath = pth('NEO4J')

RLRParts = tokList('RLRPARTS')
otherLangSet = tokList('OTHERLANGSET')
ownModuleSet = tokList('OWNMODULESET')
intModule = tokList('INTMODULE')


def SV_SIMparentsFromIdPlusMeta_IN(inId, activeStatus, effectiveTime):
    parents = []
    roleList = []
    roleList = SV_SIMgroupedRolesFromConIdPlusMeta_IN(inId[0], activeStatus, effectiveTime)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    parents.append(role)
    return parents


def SV_SIMrolesFromIdPlusMeta_IN(inId, activeStatus, effectiveTime):
    roleList = []
    roleList = SV_SIMgroupedRolesFromConIdPlusMeta_IN(inId, activeStatus, effectiveTime)
    #for roleGroup in roleList:
        #for role in roleGroup:
            #if not isinstance(role, int):
                #if role[0] != -1:
                    #roles.append(role)
    return roleList


def SV_SIMchildrenFromIdPlusMeta_IN(inId, activeStatus, effectiveTime):
    children = []
    roleList = []
    roleList = SV_SIMchildrenFromConIdPlusMeta_IN(inId[0], activeStatus, effectiveTime)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    children.append(role)
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return children
    else:
        return roleList


def SV_MDRparentsFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    parents = []
    roleList = []
    roleList = SV_MDRparentsFromConIdPlusMeta_IN(inId[0], activeStatus, effTimeSet)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    parents.append(role)
    return parents


def SV_MDRrolesFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    roleList = []
    roleList = SV_MDRgroupedRolesFromConIdPlusMeta_IN(inId, activeStatus, effTimeSet)
    return roleList


def SV_MDRchildrenFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    children = []
    childList = []
    childList = SV_MDRchildrenFromConIdPlusMeta_IN(inId[0], activeStatus, effTimeSet)
    for childGroup in childList:
        for child in childGroup:
            if not isinstance(child, int):
                if child[0] == -1:
                    children.append(child)
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return children
    else:
        return childList


def SV_MDRModCorrelparentsFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    parents = []
    roleList = []
    roleList = SV_MDRModCorrelgroupedRolesFromConIdPlusMeta_IN(inId[0], activeStatus, effTimeSet)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    parents.append(role)
    return parents


def SV_SIMchildrenFromConIdPlusMeta_IN(idIn, activeFlag, effectiveTime):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    ## children
    if activeFlag == 1:
        cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=1 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (idIn, effectiveTime))
    elif activeFlag == 2:
        cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (idIn, effectiveTime))
    elif activeFlag == 3:
        cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where destinationId = ?", tuplify(idIn))
    else:
        cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=0 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (idIn, effectiveTime))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == tok('IS_A'):
            tempSet = (role[0] - 1, role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == tok('IS_A'):
            tempSet = (role[0], role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
        preRoleList.append(tempSet)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    # attempt at plusses:
    if (cfg("SUB_SHOW") == 1) and (cfg("QUERY_TYPE") != 1):
        roleListOut = []
        for roleSet in roleList:
            if not roleSet is None:
                for role in roleSet:
                    if not isinstance(role, int):
                        if role[0] == -1:
                            if activeFlag == 1:
                                cursorIn2.execute("select typeId from relationships where active=1 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (role[3], effectiveTime))
                            elif activeFlag == 2:
                                cursorIn2.execute("select typeId from relationships where destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (role[3], effectiveTime))
                            elif activeFlag == 3:
                                cursorIn2.execute("select typeId from relationships where destinationId = ?", tuplify(role[3]))
                            else:
                                cursorIn2.execute("select typeId from relationships where active=0 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (role[3], effectiveTime))
                            recordsetIn = cursorIn2.fetchall()
                            hasSubBool = False
                            for role2 in recordsetIn:
                                if role2[0] == tok('IS_A'):
                                    hasSubBool = True
                            #if role[9] == 1:
                                ## if not recordsetIn:
                                #if not hasSubBool:
                                    #tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" +  str(role[4]) + "|" +  str(role[5]) + "|" +  str(role[6]) + "|" +  str(role[7]) + "|" +  str(role[8]) + "|" +  str(role[9]) + "| "
                                #else:
                                    #tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" +  str(role[4]) + "|" +  str(role[5]) + "|" +  str(role[6]) + "|" +  str(role[7]) + "|" +  str(role[8]) + "|" +  str(role[9]) + "|+"
                            #else:
                                #tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" +  str(role[4]) + "|" +  str(role[5]) + "|" +  str(role[6]) + "|" +  str(role[7]) + "|" +  str(role[8]) + "|" +  str(role[9]) + "|*"
                            if role[9] == 1:
                                # if not recordsetIn:
                                if not hasSubBool:
                                    tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]) + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "| |" + str(role[10])
                                else:
                                    tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]) + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|+|" + str(role[10])
                            else:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]) + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|*|" + str(role[10])
                            roleListOut.append(tempstring.split("|"))
    # end attempt at plusses
    cursorIn2.close()
    connectionIn2.close()
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return roleList
    else:
        return roleListOut


def SV_SIMrefsetRowsfromId(inId, refsetType, activeFlag, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    if activeFlag == 1:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where active=1 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    elif activeFlag == 2:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    elif activeFlag == 3:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where referencedComponentId= ?", (str(inId),))
    else:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where active=0 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = (refSet[0], pterm(refSet[0]), refSet[1], refSet[2], refSet[3], pterm(refSet[3]))
        refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[2])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def SV_SIMgroupedRolesFromConIdPlusMeta_IN(idIn, activeFlag, effectiveTime):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    # children
    if activeFlag == 1:
        cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=1 and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (idIn, effectiveTime))
    elif activeFlag == 2:
        cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (idIn, effectiveTime))
    elif activeFlag == 3:
        cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where sourceId = ?", tuplify(idIn))
    else:
        cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=0 and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (idIn, effectiveTime))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == tok('IS_A'):
            tempSet = (role[0] - 1, role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == tok('IS_A'):
            tempSet = (role[0], role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
        preRoleList.append(tempSet)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList


def SV_MDRparentsFromConIdPlusMeta_IN(idIn, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    preRoleList = []
    rgIntList = []
    coverString = ""
    for row in effTimeSet:
        # parents
        coverString = progressTrack('START', "*", coverString)
        if activeFlag == 0:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where typeId='" + tok('IS_A') + "' and active=0 and moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], idIn, row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where typeId='" + tok('IS_A') + "' and moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], idIn, row[1]))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where typeId='" + tok('IS_A') + "' and active=1 and moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], idIn, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for role in recordsetIn2:
            coverString = progressTrack('START', "+", coverString)
            tempSet = (role[0] - 1, role[1], pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            preRoleList.append(tempSet)
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
    coverString = progressTrack('END', "", coverString)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList





def SV_MDRgroupedRolesFromConIdPlusMeta_IN(idIn, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    preRoleList = []
    rgIntList = []
    coverString = ""
    for row in effTimeSet:
        # roles/IS_As
        coverString = progressTrack('START', "*", coverString)
        if activeFlag == 0:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=0 and moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], idIn, row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], idIn, row[1]))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=1 and moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], idIn, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for role in recordsetIn2:
            coverString = progressTrack('START', "+", coverString)
            if role[1] == tok('IS_A'):
                tempSet = (role[0] - 1, role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not (role[0] - 1) in rgIntList:
                    rgIntList.append(role[0] - 1)
            if not role[1] == tok('IS_A'):
                tempSet = (role[0], role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not role[0] in rgIntList:
                    rgIntList.append(role[0])
    coverString = progressTrack('END', "", coverString)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList


def SV_MDRchildrenFromConIdPlusMeta_IN(inId, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    preRoleList = []
    rgIntList = []
    coverString = ""
    for row in effTimeSet:
        # children
        coverString = progressTrack('START', "*", coverString)
        if activeFlag == 0:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where typeId='" + tok('IS_A') + "' and active=0 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], inId, row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where typeId='" + tok('IS_A') + "' and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], inId, row[1]))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where typeId='" + tok('IS_A') + "' and active=1 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], inId, row[1]))
            # cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?) and active=1 and moduleId = ? and destinationId = ?", (row[1], row[0], inId))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for role in recordsetIn2:
            coverString = progressTrack('START', "+", coverString)
            if cfg('SHOW_METADATA') == 0:
                tempSet = (role[0] - 1, role[1], role[1], role[2], SV_pterm(role[2]), role[3], role[4], role[5], role[6], role[7], role[3], role[8])
            else:
                tempSet = (role[0] - 1, role[1], pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[3], role[8])
            preRoleList.append(tempSet)
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
    coverString = progressTrack('END', "", coverString)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    if (cfg("SUB_SHOW") == 1) and (cfg("QUERY_TYPE") != 1):  # suspicious as to why this works - ? should use standard efftime format but currently doesn't' ok now!
        roleListOut = []
        for roleSet in roleList:
            if not roleSet is None:
                for role in roleSet:
                    matchBool = False
                    coverString = progressTrack('START', ".", coverString)
                    if (not isinstance(role, int)) and (role[0] == -1):
                        for row in effTimeSet:  # swap execute lines and indent below to # marker
                            if activeFlag == 0:
                                cursorIn2.execute("select typeId from relationships where typeId='" + tok('IS_A') + "' and active=0 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], role[3], row[1]))
                            elif activeFlag == 2:
                                cursorIn2.execute("select typeId from relationships where typeId='" + tok('IS_A') + "' and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], role[3], row[1]))
                            else:
                                cursorIn2.execute("select typeId from relationships where typeId='" + tok('IS_A') + "' and active=1 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], role[3], row[1]))
                            recordsetIn = cursorIn2.fetchall()
                            for row2 in recordsetIn:
                                if row2[0] == tok('IS_A'):
                                    matchBool = True  # marker
                        if role[9] == 1:
                            if not matchBool:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]) + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "| |" + str(role[10])
                            else:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]) + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|+|" + str(role[10])
                        else:
                            tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]) + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|*|" + str(role[10])
                        roleListOut.append(tempstring.split("|"))
            coverString = progressTrack('END', "", coverString)
    cursorIn2.close()
    connectionIn2.close()
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return roleList
    else:
        return roleListOut

def SV_MDRModCorrelgroupedRolesFromConIdPlusMeta_IN(idIn, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    preRoleList = []
    rgIntList = []
    for row in effTimeSet:
        # children
        if activeFlag == 0:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=0 and sourceId = ?", tuplify(idIn))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.moduleId = relationships.moduleId and rel2.effectiveTime <= ?)", (row[0], idIn, row[1]))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=1 and sourceId = ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for role in recordsetIn2:
            if role[1] == tok('IS_A'):
                tempSet = (role[0] - 1, role[1], pterm(role[1]), role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not (role[0] - 1) in rgIntList:
                    rgIntList.append(role[0] - 1)
            if not role[1] == tok('IS_A'):
                tempSet = (role[0], role[1], pterm(role[1]), role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not role[0] in rgIntList:
                    rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList


def SV_SIMconMetaFromConId_IN(idIn, effectiveTime):
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn1 = sqlite3.connect(fullPath)
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()

    cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.effectiveTime <= ? )", (idIn, effectiveTime))
    recordsetIn1 = cursorIn1.fetchone()
    if recordsetIn1:
        if recordsetIn1[1] == 0:
            cursorIn2 = connectionIn2.cursor()
            # unusually need to include active=1 to force return of latest active value.
            cursorIn2.execute("select componentId1 from crefset where active=1 and refsetId='" + tok('INAC_CON') + "' and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.id = crefset.id and cr2.effectiveTime <= ? )", (idIn, effectiveTime))
            recordsetIn2 = cursorIn2.fetchall()
            if len(recordsetIn2) == 1:
                reasonForInactivation = pterm(recordsetIn2[0])
            conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
            cursorIn2.close()
        else:
            conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
        conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn1.close()
    connectionIn2.close()
    return conMetaSet


def SV_SIMconMetaFromConIdSimple_IN(inId, effectiveTime, ptermBool):
    conMetaSetpre = ()
    conMetaSet = []
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.effectiveTime <= ? )", (inId, effectiveTime))
    recordsetIn1 = cursorIn1.fetchone()
    if recordsetIn1:
        if ptermBool:
            conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
        else:
            conMetaSetpre = (recordsetIn1[0], recordsetIn1[1], recordsetIn1[2], recordsetIn1[3])
        conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn2.close()
    return conMetaSet


def SV_MDRconMetaFromConId_IN(idIn, effTimeSet):
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()

    for row in effTimeSet:
        #cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.moduleId = ? and c2.effectiveTime <= ? )", (row[0], id, row[0], row[1]))
        cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.effectiveTime <= ? )", (row[0], idIn, row[1]))

        recordsetIn1b = cursorIn1.fetchall()
        if len(recordsetIn1b) > 0:
            recordsetIn1 = recordsetIn1b[0]
            if recordsetIn1[1] == 0:
                cursorIn2 = connectionIn2.cursor()
                # unusually need to include active=1 to force return of latest active value.
                cursorIn2.execute("select componentId1 from crefset where active=1 and refsetId='" + tok('INAC_CON') + "' and moduleId = ? and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.id = crefset.id and cr2.effectiveTime <= ? )", (row[0], idIn, row[1]))
                recordsetIn2 = cursorIn2.fetchall()
                if len(recordsetIn2) == 1:
                    reasonForInactivation = pterm(recordsetIn2[0])
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
                cursorIn2.close()
            else:
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
            conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn2.close()
    return conMetaSet


def SV_ALLconMetaFromConId_IN(idIn):  # conceptDetails for all data
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()

    cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.id = ?", (idIn, ))
    recordsetIn1b = cursorIn1.fetchall()
    if len(recordsetIn1b) > 0:
        for recordsetIn1 in recordsetIn1b:
        # recordsetIn1 = recordsetIn1b[0]
            if recordsetIn1[1] == 0:
                cursorIn2 = connectionIn2.cursor()
                # unusually need to include active=1 to force return of latest active value.
                cursorIn2.execute("select componentId1 from crefset where active=1 and refsetId='" + tok('INAC_CON') + "' and referencedComponentId= ?", (idIn, ))
                recordsetIn2 = cursorIn2.fetchall()
                if len(recordsetIn2) == 1:
                    reasonForInactivation = pterm(recordsetIn2[0])
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
                cursorIn2.close()
            else:
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
            conMetaSet.append(conMetaSetpre)
            conMetaSet.sort(key=lambda x: x[2])
    cursorIn1.close()
    connectionIn2.close()
    return conMetaSet


def SV_MDRModCorrelconMetaFromConId_IN(idIn, effTimeSet):
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()

    for row in effTimeSet:
        cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.moduleId = ? and c2.effectiveTime <= ? )", (row[0], idIn, row[0], row[1]))
        #cursorIn1.executle("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and and c2.effectiveTime <= ? )", (row[0], id, row[1]))
        recordsetIn1b = cursorIn1.fetchall()
        if len(recordsetIn1b) > 0:
            recordsetIn1 = recordsetIn1b[0]
            if recordsetIn1[1] == 0:
                cursorIn2 = connectionIn2.cursor()
                # unusually need to include active=1 to force return of latest active value.
                cursorIn2.execute("select componentId1 from crefset where active=1 and refsetId='" + tok('INAC_CON') + "' and moduleId = ? and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.referencedComponentId = crefset.referencedComponentId and cr2.effectiveTime <= ? )", (row[0], idIn, row[1]))
                recordsetIn2 = cursorIn2.fetchall()
                if len(recordsetIn2) == 1:
                    reasonForInactivation = pterm(recordsetIn2[0])
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
                cursorIn2.close()
            else:
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
            conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn2.close()
    return conMetaSet

def SV_MDRdesMetaFromDesId_IN(idIn, effTimeSet):
    reasonForInactivation = "Reason not stated"  # handles nothing returned
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    for row in effTimeSet:
        cursorIn2.execute("select componentId1 from crefset where active=1 and refsetId='" + tok('INAC_DES') + "' and moduleId = ? and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.id = crefset.id and cr2.effectiveTime <= ? )", (row[0], idIn, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        if len(recordsetIn2) == 1:
            reasonForInactivation = pterm(recordsetIn2[0])
    cursorIn2.close()
    connectionIn2.close()
    return reasonForInactivation

def SV_SIMdesMetaFromDesId_IN(idIn, effectiveTime):
    reasonForInactivation = "Reason not stated"  # handles nothing returned
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select componentId1 from crefset where active=1 and refsetId='" + tok('INAC_DES') + "' and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.id = crefset.id and cr2.effectiveTime <= ? )", (idIn, effectiveTime))
    recordsetIn2 = cursorIn2.fetchall()
    if len(recordsetIn2) == 1:
        reasonForInactivation = pterm(recordsetIn2[0])
    cursorIn2.close()
    connectionIn2.close()
    return reasonForInactivation


def effTimeSet():
    if int(cfg("EFFECTIVE_TIME")) > 20040131:
        EffTimeSet = prelim(cfg("EFFECTIVE_TIME"), ownModuleSet)
    else:
        EffTimeSet = prelim(cfg("EFFECTIVE_TIME"), (ownModuleSet + intModule))
    return EffTimeSet


def prelim(seedTime, seedModules):
    connectionIn1 = sqlite3.connect(modsetPath)

    cursorIn1 = connectionIn1.cursor()
    cursorIn2 = connectionIn1.cursor()
    cursorIn1.execute("drop table modset")
    cursorIn1.execute("create table modset (mod char(20), mod_et char(20))")
    cursorIn1.execute("ATTACH DATABASE '" + fullPath + "' AS RF2Full")
    mstring = ', '.join(map(str, seedModules))
    # with input seedModules converted to mstring, look up over modules and rcIds for seedModules and seedTime
    queryString = "select distinct referencedComponentId as mod, string2 as mod_et from RF2Full.ssrefset where \
                    moduleId in (" + mstring + ") and ssrefset.string1 = (select max(ss2.string1) from ssrefset ss2 where \
                    ss2.id = ssrefset.id and ss2.string1 <= ?) \
                    union \
                    select distinct ssrefset.moduleId as mod, string1 as mod_et from ssrefset where \
                    moduleId in (" + mstring + ") and ssrefset.string1 = (select max(ss2.string1) from ssrefset ss2 where \
                    ss2.id = ssrefset.id and ss2.string1 <= ?)"
    cursorIn2.execute(queryString, (seedTime, seedTime))
    recordsetIn2 = cursorIn2.fetchall()
    preModETWrite(seedTime, seedModules, recordsetIn2)
    for row in recordsetIn2:
        cursorIn1.execute('insert into modset values (?,?)', row)
    connectionIn1.commit()
    # second pass to find 'latest' module+ET pairs - data errors
    cursorIn1.execute('select mod, mod_et from modset where modset.mod_et = \
                    (select max(m2.mod_et) from modset m2 where m2.mod = modset.mod and m2.mod_et <= ?)', (seedTime,))
    recordsetIn1 = cursorIn1.fetchall()
    returnSet = recordsetIn1
    cursorIn1.close()
    cursorIn2.close()
    connectionIn1.close()
    modETWrite(seedTime, seedModules, returnSet)
    return returnSet


def preModETWrite(seedTime, seedModules, recSet):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'preModET_sv.txt', 'w', encoding="UTF-8")
        f.write(str(seedTime) + '\n\n')
        for row in seedModules:
            f.write(str(row) + '\n')
        f.write('\n')
        for row in recSet:
            f.write(str(row) + '\n')
    finally:
        f.close()


def modETWrite(seedTime, seedModules, recSet):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'modET_sv.txt', 'w', encoding="UTF-8")
        f.write(str(seedTime) + '\n\n')
        for row in seedModules:
            f.write(str(row) + '\n')
        f.write('\n')
        for row in recSet:
            f.write(str(row) + '\n')
    finally:
        f.close()


def SV_SIMterms(inId, activeStatus, effectiveTime, refSets):
    connectionIn2 = sqlite3.connect(fullPath)
    connectionIn3 = sqlite3.connect(fullPath)
    cursorIn3 = connectionIn3.cursor()
    cursorIn2 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    descriptionsSet = []
    outputSet = []
    tempTermRefSet = []
    tempTerm = []
    tempRef = []
    termcounter = 1
    # descriptions

    if activeStatus == 1:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=1 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    elif activeStatus == 2:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    elif activeStatus == 3:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=?;"
    else:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=0 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    if activeStatus == 3:
        cursorIn2.execute(queryString2, (inId,))
    else:
        cursorIn2.execute(queryString2, (inId, effectiveTime,))
    recordsetIn2 = cursorIn2.fetchall()
    if len(recordsetIn2) == 0:
        return []
    else:
        for term in recordsetIn2:
            descriptionsSet.append(term)
        # definitions
        if activeStatus == 1:
            queryString2 = "select definitions.* from definitions where definitions.conceptId=? and definitions.active=1 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select definitions.* from definitions where definitions.conceptId=? and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 3:
            queryString2 = "select definitions.* from definitions where definitions.conceptId=?;"
        else:
            queryString2 = "select definitions.* from definitions where definitions.conceptId=? and definitions.active=0 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        if activeStatus == 3:
            cursorIn3.execute(queryString2, (inId,))
        else:
            cursorIn3.execute(queryString2, (inId, effectiveTime,))
        recordsetIn2 = cursorIn3.fetchall()
        for term in recordsetIn2:
            descriptionsSet.append(term)
        descriptionsSet.sort(key=lambda x: (x[1], x[0]))
        for term in descriptionsSet:
            matchBool = False
            for row in outputSet:
                if term[0] == row[1][0][0]:
                    row[1].append(term)
                    matchBool = True
            if not matchBool:
                tempTerm.append(term)
                if activeStatus == 1:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=1 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                elif activeStatus == 2:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                elif activeStatus == 3:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=?;"
                else:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                cursorIn1 = connectionIn3.cursor()
                if activeStatus == 3:
                    cursorIn1.execute(queryString1, (term[0],))
                else:
                    cursorIn1.execute(queryString1, (term[0], effectiveTime,))
                recordsetIn1 = cursorIn1.fetchall()
                recordsetIn1.sort(key=lambda x: (x[1], x[4]))
                for refSetRef in recordsetIn1:
                    tempRef.append(refSetRef)
                tempTermRefSet.append(termcounter)
                tempTermRefSet.append(tempTerm[:])
                tempTermRefSet.append(tempRef[:])
                termcounter += 1
                outputSet.append(tempTermRefSet[:])
                del tempTermRefSet[:]
                del tempTerm[:]
                del tempRef[:]
        cursorIn1.close()
        cursorIn2.close()
        connectionIn2.close()
        cursorIn3.close()
        connectionIn3.close()
        return outputSet


def SV_SIMpTerm(inId, activeStatus, effectiveTime, refSets, dtype):  # ? activeStatus always 1 because effective PT has to be active
    connectionIn2 = sqlite3.connect(fullPath)
    connectionIn1 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    returnSet = []
    if activeStatus == 1:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=1 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    elif activeStatus == 2:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    elif activeStatus == 3:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=?;"
    else:
        queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=0 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    if activeStatus == 3:
        cursorIn2.execute(queryString2, (inId,))
    else:
        cursorIn2.execute(queryString2, (inId, effectiveTime,))
    recordsetIn2 = cursorIn2.fetchall()
    for term in recordsetIn2:
        if activeStatus == 1:
            queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=1 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
        elif activeStatus == 3:
            queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=?;"
        else:
            queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
        cursorIn1 = connectionIn1.cursor()
        if activeStatus == 3:
            cursorIn1.execute(queryString1, (term[0],))
        else:
            cursorIn1.execute(queryString1, (term[0], effectiveTime,))
        recordsetIn1 = cursorIn1.fetchall()
        for row in recordsetIn1:
            if term[6] == dtype and row[6] == tok('PREFTERM') and row[4] == tok('GB_ENG'): #en-GB international.
                returnSet.append((3, term[7]))
            elif term[6] == dtype and row[6] == tok('PREFTERM') and row[4] == tok('RLR_CLIN'): #RDS clinical part
                returnSet.append((1, term[7]))
            elif term[6] == dtype and row[6] == tok('PREFTERM') and row[4] == tok('RLR_PHARM'): #RDS pharmacy part
                returnSet.append((1, term[7]))
            elif term[6] == dtype and row[6] == tok('PREFTERM') and row[4] == tok('UKEN-GB_CLIN'): #enGB UK clinical
                returnSet.append((2, term[7]))
            elif term[6] == dtype and row[6] == tok('PREFTERM') and row[4] == tok('UKEN-GB_PHARM'): #enGB UK pharmacy
                returnSet.append((2, term[7]))
            elif term[6] == dtype and row[6] == tok('PREFTERM') and row[4] == tok('US_ENG'): #en-US international
                returnSet.append((4, term[7]))
        returnSet.sort(key=lambda x: x[0])
        cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()
    if len(returnSet) > 0:
        return returnSet[0][1]
    else:
        return pterm(inId) + " *"


def SV_MDRterms(inId, activeStatus, effTimeSet, refSets):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    descriptionsSet = []
    outputSet = []
    tempTermRefSet = []
    tempTerm = []
    tempRef = []
    termcounter = 1
    # descriptions
    for row in effTimeSet:
        if activeStatus == 1:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.active=1 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        else:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.active=0 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        cursorIn2.execute(queryString2, (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        for term in recordsetIn2:
            descriptionsSet.append(term)
    # definitions
    for row in effTimeSet:
        if activeStatus == 1:
            queryString2 = "select definitions.* from definitions where definitions.moduleId = ? and definitions.conceptId=? and definitions.active=1 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select definitions.* from definitions where definitions.moduleId = ? and definitions.conceptId=? and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        else:
            queryString2 = "select definitions.* from definitions where definitions.moduleId = ? and definitions.conceptId=? and definitions.active=0 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        cursorIn2.execute(queryString2, (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        for term in recordsetIn2:
            descriptionsSet.append(term)
    descriptionsSet.sort(key=lambda x: (x[1], x[0]))
    for term in descriptionsSet:
        matchBool = False
        for row in outputSet:
            if term[0] == row[1][0][0]:
                row[1].append(term)
                matchBool = True
        if not matchBool:
            tempTerm.append(term)
            for row in effTimeSet:
                if activeStatus == 1:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and crefset.moduleId = ? and referencedComponentId=? and crefset.active=1 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                elif activeStatus == 2:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and crefset.moduleId = ? and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                else:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and crefset.moduleId = ? and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                cursorIn1.execute(queryString1, (row[0], term[0], row[1]))
                recordsetIn1 = cursorIn1.fetchall()
                recordsetIn1.sort(key=lambda x: (x[1], x[4]))
                for refSetRef in recordsetIn1:
                    tempRef.append(refSetRef)
            tempTermRefSet.append(termcounter)
            tempTermRefSet.append(tempTerm[:])
            tempTermRefSet.append(tempRef[:])
            termcounter += 1
            outputSet.append(tempTermRefSet[:])
            del tempTermRefSet[:]
            del tempTerm[:]
            del tempRef[:]
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()
    return outputSet


def SV_MDRpTerm(inId, activeStatus, effTimeSet, refSets, dtype): # ? activeStatus always 1 because effective PT has to be active
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    descriptionsSet = []
    outputSet = []
    tempTermRefSet = []
    tempTerm = []
    tempRef = []
    termcounter = 1
    returnSet = []
    for row in effTimeSet:
        if activeStatus == 1:
            queryString2 = "select descriptions.* from descriptions where descriptions.active=1 and descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        else:
            queryString2 = "select descriptions.* from descriptions where descriptions.active=1 and descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        cursorIn2.execute(queryString2, (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        for term in recordsetIn2:
            descriptionsSet.append(term)
    for term in descriptionsSet:
        tempTerm.append(term)
        for row in effTimeSet:
            if activeStatus == 1:
                queryString1 = "select crefset.* from crefset where crefset.active=1 and crefset.moduleId = ? and refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            elif activeStatus == 2:
                queryString1 = "select crefset.* from crefset where crefset.moduleId = ? and refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            else:
                queryString1 = "select crefset.* from crefset where crefset.active=1 and crefset.moduleId = ? and refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            cursorIn1.execute(queryString1, (row[0], term[0], row[1]))
            recordsetIn1 = cursorIn1.fetchall()
            for refSetRef in recordsetIn1:
                tempRef.append(refSetRef)
        tempTermRefSet.append(termcounter)
        tempTermRefSet.append(tempTerm[:])
        tempTermRefSet.append(tempRef[:])
        termcounter += 1
        outputSet.append(tempTermRefSet[:])
        del tempTermRefSet[:]
        del tempTerm[:]
        del tempRef[:]
    for row in outputSet:
        for rsEntry in row[2]:
            if row[1][0][6] == dtype and rsEntry[6] == tok('PREFTERM') and rsEntry[4] == tok('GB_ENG'): #en-GB international.
                returnSet.append((3, row[1][0][7]))
            elif row[1][0][6] == dtype and rsEntry[6] == tok('PREFTERM') and rsEntry[4] == tok('RLR_CLIN'): #RDS clinical part
                returnSet.append((1, row[1][0][7]))
            elif row[1][0][6] == dtype and rsEntry[6] == tok('PREFTERM') and rsEntry[4] == tok('RLR_PHARM'): #RDS pharmacy part
                returnSet.append((1, row[1][0][7]))
            elif row[1][0][6] == dtype and rsEntry[6] == tok('PREFTERM') and rsEntry[4] == tok('UKEN-GB_CLIN'): #enGB UK clinical
                returnSet.append((2, row[1][0][7]))
            elif row[1][0][6] == dtype and rsEntry[6] == tok('PREFTERM') and rsEntry[4] == tok('UKEN-GB_PHARM'): #enGB UK pharmacy
                returnSet.append((2, row[1][0][7]))
            elif row[1][0][6] == dtype and rsEntry[6] == tok('PREFTERM') and rsEntry[4] == tok('US_ENG'): #en-US international
                returnSet.append((4, row[1][0][7]))
    returnSet.sort(key=lambda x: x[0])
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()
    if len(returnSet) > 0:
        return returnSet[0][1]
    else:
        return pterm(inId) + " *"

def SV_SIMancestorsFromConIdWithET_IN2(inId, inET):
    ET = str(inET)
    tempSet = ()
    ancestorList = []
    distinctActivePathNodes = []
    distinctActivePathNodes = neoQuery(inId, ET, 2, True)
    for node in distinctActivePathNodes:
        tempSet = (node, SV_pterm(str(node)))
        ancestorList.append(tempSet)
    ancestorList.sort(key=lambda x: x[1].lower())
    return ancestorList

def SV_SIMFindAncestorsForConIdAndET2(CON_ID, ET):
    tempSet = ()
    ancestorList = []
    distinctActivePathNodes = []
    distinctActivePathNodes = neoQuery(CON_ID, ET, 2, True)
    for node in distinctActivePathNodes:
        tempSet = (node, SV_pterm(str(node)))
        ancestorList.append(tempSet)
    ancestorList.sort(key=lambda x: x[1].lower())
    return ancestorList


def SV_SIMdescendantsFromConIdWithET_IN(inId, inET):
    ET = str(inET)
    tempSet = ()
    descendantList = []
    distinctActivePathNodes = []
    distinctActivePathNodes = neoQuery(inId, ET, 1, False)
    for node in distinctActivePathNodes:
        tempSet = (node, SV_pterm(str(node)))
        descendantList.append(tempSet)
    descendantList.sort(key=lambda x: x[1].lower())
    return descendantList

def SV_SIMancestorsFromConId_IN2(inId):
    ET = str(cfg("EFFECTIVE_TIME"))
    tempSet = ()
    ancestorList = []
    distinctActivePathNodes = []
    distinctActivePathNodes = neoQuery(inId, ET, 2, True)
    for node in distinctActivePathNodes:
        tempSet = (node, SV_pterm(str(node)))
        ancestorList.append(tempSet)
    ancestorList.sort(key=lambda x: x[1].lower())
    return ancestorList

def SV_SIMdescendantsFromConId_IN2(inId, helperDisplayBool=True):
    ET = str(cfg("EFFECTIVE_TIME"))
    tempSet = ()
    descendantList = []
    distinctActivePathNodes = []
    distinctActivePathNodes = neoQuery(inId, ET, 1, helperDisplayBool)
    for node in distinctActivePathNodes:
        tempSet = (node, SV_pterm(str(node)))
        descendantList.append(tempSet)
    descendantList.sort(key=lambda x: x[1].lower())
    return descendantList

def neoQuery(CON_ID, ET, queryType, helperDisplayBool=False, CON_ID2=tok("ROOT")):  # queryType 1=desc, 2=ances, 3 = compare
    uri = "bolt://localhost:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neo"), encrypted=False)
    resultNodes = []
    resultBool = False
    with driver.session() as session:
        if queryType == 1:
            # single logical arbitrary path written to text file.
            cypherString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con)-[*]->(sup:con {cid:'" + CON_ID + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path\n(NOTE: actually run as series of shorter steps - see cypher_sv_long.txt)", 'cypher_sv.txt', 'w')
            # single jump path [*1] and padded target id (for substitution) passed to loop below
            # operational string to loop through members of list in code
            # cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con)-[*1..2]->(sup:con {cid:'$$$$$'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
            # operational string with all members of list in IN clause
            cypherString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con)-[*1..2]->(sup:con) WHERE sup.cid in ['$$$$$'] AND  ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path", 'cypher_sv_long.txt', 'w')
        elif queryType == 2:
            cypherString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + CON_ID2 + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path", 'cypher_sv.txt', 'w')
        else:
            cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + CON_ID2 + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
        # closed arbitrary lenght paths (known source & target) efficient enough
        if queryType == 3:
            resultBool = session.read_transaction(neoAnyPath, cypherString)
            return resultBool
        elif queryType == 2:
            resultNodes = session.read_transaction(neoNodesFromPath, cypherString)
            return resultNodes
        else:
            # gradual build up of one or two hops for open-ended arbitrary paths.
            testNodes = []
            testNodes.append(CON_ID)
            resultNodesTotal = []
            foundOne = True
            printBool = True
            # unclear which is better
            codeChoice = random.choice([True, False])
            if CON_ID == tok('HISTORICAL_ASSOCIATION'): # hides progress display for simple retrieval of historical refset list
                printBool = False
            if printBool and helperDisplayBool:
                if codeChoice:
                    print("Using sets") # print
                else:
                    print("Using loop") # print
                print('Depth\tTotal\tCheck') # print
            depthCounter = 2
            while foundOne:
                foundOne = False
                resultNodes = []
                passOnNodes = []
                # call with list
                testNodeString = "', '".join(map(str, testNodes))
                testString = cypherWrite("\n\n" + cypherString.replace("$$$$$", testNodeString), 'cypher_sv_long.txt', 'a')
                resultNodes = session.read_transaction(neoNodesFromPath, testString)
                # call with loop (indent if codeChoce...)
                # for testNode in testNodes:
                #     resultNodes = session.read_transaction(neoNodesFromPath, cypherString.replace("$$$$$", testNode))
                if codeChoice:
                    passOnNodes += list(set(resultNodes) - set(resultNodesTotal))
                    if passOnNodes != []:
                        resultNodesTotal = list(set(passOnNodes) | set(resultNodesTotal))
                        foundOne = True
                else:
                    for resultNode in resultNodes:
                        if resultNode not in resultNodesTotal:
                            foundOne = True
                            resultNodesTotal.append(resultNode)
                            passOnNodes.append(resultNode)
                if printBool and helperDisplayBool:
                    print(str(depthCounter) + '\t' + str(len(resultNodesTotal)) + '\t' + str(len(passOnNodes))) # print
                testNodes = passOnNodes
                depthCounter += 2
            if helperDisplayBool:
                clearScreen() # print
            return resultNodesTotal

def SV_SIMTCCompare_IN(subId, supId):
    supertypeSet = []
    supertypeSet = SV_SIMancestorsFromConId_IN2(subId)
    checkBool = False
    for supertype in supertypeSet:
        if supertype[0] == supId:
            checkBool = True
    return checkBool

def SV_SIMPPSFromAncestors_IN(ancestorList):
    PrimList = []
    PPSDict = {}
    PPSList = []
    for ancestor in ancestorList:
        if SV_SIMconMetaFromConIdSimple_IN(ancestor[0], str(cfg("EFFECTIVE_TIME")), False)[0][3] == tok("PRIMITIVE"):
            PrimList.append(ancestor)
    for prim in PrimList:
        PPSDict.update(list(zip(listify(prim[0]), listify('1')))) #set all as PPS
    for prim1 in PrimList:
        for prim2 in PrimList:
            if SV_SIMTCCompare_IN(prim1[0], prim2[0]) and not(prim1 == prim2):
                PPSDict[prim2[0]] = '0' #unset if any shadow test positive
    for prim in PrimList:
        if PPSDict[prim[0]] == '1': #return those unset
            PPSList.append(prim)
    return PPSList


def checkEarliestReleaseSimpleRefset(inId, rsType):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select min(effectiveTime) from " + rsType + " where refsetId=?", (inId,))
    min_ETSet = cursorIn2.fetchone()
    cursorIn2.close()
    connectionIn2.close()
    return min_ETSet


def FindAllMembersOfRefset(CON_ID, rsType):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    memberList = []
    cursorIn2.execute("select distinct referencedComponentId from " + rsType + " where refsetId=?", (CON_ID,))
    recordsetIn2 = cursorIn2.fetchall()
    for member in recordsetIn2:
        tempSet = (member, refsetPterm(member))
        memberList.append(tempSet)
    cursorIn2.close()
    connectionIn2.close()
    return memberList

def SV_SIMCheckConceptAgainstRefset(refsetId, memberId, rsType, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select referencedComponentId, active from " + rsType + " where refsetId=? and referencedComponentId= ? and " + rsType + ".effectiveTime = (select max(sr2.effectiveTime) from " + rsType + " sr2 where sr2.id = " + rsType + ".id and sr2.effectiveTime <= ? )", (refsetId, memberId, ET))
    recordsetIn2 = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    return recordsetIn2


def SV_SIMcheckIsARefset(SUB_ID, helperDisplayBool=False):
    # seed ET
    if helperDisplayBool:
        clearScreen()  # print
    ET = "20020131"
    SUP_ID = tok('REFSET')
    if helperDisplayBool:
        print('Checking ' + SUB_ID, pterm(SUB_ID) + ' as a subtype of ' + SUP_ID, pterm(SUP_ID) + '...\n')  # print
    while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
        if ET[-3] == "1" and helperDisplayBool:
            print(ET[:4]) # print
        pathBool = False
        pathBool = neoQuery(SUB_ID, ET, 3, True, SUP_ID)
        if pathBool and helperDisplayBool:
            print("yes, it's a refset!'") # print
            return True
        ET = upByNMonths(ET, 6)
    if helperDisplayBool:
        print("no, it's never been a refset!'") # print
    return False


def SV_SIMcheckRefsetMemberCount(RID, rsType, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select count(referencedComponentId) from " + rsType + " where active=1 and refsetId=? and " + rsType + ".effectiveTime = (select max(sr2.effectiveTime) from " + rsType + " sr2 where sr2.id = " + rsType + ".id and sr2.effectiveTime <= ? )", (RID, ET))
    mydata = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    return mydata[0][0]

def SV_SIMC1_IsA_C2_Compare(C1, C2, testCon, ET, inDirection, showBool, helperDisplayBool=False):
    pathBool = False
    pathBool = neoQuery(C1, ET, 3, helperDisplayBool, C2)
    if ET[-3] == "1" and showBool and helperDisplayBool:
        print(ET[:4]) # print
    if pathBool:
        returnString = ""
        if inDirection == "desc":
            checkString = SV_SIMconMetaFromConIdSimple_IN(C1, ET, False)[0][3]
        else:
            checkString = SV_SIMconMetaFromConIdSimple_IN(C2, ET, False)[0][3]
        parentIdList = [item[3] for item in SV_SIMparentsFromIdPlusMeta_IN([C1], cfg("SHOW_TYPE"), ET)]
        if checkString == tok("PRIMITIVE"):
            if C2 in parentIdList:
                returnString = "="
            else:
                returnString = "+"
        else:
            if C2 in parentIdList:
                returnString = "~"
            else:
                returnString = "#"
        return returnString
    else:
        if testCon == 1:  # if it's C1 which might have moved, changed status or not been there at all...'
            if len(SV_SIMconMetaFromConIdSimple_IN(C1, ET, False)) == 1:
                if SV_SIMconMetaFromConIdSimple_IN(C1, ET, False)[0][1] == 1:
                    return '.'
                else:
                    return '--'
            else:
                return ' '
        else:  # or C2...
            if len(SV_SIMconMetaFromConIdSimple_IN(C2, ET, False)) == 1:
                if SV_SIMconMetaFromConIdSimple_IN(C2, ET, False)[0][1] == 1:
                    return '.'
                else:
                    return '--'
            else:
                return ' '


def SV_SIMrefSetFromConIdPlusMeta_IN(idIn, refsetType, activeFlag, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    if activeFlag == 1:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=1 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(idIn), ET))
    elif activeFlag == 2:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(idIn), ET))
    elif activeFlag == 3:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where referencedComponentId= ?", (str(idIn),))
    else:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=0 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(idIn), ET))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = (refSet[1], SV_pterm(refSet[1]), refSet[0], pterm(refSet[0]), refSet[2], refSet[3])
        refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def SV_MDRrefSetFromConIdPlusMeta_IN(idIn, refsetType, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    refSetList = []
    for row in effTimeSet:
        if activeFlag == 0:
            cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=0 and moduleId = ? and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], str(idIn), row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where and moduleId = ? referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], str(idIn), row[1]))
        else:
            cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=1 and moduleId = ? and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], str(idIn), row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for refSet in recordsetIn2:
            tempSet = (refSet[1], SV_pterm(refSet[1]), refSet[0], pterm(refSet[0]), refSet[2], refSet[3])
            refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def SV_pterm(inId):
    # global RDSParts
    # global otherLangSet
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    if (cfg("PTERM_TYPE") == 0) or (cfg("QUERY_TYPE") == 1):  # if instructed to be latest snapshot, or if looking for all data
        return pterm(inId)
    else:
        if cfg("QUERY_TYPE") == 2:  # if simple snapshot
            return SV_SIMpTerm(inId[0], 1, cfg('EFFECTIVE_TIME'), ((RLRParts + otherLangSet)), tok('SYNONYM'))
        else:  # if MDR snapshot
            return SV_MDRpTerm(inId[0], 1, effTimeSet(), ((RLRParts + otherLangSet)), tok('SYNONYM'))

def SV_fsn(inId):
    # global RDSParts
    # global otherLangSet
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    if (cfg("PTERM_TYPE") == 0) or (cfg("QUERY_TYPE") == 1):  # if instructed to be latest snapshot, or if looking for all data
        return fsnFromConId_IN(inId[0])[1]
    else:
        if cfg("QUERY_TYPE") == 2:  # if simple snapshot
            return SV_SIMpTerm(inId[0], 1, cfg('EFFECTIVE_TIME'), ((RLRParts + otherLangSet)), tok('FSN'))
        else:  # if MDR snapshot
            return SV_MDRpTerm(inId[0], 1, effTimeSet(), ((RLRParts + otherLangSet)), tok('FSN'))


def findSeedModules(seedTime):
    moduleList = []
    connectionIn1 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn1.cursor()
    queryString = "select distinct moduleId from ssrefset where ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.effectiveTime <= ?);"
    cursorIn1.execute(queryString, (seedTime,))
    recordsetIn1 = cursorIn1.fetchall()
    for row in recordsetIn1:
        moduleList.append(str(row[0]))
    return moduleList


def cypherWrite(inString, inFileName, appendOrWrite):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + inFileName, appendOrWrite, encoding="UTF-8")
        f.write(inString)
    finally:
        f.close()
    return inString


## sct helpers

def pterm(idIn):
    if not isinstance(idIn, tuple):
        idIn = tuplify(idIn)
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    cursorIn2.execute("select term from prefterm where conId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchone()
    if not recordsetIn2 is None:
        cursorIn2.close()
        connectionIn2.close()
        return recordsetIn2[0]
    else:
        return "No term found"

def downByNMonths(ET, n):
    effectiveTimeTemp = ET
    effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
    sixMonthIncrement = relativedelta(months=n)
    effTimeObj -= sixMonthIncrement
    return effTimeObj.strftime("%Y%m%d")

def upByNMonths(ET, n):
    effectiveTimeTemp = ET
    effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
    sixMonthIncrement = relativedelta(months=n)
    if effTimeObj.strftime("%Y%m%d") == time.strftime("%Y%m%d"):
        effTimeObj += sixMonthIncrement
        return effTimeObj.strftime("%Y%m%d")
    else:
        effTimeObj += sixMonthIncrement
        if effTimeObj.strftime("%Y%m%d") > time.strftime("%Y%m%d"):
            return time.strftime("%Y%m%d")
        else:
            return effTimeObj.strftime("%Y%m%d")

def progressTrack(startEnd, character, coverString):
    # calls on to platformSpecific - not *really* needed but code invokes sys module
    # progressTrackCmd(startEnd, character, coverString)
    return progressTrackCmd(startEnd, character, coverString)
    # return coverString


def startNeoPlatformCall():
    startNeoPlatform(neoPath)


def stopNeoPlatformCall():
    stopNeoPlatform(neoPath)


def SV_SIMrefSetMemberCountFromConId_IN(inId, refsetType, activeFlag, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    if activeFlag == 1:
        cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where active=1  and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (inId, ET))
    elif activeFlag == 2:
        cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (inId, ET))
    elif activeFlag == 3:
        cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where refsetId=?", (inId,))
    else:
        cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where active=0  and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (inId, ET))
    recordsetIn2 = cursorIn2.fetchone()
    for row in recordsetIn2:
        refsetCount = row
    cursorIn2.close()
    connectionIn2.close()
    return refsetCount

def SV_MDRrefSetMembersFromConIdPlusMeta_IN(inId, refsetType, myfilter, mycount, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    # not ET dependent
    tempstring = findRefsetMetaFields(refsetType)
    if myfilter == 1:  # 1/10
        filterString = " limit " + str(int(round(mycount / 10)))
    elif myfilter == 2:  # 1/100
        filterString = " limit " + str(int(round(mycount / 100)))
    elif myfilter == 3:  # 1/10000
        filterString = " limit " + str(int(round(mycount / 2000)))
    else:
        filterString = ""
    refSetList = []
    coverString = ""
    for row in effTimeSet:
        coverString = progressTrack('START', "*", coverString)
        if activeFlag == 0:
            cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=0 and moduleId = ? and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )" + filterString, (row[0], inId, row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where and moduleId = ? refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )" + filterString, (row[0], inId, row[1]))
        else:
            cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=1 and moduleId = ? and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )"  + filterString, (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for refsetRow in recordsetIn2:
            tempSet = refsetRecordsetMetaDecomp(refsetRow, refsetType)
            tempSet2 = [item for item in tempSet]
            tempSet2.append(refsetRow[-1])
            refSetList.append(tempSet2)
    coverString = progressTrack('END', "", coverString)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

def SV_MDRrefSetMemberCountFromConId_IN(inId, refsetType, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    coverString = ""
    refsetCount = 0
    for row in effTimeSet:
        coverString = progressTrack('START', "*", coverString)
        if activeFlag == 0:
            cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where active=0 and moduleId = ? and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], inId, row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where moduleId = ? and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], inId, row[1]))
        else:
            cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where active=1 and moduleId = ? and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchone()
        for row2 in recordsetIn2:
            preRefsetCount = row2
        refsetCount += preRefsetCount
    coverString = progressTrack('END', "", coverString)
    cursorIn2.close()
    connectionIn2.close()
    return refsetCount

def SV_SIMdescActiveFromDescID_IN(inId, effectiveTime):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()
    queryString2 = "select descriptions.active from descriptions where descriptions.id=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    cursorIn1.execute(queryString2, (inId, effectiveTime,))
    recordsetIn2 = cursorIn1.fetchall()
    if len(recordsetIn2) > 0:
        cursorIn1.close()
        connectionIn2.close()
        return recordsetIn2[0][0]
    else: # then try definitions
        queryString2 = "select definitions.active from definitions where definitions.id=? and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        cursorIn1.execute(queryString2, (inId, effectiveTime,))
        recordsetIn2 = cursorIn1.fetchall()
        if len(recordsetIn2) > 0:
            cursorIn1.close()
            connectionIn2.close()
            return recordsetIn2[0][0]
        else:
            return 0 # if descId can't be found in descriptions or definitions, return it as inactive.

def SV_SIMactiveColor_IN(inId, ET, focusOrAncBool=False):
    if inId[-2] == "1": #if referenced component is a description/definition
        descActive = SV_SIMdescActiveFromDescID_IN(inId, ET)
        if descActive == 1:
            return inId
        else:
            return '\033[31m' + inId + '\033[92m'
    else:
        conMetaList = SV_SIMconMetaFromConIdSimple_IN(inId, ET, False)
        if len(conMetaList) > 0:
            if conMetaList[0][1] == 1:
                if focusOrAncBool and conMetaList[0][3] == tok("DEFINED"):
                    return '\033[1m' + inId + '\033[22m'
                elif focusOrAncBool and conMetaList[0][3] == tok("PRIMITIVE"):
                    return '\033[2m' + inId + '\033[22m'
                else:
                    return inId
            else:
                return '\033[31m' + inId + '\033[92m'
        else:
            return inId

def SV_MDRactiveColor_IN(inId, effTimeSet):
    # Code exists, but paralysingly slow in action, so method not currently used.
    if inId[-2] == "1": #if referenced component is a description/definition
        # TODA:
        return inId
    else:
        conMetaList = SV_MDRconMetaFromConId_IN(inId, effTimeSet)
        if len(conMetaList) > 0:
            if conMetaList[0][1] == 1:
                return inId
            else:
                return '\033[31m' + inId + '\033[92m'
        else:
            return inId

def SV_SIMHistoricalRefsetRowsfromId(inId, refsetType, activeFlag, inLookup, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    listofHistoricalAssRefSets = SV_SIMdescendantsFromConId_IN2(tok('HISTORICAL_ASSOCIATION'), False)
    listofHistoricalAssRefSetIds = [i[0] for i in listofHistoricalAssRefSets]
    HARString = ', '.join(map(str, listofHistoricalAssRefSetIds))
    if inLookup == 1:
        lookupString = 'referencedComponentId'
    else:
        lookupString = 'componentId1'
    if activeFlag == 1:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1, referencedComponentId from " + refsetType + " where refsetId IN (" + HARString + ") AND active=1 and " + lookupString + " = ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    elif activeFlag == 2:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1, referencedComponentId from " + refsetType + " where refsetId IN (" + HARString + ") AND " + lookupString + " = ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    elif activeFlag == 3:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1, referencedComponentId from " + refsetType + " where refsetId IN (" + HARString + ") AND " + lookupString + " = ?", (str(inId),))
    else:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1, referencedComponentId from " + refsetType + " where refsetId IN (" + HARString + ") AND active=0 and " + lookupString + " = ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = (refSet[0], pterm(refSet[0]), refSet[1], refSet[2], refSet[3], pterm(refSet[3]), refSet[4], pterm(refSet[4]))
        refSetList.append(tempSet)
        if tempSet[0] == tok("MOVED_TO"):
            MovedFromList = SV_SIMHistoricalRefsetRowsfromId(tempSet[6], refsetType, activeFlag, 0, ET)
            for movedFromRow in MovedFromList:
                if movedFromRow[0] == tok("MOVED_FROM"):
                    tempTuple = (movedFromRow[0], movedFromRow[1], movedFromRow[2], movedFromRow[3], movedFromRow[6], movedFromRow[7], movedFromRow[4], movedFromRow[5])
                    refSetList.append(tempTuple)
    refersToList = SV_SIMrefersToRowsFromConId_IN(inId, ET)
    for refersToRow in refersToList:
        refSetList.append(refersToRow)
    refSetList.sort(key=lambda x: x[2])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

def SV_SIMrefersToRowsFromConId_IN(inId, ET):
    descriptionRows = SV_SIMterms(inId, 3, ET, [tok("REFERS_TO")])
    descriptionRows = [item for item in descriptionRows if len(item[2]) > 0]
    returnRows = []
    for item in descriptionRows:
        rowTemp = (item[2][0][4], pterm(item[2][0][4]), item[2][0][1], item[2][0][2], item[2][0][6], pterm(item[2][0][6]), inId, pterm(inId))
        if (rowTemp not in returnRows) and (int(item[2][0][1]) <= int(ET)):
            returnRows.append(rowTemp)
    return returnRows

def SV_SIMrefSetDetailsFromMemIdandSetIdPlusMeta_IN(setId, memberId, activeFlag, ET):
    refsetType = refSetTypeFromConId_IN(setId)[0][0]
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    tempstring = findRefsetMetaFields(refsetType)
    argumentList = []
    argumentList.append(memberId)
    argumentList.append(setId)
    argumentList.append(ET)
    if activeFlag == 1:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=1 and referencedComponentId = ? and refsetId = ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", argumentList)
    elif activeFlag == 2:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where referencedComponentId = ? and refsetId = ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", argumentList)
    elif activeFlag == 3:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where referencedComponentId = ? and refsetId = ?", (memberId, setId, ))
    else:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=0 and referencedComponentId = ? and refsetId = ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", argumentList)

    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSetRow in recordsetIn2:
        tempSet = refSetRow
        refSetList.append(tempSet)
    if refsetType[0:3] == 'cci':
        refSetList.sort(key=lambda x: x[3])  # orders descriptor block
    elif refsetType[0:7] == 'iissscc':
        refSetList.sort(key=lambda x: (x[1], x[2]))  # orders classification by group/priority
    elif refsetType[0:7] == 'iisssci':
        refSetList.sort(key=lambda x: (x[7], x[1], x[2]))  # orders classification by group/priority
    else:
        refSetList.sort(key=lambda x: x[0])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def SV_SIMrefSetMembersFromConIdPlusMeta_IN(inId, refsetType, myfilter, mycount, activeFlag, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    # not ET dependent
    tempstring = findRefsetMetaFields(refsetType)
    if myfilter == 1:  # 1/10
        filterString = " limit " + str(int(round(mycount / 10)))
    elif myfilter == 2:  # 1/100
        filterString = " limit " + str(int(round(mycount / 100)))
    elif myfilter == 3:  # 1/10000
        filterString = " limit " + str(int(round(mycount / 2000)))
    else:
        filterString = ""
    if activeFlag == 1:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=1 and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )" + filterString, (inId, ET))
    elif activeFlag == 2:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )" + filterString, (inId, ET))
    elif activeFlag == 3:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where refsetId=?"  + filterString, (inId,))
    else:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=0 and refsetId=? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )"  + filterString, (inId, ET))

    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refsetRow in recordsetIn2:
        tempSet = refsetRecordsetMetaDecomp(refsetRow, refsetType)
        tempSet2 = [item for item in tempSet]
        tempSet2.append(refsetRow[-1])
        refSetList.append(tempSet2)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

def neoNodesFromPath(tx, inString):
    myNodes = []
    myNodeSet = []
    for record in tx.run(inString):
        for node in record["path"].nodes:
            myNodes.append(list(node.items())[0][1])
    myNodeSet = set(myNodes)
    return myNodeSet

def neoAnyPath(tx, inString):
    internalBool = False
    for record in tx.run(inString):
        if record["path"].nodes:
            internalBool = True
            break
    return internalBool
