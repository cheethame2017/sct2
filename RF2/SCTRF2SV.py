#!/usr/bin/env python
from __future__ import print_function, unicode_literals
from SCTRF2SVFuncs import startNeoCall, stopNeoCall, snapshotHeader, focusWrite, browseFromId, ancestorsFromConId, \
                            listify, descendantsFromConId, ALLancestorsToXLSX, ALLdescendantsToXLSX, \
                            breadcrumbsWrite, ALLmembersToXLSX, showRefsetRowsFromConAndRefset, showRefsetRowsForRefset, \
                            breadcrumbsRead, setEffectiveTime, readInFromFocus, readInFromTerminal, setQueryType, \
                            setShowType, setShowMetadata, setD3Extras, setPretty, setPtermControl, setSubShow, \
                            ALLancestorsToD3, dotDrawAncestorsCall, allTermsFromConIdPlusMeta, groupedRolesFromConIdPlusMeta, \
                            readInFromWindow, refSetFromConIdPlusMeta, ALLdescendantsToD3, dotDrawDescendantsCall, dictFromId, \
                            readFocusValue, ALLfocusToD3, dotDrawFocusCall, promptString, showSupers, graphAngle, setAcrossDown, \
                            ALLcodesInSetToXLSX
from SCTRF2SVconfig import tok, cfg
from SCTRF2Help import selectHelpFile, helpIndexDict
from platformSpecific import clearScreen, headerWriterFull, input2_3

def startNeo():
    startNeoCall()


def stopNeo():
    stopNeoCall()


def browseFromRootCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in browseFromRootCall')
    snapshotHeader()
    callId = (tok('ROOT'),)
    focusWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def browseFromIndexCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in browseFromIndexCall')
    snapshotHeader()
    callId = inString
    if isinstance(callId, str):
        callId = listify(callId.strip())
    breadcrumbsWrite(callId[0])
    focusWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString


def showAncestorsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inancestorsCall')
    snapshotHeader()
    callId = inString
    returnString = ancestorsFromConId(callId)
    if debug:
        print(returnString)
    return returnString


def showDescendantsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> indescendantsCall')
    snapshotHeader()
    callId = inString
    returnString = descendantsFromConId(callId)
    if debug:
        print(returnString)
    return returnString


def ancestorsToXLSXCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inancestorsToXLSX')
    snapshotHeader()
    callId = inString
    returnString = ALLancestorsToXLSX(callId)
    if debug:
        print(returnString)
    return returnString


def descendantsToXLSXCall(inString, fullSweepBool, proceedBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> indescendantsToXLSX')
    snapshotHeader()
    callId = inString
    returnString = ALLdescendantsToXLSX(callId, cfg('REFSET'), fullSweepBool, proceedBool)
    if debug:
        print(returnString)
    return returnString

def membersToXLSXCall(inString, fullSweepBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> inmembersToXLSX')
    snapshotHeader()
    callId = inString
    returnString = ALLmembersToXLSX(callId, fullSweepBool)
    if debug:
        print(returnString)
    return returnString

def setMembersToXLSXCall(inString, fullSweepBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> inmembersToXLSX')
    snapshotHeader()
    ALLcodesInSetToXLSX(inString, fullSweepBool, False)
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def showRefsetRowsCall(memberId, refsetId):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRolesCall')
    snapshotHeader()
    returnString = showRefsetRowsFromConAndRefset(memberId[0], refsetId)
    if debug:
        print(returnString)
    return returnString

def refsetMembersCall(inString, forceMeta):
    if not debug:
        clearScreen()
    if debug:
        print('-> inRefsetMembers')
    snapshotHeader()
    if forceMeta:
        tempVal = cfg(helpIndexDict('SCTRF2SV')['k'])
        setShowMetadata(1)
    callId = inString
    returnString = showRefsetRowsForRefset(callId)
    if forceMeta:
        setShowMetadata(tempVal)
    if debug:
        print(returnString)
    return returnString

def showBreadcrumbs(inType, longBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in showBreadcrumbs')
    snapshotHeader()
    returnString = breadcrumbsRead(inType, longBool)
    if debug:
        print(returnString)
    return returnString


def inputDateThresholdCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> inputDateThresholdCall')
    returnString = setEffectiveTime()
    if not debug:
        clearScreen()
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def importFromTerminalCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> importFromTerminalCall')
    snapshotHeader()
    returnString = readInFromTerminal()
    if debug:
        print(returnString)
    return returnString

def selectHelpFileCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in selectHelpFileCall')
    snapshotHeader()
    selectHelpFile(inString, 'SCTRF2SV')
    callId = readFocusValue()
    returnString = dictFromId(callId)
    if debug:
        print(returnString)
    return returnString

def queryTypeCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> queryTypeCall', str(inInteger))
    returnString = setQueryType(inInteger)
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def showTypeCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showTypeCall', str(inInteger))
    returnString = setShowType(inInteger)
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString

def showSupersCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showSupers', str(inInteger))
    snapshotHeader()
    returnString = showSupers(inInteger)
    if debug:
        print(returnString)
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString

def showMetadataCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showTypeCall', str(inInteger))
    snapshotHeader()
    returnString = setShowMetadata(inInteger)
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def setD3ExtrasCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setD3ExtrasCall', str(inInteger))
    snapshotHeader()
    returnString = setD3Extras(inInteger)
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def setPrettyCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setPretty', str(inInteger))
    snapshotHeader()
    returnString = setPretty(inInteger)
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def ptermControlCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> ptermControlCall', str(inInteger))
    returnString = setPtermControl(inInteger)
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def subShowCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> subShowCall', str(inInteger))
    returnString = setSubShow(inInteger)
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString

def graphAngleCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> graphAngleCall', str(inInteger))
    returnString = graphAngle(inInteger)
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString

def drawAncestors(inString, inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> drawAncestors', str(inInteger))
    if inInteger == 1:
        ALLancestorsToD3(inString)
    else:
        dotDrawAncestorsCall(inString)
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def drawDescendants(inString, inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> drawDescendants', str(inInteger))
    if inInteger == 1:
        ALLdescendantsToD3(inString)
    else:
        dotDrawDescendantsCall(inString)
    snapshotHeader()
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString


def drawFocus(inString, inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> drawFocus', str(inInteger))
    snapshotHeader()
    if inInteger == 1:
        ALLfocusToD3(inString)
    else:
        dotDrawFocusCall(inString)
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString

def acrossDownCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> acrossDown', str(inInteger))
    snapshotHeader()
    setAcrossDown(inInteger)
    returnString = readInFromFocus()
    if debug:
        print(returnString)
    return returnString

def showTermsMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowTermsMetaCall')
    callId = inString
    snapshotHeader()
    returnString = allTermsFromConIdPlusMeta(callId)
    if debug:
        print(returnString)
    return returnString


def showRolesMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRolesMetaCall')
    callId = inString
    snapshotHeader()
    returnString = groupedRolesFromConIdPlusMeta(callId)
    if debug:
        print(returnString)
    return returnString


def firstTimeCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> firstTimeCall')
    print('Most recent snapshot:\n')
    snapshotHeader()
    returnString = readInFromTerminal()
    if debug:
        print(returnString)
    return returnString


def importFromWindowCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> importFromWindowCall')
    snapshotHeader()
    returnString = readInFromWindow()
    if debug:
        print(returnString)
    return returnString


def showRefsetsMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRefsetsMetaCall')
    callId = inString
    snapshotHeader()
    returnString = refSetFromConIdPlusMeta(callId)
    if debug:
        print(returnString)
    return returnString

def reloadFocusCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> reloadFocus')
    snapshotHeader()
    callId = readFocusValue()
    breadcrumbsWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

returnString = {}
debug = False
headerWriterFull()
clearScreen()
# main control code
firstTime = True
startNeo()
while True:
    if firstTime:
        topaction = "Z"
        firstTime = False
    else:
        topaction = input2_3(promptString()).strip()
    if topaction == "":
        topaction = "v"
    if topaction == 'zzz':
        if debug:
            print(returnString)
        setQueryType(2)
        clearScreen()
        stopNeo()
        break  # breaks out of the while loop and ends session
    elif topaction == 'zz':
        if debug:
            print(returnString)
        setQueryType(2)
        clearScreen()
        break  # breaks out of the while loop and ends session
    elif topaction == 'a':
        if debug:
            print(returnString)
            print('-> calling browseFromRootCall')
        returnString = browseFromRootCall()
    elif topaction == 'b':
        if debug:
            print(returnString)
            print('-> calling importFromTerminal')
        returnString = importFromTerminalCall()
    elif topaction == 'd':
        if debug:
            print(returnString)
            print('-> calling queryTypeCall - full')
        returnString = queryTypeCall(1)
    elif topaction == 'e':
        if debug:
            print(returnString)
            print('-> calling queryTypeCall - simple snapshot')
        returnString = queryTypeCall(2)
    elif topaction == 'f':
        if debug:
            print(returnString)
            print('-> calling queryTypeCall - MDR snapshot')
        returnString = queryTypeCall(3)
    elif topaction == 'g':
        if debug:
            print(returnString)
            print('-> calling showTypeCall - active')
        returnString = showTypeCall(1)
    elif topaction == 'h':
        if debug:
            print(returnString)
            print('-> calling showTypeCall - both')
        returnString = showTypeCall(2)
    elif topaction == 'i':
        if debug:
            print(returnString)
            print('-> calling showTypeCall - inactive')
        returnString = showTypeCall(0)
    elif topaction == 'l':
        if debug:
            print(returnString)
            print('-> calling showMetadataCall - hide')
        returnString = showMetadataCall(0)
    elif topaction == 'm':
        if debug:
            print(returnString)
            print('-> calling ptermControlCall - latest')
        returnString = ptermControlCall(0)
    elif topaction == 'n':
        if debug:
            print(returnString)
            print('-> calling ptermControlCall - calculated')
        returnString = ptermControlCall(1)
    elif topaction == 'o':
        if debug:
            print(returnString)
            print('-> calling subShowCall - show')
        returnString = subShowCall(1)
    elif topaction == 'p':
        if debug:
            print(returnString)
            print('-> calling subShowlCall - hide')
        returnString = subShowCall(0)
    elif topaction == 'y':
        if debug:
            print(returnString)
            print('-> calling inputDateThreshold')
        returnString = inputDateThresholdCall()
    elif topaction == 'Z':
        if debug:
            print(returnString)
            print('-> calling firstTimeCall')
        returnString = browseFromRootCall()
    elif topaction == 'v':
        if debug:
            print(returnString)
            print('-> calling reloadFocusCall')
        returnString = reloadFocusCall()
    elif topaction == 'xx':  # toggle *most* debug outputs
        if debug:
            debug = False
            print('debug OFF')
        else:
            debug = True
            print('debug ON')
    else:
        try:
            if topaction[0] == 'k':
                if debug:
                    print(returnString)
                    print('-> calling showMetadataCall - show')
                returnString = showMetadataCall(int(topaction[1:]))
            if topaction[0] == 'q':
                if topaction == 'q':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('q', callThing)
                returnString = showTermsMetaCall(callThing[0])
            elif topaction[0] == 'j':
                if debug:
                    print(returnString)
                    print('-> calling showBreadcrumbs')
                if len(topaction) == 1:
                    returnString = showBreadcrumbs('Full', False)
                elif topaction[1] == "j":
                    returnString = showBreadcrumbs('Full', True)
                else:
                    returnString = showBreadcrumbs('Full', False)
            elif topaction[0] == 'c':
                if debug:
                    print(returnString)
                    print('-> calling showBreadcrumbs')
                if len(topaction) == 1:
                    returnString = showBreadcrumbs('Snap', False)
                elif topaction[1] == "c":
                    returnString = showBreadcrumbs('Snap', True)
                else:
                    returnString = showBreadcrumbs('Snap', False)
            if topaction[0] == 'r':
                if topaction == 'r':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('r', callThing)
                returnString = showRolesMetaCall(callThing[0])
            if topaction[0] == 't':
                if topaction == 't':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('t', callThing)
                returnString = showAncestorsCall(callThing[0])
            if topaction[0] == 'u':
                if topaction == 'u':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('u', callThing)
                returnString = showDescendantsCall(callThing[0])
            if topaction[0] == 'v':
                if topaction == 'v':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('v', callThing)
                returnString = ancestorsToXLSXCall(callThing[0])
            if topaction[0] == 'w':
                if debug:
                    print('w', returnString[int(topaction[1:])])
                fullSweepBool = True
                proceedBool = True
                if len(topaction[1:]) > 1:
                    if topaction[-1] == '-':
                        fullSweepBool = False
                        topaction = topaction[:-1]
                    elif topaction[-1] == '#':
                        proceedBool = False
                        topaction = topaction[:-1]
                if topaction == 'w' or topaction == 'w-' or topaction == 'w#':
                    callThing = readFocusValue()
                    if topaction == 'w-':
                        fullSweepBool = False
                    if topaction == 'w#':
                        proceedBool = False
                else:
                    callThing = returnString[int(topaction[1:])]
                returnString = descendantsToXLSXCall(callThing[0], fullSweepBool, proceedBool)
            if topaction[0] == 'x':
                if topaction == 'x':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('x', callThing)
                returnString = showRefsetsMetaCall(callThing[0])
            if topaction[0] == 'z':
                if topaction == 'z':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('z', callThing)
                callThing = returnString[int(topaction[1:])]
                returnString = refsetMembersCall(callThing[0], False)
            if topaction[0] == 's':
                if topaction == 's':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('s', callThing)
                returnString = ancestorsToXLSXCall(callThing[0])
            if topaction[0] == '-':
                if debug:
                    print('- help file for', topaction[1:])
                returnString = selectHelpFileCall(topaction[1:])
            if topaction[0] == 'A':
                if debug:
                    print('A', returnString[int(topaction[1:])])
                fullSweepBool = True
                if len(topaction[1:]) > 1:
                    if topaction[-1] == '-':
                        fullSweepBool = False
                        topaction = topaction[:-1]
                if topaction == 'A' or topaction == 'A-':
                    callThing = readFocusValue()
                    if topaction == 'A-':
                        fullSweepBool = True
                else:
                    callThing = returnString[int(topaction[1:])]
                returnString = membersToXLSXCall(callThing[0], fullSweepBool)
            if topaction[0] == 'N':
                if debug:
                    print('N', topaction[1:])
                fullSweepBool = True
                if len(topaction[1:]) > 1:
                    if topaction[-1] == '-':
                        fullSweepBool = False
                        topaction = topaction[:-1]
                if topaction == 'N' or topaction == 'D-':
                    callThing = readFocusValue()
                    if topaction == 'D-':
                        fullSweepBool = True
                else:
                    callThing = topaction[1:]
                returnString = setMembersToXLSXCall(callThing, fullSweepBool)
            if topaction[0] == 'B':
                if debug:
                    print('B', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetRowsCall(returnString[1], callThing[0])
            if topaction[0] == 'C':
                if debug:
                    print('C', returnString[int(topaction[1:])])
                returnString = setPrettyCall(int(topaction[1:]))
            if topaction[0] == 'D':
                if topaction == 'D':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('D', callThing)
                returnString = drawAncestors(callThing[0], 1)
            if topaction[0] == 'E':
                if topaction == 'E':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('E', callThing)
                returnString = drawAncestors(callThing[0], 2)
            if topaction[0] == 'G':
                if topaction == 'G':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('G', callThing)
                returnString = drawDescendants(callThing[0], 1)
            if topaction[0] == 'H':
                if topaction == 'H':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('H', callThing)
                returnString = drawDescendants(callThing[0], 2)
            if topaction[0] == 'I':
                if topaction == 'I':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('I', callThing)
                returnString = drawFocus(callThing[0], 1)
            if topaction[0] == 'J':
                if topaction == 'J':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('J', callThing)
                returnString = drawFocus(callThing[0], 2)
            if topaction[0] == 'F':
                if debug:
                    print('F', returnString)
                returnString = setD3ExtrasCall(int(topaction[1:]))
            if topaction[0] == 'M':
                if debug:
                    print(returnString)
                returnString = acrossDownCall(int(topaction[1:]))
            if topaction[0] == 'K':
                if debug:
                    print(returnString)
                returnString = showSupersCall(int(topaction[1:]))
            if topaction[0] == 'L':
                if debug:
                    print('L', returnString[int(topaction[1:])])
                returnString = graphAngleCall(int(topaction[1:]))
            if debug:
                print(' ', returnString[int(topaction)])
            returnString = browseFromIndexCall(returnString[int(topaction)])
            True
        except:
            True


##if __name__ == '__main__':
##	print 'TCCall is being run by itself'
##else:
##	print 'TCCall being imported from another module'
