import subprocess
import shutil
from SCTRF2config import pth
from deduplicateSnap import deduplicate
from indexbuilder import buildDescIndex
from TCTableBuild import createTCByTablesController
from rscompareBuilder import refsetCompare
from modelLimitFill import fillModelLimit, buildTagsSimple
from concBuilder import concBuild
from writeModelTable import fillAttLimit
from writeModelTableHelpers import getVersion

# **************************************************************************************************************
# check that the data files to be overwritten in the data folder are the same as those being built (int. vs. uk)
# **************************************************************************************************************

dataPath = pth('BASIC_DATA_BUILD') # location of data files during construction

snapPath = dataPath + "RF2Snap.db"
outPath = dataPath + "RF2Out.db"
db1Path = dataPath + 'db1.db'
db2Path = dataPath + 'db2.db'
RF2Out2path = dataPath + 'RF2Out2.db'

# fill snap
subprocess.call(["sqlite3", snapPath, ".read " + dataPath + "importSnapshot.txt"])
subprocess.call(["sqlite3", snapPath, ".quit"])

# deduplicate
deduplicate(snapPath)

# fill out pt 1
subprocess.call(["sqlite3", outPath, ".read " + dataPath + "buildRF2Out_1.txt"])
subprocess.call(["sqlite3", outPath, ".quit"])

# fill description index
buildDescIndex(snapPath, outPath)

# fill out pt 2
subprocess.call(["sqlite3", outPath, ".read " + dataPath + "buildRF2Out_2.txt"])
subprocess.call(["sqlite3", outPath, ".quit"])

# create transitive closure
createTCByTablesController(db1Path, db2Path, snapPath, outPath, RF2Out2path, 1)

# create concrete lookups
subprocess.call(["sqlite3", outPath, ".read " + dataPath + "buildRF2Out_5.txt"])
subprocess.call(["sqlite3", outPath, ".quit"])

# now move RF2Snap.db & RF2Out.db to your preferred data location and run fillAndBuildOutGuidePostMove.py
postMoveSnapPath = pth('RF2_SNAP')
postMoveOutPath = pth('RF2_OUT')
shutil.move(snapPath, postMoveSnapPath)
shutil.move(outPath, postMoveOutPath)

# build refset comparison tables
refsetCompare(postMoveOutPath)

# fill concrete lookups
# CONC
concBuild(postMoveSnapPath, postMoveOutPath)

if getVersion(2) == 3: # only run these on latest international data [Int=3, UK=8]

    # fill model limit tables
    fillModelLimit(postMoveSnapPath)

    # fill attLimit
    fillAttLimit()

    # generate list of semantic tag introduction points
    buildTagsSimple(postMoveOutPath)
