from __future__ import print_function, unicode_literals
import io
import sqlite3
from platformSpecific import cfgPathInstall, sep

def findDups(inTable, fullPath):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    progress = 1
    try:
        data=cursorIn2.execute("SELECT * FROM " + inTable)
        columnList = [column[0] for column in data.description]
        tempString = ', '.join(map(str, columnList))
        cursorIn2.execute("select " + tempString + " from " + inTable + " tbl group by " + tempString + " having count(id) > 1;")
        recordsetIn2 = cursorIn2.fetchall()
        if recordsetIn2:
            cursorIn = connectionIn2.cursor()
            f = io.open(cfgPathInstall() + 'files' + sep() + 'dedup' + sep() + 'dedupLogFull.txt', 'a', encoding="UTF-8")
            for item in recordsetIn2:
                if (progress % 200 == 0):
                    print(item)
                try:
                    # and then deduplicate
                    if (progress % 200 == 0):
                        print("simplify:", item)
                    f.write(inTable + '\t' + 'simplify:' + '\t' + '\t'.join(str(s) for s in item) + '\n')
                    # remove all rows
                    fieldString = ' = ? and '.join(map(str, columnList)) + " = ?"
                    queryString = "delete from " + inTable + " where " + fieldString + ";"
                    cursorIn.execute(queryString, item)
                    # add back keep row
                    markerString = "?, " * len(item)
                    queryString = "insert into " + inTable + " values (" + markerString[:-2] + ");"
                    cursorIn.execute(queryString, item)
                    if (progress % 200 == 0):
                        connectionIn2.commit()
                except:
                    continue
                progress += 1
            connectionIn2.commit()
            cursorIn.close()
            f.close()
        cursorIn2.close()
        connectionIn2.close()
    except:
        print("no id field")

def tables_in_sqlite_db(fullPath):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = [
        v[0] for v in cursorIn2.fetchall()
        if v[0] != "sqlite_sequence"
    ]
    cursorIn2.close()
    connectionIn2.close()
    return tables

def deduplicate(fullPath):
    # initialise log
    f = io.open(cfgPathInstall() + 'files' + sep() + 'dedup' + sep() + 'dedupLogFull.txt', 'w', encoding="UTF-8")
    f.close()
    # List tables
    tables = tables_in_sqlite_db(fullPath)
    print(tables)
    for table in tables:
        print(table)
        findDups(table, fullPath)

if __name__ == '__main__':
    # set path to snap or out db file.
    fullPath = '/home/ed/Data/RF2Full.db'
    deduplicate(fullPath)
