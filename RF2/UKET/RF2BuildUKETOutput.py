import os
import sqlite3


w = raw_input("Input Build Type (Full, Delta, Snapshot): ")
d = raw_input("Input Build Date YYYYMMDD: ")

importItems = []
importMainItems = []

path = '/home/ed/D/Out/'
trimpath = '/home/ed/D/Out'

files = os.listdir(trimpath)                                  # open database

basicFields = 'id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, '

text_file = open("buildUKET" + w + d + ".txt", "w")

text_file.write('.echo ON')
text_file.write("\n")
text_file.write('.separator \\t')
text_file.write("\n")

text_file.write('--CREATE TABLE INSTRUCTIONS')
text_file.write("\n")
text_file.write('CREATE TABLE ConceptsUKET(' + basicFields + 'definitionStatusId TEXT, UKeffectiveTime TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE RelationshipsUKET(' + basicFields + 'sourceId TEXT, destinationId TEXT, relationshipGroup INTEGER, typeId TEXT, characteristicTypeId TEXT, modifierId TEXT, UKeffectiveTime TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE DescriptionsUKET(' + basicFields + 'conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT, UKeffectiveTime TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE DefinitionsUKET(' + basicFields + 'conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT, UKeffectiveTime TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE SimpleRefsetUKET(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, UKeffectiveTime TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE cRefsetUKET(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, componentId1 TEXT, UKeffectiveTime TEXT);')
text_file.write("\n")

text_file.close()

text_file = open("addIndexesUKET" + w + d + ".txt", "w")

text_file.write('.echo ON')
text_file.write("\n")
text_file.write('.separator \\t')
text_file.write("\n")

# declare fields
tableNames = ['ConceptsUKET',
              'RelationshipsUKET',
              'DescriptionsUKET',
              'DefinitionsUKET',
              'SimpleRefsetUKET',
              'cRefsetUKET']

indexFields = ['id',
               'active'] # should have another for full build (effectiveTime)

fullIndexFields = ['id',
               'active',
               'effectiveTime',
               'UKeffectiveTime',
               'moduleId'] # should have another for full build (effectiveTime)

#basic index additions
text_file.write('--BASIC INDEX ADDITIONS')
text_file.write("\n")

for tableName in tableNames:
    if w == 'Full':
        for indexField in fullIndexFields:
            text_file.write('CREATE INDEX ' + tableName + '_' + indexField + '_idx ON ' + tableName + ' (' + indexField + ');')
            text_file.write("\n")
    else:
        for indexField in indexFields:
            text_file.write('CREATE INDEX ' + tableName + '_' + indexField + '_idx ON ' + tableName + ' (' + indexField + ');')
            text_file.write("\n")
    if ('Refset' in tableName):
        text_file.write('CREATE INDEX ' + tableName + '_referencedComponentId_idx ON ' + tableName + ' (referencedComponentId);')
        text_file.write("\n")
        text_file.write('CREATE INDEX ' + tableName + '_refsetId_idx ON ' + tableName + ' (refsetId);')
        text_file.write("\n")

#table-specific index additions (this will grow)
text_file.write('--TABLE-SPECIFIC INDEX ADDITIONS')
text_file.write("\n")
text_file.write('CREATE INDEX DescriptionsUKET_conceptId_idx ON DescriptionsUKET (conceptId);')
text_file.write("\n")
text_file.write('CREATE INDEX DefinitionsUKET_conceptId_idx ON DefinitionsUKET (conceptId);')
text_file.write("\n")
text_file.write('CREATE INDEX DescriptionsUKET_term_idx ON DescriptionsUKET (term);')
text_file.write("\n")
text_file.write('CREATE INDEX RelationshipsUKET_SourceId_idx ON RelationshipsUKET (sourceId);')
text_file.write("\n")
text_file.write('CREATE INDEX RelationshipsUKET_DestinationId_idx ON RelationshipsUKET (destinationId);')
text_file.write("\n")
text_file.write('CREATE INDEX RelationshipsUKET_typeId_idx ON RelationshipsUKET (typeId);')
text_file.write("\n")
text_file.write('CREATE INDEX crefsetUKET_componentId1_idx ON crefsetUKET (componentId1);')
text_file.write("\n")


# tidy up header rows
text_file.write('--REMOVE HEADERS')
text_file.write("\n")

for tableName in tableNames:
    text_file.write('DELETE FROM ' + tableName + ' WHERE id == \'id\';')
    text_file.write("\n")

text_file.close()

# text_file = open("RF2" + w + d + ".bat", "w")
# text_file.write('chcp 850')
# text_file.write("\n")
# text_file.write('C:\PYTHON27\FILES\SQLITE3.EXE RF2'+ w + d + '.db')
# text_file.write("\n")
# text_file.write('.read import' + w + d + '.txt')
# text_file.write("\n")
# text_file.close()
