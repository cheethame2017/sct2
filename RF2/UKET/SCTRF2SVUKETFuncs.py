from SCTRF2SVUKETHelpers import *
from SCTRF2SVUKETconfig import *
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from xlsxHelper import *

modules = ['999000011000000103', '999000021000000109', '999000031000000106', '999000041000000102', '999000011000001104', '999000021000001108']
intModule = ['900000000000207008']

rdsparts = ['999001261000000100', '999000691000001104']
otherLangSet = ['900000000000508004', '900000000000509007', '999001251000000103', '999000681000001101', '999000671000001103']

def snapshotHeader():
    if cfg("QUERY_TYPE") == 1:
        queryTypeString = 'Showing all data'
    elif cfg("QUERY_TYPE") == 2:
        if cfg("SIMP_SNAP") == 1:
            queryTypeString = 'Showing data for ' + str(cfg("EFFECTIVE_TIME")) + ', using simple snapshot'
        else:
            queryTypeString = 'Showing data for ' + str(cfg("EFFECTIVE_TIME")) + ', using simple snapshot (UKET)'
    else:
        queryTypeString = 'Showing data for ' + str(cfg("EFFECTIVE_TIME")) + ', using MDR modified snapshot'
    if cfg('SHOW_TYPE') == 1:
        activeTypeString = ', active rows only'
    elif cfg('SHOW_TYPE') == 2:
        activeTypeString = ', active and inactive rows'
    else:
        activeTypeString = ', inactive rows only'
    if cfg("PTERM_TYPE") == 0 or cfg("QUERY_TYPE") == 1:
        pTermTypeString = ', latest snapshot PTs:'
    else:
        pTermTypeString = ', state valid PTs:'
    print queryTypeString + activeTypeString + pTermTypeString + '\n'


def browseFromId(inId):
    mydict = {}
    mycounter = 1
    # supertypes
    parentList = []
    if  inId[0][-2] == '1':
        myId = tuplify(CiDFromDiD_IN(inId))
    else:
        myId = inId
    # parentList = parentsFromId_IN(myId)  # call interface neutral code
    if cfg("QUERY_TYPE") == 1:
        parentList = SV_SIMparentsFromIdPlusMeta_IN(inId, 3, cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 2:
        parentList = SV_SIMparentsFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
    elif cfg("QUERY_TYPE") == 3:
        parentList = SV_MDRparentsFromIdPlusMeta_IN(inId, cfg('SHOW_TYPE'), effTimeSet())
    if len(parentList) > 0:
        print 'Parents: [' + str(len(parentList)) + ']'
        for listParent in parentList:
            if not listParent is None:
                # print '[' + str(mycounter) + ']\t' + str(listParent)
                print '[' + str(mycounter) + ']\t' + listParent[3] + '\t' + listParent[4]
                if cfg('SHOW_METADATA') > 0:
                    if cfg('SHOW_METADATA') == 2:
                        browseFromIdConMeta(tuplify(listParent[3]), str(mycounter), 0)
                    print '(Rel:Module=' + listParent[5] + ', Active=' + str(listParent[9]) + ', Time=' + listParent[6]+ ')'
                mydict.update(zip(listify(mycounter), listify(tuplify(listParent[3]))))
                mycounter += 1
        print

    # focus
    print 'Focus:'
    print '[' + str(mycounter) + ']\t' + myId[0] + '\t' + SV_pterm(myId)
    browseFromIdConMeta(inId, str(mycounter), 0)
    mydict.update(zip(listify(mycounter), listify(myId)))
    mycounter += 1
    print

    # subtypes
    childList = []
    # childList = childrenFromIdWithPlus_IN(myId)  # call interface neutral code
    if cfg("QUERY_TYPE") == 1:
        childList = SV_SIMchildrenFromIdPlusMeta_IN(inId, 3, cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 2:
        childList = SV_SIMchildrenFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 3:
        childList = SV_MDRchildrenFromIdPlusMeta_IN(inId, cfg('SHOW_TYPE'), effTimeSet())
    print 'Children: [' + str(len(childList)) + ']'
    for listChild in childList:
        if not listChild is None:
            if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
                print '[' + str(mycounter) + ']\t' + " " + listChild[3] + '\t' + listChild[4]
            else:
                print '[' + str(mycounter) + ']\t' + listChild[10] + listChild[3] + '\t' + listChild[4]
            if cfg('SHOW_METADATA') > 0:
                if cfg('SHOW_METADATA') == 2:
                    browseFromIdConMeta(tuplify(listChild[3]), str(mycounter), 0)
                print '(Rel:Module=' + listChild[5] + ', Active=' + str(listChild[9]) + ', Time=' + listChild[6] + ')'
            mydict.update(zip(listify(mycounter), listify(tuplify(listChild[3]))))
            mycounter += 1

    # housekeeping
    print
    return mydict


def browseFromIdConMeta(myId, counterNum, callType):
    preConMeta = []
    if cfg("QUERY_TYPE") == 1:
        preConMeta = SV_ALLconMetaFromConId_IN(myId[0])
    elif cfg("QUERY_TYPE") == 2:
        preConMeta = SV_SIMconMetaFromConId_IN(myId[0], cfg("EFFECTIVE_TIME"))
    else:
        preConMeta = SV_MDRconMetaFromConId_IN(myId[0], effTimeSet())
    someConMeta = False
    for conMeta in preConMeta:
        someConMeta = True
        # print type(preConMeta), len(preConMeta), len(preConMeta[0]), preConMeta, type(conMeta)
        if cfg('SHOW_METADATA') > 0:
            if callType == 0:
                if len(conMeta) == 4:
                    print '(Con:Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ')'
                elif len(conMeta) == 5:
                    print '(Con:Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ', Inac.reason=' + conMeta[4] + ')'
            else:
                if len(conMeta) == 4:
                    print '(Con['+ counterNum + ']:Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ')'
                elif len(conMeta) == 5:
                    print '(Con['+ counterNum + ']:Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ', Inac.reason=' + conMeta[4] + ')'
    if not someConMeta:
        print '(Con:No evidence of concept at this effectiveTime)'


#def allTermsFromConId(id):
    #mydict = {}
    #mycounter = 1
    #print "Terms for:", id, pterm(id)
    #print "([1] to return to selected concept)"
    #print
    #myterm = fsnFromConId_IN(id)
    #print "FSN:"
    #print myterm[0] + '\t' + myterm[1]
    #print
    #myterm = ptFromConId_IN(id)
    #print "Preferred term:"
    #print myterm[0] + '\t' + myterm[1]
    #print
    #myterm = synsFromConId_IN(id)
    #if len(myterm) > 0:
        #print "Acceptable synonyms:"
        #myterm.sort(key=lambda x: x[1])
        #for term in myterm:
            #print term[0] + '\t' + term[1]
        #print
    #try:
        #myterm = defnFromConId_IN(id)
        #if len(myterm) > 0:
            #print "Definition:"
            #print myterm[0] + '\t' + myterm[1]
            #print
    #except:
        #mydict.update(zip(listify(mycounter), listify(tuplify(id))))
        #return mydict
    #else:
        #mydict.update(zip(listify(mycounter), listify(tuplify(id))))
        #return mydict


def allTermsFromConIdPlusMeta(identifier):
    mydict = {}
    mycounter = 1
    effectiveTime = cfg("EFFECTIVE_TIME") #read in from SVconfig file.
    if cfg("QUERY_TYPE") == 1:
        myTerms = SV_SIMterms(identifier, 3, effectiveTime, (RDSParts + otherLangSet))
    if cfg("QUERY_TYPE") == 2:
        myTerms = SV_SIMterms(identifier, cfg("SHOW_TYPE"), effectiveTime, (RDSParts + otherLangSet))
    elif cfg("QUERY_TYPE") == 3:
        myTerms = SV_MDRterms(identifier, cfg("SHOW_TYPE"), effTimeSet(), (RDSParts + otherLangSet))


    print "Terms for:", identifier, SV_pterm(identifier)
    print "([1] to return to selected concept)"
    print
    # 900000000000003001	Fully specified name
    print "FSNs:"
    for rows in myTerms:
        if rows[1][0][6] == '900000000000003001':
            for row in rows[1]:
                print row[0] + '\t' + row[7]
                if cfg('SHOW_METADATA') > 0:
                    print '(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + ', Status=' + str(row[2]) + ')'
            if cfg('SHOW_METADATA') > 0:
                for rsEntry in rows[2]:
                    print '(Ref=' + SV_pterm(rsEntry[4]) + ', Acc=' + SV_pterm(rsEntry[6]) + ', Ref.Mod=' + SV_pterm(rsEntry[3]) + ', Time=' + str(rsEntry[1]) + ', Status=' + str(rsEntry[2]) + ')'
                print
    print "\nSynonyms:"
    # 900000000000013009	Synonym
    for rows in myTerms:
        if rows[1][0][6] == '900000000000013009':
            for row in rows[1]:
                print row[0] + '\t' + row[7]
                if cfg('SHOW_METADATA') > 0:
                    print '(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + ', Status=' + str(row[2]) + ')'
            if cfg('SHOW_METADATA') > 0:
                for rsEntry in rows[2]:
                    print '(Ref=' + SV_pterm(rsEntry[4]) + ', Acc=' + SV_pterm(rsEntry[6]) + ', Ref.Mod=' + SV_pterm(rsEntry[3]) + ', Time=' + str(rsEntry[1]) + ', Status=' + str(rsEntry[2]) + ')'
                print
    # 900000000000550004	Definition
    for rows in myTerms:
        if rows[1][0][6] == '900000000000550004':
            print "\nDefinitions:"
            for row in rows[1]:
                print row[0] + '\t' + row[7]
                if cfg('SHOW_METADATA') > 0:
                    print '(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + ', Status=' + str(row[2]) + ')'
            if cfg('SHOW_METADATA') > 0:
                for rsEntry in rows[2]:
                    print '(Ref=' + SV_pterm(rsEntry[4]) + ', Acc=' + SV_pterm(rsEntry[6]) + ', Ref.Mod=' + SV_pterm(rsEntry[3]) + ', Time=' + str(rsEntry[1]) + ', Status=' + str(rsEntry[2]) + ')'
    print
    mydict.update(zip(listify(mycounter), listify(tuplify(identifier))))
    return mydict


def allTermsFromConId(identifier):
    mydict = {}
    mycounter = 1
    effectiveTime = cfg("EFFECTIVE_TIME") #read in from SVconfig file.

    if cfg("QUERY_TYPE") == 1:
        myTerms = SV_SIMterms(identifier, 3, effectiveTime, (RDSParts + otherLangSet))
    if cfg("QUERY_TYPE") == 2:
        myTerms = SV_SIMterms(identifier, cfg("SHOW_TYPE"), effectiveTime, (RDSParts + otherLangSet))
    elif cfg("QUERY_TYPE") == 3:
        myTerms = SV_MDRterms(identifier, cfg("SHOW_TYPE"), effTimeSet(), (RDSParts + otherLangSet))

    print "Terms for:", identifier, SV_pterm(identifier)
    print "([1] to return to selected concept)"
    print
    # 900000000000003001	Fully specified name
    print "FSNs:"
    for rows in myTerms:
        if rows[1][0][6] == '900000000000003001':
            for row in rows[1]:
                print row[0] + '\t' + row[7]
    print
    print "\nSynonyms:"
    # 900000000000013009	Synonym
    for rows in myTerms:
        if rows[1][0][6] == '900000000000013009':
            for row in rows[1]:
                print row[0] + '\t' + row[7]
    print
    # 900000000000550004	Definition
    for rows in myTerms:
        if rows[1][0][6] == '900000000000550004':
            print "\nDefinitions:"
            for row in rows[1]:
                print row[0] + '\t' + row[7]
    print
    mydict.update(zip(listify(mycounter), listify(tuplify(identifier))))
    return mydict


def searchOnText(searchText, inOption):
    mydict = {}
    mycounter = 0
    counterList = []
    searchList = []
    searchList = searchOnText_IN(searchText, inOption)
    f = open('files/searchreport.txt', 'w')
    f.write('Search for "' + searchText.replace('NEAR ', '') + '":\n')
    for listSearch in searchList:
        if not listSearch is None:
            if mycounter == 0:
                counterList = listSearch
                mycounter += 1
            else:
                print '[' + str(mycounter) + ']\t' + listSearch[1] + '\t' + listSearch[0]
                f.write(listSearch[1].encode('UTF8') + " " + listSearch[0].encode('UTF8') + '\n')
                mydict.update(zip(listify(mycounter), listify(tuplify(listSearch[1]))))
                mycounter += 1
    f.close()
    print
    print '[' + counterList[0] + '] active and [' + counterList[1] + ' (*)] inactive matches'
    print
    # print '[' + (str(mycounter - 1)) + '] active and [' + (str(totalCounts - (mycounter-1))) + ' (*)] inactive matches'
    # print '[*' + (str(totalCounts - 1)) + '*] active terms'
    return mydict


def groupedRolesFromConId(id):
    mydict = {}
    mycounter = 1
    mydict.update(zip(listify(mycounter), listify(tuplify(id))))
    print "Roles for:", id, pterm(id)
    print "([1] to return to selected concept)"
    print
    myroles = groupedRolesFromConId_IN(id)
    for roleGroup in myroles:
        for role in roleGroup:
            if isinstance(role, int):
                if role == -1:
                    print "Parents:"
                elif role == 0:
                    print "Ungrouped:"
                else:
                    print "Group:"
            else:
                mycounter += 1
                mydict.update(zip(listify(mycounter), listify(tuplify(role[1]))))
                mycounter += 1
                mydict.update(zip(listify(mycounter), listify(tuplify(role[3]))))
                print '[' + str(mycounter - 1) + ']', role[1], role[2] + " = " + '[' + str(mycounter) + ']', role[3], role[4]
        print
    return mydict


def historicalOriginsFromConId(id):
    mydict = {}
    mycounter = 1
    mydict.update(zip(listify(mycounter), listify(tuplify(id))))
    print 'Historical origins for', id, pterm(id)
    a = historicalOriginsFromConId_IN(id, 1)
    if len(a) > 0:
        tempId2 = '1'
        for row in a:
            tempId = row[0]
            if tempId != tempId2:
                print
                mycounter += 1
                print '[' + str(mycounter) + ']\t' + row[0] + '\t' + pterm(row[0])
                mydict.update(zip(listify(mycounter), listify(tuplify(row[0]))))
            mycounter += 1
            print '[' + str(mycounter) + ']\t' + row[1] + '\t' + pterm(row[1])
            mydict.update(zip(listify(mycounter), listify(tuplify(row[1]))))
            tempId2 = tempId
    else:
        print '\nNo historical origins'
    print
    print '[1] for', id, pterm(id)
    print
    return mydict


def historicalOriginsFromConIdPlusMeta(id):
    mydict = {}
    mycounter = 1
    mydict.update(zip(listify(mycounter), listify(tuplify(id))))
    print 'Historical origins for', id, pterm(id)
    a = historicalOriginsFromConId_IN(id, 1)
    if len(a) > 0:
        tempId2 = '1'
        for row in a:
            tempId = row[0]
            if tempId != tempId2:
                print
                mycounter += 1
                print '[' + str(mycounter) + ']\t' + row[0] + '\t' + pterm(row[0])
                mydict.update(zip(listify(mycounter), listify(tuplify(row[0]))))
            mycounter += 1
            print '[' + str(mycounter) + ']\t' + row[1] + '\t' + pterm(row[1])
            print '(Module=' + row[3], pterm(row[3]) + ', Time=', row[4] + ')'
            mydict.update(zip(listify(mycounter), listify(tuplify(row[1]))))
            tempId2 = tempId
    else:
        print '\nNo historical origins'
    print
    print '[1] for', id, pterm(id)
    print
    return mydict


def groupedRolesFromConIdPlusMeta(inId):
    mydict = {}
    mycounter = 1
    mydict.update(zip(listify(mycounter), listify(tuplify(inId))))
    print "Roles for:", inId, pterm(inId)
    print "([1] to return to selected concept)"
    print
    effectiveTime = cfg("EFFECTIVE_TIME") #read in from SVconfig file.
    if cfg("QUERY_TYPE") == 1:
        myroles = SV_SIMrolesFromIdPlusMeta_IN(inId, 3, effectiveTime)
    if cfg("QUERY_TYPE") == 2:
        myroles = SV_SIMrolesFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), effectiveTime)
    elif cfg("QUERY_TYPE") == 3:
        myroles = SV_MDRrolesFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), effTimeSet())
    for roleGroup in myroles:
        for role in roleGroup:
            if isinstance(role, int):
                if role == -1:
                    print "Parents:"
                elif role == 0:
                    print "Ungrouped:"
                else:
                    print "Group:"
            else:
                mycounter += 1
                mydict.update(zip(listify(mycounter), listify(tuplify(role[1]))))
                mycounter += 1
                mydict.update(zip(listify(mycounter), listify(tuplify(role[3]))))
                print '[' + str(mycounter - 1) + ']', role[1], role[2] + " = " + '[' + str(mycounter) + ']', role[3], role[4]
                if cfg('SHOW_METADATA') > 0:
                    if cfg('SHOW_METADATA') == 2:
                        browseFromIdConMeta(tuplify(role[1]), str(mycounter - 1), 1)
                        browseFromIdConMeta(tuplify(role[3]), str(mycounter), 1)
                    print '(Rel:Module=' + role[5] + ', Active=' + str(role[9]) + ', Time=' + role[6] + ', Type=' + role[7] + ', Logic=' + role[8] + ')'
        print
    return mydict


def breadcrumbsWrite(inString):
    readbackList = []
    writeToList = []
    if len(inString) > 5:
        try:
            f = open('files/breadcrumbs_sv.txt', 'r')
            lines = f.readlines()
            for line in lines:
                readbackList.append(line)
            matchBool = False
            if (readbackList[-1]).strip() == inString.strip():
                matchBool = True
            if not matchBool:
                readbackList.append(inString.strip() + '\n')
            f.close()
            f = open('files/breadcrumbs_sv.txt', 'w')
            if matchBool:
                writeToList = readbackList
            else:
                writeToList = readbackList[1:]
            for entry in writeToList:
                f.write(entry)
        finally:
            f.close()


def breadcrumbsRead():
    mydict = {}
    mycounter = 1
    f = open('files/breadcrumbs_sv.txt', 'r')
    for line in f:
        print '[' + str(mycounter) + ']\t' + line.strip() + '\t' + pterm(line.strip())
        mydict.update(zip(listify(mycounter), listify(tuplify(line.strip()))))
        mycounter += 1
    f.close()
    return mydict


def focusWrite(inString):
    if len(inString) > 5:
        try:
            f = open('files/focus_sv.txt', 'w')
            f.write(inString)
        finally:
            f.close()


def readInFromTerminal():
    mydict = {}
    f = open('files/focusSwap.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    focusWrite(myId)
    mydict = browseFromId(tuplify(myId))
    return mydict


def readInFromFocus():
    mydict = {}
    f = open('files/focus_sv.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    mydict = browseFromId(tuplify(myId))
    return mydict


def readInFromWindow():
    mydict = {}
    f = open('files/focusSwap_mw.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    focusWrite(myId)
    mydict = browseFromId(tuplify(myId))
    return mydict


def showRefsetRowsFromConAndRefset(memberId, refsetId):
    #chris peter descriptor
    mydict = {}
    mycounter = 1
    mydict.update(zip(listify(mycounter), listify(tuplify(memberId))))
    print 'Rows for ', memberId, pterm(memberId), 'in', refsetId, ptFromConId_IN(refsetId)[1]
    print
    myValueRows = refSetDetailsFromMemIdandSetId_IN(refsetId, memberId)
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    myRefsetType = 'c' + refSetTypeFromConId_IN(refsetId)[0][0]  # if component datatype (can't pick up which one directly)
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                print '[' + str(mycounter) + ']\t' + row[0] + '\t' + value[counter] + '\t' + pterm(value[counter])
                mydict.update(zip(listify(mycounter), listify(tuplify(value[counter]))))
                mycounter += 1
            #if (row[1] == '900000000000461009') or (row[1] == '900000000000460005'):  # if componentId datatype
                #print '[' + str(mycounter) + ']\t' + row[0] + '\t' + value[row[2]] + '\t' + pterm(value[row[2]])
                #mydict.update(zip(listify(mycounter), listify(tuplify(value[row[2]]))))
                #mycounter += 1
            else:
                print '[*]\t' + row[0] + '\t' + str(value[row[2]])
            counter += 1
        print
    print '[1] for', memberId, pterm(memberId)
    print
    return mydict


def showRefsetRowsFromConAndRefsetPlusMeta(memberId, refsetId):
    mydict = {}
    mycounter = 1
    mydict.update(zip(listify(mycounter), listify(tuplify(memberId))))
    print 'Rows for ', memberId, pterm(memberId), 'in', refsetId, ptFromConId_IN(refsetId)[1]
    print
    myValueRows = refSetDetailsFromMemIdandSetIdPlusMeta_IN(refsetId, memberId)
    print myValueRows
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    print myDescRows
    for value in myValueRows:
        print value
        for row in myDescRows:
            print row
            if (row[1] == '900000000000461009') or (row[1] == '900000000000460005'):  # if componentId datatype
                print '[' + str(mycounter) + ']\t' + row[0] + '\t' + value[row[2]] + '\t' + pterm(value[row[2]])
                #print '(Module=' + row[3], pterm(row[3]) + ', Time=', row[4] + ')'
                mydict.update(zip(listify(mycounter), listify(tuplify(value[row[2]]))))
                mycounter += 1
            else:
                print '[*]\t' + row[0] + '\t' + str(value[row[2]])
        print
    print '[1] for', memberId, pterm(memberId)
    print
    return mydict


def showRefsetRowsForRefset(refsetId):
    mydict = {}
    mycounter = 1
    mydict.update(zip(listify(mycounter), listify(tuplify(refsetId))))
    mycounter += 1
    print 'Rows for ', refsetId, ptFromConId_IN(refsetId)[1]
    print
    myRefset = refSetTypeFromConId_IN(refsetId)
    for refset in myRefset:
        refsetType = refset[0]
    myCount = refSetMemberCountFromConId_IN(refsetId, refsetType)
    print "refset member count= ", str(myCount)
    secondaction = raw_input('Filter [Y/N]:')
    if secondaction == 'y':
        if myCount < 250:
            print
            myfilter = 0
        elif myCount < 2500:
            print "approx 1/10 sample:"
            print
            myfilter = 1
        elif myCount < 25000:
            print "approx 1/100 sample:"
            print
            myfilter = 2
        else:
            print "too many members to list"
            print
            return mydict
    else:
        myfilter = 0
    myValueRows = refSetMembersFromConId_IN(refsetId, refsetType, myfilter)
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    myRefsetType = 'c' + refSetTypeFromConId_IN(refsetId)[0][0]  # if component datatype (can't pick up which one directly)
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                print '[' + str(mycounter) + ']\t' + row[0] + '\t' + value[counter] + '\t' + pterm(value[counter])
                mydict.update(zip(listify(mycounter), listify(tuplify(value[counter]))))
                mycounter += 1
            #if (row[1] == '900000000000461009') or (row[1] == '900000000000460005'):  # if componentId datatype
                #print '[' + str(mycounter) + ']\t' + row[0] + '\t' + value[row[2]] + '\t' + pterm(value[row[2]])
                #mydict.update(zip(listify(mycounter), listify(tuplify(value[row[2]]))))
                #mycounter += 1
            else:
                print '[*]\t' + row[0] + '\t' + str(value[row[2]])
            counter += 1
    print
    print '[1] for', refsetId, pterm(refsetId)
    print
    return mydict


def conMetaFromConId(id):
    mydict = {}
    mycounter = 1
    conMeta = []
    conMeta = conMetaFromConId_IN(id)
    print "Concept Metadata for:\n", id, pterm(id)
    if len(conMeta) == 4:
        print '(Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ')'
    else:
        print '(Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ', Inac.reason=' + conMeta[4] + ')'
    print
    print "([1] to return to selected concept)"
    print
    mydict.update(zip(listify(mycounter), listify(tuplify(id))))
    return mydict


def ancestorsFromConId(inId):
    mydict = {}
    mycounter = 1
    # ancestors
    ancestorList = []
    ancestorList = SV_SIMancestorsFromConId_IN(inId)  # call interface neutral code
    inactiveAncestorList = []
    allAncestorList = []
    if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
        allAncestorList = ALLancestorsFromConIdToET(inId, 1)
        inactiveAncestorList = list(set(allAncestorList).difference(set(ancestorList)))
        print "\033c"
        snapshotHeader()
    if len(ancestorList) > 0:
        print 'Active ancestors: [' + str(len(ancestorList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)'
        print
        for ancestor in ancestorList:
            if not ancestor is None:
                if (cfg('SHOW_TYPE') == 1) or (cfg('SHOW_TYPE') == 2):
                    print '[' + str(mycounter) + ']\t' + ancestor[0] + '\t' + ancestor[1]
                mydict.update(zip(listify(mycounter), listify(tuplify(ancestor[0]))))
                if ancestor[0] == inId:
                    findid = mycounter
                mycounter += 1
        print
        if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
            print 'Former ancestors: [' + str(len(inactiveAncestorList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)'
            print
            for ancestor in inactiveAncestorList:
                if not ancestor is None:
                    print '[' + str(mycounter) + ']\t' + ancestor[0] + '\t' + ancestor[1]
                    mydict.update(zip(listify(mycounter), listify(tuplify(ancestor[0]))))
                    mycounter += 1
            print
        print '[' + str(findid) + '] to return to', inId, SV_pterm(inId)
        print
    else:
        print "\033c"
        mydict = browseFromId(tuplify(id))
    return mydict


def descendantsFromConId(inId):
    # need preliminary code to check for descendant count
    #descCount = descendantCountFromConId_IN(inId)
    #print inId, pterm(inId), 'has', str(descCount), 'descendants.'
    #secondaction = raw_input('Continue [Y/N]:')
    mydict = {}
    mycounter = 1
    descendantList = []
    descendantList = SV_SIMdescendantsFromConId_IN(inId)
    inactiveDescendantList = []
    allDescendantList = []
    if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
        allDescendantList = ALLdescendantsFromConIdToET(inId, 1)
        inactiveDescendantList = list(set(allDescendantList).difference(set(descendantList)))
        print "\033c"
        snapshotHeader()
    if len(descendantList) > 0:
        print 'Active descendants: [' + str(len(descendantList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)'
        print
        for descendant in descendantList:
            if not descendant is None:
                if (cfg('SHOW_TYPE') == 1) or (cfg('SHOW_TYPE') == 2):
                    print '[' + str(mycounter) + ']\t' + descendant[0] + '\t' + descendant[1]
                mydict.update(zip(listify(mycounter), listify(tuplify(descendant[0]))))
                if descendant[0] == inId:
                    findid = mycounter
                mycounter += 1
        print
        if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
            print 'Former descendants: [' + str(len(inactiveDescendantList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)'
            print
            for descendant in inactiveDescendantList:
                if not descendant is None:
                    print '[' + str(mycounter) + ']\t' + descendant[0] + '\t' + descendant[1]
                    mydict.update(zip(listify(mycounter), listify(tuplify(descendant[0]))))
                    mycounter += 1
            print
        print '[' + str(findid) + '] to return to', inId, SV_pterm(inId)
        print
    else:
        print "\033c"
        mydict = browseFromId(tuplify(id))
            # mydict.update(zip(listify(mycounter), listify(tuplify(id))))
    return mydict


#def lookUpDiD(inId):
    #mydict = {}
    #mycounter = 1
    #print '[' + str(mycounter) + ']\t' + line.strip() + '\t' + pterm(line.strip())
    #mydict.update(zip(listify(mycounter), listify(tuplify(line.strip()))))
    #return mydict

def setEffectiveTime():
    while True:
        effectiveTime = raw_input('Date YYYYMMDD:')
        if len(effectiveTime) == 8 and not(effectiveTime == 'uuuuuuuu' or effectiveTime == 'dddddddd'):  # should do with regex
        # if re.match("[^0-9]([0-9]{8})[^0-9]", effectiveTime):
            cfgWrite("EFFECTIVE_TIME", effectiveTime)
            try:
                f = open('files/effectiveTime.txt', 'w')
                f.write(effectiveTime)
            finally:
                f.close()
            return effectiveTime
        else:
            True
        if effectiveTime == 'now':
            effectiveTime = time.strftime("%Y%m%d")
            cfgWrite("EFFECTIVE_TIME", effectiveTime)
            try:
                f = open('files/effectiveTime.txt', 'w')
                f.write(effectiveTime)
            finally:
                f.close()
            return effectiveTime
        if len(effectiveTime) > 0:
            if effectiveTime[0] == 'u':
                effectiveTimeTemp = str(cfg('EFFECTIVE_TIME'))
                effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
                sixMonthIncrement = relativedelta(months=(6 * len(effectiveTime)))
                effTimeObj += sixMonthIncrement
                effectiveTime = effTimeObj.strftime("%Y%m%d")
                cfgWrite("EFFECTIVE_TIME", effectiveTime)
                try:
                    f = open('files/effectiveTime.txt', 'w')
                    f.write(effectiveTime)
                finally:
                    f.close()
                return effectiveTime
            if effectiveTime[0] == 'd':
                effectiveTimeTemp = str(cfg('EFFECTIVE_TIME'))
                effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
                sixMonthIncrement = relativedelta(months=(6 * len(effectiveTime)))
                effTimeObj -= sixMonthIncrement
                effectiveTime = effTimeObj.strftime("%Y%m%d")
                cfgWrite("EFFECTIVE_TIME", effectiveTime)
                try:
                    f = open('files/effectiveTime.txt', 'w')
                    f.write(effectiveTime)
                finally:
                    f.close()
                return effectiveTime
        else:
            True

def ALLancestorsFromConIdToET(inId, latestOrSnap): #latestOrSnap: 0=latest date, 1=snapshotdate
    allAncestorsEver = []
    ET = "20020131"
    CON_ID = inId
    u = 1
    if latestOrSnap == 1:
        endET = str(cfg("EFFECTIVE_TIME"))
    else:
        endET = str(time.strftime("%Y%m%d"))
    # identify all ancestors up to endET
    while ET <= endET:
        print "\033c"
        print 'Calculating ancestors: '+ '*' * u
        print ET
        ancestorList = SV_SIMFindAncestorsForConIdAndET(CON_ID, ET)
        for node in ancestorList:
            allAncestorsEver.append(node)
        ET = upByNMonths(ET, 6)
        u += 1
    uniqueAllAncestorsEver = list(set(allAncestorsEver))
    uniqueAllAncestorsEver.sort(key=lambda x: x[1].lower())
    return uniqueAllAncestorsEver


def ALLdescendantsFromConIdToET(inId, latestOrSnap): #latestOrSnap: 0=latest date, 1=snapshotdate
    allDescendantsEver = []
    ET = "20020131"
    CON_ID = inId
    u = 1
    if latestOrSnap == 1:
        endET = str(cfg("EFFECTIVE_TIME"))
    else:
        endET = str(time.strftime("%Y%m%d"))
    # identify all ancestors up to endET
    while ET <= endET:
        print "\033c"
        print 'Calculating descendants: '+ '*' * u
        print ET
        descendantList = SV_SIMFindDescendantsForConIdAndET(CON_ID, ET)
        for node in descendantList:
            allDescendantsEver.append(node)
        ET = upByNMonths(ET, 6)
        u += 1
    uniqueAllDescendantsEver = list(set(allDescendantsEver))
    uniqueAllDescendantsEver.sort(key=lambda x: x[1].lower())
    return uniqueAllDescendantsEver


def ALLdescendantsToXLSX(inId):
    mydict = {}
    CON_ID = inId
    uniqueAllDescendantsEver = ALLdescendantsFromConIdToET(CON_ID, 0)
    topLine = []
    matrix = []
    topLine.append(" ")
    topLine.append(" ")
    ET = "20020131"
    while ET < time.strftime("%Y%m%d"):
        topLine.append(ET)
        ET = upByNMonths(ET, 6)
    u = len(uniqueAllDescendantsEver)
    for thing in uniqueAllDescendantsEver:
        tempLine =[]
        ET = "20020131"
        print "\033c"
        snapshotHeader()
        if u > 1:
            print 'Processing descendants: '+ '*' * (u - 1)
            print thing[0], thing[1]
        else:
            print 'Completed.'
        tempLine.append(thing[0])
        tempLine.append(repr(thing[1]))
        while ET < time.strftime("%Y%m%d"):
            character = SV_SIMC1_IsA_C2_Compare(thing[0], CON_ID, 1, ET)
            tempLine.append(character)
            ET = upByNMonths(ET, 6)
        if CON_ID != thing[0]:
            matrix.append(tempLine)
        u -= 1
    csvFile('descendants2', CON_ID, topLine, matrix)
    xlsxFile('descendants2', CON_ID, topLine, matrix)
    mydict = browseFromId(tuplify(inId))
    return mydict


def ALLancestorsToXLSX(inId):
    mydict = {}
    CON_ID = inId
    uniqueAllAncestorsEver = ALLancestorsFromConIdToET(CON_ID, 0)
    topLine = []
    matrix = []
    topLine.append(" ")
    topLine.append(" ")
    ET = "20020131"
    while ET < time.strftime("%Y%m%d"):
        topLine.append(ET)
        ET = upByNMonths(ET, 6)
    u = len(uniqueAllAncestorsEver)
    for thing in uniqueAllAncestorsEver:
        tempLine =[]
        ET = "20020131"
        print "\033c"
        snapshotHeader()
        if u > 1:
            print 'Processing ancestors: '+ '*' * (u - 1)
            print thing[0], thing[1]
        else:
            print 'Completed.'
        tempLine.append(thing[0])
        tempLine.append(repr(thing[1]))
        while ET < time.strftime("%Y%m%d"):
            character = SV_SIMC1_IsA_C2_Compare(CON_ID, thing[0], 2, ET)
            tempLine.append(character)
            ET = upByNMonths(ET, 6)
        if (CON_ID != thing[0]) and ('138875005' != thing[0]):
            matrix.append(tempLine)
        u -= 1
    csvFile('ancestors2', CON_ID, topLine, matrix)
    xlsxFile('ancestors2', CON_ID, topLine, matrix)
    mydict = browseFromId(tuplify(inId))
    return mydict


def refSetFromConIdPlusMeta(id):
    mydict = {}
    mycounter = 1
    refsetType = ['simplerefset',
                'ccirefset',
                'cirefset',
                'crefset',
                'icrefset',
                'iisssccrefset',
                'iissscirefset',
                'iissscrefset',
                'srefset',
                'ssrefset']

    mydict.update(zip(listify(mycounter), listify(tuplify(id))))
    print "Refsets for:", id, pterm(id)
    print "([1] to return to selected concept)"
    print

    for rType in refsetType:
        if cfg("QUERY_TYPE") == 1:
            refs = SV_SIMrefSetFromConIdPlusMeta_IN(id, rType, 3, cfg("EFFECTIVE_TIME"))
        if cfg("QUERY_TYPE") == 2:
            refs = SV_SIMrefSetFromConIdPlusMeta_IN(id, rType, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
        if cfg("QUERY_TYPE") == 3:
            refs = SV_MDRrefSetFromConIdPlusMeta_IN(id, rType, cfg('SHOW_TYPE'), effTimeSet())
        for ref in refs:
            mycounter += 1
            mydict.update(zip(listify(mycounter), listify(tuplify(ref[0]))))
            print '[' + str(mycounter) + ']\t' + ref[0] + '\t' + ref[1]
            if cfg('SHOW_METADATA') > 0:
                print '(Ref:Module=' + ref[3] + ', Active=' + str(ref[5]) + ', Time=' + ref[4] + ')'
    print
    return mydict

def setQueryType(inInteger):
    cfgWrite("QUERY_TYPE", inInteger)
    # if inInteger == 1 or inInteger == 3:
    #if inInteger == 1:
        #cfgWrite("SUB_SHOW", 0)
    return inInteger


def setSimpleSnap(inInteger):
    cfgWrite("SIMP_SNAP", inInteger)
    # if inInteger == 1 or inInteger == 3:
    #if inInteger == 1:
        #cfgWrite("SUB_SHOW", 0)
    return inInteger


def setShowType(inInteger):
    cfgWrite("SHOW_TYPE", inInteger)
    return inInteger


def setShowMetadata(inInteger):
    cfgWrite("SHOW_METADATA", inInteger)
    return inInteger


def setPtermControl(inInteger):
    cfgWrite("PTERM_TYPE", inInteger)
    return inInteger


def setSubShow(inInteger):
    cfgWrite("SUB_SHOW", inInteger)
    return inInteger

# if cfg('ROLE_DISPLAY_TYPE') == 0