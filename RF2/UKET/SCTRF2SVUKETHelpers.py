import sqlite3
from helpers import *
from SCTRF2SVUKETconfig import *
from py2neo import *
import time

fullPath = pth('RF2_FULL')
UKOutPath = pth('RF2_UKOUT')
modsetPath = pth('MODSET')

RDSParts = ['999001261000000100', '999000691000001104']
otherLangSet = ['900000000000508004', '900000000000509007', '999001251000000103', '999000681000001101', '999000671000001103']
modules = ['999000011000000103', '999000021000000109', '999000031000000106', '999000041000000102', '999000011000001104', '999000021000001108']
intModule = ['900000000000207008']


def SV_SIMparentsFromIdPlusMeta_IN(inId, activeStatus, effectiveTime):
    parents = []
    roleList = []
    roleList = SV_SIMgroupedRolesFromConIdPlusMeta_IN(inId[0], activeStatus, effectiveTime)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    parents.append(role)
    return parents


def SV_SIMrolesFromIdPlusMeta_IN(inId, activeStatus, effectiveTime):
    roleList = []
    roleList = SV_SIMgroupedRolesFromConIdPlusMeta_IN(inId, activeStatus, effectiveTime)
    #for roleGroup in roleList:
        #for role in roleGroup:
            #if not isinstance(role, int):
                #if role[0] != -1:
                    #roles.append(role)
    return roleList


def SV_SIMchildrenFromIdPlusMeta_IN(inId, activeStatus, effectiveTime):
    children = []
    roleList = []
    roleList = SV_SIMchildrenFromConIdPlusMeta_IN(inId[0], activeStatus, effectiveTime)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    children.append(role)
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return children
    else:
        return roleList


def SV_MDRparentsFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    parents = []
    roleList = []
    roleList = SV_MDRgroupedRolesFromConIdPlusMeta_IN(inId[0], activeStatus, effTimeSet)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    parents.append(role)
    return parents


def SV_MDRrolesFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    roleList = []
    roleList = SV_MDRgroupedRolesFromConIdPlusMeta_IN(inId, activeStatus, effTimeSet)
    return roleList


def SV_MDRchildrenFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    children = []
    childList = []
    # childList = SV_SIMchildrenFromConIdPlusMeta_IN(inId[0], 3, str(cfg('EFFECTIVE_TIME')))
    childList = SV_MDRchildrenFromConIdPlusMeta_IN(inId[0], activeStatus, effTimeSet)
    for childGroup in childList:
        for child in childGroup:
            if not isinstance(child, int):
                if child[0] == -1:
                    children.append(child)
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return children
    else:
        return childList


def SV_MDRModCorrelparentsFromIdPlusMeta_IN(inId, activeStatus, effTimeSet):
    parents = []
    roleList = []
    roleList = SV_MDRModCorrelgroupedRolesFromConIdPlusMeta_IN(inId[0], activeStatus, effTimeSet)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    parents.append(role)
    return parents


def SV_SIMchildrenFromConIdPlusMeta_IN(id, activeFlag, effectiveTime):
    if cfg("SIMP_SNAP") == 1:
        connectionIn2 = sqlite3.connect(fullPath)
    else:
        connectionIn2 = sqlite3.connect(UKOutPath)
    cursorIn2 = connectionIn2.cursor()
    ## children
    if cfg("SIMP_SNAP") == 1:
        if activeFlag == 1:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=1 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 3:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where destinationId = ?", tuplify(id))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=0 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (id, effectiveTime))
    else:
        if activeFlag == 1:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, UKeffectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where active=1 and destinationId = ? and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, UKeffectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where destinationId = ? and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 3:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, UKeffectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where destinationId = ?", tuplify(id))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, UKeffectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where active=0 and destinationId = ? and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (id, effectiveTime))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == '116680003':
            tempSet = (role[0] - 1, role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == '116680003':
            tempSet = (role[0], role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
        preRoleList.append(tempSet)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    # attempt at plusses:
    if (cfg("SUB_SHOW") == 1) and (cfg("QUERY_TYPE") != 1):
        roleListOut = []
        for roleSet in roleList:
            if not roleSet is None:
                for role in roleSet:
                    if not isinstance(role, int):
                        if role[0] == -1:
                            if cfg("SIMP_SNAP") == 1:
                                if activeFlag == 1:
                                    cursorIn2.execute("select typeId from relationships where active=1 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (role[3], effectiveTime))
                                elif activeFlag == 2:
                                    cursorIn2.execute("select typeId from relationships where destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (role[3], effectiveTime))
                                elif activeFlag == 3:
                                    cursorIn2.execute("select typeId from relationships where destinationId = ?", tuplify(role[3]))
                                else:
                                    cursorIn2.execute("select typeId from relationships where active=0 and destinationId = ? and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (role[3], effectiveTime))
                            else:
                                if activeFlag == 1:
                                    cursorIn2.execute("select typeId from relationshipsUKET where active=1 and destinationId = ? and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (role[3], effectiveTime))
                                elif activeFlag == 2:
                                    cursorIn2.execute("select typeId from relationshipsUKET where destinationId = ? and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (role[3], effectiveTime))
                                elif activeFlag == 3:
                                    cursorIn2.execute("select typeId from relationshipsUKET where destinationId = ?", tuplify(role[3]))
                                else:
                                    cursorIn2.execute("select typeId from relationshipsUKET where active=0 and destinationId = ? and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (role[3], effectiveTime))
                            recordsetIn = cursorIn2.fetchall()
                            hasSubBool = False
                            for role2 in recordsetIn:
                                if role2[0] == '116680003':
                                    hasSubBool = True
                            #if role[9] == 1:
                                ## if not recordsetIn:
                                #if not hasSubBool:
                                    #tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" +  str(role[4]) + "|" +  str(role[5]) + "|" +  str(role[6]) + "|" +  str(role[7]) + "|" +  str(role[8]) + "|" +  str(role[9]) + "| "
                                #else:
                                    #tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" +  str(role[4]) + "|" +  str(role[5]) + "|" +  str(role[6]) + "|" +  str(role[7]) + "|" +  str(role[8]) + "|" +  str(role[9]) + "|+"
                            #else:
                                #tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" +  str(role[4]) + "|" +  str(role[5]) + "|" +  str(role[6]) + "|" +  str(role[7]) + "|" +  str(role[8]) + "|" +  str(role[9]) + "|*"
                            if role[9] == 1:
                                # if not recordsetIn:
                                if not hasSubBool:
                                    tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "| "
                                else:
                                    tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|+"
                            else:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|*"
                            roleListOut.append(tempstring.split("|"))
    # end attempt at plusses
    cursorIn2.close()
    connectionIn2.close()
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return roleList
    else:
        return roleListOut


def SV_SIMgroupedRolesFromConIdPlusMeta_IN(id, activeFlag, effectiveTime):
    if cfg("SIMP_SNAP") == 1:
        connectionIn2 = sqlite3.connect(fullPath)
        cursorIn2 = connectionIn2.cursor()
        # children
        if activeFlag == 1:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=1 and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 3:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where sourceId = ?", tuplify(id))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=0 and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (id, effectiveTime))
    else:
        connectionIn2 = sqlite3.connect(UKOutPath)
        cursorIn2 = connectionIn2.cursor()
        # children
        if activeFlag == 1:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where active=1 and sourceId = ?  and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where sourceId = ?  and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (id, effectiveTime))
        elif activeFlag == 3:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where sourceId = ?", tuplify(id))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationshipsUKET where active=0 and sourceId = ?  and relationshipsUKET.UKeffectiveTime=(select max(rel2.UKeffectiveTime) from relationshipsUKET rel2 where rel2.id = relationshipsUKET.id and rel2.UKeffectiveTime <= ?)", (id, effectiveTime))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == '116680003':
            tempSet = (role[0] - 1, role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == '116680003':
            tempSet = (role[0], role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
        preRoleList.append(tempSet)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList


def SV_MDRgroupedRolesFromConIdPlusMeta_IN(id, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    preRoleList = []
    rgIntList = []
    for row in effTimeSet:
        # children
        if activeFlag == 0:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=0 and moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], id, row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], id, row[1]))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=1 and moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], id, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for role in recordsetIn2:
            if role[1] == '116680003':
                tempSet = (role[0] - 1, role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not (role[0] - 1) in rgIntList:
                    rgIntList.append(role[0] - 1)
            if not role[1] == '116680003':
                tempSet = (role[0], role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not role[0] in rgIntList:
                    rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList


def SV_MDRchildrenFromConIdPlusMeta_IN(inId, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    preRoleList = []
    rgIntList = []
    for row in effTimeSet:
        # children
        if activeFlag == 0:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=0 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], inId, row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], inId, row[1]))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=1 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], inId, row[1]))
            # cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?) and active=1 and moduleId = ? and destinationId = ?", (row[1], row[0], inId))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for role in recordsetIn2:
            if role[1] == '116680003':
                tempSet = (role[0] - 1, role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[3])
                preRoleList.append(tempSet)
                if not (role[0] - 1) in rgIntList:
                    rgIntList.append(role[0] - 1)
            if not role[1] == '116680003':
                tempSet = (role[0], role[1], SV_pterm(role[1]), role[2], SV_pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[3])
                preRoleList.append(tempSet)
                if not role[0] in rgIntList:
                    rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    if (cfg("SUB_SHOW") == 1) and (cfg("QUERY_TYPE") != 1):  # suspicious as to why this works - ? should use standard efftime format but currently doesn't' ok now!
        roleListOut = []
        for roleSet in roleList:
            if not roleSet is None:
                for role in roleSet:
                    matchBool = False
                    if (not isinstance(role, int)) and (role[0] == -1):
                        for row in effTimeSet:  # swap execute lines and indent below to # marker
                            if activeFlag == 0:
                                cursorIn2.execute("select typeId from relationships where active=0 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], role[3], row[1]))
                            elif activeFlag == 2:
                                cursorIn2.execute("select typeId from relationships where moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], role[3], row[1]))
                            else:
                                cursorIn2.execute("select typeId from relationships where active=1 and moduleId = ? and destinationId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (row[0], role[3], row[1]))
                            recordsetIn = cursorIn2.fetchall()
                            for row2 in recordsetIn:
                                if row2[0] == '116680003':
                                    matchBool = True  # marker
                        if role[9] == 1:
                            if not matchBool:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "| "
                            else:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|+"
                        else:
                            tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|*"
                        roleListOut.append(tempstring.split("|"))
    cursorIn2.close()
    connectionIn2.close()
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return roleList
    else:
        return roleListOut


def SV_MDRModCorrelgroupedRolesFromConIdPlusMeta_IN(id, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    preRoleList = []
    rgIntList = []
    for row in effTimeSet:
        # children
        if activeFlag == 0:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=0 and sourceId = ?", tuplify(id))
        elif activeFlag == 2:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where moduleId = ? and sourceId = ?  and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.moduleId = relationships.moduleId and rel2.effectiveTime <= ?)", (row[0], id, row[1]))
        else:
            cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active from relationships where active=1 and sourceId = ?", tuplify(id))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for role in recordsetIn2:
            if role[1] == '116680003':
                tempSet = (role[0] - 1, role[1], pterm(role[1]), role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not (role[0] - 1) in rgIntList:
                    rgIntList.append(role[0] - 1)
            if not role[1] == '116680003':
                tempSet = (role[0], role[1], pterm(role[1]), role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7])
                preRoleList.append(tempSet)
                if not role[0] in rgIntList:
                    rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList


def SV_SIMconMetaFromConId_IN(id, effectiveTime):
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn1 = sqlite3.connect(fullPath)
    if cfg("SIMP_SNAP") == 1:
        connectionIn2 = sqlite3.connect(fullPath)
        cursorIn1 = connectionIn2.cursor()

        cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.effectiveTime <= ? )", (id, effectiveTime))
    else:
        connectionIn2 = sqlite3.connect(UKOutPath)
        cursorIn1 = connectionIn2.cursor()

        cursorIn1.execute("select conceptsUKET.moduleId, conceptsUKET.active, conceptsUKET.effectiveTime, conceptsUKET.definitionStatusId from conceptsUKET where conceptsUKET.id = ? and conceptsUKET.UKeffectiveTime = (select max(c2.UKeffectiveTime) from conceptsUKET c2 where c2.id = conceptsUKET.id and c2.UKeffectiveTime <= ? )", (id, effectiveTime))

    recordsetIn1 = cursorIn1.fetchone()
    if recordsetIn1:
        if recordsetIn1[1] == 0:
            cursorIn2 = connectionIn1.cursor()
            cursorIn2.execute("select componentId1 from crefset where refsetId='900000000000489007' and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.id = crefset.id and cr2.effectiveTime <= ? )", (id, effectiveTime))
            recordsetIn2 = cursorIn2.fetchall()
            if len(recordsetIn2) == 1:
                reasonForInactivation = pterm(recordsetIn2[0])
            conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
            cursorIn2.close()
        else:
            conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
        conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn1.close()
    connectionIn2.close()
    #900000000000489007
    return conMetaSet


def SV_SIMconMetaFromConIdSimple_IN(inId, effectiveTime):
    conMetaSetpre = ()
    conMetaSet = []
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.effectiveTime <= ? )", (inId, effectiveTime))
    recordsetIn1 = cursorIn1.fetchone()
    if recordsetIn1:
        conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
        conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn2.close()
    #900000000000489007
    return conMetaSet


def SV_MDRconMetaFromConId_IN(id, effTimeSet):
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()

    for row in effTimeSet:
        #cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.moduleId = ? and c2.effectiveTime <= ? )", (row[0], id, row[0], row[1]))
        cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.effectiveTime <= ? )", (row[0], id, row[1]))

        recordsetIn1b = cursorIn1.fetchall()
        if len(recordsetIn1b) > 0:
            recordsetIn1 = recordsetIn1b[0]
            if recordsetIn1[1] == 0:
                cursorIn2 = connectionIn2.cursor()
                cursorIn2.execute("select componentId1 from crefset where refsetId='900000000000489007' and moduleId = ? and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.id = crefset.id and cr2.effectiveTime <= ? )", (row[0], id, row[1]))
                recordsetIn2 = cursorIn2.fetchall()
                if len(recordsetIn2) == 1:
                    reasonForInactivation = pterm(recordsetIn2[0])
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
                cursorIn2.close()
            else:
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
            conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn2.close()
    #900000000000489007
    return conMetaSet


def SV_ALLconMetaFromConId_IN(id):  # conceptDetails for all data
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()

    cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.id = ?", (id, ))
    recordsetIn1b = cursorIn1.fetchall()
    if len(recordsetIn1b) > 0:
        for recordsetIn1 in recordsetIn1b:
        # recordsetIn1 = recordsetIn1b[0]
            if recordsetIn1[1] == 0:
                cursorIn2 = connectionIn2.cursor()
                cursorIn2.execute("select componentId1 from crefset where refsetId='900000000000489007' and referencedComponentId= ?", (id, ))
                recordsetIn2 = cursorIn2.fetchall()
                if len(recordsetIn2) == 1:
                    reasonForInactivation = pterm(recordsetIn2[0])
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
                cursorIn2.close()
            else:
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
            conMetaSet.append(conMetaSetpre)
            conMetaSet.sort(key=lambda x: x[2])
    cursorIn1.close()
    connectionIn2.close()
    return conMetaSet


def SV_MDRModCorrelconMetaFromConId_IN(id, effTimeSet):
    conMetaSetpre = ()
    conMetaSet = []
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn2.cursor()

    for row in effTimeSet:
        cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and c2.moduleId = ? and c2.effectiveTime <= ? )", (row[0], id, row[0], row[1]))
        #cursorIn1.executle("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.moduleId = ? and concepts.id = ? and concepts.effectiveTime = (select max(c2.effectiveTime) from concepts c2 where c2.id = concepts.id and and c2.effectiveTime <= ? )", (row[0], id, row[1]))
        recordsetIn1b = cursorIn1.fetchall()
        if len(recordsetIn1b) > 0:
            recordsetIn1 = recordsetIn1b[0]
            if recordsetIn1[1] == 0:
                cursorIn2 = connectionIn2.cursor()
                cursorIn2.execute("select componentId1 from crefset where refsetId='900000000000489007' and moduleId = ? and referencedComponentId= ? and crefset.effectiveTime = (select max(cr2.effectiveTime) from crefset cr2 where cr2.referencedComponentId = crefset.referencedComponentId and cr2.effectiveTime <= ? )", (row[0], id, row[1]))
                recordsetIn2 = cursorIn2.fetchall()
                if len(recordsetIn2) == 1:
                    reasonForInactivation = pterm(recordsetIn2[0])
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
                cursorIn2.close()
            else:
                conMetaSetpre = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
            conMetaSet.append(conMetaSetpre)
    cursorIn1.close()
    connectionIn2.close()
    #900000000000489007
    return conMetaSet


def effTimeSet():
    if int(cfg("EFFECTIVE_TIME")) > 20040131:
        EffTimeSet = prelim(cfg("EFFECTIVE_TIME"), modules)
    else:
        EffTimeSet = prelim(cfg("EFFECTIVE_TIME"), (modules + intModule))
    return EffTimeSet


def prelim(seedTime, seedModules):
    connectionIn1 = sqlite3.connect(modsetPath)

    cursorIn1 = connectionIn1.cursor()
    cursorIn2 = connectionIn1.cursor()
    cursorIn1.execute("drop table modset")
    cursorIn1.execute("create table modset (mod char(20), mod_et char(20))")
    cursorIn1.execute("ATTACH DATABASE '" + fullPath + "' AS RF2Full")
    #mstring1 = ', '.join('?' * len(modules))
    mstring = ', '.join(map(str, seedModules))
    # with input seedModules converted to mstring
    queryString = "select distinct referencedComponentId as mod, string2 as mod_et from RF2Full.ssrefset where moduleId in (" + mstring + ") and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?) union select distinct ssrefset.moduleId as mod, effectiveTime as mod_et from ssrefset where moduleId in (" + mstring + ") and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?)"
    cursorIn2.execute(queryString, (seedTime, seedTime))
    # with seedModules subquery
    #queryString = "select distinct ssrefset.referencedComponentId as mod, string2 as mod_et from ssrefset where moduleId in (select distinct moduleId from ssrefset where ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.effectiveTime <= ?)) and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?) union select distinct ssrefset.moduleId as mod, effectiveTime as mod_et from ssrefset where moduleId in (select distinct moduleId from ssrefset where ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.effectiveTime <= ?)) and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?)"
    #cursorIn2.execute(queryString, (seedTime, seedTime, seedTime, seedTime))
    recordsetIn2 = cursorIn2.fetchall()
    preModETWrite(seedTime, seedModules, recordsetIn2)
    for row in recordsetIn2:
        cursorIn1.execute('insert into modset values (?,?)', row)
    connectionIn1.commit()
    # needed because of broken data
    cursorIn1.execute('select mod, mod_et from modset where modset.mod_et = (select max(m2.mod_et) from modset m2 where m2.mod = modset.mod and m2.mod_et <= ?)', (seedTime,))
    recordsetIn1 = cursorIn1.fetchall()
    returnSet = recordsetIn1
    cursorIn1.close()
    cursorIn2.close()
    connectionIn1.close()
    modETWrite(seedTime, seedModules, returnSet)
    return returnSet


def preModETWrite(seedTime, seedModules, recSet):
    try:
        f = open('files/preModET_sv.txt', 'w')
        f.write(str(seedTime) + '\n\n')
        for row in seedModules:
            f.write(str(row) + '\n')
        f.write('\n')
        for row in recSet:
            f.write(str(row) + '\n')
    finally:
        f.close()


def modETWrite(seedTime, seedModules, recSet):
    try:
        f = open('files/modET_sv.txt', 'w')
        f.write(str(seedTime) + '\n\n')
        for row in seedModules:
            f.write(str(row) + '\n')
        f.write('\n')
        for row in recSet:
            f.write(str(row) + '\n')
    finally:
        f.close()


def SV_SIMterms(inId, activeStatus, effectiveTime, refSets):
    if cfg("SIMP_SNAP") == 1:
        connectionIn2 = sqlite3.connect(fullPath)
    else:
        connectionIn2 = sqlite3.connect(UKOutPath)
    connectionIn3 = sqlite3.connect(fullPath)
    cursorIn3 = connectionIn3.cursor()
    cursorIn2 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    descriptionsSet = []
    outputSet = []
    tempTermRefSet = []
    tempTerm = []
    tempRef = []
    termcounter = 1
    # descriptions

    if cfg("SIMP_SNAP") == 1:
        if activeStatus == 1:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=1 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 3:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=?;"
        else:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=0 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    else:
        if activeStatus == 1:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=? and descriptionsUKET.active=1 and descriptionsUKET.UKeffectiveTime = (select max(d2.UKeffectiveTime) from descriptionsUKET d2 where descriptionsUKET.id=d2.id and d2.UKeffectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=? and descriptionsUKET.UKeffectiveTime = (select max(d2.UKeffectiveTime) from descriptionsUKET d2 where descriptionsUKET.id=d2.id and d2.UKeffectiveTime <= ?);"
        elif activeStatus == 3:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=?;"
        else:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=? and descriptionsUKET.active=0 and descriptionsUKET.UKeffectiveTime = (select max(d2.UKeffectiveTime) from descriptionsUKET d2 where descriptionsUKET.id=d2.id and d2.UKeffectiveTime <= ?);"
    if activeStatus == 3:
        cursorIn2.execute(queryString2, (inId,))
    else:
        cursorIn2.execute(queryString2, (inId, effectiveTime,))
    recordsetIn2 = cursorIn2.fetchall()
    for term in recordsetIn2:
            descriptionsSet.append(term)
    # definitions
    if activeStatus == 1:
        queryString2 = "select definitions.* from definitions where definitions.conceptId=? and definitions.active=1 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
    elif activeStatus == 2:
        queryString2 = "select definitions.* from definitions where definitions.conceptId=? and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
    elif activeStatus == 3:
        queryString2 = "select definitions.* from definitions where definitions.conceptId=?;"
    else:
        queryString2 = "select definitions.* from definitions where definitions.conceptId=? and definitions.active=0 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
    if activeStatus == 3:
        cursorIn3.execute(queryString2, (inId,))
    else:
        cursorIn3.execute(queryString2, (inId, effectiveTime,))
    recordsetIn2 = cursorIn3.fetchall()
    for term in recordsetIn2:
            descriptionsSet.append(term)
    descriptionsSet.sort(key=lambda x: (x[1], x[0]))
    for term in descriptionsSet:
        matchBool = False
        for row in outputSet:
            if term[0] == row[1][0][0]:
                row[1].append(term)
                matchBool = True
        if matchBool == False:
            tempTerm.append(term)
            if activeStatus == 1:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=1 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            elif activeStatus == 2:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            elif activeStatus == 3:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=?;"
            else:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            cursorIn1 = connectionIn3.cursor()
            if activeStatus == 3:
                cursorIn1.execute(queryString1, (term[0],))
            else:
                cursorIn1.execute(queryString1, (term[0], effectiveTime,))
            recordsetIn1 = cursorIn1.fetchall()
            recordsetIn1.sort(key=lambda x: (x[1], x[4]))
            for refSetRef in recordsetIn1:
                tempRef.append(refSetRef)
            tempTermRefSet.append(termcounter)
            tempTermRefSet.append(tempTerm[:])
            tempTermRefSet.append(tempRef[:])
            termcounter += 1
            outputSet.append(tempTermRefSet[:])
            del tempTermRefSet[:]
            del tempTerm[:]
            del tempRef[:]
    return outputSet
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()
    cursorIn3.close()
    connectionIn3.close()


def SV_SIMpTerm(inId, activeStatus, effectiveTime, refSets):  # ? activeStatus always 1 because effective PT has to be active
    if cfg("SIMP_SNAP") == 1:
        connectionIn2 = sqlite3.connect(fullPath)
    else:
        connectionIn2 = sqlite3.connect(UKOutPath)
    cursorIn2 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    returnSet = []
    if cfg("SIMP_SNAP") == 1:
        if activeStatus == 1:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=1 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 3:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=?;"
        else:
            queryString2 = "select descriptions.* from descriptions where descriptions.conceptId=? and descriptions.active=0 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
    else:
        if activeStatus == 1:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=? and descriptionsUKET.active=1 and descriptionsUKET.UKeffectiveTime = (select max(d2.UKeffectiveTime) from descriptionsUKET d2 where descriptionsUKET.id=d2.id and d2.UKeffectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=? and descriptionsUKET.UKeffectiveTime = (select max(d2.UKeffectiveTime) from descriptionsUKET d2 where descriptionsUKET.id=d2.id and d2.UKeffectiveTime <= ?);"
        elif activeStatus == 3:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=?;"
        else:
            queryString2 = "select descriptionsUKET.* from descriptionsUKET where descriptionsUKET.conceptId=? and descriptionsUKET.active=0 and descriptionsUKET.UKeffectiveTime = (select max(d2.UKeffectiveTime) from descriptionsUKET d2 where descriptionsUKET.id=d2.id and d2.UKeffectiveTime <= ?);"
    if activeStatus == 3:
        cursorIn2.execute(queryString2, (inId,))
    else:
        cursorIn2.execute(queryString2, (inId, effectiveTime,))
    recordsetIn2 = cursorIn2.fetchall()
    for term in recordsetIn2:
        if cfg("SIMP_SNAP") == 1:
            if activeStatus == 1:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=1 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            elif activeStatus == 2:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            elif activeStatus == 3:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=?;"
            else:
                queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
        else:
            if activeStatus == 1:
                queryString1 = "select crefsetuket.* from crefsetuket where refsetId in (" + refString + ") and referencedComponentId=? and crefsetuket.active=1 and crefsetuket.UKeffectiveTime = (select max(c2.UKeffectiveTime) from crefsetuket c2 where crefsetuket.id=c2.id and c2.UKeffectiveTime <= ?);"
            elif activeStatus == 2:
                queryString1 = "select crefsetuket.* from crefsetuket where refsetId in (" + refString + ") and referencedComponentId=? and crefsetuket.UKeffectiveTime = (select max(c2.UKeffectiveTime) from crefsetuket c2 where crefsetuket.id=c2.id and c2.UKeffectiveTime <= ?);"
            elif activeStatus == 3:
                queryString1 = "select crefsetuket.* from crefsetuket where refsetId in (" + refString + ") and referencedComponentId=?;"
            else:
                queryString1 = "select crefsetuket.* from crefsetuket where refsetId in (" + refString + ") and referencedComponentId=? and crefsetuket.active=0 and crefsetuket.UKeffectiveTime = (select max(c2.UKeffectiveTime) from crefsetuket c2 where crefsetuket.id=c2.id and c2.UKeffectiveTime <= ?);"
        cursorIn1 = connectionIn2.cursor()
        if activeStatus == 3:
            cursorIn1.execute(queryString1, (term[0],))
        else:
            cursorIn1.execute(queryString1, (term[0], effectiveTime,))
        recordsetIn1 = cursorIn1.fetchall()
        for row in recordsetIn1:
            if term[6] == '900000000000013009' and row[6] == '900000000000548007' and row[4] == '900000000000508004': #en-GB international.
                returnSet.append((3, term[7]))
            elif term[6] == '900000000000013009' and row[6] == '900000000000548007' and row[4] == '999001261000000100': #RDS clinical part
                returnSet.append((1, term[7]))
            elif term[6] == '900000000000013009' and row[6] == '900000000000548007' and row[4] == '999000691000001104': #RDS pharmacy part
                returnSet.append((1, term[7]))
            elif term[6] == '900000000000013009' and row[6] == '900000000000548007' and row[4] == '999001251000000103': #enGB UK clinical
                returnSet.append((2, term[7]))
            elif term[6] == '900000000000013009' and row[6] == '900000000000548007' and row[4] == '999000681000001101': #enGB UK pharmacy
                returnSet.append((2, term[7]))
            elif term[6] == '900000000000013009' and row[6] == '900000000000548007' and row[4] == '900000000000509007': #en-US international
                returnSet.append((4, term[7]))
    returnSet.sort(key=lambda x: x[0])
    if len(returnSet) > 0:
        return returnSet[0][1]
    else:
        return pterm(inId) + " *"
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()


def SV_MDRterms(inId, activeStatus, effTimeSet, refSets):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    descriptionsSet = []
    outputSet = []
    tempTermRefSet = []
    tempTerm = []
    tempRef = []
    termcounter = 1
    # descriptions
    for row in effTimeSet:
        if activeStatus == 1:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.active=1 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        else:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.active=0 and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        cursorIn2.execute(queryString2, (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        for term in recordsetIn2:
            descriptionsSet.append(term)
    # definitions
    for row in effTimeSet:
        if activeStatus == 1:
            queryString2 = "select definitions.* from definitions where definitions.moduleId = ? and definitions.conceptId=? and definitions.active=1 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select definitions.* from definitions where definitions.moduleId = ? and definitions.conceptId=? and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        else:
            queryString2 = "select definitions.* from definitions where definitions.moduleId = ? and definitions.conceptId=? and definitions.active=0 and definitions.effectiveTime = (select max(d2.effectiveTime) from definitions d2 where definitions.id=d2.id and d2.effectiveTime <= ?);"
        cursorIn2.execute(queryString2, (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        for term in recordsetIn2:
            descriptionsSet.append(term)
    descriptionsSet.sort(key=lambda x: (x[1], x[0]))
    for term in descriptionsSet:
        matchBool = False
        for row in outputSet:
            if term[0] == row[1][0][0]:
                row[1].append(term)
                matchBool = True
        if matchBool == False:
            tempTerm.append(term)
            for row in effTimeSet:
                if activeStatus == 1:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and crefset.moduleId = ? and referencedComponentId=? and crefset.active=1 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                elif activeStatus == 2:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and crefset.moduleId = ? and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                else:
                    queryString1 = "select crefset.* from crefset where refsetId in (" + refString + ") and crefset.moduleId = ? and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
                cursorIn1.execute(queryString1, (row[0], term[0], row[1]))
                recordsetIn1 = cursorIn1.fetchall()
                recordsetIn1.sort(key=lambda x: (x[1], x[4]))
                for refSetRef in recordsetIn1:
                    tempRef.append(refSetRef)
            tempTermRefSet.append(termcounter)
            tempTermRefSet.append(tempTerm[:])
            tempTermRefSet.append(tempRef[:])
            termcounter += 1
            outputSet.append(tempTermRefSet[:])
            del tempTermRefSet[:]
            del tempTerm[:]
            del tempRef[:]
    return outputSet
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()


def SV_MDRpTerm(inId, activeStatus, effTimeSet, refSets): # ? activeStatus always 1 because effective PT has to be active
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    refString = ', '.join(map(str, refSets))
    descriptionsSet = []
    outputSet = []
    tempTermRefSet = []
    tempTerm = []
    tempRef = []
    termcounter = 1
    returnSet = []
    for row in effTimeSet:
        if activeStatus == 1:
            queryString2 = "select descriptions.* from descriptions where descriptions.active=1 and descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        elif activeStatus == 2:
            queryString2 = "select descriptions.* from descriptions where descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        else:
            queryString2 = "select descriptions.* from descriptions where descriptions.active=1 and descriptions.moduleId = ? and descriptions.conceptId=? and descriptions.effectiveTime = (select max(d2.effectiveTime) from descriptions d2 where descriptions.id=d2.id and d2.effectiveTime <= ?);"
        cursorIn2.execute(queryString2, (row[0], inId, row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        for term in recordsetIn2:
            descriptionsSet.append(term)
    for term in descriptionsSet:
        tempTerm.append(term)
        for row in effTimeSet:
            if activeStatus == 1:
                queryString1 = "select crefset.* from crefset where crefset.active=1 and crefset.moduleId = ? and refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            elif activeStatus == 2:
                queryString1 = "select crefset.* from crefset where crefset.moduleId = ? and refsetId in (" + refString + ") and referencedComponentId=? and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            else:
                queryString1 = "select crefset.* from crefset where crefset.active=1 and crefset.moduleId = ? and refsetId in (" + refString + ") and referencedComponentId=? and crefset.active=0 and crefset.effectiveTime = (select max(c2.effectiveTime) from crefset c2 where crefset.id=c2.id and c2.effectiveTime <= ?);"
            cursorIn1.execute(queryString1, (row[0], term[0], row[1]))
            recordsetIn1 = cursorIn1.fetchall()
            for refSetRef in recordsetIn1:
                tempRef.append(refSetRef)
        tempTermRefSet.append(termcounter)
        tempTermRefSet.append(tempTerm[:])
        tempTermRefSet.append(tempRef[:])
        termcounter += 1
        outputSet.append(tempTermRefSet[:])
        del tempTermRefSet[:]
        del tempTerm[:]
        del tempRef[:]
    for row in outputSet:
        for rsEntry in row[2]:
            if row[1][0][6] == '900000000000013009' and rsEntry[6] == '900000000000548007' and rsEntry[4] == '900000000000508004': #en-GB international.
                returnSet.append((3, row[1][0][7]))
            elif row[1][0][6] == '900000000000013009' and rsEntry[6] == '900000000000548007' and rsEntry[4] == '999001261000000100': #RDS clinical part
                returnSet.append((1, row[1][0][7]))
            elif row[1][0][6] == '900000000000013009' and rsEntry[6] == '900000000000548007' and rsEntry[4] == '999000691000001104': #RDS pharmacy part
                returnSet.append((1, row[1][0][7]))
            elif row[1][0][6] == '900000000000013009' and rsEntry[6] == '900000000000548007' and rsEntry[4] == '999001251000000103': #enGB UK clinical
                returnSet.append((2, row[1][0][7]))
            elif row[1][0][6] == '900000000000013009' and rsEntry[6] == '900000000000548007' and rsEntry[4] == '999000681000001101': #enGB UK pharmacy
                returnSet.append((2, row[1][0][7]))
            elif row[1][0][6] == '900000000000013009' and rsEntry[6] == '900000000000548007' and rsEntry[4] == '900000000000509007': #en-US international
                returnSet.append((4, row[1][0][7]))
    returnSet.sort(key=lambda x: x[0])
    if len(returnSet) > 0:
        return returnSet[0][1]
    else:
        return pterm(inId) + " *"
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()


def SV_SIMancestorsFromConId_IN(inId):
    ancestorList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    graph = Graph(password="neo")
    ET = str(cfg("EFFECTIVE_TIME"))
    CON_ID = inId
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[rels:REL*]->(sup:con {cid:'" + tok('ROOT') + "'}) WHERE ALL(r IN rels WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], SV_pterm(str(node['cid'])))
        ancestorList.append(tempSet)
    ancestorList.sort(key=lambda x: x[1])
    return ancestorList


def SV_SIMFindAncestorsForConIdAndET(CON_ID, ET):
    graph = Graph(password="neo")
    ancestorList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[rels:REL*]->(sup:con {cid:'" + tok('ROOT') + "'}) WHERE ALL(r IN rels WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], pterm(str(node['cid'])))
        ancestorList.append(tempSet)
    return ancestorList


def SV_SIMdescendantsFromConId_IN(inId):
    descendantList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    graph = Graph(password="neo")
    ET = str(cfg("EFFECTIVE_TIME"))
    CON_ID = inId
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con)-[rels:REL*]->(sup:con {cid:'" + CON_ID + "'}) WHERE ALL(r IN rels WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], SV_pterm(str(node['cid'])))
        descendantList.append(tempSet)
    descendantList.sort(key=lambda x: x[1])
    return descendantList


def SV_SIMFindDescendantsForConIdAndET(CON_ID, ET):
    graph = Graph(password="neo")
    descendantList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con)-[rels:REL*]->(sup:con {cid:'" + CON_ID + "'}) WHERE ALL(r IN rels WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], pterm(str(node['cid'])))
        descendantList.append(tempSet)
    return descendantList


def SV_SIMcheckSimpleRefset(SUB_ID):
    # seed ET
    print "\033c"  # print violation
    ET = "20020131"
    SUP_ID = '446609009'
    graph = Graph(password="neo")
    print 'Checking ' + SUB_ID, pterm(SUB_ID) + ' as a subtype of ' + SUP_ID, pterm(SUP_ID) + '...\n'  # print violation
    while ET < time.strftime("%Y%m%d"):
        pathList = []
        queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + SUB_ID + "'})-[rels:REL*]->(sup:con {cid:'" + SUP_ID + "'}) WHERE ALL(r IN rels WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
        mydata = graph.data(queryString)
        for item in mydata:
            pathList.append(item['path'])
        if len(pathList) > 0:
            print "yes, it's a refset!'"
            return True
        ET = upByNMonths(ET, 6)
    print "no, it's never been a refset!'"
    return False


def SV_SIMC1_IsA_C2_Compare(C1, C2, testCon, ET):
    graph = Graph(password="neo")
    pathList = []
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + C1 + "'})-[rels:REL*]->(sup:con {cid:'" + C2 + "'}) WHERE ALL(r IN rels WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path LIMIT 1")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    if len(pathList) > 0:
        return '+'
    else:
        if testCon == 1:  # if it's C1 which might have moved, changed status or not been there at all...'
            if len(SV_SIMconMetaFromConIdSimple_IN(C1, ET)) == 1:
                if SV_SIMconMetaFromConIdSimple_IN(C1, ET)[0][1] == 1:
                    return '.'
                else:
                    return '--'
            else:
                return ' '
        else:  # or C2...
            if len(SV_SIMconMetaFromConIdSimple_IN(C2, ET)) == 1:
                if SV_SIMconMetaFromConIdSimple_IN(C2, ET)[0][1] == 1:
                    return '.'
                else:
                    return '--'
            else:
                return ' '


def SV_SIMrefSetFromConIdPlusMeta_IN(id, refsetType, activeFlag, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    if activeFlag == 1:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=1 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(id), ET))
    elif activeFlag == 2:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(id), ET))
    elif activeFlag == 3:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where referencedComponentId= ?", (str(id),))
    else:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=0 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(id), ET))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = (refSet[1], SV_pterm(refSet[1]), refSet[0], pterm(refSet[0]), refSet[2], refSet[3])
        refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def SV_MDRrefSetFromConIdPlusMeta_IN(id, refsetType, activeFlag, effTimeSet):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    refSetList = []
    for row in effTimeSet:
        if activeFlag == 0:
            cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=0 and moduleId = ? and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], str(id), row[1]))
        elif activeFlag == 2:
            cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where and moduleId = ? referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], str(id), row[1]))
        else:
            cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=1 and moduleId = ? and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (row[0], str(id), row[1]))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for refSet in recordsetIn2:
            tempSet = (refSet[1], SV_pterm(refSet[1]), refSet[0], pterm(refSet[0]), refSet[2], refSet[3])
            refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def SV_pterm(inId):
    global RDSParts
    global otherLangSet
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    if (cfg("PTERM_TYPE") == 0) or (cfg("QUERY_TYPE") == 1):  # if instructed to be latest snapshot, or if looking for all data
        return pterm(inId)
    else:
        if cfg("QUERY_TYPE") == 2:  # if simple snapshot
            return SV_SIMpTerm(inId[0], 1, cfg('EFFECTIVE_TIME'), (RDSParts + otherLangSet))
        else:  # if MDR snapshot
            return SV_MDRpTerm(inId[0], 1, effTimeSet(), (RDSParts + otherLangSet))


def findSeedModules(seedTime):
    moduleList = []
    connectionIn1 = sqlite3.connect(fullPath)
    cursorIn1 = connectionIn1.cursor()
    queryString = "select distinct moduleId from ssrefset where ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.effectiveTime <= ?);"
    cursorIn1.execute(queryString, (seedTime,))
    recordsetIn1 = cursorIn1.fetchall()
    for row in recordsetIn1:
        moduleList.append(str(row[0]))
    return moduleList


def cypherWrite(inString):
    try:
        f = open('files/cypher_sv.txt', 'w')
        f.write(inString)
    finally:
        f.close()
    return inString
