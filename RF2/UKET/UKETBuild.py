import sqlite3


def UKConcepts():
    connectionIn2 = sqlite3.connect('RF2Full.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("attach 'RF2UKOut.db' as RF2UKOut")
    cursorIn1.execute("DROP TABLE RF2UKOut.ConceptsUKET")
    cursorIn1.execute("CREATE TABLE RF2UKOut.ConceptsUKET(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, definitionStatusId TEXT, UKeffectiveTime TEXT);")
    cursorIn2.execute("select concepts.* from concepts")
    recordsetIn2 = cursorIn2.fetchall()
    tot = len(recordsetIn2)
    print tot
    progress = 1
    for row in recordsetIn2:
        if row[3] == '900000000000207008' or row[3] == '900000000000012004' or row[3] == '449080006':
            cursorIn3.execute("select string1 from ssrefset where referencedComponentId=? and string2=? and moduleId='999000011000000103'", (row[3], row[1]))
            rsrow = cursorIn3.fetchone()
            if rsrow:
                if (progress % 500 == 0):
                    print '>>', row[0], row[1], row[3], rsrow[0]
                    print '>>', progress, "/", tot
                cursorIn1.execute("insert into RF2UKOut.ConceptsUKET values (?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], rsrow[0]))
            else:
                cursorIn1.execute("insert into RF2UKOut.ConceptsUKET values (?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[1]))
        else:
            cursorIn1.execute("insert into RF2UKOut.ConceptsUKET values (?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[1]))
        if (progress % 500 == 0):
            print '<<', row[0], row[1], row[3], row[1]
            print '<<', progress, "/", tot
            connectionIn2.commit()
        progress += 1
    # cleanup commit
    connectionIn2.commit()
    cursorIn1.close()
    cursorIn3.close()
    cursorIn2.close()
    connectionIn2.close()


def UKConceptsSimplest():
    connectionIn2 = sqlite3.connect('RF2Full.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("attach 'RF2UKOut.db' as RF2UKOut")
    cursorIn1.execute("DROP TABLE RF2UKOut.ConceptsUKET2")
    cursorIn1.execute("CREATE TABLE RF2UKOut.ConceptsUKET2(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, definitionStatusId TEXT, UKeffectiveTime TEXT);")
    cursorIn2.execute("select concepts.* from concepts")
    recordsetIn2 = cursorIn2.fetchall()
    # feedback
    tot = len(recordsetIn2)
    print tot
    progress = 1
    # feedback
    for row in recordsetIn2:
        cursorIn3.execute("select string1 from ssrefset where referencedComponentId=? and string2=? and moduleId='999000011000000103'", (row[3], row[1]))
        rsrow = cursorIn3.fetchone()
        if rsrow:
            cursorIn1.execute("insert into RF2UKOut.ConceptsUKET2 values (?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], rsrow[0]))
        else:
            cursorIn1.execute("insert into RF2UKOut.ConceptsUKET2 values (?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[1]))
        # feedback
        if (progress % 500 == 0):
            print '<<', row[0], row[1], row[3], row[1]
            print '<<', progress, "/", tot
            connectionIn2.commit()
        progress += 1
        # feedback
        # connectionIn2.commit()
    # cleanup commit
    connectionIn2.commit()
    cursorIn1.close()
    cursorIn3.close()
    cursorIn2.close()
    connectionIn2.close()


def UKRelationships():
    connectionIn2 = sqlite3.connect('RF2Full.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("attach 'RF2UKOut.db' as RF2UKOut")
    cursorIn1.execute("DROP TABLE RF2UKOut.RelationshipsUKET")
    cursorIn1.execute("CREATE TABLE RF2UKOut.RelationshipsUKET(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, sourceId TEXT, destinationId TEXT, relationshipGroup INTEGER, typeId TEXT, characteristicTypeId TEXT, modifierId TEXT, UKeffectiveTime TEXT);")
    for n in range(0, 10):
        cursorIn2.execute("select relationships.* from relationships where id like '%" + str(n) + "'")
        recordsetIn2 = cursorIn2.fetchall()
        tot = len(recordsetIn2)
        print tot
        progress = 1
        for row in recordsetIn2:
            if row[3] == '900000000000207008' or row[3] == '900000000000012004' or row[3] == '449080006':
                cursorIn3.execute("select string1 from ssrefset where referencedComponentId=? and string2=? and moduleId='999000011000000103'", (row[3], row[1]))
                rsrow = cursorIn3.fetchone()
                if rsrow:
                    if (progress % 500 == 0):
                        print '>>', row[0], row[1], row[3], rsrow[0]
                        print '>>', progress, "/", tot
                    cursorIn1.execute("insert into RF2UKOut.RelationshipsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9],  rsrow[0]))
                else:
                    cursorIn1.execute("insert into RF2UKOut.RelationshipsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4],row[5], row[6], row[7], row[8], row[9], row[1]))
            else:
                cursorIn1.execute("insert into RF2UKOut.RelationshipsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[1]))
            if (progress % 500 == 0):
                print '<<', row[0], row[1], row[3], row[1]
                print '<<', progress, "/", tot
                connectionIn2.commit()
            progress += 1
    # cleanup commit
    connectionIn2.commit()
    cursorIn1.close()
    cursorIn3.close()
    cursorIn2.close()
    connectionIn2.close()


def UKDescriptions():
    connectionIn2 = sqlite3.connect('RF2Full.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("attach 'RF2UKOut.db' as RF2UKOut")
    cursorIn1.execute("DROP TABLE RF2UKOut.DescriptionsUKET")
    cursorIn1.execute("CREATE TABLE RF2UKOut.DescriptionsUKET(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT, UKeffectiveTime TEXT);")
    for n in range(0, 10):
        cursorIn2.execute("select descriptions.* from descriptions where id like '%" + str(n) + "'")
        recordsetIn2 = cursorIn2.fetchall()
        tot = len(recordsetIn2)
        print tot
        progress = 1
        for row in recordsetIn2:
            if row[3] == '900000000000207008' or row[3] == '900000000000012004' or row[3] == '449080006':
                cursorIn3.execute("select string1 from ssrefset where referencedComponentId=? and string2=? and moduleId='999000011000000103'", (row[3], row[1]))
                rsrow = cursorIn3.fetchone()
                if rsrow:
                    if (progress % 500 == 0):
                        print '>>', row[0], row[1], row[3], rsrow[0]
                        print '>>', progress, "/", tot
                    cursorIn1.execute("insert into RF2UKOut.DescriptionsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], rsrow[0]))
                else:
                    cursorIn1.execute("insert into RF2UKOut.DescriptionsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4],row[5], row[6], row[7], row[8], row[1]))
            else:
                cursorIn1.execute("insert into RF2UKOut.DescriptionsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[1]))
            if (progress % 500 == 0):
                print '<<', row[0], row[1], row[3], row[1]
                print '<<', progress, "/", tot
                connectionIn2.commit()
            progress += 1
    # cleanup commit
    connectionIn2.commit()
    cursorIn1.close()
    cursorIn3.close()
    cursorIn2.close()
    connectionIn2.close()


def UKDefinitions():
    connectionIn2 = sqlite3.connect('RF2Full.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("attach 'RF2UKOut.db' as RF2UKOut")
    cursorIn1.execute("DROP TABLE RF2UKOut.DefinitionsUKET")
    cursorIn1.execute("CREATE TABLE RF2UKOut.DefinitionsUKET(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT, UKeffectiveTime TEXT);")
    for n in range(0, 10):
        cursorIn2.execute("select definitions.* from definitions where id like '%" + str(n) + "'")
        recordsetIn2 = cursorIn2.fetchall()
        tot = len(recordsetIn2)
        print tot
        progress = 1
        for row in recordsetIn2:
            if row[3] == '900000000000207008' or row[3] == '900000000000012004' or row[3] == '449080006':
                cursorIn3.execute("select string1 from ssrefset where referencedComponentId=? and string2=? and moduleId='999000011000000103'", (row[3], row[1]))
                rsrow = cursorIn3.fetchone()
                if rsrow:
                    if (progress % 500 == 0):
                        print '>>', row[0], row[1], row[3], rsrow[0]
                        print '>>', progress, "/", tot
                    cursorIn1.execute("insert into RF2UKOut.DefinitionsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], rsrow[0]))
                else:
                    cursorIn1.execute("insert into RF2UKOut.DefinitionsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4],row[5], row[6], row[7], row[8], row[1]))
            else:
                cursorIn1.execute("insert into RF2UKOut.DefinitionsUKET values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[1]))
            if (progress % 500 == 0):
                print '<<', row[0], row[1], row[3], row[1]
                print '<<', progress, "/", tot
                connectionIn2.commit()
            progress += 1
    # cleanup commit
    connectionIn2.commit()
    cursorIn1.close()
    cursorIn3.close()
    cursorIn2.close()
    connectionIn2.close()


def UKSimpleRefset():
    connectionIn2 = sqlite3.connect('RF2Full.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("attach 'RF2UKOut.db' as RF2UKOut")
    cursorIn1.execute("DROP TABLE RF2UKOut.SimpleRefsetUKET")
    cursorIn1.execute("CREATE TABLE RF2UKOut.SimpleRefsetUKET(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, UKeffectiveTime TEXT);")
    for n in range(0, 10):
        cursorIn2.execute("select simplerefset.* from simplerefset where referencedComponentId like '%" + str(n) + "'")
        recordsetIn2 = cursorIn2.fetchall()
        tot = len(recordsetIn2)
        print tot
        progress = 1
        for row in recordsetIn2:
            if row[3] == '900000000000207008' or row[3] == '900000000000012004' or row[3] == '449080006':
                cursorIn3.execute("select string1 from ssrefset where referencedComponentId=? and string2=? and moduleId='999000011000000103'", (row[3], row[1]))
                rsrow = cursorIn3.fetchone()
                if rsrow:
                    if (progress % 500 == 0):
                        print '>>', row[0], row[1], row[3], rsrow[0]
                        print '>>', progress, "/", tot
                    cursorIn1.execute("insert into RF2UKOut.SimpleRefsetUKET values (?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], rsrow[0]))
                else:
                    cursorIn1.execute("insert into RF2UKOut.SimpleRefsetUKET values (?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4],row[5], row[1]))
            else:
                cursorIn1.execute("insert into RF2UKOut.SimpleRefsetUKET values (?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[1]))
            if (progress % 500 == 0):
                print '<<', row[0], row[1], row[3], row[1]
                print '<<', progress, "/", tot
                connectionIn2.commit()
            progress += 1
    # cleanup commit
    connectionIn2.commit()
    cursorIn1.close()
    cursorIn3.close()
    cursorIn2.close()
    connectionIn2.close()


def UKcRefset():
    connectionIn2 = sqlite3.connect('RF2Full.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("attach 'RF2UKOut.db' as RF2UKOut")
    cursorIn1.execute("DROP TABLE RF2UKOut.cRefsetUKET")
    cursorIn1.execute("CREATE TABLE RF2UKOut.cRefsetUKET(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, componentId1 TEXT, UKeffectiveTime TEXT);")
    for n in range(0, 10):
        cursorIn2.execute("select crefset.* from crefset where referencedComponentId like '%" + str(n) + "'")
        recordsetIn2 = cursorIn2.fetchall()
        tot = len(recordsetIn2)
        print tot
        progress = 1
        for row in recordsetIn2:
            if row[3] == '900000000000207008' or row[3] == '900000000000012004' or row[3] == '449080006':
                cursorIn3.execute("select string1 from ssrefset where referencedComponentId=? and string2=? and moduleId='999000011000000103'", (row[3], row[1]))
                rsrow = cursorIn3.fetchone()
                if rsrow:
                    if (progress % 500 == 0):
                        print '>>', row[0], row[1], row[3], rsrow[0]
                        print '>>', progress, "/", tot
                    cursorIn1.execute("insert into RF2UKOut.cRefsetUKET values (?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], rsrow[0]))
                else:
                    cursorIn1.execute("insert into RF2UKOut.cRefsetUKET values (?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4],row[5], row[6], row[1]))
            else:
                cursorIn1.execute("insert into RF2UKOut.cRefsetUKET values (?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[1]))
            if (progress % 500 == 0):
                print '<<', row[0], row[1], row[3], row[1]
                print '<<', progress, "/", tot
                connectionIn2.commit()
            progress += 1
    # cleanup commit
    connectionIn2.commit()
    cursorIn1.close()
    cursorIn3.close()
    cursorIn2.close()
    connectionIn2.close()

UKConcepts()
UKRelationships()
UKDescriptions()
UKDefinitions()
UKSimpleRefset()
UKcRefset()
# UKConceptsSimplest()
