def tok(inString):  # short name for typing convenience - setConfig reallly
    f = open('files/SCTRF2SVtoken.txt', 'r')
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = tempList[1]
    f.close()
    return returnValue


def cfg(inString):  # short name for typing convenience - setConfig reallly
    f = open('files/SCTRF2SVUKETconfig.cfg', 'r')
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = int(tempList[1])
    f.close()
    return returnValue

def pth(inString):  # short name for typing convenience - setConfig really
    f = open('files/SCTRF2datapaths.cfg', 'r')
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = tempList[1]
    f.close()
    return returnValue

def cfgWrite(inString, inValue):
    writeToList = []
    try:
        f = open('files/SCTRF2SVUKETconfig.cfg', 'r+')
        lines = f.readlines()
        for line in lines:
            tempLine = line.strip()
            if not line[0] == "#":
                tempList = tempLine.split('|')
                if tempList[0] == inString:
                    writeToList.append(tempList[0] + '|' + str(inValue))
                else:
                    writeToList.append(tempLine)
            else:
                writeToList.append(tempLine)
        #print writeToList
        f.seek(0)
        for entry in writeToList:
            f.write(entry + '\n')
    finally:
        f.close()




##param = 'SEARCH_TYPE'
##a = getConfig(param)
##print a, param
##param = 'SEARCH_ORDER'
##a = getConfig(param)
##print a, param