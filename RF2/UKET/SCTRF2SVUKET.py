#!/usr/bin/env python

from SCTRF2SVUKETFuncs import *
from SCTRF2SVconfig import *
import os
import subprocess
import sys

def startNeo():
    cwdOld = os.getcwd()
    # print cwdOld
    # path = '/home/ed/N4J9/neo4j-community-3.1.1'
    path = pth('NEO4J')
    os.chdir( path )
    #cwdNew = os.getcwd()
    #print cwdNew
    #os.system("ls")
    subprocess.call(["./bin/neo4j", "start"])
    os.chdir( cwdOld )

def stopNeo():
    cwdOld = os.getcwd()
    # print cwdOld
    # path = '/home/ed/N4J9/neo4j-community-3.1.1'
    path = pth('NEO4J')
    os.chdir( path )
    #cwdNew = os.getcwd()
    #print cwdNew
    #os.system("ls")
    subprocess.call(["./bin/neo4j", "stop"])
    os.chdir( cwdOld )


def browseFromRootCall():
    if not debug:
        print "\033c"
    if debug:
        print '-> in browseFromRootCall'
    snapshotHeader()
    callId = (tok('ROOT'),)
    focusWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString


def showBreadcrumbs():
    if not debug:
        print "\033c"
    if debug:
        print '-> in showBreadcrumbs'
    returnString = breadcrumbsRead()
    if debug:
        print returnString
    return returnString


def browseFromIndexCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> in browseFromIndexCall'
    snapshotHeader()
    callId = inString
    if isinstance(callId, str):
        callId = listify(callId.strip())
    breadcrumbsWrite(callId[0])
    focusWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString


def showAncestorsCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> inancestorsCall'
    snapshotHeader()
    callId = inString
    returnString = ancestorsFromConId(callId)
    if debug:
        print returnString
    return returnString


def showDescendantsCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> indescendantsCall'
    snapshotHeader()
    callId = inString
    returnString = descendantsFromConId(callId)
    if debug:
        print returnString
    return returnString


def ancestorsToXLSXCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> inancestorsToXLSX'
    snapshotHeader()
    callId = inString
    returnString = ALLancestorsToXLSX(callId)
    if debug:
        print returnString
    return returnString


def descendantsToXLSXCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> indescendantsToXLSX'
    snapshotHeader()
    callId = inString
    returnString = ALLdescendantsToXLSX(callId)
    if debug:
        print returnString
    return returnString


def inputDateThresholdCall():
    if not debug:
        print "\033c"
    if debug:
        print '-> inputDateThresholdCall'
    returnString = setEffectiveTime()
    if not debug:
        print "\033c"
    snapshotHeader()
    if debug:
        print returnString
    returnString = readInFromFocus()
    return returnString


def importFromTerminalCall():
    if not debug:
        print "\033c"
    if debug:
        print '-> importFromTerminalCall'
    snapshotHeader()
    returnString = readInFromTerminal()
    if debug:
        print returnString
    return returnString


def queryTypeCall(inInteger):
    if not debug:
        print "\033c"
    if debug:
        print '-> queryTypeCall', str(inInteger)
    if inInteger == 4: #  uket
        returnString = setSimpleSnap(2)
        returnString = setQueryType(2)
    elif inInteger == 2: #  international et
        returnString = setSimpleSnap(1)
        returnString = setQueryType(2)
    else:
        returnString = setQueryType(inInteger)
    if debug:
        print returnString
    snapshotHeader()
    returnString = readInFromFocus()
    return returnString


def showTypeCall(inInteger):
    if not debug:
        print "\033c"
    if debug:
        print '-> showTypeCall', str(inInteger)
    returnString = setShowType(inInteger)
    if debug:
        print returnString
    snapshotHeader()
    returnString = readInFromFocus()
    return returnString


def showMetadataCall(inInteger):
    if not debug:
        print "\033c"
    if debug:
        print '-> showTypeCall', str(inInteger)
    snapshotHeader()
    returnString = setShowMetadata(inInteger)
    if debug:
        print returnString
    returnString = readInFromFocus()
    return returnString


def ptermControlCall(inInteger):
    if not debug:
        print "\033c"
    if debug:
        print '-> ptermControlCall', str(inInteger)
    returnString = setPtermControl(inInteger)
    snapshotHeader()
    if debug:
        print returnString
    returnString = readInFromFocus()
    return returnString


def subShowCall(inInteger):
    if not debug:
        print "\033c"
    if debug:
        print '-> subShowCall', str(inInteger)
    returnString = setSubShow(inInteger)
    snapshotHeader()
    if debug:
        print returnString
    returnString = readInFromFocus()
    return returnString


def showTermsMetaCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> inshowTermsMetaCall'
    callId = inString
    snapshotHeader()
    returnString = allTermsFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString


def showRolesMetaCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> inshowRolesMetaCall'
    callId = inString
    snapshotHeader()
    returnString = groupedRolesFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString


def firstTimeCall():
    if not debug:
        print "\033c"
    if debug:
        print '-> firstTimeCall'
    print 'Most recent snapshot:\n'
    snapshotHeader()
    returnString = readInFromTerminal()
    if debug:
        print returnString
    return returnString


def importFromWindowCall():
    if not debug:
        print "\033c"
    if debug:
        print '-> importFromWindowCall'
    snapshotHeader()
    returnString = readInFromWindow()
    if debug:
        print returnString
    return returnString


def showRefsetsMetaCall(inString):
    if not debug:
        print "\033c"
    if debug:
        print '-> inshowRefsetsMetaCall'
    callId = inString
    snapshotHeader()
    returnString = refSetFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString


returnString = {}
debug = False
sys.stdout.write("\x1b]2;SCT Full Data Browser [UKET]\x07")
print "\033c"
# main control code
firstTime = True
startNeo()
while True:
    if firstTime:
        topaction = "Z"
        firstTime = False
    else:
        topaction = raw_input('Action:\n\
[a] Browse from root\t[b] Import terminal\t[c] Import window\n\
[d] Show all data\t[e] Simple snap (1/2)\t[f] MDR snapshot\n\
[g] Show active data\t[h] Show active+inac.\t[i] Show inactive data\n\
[j] Breadcrumbs   \t[k] Show metadata (1/2)\t[l] Hide metadata\n\
[m] Latest terms\t[n] Snapshot terms\t[o] Indicate subtypes\n\
[p] Hide subtypes\t[q] (+row) Term+meta\t[r] (+row) Role+meta\n\
[s]*(+row) Refs+meta\t[t] (+row) Ancestors\t[u] (+row) Descendants\n\
[v] (+row) Ances->xlsx\t[w] (+row) Desc->xlsx\t[x] (+row) Refsets\n\
[y] Input effecTime\n\
[zz] Exit\n\
Input selection:').strip()
    if topaction == 'zz':
        if debug:
            print returnString
        print "\033c"
        stopNeo()
        break  # breaks out of the while loop and ends session
    elif topaction == 'a':
        if debug:
            print returnString
            print '-> calling browseFromRootCall'
        returnString = browseFromRootCall()
    elif topaction == 'b':
        if debug:
            print returnString
            print '-> calling importFromTerminal'
        returnString = importFromTerminalCall()
    elif topaction == 'c':
        if debug:
            print returnString
            print '-> calling importFromWindowCall'
        returnString = importFromWindowCall()
    elif topaction == 'd':
        if debug:
            print returnString
            print '-> calling queryTypeCall - full'
        returnString = queryTypeCall(1)
    elif topaction == 'e1':
        if debug:
            print returnString
            print '-> calling queryTypeCall - simple snapshot'
        returnString = queryTypeCall(2)
    elif topaction == 'e2':
        if debug:
            print returnString
            print '-> calling queryTypeCall - simple snapshot'
        returnString = queryTypeCall(4)
    elif topaction == 'f':
        if debug:
            print returnString
            print '-> calling queryTypeCall - MDR snapshot'
        returnString = queryTypeCall(3)
    elif topaction == 'g':
        if debug:
            print returnString
            print '-> calling showTypeCall - active'
        returnString = showTypeCall(1)
    elif topaction == 'h':
        if debug:
            print returnString
            print '-> calling showTypeCall - both'
        returnString = showTypeCall(2)
    elif topaction == 'i':
        if debug:
            print returnString
            print '-> calling showTypeCall - inactive'
        returnString = showTypeCall(0)
    elif topaction == 'j':
        if debug:
            print returnString
            print '-> calling showBreadcrumbs'
        returnString = showBreadcrumbs()
    elif topaction == 'l':
        if debug:
            print returnString
            print '-> calling showMetadataCall - hide'
        returnString = showMetadataCall(0)
    elif topaction == 'm':
        if debug:
            print returnString
            print '-> calling ptermControlCall - latest'
        returnString = ptermControlCall(0)
    elif topaction == 'n':
        if debug:
            print returnString
            print '-> calling ptermControlCall - calculated'
        returnString = ptermControlCall(1)
    elif topaction == 'o':
        if debug:
            print returnString
            print '-> calling subShowCall - show'
        returnString = subShowCall(1)
    elif topaction == 'p':
        if debug:
            print returnString
            print '-> calling subShowlCall - hide'
        returnString = subShowCall(0)
    elif topaction == 'y':
        if debug:
            print returnString
            print '-> calling inputDateThreshold'
        returnString = inputDateThresholdCall()
    elif topaction == 'Z':
        if debug:
            print returnString
            print '-> calling firstTimeCall'
        returnString = browseFromRootCall()
    elif topaction == 'x':  # toggle *most* debug outputs
        if debug:
            debug = False
            print 'debug OFF'
        else:
            debug = True
            print 'debug ON'
    else:
        try:
            if topaction[0] == 'k':
                if debug:
                    print returnString
                    print '-> calling showMetadataCall - show'
                returnString = showMetadataCall(int(topaction[1:]))
            if topaction[0] == 'q':
                if debug:
                    print 'q', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showTermsMetaCall(callThing[0])
            if topaction[0] == 'r':
                if debug:
                    print 'r', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRolesMetaCall(callThing[0])
            if topaction[0] == 't':
                if debug:
                    print 't', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showAncestorsCall(callThing[0])
            if topaction[0] == 'u':
                if debug:
                    print 'u', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showDescendantsCall(callThing[0])
            if topaction[0] == 'v':
                if debug:
                    print 'v', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = ancestorsToXLSXCall(callThing[0])
            if topaction[0] == 'w':
                if debug:
                    print 'w', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = descendantsToXLSXCall(callThing[0])
            if topaction[0] == 'x':
                if debug:
                    print 'x', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetsMetaCall(callThing[0])
            if debug:
                print ' ', returnString[int(topaction)]
            returnString = browseFromIndexCall(returnString[int(topaction)])
            True
        except:
            True


##if __name__ == '__main__':
##	print 'TCCall is being run by itself'
##else:
##	print 'TCCall being imported from another module'
