from __future__ import print_function, unicode_literals
import io
import shutil
import time
import os
from SCTRF2config import pth, tok
from platformSpecific import cfgPathInstall, sep

path = pth('BASIC_DATA_BUILD') + 'Out/'
codePath = cfgPathInstall()
trimpath = path[:-1]
w = "Snapshot"
LAN = 1 # 1 if doing NHS RLR build, 2 if doing interim International build, 3 if doing spanish
# global search for '# import addn' tags to update

timestr = time.strftime("%Y%m%d-%H%M%S")
shutil.copy2(codePath + 'SCTRF2Refset.py', codePath + 'SCTRF2Refset_' + timestr + '.py')

files = os.listdir(trimpath)                                  # open database

def uniq(lst):
    last = object()
    for item in lst:
        if item == last:
            continue
        yield item
        last = item

def sort_and_deduplicate(l):
    return list(uniq(sorted(l, reverse=False)))

# read back refsetTypes to preserve last set (allows swapping between UK and interim international)
text_file_types = io.open(codePath + "files" + sep() + "build" + sep() + "refsetTypes.txt", "r", encoding="UTF-8")
patternList = []
for line in text_file_types:
    patternList.append(line.strip())
patternList = sort_and_deduplicate(patternList)
text_file_types.close()

text_file_types = io.open(codePath + "files" + sep() + "build" + sep() + "refsetTypes.txt", "w", encoding="UTF-8")
for fileName in files:
    if ('Refset_' in fileName):
        # import addn
        if ((w + '_' in fileName) or (w + '-en_' in fileName) or (w + '-es_' in fileName) or (w + '-en-GB_' in fileName)):
            RSType = fileName[fileName.find('_'):fileName.find('Refset')]
            if len(RSType) == 1:
                pattern = "Simple"
            else:
                pattern = RSType[1:]
            # add any new patterns
            if pattern not in patternList:
                patternList.append(pattern)
# write back augmented (or unchanged) list
for pattern in patternList:
    text_file_types.write(pattern)
    text_file_types.write("\n")
text_file_types.close()

def patternExpand(inString):
    returnString = ""
    characterDict = {'c':'Component', 'i': 'Integer', 's':'String'}
    for character in inString:
        returnString += characterDict[character]
    return returnString

def patternToFields(inString):
    returnString = ""
    tempString = ""
    characterDict = {'c':'ComponentId$ TEXT, ', 'i': 'Integer$ INTEGER, ', 's':'String$ TEXT, '}
    componentCounter = 1
    stringCounter = 1
    integerCounter = 1
    for character in inString:
        tempString = characterDict[character]
        if character == 'c':
            tempString = tempString.replace("$", str(componentCounter))
            componentCounter += 1
        elif character == 'i':
            tempString = tempString.replace("$", str(integerCounter))
            integerCounter += 1
        elif character == 's':
            tempString = tempString.replace("$", str(stringCounter))
            stringCounter += 1
        returnString += tempString
    return returnString[:-2]

def patternToMetaFields(inString):
    returnString = ""
    tempString = ""
    characterDict = {'c':'componentId$, ', 'i': 'integer$, ', 's':'string$, '}
    componentCounter = 1
    stringCounter = 1
    integerCounter = 1
    for character in inString:
        tempString = characterDict[character]
        if character == 'c':
            tempString = tempString.replace("$", str(componentCounter))
            componentCounter += 1
        elif character == 'i':
            tempString = tempString.replace("$", str(integerCounter))
            integerCounter += 1
        elif character == 's':
            tempString = tempString.replace("$", str(stringCounter))
            stringCounter += 1
        returnString += tempString
    return returnString[:-2]

def patternToRefList(inString, componentCounter):
    returnString = ""
    for character in inString:
        print(character)
        returnString += "refSet[" + str(componentCounter) + "], "
        componentCounter += 1
    return returnString[:-2]

text_file_types = io.open(codePath + "files" + sep() + "build" + sep() + "refsetTypes.txt", "r", encoding="UTF-8")
patternList = []
for line in text_file_types:
    patternList.append(line.strip())
patternList = sort_and_deduplicate(patternList)
text_file_types.close()

print("### patternlist")
print(patternList)

text_file = io.open(codePath + "importGuide.py", "w", encoding="UTF-8")

text_file_refset = io.open(codePath + "SCTRF2Refset.py", "w", encoding="UTF-8")

text_file.write("from __future__ import print_function, unicode_literals")
text_file.write("\n")
text_file.write("import io")
text_file.write("\n")
text_file.write("import os")
text_file.write("\n")
text_file.write("import sys")
text_file.write("\n")
text_file.write("\n")
text_file.write('if (sys.version_info > (3, 0)):')
text_file.write("\n")
text_file.write('    # Python 3 code:')
text_file.write("\n")
text_file.write('    w = input("Input Build Type (Full, Delta, Snapshot): ")')
text_file.write("\n")
text_file.write('else:')
text_file.write("\n")
text_file.write('    # Python 2 code:')
text_file.write("\n")
text_file.write('    w = raw_input("Input Build Type (Full, Delta, Snapshot): ").decode(\'UTF-8\')')
text_file.write("\n")
text_file.write("\n")
text_file.write('importItems = []')
text_file.write("\n")
text_file.write('importMainItems = []')
text_file.write("\n")
text_file.write("\n")
text_file.write("path = '" + path + "'")
text_file.write("\n")
text_file.write("trimpath = path[:-1]")
text_file.write("\n")
text_file.write("\n")
text_file.write("files = os.listdir(trimpath)")
text_file.write("\n")
text_file.write("\n")

text_file.write("basicFields = 'id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, '")
text_file.write("\n")
text_file.write("\n")
text_file.write('text_file = io.open(path[:-4] + "import" + w + ".txt", "w", encoding="UTF-8")')
text_file.write("\n")
text_file.write("\n")
text_file.write("text_file.write('.echo ON')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('.separator \\\\t')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("\n")
text_file.write("text_file.write('--CREATE TABLE INSTRUCTIONS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE TABLE Concepts(' + basicFields + 'definitionStatusId TEXT);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE TABLE Relationships(' + basicFields + 'sourceId TEXT, destinationId TEXT, relationshipGroup INTEGER, typeId TEXT, characteristicTypeId TEXT, modifierId TEXT);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE TABLE Descriptions(' + basicFields + 'conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE TABLE Definitions(' + basicFields + 'conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

for pattern in patternList:
    print(pattern)
    if pattern == "Simple":
        text_file.write("text_file.write('CREATE TABLE " + pattern + "Refset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT);')")
    else:
        patternString = patternToFields(pattern)
        text_file.write("text_file.write('CREATE TABLE " + pattern + "Refset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, " + patternString + ");')")
    text_file.write("\n")
    text_file.write('text_file.write("\\n")')
    text_file.write("\n")

text_file.write("\n")
text_file.write("text_file.write('--MAIN TABLE IMPORTS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

text_file.write("for fileName in files:")
text_file.write("\n")
text_file.write("    if ('_Concept_' in fileName) and (w + '_' in fileName):")
text_file.write("\n")
text_file.write("        print('Concepts: ', fileName)")
text_file.write("\n")
text_file.write("        importMainItems.append('.import ' + path + fileName + ' Concepts' + '\\n')")
text_file.write("\n")
text_file.write("    if ('_Relationship_' in fileName) and (w + '_' in fileName):")
text_file.write("\n")
text_file.write("        print('Relationships: ', fileName)")
text_file.write("\n")
text_file.write("        importMainItems.append('.import ' + path + fileName + ' Relationships' + '\\n')")
text_file.write("\n")
text_file.write("    if ('_Description_' in fileName) and (w + '_' in fileName):")
text_file.write("\n")
text_file.write("        print('Descriptions: ', fileName)")
text_file.write("\n")
text_file.write("        importMainItems.append('.import ' + path + fileName + ' Descriptions' + '\\n')")
text_file.write("\n")
text_file.write("    if ('_Definition_' in fileName) and (w + '_' in fileName):")
text_file.write("\n")
text_file.write("        print('Definitions: ', fileName)")
text_file.write("\n")
text_file.write("        importMainItems.append('.import ' + path + fileName + ' Definitions' + '\\n')")
text_file.write("\n")
text_file.write("    if ('Refset_' in fileName):")
text_file.write("\n")
# import addn
text_file.write("        if ((w + '_' in fileName) or (w + '-en_' in fileName) or (w + '-es_' in fileName) or (w + '-en-GB_' in fileName)):")
text_file.write("\n")
text_file.write("            RSType = fileName[fileName.find('_'):fileName.find('Refset')]")
text_file.write("\n")
text_file.write("            if RSType == '_':")
text_file.write("\n")
text_file.write("                print('Simple: ', fileName)")
text_file.write("\n")
text_file.write("                importItems.append('.import ' + path + fileName + ' SimpleRefset' + '\\n')")
text_file.write("\n")

for pattern in patternList:
    print(pattern)
    if not pattern == "Simple":
        text_file.write("            elif RSType == '_" + pattern + "':")
        text_file.write("\n")
        text_file.write("                print('" + patternExpand(pattern) + ": ', fileName)")
        text_file.write("\n")
        text_file.write("                importItems.append('.import ' + path + fileName + ' " + pattern + "Refset' + '\\n')")
        text_file.write("\n")

text_file.write("            else:")
text_file.write("\n")
text_file.write("                print('Dunno: ', fileName)")
text_file.write("\n")
text_file.write("        elif ((w == 'Snapshot') and ('NHSRDS' in fileName)):")
text_file.write("\n")
text_file.write("            print('Component [NHSRDS snapshot exception]: ', fileName)")
text_file.write("\n")
text_file.write("            importItems.append('.import ' + path + fileName + ' cRefset' + '\\n')")
text_file.write("\n")
text_file.write("        elif (not ('Full' in fileName) and not ('Delta' in fileName) and not ('Snapshot' in fileName)):")
text_file.write("\n")
text_file.write("            print('NOT FOUND: ', fileName)")
text_file.write("\n")

text_file.write("\n")
text_file.write("for importItem in importMainItems:")
text_file.write("\n")
text_file.write("    text_file.write(str(importItem))")
text_file.write("\n")

text_file.write("\n")
text_file.write("text_file.write('--REFSET IMPORTS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

text_file.write("\n")
text_file.write("for importItem in importItems:")
text_file.write("\n")
text_file.write("    text_file.write(str(importItem))")
text_file.write("\n")

#############################

text_file.write("\n")
text_file.write("tableNames = ['Concepts',")
text_file.write("\n")
text_file.write("            'Relationships',")
text_file.write("\n")
text_file.write("            'Descriptions',")
text_file.write("\n")
text_file.write("            'Definitions',")

for pattern in patternList:
    print(pattern)
    text_file.write("\n")
    text_file.write("            '" + pattern + "Refset',")
text_file.write("]")
text_file.write("\n")
text_file.write("\n")
text_file.write("indexFields = ['id',")
text_file.write("\n")
text_file.write("                'active']")
text_file.write("\n")

#basic index additions
text_file.write("\n")
text_file.write("text_file.write('--BASIC INDEX ADDITIONS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

text_file.write("\n")
text_file.write("for tableName in tableNames:")
text_file.write("\n")
text_file.write("    for indexField in indexFields:")
text_file.write("\n")
text_file.write("        text_file.write('CREATE INDEX ' + tableName + '_' + indexField + '_idx ON ' + tableName + ' (' + indexField + ');')")
text_file.write("\n")
text_file.write('        text_file.write("\\n")')
text_file.write("\n")
text_file.write("    if ('Refset' in tableName):")
text_file.write("\n")
text_file.write("        text_file.write('CREATE INDEX ' + tableName + '_referencedComponentId_idx ON ' + tableName + ' (referencedComponentId);')")
text_file.write("\n")
text_file.write('        text_file.write("\\n")')
text_file.write("\n")
text_file.write("        text_file.write('CREATE INDEX ' + tableName + '_refsetId_idx ON ' + tableName + ' (refsetId);')")
text_file.write("\n")
text_file.write('        text_file.write("\\n")')
text_file.write("\n")

text_file.write("\n")
text_file.write("#table-specific index additions (this will grow)")
text_file.write("\n")
text_file.write("text_file.write('--TABLE-SPECIFIC INDEX ADDITIONS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX Descriptions_conceptId_idx ON Descriptions (conceptId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX Definitions_conceptId_idx ON Definitions (conceptId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX Descriptions_term_idx ON Descriptions (term);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX Relationships_SourceId_idx ON Relationships (sourceId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX Relationships_DestinationId_idx ON Relationships (destinationId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX Relationships_CharacteristicTypeId_idx ON Relationships (characteristicTypeId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX Relationships_typeId_idx ON Relationships (typeId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX crefset_componentId1_idx ON crefset (componentId1);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX concepts_moduleId_idx ON concepts (moduleId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX descriptions_moduleId_idx ON descriptions (moduleId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('CREATE INDEX relationships_moduleId_idx ON relationships (moduleId);')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

text_file.write('#build time views (this will grow)')
text_file.write("\n")
text_file.write("text_file.write('--VIEWS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
if LAN == 1:
    text_file.write('#1')
    text_file.write("\n")
    text_file.write('text_file.write("CREATE VIEW \\"rdscodes\\" AS select id as CID from concepts where id IN (\\"' + tok("RLR_CLIN") + '\\", \\"' + tok("RLR_PHARM") + '\\") and active = 1;")')
    text_file.write("\n")
elif LAN == 2:
    text_file.write('#1')
    text_file.write("\n")
    text_file.write('text_file.write("CREATE VIEW \\"rdscodes\\" AS select id as CID from concepts where id=\\"' + tok("GB_ENG") + '\\" and active = 1;")')
    text_file.write("\n")
else:
    text_file.write('#1')
    text_file.write("\n")
    text_file.write('text_file.write("CREATE VIEW \\"rdscodes\\" AS select id as CID from concepts where id=\\"' + tok("SPANISH") + '\\" and active = 1;")')
    text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write('#2 - depends on 1')
text_file.write("\n")
text_file.write('text_file.write("CREATE VIEW \\"rdsrows\\" AS select referencedComponentId as id, componentId1 as acc from crefset where refsetId in (select CID from rdscodes) and active = 1;")')
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write('#3 - depends on 2')
text_file.write("\n")
text_file.write('text_file.write("CREATE VIEW \\"prefTerm\\" AS select descriptions.conceptId, descriptions.id, descriptions.term from descriptions, rdsrows where descriptions.id = rdsrows.id and descriptions.typeId = \\"' + tok("SYNONYM") + '\\" and rdsrows.acc = \\"' + tok("PREFTERM") + '\\" and descriptions.active = 1;")')
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write('#6 ->')
text_file.write("\n")
text_file.write('text_file.write("CREATE VIEW \\"refsetModules\\" AS \\')
text_file.write("\n")
text_file.write("select distinct 'Concepts' as filetype, 'Concepts' as refsetId, moduleId from Concepts where active=1 UNION \\")
text_file.write("\n")
text_file.write("select distinct 'Descriptions' as filetype, 'Descriptions' as refsetId, moduleId from Descriptions where active=1 UNION \\")
text_file.write("\n")
text_file.write("select distinct 'Definitions' as filetype, 'Definitions' as refsetId, moduleId from Definitions where active=1 UNION \\")
text_file.write("\n")
text_file.write("select distinct 'Relationships' as filetype, 'Relationships' as refsetId, moduleId from Relationships where active=1 UNION \\")
text_file.write("\n")
total = len(patternList)
counter = 0
for pattern in patternList:
    counter += 1
    print(pattern, total, counter)
    if not (total == counter):
        text_file.write("select distinct '" + pattern.lower() + "refset' as filetype, refsetId, moduleId from " + pattern.lower() + "refset where active=1 UNION \\")
    else:
        text_file.write("select distinct '" + pattern.lower() + "refset' as filetype, refsetId, moduleId from " + pattern.lower() + "refset where active=1;\")")
    text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

text_file.write("\n")
text_file.write("# tidy up header rows")
text_file.write("\n")
text_file.write("text_file.write('--REMOVE HEADERS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("\n")
text_file.write("for tableName in tableNames:")
text_file.write("\n")
text_file.write("    text_file.write('DELETE FROM ' + tableName + ' WHERE id == \\'id\\';')")
text_file.write("\n")
text_file.write('    text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('--DE-ESCAPE TERMS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('UPDATE descriptions SET term = replace( term, \\'\\\\\"\\', \\'\"\\' ) WHERE term LIKE \\'%\\\\\"%\\';')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('UPDATE definitions SET term = replace( term, \\'\\\\\"\\', \\'\"\\' ) WHERE term LIKE \\'%\\\\\"%\\';')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
# CONC - de-escape values post import
text_file.write("text_file.write('UPDATE relationships SET destinationId = replace( destinationId, \\'\\\\\"\\', \\'\"\\' ) WHERE destinationId LIKE \\'%\\\\\"%\\';')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

text_file.write("\n")
text_file.write("text_file.close()")
text_file.write("\n")
# text_file.close()

# test file creation

text_file.write('text_file = io.open(path[:-4] + "test" + w + ".txt", "w", encoding="UTF-8")')
text_file.write("\n")
text_file.write("\n")
text_file.write("text_file.write('.echo ON')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('.separator \\\\t')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("\n")

text_file.write("text_file.write('--READ TABLE INSTRUCTIONS')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('SELECT COUNT(*) FROM Concepts;')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('SELECT COUNT(*) FROM Relationships;')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('SELECT COUNT(*) FROM Descriptions;')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")
text_file.write("text_file.write('SELECT COUNT(*) FROM Definitions;')")
text_file.write("\n")
text_file.write('text_file.write("\\n")')
text_file.write("\n")

for pattern in patternList:
    print(pattern)
    text_file.write("text_file.write('SELECT COUNT(*) FROM " + pattern + "Refset;')")
    text_file.write("\n")
    text_file.write('text_file.write("\\n")')
    text_file.write("\n")

text_file.write("\n")
text_file.write("text_file.close()")
text_file.write("\n")
text_file.close()

# refset file creation

text_file_refset.write("# Refset processing methods autogenerated during data build.\n\n")
text_file_refset.write("def findRefsetMetaFields(refsetType):")
text_file_refset.write("\n")
text_file_refset.write("    if refsetType == 'simplerefset':")
text_file_refset.write("\n")
text_file_refset.write('        returnString = ""')
text_file_refset.write("\n")

for pattern in patternList:
    print(pattern)
    if pattern == "Simple":
        patternString = ""
    else:
        patternString = patternToMetaFields(pattern)
        text_file_refset.write("    elif refsetType == '" + pattern + "refset':")
        text_file_refset.write("\n")
        text_file_refset.write('        returnString = ", ' + patternString + '"')
        text_file_refset.write("\n")

text_file_refset.write("    else:")
text_file_refset.write("\n")
text_file_refset.write('        returnString = ""')
text_file_refset.write("\n")
text_file_refset.write("    return returnString")
text_file_refset.write("\n")

text_file_refset.write("\n")
text_file_refset.write("def refsetRecordsetDecomp(refSet, refsetType):")
text_file_refset.write("\n")
text_file_refset.write("    returnSet = ()")
text_file_refset.write("\n")
text_file_refset.write("    if refsetType == 'simplerefset':")
text_file_refset.write("\n")
text_file_refset.write("        returnSet = (refSet[0],)")
text_file_refset.write("\n")

for pattern in patternList:
    print(pattern)
    if pattern == "Simple":
        patternString = ""
    else:
        patternString = patternToRefList(pattern, 1)
        text_file_refset.write("    elif refsetType == '" + pattern + "refset':")
        text_file_refset.write("\n")
        text_file_refset.write("        returnSet = (refSet[0], " + patternString + ")")
        text_file_refset.write("\n")
text_file_refset.write("    else:")
text_file_refset.write("\n")
text_file_refset.write("        returnSet = (refSet[0],)")
text_file_refset.write("\n")
text_file_refset.write("    return returnSet")
text_file_refset.write("\n")
text_file_refset.write("\n")
text_file_refset.write("def refsetRecordsetMetaDecomp(refSet, refsetType):")
text_file_refset.write("\n")
text_file_refset.write("    returnSet = ()")
text_file_refset.write("\n")
text_file_refset.write("    if refsetType == 'simplerefset':")
text_file_refset.write("\n")
text_file_refset.write("        returnSet = (refSet[0], refSet[1], refSet[2], refSet[3])")
text_file_refset.write("\n")

for pattern in patternList:
    print(pattern)
    if pattern == "Simple":
        patternString = ""
    else:
        patternString = patternToRefList(pattern, 4)
        text_file_refset.write("    elif refsetType == '" + pattern + "refset':")
        text_file_refset.write("\n")
        text_file_refset.write("        returnSet = (refSet[0], refSet[1], refSet[2], refSet[3], " + patternString + ")")
        text_file_refset.write("\n")
text_file_refset.write("    else:")
text_file_refset.write("\n")
text_file_refset.write("        returnSet = (refSet[0], refSet[1], refSet[2], refSet[3])")
text_file_refset.write("\n")
text_file_refset.write("    return returnSet")
text_file_refset.write("\n")

text_file_refset.write("\n")
text_file_refset.write("def refsetList():")
text_file_refset.write("\n")
text_file_refset.write("    returnList = [")

patternCounter = 1
patternMax = len(patternList)
for pattern in patternList:
    print(pattern, len(patternList), patternCounter)
    text_file_refset.write("'" + pattern.lower() + "refset'")
    if patternCounter < patternMax:
        text_file_refset.write(",")
        text_file_refset.write("\n\t\t\t\t\t")
    patternCounter += 1

text_file_refset.write("]")
text_file_refset.write("\n")
text_file_refset.write("\n")
text_file_refset.write("    return returnList")
text_file_refset.write("\n")

text_file_refset.close()
