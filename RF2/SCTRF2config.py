from __future__ import unicode_literals
import io
from platformSpecific import cfgPath, dataPath, sep

def tok(inString):
    f = io.open(cfgPath() + sep() + 'token' + sep() + 'SCTRF2token.txt', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = tempList[1]
    f.close()
    return returnValue

def tokDict():
    returnDict = {}
    f = io.open(cfgPath() + sep() + 'token' + sep() + 'SCTRF2token.txt', 'r', encoding="UTF-8")
    for line in f:
        if (not line[0] == "#") and ('|' in line):  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            returnDict[tempList[0]] = tempList[1]
    f.close()
    return returnDict

def tokList(inString):
    f = io.open(cfgPath() + sep() + 'token' + sep() + 'SCTRF2token.txt', 'r', encoding="UTF-8")
    returnList = []
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnList = tempList[1].split(',')
    f.close()
    return returnList

def cfg(inString):
    f = io.open(cfgPath() + sep() + 'config' + sep() + 'SCTRF2config.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                if tempList[1].isdigit():
                    returnValue = int(tempList[1])
                else:
                    returnValue = tempList[1]
    f.close()
    return returnValue

def pth(inString):
    f = io.open(cfgPath() + dataPath() + 'SCTRF2datapaths.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = tempList[1]
    f.close()
    return returnValue

def cfgWrite(inString, inValue):
    writeToList = []
    try:
        f = io.open(cfgPath() + sep() + 'config' + sep() + 'SCTRF2config.cfg', 'r+', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            tempLine = line.strip()
            if not line[0] == "#":
                tempList = tempLine.split('|')
                if tempList[0] == inString:
                    writeToList.append(tempList[0] + '|' + str(inValue))
                else:
                    if len(tempLine) > 1:
                        writeToList.append(tempLine)
            else:
                writeToList.append(tempLine)
        #print writeToList
        f.seek(0)
        for entry in writeToList:
            f.write(entry + '\n')
    finally:
        f.close()

def cfg_MRCM(inString):  # moved here for helpfiles access
    f = io.open(cfgPath() + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = int(tempList[1])
    f.close()
    return returnValue

def cfg_SV(inString):  # copy for helpfiles access
    f = io.open(cfgPath() + sep() + 'config' + sep() + 'SCTRF2SVconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = int(tempList[1])
    f.close()
    return returnValue
