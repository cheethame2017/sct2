from __future__ import print_function, unicode_literals
from SCTRF2Helpers import childrenFromId_IN, TCCompare_IN, ancestorsIDFromConId_IN
from SCTRF2config import tokDict

tok = tokDict()

def tlc():
    tlcList = childrenFromId_IN((tok['ROOT'],))
    return tlcList

def setToGraph(inSet, debugBool, addOnList):
    overList = []
    setList = []
    tlcLocal = []
    tlcLocal = tlc()
    for item in tlcLocal:
        temp = item[2]
        overList.append({temp:[(temp,)]})
    for item in addOnList:
        inSet.append(item)
    memberCounter = 1
    for member in inSet:
        if debugBool and (memberCounter % 100 == 0):
            print(member)
        for item in overList:
            for key in list(item.keys()):
                if TCCompare_IN(member, key):
                    item[key].append((member,))
    for item in overList:
        # removes tlcs that have no subtypes
        if len(list(item.values())[0]) > 1:
            setList.append(item)
    return setList

def adjacencyBuild(chapterList, debugBool):
    adjList = []
    for item in chapterList:
        for key in list(item.keys()):
            adjList.append({key:[]})
    for item1 in adjList:
        for key1 in list(item1.keys()):
            for item2 in chapterList:
                for key2 in list(item2.keys()):
                    if key1 == key2:
                        expandCounter = 1
                        for item3 in list(item2.values()):
                            total = str(len(item3))
                            for item4 in item3:
                                expandCounter += 1
                                TCList = []
                                TCList = [item for item in ancestorsIDFromConId_IN(item4[0]) if item != tok["ROOT"]]
                                for item5 in TCList:
                                    for item6 in item3:
                                        if item5 == item6[0] and not(item4[0] == item6[0]):
                                            item1[key1].append(list(zip(item4, item6))[0])
                                if debugBool and (expandCounter % 100 == 0):
                                    print(key1, str(expandCounter) + " / "  + total)
    # remove transitive relations...
    c1 = 0
    for item1 in adjList:
        removeList = []
        for key1 in list(item1.keys()):
            c1 += 1
            # for each tuple in each value list
            c2 = 0
            total = str(len(item1[key1]))
            for pair in item1[key1]:
                c2 += 1
                # match source
                for sourceMatch in [item for item in item1[key1] if (item[0] == pair[0]) and not(item[1] == pair[1])]:
                    # if there is a source match but not a destinationMatch...
                    # check to see if there is a destination match
                    c3 = 0
                    for destinationMatch in [item for item in item1[key1] if item[1] == pair[1]]:
                        # if the destination code of the source match matches the source of another, and
                        # the destination of this matches the destination of value 1...
                        c3 += 1
                        if (sourceMatch[1] == destinationMatch[0]) and (destinationMatch[1] == pair[1]):
                            # pair is redundant and can be added to removeList
                            removeList.append(pair)
                if debugBool and (c2 % 100 == 0):
                    print(str(c1), str(c2) + " / " + total)
        # remove rows that are in removeList
        item1[key1] = [item for item in item1[key1] if item not in removeList]
        item1[key1].append((key1, tok['ROOT']))
    returnList = []
    for item1 in adjList:
        for key1 in list(item1.keys()):
            for item2 in item1[key1]:
                if (item2[0], item2[1]) not in returnList:
                    returnList.append((item2[0], item2[1]))
    if debugBool:
        total = 0
        for item1 in adjList:
            for key1 in list(item1.keys()):
                total += len(item1[key1])
            print("total", total)
    return returnList

def setToGraphAugmentTest(inIdList, augmentList):
    returnList = []
    for augId in augmentList:
        addMeBool = False
        for listId in inIdList:
            if TCCompare_IN(listId, augId):
                addMeBool = True
                break
        if addMeBool:
            returnList.append(augId)
    return returnList
