from SCTRF2config import pth
from rscompareBuilder import refsetCompare
from modelLimitFill import fillModelLimit, buildTagsSimple
from concBuilder import concBuild
from writeModelTable import fillAttLimit
from writeModelTableHelpers import getVersion

snapPath = pth('RF2_SNAP')
outPath = pth('RF2_OUT')

# build refset comparison tables
refsetCompare(outPath)

# fill concrete lookups
# CONC
concBuild(snapPath, outPath)

if getVersion(2) == 3: # only run these on latest international data [Int=3, UK=8]

    # fill model limit tables
    fillModelLimit(snapPath)

    # fill attLimit using the SCTMRCM.py terminal program, with the following sequence:
    # Enter 'v' (Reset all)
    # Enter 'bb' (Include all domains)
    # Enter 'x1' (Limit processing to 'only attributes')
    # Enter 'd' (Run)
    fillAttLimit()

    # generate list of semantic tag introduction points
    buildTagsSimple(outPath)
