from __future__ import unicode_literals
import io
from platformSpecific import cfgPath, dataPath, sep

def tok(inString):  # short name for typing convenience - setConfig reallly
    f = io.open(cfgPath() + sep() + 'token' + sep() + 'SCTRF2SVtoken.txt', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = tempList[1]
    f.close()
    return returnValue

def tokList(inString):
    f = io.open(cfgPath() + sep() + 'token' + sep() + 'SCTRF2SVtoken.txt', 'r', encoding="UTF-8")
    returnList = []
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnList = tempList[1].split(',')
    f.close()
    return returnList

def cfg(inString):  # short name for typing convenience - setConfig reallly
    f = io.open(cfgPath() + sep() + 'config' + sep() + 'SCTRF2SVconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = int(tempList[1])
    f.close()
    return returnValue

def pth(inString):  # short name for typing convenience - setConfig really
    f = io.open(cfgPath() + dataPath() + 'SCTRF2datapaths.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = tempList[1]
    f.close()
    return returnValue

def cfgWrite(inString, inValue):
    writeToList = []
    try:
        f = io.open(cfgPath() + sep() + 'config' + sep() + 'SCTRF2SVconfig.cfg', 'r+', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            tempLine = line.strip()
            if not line[0] == "#":
                tempList = tempLine.split('|')
                if tempList[0] == inString:
                    writeToList.append(tempList[0] + '|' + str(inValue))
                else:
                    if len(tempLine) > 1:
                        writeToList.append(tempLine)
            else:
                writeToList.append(tempLine)
        #print writeToList
        f.seek(0)
        for entry in writeToList:
            f.write(entry + '\n')
    finally:
        f.close()




##param = 'SEARCH_TYPE'
##a = getConfig(param)
##print a, param
##param = 'SEARCH_ORDER'
##a = getConfig(param)
##print a, param
