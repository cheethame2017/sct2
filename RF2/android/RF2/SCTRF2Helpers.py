from __future__ import print_function, unicode_literals
import io
import sqlite3
import re
from unidecode import unidecode
from platformSpecific import cfgPathInstall, sep
from SCTRF2config import tokDict, tokList, pth, cfg
from SCTRF2Refset import findRefsetMetaFields, refsetRecordsetMetaDecomp, refsetRecordsetDecomp
# CONC HEADER
from SCTRF2Concrete import concreteTakingRoles, toNumber, concreteAttAdds

snapPath = pth('RF2_SNAP')
outPath = pth('RF2_OUT')

ownModuleSet = tokList('OWNMODULESET')
intModuleSet = tokList('INTMODULE')

tok = tokDict()

def listify(inThing):
    ret = []
    ret.append(inThing)
    return ret


def tuplify(inThing):
    preReturnThing = []
    preReturnThing.append(inThing)
    returnThing = tuple(preReturnThing)
    return returnThing


def simplify(inTerm):
    outTerm = ""
    outTerm = unidecode(str(inTerm))
    outTerm = outTerm.replace("\"", "")
    outTerm = outTerm.replace("\'", "")
    return outTerm

def AND(Aset, Bset): # AND logic
    return Aset & Bset

def OR(Aset, Bset): # OR logic
    return Aset | Bset

def NOT(Aset, Bset): # NOT logic
    return Aset - Bset

def refsetPterm(idIn):
    if not isinstance(idIn, tuple):
        idIn = tuplify(idIn)
    if idIn[0][-2] == "1": #if referenced component is a description, just return the associated term
        connectionIn2 = sqlite3.connect(outPath)
        cursorIn2 = connectionIn2.cursor()
        # check if it's in prefterm index
        cursorIn2.execute("select term from prefterm where descid = ?", (idIn[0],))
        recordsetIn2 = cursorIn2.fetchone()
        if not recordsetIn2 is None:
            cursorIn2.close()
            connectionIn2.close()
            return recordsetIn2[0]
        else:
            cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
            # or larger descriptions table
            cursorIn2.execute("select term from descriptions where id = ?", (idIn[0],))
            recordsetIn2 = cursorIn2.fetchone()
            if not recordsetIn2 is None:
                cursorIn2.close()
                connectionIn2.close()
                return recordsetIn2[0]
            else:
                # or is it a definition...
                cursorIn2.execute("select term from definitions where id = ?", (idIn[0],))
                recordsetIn2 = cursorIn2.fetchone()
                cursorIn2.close()
                connectionIn2.close()
                if not recordsetIn2 is None:
                    return recordsetIn2[0]
                else:
                    return "No term found"
    elif idIn[0][-2] == "2": # if a relationship
        return "Relationship"
    else:
        return pterm(idIn)  # if not, return the pterm of the concept


def pterm(inId):
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    # CONC - pterm handler: if called on #number value from rels table then return ""
    if inId[0][0] == "#":
        if cfg("CONC_DATA") == 1:
            return inId[0][1:]
        else:
            returnterm = ''
    # CONC - pterm handler: if called on "string" value from rels table then return same
    if inId[0][0] == "\"":
        if cfg("CONC_DATA") == 1:
            return inId[0]
        else:
            returnterm = ''
    elif inId[0][-2] == '0':
        returnterm = ptermCid(inId)
    elif  inId[0][-2] == '1':
        returnterm = ptermDid(inId)
    else:
        returnterm = ''
    return returnterm


def ptermDid(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    returnable = []
    cursorIn2.execute("select term, active, typeId from descriptionsdx where id = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchall()
    if recordsetIn2:
        for row in recordsetIn2:
            if row[1] == 1:
                if row[2] == tok["DEFINIT"]:
                    returnable = row[0] + " (Acti. DefnID)"
                else:
                    returnable = row[0] + " (Acti. DescID)"
                return returnable
            else:
                if row[2] == tok["DEFINIT"]:
                    returnable = row[0] + " (Inac. DefnID)"
                else:
                    returnable = row[0] + " (Inac. DescID)"
                return returnable
    else:
        # alternative lookup for terms of inactive descriptions (e.g. in ''refers to'' refset)
        cursorIn2.execute("select term, active from descriptions where id = ?", (idIn[0],))
        recordsetIn2 = cursorIn2.fetchall()
        if recordsetIn2:
            for row in recordsetIn2:
                if row[1] == 0:
                    returnable = row[0] + " (Inac. DescID)"
                    return returnable
                else:
                    returnable = row[0]
                    return returnable
        else:
            # alternative lookup for terms of inactive definitions (e.g. in ''refers to'' refset)
            cursorIn2.execute("select term, active from definitions where id = ?", (idIn[0],))
            recordsetIn2 = cursorIn2.fetchall()
            if recordsetIn2:
                for row in recordsetIn2:
                    if row[1] == 0:
                        returnable = row[0] + " (Inac. DefnID)"
                        return returnable
                    else:
                        returnable = row[0]
                        return returnable
            else:
                return 'DiD: synonym not found'
    cursorIn2.close()
    connectionIn2.close()


def ptermCid(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    cursorIn2.execute("select term from prefterm where conId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchone()
    if recordsetIn2:
        return recordsetIn2[0]
    else:
        # alternative lookup for old rf2 metadata not covered by RDS
        cursorIn2.execute("select term from RF2Snap.descriptions where typeid='" + tok["SYNONYM"] + "' and active=1 and conceptId = ?", (idIn[0],))
        recordsetIn2 = cursorIn2.fetchone()
        cursorIn2.close()
        connectionIn2.close()
        if recordsetIn2:
            return recordsetIn2[0] + " *"
        else:
            return 'Preferred synonym/FSN not found'

def fsnFallback(idIn):
    # alternative fsn lookup when drug data not loaded!
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select term from descriptions where typeid='" + tok["FSN"] + "' and active=1 and conceptId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchone()
    cursorIn2.close()
    connectionIn2.close()
    if recordsetIn2:
        return recordsetIn2[0] + " *"
    else:
        return 'Preferred synonym/FSN not found'

def parentsFromId_IN(idIn, FromDotCodeBool=False):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath + "' as RF2Out")
    # supertypes
    cursorIn2.execute("select destinationId from RF2Out.rellite where sourceId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchall()
    parentList = []
    focusFSN = fsnFromConId_IN(idIn[0])[1]
    if len(recordsetIn2) > 0:
        for parent in recordsetIn2:
            if not parent is None:
                if cfg("SHOW_DEFS") < 3 or (parent[0] == tok["ROOT"]) or FromDotCodeBool:
                    tempstring = pterm(parent) + "|" + parent[0]
                else:
                    tempstring = hasDifferentTagTerm(parent, focusFSN) + "|" + parent[0]
                parentList.append(tempstring.split("|"))
        if cfg("SORT_DIAG") == 0 or cfg("SORT_DIAG") == 3: # ascii or length
            parentList.sort(key=lambda x: x[0].lower())
        else: # natural sort
            parentList = naturalSort(parentList, False, 1, 0)
    cursorIn2.close()
    connectionIn2.close()
    return parentList

def parentIdsFromId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    # supertypes
    cursorIn2.execute("select destinationId from rellite where sourceId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchall()
    parentList = []
    if len(recordsetIn2) > 0:
        for parent in recordsetIn2:
            parentList.append(parent)
    cursorIn2.close()
    connectionIn2.close()
    return parentList


def parentsFromIdPlusMeta_IN(inId, activeStatus):
    parents = []
    roleList = []
    roleList = groupedRolesFromConIdPlusMeta_IN(inId[0], activeStatus)
    for roleGroup in roleList:
        for role in roleGroup:
            if not isinstance(role, int):
                if role[0] == -1:
                    parents.append(role)
    return parents


def childrenFromIdWithPlus_IN(idIn, limit=False):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn = connectionIn2.cursor()
    cursorIn2 = connectionIn2.cursor()
    # children
    if not limit:
        cursorIn2.execute("select sourceId from rellite where destinationId = ?", (idIn[0],))
    else:
        cursorIn2.execute("select sourceId from rellite where destinationId = ? limit ?", (idIn[0], cfg("CHILD_LIMIT")))
    recordsetIn2 = cursorIn2.fetchall()
    childList = []
    childListTemp = []
    focusFSN =  fsnFromConId_IN(idIn[0])[1]
    for child in recordsetIn2:
        if not child is None:
            cCount = childCountFromId_IN(child)
            if cCount == 0:
                plusString = "| |"
            elif cCount > 1500:
                plusString = "|#|"
            else:
                plusString = "|+|"
            if cfg("SHOW_DEFS") < 3 or (idIn[0] == tok["ROOT"]):
                tempstring = pterm(child) + plusString + child[0]
            else:
                tempstring = hasDifferentTagTerm(child, focusFSN) + plusString + child[0]
            childList.append(tempstring.split("|"))
    if cfg("SORT_DIAG") == 0: # ascii sort
        childList.sort(key=lambda x: x[0].lower())
    elif cfg("SORT_DIAG") == 1 or cfg("SORT_DIAG") == 2 or cfg("SORT_DIAG") == 4 or cfg("SORT_DIAG") == 5: # natural sort
        childList = naturalSort(childList, False, 2, 0)
    else: # descendant count
        for child in recordsetIn2:
            if not child is None:
                subCount = descendantCountFromConId_IN(child[0])
                if subCount == 1:
                    plusString = "| |"
                else:
                    cCount = childCountFromId_IN(child)
                    if cCount > 1500:
                        plusString = "|#|"
                    else:
                        plusString = "|+|"
                if cfg("SHOW_DEFS") < 3 or (idIn[0] == tok["ROOT"]):
                    tempstring = pterm(child) + plusString + child[0] + "|" + str(subCount)
                else:
                    tempstring = hasDifferentTagTerm(child, focusFSN) + plusString + child[0] + "|" + str(subCount)
                childListTemp.append(tempstring.split("|"))
        childListTemp.sort(key=lambda x: (-(int(x[3])), x[0].lower()))
        childList = [[item[0], item[1], item[2]] for item in childListTemp]
    cursorIn.close()
    cursorIn2.close()
    connectionIn2.close()
    return childList

def naturalSortTerm(inTerm, toNumberDict, toNumberDictKeys, p=re.compile("\d*\.?\d+")): # experimental method for sorting terms 'numerically' rather than 'ascii-betically'; create extra index field at runtime, sort on this then discard.
    matchDict = {}
    matchDictCounter = 1
    tempTerm = ""
    for toNumberDictKey in toNumberDictKeys:
        # inTerm = inTerm.replace(toNumberDictKey, toNumberDict[toNumberDictKey])
        inTerm = re.sub(toNumberDictKey, toNumberDict[toNumberDictKey], inTerm)
    for m in p.finditer(inTerm):
        matchDict[matchDictCounter] = [m.start(), m.group()]
        matchDictCounter += 1
    if matchDict:
        matchDictKeys = list(matchDict.keys())
        matchDictKeys.reverse()
        for matchDictKey in matchDictKeys:
            if matchDictKey + 1 == matchDictCounter and matchDictKey != 1: # last of more than one number, includes run to end of term
                tempTerm = zfill_local(matchDict[matchDictKey][1]) + inTerm[matchDict[matchDictKey][0] + len(matchDict[matchDictKey - 1][1]):] + tempTerm
            elif matchDictKey == 1: # first number
                if matchDictCounter == 2: # one and only one number
                    tempTerm = inTerm[:matchDict[matchDictKey][0]] + zfill_local(matchDict[matchDictKey][1]) + inTerm[matchDict[matchDictKey][0] + len(matchDict[matchDictKey][1]):] + tempTerm
                else: # first of more than one number
                    tempTerm = inTerm[:matchDict[matchDictKey][0]] + zfill_local(matchDict[matchDictKey][1]) + inTerm[matchDict[matchDictKey][0] + len(matchDict[matchDictKey][1]):matchDict[matchDictKey + 1][0]] + tempTerm
            else: # any mid section
                tempTerm = zfill_local(matchDict[matchDictKey][1]) + inTerm[matchDict[matchDictKey][0] + len(matchDict[matchDictKey - 1][1]):matchDict[matchDictKey + 1][0]] + tempTerm
    if tempTerm == "":
        return inTerm
    else:
        return tempTerm

def naturalSort(inList, generateTermBool, idPos=0, termPos=0): # list equivalent of naturalSortTerm; call with e.g. myList = naturalSort(myList, False, 0, 1) or myList = naturalSort(myList, True, 0)
    outList = []
    returnList = []
    p = re.compile("\d*\.?\d+")
    if cfg("SORT_DIAG") == 2 or cfg("SORT_DIAG") == 4 or cfg("SORT_DIAG") == 5:
        toNumberDict, toNumberDictKeys = numberDict()
    else:
        toNumberDict = {}
        toNumberDictKeys = []
    for inTerm in inList:
        tempTerm = ""
        if generateTermBool:
            processTerm = inTerm[termPos]
        else:
            processTerm = pterm(inTerm[idPos])
        tempTerm = naturalSortTerm(processTerm, toNumberDict, toNumberDictKeys, p)
        tempList = [item for item in inTerm]
        if tempTerm == processTerm:
            tempList.append(processTerm)
        else:
            tempList.append(tempTerm)
            if cfg("SORT_DIAG") == 4 or cfg("SORT_DIAG") == 5:
                tempList[termPos] = '\033[31m' + tempList[termPos] + '\033[92m'
        outList.append(tempList)
    outList = sorted(outList, key=lambda x: x[-1].lower())
    returnList = [item[:-1] for item in outList]
    return returnList

def zfill_local(inString):
    n = inString.find(".")
    outString = ""
    if n == -1:
        outString = inString.zfill(20)
    else:
        outString = inString.zfill(20 + len(inString) - n)
    return outString

def numberDict():
    toNumberDictPre = {"\\b[O|o]ne\\b":"1",
                "\\b[T|t]wo\\b":"2",
                "\\b[T|t]hree\\b":"3",
                "\\b[F|f]our\\b":"4",
                "\\b[F|f]ive\\b":"5",
                "\\b[S|s]ix\\b":"6",
                "\\b[S|s]even\\b":"7",
                "\\b[E|e]ight\\b":"8",
                "\\b[N|n]ine\\b":"9",
                "\\b[T|t]en\\b":"10",
                "\\b[E|e]leven\\b":"11",
                "\\b[T|t]welve\\b":"12",
                "\\b[T|t]hirteen\\b":"13",
                "\\b[F|f]ourteen\\b":"14",
                "\\b[F|f]ifteen\\b":"15",
                "\\b[S|s]ixteen\\b":"16",
                "\\b[S|s]eventeen\\b":"17",
                "\\b[E|e]ighteen\\b":"18",
                "\\b[N|n]ineteen\\b":"19",
                "\\b[T|t]wenty\\b":"20",
                "\\b[T|t]hirty\\b":"30",
                "\\b[F|f][o|ou]rty\\b":"40",
                "\\b[F|f]ifty\\b":"50",
                "\\b[S|s]ixty\\b":"60",
                "\\b[S|s]eventy\\b":"70",
                "\\b[E|e]ighty\\b":"80",
                "\\b[N|n]inety\\b":"90",
                "\\b[H|h]undred\\b":"100",
                "\\b[Z|z]ero\\b":"0",
                "\\b[F|f]irst\\b":"1.000001",
                "\\b[S|s]econd\\b":"2.000001",
                "\\b[T|t]hird\\b":"3.000001",
                "\\b[F|f]ourth\\b":"4.000001",
                "\\b[F|f]ifth\\b":"5.000001",
                "\\b[S|s]ixth\\b":"6.000001",
                "\\b[S|s]eventh\\b":"7.000001",
                "\\b[E|e]ighth\\b":"8.000001",
                "\\b[N|n]inth\\b":"9.000001",
                "\\b[T|t]enth\\b":"10.000001",
                "\\b[E|e]leventh\\b":"11.000001",
                "\\b[T|t]welfth\\b":"12.000001",
                "\\b[T|t]hirteenth\\b":"13.000001",
                "\\b[F|f][o|o]urteenth\\b":"14.000001",
                "\\b[F|f]ifteenth\\b":"15.000001",
                "\\b[S|s]ixteenth\\b":"16.000001",
                "\\b[S|s]eventeenth\\b":"17.000001",
                "\\b[E|e]ighteenth\\b":"18.000001",
                "\\b[N|n]ineteenth\\b":"19.000001",
                "\\b[T|t]wentieth\\b":"20.000001",
                "\\b[T|t]hirtieth\\b":"30.000001",
                "\\b[F|f][o|ou]rtieth\\b":"40.000001",
                "\\b[F|f]iftieth\\b":"50.000001",
                "\\b[S|s]ixtieth\\b":"60.000001",
                "\\b[S|s]eventieth\\b":"70.000001",
                "\\b[E|e]ightieth\\b":"80.000001",
                "\\b[N|n]inetieth\\b":"90.000001",
                "\\b[H|h]undredth\\b":"100.000001",
                "\\b[O|o]nce\\b":"1.0000001",
                "\\b[T|t]wice\\b":"2.0000001",
                "\\b[T|t]hrice\\b":"3.0000001"}

    digitDict = {"\\b[T|t]humb\\b":"1 digit",
                "\\b[H|h]allux\\b":"1 toe",
                "\\b[G|g]reat toe\\b":"1 toe",
                "\\b[I|i]ndex finger\\b":"2 digit",
                "\\b[M|m]iddle finger\\b":"3 digit",
                "\\b[R|r]ing finger\\b":"4 digit",
                "\\b[L|l]ittle finger\\b":"5 digit",
                "\\b[L|l]ittle toe\\b":"5 toe"}

    cranialDict = {"\\b[O|o]lfactory ne[rv|ur]":"1 nerve ",
                "\\b[O|o]ptic ne[rv|ur]":"2 nerve ",
                "\\b[O|o]culomotor ne[rv|ur]":"3 nerve ",
                "\\b[T|t]rochlear ne[rv|ur]":"4 nerve ",
                "\\b[T|t]rigeminal ne[rv|ur]":"5 nerve ",
                "\\b[A|a]bducens ne[rv|ur]":"6 nerve ",
                "\\b[F|f]acial ne[rv|ur]":"7 nerve ",
                "\\b[V|v]estibulocochlear ne[rv|ur]":"8 nerve ",
                "\\b[A|a]coustic (vestibular)?.*ne[rv|ur]":"8 nerve ",
                "\\b[G|g]lossopharyngeal ne[rv|ur]":"9 nerve ",
                "\\b[V|v]agus ne[rv|ur]":"10 nerve ",
                "\\b[A|a]ccessory ne[rv|ur]":"11 nerve ",
                "\\b[S|s]pinal accessory ne[rv|ur]":"11 nerve ",
                "\\b[H|h]ypoglossal ne[rv|ur]":"12 nerve "}

    romanNumeralsDict = {"\\bI(.)?\\b":"1",
                        "\\bII(.)?\\b":"2",
                        "\\bIII(.)?\\b":"3",
                        "\\bIV(.)?\\b":"4",
                        "\\bV(.)?\\b":"5",
                        "\\bVI(.)?\\b":"6",
                        "\\bVII(.)?\\b":"7",
                        "\\bVIII(.)?\\b":"8",
                        "\\bIX(.)?\\b":"9",
                        "\\bX(.)?\\b":"10",
                        "\\bXI(.)?\\b":"11",
                        "\\bXII(.)?\\b":"12",
                        "\\bXIII(.)?\\b":"13",
                        "\\bXIV(.)?\\b":"14",
                        "\\bXV(.)?\\b":"15",
                        "\\bXVI(.)?\\b":"16",
                        "\\bXVII(.)?\\b":"17",
                        "\\bXVIII(.)?\\b":"18",
                        "\\bXIX(.)?\\b":"19",
                        "\\bXX(.)?\\b":"20",}

    daysOfWeekDict = {"\\b[M|m]onday\\b":"1 day",
                "\\b[T|t]uesday\\b":"2 day",
                "\\b[W|w]ednesday\\b":"3 day",
                "\\b[T|t]hursday\\b":"4 day",
                "\\b[F|f]riday\\b":"5 day",
                "\\b[S|s]aturday\\b":"6 day",
                "\\b[S|s]unday\\b":"7 day"}

    if cfg("SORT_DIAG") == 5:
        toNumberDict = dict(**toNumberDictPre,
                            **digitDict,
                            **cranialDict,
                            **romanNumeralsDict,
                            **daysOfWeekDict)
    else:
        toNumberDict = dict(**toNumberDictPre,
                            **cranialDict)

    toNumberDictKeys = list(toNumberDict.keys())
    toNumberDictKeys.sort(key=len)
    toNumberDictKeys.reverse()
    return toNumberDict, toNumberDictKeys

def childrenFromId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    # children
    cursorIn2.execute("select sourceId from rellite where destinationId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchall()
    childList = []
    for child in recordsetIn2:
        tempstring = pterm(child) + "| |" + child[0]
        childList.append(tempstring.split("|"))
    childList.sort(key=lambda x: x[0].lower())
    cursorIn2.close()
    connectionIn2.close()
    return childList

def childrenIDFromId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    # children
    cursorIn2.execute("select sourceId from rellite where destinationId = ?", (idIn,))
    recordsetIn2 = cursorIn2.fetchall()
    childList = []
    for child in recordsetIn2:
        childList.append(child[0])
    cursorIn2.close()
    connectionIn2.close()
    return childList

def childCountFromId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    # children
    childCount = cursorIn2.execute("select count(sourceId) from rellite where destinationId = ?", (idIn[0],)).fetchone()[0]
    cursorIn2.close()
    connectionIn2.close()
    return childCount

def attUseCount(inAtt):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select count(typeId) from relationships where active=1 and typeId = ?", (inAtt,))
    recordsetIn2 = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    return recordsetIn2[0][0]

def attUseInDomCount(inAtt, inDomList):
    domList = ', '.join(map(str, inDomList))
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath + "' as RF2Out")
    cursorIn2.execute("select count(typeId) from relationships, RF2Out.tc where relationships.active=1 and relationships.typeId = ? and RF2Out.tc.subtypeId=relationships.sourceId and RF2Out.tc.supertypeId in (" + domList + ")", (inAtt,))
    recordsetIn2 = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    return recordsetIn2[0][0]

def allTermsFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    # terms
    cursorIn2.execute("select descriptionsdx.id, descriptionsdx.term, descriptionsdx.typeId, rdsrows.acc FROM descriptionsdx, rdsrows WHERE descriptionsdx.id=rdsrows.id AND descriptionsdx.conceptId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    termList = []
    fsn = []
    tempFsn = ()
    pt = []
    tempPt = ()
    syns = []
    tempSyn = ()
    defn = []
    tempDefn = ()
    for term in recordsetIn2:
        if term[2] == tok['FSN'] and term[3] == tok['PREFTERM']:
            tempFsn = (term[0], term[1])
            fsn.append(tempFsn)
        elif term[2] == tok['SYNONYM'] and term[3] == tok['PREFTERM']:
            tempPt = (term[0], term[1])
            pt.append(tempPt)
        elif term[2] == tok['SYNONYM'] and term[3] == tok['ACCEPTABLE']:
            tempSyn = (term[0], term[1])
            syns.append(tempSyn)
        elif term[2] == tok['DEFINIT'] and term[3] == tok['PREFTERM']:
            tempDefn = (term[0], term[1])
            defn.append(tempDefn)
    if fsn == []:
        tempFsn = ("*", fsnFallback(tuplify(idIn)))
        fsn.append(tempFsn)
    if pt == []:
        tempPt = ("*", pterm(idIn))
        pt.append(tempPt)
    if defn == []: # if no definitions from descriptionsdx, check en-GB LRS
        cursorIn2.execute("select definitions.id, definitions.conceptId, definitions.term from definitions, crefset where definitions.active=1 and crefset.active=1 and crefset.refsetId='" + tok['GB_ENG'] + "' and definitions.id=crefset.referencedComponentId and conceptId= ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
        if not recordsetIn2 is None:
            for term in recordsetIn2:
                tempDefn = (term[0], term[2] + ' (*)')
                defn.append(tempDefn)
    termList.append(fsn)
    termList.append(pt)
    termList.append(syns)
    termList.append(defn)
    cursorIn2.close()
    connectionIn2.close()
    return termList

def hasDifferentTagTerm(idIn, focusFSN):
    focusFSNTag = focusFSN[focusFSN.rfind("("):]
    fsn = fsnFromConId_IN(idIn[0])[1]
    insertPoint = fsn.rfind("(")
    fsnTag = fsn[insertPoint:]
    if fsnTag != focusFSNTag:
        newTag = " \033[36;2m" + fsn[insertPoint:] + "\033[92;22m"
        newTag = newTag.replace("(", "[")
        newTag = newTag.replace(")", "]")
        return pterm(idIn) + newTag
    else:
        return pterm(idIn)

def onlyDefTermsFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    # terms
    cursorIn2.execute("select descriptionsdx.id, descriptionsdx.term FROM descriptionsdx, rdsrows WHERE descriptionsdx.id=rdsrows.id AND descriptionsdx.typeId = " + tok['DEFINIT'] + " AND rdsrows.acc = " + tok['PREFTERM'] + " AND descriptionsdx.conceptId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    termList = []
    defn = []
    tempDefn = ()
    for term in recordsetIn2:
        tempDefn = (term[0], term[1])
        defn.append(tempDefn)
    if defn == []: # if no definitions from descriptionsdx, check en-GB LRS
        cursorIn2.execute("select definitions.id, definitions.conceptId, definitions.term from definitions, crefset where definitions.active=1 and crefset.active=1 and crefset.refsetId='" + tok['GB_ENG'] + "' and definitions.id=crefset.referencedComponentId and conceptId= ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
        if not recordsetIn2 is None:
            for term in recordsetIn2:
                tempDefn = (term[0], term[2] + ' (*)')
                defn.append(tempDefn)
    termList.append(defn)
    cursorIn2.close()
    connectionIn2.close()
    # if termList != [[]]:
    return termList[0][0]
    # else:
    #     return []

def inactiveTermsFromConIdPlusMeta_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn2 = connectionIn2.cursor()
    cursorIn1.execute("attach '" + snapPath + "' as RF2Snap")
    # terms
    cursorIn2.execute("select descriptions.id, descriptions.term, descriptions.typeId, descriptions.moduleId, descriptions.effectiveTime, descriptions.caseSignificanceId, descriptions.active FROM descriptions WHERE descriptions.active=0 AND descriptions.conceptId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    termList = []
    for term in recordsetIn2:
        termList.append(term)
    # definitions
    cursorIn2.execute("select definitions.id, definitions.term, definitions.typeId, definitions.moduleId, definitions.effectiveTime, definitions.caseSignificanceId, definitions.active FROM definitions WHERE definitions.active=0 AND definitions.conceptId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    for term in recordsetIn2:
        termList.append(term)
    cursorIn2.close()
    connectionIn2.close()
    return termList


def otherTermsFromConIdPlusMeta_IN(idIn, allRDSTermIds):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    # non-rds/rlr covered terms/inactive terms
    othersList = []
    cursorIn2.execute("select descriptions.id, descriptions.conceptId, descriptions.term, descriptions.typeId, descriptions.moduleId, descriptions.effectiveTime, descriptions.caseSignificanceId, descriptions.active from descriptions where conceptId= ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    for term in recordsetIn2:
        tempOther = ()
        if term[0] not in allRDSTermIds:
            tempOther = (term[0], term[2], pterm(term[3]), term[5], pterm(term[4]), pterm(term[6]), term[7])
            othersList.append(tempOther)
    cursorIn2.execute("select definitions.id, definitions.conceptId, definitions.term, definitions.typeId, definitions.moduleId, definitions.effectiveTime, definitions.caseSignificanceId, definitions.active from definitions where conceptId= ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    for term in recordsetIn2:
        tempOther = ()
        if (term[7] == 0) or (term[0] not in allRDSTermIds):
            tempOther = (term[0], term[2], pterm(term[3]), term[5], pterm(term[4]), pterm(term[6]), term[7])
            othersList.append(tempOther)
    cursorIn2.close()
    connectionIn2.close()
    othersList.sort(key=lambda x: (x[2], x[1]))
    return othersList


def allTermsFromConIdPlusMeta_IN(idIn, inVersion):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn2 = connectionIn2.cursor()
    cursorIn1.execute("attach '" + snapPath + "' as RF2Snap")
    # active terms
    cursorIn2.execute("select descriptionsdx.id, descriptionsdx.term, descriptionsdx.typeId, rdsrows.acc, descriptionsdx.moduleId, descriptionsdx.effectiveTime, descriptionsdx.caseSignificanceId FROM descriptionsdx, rdsrows WHERE descriptionsdx.id=rdsrows.id AND descriptionsdx.conceptId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    termList = []
    fsn = []
    tempFsn = ()
    pt = []
    tempPt = ()
    syns = []
    tempSyn = ()
    if inVersion == "Int,":
        RLRString = tok['RLR_CLIN'] + "','" + tok['RLR_PHARM']
    else:
        RLRString = tok['GB_ENG'] + "','" + tok['GB_ENG'] # repeat speeds up query (? slow with single 'in' )
    for term in recordsetIn2:
        if term[2] == tok['FSN'] and term[3] == tok['PREFTERM']:
            cursorIn1.execute("select RF2Snap.crefset.moduleId, RF2Snap.crefset.effectiveTime FROM RF2Snap.crefset WHERE RF2Snap.crefset.refsetId IN ('" + RLRString + "') AND RF2Snap.crefset.active=1 AND RF2Snap.crefset.referencedComponentId = ?", tuplify(term[0]))
            recordsetIn1 = cursorIn1.fetchone()
            tempFsn = (term[0], term[1], pterm(term[4]), term[5], pterm(term[6]), pterm(recordsetIn1[0]), recordsetIn1[1])
            fsn.append(tempFsn)
            cursorIn1.close
        elif term[2] == tok['SYNONYM'] and term[3] == tok['PREFTERM']:
            cursorIn1.execute("select RF2Snap.crefset.moduleId, RF2Snap.crefset.effectiveTime FROM RF2Snap.crefset WHERE RF2Snap.crefset.refsetId IN ('" + RLRString + "') AND RF2Snap.crefset.active=1 AND RF2Snap.crefset.referencedComponentId = ?", tuplify(term[0]))
            recordsetIn1 = cursorIn1.fetchone()
            tempPt = (term[0], term[1], pterm(term[4]), term[5], pterm(term[6]), pterm(recordsetIn1[0]), recordsetIn1[1])
            pt.append(tempPt)
            cursorIn1.close
        elif term[2] == tok['SYNONYM'] and term[3] == tok['ACCEPTABLE']:
            cursorIn1.execute("select RF2Snap.crefset.moduleId, RF2Snap.crefset.effectiveTime FROM RF2Snap.crefset WHERE RF2Snap.crefset.refsetId IN ('" + RLRString + "') AND RF2Snap.crefset.active=1 AND RF2Snap.crefset.referencedComponentId = ?", tuplify(term[0]))
            recordsetIn1 = cursorIn1.fetchone()
            tempSyn = (term[0], term[1], pterm(term[4]), term[5], pterm(term[6]), pterm(recordsetIn1[0]), recordsetIn1[1])
            syns.append(tempSyn)
            cursorIn1.close
    defn = []
    tempDefn = ()
    cursorIn2.execute("select definitions.id, definitions.conceptId, definitions.term, definitions.typeId, definitions.moduleId, definitions.effectiveTime, definitions.caseSignificanceId, crefset.moduleId, crefset.effectiveTime from definitions, crefset where definitions.active=1 and crefset.active=1 and crefset.refsetId IN ('" + RLRString + "') and definitions.id=crefset.referencedComponentId and conceptId= ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    if not recordsetIn2 is None:
        for term in recordsetIn2:
            if term[3] == tok['DEFINIT']:
                tempDefn = (term[0], term[2], pterm(term[4]), term[5], pterm(term[6]), pterm(term[7]), term[8])
                defn.append(tempDefn)
    termList.append(fsn)
    termList.append(pt)
    termList.append(syns)
    termList.append(defn)
    cursorIn2.close()
    connectionIn2.close()
    return termList


def ptFromConId_IN(idIn):
    term = allTermsFromConId_IN(idIn)[1][0]
    return term


def fsnFromConId_IN(idIn):
    term = allTermsFromConId_IN(idIn)[0][0]
    return term


def synsFromConId_IN(idIn):
    syns = []
    syns = allTermsFromConId_IN(idIn)[2]
    return syns


def defnFromConId_IN(idIn):
    term = allTermsFromConId_IN(idIn)[3][0]
    return term


def ptFromConIdPlusMeta_IN(idIn, inVersion):
    term = allTermsFromConIdPlusMeta_IN(idIn, inVersion)[1][0]
    return term


def fsnFromConIdPlusMeta_IN(idIn, inVersion):
    term = allTermsFromConIdPlusMeta_IN(idIn, inVersion)[0][0]
    return term


def synsFromConIdPlusMeta_IN(idIn, inVersion):
    syns = []
    syns = allTermsFromConIdPlusMeta_IN(idIn, inVersion)[2]
    return syns


def defnFromConIdPlusMeta_IN(idIn, inVersion):
    term = ""
    preTermList = allTermsFromConIdPlusMeta_IN(idIn, inVersion)[3]
    if len(preTermList) > 0:
        term = preTermList[0]
    return term


def allRolesFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select distinct typeId, destinationId from relationships where active=1 and sourceId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    roleList = []
    tempSet = ()
    concRoles = concreteTakingRoles()
    concVals = cfg("CONC_VALS")
    concData = cfg("CONC_DATA")
    isaTok = tok['IS_A']
    for role in recordsetIn2:
        #tempstring = pterm(role[0]) + "| |" + role[0]
        #roleList.append(tempstring.split("|"))
        # CONC - all roles cater for concretes
        if not role[0] == isaTok:
            if role[1] in concRoles and (concVals == 1 or concData == 1):
                if concRoles[role[1]] == 0:
                    tempSet = (role[0], pterm(role[0]), pterm(role[1]), "#" + toNumber(pterm(role[1])), "")
                else:
                    tempSet = (role[0], pterm(role[0]), pterm(role[1]), pterm(role[1]), "")
            else:
                tempSet = (role[0], pterm(role[0]), role[1], pterm(role[1]))
            roleList.append(tempSet)
    roleList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return roleList

def inactiveIsAsFromConId_IN(idIn, ET, equalitySign):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select distinct typeId, destinationId from relationships where sourceId = ? and effectiveTime " + equalitySign + " ?", (idIn, ET))
    recordsetIn2 = cursorIn2.fetchall()
    roleList = []
    isaTok = tok['IS_A']
    for role in recordsetIn2:
        if role[0] == isaTok:
            roleList.append(role[1])
    cursorIn2.close()
    connectionIn2.close()
    return roleList

def fasterRolesFromConId_IN(idIn, concRoles, concVals, concData, isaTok):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    # children
    cursorIn2.execute("select distinct typeId, destinationId from relationships where active=1 and sourceId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    roleList = []
    tempSet = ()
    for role in recordsetIn2:
        # CONC - all roles cater for concretes
        if not role[0] == isaTok:
            if role[1] in concRoles and (concVals == 1 or concData == 1):
                if concRoles[role[1]] == 0:
                    tempSet = (role[0], pterm(role[0]), pterm(role[1]), "#" + toNumber(pterm(role[1])), "")
                else:
                    tempSet = (role[0], pterm(role[0]), pterm(role[1]), pterm(role[1]), "")
            else:
                tempSet = (role[0], pterm(role[0]), role[1], pterm(role[1]))
            roleList.append(tempSet)
    roleList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return roleList

def groupedRolesFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    # children
    cursorIn2.execute("select relationshipGroup, typeId, destinationId from relationships where active=1 and sourceId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == tok['IS_A']:
            tempSet = (role[0] - 1, role[1], pterm(role[1]), role[2], pterm(role[2]))
            preRoleList.append(tempSet)
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == tok['IS_A']:
            # CONC - insert # + number + "" in place of id and term in definition row 2
            if role[1] in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
                if concreteTakingRoles()[role[1]] == 0:
                    tempSet = (role[0], role[1], pterm(role[1]), "#" + toNumber(pterm(role[2])), "")
                else:
                    tempSet = (role[0], role[1], pterm(role[1]), pterm(role[2]), "")
            else:
                tempSet = (role[0], role[1], pterm(role[1]), role[2], pterm(role[2]))
            preRoleList.append(tempSet)
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) == 0:
        return roleList
    else:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
        cursorIn2.close()
        connectionIn2.close()
        return roleList

def groupedDefiningRolesFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    # children
    cursorIn2.execute("select relationshipGroup, typeId, destinationId from relationships where (not characteristicTypeId='" + tok["ADDN_REL"] + "') and active=1 and sourceId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == tok['IS_A']:
            tempSet = (role[0] - 1, role[1], pterm(role[1]), role[2], pterm(role[2]))
            preRoleList.append(tempSet)
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == tok['IS_A']:
            # CONC - insert # + number + "" in place of id and term in definition row 1
            if role[1] in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
                if concreteTakingRoles()[role[1]] == 0:
                    tempSet = (role[0], role[1], pterm(role[1]), "#" + toNumber(pterm(role[2])), "")
                else:
                    tempSet = (role[0], role[1], pterm(role[1]), pterm(role[2]), "")
            else:
                tempSet = (role[0], role[1], pterm(role[1]), role[2], pterm(role[2]))
            preRoleList.append(tempSet)
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) == 0:
        return roleList
    else:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
        cursorIn2.close()
        connectionIn2.close()
        return roleList

def rolesAsTargetFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    # children
    cursorIn2.execute("select relationshipGroup, typeId, sourceId from relationships where active=1 and destinationId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == tok['IS_A']:
            tempSet = (role[0] - 1, role[1], pterm(role[1]) + ' [INV]', role[2], pterm(role[2]))
            preRoleList.append(tempSet)
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == tok['IS_A']:
            tempSet = (role[0], role[1], pterm(role[1]) + ' [INV]', role[2], pterm(role[2]))
            preRoleList.append(tempSet)
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) == 0:
        return roleList
    else:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
        cursorIn2.close()
        connectionIn2.close()
        return roleList


def groupedRolesFromConIdPlusMeta_IN(idIn, activeFlag):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    # children
    if activeFlag == 0:
        cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=0 and sourceId = ?", tuplify(idIn))
    elif activeFlag == 2:
        cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where sourceId = ?", tuplify(idIn))
    else:
        cursorIn2.execute("select relationshipGroup, typeId, destinationId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=1 and sourceId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        if role[1] == tok['IS_A']:
            tempSet = (role[0] - 1, role[1], pterm(role[1]), role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            preRoleList.append(tempSet)
            if not (role[0] - 1) in rgIntList:
                rgIntList.append(role[0] - 1)
        if not role[1] == tok['IS_A']:
            # CONC - grouped roles plus metadata
            if role[1] in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
                if concreteTakingRoles()[role[1]] == 0:
                    tempSet = (role[0], role[1], pterm(role[1]), "#" + toNumber(pterm(role[2])), "", pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
                else:
                    tempSet = (role[0], role[1], pterm(role[1]), pterm(role[2]), "", pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            else:
                tempSet = (role[0], role[1], pterm(role[1]), role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8])
            preRoleList.append(tempSet)
            if not role[0] in rgIntList:
                rgIntList.append(role[0])
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList


def childrenFromConIdPlusMeta_IN(idIn, activeFlag, invRolesFlag):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    # children
    if activeFlag == 0:
        cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=0 and destinationId = ?", tuplify(idIn))
    elif activeFlag == 2:
        cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where destinationId = ?", tuplify(idIn))
    else:
        cursorIn2.execute("select relationshipGroup, typeId, sourceId, moduleId, effectiveTime, characteristicTypeId, modifierId, active, id from relationships where active=1 and destinationId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    rgIntList = []
    tempSet = ()
    for role in recordsetIn2:
        cCount = childCountFromId_IN(tuplify(role[2]))
        if cCount > 2000:
            cCountStr = "\033[31m" + str(cCount) + "\033[36m"
        else:
            cCountStr = str(cCount)
        if role[1] == tok['IS_A']:
            tempSet = (role[0] - 2, role[1], pterm(role[1]) + ' [INV]', role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8], cCountStr)
            preRoleList.append(tempSet)
            if not (role[0] - 2) in rgIntList:
                rgIntList.append(role[0] - 2)
        if (role[1] != tok['IS_A']) and (invRolesFlag == 1):
            tempSet = (role[0] - 2, role[1], pterm(role[1]) + ' [INV]', role[2], pterm(role[2]), pterm(role[3]), role[4], pterm(role[5]), pterm(role[6]), role[7], role[8], cCountStr)
            preRoleList.append(tempSet)
            if not (role[0] - 2) in rgIntList:
                rgIntList.append(role[0] - 2)
    preRoleList.sort(key=lambda x: (x[2], x[4]))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        for roleCount in sorted(rgIntList):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    cursorIn2.close()
    connectionIn2.close()
    return roleList

def charEquivTester(inTerm):
    equivList = readInCharEquivs(cfg('LOCALE'))
    tempList = [[[], inTerm.lower()]]
    outList = equivTester(tempList, equivList)
    return outList

def wordEquivTester(inTerm):
    equivList = readInWordEquivs()
    tempList = [[[], inTerm.lower()]]
    outList = equivTester(tempList, equivList)
    return outList

def searchOnText_IN(initialTerm, inOption, inLimit, showDefs):
    #first entry in list returned is number of active and inactive matches
    equivCharList = []
    equivWordList = []
    equivWordListTotal = []
    searchList = []
    searchListTotal = []
    searchText = []
    activeCon = 0
    inactiveCon = 0
    onlyShowDefs = False
    if initialTerm[0] == '*': # stops automatically picking up defintions for ''c' type searches
        showDefs = False
        initialTerm = initialTerm[1:]
    if initialTerm[0] == '@':
        showDefs = True
        initialTerm = initialTerm[1:]
        onlyShowDefs = True
    if initialTerm[0] == '[' or initialTerm[-1] == ']' or initialTerm[-1] == ' ': # term ends or starts with/token ends with
        endsOrStartsWith = True
    else:
        endsOrStartsWith = False
    if initialTerm[0] == '[' and initialTerm[-1] == ']': # term ends or starts with
        initialTerm = initialTerm[1:-1]
        inLimit = 1
    if (initialTerm[0] == '"' and initialTerm[-1] == '"') or (initialTerm[0] == "'" and initialTerm[-1] == "'"): # phrase match
        initialTerm = initialTerm[1:-1]
        inLimit = 3
    # 1. generate char eqiv list
    # import addn
    equivCharList = charEquivTester(initialTerm)
    f = io.open(cfgPathInstall() + 'files' + sep() + 'search' + sep() + 'searchTerms1.txt', 'w', encoding="UTF-8")
    for term in equivCharList:
        f.write(term + '\n')
    f.close()
    for equivCharTerm in equivCharList:
        # 3. generate word equiv list
        if cfg('WORD_EQUIV') > 0:
            equivWordList = wordEquivTester(equivCharTerm)
        else:
            equivWordList.append(equivCharTerm)
        # 4. for loop for each member of equivWordList
        for equivWord in equivWordList:
            if equivWord not in equivWordListTotal:
                equivWordListTotal.append(equivWord)
        for equivTerm in equivWordList:
            searchText = []
            tempList = re.split(' ', unidecode(equivTerm))  # only split tokens on spaces (unidecoded set)
            tempListAcc = re.split(' ', equivTerm)  # only split tokens on spaces (unmodified set)
            predicateTerm = ""
            matchLike = 'match'
            equalLike = '='
            if (inLimit == 0) or (inLimit == 2):
                for term in tempList:
                    if (not term == "") and (not term in "%*'\""): #not blank and not SQL reserved character
                        if initialTerm[0] == " ": # match tokens exactly
                            term = " " + term + "  NEAR "
                        elif initialTerm[0] == "[": # term opens with
                            term = term[1:] + "% NEAR "
                            matchLike = 'like'
                        elif initialTerm[-1] == " ": # token ends with
                            term = "%" + term + "% NEAR "
                            matchLike = 'like'
                        elif initialTerm[-1] == "]": # term ends with
                            term = "%" + term[:-1] + " NEAR "
                            matchLike = 'like'
                        else: # token starts with (default)
                            term = term.strip() + "* NEAR "
                        predicateTerm += term
                searchText.append(predicateTerm[0:-6])  # trim final 'NEAR'
            elif inLimit == 1:
                searchText.append(unidecode(equivTerm.upper()))
            elif inLimit == 3:
                searchText.append("%" + unidecode(equivTerm.upper()) + "%")
                equalLike = 'like'
            connectionIn2 = sqlite3.connect(outPath)
            cursorIn2 = connectionIn2.cursor()
            cursorIn1 = connectionIn2.cursor()
            cursorIn1.execute("attach '" + snapPath + "' as RF2Snap")
            if showDefs:
                if onlyShowDefs:
                    addnCondition =  " and typeId = \'" + tok["DEFINIT"] + "\'"
                else:
                    addnCondition = ""
            else:
                addnCondition =  " and typeId <> \'" + tok["DEFINIT"] + "\'"
            if (inLimit == 0) or (inLimit == 2):
                if (inOption == 2) or (inOption == 0):
                    cursorIn2.execute("select conceptId, term, rdstype, id from unideterms where unideterm " + matchLike + " ?" + addnCondition, (searchText[0],))
                elif inOption == 1:
                    cursorIn2.execute("select conceptId, term, rdstype, id from unideterms where rdstype is not Null and unideterm " + matchLike + " ?" + addnCondition, (searchText[0],))
            elif inLimit == 1 or inLimit == 3:
                if (inOption == 2) or (inOption == 0):
                    cursorIn2.execute("select conceptId, term, rdstype, id from unideterms where unideterm " + equalLike + " ?" + addnCondition, (searchText[0],))
                elif inOption == 1:
                    cursorIn2.execute("select conceptId, term, rdstype, id from unideterms where rdstype is not Null and unideterm " + equalLike + " ?" + addnCondition, (searchText[0],))
            recordsetIn2 = cursorIn2.fetchall()
            for match in recordsetIn2:
                if not match is None:
                    tempListItemCounter = 0
                    if tempListAcc == tempList and cfg('LOCALE') == 'en': # if no accented stuff changed and if 'en' locale
                        for item in tempList:
                            if  inStart(unidecode(item.upper()), unidecode(match[1].upper())) or endsOrStartsWith:
                                tempListItemCounter += 1
                    else: # non-en locale or accented search
                        for item in tempListAcc:
                            if inStart(item.upper(), match[1].upper()) or endsOrStartsWith:
                                tempListItemCounter += 1
                    if tempListItemCounter == len(tempList):
                        cursorIn1.execute("select RF2Snap.concepts.active from RF2Snap.concepts where RF2Snap.concepts.id = ?", tuplify(match[0]))
                        if [match[1], match[0], match[2]] not in searchListTotal: # avoids multiple counting of inactive matches
                            searchListTotal.append([match[1], match[0], match[2]])
                            tempstring = ""
                            tempResult = cursorIn1.fetchone()
                            if tempResult is None:
                                tempResult = (0,)
                            if tempResult[0] == 1:  # if term of an active concept
                                if inOption == 1:
                                    tempstring = match[1] + "| |" + match[0]
                                if (inOption == 2):
                                    if match[2] is None:
                                        if (ptermDid(tuplify(match[3]))[-14:] == '(Acti. DefnID)') or (ptermDid(tuplify(match[3]))[-14:] == '(Acti. DescID)'): # covers active defs/descs not in NHS RLR
                                            tempstring = match[1] + "|+|" + match[0]
                                        else:
                                            tempstring = match[1] + "|-|" + match[0]
                                    else:
                                        tempstring = match[1] + "| |" + match[0]
                                if (inOption == 0):
                                    if match[2] is None:
                                        if (ptermDid(tuplify(match[3]))[-14:] != '(Acti. DefnID)') and (ptermDid(tuplify(match[3]))[-14:] != '(Acti. DescID)'): # excludes active defs not in NHS RLR
                                            tempstring = match[1] + "|-|" + match[0]
                                if "|" in tempstring:
                                    if tempstring.split("|") not in searchList:
                                        searchList.append(tempstring.split("|"))
                                        activeCon += 1
                            else:
                                if inOption == 1:
                                    inactiveCon += 1
                                if (inOption == 0) or (inOption == 2):
                                    tempstring = match[1] + "|*|" + match[0]
                                    if tempstring.split("|") not in searchList:
                                        searchList.append(tempstring.split("|"))
                                        inactiveCon += 1
    # searchList now the union of all charEquiv/wordEquiv searches
    f = io.open(cfgPathInstall() + 'files' + sep() + 'search' + sep() + 'searchTerms2.txt', 'w', encoding="UTF-8")
    for term in equivWordListTotal:
        f.write(term + '\n')
    f.close()
    searchList.sort(key=lambda x: (len(x[0]), x[0]))
    tempstring = str(activeCon) + "|" + str(inactiveCon)
    searchList.insert(0, tempstring.split("|"))  # prepend the number of hits (active and inactive)
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()
    return searchList

def inStart(inString1, inString2):
    # check first character
    firstBool = False
    if inString1 == inString2[0:len(inString1)]:
        firstBool = True
    # check later characters after spaces and other potential dividing characters
    laterBool = False
    for char in " .,/=-+><^?@~{}[]'\"*():;#":
        if char + inString1 in inString2[1:]:
            laterBool = True
    if firstBool or laterBool:
        return True
    else:
        return False

def conMetaFromConId_IN(idIn, ptermBool):
    conMetaSet = ()
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn2.cursor()

    cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.effectiveTime, concepts.definitionStatusId from concepts where concepts.id = ?", tuplify(idIn))
    recordsetIn1 = cursorIn1.fetchone()
    if recordsetIn1 is not None:
        if recordsetIn1[1] == 0:
            cursorIn2 = connectionIn2.cursor()
            cursorIn2.execute("select componentId1 from crefset where refsetId='" + tok['INAC_CON'] + "' and active=1 and referencedComponentId= ?", tuplify(idIn))
            recordsetIn2 = cursorIn2.fetchall()
            if len(recordsetIn2) == 1:
                reasonForInactivation = pterm(recordsetIn2[0])
            conMetaSet = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]), reasonForInactivation)
            cursorIn2.close()
        else:
            if ptermBool:
                conMetaSet = (pterm(recordsetIn1[0]), recordsetIn1[1], recordsetIn1[2], pterm(recordsetIn1[3]))
            else:
                conMetaSet = (recordsetIn1[0], recordsetIn1[1], recordsetIn1[2], recordsetIn1[3])
    cursorIn1.close()
    connectionIn2.close()
    return conMetaSet

def isActiveFromConId_IN(idIn):
    conMetaSet = ()
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn2.cursor()

    cursorIn1.execute("select concepts.moduleId, concepts.active, concepts.definitionStatusId from concepts where concepts.id = ?", tuplify(idIn))
    recordsetIn1 = cursorIn1.fetchone()
    if recordsetIn1 is not None:
        conMetaSet = (pterm(recordsetIn1[0]), recordsetIn1[1], pterm(recordsetIn1[2]))
    else:
        conMetaSet = ("Not found in this data", 0, "Primitive")
    cursorIn1.close()
    connectionIn2.close()
    return conMetaSet

def desMetaFromDesId_IN(idIn):
    reasonForInactivation = "Reason not stated"  # handles nothing returned on inner query (not in reason for inactivation)
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select componentId1 from crefset where refsetId='" + tok['INAC_DES'] + "' and active=1 and referencedComponentId= ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    if len(recordsetIn2) == 1:
        reasonForInactivation = pterm(recordsetIn2[0])
    cursorIn2.close()
    connectionIn2.close()
    return reasonForInactivation

def conModCheckFromConId_IN(idIn):
    # checks for module dependency muddles.
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn2 = connectionIn2.cursor()
    cursorIn1.execute("select concepts.moduleId from concepts where concepts.id = ?", idIn)
    recordsetIn1 = cursorIn1.fetchall()
    anyBool = False
    for row in recordsetIn1:
        # checks for 'depended on' mod+et pairs (RCID+T2) that don't themselves 'depend on' (MODID+T1) something else.
        cursorIn2.execute("select distinct (referencedComponentId || '|' || string2) as jn from ssrefset where active=1 and moduleId=? \
                            except \
                            select distinct (moduleId || '|' || string1) as jn from ssrefset where (moduleId || '|' || string1) in \
                            (select (referencedComponentId || '|' || string2) from ssrefset where active=1 and moduleId=?)", (row[0], row[0],))
        recordsetIn2 = cursorIn2.fetchall()
        if not(len(recordsetIn2) == 1): # if more than just a single row (international model component - 'top')
            anyBool = True # assume an odd MDR tree for this module
    cursorIn1.close()
    cursorIn2.close()
    connectionIn2.close()
    if anyBool and not(recordsetIn1[0][0] == tok['MODEL']):
        return True
    else:
        return False

def conModuleFromConId_IN(idIn):
    conMod = ()
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn1.execute("select concepts.moduleId from concepts where concepts.id = ?", tuplify(idIn))
    recordsetIn1 = cursorIn1.fetchone()
    conMod = (pterm(recordsetIn1))
    cursorIn1.close()
    connectionIn2.close()
    return conMod


def conDefStatusFromConId_IN(idIn):
    conStatus = ()
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn1.execute("select concepts.definitionStatusId from concepts where concepts.id = ?", tuplify(idIn))
    recordsetIn1 = cursorIn1.fetchone()
    conStatus = (pterm(recordsetIn1))
    cursorIn1.close()
    connectionIn2.close()
    return conStatus


def refSetFromConId_IN(idIn, refsetType):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select distinct refsetId from " + refsetType + " where active=1 and referencedComponentId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = (refSet[0], pterm(refSet[0]))
        refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

def checkConIdVsRefset_IN(idIn, refsetId):
    refsetType = refSetTypeFromConId_IN(refsetId)[0][0]
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select referencedComponentId from " + refsetType + " where active=1 and referencedComponentId = ? and refsetId = ?", (idIn, refsetId))
    recordsetIn2 = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    if len(recordsetIn2) > 0:
        return True
    else:
        return False

def nonRCIDRefSetFromConId_IN(idIn, refsetType):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    fieldList = []
    refSetList = []
    fullFieldList = findRefsetMetaFields(refsetType).split(", ")
    fullFieldDict = [x for x in dict(zip(range(len(fullFieldList)), fullFieldList)).items()]
    # component fields
    fieldList = [field for field in [fieldInner for fieldInner in findRefsetMetaFields(refsetType).split(", ") if len(fieldInner) > 0] if field[0] == 'c'] # identify any component fields
    for field in fieldList:
        cursorIn2.execute("select distinct refsetId from " + refsetType + " where active=1 and " + field + " = ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for refSet in recordsetIn2:
            trueField = getDescriptorField(fullFieldDict, field, refSet)
            tempSet = (refSet[0], pterm(refSet[0]) + ' (\'' + trueField + '\')')
            refSetList.append(tempSet)
        refSetList.sort(key=lambda x: x[1])
    # string fields
    fieldList = [field for field in [fieldInner for fieldInner in findRefsetMetaFields(refsetType).split(", ") if len(fieldInner) > 0] if field[0] == 's'] # identify any string fields
    for field in fieldList:
        cursorIn2.execute("select distinct refsetId from " + refsetType + " where active=1 and " + field + " like ?", tuplify('%' + idIn + '%'))
        recordsetIn2 = cursorIn2.fetchall()
        tempSet = ()
        for refSet in recordsetIn2:
            trueField = getDescriptorField(fullFieldDict, field, refSet)
            tempSet = (refSet[0], pterm(refSet[0]) + ' (in \'' + trueField + '\')')
            refSetList.append(tempSet)
        refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

def getDescriptorField(fullFieldDict, field, refSet):
    returnField = field
    descriptorList = refSetDescriptorFromConId_IN(refSet[0])
    for row in fullFieldDict:
        if row[1] == field:
            returnField = descriptorList[row[0]][3]
    return returnField

def refSetFromConIdPlusMeta_IN(idIn, refsetType, inOption):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    if inOption == 0:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=0 and referencedComponentId = ?", tuplify(idIn))
    elif inOption == 2:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where referencedComponentId = ?", tuplify(idIn))
    else:
        cursorIn2.execute("select distinct moduleId, refsetId, effectiveTime, active from " + refsetType + " where active=1 and referencedComponentId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = (refSet[1], pterm(refSet[1]), refSet[0], pterm(refSet[0]), refSet[2], refSet[3])
        refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def refSetMembersFromConIdPlusMeta_IN(idIn, refsetType, myfilter, mycount):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    tempstring = findRefsetMetaFields(refsetType)
    if myfilter == 1:  # 1/10
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=1 and refsetId = ? limit ?", (idIn, int(round(mycount / 10))))
    elif myfilter == 2:  # 1/100
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=1 and refsetId = ? limit ?", (idIn, int(round(mycount / 100))))
    elif myfilter == 3:  # 1/10000
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=1 and refsetId = ? limit ?", (idIn, int(round(mycount / 2000))))
    else:
        cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refsetRow in recordsetIn2:
        tempSet = refsetRecordsetMetaDecomp(refsetRow, refsetType)
        tempSet2 = [item for item in tempSet]
        tempSet2.append(refsetRow[-1])
        refSetList.append(tempSet2)
    if refsetType[0:3] == 'cci':
        refSetList.sort(key=lambda x: (x[0], x[3]))  # orders descriptor block
    elif refsetType[0:5] == 'iisss':
        refSetList.sort(key=lambda x: (x[4], x[5]))  # orders classification by group/priority
    elif refsetType[0:2] == 'ss':
        refSetList.sort(key=lambda x: x[3])  # orders classification by group/priority
    else:
        refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def refSetMembersFromConId_IN(idIn, refsetType, myfilter, mycount):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    tempstring = findRefsetMetaFields(refsetType)
    if myfilter == 1:  # 1/10
        cursorIn2.execute("select referencedComponentId" + tempstring + " from " + refsetType + " where active=1 and refsetId = ? limit ?", (idIn, int(round(mycount / 10))))
    elif myfilter == 2:  # 1/100
        cursorIn2.execute("select referencedComponentId" + tempstring + " from " + refsetType + " where active=1 and refsetId = ? limit ?", (idIn, int(round(mycount / 100))))
    elif myfilter == 3:  # 1/10000
        cursorIn2.execute("select referencedComponentId" + tempstring + " from " + refsetType + " where active=1 and refsetId = ? limit ?", (idIn, int(round(mycount / 2000))))
    else:
        cursorIn2.execute("select referencedComponentId" + tempstring + " from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = refsetRecordsetDecomp(refSet, refsetType)
        refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[0])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

def maxETForRefset_IN(idIn, refsetType):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select max(effectiveTime) from " + refsetType + " where active=1 and refsetId = ?", (idIn,))
    maxET = cursorIn2.fetchone()[0]
    cursorIn2.close()
    connectionIn2.close()
    return maxET

def readFileMembers(inPath): # reads back members of a file
    memberList = []
    f = io.open(inPath, 'r', encoding="UTF-8")
    for line in f:
        memberList.append(line.strip())
    f.close()
    return memberList

def refSetMembersCompareFromConIds_IN(idIn1, idIn2, refsetType, topLevelOrRefsets, chapterDict, refsetDict, debugBool, memberReturn):
    # overloaded method - should be split up:
    # used for data build (hence the debug=True progress print clause in a helper module)
    # used for letter set comparision (detected as len(IdIn1) == 1)
    # - currently called with separate idIn2's - to indicate progress in funcs code, but can be called with blank idIn2 (and refset idIn2s generated here)
    # calculates comparison data for both top level concepts and refsets
    # compares letter membership with only simple refsets
    # compares between refsets of the same pattern (and only compares rsids)
    # has capability to return actual members matched, but currently this is hanlded by creating equivalent clause sets ('I' & 'J' from snapshot)
    idIn2List = []
    if topLevelOrRefsets == 1:
        idIn2List = [child[2] for child in childrenFromId_IN((tok['ROOT'],))]
    else:
        if not idIn2 == "":
            idIn2List.append(idIn2)
        else:
            if len(idIn1) == 1: # if it's a letter set comparison
                idIn2List = [item[0] for item in descendantsFromConId_IN(tok["SIMPLE_REFSETS"])]
            else:
                idIn2List = siblingRefsets(idIn1)
    comparisonList = []
    connectionIn2 = sqlite3.connect(snapPath)
    cursorInLeft = connectionIn2.cursor()
    cursorInLeft.execute("attach '" + outPath + "' as RF2Out")
    cursorInRight = connectionIn2.cursor()
    if len(idIn1) == 1:
        recordsetInLeft = [(a,) for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + idIn1 + '.txt')]
    else:
        if idIn1 in refsetDict:
            recordsetInLeft = refsetDict[idIn1]
        else:
            cursorInLeft.execute("select referencedComponentId from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn1))
            recordsetInLeft = cursorInLeft.fetchall()
            refsetDict[idIn1] = recordsetInLeft
    recordsetInLeftCount = len(recordsetInLeft)
    counter = 1
    for idIn2 in idIn2List:
        recordsetInRight = ()
        if topLevelOrRefsets == 1:
            if idIn2 in refsetDict:
                recordsetInRight = refsetDict[idIn2]
            else:
                cursorInRight.execute("select subtypeId from RF2Out.tc where supertypeId = ?", tuplify(idIn2))
                recordsetInRight = cursorInRight.fetchall()
                refsetDict[idIn2] = recordsetInRight
        else:
            if counter % 50 == 0 and debugBool:
                print(str(counter))
            if idIn2 in chapterDict:
                if set(chapterDict[idIn1]) & set(chapterDict[idIn2]): # if the refsets match on at least one major chapter...
                    if idIn2 in refsetDict:
                        recordsetInRight = refsetDict[idIn2]
                    else:
                        cursorInRight.execute("select referencedComponentId from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn2))
                        recordsetInRight = cursorInRight.fetchall()
                        refsetDict[idIn2] = recordsetInRight
        recordsetInRightCount = len(recordsetInRight)
        if len(idIn1) != 1:
            onlyLeft = tuple(NOT(set(recordsetInLeft), set(recordsetInRight)))
            onlyLeftCount = len(onlyLeft)
            onlyRight = tuple(NOT(set(recordsetInRight), set(recordsetInLeft)))
            onlyRightCount = len(onlyRight)
        else:
            onlyLeftCount = 0
            onlyRightCount = 0
        intersection = tuple(AND(set(recordsetInRight), set(recordsetInLeft)))
        intersectionCount = len(intersection)
        if intersectionCount > 0:
            if memberReturn == 1:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, onlyLeft])
            elif memberReturn == 2:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, intersection])
            elif memberReturn == 2:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, onlyRight])
            else:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount])
        counter += 1
    cursorInRight.close()
    cursorInLeft.close()
    connectionIn2.close()
    comparisonList.sort(key=lambda x: (x[3], x[6]), reverse=True)
    if len(idIn1) == 1:
        return len(comparisonList), comparisonList, refsetDict
    else:
        return recordsetInLeftCount, comparisonList, refsetDict

def refSetMembersCompareRefTableFromConIds_IN(idIn1, idIn2, topLevelOrRefsets):
    comparisonList = []
    connectionIn2 = sqlite3.connect(outPath)
    cursorInLeft = connectionIn2.cursor()
    if parentsFromId_IN(tuplify(idIn1))[0][1] == tok["ROOT"]:
        topLevelCallBool = True
    else:
        topLevelCallBool = False
    if idIn2 == "":
        if topLevelCallBool:
            cursorInLeft.execute("select * from rscompare where compareType = ? and rightId = ?", (topLevelOrRefsets, idIn1))
        else:
            cursorInLeft.execute("select * from rscompare where compareType = ? and leftId = ?", (topLevelOrRefsets, idIn1))
        recordsetInLeft = cursorInLeft.fetchall()
        recordsetInLeftCount = len(recordsetInLeft)
        for row in recordsetInLeft:
            comparisonList.append(row)
    cursorInLeft.close()
    connectionIn2.close()
    comparisonList.sort(key=lambda x: (x[6], x[3]), reverse=True)
    return recordsetInLeftCount, comparisonList

def refSetChaptersRefTableFromConIds_IN(refsetDict, idIn2List):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn2.cursor()
    for idIn2 in idIn2List:
        tempList = []
        cursorIn1.execute("select rightId from rscompare where compareType = 1 and leftId = ?", (idIn2,))
        recordsetIn1 = cursorIn1.fetchall()
        if len(recordsetIn1) > 0:
            for item in recordsetIn1:
                tempList.append(item[0])
            refsetDict[idIn2] = tempList
    return refsetDict

def siblingRefsets(idIn):
    refsetList = []
    refsetPatternList = [ref[2] for ref in childrenFromId_IN(tuplify(tok["REFSETS"]))]
    refsetAncestors = ancestorsIDFromConId_IN(idIn)
    refsetAncestor = list(AND(set(refsetAncestors), set(refsetPatternList)))
    if len(refsetAncestor) == 1:
        refsetList = descendantsIDFromConId_IN(refsetAncestor[0])
        refsetList.remove(tuplify(idIn))
        refsetList = [refset[0] for refset in refsetList]
    return refsetList

def refSetDetailsFromMemIdandSetIdPlusMeta_IN(setId, memberId, inOption):
    refsetType = refSetTypeFromConId_IN(setId)[0][0]
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    tempstring = findRefsetMetaFields(refsetType)
    argumentList = []
    argumentList.append(memberId)
    argumentList.append(setId)
    if setId == tok['REFSETDESC']:  # needed because of active problem
        cursorIn2.execute("select moduleId, effectiveTime, active, referencedComponentId" + tempstring + " from " + refsetType + " where referencedComponentId = ? and refsetId = ?", argumentList)
    else:
        if inOption == 0:
            cursorIn2.execute("select moduleId, effectiveTime, active, referencedComponentId" + tempstring + " from " + refsetType + " where active=0 and referencedComponentId = ? and refsetId = ?", argumentList)
        elif inOption == 2:
            cursorIn2.execute("select moduleId, effectiveTime, active, referencedComponentId" + tempstring + " from " + refsetType + " where referencedComponentId = ? and refsetId = ?", argumentList)
        else:
            cursorIn2.execute("select moduleId, effectiveTime, active, referencedComponentId" + tempstring + " from " + refsetType + " where active=1 and referencedComponentId = ? and refsetId = ?", argumentList)

    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = refSet
        refSetList.append(tempSet)
    if refsetType == 'cci':
        refSetList.sort(key=lambda x: x[6])  # orders descriptor block
    elif refsetType[0:5] == 'iisss':
        refSetList.sort(key=lambda x: (x[4], x[5]))  # orders classification by group/priority
    else:
        refSetList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def refSetDetailsFromMemIdandSetId_IN(setId, memberId):
    refsetType = refSetTypeFromConId_IN(setId)
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    tempstring = findRefsetMetaFields(refsetType[0][0])
    argumentList = []
    argumentList.append(memberId)
    argumentList.append(setId)
    cursorIn2.execute("select referencedComponentId" + tempstring + ", moduleId, effectiveTime, active, id from " + refsetType[0][0] + " where active=1 and referencedComponentId = ? and refsetId = ?", argumentList)
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSetRow in recordsetIn2:
        tempSet = refSetRow
        refSetList.append(tempSet)
    if refsetType[0][0][0:3] == 'cci':
        refSetList.sort(key=lambda x: x[3])  # orders descriptor block
    elif refsetType[0][0][0:7] == 'iissscc':
        refSetList.sort(key=lambda x: (x[1], x[2]))  # orders classification by group/priority
    elif refsetType[0][0][0:7] == 'iisssci':
        refSetList.sort(key=lambda x: (x[7], x[1], x[2]))  # orders classification by block/group/priority
    else:
        refSetList.sort(key=lambda x: x[0])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList


def refSetMemberCountFromConId_IN(idIn, refsetType):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select count(referencedComponentId) as number from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchone()
    for row in recordsetIn2:
        refsetCount = row
    cursorIn2.close()
    connectionIn2.close()
    return refsetCount


def refSetTypeFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select filetype, moduleId from refsetModules where refsetId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    if len(recordsetIn2) > 0:
        for refSet in recordsetIn2:
            tempSet = (refSet[0], refSet[1], pterm(refSet[1]))
    else:
        tempSet = ("simplerefset", "not distributed", "not distributed")
    refSetList.append(tempSet)
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

def findModuleMaxET(moduleList, fileTypeList):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    moduleETDict = {}
    for module in moduleList:
        tempET = "0"
        for fileType in [file for file in  fileTypeList if file[0] != "ssrefset"]:
            cursorIn2.execute("select max(effectiveTime) from " + fileType[0] + " where moduleId =" + module[0])
            recordsetIn2 = cursorIn2.fetchall()
            if recordsetIn2[0][0] is not None:
                if recordsetIn2[0][0] > tempET:
                    tempET = recordsetIn2[0][0]
        if tempET == "0": # catch UK composition module
            cursorIn2.execute("select max(effectiveTime) from ssrefset where moduleId =" + module[0])
            recordsetIn2 = cursorIn2.fetchall()
            if recordsetIn2[0][0] is not None:
                tempET = recordsetIn2[0][0] + " *"
        moduleETDict[module[0]] = tempET
    cursorIn2.close()
    connectionIn2.close()
    return moduleETDict

def fileAndModuleDistribution_IN(fullModuleList):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    tempList = []
    # get modules
    moduleList = []
    cursorIn2.execute("select distinct moduleId from refsetModules")
    recordsetIn2 = cursorIn2.fetchall()
    for item in recordsetIn2:
        tempList = [item[0], pterm(item[0])]
        moduleList.append(tempList)
    moduleList.sort(key=lambda x: x[1].lower())
    # get filetypes
    fileTypeList = []
    cursorIn2.execute("select distinct filetype from refsetModules")
    recordsetIn2 = cursorIn2.fetchall()
    fileTypeList = recordsetIn2
    fileTypeList.sort(key=lambda x: x[0])
    moduleETDict = {}
    if fullModuleList:
        moduleETDict = findModuleMaxET(moduleList, fileTypeList)
    # get everything for grouping
    fullList = []
    cursorIn2.execute("select filetype, refsetId, moduleId from refsetModules")
    recordsetIn2 = cursorIn2.fetchall()
    fullList = recordsetIn2
    returnList = []
    doneTypeList = []
    doneSetList = []
    # for each filetype
    for fileType in fileTypeList:
        tempListOuter = []
        # for every full row
        for fullRowOuter in fullList:
            # if this full row is of the filetype and the type hasn't already been picked up
            if (fullRowOuter[0] == fileType[0]) and fullRowOuter[0] not in doneTypeList:
                # add to the type check list
                doneTypeList.append(fullRowOuter[1])
                if fullRowOuter[1] not in doneSetList:
                    # add the refsetId/term the tempListOuter
                    tempListOuter.append([fullRowOuter[1], pterm(fullRowOuter[1])])
                    # add to the doneSetList
                    doneSetList.append(fullRowOuter[1])
        tempListOuter.sort(key=lambda x: x[1])
        tempListOuter.insert(0, fileType[0])
        returnList.append(tempListOuter)
    # and then for every row in returnList
    for row in returnList:
        for refsetOrTable in row[1:]:
            tempListInner = []
            for fullRowInner in fullList:
                # if this refsetId matches
                if fullRowInner[1] == refsetOrTable[0]:
                    innerTempList = [fullRowInner[2], pterm(fullRowInner[2])]
                    # add the moduleId/term to the tempListInner
                    if innerTempList not in tempListInner:
                        tempListInner.append(innerTempList)
            # sort and tack this list of modules onto the end of the refsetId row
            tempListInner.sort(key=lambda x: x[1])
            refsetOrTable.append(tempListInner)
    cursorIn2.close()
    connectionIn2.close()
    return moduleList, returnList, moduleETDict

def activeColor_IN(inId, focusOrAncBool=False):
    if inId[-2] == "1": #if referenced component is a description/definition
        connectionIn2 = sqlite3.connect(snapPath)
        cursorIn2 = connectionIn2.cursor()
        cursorIn2.execute("select active from descriptions where id = ? UNION select active from definitions where id = ?", (inId, inId,))
        recordsetIn2 = cursorIn2.fetchone()
        if not recordsetIn2 is None:
            if recordsetIn2[0] == 1:
                return inId
            else:
                return '\033[31m' + inId + '\033[92m'
        else:
            return inId
    else:
        conMetaList = isActiveFromConId_IN(inId)
        if len(conMetaList) > 0:
            if conMetaList[1] == 1:
                if focusOrAncBool and conMetaList[2] == 'Defined':
                    return '\033[1m' + inId + '\033[22m'
                elif focusOrAncBool and conMetaList[2] == 'Primitive':
                    return '\033[2m' + inId + '\033[22m'
                else:
                    return inId
            else:
                return '\033[31m' + inId + '\033[92m'
        else:
            return inId

def attribColor_IN(inId):
    # CONC - relax level of concept model attributes
    if (inId == tok["CONMOD_ATTRIB"] and not checkAllAtts()): # if it's the attribute root and not all are active - red
        return '\033[31m' + inId + '\033[92m'
    elif inId in limitAttributes(True): # if it's a usable attribute and it's active - blue
        return '\033[36m' + inId + '\033[92m'
    elif inId in limitAttributes(False): # if it's a usable attribute and it's inactive - yellow
        return '\033[93m' + inId + '\033[92m'
    else:
        return inId

def activeColorOnTerm_IN(inId, inMarker):
    if inMarker == "*":
        return '\033[31m' + inId + '\033[92m'
    else:
        return activeColor_IN(inId, True)

def proportionColor_IN(inNo):
    if inNo >= 80.00:
        return '\033[31m(' + str(inNo) + '%)\033[92m'
    elif inNo >= 60.00:
        return '\033[93m(' + str(inNo) + '%)\033[92m'
    elif inNo >= 40.00:
        return '(' + str(inNo) + '%)'
    elif inNo >= 20.00:
        return '\033[36m(' + str(inNo) + '%)\033[92m'
    else:
        return '\033[95m(' + str(inNo) + '%)\033[92m'

def modColor_IN(inId, inTerm, inVersion):
    if inVersion == 'Int,': # if UK data
        testModList = prelim(ownModuleSet)
    else:
        testModList = prelim(intModuleSet)
    if tuplify(inId) in testModList:
        return inTerm
    else:
        return '\033[31m' + inTerm + '\033[92m'

def metaColor_IN(inId, inVersion, inRefsetRowModuleId='987654321'):
    testModList = []
    if inVersion == 'Int,': # if UK data
        testModList = prelim(ownModuleSet)
    else:
        testModList = prelim(intModuleSet)
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select moduleId from refsetModules where refsetId = ?", tuplify(inId))
    recordsetIn2 = cursorIn2.fetchall()
    if len(recordsetIn2) > 0:
        # if call did not come from refsetMemberList method, check the modules in the refset as a whole
        if inRefsetRowModuleId == '987654321':
            # - all valid members -> blue
            if len(set(recordsetIn2) & set(testModList)) == len(set(recordsetIn2)):
                returnString = '\033[36m'
            # - some valid members (fewer, but > 0) -> purple
            elif (len(set(recordsetIn2) & set(testModList)) < len(set(recordsetIn2))) and (len(set(recordsetIn2) & set(testModList)) > 0):
                returnString = '\033[95m'
            # no valid members - red
            else:
                returnString = '\033[31m'
        # if call from refsetmemberlist, check the module of each row.
        else:
            if tuplify(inRefsetRowModuleId) in testModList:
                # valid member - blue
                returnString = '\033[36m'
            else:
                # not valid member - red
                returnString = '\033[31m'
    else:
        returnString = '\033[36m'
    cursorIn2.close()
    connectionIn2.close()
    return returnString

def latestConcepts_IN(inVersion, inAllBool):
    latestETList = []
    conceptList = []
    conceptIdList = []
    if inVersion == 'Int,': # if UK data
        if inAllBool:
            latestETList = latestET(ownModuleSet)
        else:
            latestETList = [sorted(latestET(ownModuleSet))[-1]]
    else:
        if inAllBool:
            latestETList = latestET(intModuleSet)
        else:
            latestETList = [sorted(latestET(intModuleSet))[-1]]
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    mstring = ', '.join(map(str, latestETList))
    queryString = "select id, active from Concepts where effectiveTime in (" + mstring + ")"
    cursorIn2.execute(queryString)
    recordsetIn2 = cursorIn2.fetchall()
    for concept in recordsetIn2:
        tempSet = (concept[0], pterm(concept), concept[1])
        conceptList.append(tempSet)
        conceptIdList.append(concept[0])
    conceptList.sort(key=lambda x: x[1].lower())
    descriptionList = []
    queryString = "select conceptId, id, term, active from Descriptions where effectiveTime in (" + mstring + ")"
    cursorIn2.execute(queryString)
    recordsetIn2 = cursorIn2.fetchall()
    for description in recordsetIn2:
        if description[0] not in conceptIdList:
            tempSet = (description[0], description[1], description[2], description[3])
            descriptionList.append(tempSet)
    descriptionList.sort(key=lambda x: (x[0], x[2].lower()))

    cursorIn2.close()
    connectionIn2.close()
    return conceptList, descriptionList

def refSetDescriptorFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select referencedComponentId, componentId1, componentId2, integer1, moduleId, effectiveTime, active from ccirefset where active=1 and referencedComponentId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    if len(recordsetIn2) == 0:
        cursorIn2.execute("select referencedComponentId, componentId1, componentId2, integer1, moduleId, effectiveTime, active from ccirefset where active=0 and referencedComponentId = ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
    rowList = []
    tempSet = ()
    for row in recordsetIn2:
        tempSet = (row[0], pterm(row[0]), row[1], pterm(row[1]), row[2], pterm(row[2]), row[3], row[4], row[5], row[6])
        rowList.append(tempSet)
    rowList.sort(key=lambda x: x[6])
    cursorIn2.close()
    connectionIn2.close()
    return rowList


def refSetFieldsFromConId_IN(idIn):
    typeDict = {"c":tok["COMPTYPE"], "s":tok["STRINGTYPE"], "i":tok["INTEGERTYPE"]}
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select componentId1, componentId2, integer1 from ccirefset where active=1 and referencedComponentId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    if len(recordsetIn2) == 0:
        cursorIn2.execute("select componentId1, componentId2, integer1 from ccirefset where active=0 and referencedComponentId = ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
    rowList = []
    tempSet = ()
    for row in recordsetIn2:
        tempSet = (pterm(row[0]), row[1], row[2])
        rowList.append(tempSet)
    # if no entry in descriptor refset, generate based on pattern.
    if len(rowList) == 0:
        myRefsetFieldList = findRefsetMetaFields(refSetTypeFromConId_IN(idIn)[0][0]).split(", ")[1:]
        counter = 0
        rowList.append(('Referenced component *', typeDict["c"], counter))
        counter += 1
        for item in myRefsetFieldList:
            rowList.append((item.capitalize() + " *", typeDict[item[0]], counter))
            counter += 1
    rowList.sort(key=lambda x: x[2])
    cursorIn2.close()
    connectionIn2.close()
    return rowList

def descendantsFromConId_IN(idIn, FromDotCodeBool=False):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    descendantList = []
    tempSet = ()
    cursorIn2.execute("select subtypeId from tc where supertypeId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    focusFSN = fsnFromConId_IN(idIn)[1]
    for descendant in recordsetIn2:
        if cfg("SHOW_DEFS") < 4 or (descendant[0] == tok["ROOT"]) or FromDotCodeBool:
            if cfg("GRAPH_ANGLE") in (2, 3) and FromDotCodeBool:
                tempSet = (descendant[0], fsnFromConId_IN(descendant[0])[1])
            else:
                tempSet = (descendant[0], pterm(descendant))
        else:
            tempSet = (descendant[0], hasDifferentTagTerm(descendant, focusFSN))
        descendantList.append(tempSet)
    if cfg("SORT_DIAG") == 0: # ascii sort
        descendantList.sort(key=lambda x: x[1].lower())
    elif cfg("SORT_DIAG") == 1 or cfg("SORT_DIAG") == 2 or cfg("SORT_DIAG") == 4 or cfg("SORT_DIAG") == 5: # natural sort
        descendantList = naturalSort(descendantList, False, 0, 1)
    else: # descendant count
        descendantListTemp = []
        for descendant in descendantList:
            subCount = descendantCountFromConId_IN(descendant[0])
            tempstring = descendant[0] + "|" + descendant[1] + "|" + str(subCount)
            descendantListTemp.append(tempstring.split("|"))
        descendantListTemp.sort(key=lambda x: (-(int(x[2])), x[1].lower()))
        descendantList = [[item[0], item[1]] for item in descendantListTemp]
    cursorIn2.close()
    connectionIn2.close()
    return descendantList

def descendantsIDFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    descendantList = []
    cursorIn2.execute("select subtypeId from tc where supertypeId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    for descendant in recordsetIn2:
        descendantList.append(tuplify(descendant[0]))
    cursorIn2.close()
    connectionIn2.close()
    return descendantList

def descendantsCleanIDFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    descendantList = []
    cursorIn2.execute("select subtypeId from tc where supertypeId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    for descendant in recordsetIn2:
        descendantList.append(descendant[0])
    cursorIn2.close()
    connectionIn2.close()
    return descendantList

def descendantCountFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    descendantCount = 0
    if isActiveFromConId_IN(idIn)[1] == 1:
        descendantCount = cursorIn2.execute("select STCount from TCSubCount where supertypeId = ?", tuplify(idIn)).fetchone()[0]
    cursorIn2.close()
    connectionIn2.close()
    return descendantCount

def ancestorsFromConId_IN(idIn, FromDotCodeBool=False):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    ancestorList = []
    tempSet = ()
    if idIn == tok["ROOT"]:
        tempSet = (tok["ROOT"], pterm(tok["ROOT"]))
        ancestorList.append(tempSet)
    else:
        cursorIn2.execute("select supertypeId from tc where subtypeId = ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
        focusFSN = fsnFromConId_IN(idIn)[1]
        for ancestor in recordsetIn2:
            if cfg("SHOW_DEFS") < 3 or (ancestor[0] == tok["ROOT"]) or FromDotCodeBool:
                if cfg("GRAPH_ANGLE") in (2, 3) and FromDotCodeBool:
                    tempSet = (ancestor[0], fsnFromConId_IN(ancestor[0])[1])
                else:
                    tempSet = (ancestor[0], pterm(ancestor))
            else:
                tempSet = (ancestor[0], hasDifferentTagTerm(ancestor, focusFSN))
            ancestorList.append(tempSet)
        if cfg("SORT_DIAG") == 0: # ascii sort
            ancestorList.sort(key=lambda x: x[1].lower())
        elif cfg("SORT_DIAG") == 1 or cfg("SORT_DIAG") == 2 or cfg("SORT_DIAG") == 4 or cfg("SORT_DIAG") == 5: # natural sort
            ancestorList = naturalSort(ancestorList, False, 0, 1)
        else: # descendant count
            ancestorListTemp = []
            for ancestor in ancestorList:
                subCount = descendantCountFromConId_IN(ancestor[0])
                tempstring = ancestor[0] + "|" + ancestor[1] + "|" + str(subCount)
                ancestorListTemp.append(tempstring.split("|"))
            ancestorListTemp.sort(key=lambda x: (-(int(x[2])), x[1].lower()))
            ancestorList = [(item[0], item[1]) for item in ancestorListTemp]
        cursorIn2.close()
        connectionIn2.close()
    return ancestorList

def ancestorsIDFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    ancestorIDList = []
    if idIn == tok["ROOT"]:
        ancestorIDList.append(idIn)
    else:
        cursorIn2.execute("select supertypeId from tc where subtypeId = ?", tuplify(idIn))
        recordsetIn2 = cursorIn2.fetchall()
        for ancestor in recordsetIn2:
            ancestorIDList.append(ancestor[0])
        cursorIn2.close()
        connectionIn2.close()
    return ancestorIDList

def ancestorsIDFromConIdList_IN(idInList):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    if tok["ROOT"] in idInList:
        idInList.remove(tok["ROOT"])
    subString = ', '.join(map(str, idInList))
    ancestorIDList = []
    cursorIn2.execute("select distinct supertypeId from tc where subtypeId in (" + subString + ")")
    recordsetIn2 = cursorIn2.fetchall()
    for ancestor in recordsetIn2:
        ancestorIDList.append(ancestor[0])
    cursorIn2.close()
    connectionIn2.close()
    return ancestorIDList

def TCCompare_IN(subId, supId):
    supertypeSet = []
    supertypeSet = ancestorsIDFromConId_IN(subId)
    checkBool = False
    if supId in supertypeSet:
        checkBool = True
    return checkBool

def PPSFromAncestors_IN(ancestorList):
    PrimList = []
    PPSDict = {}
    PPSList = []
    for ancestor in ancestorList:
        if conDefStatusFromConId_IN(ancestor[0]) == 'Primitive':
            PrimList.append(ancestor)
    for prim in PrimList:
        PPSDict.update(list(zip(listify(prim[0]), listify('1')))) #set all as PPS
    for prim1 in PrimList:
        for prim2 in PrimList:
            if TCCompare_IN(prim1[0], prim2[0]) and not(prim1 == prim2):
                PPSDict[prim2[0]] = '0' #unset if any shadow test positive
    for prim in PrimList:
        if PPSDict[prim[0]] == '1': #return those unset
            PPSList.append(prim)
    return PPSList

def primAncestors_IN(ancestorList):
    PrimList = []
    for ancestor in ancestorList:
        if conDefStatusFromConId_IN(ancestor[0]) == 'Primitive':
            PrimList.append(ancestor)
    return PrimList

def primAncestorsFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    ancestorList = []
    tempSet = ()
    cursorIn2.execute("select supertypeId, definitionStatusId from tc, concepts where concepts.id=tc.supertypeId and concepts.active=1 and subtypeId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchall()
    for ancestor in recordsetIn2:
        if ancestor[1] == tok['PRIMITIVE']:
            tempSet = (ancestor[0], pterm(ancestor))
            ancestorList.append(tempSet)
    ancestorList.sort(key=lambda x: x[1])
    cursorIn2.close()
    connectionIn2.close()
    return ancestorList


def ancestorCountFromConId_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select count(supertypeId) as number from tc where subtypeId = ?", tuplify(idIn))
    recordsetIn2 = cursorIn2.fetchone()
    for row in recordsetIn2:
        ancestorCount = row
    cursorIn2.close()
    connectionIn2.close()
    return ancestorCount


def historicalOriginsFromConId_IN(inId, inOption):
    listofHistoricalAssRefSets = descendantsFromConId_IN(tok['HISTORICAL_ASSOCIATION'])
    listofHistoricalAssRefSetIds = [i[0] for i in listofHistoricalAssRefSets]
    HARString = ', '.join(map(str, listofHistoricalAssRefSetIds))
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    if inOption == 0:
        cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND crefset.active=0 and componentId1 = ?", tuplify(inId))
    elif inOption == 2:
        cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND componentId1 = ?", tuplify(inId))
    else:
        cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND crefset.active=1 and componentId1 = ?", tuplify(inId))
    recordsetIn2 = cursorIn2.fetchall()
    originList = []
    tempRow = ()
    for row in recordsetIn2:
        tempRow = row
        originList.append(tempRow)
    cursorIn2.close()
    connectionIn2.close()
    originList.sort(key=lambda x: x[0])
    return originList

def historicalDestinationsFromConId_IN(inId, inOption, inLookup, movedFromBool=True, refersToRowsBool=True):
    listofHistoricalAssRefSets = descendantsFromConId_IN(tok['HISTORICAL_ASSOCIATION'])
    listofHistoricalAssRefSetIds = [i[0] for i in listofHistoricalAssRefSets]
    HARString = ', '.join(map(str, listofHistoricalAssRefSetIds))
    if inLookup == 1:
        lookupString = 'referencedComponentId'
    else:
        lookupString = 'componentId1'
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    if inOption == 0:
        cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND crefset.active=0 and " + lookupString + "  = ?", tuplify(inId))
    elif inOption == 2:
        cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND " + lookupString + " = ?", tuplify(inId))
    else:
        cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND crefset.active=1 and " + lookupString + " = ?", tuplify(inId))
    recordsetIn2 = cursorIn2.fetchall()
    destinationList = []
    refersToList = []
    tempRow = ()
    for row in recordsetIn2:
        tempRow = row
        destinationList.append(tempRow)
        if tempRow[0] == tok["MOVED_TO"]:
            MovedFromList = historicalDestinationsFromConId_IN(row[1], 1, 0)
            for movedFromRow in MovedFromList:
                if (movedFromRow[0] == tok["MOVED_FROM"]):
                    if movedFromBool:
                        tempTuple = (movedFromRow[0], movedFromRow[2], movedFromRow[1], movedFromRow[3], movedFromRow[4], movedFromRow[5], movedFromRow[6])
                    else:
                        tempTuple = movedFromRow
                    destinationList.append(tempTuple)
    if refersToRowsBool:
        refersToList = refersToRowsFromConId_IN(inId, inOption)
    for refersToRow in refersToList:
        tempTuple = (refersToRow[0], inId, refersToRow[2], refersToRow[3], refersToRow[4], refersToRow[5], refersToRow[6])
        destinationList.append(tempTuple)
    cursorIn2.close()
    connectionIn2.close()
    destinationList.sort(key=lambda x: x[0])
    return destinationList

def historicalDestinationsCountFromConId_IN(inOption):
    listofHistoricalAssRefSets = descendantsFromConId_IN(tok['HISTORICAL_ASSOCIATION'])
    listofHistoricalAssRefSetIds = [i[0] for i in listofHistoricalAssRefSets]
    HARString = ', '.join(map(str, listofHistoricalAssRefSetIds))
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    if inOption == 0:
        cursorIn2.execute("select referencedComponentId, count(referencedComponentId) as c from crefset where refsetId IN (" + HARString + ") AND crefset.active=0 group by referencedComponentId order by c desc")
    elif inOption == 2:
        cursorIn2.execute("select referencedComponentId, count(referencedComponentId) as c from crefset where refsetId IN (" + HARString + ") group by referencedComponentId order by c desc")
    else:
        cursorIn2.execute("select referencedComponentId, count(referencedComponentId) as c from crefset where refsetId IN (" + HARString + ") AND crefset.active=1 group by referencedComponentId order by c desc")
    recordsetIn2 = cursorIn2.fetchall()
    destinationList = []
    for row in recordsetIn2:
        destinationList.append(list(row))
    for row in destinationList:
        if inOption == 0:
            cursorIn2.execute("select distinct refsetId from crefset where refsetId IN (" + HARString + ") AND crefset.active=0 and referencedComponentId=?", (row[0], ))
        elif inOption == 2:
            cursorIn2.execute("select distinct refsetId from crefset where refsetId IN (" + HARString + ") and referencedComponentId=?", (row[0], ))
        else:
            cursorIn2.execute("select distinct refsetId from crefset where refsetId IN (" + HARString + ") AND crefset.active=1 and referencedComponentId=?", (row[0], ))
        recordsetIn2 = cursorIn2.fetchall()
        row.append(len(recordsetIn2))
    cursorIn2.close()
    connectionIn2.close()
    return destinationList

def refersToRowsFromConId_IN(inId, inOption):
    inactiveDescriptionRows = inactiveTermsFromConIdPlusMeta_IN(inId)
    inactiveDescIds = [row[0] for row in inactiveDescriptionRows]
    HARString = tok["REFERS_TO"]
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    originList = []
    for inactiveDescId in inactiveDescIds:
        if inOption == 0:
            cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND crefset.active=0 and referencedComponentId  = ?", tuplify(inactiveDescId))
        elif inOption == 2:
            cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND referencedComponentId = ?", tuplify(inactiveDescId))
        else:
            cursorIn2.execute("select refsetId, referencedComponentId, componentId1, crefset.moduleId, crefset.effectiveTime, crefset.active, crefset.id from crefset where refsetId IN (" + HARString + ") AND crefset.active=1 and referencedComponentId = ?", tuplify(inactiveDescId))
        recordsetIn2 = cursorIn2.fetchall()
        tempRow = ()
        for row in recordsetIn2:
            tempRow = row
            originList.append(tempRow)
    cursorIn2.close()
    connectionIn2.close()
    originList.sort(key=lambda x: x[0])
    return originList


def CiDFromDiD_IN(idIn):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    cursorIn2.execute("select conceptId, active from descriptionsdx where id = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchall()
    if len(recordsetIn2) > 0:
        for row in recordsetIn2:
            return row[0]
    else:
    # alternative lookup for terms of inactive descriptions (e.g. in ''refers to'' refset)
        cursorIn2.execute("select conceptId, active from descriptions where id = ?", (idIn[0],))
        recordsetIn2 = cursorIn2.fetchall()
        if len(recordsetIn2) > 0:
            for row in recordsetIn2:
                return row[0]
        else:
            # alternative lookup for terms of definitions (e.g. in ''refers to'' refset)
            cursorIn2.execute("select conceptId, active from definitions where id = ?", (idIn[0],))
            recordsetIn2 = cursorIn2.fetchall()
            if len(recordsetIn2) > 0:
                for row in recordsetIn2:
                    return row[0]
            else:
                return tok['ROOT']
    cursorIn2.close()
    connectionIn2.close()


def dTypeTermFromId(inId):
    if inId == tok['FSN']:
        return 'FSN'
    elif inId == tok['SYNONYM']:
        return 'Synonym'
    elif inId == tok['DEFINIT']:
        return 'Definition'
    else:
        return 'Not recognised'

def equivTester(inList, equivList):
    counter = 1
    for sentPair in inList:
        tempTermList = []
        counter += 1
        sent = sentPair[1]
        tempTermList.append(sent)
        matches = sentPair[0]
        testableEquivList = []
        if len(equivList) != 0: # if there's anything in the equivalents list
            if len(equivList[0]) == 4: # if its a characterEquivalents call
                testableEquivList = [equiv for equiv in equivList if equiv[3] == '1'] # if its an 'unmarked' match
            else: # if it's a word equivalents call
                testableEquivList = [equiv for equiv in equivList]
        for equiv in testableEquivList:
            matchList = multi_find(sent.lower(), equiv[1].lower())
            if len(matchList) > 0:
                for marker in matchList:
                    matches.append((marker, equiv[0]))
        powerMatchList = []
        prePowerMatchList = get_power_set(inList[0][0])
        powerMatchList = prePowerMatchList[1:] #
        for tempterm in tempTermList:
            if len(tempTermList) < 120: # arbitrary list length in case of 'urethra' / 'urethral' cycle; needs better solution
                for items in powerMatchList:
                    items.sort(key=lambda x: x[0], reverse=True)
                    for item in items:
                        equivTermRefList = []
                        equivTermCheckList = []
                        equivTermRefList = equivTerms(equivList, item[1])
                        if len(equivList[0]) == 4: # if its a characterEquivalents call
                            equivTermCheckList = equivTerms(testableEquivList, item[1])
                        else: # if it's a word equivalents call
                            equivTermCheckList = equivTerms(equivList, item[1])
                        temptempterm = tempterm
                        for term1 in equivTermRefList:
                            for term2 in equivTermRefList:
                                upTo = temptempterm[0:item[0]].lower()
                                beyond = temptempterm[item[0]:].lower()
                                if not(term1 == term2) and (term1 in equivTermCheckList):  # if its a characterEquivalents call, only test for an 'unmarked' match
                                    beyond = beyond.replace(term1, term2, 1)
                                    if not((upTo + beyond) == sent):
                                        if not((upTo + beyond) in tempTermList):
                                            tempTermList.append(upTo + beyond)
    returnList = [" ".join(list(set(item.split()))) for item in tempTermList if len(sorted(item.split(), key=len)[-1]) < 50] # and limit ridiculous longest word
    return returnList

def equivTerms(inEquivs, inId):
    outList = []
    for item in inEquivs:
        if item[0] == inId:
            outList.append(item[1])
    return outList

def readInWordEquivs():
    readbackList = []
    returnList = []
    try:
        f = io.open(cfgPathInstall() + "files" + sep() + "search" + sep() + "wordEquiv.txt", "r", encoding="UTF-8")
        for line in f:
            readbackList.append(line)
        f.close()
        for item in readbackList:
            row = item.split("\t")
            rowTemp = [r.strip() for r in row]
            returnList.append(rowTemp)
    finally:
        returnList.sort(key=lambda x: (int(x[0]), x[1]))
        return returnList

def readInCharEquivs(langCode):
    # import addn
    # https://unicode-org.github.io/cldr-staging/charts/latest/collation/index.html
    # and https://github.com/unicode-org/cldr/tree/main/common/collation for extending charEquiv.txt per locale.
    readbackList = []
    returnList = []
    try:
        f = io.open(cfgPathInstall() + "files" + sep() + "search" + sep() + "charEquiv.txt", "r", encoding="UTF-8")
        for line in f:
            readbackList.append(line)
        f.close()
        for item in readbackList:
            row = item.split("\t")
            rowTemp = [r.strip() for r in row]
            if rowTemp[2] == langCode:
                returnList.append(rowTemp)
    finally:
        return returnList

def multi_find(string, value, start=0, stop=None):
    values = []
    while True:
        found = string.find(value, start, stop)
        if found == -1:
            break
        values.append(found)
        start = found + 1
    return values

def get_power_set(s):
    power_set = [[]]
    for elem in s:
        for sub_set in power_set:
            power_set = power_set + [list(sub_set) + [elem]]
    return power_set

def prelim(seedModules):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    mstring = ', '.join(map(str, seedModules))
    queryString = "select distinct referencedComponentId as mod from ssrefset where active=1 and moduleId in (" + mstring + ") \
                    union \
                    select distinct ssrefset.moduleId as mod from ssrefset where active=1 and moduleId in (" + mstring + ")"
    cursorIn1.execute(queryString)
    recordsetIn1 = cursorIn1.fetchall()
    returnSet = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    return returnSet

def latestET(seedModules):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    mstring = ', '.join(map(str, seedModules))
    queryString = "select distinct string2 from ssrefset where active=1 and moduleId in (" + mstring + ")"
    cursorIn1.execute(queryString)
    recordsetIn1 = cursorIn1.fetchall()
    returnSet = [item[0] for item in recordsetIn1]
    cursorIn1.close()
    connectionIn1.close()
    return returnSet

def deepestRels_IN(limitInt):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    firstTime = True
    tempList = []
    outList = []
    cycleNum = 3
    selectString = ""
    selectString1 = "select r1.sourceId, r"
    selectString2 = ".destinationId "
    fromString =  "from relationships r1, relationships r2, "
    fromString1 = "relationships r"
    fromString2 = ", "
    whereString = " where r1.active=1 and r1.typeId <> '" + tok["IS_A"] + "' and r2.active=1 and r2.typeId <> '" + tok["IS_A"] + "' "
    activeTypeWhereString1 = "and r"
    activeTypeWhereString2 = ".active=1 and r"
    activeTypeWhereString3 = ".typeId <> '" + tok["IS_A"] + "' "
    destSourceString = "and r1.destinationId = r2.sourceId "
    destSourceString1 = "and r"
    destSourceString2 = ".destinationId = r"
    destSourceString3 = ".sourceId "
    endString = ";"
    while firstTime or (len(tempList) > 0 and cycleNum <= limitInt):
        firstTime = False
        selectString = selectString1 + str(cycleNum) + selectString2
        fromString += fromString1 + str(cycleNum) + fromString2
        whereString += activeTypeWhereString1 + str(cycleNum) + activeTypeWhereString2 + str(cycleNum) + activeTypeWhereString3
        destSourceString += destSourceString1 + str(cycleNum - 1) + destSourceString2 + str(cycleNum) + destSourceString3
        queryString = selectString + fromString[:-2] + whereString + destSourceString[:-1] + endString
        cursorIn1.execute(queryString)
        recordsetIn1 = cursorIn1.fetchall()
        tempList = recordsetIn1
        if len(tempList) > 0:
            outList = tempList[:]
            if cycleNum <= limitInt:
                cycleNum += 1
    cursorIn1.close()
    connectionIn1.close()
    return list(set(outList)), cycleNum - 1

def busyCrossMaps_IN(limitInt):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    tempList = []
    outList = []
    queryString = "SELECT distinct Integer3, referencedComponentId FROM iisssciRefset WHERE active=1 AND Integer1 >= 2 AND Integer2 >= 2 ORDER BY Integer3 DESC LIMIT ?;"
    cursorIn1.execute(queryString, (limitInt + 1,))
    recordsetIn1 = cursorIn1.fetchall()
    tempList = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    for row in tempList:
        outList.append([row[0], row[1], pterm(row[1])])
    return sorted(outList, key=lambda x: (-x[0], x[2])), limitInt

def reasonList():
    desList = descendantsIDFromConId_IN(tok["DES_INAC"])
    conList = descendantsIDFromConId_IN(tok["CON_INAC"])
    reasons = list(set(desList) | set(conList))
    reasons.remove((tok["CON_INAC"], ))
    reasons.remove((tok["DES_INAC"], ))
    return reasons

def inactivationByReason_IN(limitInt):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    summaryListDict = {}
    reasons = reasonList()
    for reason in reasons:
        tempList = []
        summaryListDict[(reason[0], pterm(reason[0]))] = []
        queryString = "SELECT referencedComponentId FROM cRefset WHERE active=1 AND ComponentId1=? LIMIT ?;"
        cursorIn1.execute(queryString, (reason[0], limitInt))
        recordsetIn1 = cursorIn1.fetchall()
        for record in recordsetIn1:
            tempList.append((record[0], pterm(record)))
        tempList = sorted(tempList, key=lambda x: x[1])
        summaryListDict[(reason[0], pterm(reason[0]))] = tempList
    cursorIn1.close()
    connectionIn1.close()
    return summaryListDict, limitInt

def highDefRels_IN(limitInt):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    tempList = []
    outList = []
    queryString = "select count(sourceId), sourceId from relationships where active=1 and typeId <> '" + tok["IS_A"] + "' group by sourceId order by count(sourceId) desc LIMIT ?;"
    cursorIn1.execute(queryString, (limitInt,))
    recordsetIn1 = cursorIn1.fetchall()
    tempList = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    for row in tempList:
        outList.append([row[0], row[1], pterm(row[1])])
    return sorted(outList, key=lambda x: (-x[0], x[2])), limitInt

def randomConcepts_IN(limitInt):
    connectionIn1 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn1.cursor()
    tempList = []
    outList = []
    queryString = "SELECT * FROM TCSubcount WHERE STCount > 10 ORDER BY RANDOM() LIMIT ?;"
    cursorIn1.execute(queryString, (limitInt,))
    recordsetIn1 = cursorIn1.fetchall()
    tempList = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    for row in tempList:
        outList.append([row[0], pterm(row[0]), ancestorCountFromConId_IN(row[0]), row[1]])
    return sorted(outList, key=lambda x: x[1]), limitInt

def highAncesCount_IN(limitInt):
    connectionIn1 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn1.cursor()
    tempList = []
    outList = []
    queryString = "select count(subtypeId), subtypeId from tc group by subtypeId order by count(subtypeId) desc LIMIT ?;"
    cursorIn1.execute(queryString, (limitInt,))
    recordsetIn1 = cursorIn1.fetchall()
    tempList = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    for row in tempList:
        outList.append([row[0], row[1], pterm(row[1])])
    return sorted(outList, key=lambda x: (-x[0], x[2])), limitInt

def highDescCount_IN(limitInt):
    connectionIn1 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn1.cursor()
    tempList = []
    outList = []
    queryString = "select count(supertypeId), supertypeId from tc group by supertypeId order by count(supertypeId) desc LIMIT ?;"
    cursorIn1.execute(queryString, (limitInt,))
    recordsetIn1 = cursorIn1.fetchall()
    tempList = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    for row in tempList:
        outList.append([row[0], row[1], pterm(row[1])])
    return sorted(outList, key=lambda x: (-x[0], x[2])), limitInt

def highParentCount_IN(limitInt):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    tempList = []
    outList = []
    queryString = "select count(sourceId), sourceId from relationships where active=1 and typeId='" + tok["IS_A"] + "' group by sourceId order by count(sourceId) desc LIMIT ?;"
    cursorIn1.execute(queryString, (limitInt,))
    recordsetIn1 = cursorIn1.fetchall()
    tempList = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    for row in tempList:
        outList.append([row[0], row[1], pterm(row[1])])
    return sorted(outList, key=lambda x: (-x[0], x[2])), limitInt

def highChildCount_IN(limitInt):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    tempList = []
    outList = []
    queryString = "select count(destinationId), destinationId from relationships where active=1 and typeId='" + tok["IS_A"] + "' group by destinationId order by count(destinationId) desc LIMIT ?;"
    cursorIn1.execute(queryString, (limitInt,))
    recordsetIn1 = cursorIn1.fetchall()
    tempList = recordsetIn1
    cursorIn1.close()
    connectionIn1.close()
    for row in tempList:
        outList.append([row[0], row[1], pterm(row[1])])
    return sorted(outList, key=lambda x: (-x[0], x[2])), limitInt

def limitAttributes(returnIncluded):
    inAttributeList = allAttributes()
    returnAttList = []
    attLimitList = []
    hashCounter = 0
    f = io.open(cfgPathInstall() + 'files' + sep() + 'atts' + sep() + 'attLimit.txt', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # put # at start of domain rows you want to limit the diagram to
            attLimitList.append(line.strip().split('\t')[0])
        else:
            hashCounter += 1
    f.close()
    # CONC - temporary fudge to replace 'old' pre-concrete attribute names
    if cfg("CONC_DATA") == 0:
        attLimitList = concreteAttAdds(attLimitList)
    if returnIncluded:
        returnAttList = [row for row in inAttributeList if row not in attLimitList]
        if len(returnAttList) > 0  and (len(inAttributeList) != hashCounter):
            return returnAttList
        elif (len(attLimitList) == 0) and (len(inAttributeList) == hashCounter):
            return attLimitList
        else:
            return inAttributeList
    else:
        return attLimitList

def allAttributes():
    attLimitList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'atts' + sep() + 'attLimit.txt', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # put # at start of domain rows you want to limit the diagram to
            attLimitList.append(line.strip().split('\t')[0])
        else:
            tempString = line.strip().split('\t')[0]
            attLimitList.append(tempString.split(" ")[1])
    f.close()
    # CONC - temporary fudge to replace 'old' pre-concrete attribute names
    if cfg("CONC_DATA") == 0:
        attLimitList = concreteAttAdds(attLimitList)
    return attLimitList

def tabCfg():
    return cfg("TAB_INDENT")

def tabString(inString): # holding position - see tabexperiments.py
    if len(inString) <= 7:
        if cfg("TAB_INDENT") == 1:
            return "\t\t\t"
        else:
            return "\t\t"
    elif len(inString) >= 16:
        return "\t"
    else:
        if cfg("TAB_INDENT") == 1:
            return "\t\t"
        else:
            return "\t"

def tabStringTwoId(inString):
    if len(inString) <= 14:
        if cfg("TAB_INDENT") == 1:
            return "\t\t\t"
        else:
            return "\t\t"
    elif len(inString) >= 31:
        return "\t"
    else:
        if cfg("TAB_INDENT") == 1:
            return "\t\t"
        else:
            return "\t"

def checkAllAtts():
    returnBool = True
    attList = readAtts()
    for att in attList:
        if att[0] == "#":
            returnBool = False
            break
    if (cfg("SHOW_SUPERS") == 0) or (cfg("SHOW_SUPERS") == 2):
        returnBool = False
    return returnBool

def readAtts():
    attLimitList = []
    # readback current set of attributes
    f = io.open(cfgPathInstall() + 'files' + sep() + 'atts' + sep() + 'attLimit.txt', 'r', encoding="UTF-8")
    for line in f:
        attLimitList.append(line.strip())
    f.close()
    return attLimitList

def anyAttExclusions(attLimitList):
    total = len(attLimitList)
    hashMatches = 0
    for row in attLimitList:
        if row[0] == "#":
            hashMatches += 1
    if hashMatches > 0 and hashMatches < total:
        return True
    else:
        return False

def setAtts(testId, incExclString, operatorString):
    attsOfInterest = []
    attLimitList = []
    alreadyExcl = []
    # determine attsOfInterest
    if operatorString == "=":
        attsOfInterest.append(testId)
    else:
        attsOfInterest = [row[0] for row in descendantsFromConId_IN(testId)]
    attLimitList = readAtts()
    if anyAttExclusions(attLimitList):
        alreadyExcl = limitAttributes(False)
    # writeback with mods
    f = io.open(cfgPathInstall() + 'files' + sep() + 'atts' + sep() + 'attLimit.txt', 'w', encoding="UTF-8")
    # if inclExcl is include:
    if incExclString == "=":
        for row in attLimitList:
            if row.strip().split('\t')[0] in attsOfInterest:
                f.write("# " + row + "\n")
            else:
                f.write(row + "\n")
    elif incExclString == "!":
        attsOfInterest += alreadyExcl
        for row in attLimitList:
            if (row.strip().split('\t')[0] not in attsOfInterest) and not (row[0] == "#"):
                f.write("# " + row + "\n")
            elif ' ' in row:
                if (row.strip().split(' ')[1].split('\t')[0] in attsOfInterest) and (row[0] == "#"):
                    f.write(row[2:] + "\n")
            else:
                f.write(row + "\n")
    else:
        # all in or all out:
        if incExclString == "*":
            for row in attLimitList:
                if row[0] == "#":
                    f.write(row[2:] + "\n")
                else:
                    f.write(row + "\n")
        else:
            for row in attLimitList:
                if row[0] == "#":
                    f.write(row + "\n")
                else:
                    f.write("# " + row + "\n")
    f.close()
    attLimitList = readAtts()
    return testId

# CONC - check whether data contains concrete rows (uses MRCM domain refset)
def concLookUp_IN():
    concBool = False
    refsetId = tok["MRCM_DIR"]
    checkStringList = ["[[+str(\"*\")]]", "[[+dec(>#0..)]]", "[[+int(>#0..)]]"]
    myRefset = refSetTypeFromConId_IN(refsetId)
    for refset in myRefset:
        refsetType = refset[0]
    myCount = refSetMemberCountFromConId_IN(refsetId, refsetType)
    myValueRows = refSetMembersFromConId_IN(refsetId, refsetType, 0, myCount)
    for row in myValueRows:
        for checkString in checkStringList:
            if checkString in row[6]:
                concBool = True
    return concBool

def pathCalcFromId_IN(inId):
    pathList = []
    firstIdList = []
    # initialise
    pathList.append([inId])
    firstIdList.append(inId)
    shortestPath = 250
    longestPath = 0
    # main loop
    while firstIdList != [tok["ROOT"]]:
        for firstId in firstIdList:
            parents = parentsFromId_IN(tuplify(firstId))
            for parent in parents:
                for path in pathList:
                    tempPath = []
                    if path[0] == firstId:
                        tempPath.append(parent[1])
                        for item in path:
                            tempPath.append(item)
                        if tempPath[0] == tok["ROOT"]:
                            if len(tempPath) > longestPath:
                                longestPath = len(tempPath)
                            if len(tempPath) < shortestPath:
                                shortestPath = len(tempPath)
                        pathList.append(tempPath)
            pathList = [item for item in pathList if (item[0] != firstId) or (item[0] == tok["ROOT"])]
            firstIdList = []
            for item in pathList:
                if item[0] not in firstIdList:
                    firstIdList.append(item[0])
    if shortestPath == 250:
        shortestPath = 0
    return [pathList, pathRowsFromPath(pathList[0]), pathRowsFromPath(pathList[-1]), longestPath, shortestPath]

def pathRowsFromPath(inPath):
    outPathList = []
    for counter in range(len(inPath[:-1])):
        outPathList.append([inPath[counter + 1], inPath[counter]])
    return outPathList

def OWLPattern(inPattern):
    OWLPatternDict = {"|SubClassOf(|": "Primitive",
                    "|EquivalentClasses(|": "Defined",
                    "|SubObjectPropertyOf(|": "Object attribute",
                    "|SubDataPropertyOf(|": "Data attribute",
                    "|SubObjectPropertyOf(|TransitiveObjectProperty(|": "Transitive",
                    "|SubObjectPropertyOf(|SubObjectPropertyOf(ObjectPropertyChain(|": "Property chain",
                    "|ReflexiveObjectProperty(|SubObjectPropertyOf(|TransitiveObjectProperty(|": "Transitive and reflexive",
                    "|EquivalentClasses(|SubClassOf(|": "Multiple necessary",
                    "|SubClassOf(|SubClassOf(ObjectIntersectionOf(|": "GCI",
                    "|EquivalentClasses(|SubClassOf(ObjectIntersectionOf(|": "Defined GCI"}
    if inPattern in OWLPatternDict:
        return OWLPatternDict[inPattern]
    else:
        return "Not pattern recognised"

def singleOWLAxioms():
    conn = sqlite3.connect(snapPath)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, string1 as s, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r having c=1 order by c desc;")
    recordset = cur.fetchall()
    summaryDict = {}
    summaryListDict = {}
    for itemA in recordset:
        startList = []
        OWLString = ""
        OWLString = itemA[1]
        marker = OWLString.find(":")
        if OWLString[0:marker] not in startList:
            startList.append(OWLString[0:marker])
        entryString = "|"
        for entry in sorted(startList):
            entryString += entry + "|"
        if entryString not in summaryDict:
            summaryDict[entryString] = 1
        else:
            summaryDict[entryString] += 1
        if entryString not in ["|SubClassOf(|", "|EquivalentClasses(|"]:
            if OWLPattern(entryString) not in summaryListDict:
                summaryListDict[OWLPattern(entryString)] = [[itemA[0]]]
            else:
                summaryListDict[OWLPattern(entryString)].append([itemA[0]])
    cur.close
    conn.close
    return summaryListDict

def multipleOWLAxioms():
    conn = sqlite3.connect(snapPath)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r having c > 1 order by c desc;")
    recordset = cur.fetchall()
    summaryDict = {}
    summaryListDict = {}

    IDList = [item[0] for item in recordset]
    IDString = "'" + "', '".join(IDList) + "'"
    cur.execute("select referencedComponentId, string1 from srefset where refsetId='733073007' and active = 1 and referencedComponentId IN (" + IDString + ")")
    recordset2 = cur.fetchall()

    detailDict = {}
    for itemA in recordset:
        for row in recordset2:
            if itemA[0] == row[0]:
                if itemA[0] not in detailDict:
                    detailDict[itemA[0]] = [row[1]]
                else:
                    detailDict[itemA[0]].append(row[1])
    cur.close
    conn.close

    for itemA in recordset:
        startList = []
        OWLString = ""
        for item in detailDict[itemA[0]]:
            OWLString = item
            marker = OWLString.find(":")
            if OWLString[0:marker] not in startList:
                startList.append(OWLString[0:marker])
        entryString = "|"
        for entry in sorted(startList):
            entryString += entry + "|"
        if entryString not in summaryDict:
            summaryDict[entryString] = 1
            summaryListDict[OWLPattern(entryString)] = [[itemA[0], itemA[1]]]
        else:
            summaryDict[entryString] += 1
            summaryListDict[OWLPattern(entryString)].append([itemA[0], itemA[1]])
    return summaryListDict


def interestingOWL_IN():
    dict1 = multipleOWLAxioms()
    dict2 = singleOWLAxioms()
    return {**dict1, **dict2}

def hasDef_IN(idIn):
    try:
        myterm = onlyDefTermsFromConId_IN(idIn)
        if myterm:
            return True
        else:
            return False
    except:
        return False

def testDefs_IN(runNum, withDef):
    conn = sqlite3.connect(outPath)
    cur2 = conn.cursor()
    outList = []
    progress = 0
    # first pass to find and order concepts with definitions in ancestry.
    # subtypes identified this way then need cleaing up (cf. rlr etc) before returning.
    cur2.execute("SELECT t.SubtypeId as s, count(t.supertypeId) as c FROM tc as t, Descriptionsdx as d1 \
                    WHERE t.SupertypeId=d1.conceptId and \
                    d1.active=1 and \
                    d1.typeId = '" + tok["DEFINIT"] + "' \
                    group by s order by c desc;")
    recordsetIn2 = cur2.fetchall()
    setToRun = recordsetIn2[:]
    while len(outList) < runNum:
        item = setToRun[progress]
        ancCounter = 0
        if withDef and hasDef_IN(item[0]):
            ancList = ancestorsIDFromConId_IN(item[0])
            for anc in ancList:
                if hasDef_IN(anc):
                    ancCounter += 1
        elif not withDef and not hasDef_IN(item[0]):
            ancList = ancestorsIDFromConId_IN(item[0])
            for anc in ancList:
                if hasDef_IN(anc) and not(item[0] == anc):
                    ancCounter += 1
        if ancCounter > 0:
            outList.append([item[0], ancCounter, pterm(item[0])])
        progress += 1
    outList = sorted(outList, key=lambda x: (-x[1], x[2]))
    if withDef:
        cur2.execute("SELECT count(distinct id) FROM Descriptionsdx as d1 WHERE d1.active=1 and d1.typeId = '" + tok["DEFINIT"] + "'")
        recordsetIn2 = cur2.fetchall()
        localDefCount = recordsetIn2[0][0]
        cur2.execute("SELECT count(distinct t.SubtypeId) FROM tc as t, Descriptionsdx as d1 WHERE t.SupertypeId=d1.conceptId and d1.active=1 and d1.typeId = '" + tok["DEFINIT"] + "'")
        recordsetIn2 = cur2.fetchall()
        ancOrLocalDefCount = recordsetIn2[0][0]
        outList.insert(0, [str(localDefCount), str(ancOrLocalDefCount)])
    cur2.close()
    conn.close()
    return outList, len(outList)
