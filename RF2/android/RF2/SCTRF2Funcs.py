from __future__ import print_function, unicode_literals
import io
import re
import sys
from random import random
import math
import textwrap
from SCTRF2Helpers import ancestorsIDFromConId_IN, deepestRels_IN, descendantsIDFromConId_IN, fsnFromConIdPlusMeta_IN, parentIdsFromId_IN, tuplify, CiDFromDiD_IN, parentsFromId_IN, listify, pterm, \
                        conModCheckFromConId_IN, childrenFromIdWithPlus_IN, conMetaFromConId_IN, descendantsFromConId_IN, \
                        fsnFromConId_IN, ptFromConId_IN, synsFromConId_IN, defnFromConId_IN, ptFromConIdPlusMeta_IN, \
                        synsFromConIdPlusMeta_IN, defnFromConIdPlusMeta_IN, searchOnText_IN, groupedRolesFromConId_IN, \
                        historicalOriginsFromConId_IN, groupedRolesFromConIdPlusMeta_IN, metaColor_IN, refSetFieldsFromConId_IN, \
                        refSetDetailsFromMemIdandSetId_IN, refSetTypeFromConId_IN, refSetDetailsFromMemIdandSetIdPlusMeta_IN, \
                        refSetMemberCountFromConId_IN, refSetMembersFromConIdPlusMeta_IN, refSetMembersFromConId_IN, \
                        refsetPterm, refSetFromConIdPlusMeta_IN, refSetFromConId_IN, ancestorsFromConId_IN, TCCompare_IN, \
                        PPSFromAncestors_IN, descendantCountFromConId_IN, groupedDefiningRolesFromConId_IN, childrenFromId_IN, \
                        activeColor_IN, refersToRowsFromConId_IN, activeColorOnTerm_IN, otherTermsFromConIdPlusMeta_IN, \
                        desMetaFromDesId_IN, attribColor_IN, limitAttributes, tabString, simplify, setAtts, checkAllAtts, \
                        refSetMembersCompareRefTableFromConIds_IN, proportionColor_IN, historicalDestinationsFromConId_IN, \
                        concLookUp_IN, prelim, childCountFromId_IN, childrenIDFromId_IN, latestConcepts_IN, tabStringTwoId, \
                        fileAndModuleDistribution_IN, modColor_IN, maxETForRefset_IN, nonRCIDRefSetFromConId_IN, \
                        parentsFromIdPlusMeta_IN, childrenFromConIdPlusMeta_IN, allRolesFromConId_IN, onlyDefTermsFromConId_IN, \
                        readFileMembers, busyCrossMaps_IN, highDefRels_IN, highAncesCount_IN, highDescCount_IN, highParentCount_IN, \
                        highChildCount_IN, interestingOWL_IN, testDefs_IN, historicalDestinationsCountFromConId_IN, \
                        inactiveIsAsFromConId_IN, inactivationByReason_IN, randomConcepts_IN, pathCalcFromId_IN
from SCTRF2Refset import refsetList
from SCTRF2Subgraph import setToGraph, adjacencyBuild, tlc
from SCTRF2config import cfg, cfgWrite, tokDict, tokList, pth
from SCTRF2Help import actionIndexDict
from platformSpecific import cfgPathInstall, clearScreen, renameDiagramFile, swapData, sep, xmapToSVGLocal, input2_3
# CONC HEADER
from SCTRF2Concrete import concreteTakingRoles, wrapStringCheck

tagList = readFileMembers(cfgPathInstall() + 'files' + sep() + 'misc' + sep() + 'tagList.txt')
tok = tokDict()

def charCheck(inString): # check which direction
    if inString[-1] in '<>' and len(inString) > 1:
        return inString[:-1], inString[-1]
    else:
        return inString, "<"

def plusCheck(inString): # check whether predicate changing instruction from SCTRF2R is trailed by a cloning '+' instruction
    if inString[-1] == '+' and len(inString) == 2:
        return True
    else:
        return False

def refersToRowsFromConIdFocusCheck(idIn):
    if idIn[0] == tok["ROOT"]:
        return False
    else:
        if len(refersToRowsFromConId_IN(idIn[0], 1)) > 0:
            return True
        else:
            return False

def anyDefs(inList, fromRoleBool, showDefsInLists = 0):
    hasDef = False
    if showDefsInLists > 1:
        for item in inList:
            try:
                myterm = onlyDefTermsFromConId_IN(item)
                if myterm:
                    hasDef = True
                    break
                else:
                    pass
            except:
                pass
    if hasDef:
        if fromRoleBool:
            return "\033[35m" + "*" + "\033[92m"
        else:
            return "\033[35m" + " *" + "\033[92m"
    else:
        return ""

def ancReverse(inString):
    if "*" not in inString:
        return "\033[31m" + "-" + "\033[92m"
    else:
        return inString

def hasDefInList(idIn, showDefsInLists = 0):
    if showDefsInLists > 0: # show trailing star in lists
        try:
            myterm = onlyDefTermsFromConId_IN(idIn)
            if myterm:
                if myterm[1][-4:] == ' (*)':
                    return "\033[31m" + " *" + "\033[92m"
                else:
                    return "\033[36m" + " *" + "\033[92m"
            else:
                return ""
        except:
            return ""
    else:
        return ""

def hasDef(idIn, inTerm = ""):
    if inTerm == "": # simple boolean option for patchwork
        try:
            myterm = onlyDefTermsFromConId_IN(idIn)
            if myterm:
                return True
            else:
                return False
        except:
            return False
    else: # returns tag color escape sequence
        try:
            myterm = onlyDefTermsFromConId_IN(idIn)
            if myterm:
                insertPoint = inTerm.rfind("(")
                if insertPoint > 0:
                    if myterm[1][-4:] == ' (*)':
                        return "\033[2m" + inTerm[:insertPoint] + "\033[31m" + inTerm[insertPoint:]
                    else:
                        return "\033[2m" + inTerm[:insertPoint] + "\033[36m" + inTerm[insertPoint:]
                else:
                    return "\033[2m" + inTerm
        except:
            return "\033[2m" + inTerm

def browseFromId(inId, showMeta=False):
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    mydict = {}
    mycounter = 1
    # supertypes
    parentList = []
    if  inId[0][-2] == '1':
        myId = tuplify(CiDFromDiD_IN(inId))
    else:
        myId = inId
    # CONC - relax level of concept model attributes
    attBool = TCCompare_IN(myId[0], tok["CONMOD_ATTRIB"])
    parentList = parentsFromId_IN(myId)
    if showMeta:
        parentListMeta = parentsFromIdPlusMeta_IN(inId, 1)
        parentDictMeta = {}
        for parent in parentListMeta:
            if parent[3] not in parentDictMeta:
                parentDictMeta[parent[3]] = parent[5:]
    if len(parentList) > 0:
        print('Parents: [' + str(len(parentList)) + ']' + anyDefs(list(set(ancestorsIDFromConId_IN(myId[0])) - (set(myId) | set([item[1] for item in parentList]))), False, cfg("SHOW_DEFS")))
        for listParent in parentList:
            if not listParent is None:
                if attBool:
                    print('[' + str(mycounter) + ']\t' + attribColor_IN(listParent[1]) + tabString(listParent[1]) + listParent[0] + hasDefInList(listParent[1], cfg("SHOW_DEFS")))
                else:
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(listParent[1], True) + tabString(listParent[1]) + listParent[0] + hasDefInList(listParent[1], cfg("SHOW_DEFS")))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(listParent[1])))))
                mycounter += 1
                if showMeta:
                    browseFromIdConMeta(listParent[1], "0")
                    print('\033[36m(Rel:Module=' + parentDictMeta[listParent[1]][0] + ', Active=' + str(parentDictMeta[listParent[1]][4]) + ', Time=' + parentDictMeta[listParent[1]][1] + ')\033[92m')
                    print('\033[36m(Rel Id=' + parentDictMeta[listParent[1]][5] + ('' if (descendantCountFromConId_IN(listParent[1]) == 1) else ', Descendants=' + str(descendantCountFromConId_IN(listParent[1]))) + ')\033[92m')
        print()

    print('Focus:')
    if attBool:
        print('[' + str(mycounter) + ']\t' + attribColor_IN(myId[0]) + tabString(myId[0]) + '\033[1m' + pterm(myId) + '\033[22m')
    else:
        print('[' + str(mycounter) + ']\t' + activeColor_IN(myId[0], True) + tabString(myId[0]) + '\033[1m' + pterm(myId) + '\033[22m')
    mydict.update(list(zip(listify(mycounter), listify(myId))))
    print('\t' + (' ' * len(myId[0])) + tabString(myId[0]) + hasDef(myId[0], fsnFromConId_IN(myId[0])[1]) + '\033[92;22m'  + anyDefs(list(set(growValues(myId[0], True)) - (set(myId))), False, cfg("SHOW_DEFS")))
    if conModCheckFromConId_IN(myId):
        print('\t' + (' ' * len(myId[0])) + tabString(myId[0]) + "\033[31mModule dependency risk\033[92m")
    if refersToRowsFromConIdFocusCheck(myId):
        print('\t' + (' ' * len(myId[0])) + tabString(myId[0]) + "\033[31mMay refer to..." + refersToArrows(refersToRowsFromConId_IN(myId[0], 1), myId[0]) + "\033[92m")
    mycounter += 1
    if showMeta:
        browseFromIdConMeta(myId[0], "0", True)
    print()

    # subtypes
    childList = []
    limitChildren = False
    childNumber = childCountFromId_IN(myId)
    if (childNumber > 0) and (childNumber < 1501) and (cfg("SORT_DIAG") > 0):
        childList = childrenFromIdWithPlus_IN(myId)  # if displaying concept with less than 1000 children, show all (slower sort)
    elif (childNumber > 1500) and (cfg("SORT_DIAG") > 0):
        childList = childrenFromIdWithPlus_IN(myId, True)  # if displaying concept with more than 1000 children, only show 1000 (slower sort)
        limitChildren = True
    elif (childNumber > 0) and (cfg("SORT_DIAG") == 0):
        childList = childrenFromIdWithPlus_IN(myId)  # show all children if loading focus with any (>0) number of children (but fastest sort)
    if limitChildren:
        childCount = "\033[31m" + str(cfg("CHILD_LIMIT")) + "/" + str(childNumber) + "\033[92m"
    else:
        childCount = str(childNumber)
    if descendantCountFromConId_IN(myId[0]) < 2001:
        print('Children: [' + childCount + ']' + anyDefs([outerItem[0] for outerItem in list(set(descendantsIDFromConId_IN(myId[0])) - (set(tuplify(myId)) | set([tuplify(item[2]) for item in childList])))], False, cfg("SHOW_DEFS")))
    elif cfg("SHOW_DEFS") > 1:
        print('Children: [' + childCount + ']\033[90m *\033[92m')
    else:
        print('Children: [' + childCount + ']')
    if childNumber > 0:
        if showMeta:
            # childListMeta = parentsFromIdPlusMeta_IN(myId, 1)
            childListMeta = childrenFromConIdPlusMeta_IN(myId[0], 1, 0)
            childDictMeta = {}
            for child in childListMeta[0][1:]:
                if child[3] not in childDictMeta:
                    childDictMeta[child[3]] = child[5:]
    hasChildren = False
    for listChild in childList:
        if not listChild is None:
            hasChildren = True
            if attBool:
                print('[' + str(mycounter) + ']\t' + listChild[1] + attribColor_IN(listChild[2]) + tabString(listChild[1] + listChild[2]) + listChild[0] + hasDefInList(listChild[2], cfg("SHOW_DEFS")))
            else:
                print('[' + str(mycounter) + ']\t' + listChild[1] + activeColor_IN(listChild[2], True) + tabString(listChild[1] + listChild[2]) + listChild[0] + hasDefInList(listChild[2], cfg("SHOW_DEFS")))
            mydict.update(list(zip(listify(mycounter), listify(tuplify(listChild[2])))))
            mycounter += 1
            if showMeta:
                browseFromIdConMeta(listChild[2], "0")
                print('\033[36m(Rel:Module=' + childDictMeta[listChild[2]][0] + ', Active=' + str(childDictMeta[listChild[2]][4]) + ', Time=' + childDictMeta[listChild[2]][1] + ')\033[92m')
                print('\033[36m(Rel Id=' + childDictMeta[listChild[2]][5] + ', Children=' + str(childDictMeta[listChild[2]][6]) + ('' if (descendantCountFromConId_IN(listChild[2]) == 1) else ', Descendants=' + str(descendantCountFromConId_IN(listChild[2]))) + ')\033[92m')

    if limitChildren:
        print("\n\033[31mOnly showing sample of children (" + str(cfg("CHILD_LIMIT")) + "/" + str(childNumber) + "). Set L0 to see all.\033[92m")
    drawBrowseId(myId, mydict, hasChildren)
    # housekeeping
    print()
    return mydict

def drawBrowseId(myId, mydict, childBool):
    # draw image file
    conMetaList = conMetaFromConId_IN(myId[0], False)
    notYetDrawnFocus = True
    if len(conMetaList) > 0:
        if conMetaList[1] == 1:
            if cfg("DRAW_DIAG") in (1, 3, 4, 5, 6, 7):
                dotDrawFocus(myId[0], [], ">")
            if cfg("DRAW_DIAG") in (2, 3, 8):
                if cfg("DRAW_DIAG") == 8:
                    dotDrawFocus(myId[0], [], ">")
                    notYetDrawnFocus = False
                dotDrawAncestors(myId[0], 1, mydict)
            if cfg("DRAW_DIAG") == 5:
                dotDrawDescendants(myId[0])
            if cfg("DRAW_DIAG") == 6:
                dotDrawAncestorsAndDescendants(myId[0], "RL")
            if cfg("DRAW_DIAG") == 7:
                dotDrawAncestorsAndDescendants(myId[0], "BT")
            if cfg("DRAW_DIAG") == 12:
                dotDrawFocus(tok["ROOT"], [], ">")
                dotDrawAncestors(tok["ROOT"], 1, mydict)
                dotDrawTreeMapIsh((tok["ROOT"],), {}, False, False, False)
                drawPics(3)
            if childBool:
                if notYetDrawnFocus and cfg("DRAW_DIAG") > 2: # changed from > 0 to isolate A=2
                    dotDrawFocus(myId[0], [], ">")
                if cfg("DRAW_DIAG") == 8 and len(mydict) < 1501:
                    dotDrawTreeMapIsh(readFocusValue(), mydict, False, False, True)
                elif cfg("DRAW_DIAG") == 9 and len(mydict) < 1501:
                    dotDrawTreeMapIsh(readFocusValue(), mydict, False, False, False)
                elif cfg("DRAW_DIAG") == 10 and len(mydict) < 1501: # mondrian call
                    dotDrawTreeMapIsh(readFocusValue(), mydict, True, False, False)
                elif cfg("DRAW_DIAG") == 11 and len(mydict) < 1501: # mondrian call (black background)
                    dotDrawTreeMapIsh(readFocusValue(), mydict, True, True, False)
                else:
                    dotDrawTreeMapIsh((tok["ROOT"],), {}, False, False, False)
            else:
                dotDrawTreeMapIsh((tok["ROOT"],), {}, False, False, False)
        else:
            if cfg("DRAW_DIAG") in (1, 3, 8):
                if notYetDrawnFocus:
                    dotDrawFocus(myId[0], [], ">")
                dotDrawAncestors(myId[0], 0)
            elif cfg("DRAW_DIAG") == 6:
                dotDrawAncestors(myId[0], 0, 0, "RL")
                drawPics(3)
        if cfg("SHOW_DEFS") > 4 and cfg("DEMO") == 1:
            cfgWrite("SHOW_DEFS", 4)

def dictFromId(inId):
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    mydict = {}
    mycounter = 1
    if  inId[0][-2] == '1':
        myId = tuplify(CiDFromDiD_IN(inId))
    else:
        myId = inId
    mydict.update(list(zip(listify(mycounter), listify(myId))))
    return mydict

def setSupers(inInteger):
    cfgWrite("ROLE_SUPER", inInteger)
    return inInteger

def setEquiv(inInteger):
    cfgWrite("WORD_EQUIV", inInteger)
    return inInteger

def tabIndent(inInteger):
    cfgWrite("TAB_INDENT", inInteger)
    return inInteger

def showDefs(inInteger):
    cfgWrite("SHOW_DEFS", inInteger)
    return inInteger

def sortDiagram(inInteger):
    if inInteger < 21:
        cfgWrite("SORT_DIAG", inInteger)
    else:
        cfgWrite("CHILD_LIMIT", inInteger)
    return inInteger

# CONC - write configuration value
def concNumber(inInteger):
    cfgWrite("CONC_VALS", inInteger)
    return inInteger

def showSupers(inInteger):
    cfgWrite("SHOW_SUPERS", inInteger)
    return inInteger

def setLocale(inString):
    cfgWrite("LOCALE", inString)

def setAcrossDown(inString):
    cfgWrite("ACROSS_DOWN", inString)

def setLongShort(inString):
    cfgWrite("LONG_SHORT", inString)

def setAddInvisibleEdges(inString):
    cfgWrite("ADD_INVIS", inString)

def historyDiagram(inInteger):
    if inInteger < 5:
        cfgWrite("DIAG_HIST", inInteger)
    else:
        cfgWrite("COLOR_REFERS_TO", inInteger - 5)
    return inInteger

def showUseful():
    print('Useful codes:\n')
    mydict = {}
    mycounter = 1
    # metadata concepts
    seedList1 = [('DESCTYPE', 'Description type values:'),
                ('ACCEPT', 'Acceptability values:'),
                ('CASESIG', 'Case significance values:'),
                ('DEFSTAT', 'Definition status values:'),
                ('CHARTYPE', 'Characteristic type values:')]
    metadataList = []
    for pair in seedList1:
        metadataList = descendantsFromConId_IN(tok[pair[0]], True)
        if len(metadataList) > 0:
            print(pair[1])
            for value in metadataList:
                if not value is None:
                    if not value[0] == tok[pair[0]]:
                        print('[' + str(mycounter) + ']\t' + value[0] + tabString(value[0]) + value[1])
                        mydict.update(list(zip(listify(mycounter), listify(tuplify(value[0])))))
                        mycounter += 1

    metadataList = childrenFromIdWithPlus_IN(tuplify(tok['MODULES']))
    if len(metadataList) > 0:
        print('Upper level modules:')
        for value in metadataList:
            if not value is None:
                print('[' + str(mycounter) + ']\t' + value[1] + value[2] + tabString(value[1] + value[2]) + value[0])
                mydict.update(list(zip(listify(mycounter), listify(tuplify(value[2])))))
                mycounter += 1

    # CONC - relax level of concept model attributes
    print('Concept model attributes:')
    print('[' + str(mycounter) + ']\t' + tok['CONMOD_ATTRIB'] + tabString(tok['CONMOD_ATTRIB']) + pterm(tok['CONMOD_ATTRIB']))
    mydict.update(list(zip(listify(mycounter), listify(tuplify(tok['CONMOD_ATTRIB'])))))
    mycounter += 1

    seedList2 = ['REFSETS',
                'REFVAL',
                'ATTVAL']

    print('Refsets, attributes & values:')
    for value in seedList2:
        print('[' + str(mycounter) + ']\t' + tok[value] + tabString(tok[value]) + pterm(tok[value]))
        mydict.update(list(zip(listify(mycounter), listify(tuplify(tok[value])))))
        mycounter += 1
    print()
    return mydict


def allTermsFromConId(idIn):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    mycounter += 1
    print("Terms for:", activeColor_IN(idIn), pterm(idIn))
    print("([1] to select concept, [v] to return)")
    print()
    myterm = fsnFromConId_IN(idIn)
    print("FSN:")
    print('[' + str(mycounter) + ']\t' + myterm[0] + tabString(myterm[0]) + myterm[1])
    mydict.update(list(zip(listify(mycounter), listify(tuplify(myterm[0])))))
    mycounter += 1
    print()
    myterm = ptFromConId_IN(idIn)
    print("Preferred term:")
    print('[' + str(mycounter) + ']\t' + myterm[0] + tabString(myterm[0]) + myterm[1])
    mydict.update(list(zip(listify(mycounter), listify(tuplify(myterm[0])))))
    mycounter += 1
    print()
    myterm = synsFromConId_IN(idIn)
    if len(myterm) > 0:
        print("Acceptable synonyms:")
        myterm.sort(key=lambda x: x[1])
        for term in myterm:
            print('[' + str(mycounter) + ']\t' + term[0] + tabString(term[0]) + term[1])
            mydict.update(list(zip(listify(mycounter), listify(tuplify(term[0])))))
            mycounter += 1
        print()
    try:
        myterm = defnFromConId_IN(idIn)
        if len(myterm) > 0:
            if myterm[1][-4:] == ' (*)':
                print("Definition (\033[31mfrom en-GB LRS\033[92m):")
                mytermtemp = myterm[1][:-4]
            else:
                print("Definition:")
                mytermtemp = myterm[1]
            print('[' + str(mycounter) + ']\t' + myterm[0] + tabString(myterm[0]) + mytermtemp)
            mydict.update(list(zip(listify(mycounter), listify(tuplify(myterm[0])))))
            mycounter += 1
            print()
    except:
        return mydict
    else:
        return mydict



def allTermsFromConIdPlusMeta(idPreIn):
    if  idPreIn[-2] == '1':
        idIn = CiDFromDiD_IN(tuplify(idPreIn))
    elif idPreIn[-2] == '0':
        idIn = idPreIn
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    mycounter += 1
    idBuildup = []
    print("Terms for:", activeColor_IN(idIn), pterm(idIn))
    print("([1] to select concept, [v] to return)")
    print()
    myterm = fsnFromConIdPlusMeta_IN(idIn, getVersion())
    print("FSN:")
    print('[' + str(mycounter) + ']\t' + activeColor_IN(myterm[0]) + tabString(myterm[0]) + myterm[1] + '\n\033[36m(Des:Module=' + myterm[2] + ', Time=' + myterm[3] + ', ICS=' + myterm[4] + ')\n(Ref:Module=' + myterm[5] + ', Time=' + myterm[6] + ')\033[92m')
    idBuildup.append(myterm[0])
    mydict.update(list(zip(listify(mycounter), listify(tuplify(myterm[0])))))
    mycounter += 1
    print()
    myterm = ptFromConIdPlusMeta_IN(idIn, getVersion())
    print("Preferred term:")
    print('[' + str(mycounter) + ']\t' + activeColor_IN(myterm[0]) + tabString(myterm[0]) + myterm[1] + '\n\033[36m(Des:Module=' + myterm[2] + ', Time=' + myterm[3] + ', ICS=' + myterm[4] + ')\n(Ref:Module=' + myterm[5] + ', Time=' + myterm[6] + ')\033[92m')
    idBuildup.append(myterm[0])
    mydict.update(list(zip(listify(mycounter), listify(tuplify(myterm[0])))))
    mycounter += 1
    print()
    myterm = synsFromConIdPlusMeta_IN(idIn, getVersion())
    if len(myterm) > 0:
        print("Acceptable synonyms:")
        for term in myterm:
            print('[' + str(mycounter) + ']\t' + activeColor_IN(term[0]) + tabString(term[0]) + term[1] + '\n\033[36m(Des:Module=' + term[2] + ', Time=' + term[3] + ', ICS=' + term[4] + ')\n(Ref:Module=' + term[5] + ', Time=' + term[6] + ')\033[92m')
            idBuildup.append(term[0])
            mydict.update(list(zip(listify(mycounter), listify(tuplify(term[0])))))
            mycounter += 1
        print()
    try:
        myterm = defnFromConIdPlusMeta_IN(idIn, getVersion())
        if len(myterm) > 0:
            print("Definition:")
            print('[' + str(mycounter) + ']\t' + activeColor_IN(myterm[0]) + tabString(myterm[0]) + myterm[1] + '\n\033[36m(Des:Module=' + myterm[2] + ', Time=' + myterm[3] + ', ICS=' + myterm[4] + ')\n(Ref:Module=' + myterm[5] + ', Time=' + myterm[6] + ')\033[92m')
            idBuildup.append(myterm[0])
            mydict.update(list(zip(listify(mycounter), listify(tuplify(myterm[0])))))
            mycounter += 1
            print()
        myterm = otherTermsFromConIdPlusMeta_IN(idIn, idBuildup)
        if len(myterm) > 0:
            print("Other terms:")
            for term in myterm:
                print('[' + str(mycounter) + ']\t' + activeColor_IN(term[0]) + tabString(term[0]) + term[1] + '\n\033[36m(Type=' + term[2] + ', Module=' + term[4] + ', ICS=' + term[5] + ')\033[92m')
                mydict.update(list(zip(listify(mycounter), listify(tuplify(term[0])))))
                mycounter += 1
                if term[6] == 0:
                    reasonString = desMetaFromDesId_IN(term[0])
                    print('\033[36m(Time=' + term[3] + activeMetaColor(str(term[6])) + ', Reason=' + reasonString + ')\033[92m')
                else:
                    print('\033[36m(Time=' + term[3] + activeMetaColor(str(term[6])) + ')\033[92m')
            print()
    except:
        return mydict
    else:
        return mydict


def searchOnText(searchText, inOption, inLimit, showDefs):
    mydict = {}
    mycounter = 0
    counterList = []
    searchList = []
    searchIdList = []
    searchList = searchOnText_IN(searchText, inOption, inLimit, showDefs)
    if len(searchList) > 1:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'search' + sep() + 'searchreport.txt', 'w', encoding="UTF-8")
        f.write('Search text: ' + searchText.replace('NEAR ', '') + '\n')
        print()
        for listSearch in searchList:
            if not listSearch is None:
                if mycounter == 0:
                    counterList = listSearch
                    mycounter += 1
                else:
                    tail = (" (" + listSearch[1] + ")") if listSearch[1] != " " else ""
                    if (inLimit == 2 and mycounter < 26) or inLimit == 0 or inLimit == 1:
                        print('[' + str(mycounter) + ']\t' + activeColorOnTerm_IN(listSearch[2], listSearch[1]) + tabString(listSearch[2]) + listSearch[0] + tail)
                    f.write(listSearch[2] + " " + listSearch[0] + tail + '\n')
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(listSearch[2])))))
                    mycounter += 1
        f.close()
        print()
    else:
        counterList = searchList[0]
        print()
    if moreToShow(inOption, inLimit, counterList):
        prefixString = 'Showing first 25 results.\n'
        postfixString = '. Enter \'dd\' for full list.'
    else:
        prefixString = ''
        postfixString = ''
    print(prefixString + '[' + counterList[0] + '] active and [' + counterList[1] + '] inactive matches' + postfixString)
    print()
    if len(searchList) > 1:
        for listSearch in searchList[1:]:
            searchIdList.append(listSearch[2])
        searchIdList = list(set(searchIdList))
        f = io.open(cfgPathInstall() + 'files' + sep() + 'search' + sep() + 'searchreportId.txt', 'w', encoding="UTF-8")
        for listSearch in searchIdList:
            f.write(listSearch + '\n')
        f.close()
    return mydict

def moreToShow(inOption, inLimit, counterList):
    if inLimit in (0, 1): # show all from the start or just show exact match
        return False
    else:
        if inOption == 1 and int(counterList[0]) > 25: # only actives and more actives to come
            return True
        elif inOption in (0, 2) and (int(counterList[0]) + int(counterList[1])) > 25:
            return True
        else:
            return False

def groupedRolesFromConId(idIn):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    print("Roles for:", idIn, pterm(idIn))
    print("([1] to select concept, [v] to return)")
    print()
    myroles = groupedRolesFromConId_IN(idIn)
    for roleGroup in myroles:
        for role in roleGroup:
            if isinstance(role, int):
                if role == -1:
                    print("Parents:")
                elif role == 0:
                    print("Ungrouped:")
                else:
                    print("Group:")
            else:
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(role[1])))))
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(role[3])))))
                hasValDefInListString = hasDefInList(role[3], cfg("SHOW_DEFS"))
                print('[' + str(mycounter - 1) + ']', role[1], role[2] + hasDefInList(role[1], cfg("SHOW_DEFS")) + " = " + '[' + str(mycounter) + ']', role[3], role[4] + hasDefInList(role[3], cfg("SHOW_DEFS")) + ("" if role[0] == -1 else anyDefs(list(set(growValues(role[3], True)) - (set([role[3]]))), (False if hasValDefInListString == "" else True), cfg("SHOW_DEFS"))))
        print()
    writeRoleAndDotFiles(idIn)
    print("Definition:\n")
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'pre_dot.txt', 'r', encoding="UTF-8")
    for line in f:
        print(parsablePretty(line.strip()))
    f.close()
    print()
    return mydict


def historicalOriginsFromConId(idIn):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    a = historicalOriginsFromConId_IN(idIn, 1)
    if len(a) > 0:
        print('Historical origins for', idIn, pterm(idIn) + ":")
        tempId2 = '1'
        for row in a:
            tempId = row[0]
            if tempId != tempId2:
                print()
                mycounter += 1
                print('\033[4m[' + str(mycounter) + ']\t' + row[0] + tabString(row[0]) + pterm(row[0]) + '\033[24m')
                mydict.update(list(zip(listify(mycounter), listify(tuplify(row[0])))))
            mycounter += 1
            if row[0] == tok["REFERS_TO"]:
                if row[1][-2] == '1':
                    sourceTerm = refersToTermColor(idIn, row[1], False)
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(row[1]) + " / " + activeColor_IN(CiDFromDiD_IN(tuplify(row[1]))) + tabString(CiDFromDiD_IN(tuplify(row[1]))) + sourceTerm)
                else:
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(row[1]) + tabString(row[1]) + refsetPterm(row[1]))
            else:
                if row[1][-2] == '1':
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(CiDFromDiD_IN(tuplify(row[1]))) + tabString(CiDFromDiD_IN(tuplify(row[1]))) + refsetPterm(row[1]))
                else:
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(row[1]) + tabString(row[1]) + refsetPterm(row[1]))
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[1])))))
            tempId2 = tempId
    b = refersToRowsFromConId_IN(idIn, 1)
    if len(b) > 0:
        if len(a) == 0:
            print("Outgoing relationships for", idIn, pterm(idIn) + ":")
        else:
            print("\nOutgoing relationships:")
        tempId2 = '1'
        for row in b:
            tempId = row[0]
            if tempId != tempId2:
                print()
                mycounter += 1
                print('\033[4m[' + str(mycounter) + ']\t' + row[0] + tabString(row[0]) + pterm(row[0]) + '\033[24m')
                mydict.update(list(zip(listify(mycounter), listify(tuplify(row[0])))))
            mycounter += 1
            print('[' + str(mycounter) + ']\t' + activeColor_IN(row[1]) + tabString(row[1]) + pterm(row[1]) + " NOW REFERS TO...")
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[1])))))
            mycounter += 1
            print('[' + str(mycounter) + ']\t' + activeColor_IN(row[2]) + tabString(row[2]) + refersToTermColor(row[2], row[1], False))
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[2])))))
            tempId2 = tempId
    if len(a) == 0 and len(b) == 0:
        print('\nNo historical origins')
    print()
    print('[1] for', idIn, pterm(idIn))
    print()
    return mydict


def historicalOriginsFromConIdPlusMeta(idIn):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    a = historicalOriginsFromConId_IN(idIn, 1)
    if len(a) > 0:
        print('Historical origins for', idIn, pterm(idIn) + ":")
        tempId2 = '1'
        for row in a:
            tempId = row[0]
            if tempId != tempId2:
                print()
                mycounter += 1
                print('\033[4m[' + str(mycounter) + ']\t' + row[0] + tabString(row[0]) + pterm(row[0]) + '\033[24m')
                mydict.update(list(zip(listify(mycounter), listify(tuplify(row[0])))))
            mycounter += 1
            if row[1][-2] == '1':
                print('[' + str(mycounter) + ']\t' + activeColor_IN(CiDFromDiD_IN(tuplify(row[1]))) + tabString(row[1]) + refsetPterm(row[1]))
            else:
                print('[' + str(mycounter) + ']\t' + activeColor_IN(row[1]) + tabString(row[1]) + refsetPterm(row[1]))
            print('\033[36m(Module=' + pterm(row[3]) + ', Time=' + row[4] + ')\033[92m')
            print('\033[36m(Row Id=' + row[6] + ')\033[92m')
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[1])))))
            tempId2 = tempId
    b = refersToRowsFromConId_IN(idIn, 1)
    if len(b) > 0:
        if len(a) == 0:
            print("Outgoing relationships for", idIn, pterm(idIn) + ":")
        else:
            print("\nOutgoing relationships:")
        tempId2 = '1'
        for row in b:
            tempId = row[0]
            if tempId != tempId2:
                print()
                mycounter += 1
                print('\033[4m[' + str(mycounter) + ']\t' + row[0] + tabString(row[0]) + pterm(row[0]) + '\033[24m')
                mydict.update(list(zip(listify(mycounter), listify(tuplify(row[0])))))
            mycounter += 1
            print('[' + str(mycounter) + ']\t' + activeColor_IN(row[1]) + tabString(row[1]) + refsetPterm(row[1]) + " NOW REFERS TO...")
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[1])))))
            mycounter += 1
            print('[' + str(mycounter) + ']\t' + activeColor_IN(row[2]) + tabString(row[2]) + refsetPterm(row[2]))
            print('\033[36m(Module=' + pterm(row[3]) + ', Time=' + row[4] + ')\033[92m')
            print('\033[36m(Row Id=' + row[6] + ')\033[92m')
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[2])))))
            tempId2 = tempId
    if len(a) == 0 and len(b) == 0:
        print('\nNo historical origins')
    print()
    print('[1] for', idIn, pterm(idIn))
    print()
    return mydict


def groupedRolesFromConIdPlusMeta(idIn):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    print("Roles for:", idIn, pterm(idIn))
    print("([1] to select concept, [v] to return)")
    print()
    myroles = groupedRolesFromConIdPlusMeta_IN(idIn, 1)
    for roleGroup in myroles:
        for role in roleGroup:
            if isinstance(role, int):
                if role == -1:
                    print("Parents:")
                elif role == 0:
                    print("Ungrouped:")
                else:
                    print("Group:")
            else:
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(role[1])))))
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(role[3])))))
                print('[' + str(mycounter - 1) + ']', role[1], role[2] + " = " + '[' + str(mycounter) + ']', role[3], role[4])
                browseFromIdConMeta(role[1], str(mycounter - 1))
                # CONC - handle metadata lookup on concrete values
                if role[3][0] == "#" or role[3][0] == "\"":
                    print('\033[36m(Val['+ str(mycounter) + ']: Concrete)\033[92m')
                else:
                    browseFromIdConMeta(role[3], str(mycounter))
                print('\033[36m(Rel:Module=' + role[5] + ', Time=' + role[6] + ', Type=' + role[7] + ', Logic=' + role[8] + ')\033[92m')
                print('\033[36m(Rel Id=' + role[10] + ("" if role[0] == -1 else ", RG No=" + str(role[0])) + ')\033[92m')
        print()
    return mydict


def browseFromIdConMeta(myId, counterNum, fromFocus=False):
    if counterNum != "0":
        conString = 'Con['+ counterNum + ']:'
    else:
        conString = "Con:"
    conMeta = []
    conMeta = conMetaFromConId_IN(myId, True)
    if len(conMeta) == 4:
        print('\033[36m(' + conString + 'Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ')\033[92m')
        if fromFocus:
            print('\033[36m(Descendants=' + str(descendantCountFromConId_IN(myId)) + ')\033[92m')
    else:
        print('\033[36m(' + conString + 'Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ', Inac.reason=' + conMeta[4] + ')\033[92m')


def breadcrumbsWrite(inString):
    if pterm(inString) != "" and inString != tok["ROOT"]:
        for fileName in ['breadcrumbs.txt', 'breadcrumbsLong.txt']:
            readbackList = []
            writeToList = []
            try:
                f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + fileName, "r", encoding="UTF-8")
                lines = f.readlines()
                for line in lines:
                    readbackList.append(line)
                matchBool = False
                if inString.strip() + '\n' in readbackList:
                    matchBool = True
                if not matchBool:
                    readbackList.append(inString.strip() + '\n')
                f.close()
                f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + fileName, 'w', encoding="UTF-8")
                if matchBool:
                    writeToList = readbackList
                else:
                    writeToList = readbackList[1:]
                for entry in writeToList:
                    f.write(entry)
            finally:
                f.close()


def breadcrumbsRead(longBool=False):
    mydict = {}
    mycounter = 1
    if longBool:
        fileName = 'breadcrumbsLong.txt'
    else:
        fileName = 'breadcrumbs.txt'
    breadList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + fileName, "r", encoding="UTF-8")
    for line in f:
        tempLine = [line.strip(), refsetPterm(line.strip())]
        if tempLine not in breadList:
            breadList.append(tempLine)
    f.close()
    print('Recent codes:\n')
    if longBool:
        breadList.sort(key=lambda x: x[1])
    for line in breadList:
        print('[' + str(mycounter) + ']\t' + activeColor_IN(line[0], True) + tabString(line[0]) + line[1])
        mydict.update(list(zip(listify(mycounter), listify(tuplify(line[0])))))
        mycounter += 1
    print()
    return mydict

# CONC - menu item in matrix
# see item [K]
def promptString(inShow):
    if inShow == 1:
        indexDict = actionIndexDict('SCTRF2')
        indexDictLetters = list(indexDict)
        promptString = 'Action:\n\033[36m'
        counter = 1
        if cfg("TAB_INDENT") == 0:
            for letter in indexDictLetters:
                promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 3 == 0 else '\t')
                counter += 1
        else:
            for letter in indexDictLetters:
                promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 4 == 0 else '\t')
                counter += 1
        promptString += '\n[zz] Exit\t\t[-] (+letter) help\n\n\033[92;7mInput selection or search text:\033[27m '
    else:
        promptString = '\033[92;7mInput selection or search text:\033[27m '
    return promptString

def readInSwap():
    mydict = {}
    f = io.open(cfgPathInstall() + 'files/focusSwap_mw.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    mydict = browseFromId(tuplify(myId))
    return mydict

def readInFocus():
    mydict = {}
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'focusSwap.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    mydict = browseFromId(tuplify(myId))
    return mydict

def readFocusValue(): # *SCT*
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'focusSwap.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    return tuplify(myId)


def snapHeaderRead():
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'snapHeader.txt', 'r', encoding="UTF-8")
    myLine = ""
    for line in f:
        myLine = line.strip()
    f.close()
    return myLine

def swapDataCall():
    swapData(snapHeaderRead())

def readInSwapSV():
    mydict = {}
    f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'focus_sv.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    focusSwapWrite(myId)
    mydict = browseFromId(tuplify(myId))
    return mydict

def readBackSearch(inLimit):
    mydict = {}
    mycounter = 1
    letterSetBool = False
    if len(inLimit) == 1 and inLimit.isupper():
        letterSetBool = True
    if inLimit == "First25" or inLimit == "ByConLenShort":
        upperLimit = 26
    else:
        upperLimit = 100000
    if len(inLimit) == 1 and inLimit.isupper():
        f = io.open(cfgPathInstall() + 'sets' + sep() + inLimit + '.txt', 'r', encoding="UTF-8")
        templines = f.readlines()
        tempList = []
        for line in templines:
            append_distinct(tempList, CiDFromDiD_IN(tuplify(line.strip())))
        f.close()
        f = io.open(cfgPathInstall() + 'files' + sep() + 'search' + sep() + 'searchreportId.txt', 'w', encoding="UTF-8")
        for item in tempList:
            f.write(item + "\n")
        f.close()
        f = io.open(cfgPathInstall() + 'sets' + sep() + inLimit + '.txt', 'r', encoding="UTF-8")
    else:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'search' + sep() + 'searchreport.txt', 'r', encoding="UTF-8")
    fileLength = str(sum(1 for line in f) - 1)
    f.seek(0)
    if inLimit == "First25" or inLimit == "JustResults":
        for line in f:
            if line[:13] == 'Search text: ':
                print(line)
            else:
                if mycounter < upperLimit:
                    print('[' + str(mycounter) + ']\t' + activeColorOnTerm_IN(line.strip().split(' ')[0], ' '.join(line.strip().split(' ')[1:])) + tabString(line.strip().split(' ')[0]) + ' '.join(line.strip().split(' ')[1:]))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(line.strip().split(' ')[0])))))
                    mycounter += 1
    else:
        tlcLocal = tlc()
        lineList = []
        for line in f:
            if line[:13] == 'Search text: ':
                print(line)
            else:
                if mycounter < upperLimit:
                    lineList.append((line.strip().split(' ')[0], ' '.join(line.strip().split(' ')[1:])))
                    mycounter += 1
                else:
                    break
        mycounter = 1
        if inLimit == "ByConAlpha" or inLimit == "ByConLenShort" or inLimit == "ByConLen":
            tempList = []
            for line in lineList:
                if inLimit == "ByConAlpha":
                    append_distinct(tempList, (line[0], fsnFromConId_IN(line[0])[1]))
                else:
                    tempList.append((line[0], fsnFromConId_IN(line[0])[1], line[1]))
            if inLimit == "ByConAlpha":
                tempList.sort(key=lambda x: x[1])
            else:
                tempList.sort(key=lambda x: len(x[2]))
                tempList = [[item[0], item[1]] for item in tempList]
                if inLimit == "ByConLenShort":
                    upperLimit = 26
            for line in tempList:
                if mycounter < upperLimit:
                    print('[' + str(mycounter) + ']\t' + activeColorOnTerm_IN(line[0], line[1]) + tabString(line[0]) + line[1])
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(line[0])))))
                    mycounter += 1
        else: # ByTLC or ByTLCCon
            for item in tlcLocal:
                tempList = []
                for line in lineList:
                    if TCCompare_IN(line[0], item[2]):
                        if inLimit == "ByTLCCon" or letterSetBool:
                            append_distinct(tempList, (line[0], pterm(line[0])))
                        else:
                            tempList.append((line[0], line[1]))
                    if inLimit == "ByTLCCon":
                        tempList.sort(key=lambda x: x[1])
                if len(tempList) > 0:
                    print('\033[4m[' + str(mycounter) + ']\t' + activeColorOnTerm_IN(item[2], item[0]) + tabString(item[2]) + item[0] + '\033[24m')
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(item[2])))))
                    mycounter += 1
                    print()
                    for line in tempList:
                        if mycounter < upperLimit:
                            print('[' + str(mycounter) + ']\t' + activeColorOnTerm_IN(line[0], line[1]) + tabString(line[0]) + line[1] + hasDefInList(line[0], cfg("SHOW_DEFS")))
                            mydict.update(list(zip(listify(mycounter), listify(tuplify(line[0])))))
                            mycounter += 1
                    print()
    if upperLimit == 26:
        if int(fileLength) < 25:
            print('\nShowing all ' + fileLength + ' results.')
        else:
            print('\nShowing first 25 of ' + fileLength + ' results.')
    print()
    f.close()
    return mydict

def refersToTermColor(targetCId, sourceDId, FSNBool):
    if FSNBool:
        bareTerm = fsnFromConId_IN(sourceDId)[1]
    else:
        bareTerm = refsetPterm(sourceDId)
        sourceDId = CiDFromDiD_IN(tuplify(sourceDId))
    if TCCompare_IN(targetCId, sourceDId) and (targetCId != sourceDId):
        returnString = bareTerm
    elif TCCompare_IN(sourceDId, targetCId) and (targetCId != sourceDId):
        returnString = "\033[35m" + bareTerm + "\033[92m"
    else:
        returnString = "\033[31m" + bareTerm + "\033[92m"
    return returnString

def refersToArrows(refersToList, sourceCid):
    returnString = "("
    redSymbol = "\u2194" if cfg("TAB_INDENT") == 1 else "~"
    for row in refersToList:
        targetCid = row[2]
        if TCCompare_IN(targetCid, sourceCid) and ("\u2193" not in returnString) and (targetCid != sourceCid):
            returnString += "\033[92m\u2193\033[31m" # green down arrows
        elif TCCompare_IN(sourceCid, targetCid) and ("\u2191" not in returnString) and (targetCid != sourceCid):
            returnString += "\033[35m\u2191\033[31m" # magenta up arrow
        elif not TCCompare_IN(sourceCid, targetCid) and not TCCompare_IN(targetCid, sourceCid) and (redSymbol not in returnString):
            returnString += "\033[31m" + redSymbol + "\033[31m" # red side-side arrow
        elif (targetCid == sourceCid) and ("?" not in returnString):
            returnString += "\033[31m?\033[31m" # red question mark
    return returnString + ")"

def refersToEdgeColor(targetCid, sourceCid):
    if TCCompare_IN(targetCid, sourceCid) and (targetCid != sourceCid):
        returnString = "green"
    elif TCCompare_IN(sourceCid, targetCid) and (targetCid != sourceCid):
        returnString = "magenta"
    else:
        returnString = "red"
    return returnString

def showRefsetRowsFromConAndRefset(memberId, refsetId, mydict=0, mycounter=1):
    if mydict == 0:
        mydict = {}
    initialCounter = mycounter
    mydict.update(list(zip(listify(mycounter), listify(tuplify(memberId)))))
    myValueRows = refSetDetailsFromMemIdandSetId_IN(refsetId, memberId)
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    if len(myValueRows) == 0:
        openString = "No rows for "
    else:
        openString = "Rows for "
    print(openString + activeColor_IN(memberId) + " " + pterm(memberId) + ' in ' + metaColor_IN(refsetId, getVersion()) + refsetId + '\033[92m ' + pterm(refsetId))
    print()
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if (row[1] == tok['CONTYPE']) or (row[1] == tok['COMPTYPE']) or (row[1] == tok['DESCTYPECOMP']):  # if componentId datatype
                if refsetId == tok["REFERS_TO"] and value[counter][-2] == '1':
                    print('[' + str(mycounter) + ']\t' + row[0] + "\t" + activeColor_IN(value[counter]) + "\t" + pterm(value[counter]))
                    SourceId = CiDFromDiD_IN(tuplify(value[counter]))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(SourceId)))))
                    FSNString = refersToTermColor(value[counter + 1], SourceId, True)
                    print(' ' * (len(row[0])) + '\t\t' + activeColor_IN(SourceId, True) + '\t\033[2m' + FSNString + '\033[22m')
                else:
                    print('[' + str(mycounter) + ']\t' + row[0] + "\t" + activeColor_IN(value[counter]) + "\t" + pterm(value[counter]))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(value[counter])))))
                mycounter += 1
            elif row[1] == tok["PARS_STR"] and cfg("PRETTY") > 0:
                print('[*]\t' + row[0] + "\t" + str(parsablePretty(value[row[2]])))
            elif row[1] == tok['OWL_2_STR'] and cfg("PRETTY") > 0:
                print('[*]\t' + row[0] + "\t" + str(parsablePrettyOwl(value[row[2]])))
            else:
                print('[*]\t' + row[0] + "\t" + str(value[row[2]]))
            counter += 1
        print(metaColor_IN(refsetId, getVersion(), value[-4]) + '(Module=' + pterm(value[-4]) + ', Active=' + str(value[-2]) + ', Time=' + value[-3] + ')\033[92m')
        print('\033[36m(Row Id=' + value[-1] + ')\033[92m')
        print()
    if refsetId == tok["MOVED_TO"]:
        a = historicalOriginsFromConId_IN(memberId, 1)
        if len(a) > 0:
            print('Historical origins for', activeColor_IN(memberId), pterm(memberId))
            tempId2 = '1'
            for row in a:
                tempId = row[0]
                if tempId != tempId2:
                    print()
                    print('[' + str(mycounter) + ']\t' + row[0] + tabString(row[0]) + pterm(row[0]))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(row[0])))))
                mycounter += 1
                if row[1][-2] == '1':
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(CiDFromDiD_IN(tuplify(row[1]))) + tabString(CiDFromDiD_IN(tuplify(row[1]))) + refsetPterm(row[1]))
                else:
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(row[1]) + tabString(row[1]) + refsetPterm(row[1]))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(row[1])))))
                tempId2 = tempId
                print(metaColor_IN(refsetId, getVersion(), row[3]) + '(Module=' + pterm(row[3]) + ', Active=' + str(row[5]) + ', Time=' + row[4] + ')\033[92m')
                print('\033[36m(Row Id=' + row[6] + ')\033[92m')
        else:
            print('No historical origins available.')
        print()
    print('[' + str(initialCounter) + '] for', activeColor_IN(memberId), pterm(memberId))
    print()
    drawMap(memberId, pterm(memberId), refsetId, myValueRows)
    return mydict

def drawMap(memberId, term, refsetId, myValueRows):
    refsetType = refSetTypeFromConId_IN(refsetId)
    blockNoList = []
    blockGroupDict = {}
    blockGroupPriorityCodeList = []
    blockGroupMaxPriorityDict = {}
    nodeList = []
    edgeIdDict = {}
    # check it's a uk complex map
    if refsetType[0][0][0:7] == 'iisssci':
        # identify blocks
        for value in myValueRows:
            if value[7] not in blockNoList:
                blockNoList.append(value[7])
        # - and what's in them
        # prime groups in blocks dictionary
        for blockNo in blockNoList:
            blockGroupDict[blockNo] = []
        # identify groups
        for blockNo in blockNoList:
            for value in myValueRows:
                if value[1] not in blockGroupDict[blockNo] and (value[7] == blockNo):
                    blockGroupDict[blockNo].append(value[1])
        # identify block + group codes with notes and priorities
        for blockNo in blockNoList:
            for groupNo in blockGroupDict[blockNo]:
                for value in myValueRows:
                    tempPriorityCodeList = []
                    tempPriorityCodeList.append(blockNo)
                    tempPriorityCodeList.append(groupNo)
                    tempPriorityCodeList.append(value[2])
                    tempPriorityCodeList.append(value[4])
                    tempPriorityCodeList.append(value[5])
                    if tempPriorityCodeList not in blockGroupPriorityCodeList and (value[7] == blockNo) and (value[1] == groupNo):
                        blockGroupPriorityCodeList.append(tempPriorityCodeList)
        # identify default group codes (as maxValue)
        for blockNo in blockNoList:
            for groupNo in blockGroupDict[blockNo]:
                maxValue = 0
                for blockGroupPriorityCode in blockGroupPriorityCodeList:
                    if blockGroupPriorityCode[2] > maxValue and (blockGroupPriorityCode[0] == blockNo) and (blockGroupPriorityCode[1] == groupNo):
                        maxValue = blockGroupPriorityCode[2]
                blockGroupMaxPriorityDict[str(blockNo) + "_" + str(groupNo)] = maxValue
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc_temp.dot', 'w', encoding="UTF-8")
        # write headers
        f.write("strict digraph G { rankdir=LR; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white, color=white];\n")
        # write containing cluster
        mapTermString = mapTerm(refsetId, len(blockGroupPriorityCodeList))
        checkString = conMetaFromConId_IN(memberId, False)[3]
        if checkString == tok["PRIMITIVE"]:
            colorString = "\"#99CCFF\""

        else:
            colorString = "\"#CCCCFF\"; peripheries=2"
        f.write("subgraph cluster_0 { style=filled; color=" + colorString + "; fontsize=9; fontname=Helvetica;\n")
        f.write("label = \"" + mapTermString + memberId + "\\n" + term + "\";\n")
        for blockNo in blockNoList:
            # write block clusters
            f.write("subgraph cluster_" + str(blockNo) + " { color=coral;\n")
            f.write("label = \"Block " + str(blockNo) + "\";\n")
            for groupNo in blockGroupDict[blockNo]:
                # write group clusters
                f.write("subgraph cluster_" + str(blockNo) + "_" + str(groupNo) + " { color=aquamarine;\n")
                f.write("label = \"Group " + str(groupNo)  + "\";\n")
                for blockGroupPriorityCode in blockGroupPriorityCodeList:
                    if (blockGroupPriorityCode[0] == blockNo) and (blockGroupPriorityCode[1] == groupNo):
                        # write nodes in group clusters
                        f.write("\"" + str(blockNo) + "_" + str(groupNo) + "_" + blockGroupPriorityCode[4] + "\";\n")
                        # fill node list and levelling edge Ids list
                        if blockGroupPriorityCode[2] == blockGroupMaxPriorityDict[str(blockNo) + "_" + str(groupNo)]:
                            nodeList.append("\"" + str(blockNo) + "_" + str(groupNo) + "_" + blockGroupPriorityCode[4] + "\" [label=\"" + targetPrepare(blockGroupPriorityCode[4]) + "\\n" + advicePrepare(blockGroupPriorityCode[3]) + "\", fillcolor=yellow, color=yellow];\n")
                        else:
                            nodeList.append("\"" + str(blockNo) + "_" + str(groupNo) + "_" + blockGroupPriorityCode[4] + "\" [label=\"" + targetPrepare(blockGroupPriorityCode[4]) + "\\n(" + str(blockGroupPriorityCode[2]) + ")\\n" + advicePrepare(blockGroupPriorityCode[3]) + "\"];\n")
                        if blockGroupPriorityCode[1] == 1 and blockGroupPriorityCode[2] == 1:
                            edgeIdDict[blockNo] = blockGroupPriorityCode[4]
                f.write("}\n")
            f.write("}\n")
        # write invisible levelling edges
        for n in range(max(blockNoList), 1, -1):
            f.write("\"" + str(n -1) + "_1_" + edgeIdDict[n -1] + "\"->\"" + str(n) + "_1_" + edgeIdDict[n] + "\" [style=invis];\n")
        # write nodes
        for node in nodeList:
            f.write(node)
        f.write("}\n}")
        f.close()
        renameDiagramFile('contc_temp.dot', 'contc.dot')
        xmapToSVGLocal()

def drawMDR(valueRows, inET="Snap"):
    nonReferencedList = []
    matchedModel = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc_temp.dot', 'w', encoding="UTF-8")
    if getVersion() == 'Int,': # if UK data
        testModList = [mod[0] for mod in prelim(tokList('OWNMODULESET'))]
    else:
        testModList = [mod[0] for mod in prelim(tokList('INTMODULE'))]
    f.write("strict digraph G { rankdir=BT; ranksep=.3; splines=ortho; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=\"#99CCFF\"];\n")
    findSequences = []
    for row in valueRows:
        if (row[0], row[2]) not in findSequences:
            findSequences.append((row[0], row[2]))
        if (row[3], row[1]) not in findSequences:
            findSequences.append((row[3], row[1]))
        fromString = "\"" + row[3] + " (" + row[1] + ")\\n" + simplify(pterm(row[3])) + "\""
        toString = "\"" + row[0] + " (" + row[2] + ")\\n" + simplify(pterm(row[0])) + "\";\n"
        if row[3] not in testModList:
            append_distinct(nonReferencedList, "\"" + row[3] + " (" + row[1] + ")\\n" + simplify(pterm(row[3])) + "\" [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=pink];")
        if row[0] not in testModList:
            append_distinct(nonReferencedList, "\"" + row[0] + " (" + row[2] + ")\\n" + simplify(pterm(row[3])) + "\" [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=pink];")
        f.write(fromString + "->" + toString)
        if row[0] == tok["MODEL"] and inET != "Snap" and toString not in matchedModel:
            matchedModel.append(toString)
            f.write(toString[:-2] + "->" + "\"" + str(inET) + "\" [style=invis];\n")
    findSequences.sort(key=lambda x: (x[0], x[1]))
    for counter in range(len(findSequences) - 1):
        if findSequences[counter][0] == findSequences[counter + 1][0]:
            fromString = "\"" + findSequences[counter][0] + " (" + findSequences[counter][1] + ")\\n" + simplify(pterm(findSequences[counter][0])) + "\""
            toString = "\"" + findSequences[counter + 1][0] + " (" + findSequences[counter + 1][1] + ")\\n" + simplify(pterm(findSequences[counter + 1][0])) + "\" [style=dotted, color=blue];\n"
            f.write(fromString + "->" + toString)
    if inET != "Snap":
        f.write("\"" + str(inET) + "\" [label=\"" + str(inET) + "\", shape=box, peripheries=2, style=filled, fillcolor=pink];\n")
    for item in nonReferencedList:
        f.write(item)
    f.write("}\n")
    f.close()
    renameDiagramFile('contc_temp.dot', 'contc.dot')

def checkColorStringForNav(inId, inColor):
    if (cfg("DRAW_DIAG") == 13) and (cfg("NAV_SS") == 2):
        actualList, structuralList, pathList = readSSFiles()
        childList = childrenIDFromId_IN(inId)
        if inId in structuralList:
            return "\"#BD00FF\""
        elif inId in actualList:
            return "\"#0078FF\""
        elif any(item in pathList for item in childList) or any(item in actualList for item in childList):
            return "\"#FFF37D\""
        else:
            return "\"#55EE55\""
    else:
        return inColor

def toString(inList):
    outString = ""
    if len(inList) == 1:
        outString =  inList[0]
    else:
        for item in inList[0:-1]:
            outString += item + "\\n"
        outString += inList[-1]
    return outString

def dotDrawTreeMapIsh(centreString, inDict, mondrianBool, mondBackBool, guidelineColorsBool):
    key_list = list(inDict.keys())
    val_list = list(inDict.values())
    classList = []
    children = childrenFromId_IN(centreString)
    if cfg("ADD_INVIS") == 1:
        shapeString = "box"
    else:
        shapeString = "ellipse"
    if mondBackBool or ((cfg("DRAW_DIAG") == 13) and (cfg("NAV_SS") == 2)):
        outString = "digraph { rankdir=BT; ranksep=.3; overlap=false; node [shape=" + shapeString + ", fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\nsubgraph cluster_0 { style=filled; color=black;  penwidth=200;\n"
    else:
        outString = "digraph { rankdir=BT; ranksep=.3; overlap=false; node [shape=" + shapeString + ", fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n"
    colorString1 = "89ABCDEF"
    colorString3 = "0123456789ABCDEF"

    sizeTot = 0
    treemapIshList = []
    patchworkList = []
    totalSubs = len(children)
    for item in children:
        sizestring = descendantCountFromConId_IN(item[2])
        childSizestring = str(childCountFromId_IN((item[2],)))
        sizeTot = sizeTot + sizestring
        classList.append([item[2], sizestring, childSizestring])

    highestQualFont = 0
    lowestQualFont = 1000

    evenOdd = 1
    for classRow in classList:
        peripheries = "1"
        if hasDef(classRow[0]) and cfg("SHOW_DEFS") > 0:
            fontColor = "blue"
            if isTag(classRow[0]) and cfg("SHOW_DEFS") > 4: # overrides simple hasDef+
                fontColor = "purple" # any tag insertion point
        elif isTag(classRow[0]) and cfg("SHOW_DEFS") > 4:
            fontColor = "purple" # any tag insertion point
        else:
            fontColor = "black"
        if inDict == {}: # greyed out top level resting position
            pw = 1
            colorString2 = '\"#EEEEEE#\"'
            fontColor = 'gray'
        else:
            # basic colours
            if not mondrianBool:
                pw = 1
                if guidelineColorsBool:
                    pw = 15
                    checkString = conMetaFromConId_IN(classRow[0], False)[3]
                    if checkString == tok["PRIMITIVE"]:
                        colorString2 = "\"#99CCFF\""
                        colorString2 = checkColorStringForNav(classRow[0], colorString2)
                    else:
                        peripheries = "2"
                        colorString2 = "\"#CCCCFF\""
                        colorString2 = checkColorStringForNav(classRow[0], colorString2)
                else:
                    colorString2 = '\"#'
                    colorCounter = 1
                    while colorCounter < 4:
                        colorPosition = int(random() * len(colorString1))
                        colorPosition2 = int(random() * len(colorString3))
                        colorString2 = colorString2 + colorString1[colorPosition] + colorString3[colorPosition2]
                        colorCounter += 1
                    colorString2 = colorString2 + '#\"'
                    CS2ChoiceDict = {1:colorString2[0:2] + "10" + colorString2[4:10],
                                    2:colorString2[0:4] + "10" + colorString2[6:10],
                                    3:colorString2[0:6] + "10" + colorString2[8:10],
                                    4:colorString2[0:2] + "1010" + colorString2[6:10],
                                    5:colorString2[0:4] + "1010" + colorString2[8:10],
                                    6:colorString2[0:2] + "10" + colorString2[4:6] + "10" + colorString2[8:10]}
                    FCChoiceDict = {1:"black", 2:"white", 3:"black", 4:"white", 5:"white", 6:"black"}
                    if (evenOdd / 2) == (int(evenOdd / 2)):
                        testNum = int(random() * 6) + 1
                        colorString2 = CS2ChoiceDict[testNum]
                        fontColor = FCChoiceDict[testNum]
            # mondrian colours...
            else:
                if mondBackBool:
                    pw = 1
                    colorNumber = int(random() * 100)
                    if colorNumber < 60:
                        colorString2 = "white"
                        fontColor = "white"
                    elif colorNumber < 70:
                        colorString2 = "red"
                        fontColor = "red"
                    elif colorNumber < 80:
                        colorString2 = "yellow"
                        fontColor = "yellow"
                    elif colorNumber < 90:
                        colorString2 = "lightgray"
                        fontColor = "lightgray"
                    else:
                        colorString2 = "blue"
                        fontColor = "blue"
                else:
                    pw = 80
                    colorNumber = int(random() * 100)
                    if colorNumber < 50:
                        colorString2 = "white"
                        fontColor = "white"
                    elif colorNumber < 60:
                        colorString2 = "red"
                        fontColor = "red"
                    elif colorNumber < 70:
                        colorString2 = "yellow"
                        fontColor = "yellow"
                    elif colorNumber < 80:
                        colorString2 = "lightgray"
                        fontColor = "lightgray"
                    elif colorNumber < 90:
                        colorString2 = "blue"
                        fontColor = "blue"
                    else:
                        colorString2 = "black"
                        fontColor = "black"

        dimensionInt = int(math.sqrt(50000 * (classRow[1] / sizeTot)))
        # compensate penwidth for very small
        # if mondrianBool and not mondBackBool:
        if pw > 1:
            patchPw = pw / 10
        else:
            patchPw = pw
        if dimensionInt < 5:
            pw = dimensionInt + 1
        dimensionString = str(dimensionInt)
        if totalSubs > 10:
            fontSizeString = str(int(math.sqrt(1000000 * (classRow[1] / sizeTot))))
        else:
            fontSizeString = str(int(math.sqrt(500000 * (classRow[1] / sizeTot))))
        if classRow[1] > 1:
            countString = "\\n" + str(classRow[1]) + " (" + str(classRow[2]) + ")"
        else:
            countString = ""
        patchFontSizeInt = round(int(fontSizeString)/20) + 1
        if patchFontSizeInt < 3:
            patchFontSizeInt *= 2
        patchFontSize = str(patchFontSizeInt)
        if inDict == {}:
            treemapIshList.append('\"' + classRow[0] + '\" [peripheries=' + peripheries + ', fillcolor=' + colorString2 + ', fontcolor=' + fontColor + ', penwidth=' + str(pw) + ', shape=' + shapeString + ', label=\"' + classRow[0] + '\\n' + toString(textwrap.wrap(simplify(pterm(classRow[0])), 25)) + countString + '\", height=' + dimensionString + ', width=' + dimensionString + ', fontsize=' + fontSizeString + ' ];')
            patchworkList.append(['\"' + classRow[0] + '\" [peripheries=1, fillcolor=' + colorString2 + ', fontcolor=' + fontColor + ', penwidth=' + str(patchPw) + ', shape=' + shapeString + ', label=\"' + classRow[0] + '\\n' + toString(textwrap.wrap(simplify(pterm(classRow[0])), 25)) + countString + '\", area=' + str(int(dimensionString) + 1) + ', fontsize=' + patchFontSize + ' ];\n', str(int(dimensionString) + 1)])
        else:
            treemapIshList.append('\"' + classRow[0] + '\" [peripheries=' + peripheries + ', fillcolor=' + colorString2 + ', fontcolor=' + fontColor + ', penwidth=' + str(pw) + ', shape=' + shapeString + ', label=\"[' + str(key_list[val_list.index(tuplify(classRow[0]))]) + '] ' + classRow[0] + '\\n' + toString(textwrap.wrap(simplify(pterm(classRow[0])), 25)) + countString + '\", height=' + dimensionString + ', width=' + dimensionString + ', fontsize=' + fontSizeString + ' ];')
            showIndex = True
            if showIndex:
                patchworkList.append(['\"' + classRow[0] + '\" [peripheries=1, fillcolor=' + colorString2 + ', fontcolor=' + fontColor + ', penwidth=' + str(patchPw) + ', shape=' + shapeString + ', label=\"[' + str(key_list[val_list.index(tuplify(classRow[0]))]) + '] ' + classRow[0] + '\\n' + toString(textwrap.wrap(simplify(pterm(classRow[0])), 25)) + countString + '\", area=' + str(int(dimensionString) + 1) + ', fontsize=' + patchFontSize + ' ];\n', str(int(dimensionString) + 1)])
            else:
                patchworkList.append(['\"' + classRow[0] + '\" [peripheries=1, fillcolor=' + colorString2 + ', fontcolor=' + fontColor + ', penwidth=' + str(patchPw) + ', shape=' + shapeString + ', label=\"' + classRow[0] + '\\n' + toString(textwrap.wrap(simplify(pterm(classRow[0])), 25)) + countString + '\", area=' + str(int(dimensionString) + 1) + ', fontsize=' + patchFontSize + ' ];\n', str(int(dimensionString) + 1)])
        if (int(2000 * (classRow[1] / sizeTot))) > 20:
            if int(2000 * (classRow[1] / sizeTot)) > highestQualFont:
                highestQualFont = int(2000 * (classRow[1] / sizeTot))
            if int(2000 * (classRow[1] / sizeTot)) < lowestQualFont:
                lowestQualFont = int(2000 * (classRow[1] / sizeTot))
        evenOdd += 1

    avgFontSize = str(int((highestQualFont + lowestQualFont) / 4))

    if inDict == {}:
        classStringColor = '\"#EEEEEE#\"'
        fontColor = 'gray'
    else:
        if guidelineColorsBool:
            pw = 0
            classStringColor = 'white'
            pw = 15
            checkString = conMetaFromConId_IN(centreString[0], False)[3]
            if checkString == tok["PRIMITIVE"]:
                classStringColor = "\"#99CCFF\""
            else:
                peripheries = "2"
                classStringColor = "\"#CCCCFF\""
        else:
            classStringColor = 'yellow'
        if mondrianBool:
            fontColor = 'yellow'
        else:
            if guidelineColorsBool:
                fontColor = 'red'
            else:
                fontColor = 'black'
    if totalSubs > 1:
        conceptString = " concepts"
    else:
        conceptString = " concept"
    treemapIshList.append('\"' + centreString[0] + '\" [fillcolor=' + classStringColor + ', fontsize=' + avgFontSize + ', fontcolor=' + fontColor + ', penwidth=' + str(pw) + ', shape=' + shapeString + ', label=\"' + '\\nShowing ' + str(totalSubs) + conceptString + " beneath:\\n" + centreString[0] + "\\n" + toString(textwrap.wrap(simplify(pterm(centreString[0])), 25)) + '\\n \"];')

    # generate treemapIsh treemap
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'treemapIsh.dot', 'w', encoding="UTF-8")
        f.write(outString)
        # f.write(classString)
        for line in treemapIshList:
            f.write(line)
        if mondBackBool  or ((cfg("DRAW_DIAG") == 13) and (cfg("NAV_SS") == 2)):
            f.write("}\n}")
        else:
            f.write("}")
    finally:
        f.close()
   # generate patchwork treemap
    patchworkList.sort(key=lambda x: int(x[1]), reverse=True)
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'patchwork_temp.dot', 'w', encoding="UTF-8")
        f.write(outString.replace("rankdir=BT;", "layout=\"patchwork\";"))
        for line in patchworkList:
            f.write(line[0])
        if mondBackBool  or ((cfg("DRAW_DIAG") == 13) and (cfg("NAV_SS") == 2)):
            f.write("}\n}")
        else:
            f.write("}")
    finally:
        f.close()
        renameDiagramFile('patchwork_temp.dot', 'patchwork.dot')

def targetPrepare(inString):
    if inString[0] == "#":
        return inString
    else:
        return inString[:3] + "." + inString[3:]

def advicePrepare(inString):
    returnString = ""
    tempList = inString.split("|")
    for item in tempList:
        if "ALWAYS" not in item:
            returnString += item.strip() + "\\n"
    return returnString

def mapTerm(refsetId, mapCount):
    if mapCount == 1:
        mapString = "ap"
    else:
        mapString = "aps"
    if refsetId == tok["ICD_10"]:
        return "ICD 10 m" + mapString + " for "
    elif refsetId == tok["OPCS_48"]:
        return "OPCS 4.8 m" + mapString + " for "
    elif refsetId == tok["OPCS_49"]:
        return "OPCS 4.9 m" + mapString + " for "
    else:
        return "M" + mapString + " for "

def showRefsetRowsFromConAndRefsetPlusMeta(memberId, refsetId):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(memberId)))))
    print('Rows for ', activeColor_IN(memberId), pterm(memberId), 'in', refsetId, pterm(refsetId))
    print()
    myValueRows = refSetDetailsFromMemIdandSetIdPlusMeta_IN(refsetId, memberId, 1)
    print(myValueRows)
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    print(myDescRows)
    for value in myValueRows:
        counter = 0
        print(value)
        for row in myDescRows:
            print(row)
            if (row[1] == tok['CONTYPE']) or (row[1] == tok['COMPTYPE']):  # if componentId datatype
                print('[' + str(mycounter) + ']\t' + row[0] + "\t" + activeColor_IN(value[mycounter + 2]) + "\t" + pterm(value[counter + 2]))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(value[counter])))))
                mycounter += 1
            elif row[1] == tok["PARS_STR"] and cfg("PRETTY") > 0:
                print('[*]\t' + row[0] + "\t" + str(parsablePretty(value[row[2] + 3])))
            elif row[1] == tok['OWL_2_STR'] and cfg("PRETTY") > 0:
                print('[*]\t' + row[0] + "\t" + str(parsablePrettyOwl(value[row[2] + 3])))
            else:
                print('[*]\t' + row[0] + "\t" + str(value[row[2] + 3]))
        print()
    print('[1] for', memberId, pterm(memberId))
    print()
    return mydict


def refsetPtermWithDict(inDict, inKey):
    if inKey in inDict:
        return inDict[inKey]
    else:
        return refsetPterm(inKey)


def showRefsetRowsForRefset(refsetId, showMeta):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(refsetId)))))
    mycounter += 1
    print('Rows for ' + metaColor_IN(refsetId, getVersion()) + refsetId + '\033[92m', pterm(refsetId))
    print()
    myRefset = refSetTypeFromConId_IN(refsetId)
    for refset in myRefset:
        refsetType = refset[0]
    myCount = refSetMemberCountFromConId_IN(refsetId, refsetType)
    print("refset member count= ", str(myCount))
    secondaction = input2_3('Sample [Y/N]:')
    if secondaction == 'y':
        if myCount < 300:
            print()
            myfilter = 0
        elif myCount < 9000:
            print("approx 1/10 sample:")
            print()
            myfilter = 1
        elif myCount < 90000:
            print("approx 1/100 sample:")
            print()
            myfilter = 2
        elif myCount < 1000000:
            print("approx 1/10000 sample:")
            print()
            myfilter = 3
        else:
            print("too many members to list")
            print()
            return mydict
    else:
        if myCount < 6000:
            print()
            myfilter = 0
        else:
            thirddaction = input('Really? [Y/N]:')
            if thirddaction == 'y':
                myfilter = 0
            else:
                print("sorry, too many members to list unfiltered")
                print()
                return mydict
    if showMeta:
        myValueRows = refSetMembersFromConIdPlusMeta_IN(refsetId, refsetType, myfilter, myCount)
    else:
        myValueRows = refSetMembersFromConId_IN(refsetId, refsetType, myfilter, myCount)
    # find terms for myValueRows and sort memberIds on them
    myValueTermsDict = {}
    for value in myValueRows:
        myValueTermsDict[value[0]] = refsetPterm(value[0])
    sortOrderIds = [x[0] for x in sorted(myValueTermsDict.items(), key=lambda x: x[1].lower())]
    if (sys.version_info > (3, 0)):
        # Python 3 code:
        myValueRows.sort(key=lambda i: sortOrderIds.index(i[0]))
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    myRefsetType = 'c' + refSetTypeFromConId_IN(refsetId)[0][0]  # if component datatype (can't pick up which one directly)
    refersToCounterDict = {"Refers to child":0, "\033[35mRefers to ancestor\033[92m":0, "\033[31mSemantically remote\033[92m":0}
    inactiveRefersToSources = 0
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            # if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
            if (row[1] == tok['CONTYPE']) or (TCCompare_IN(row[1], tok['COMPTYPE'])):  # if componentId datatype
                if refsetId == tok["REFERS_TO"] and value[counter][-2] == '1':
                    print('[' + str(mycounter) + ']\t' + row[0] + "\t" + activeColor_IN(value[counter]) + "\t" + refsetPtermWithDict(myValueTermsDict, value[counter]))
                    SourceId = CiDFromDiD_IN(tuplify(value[counter]))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(SourceId)))))
                    FSNString = refersToTermColor(value[counter + 1], SourceId, True)
                    # if inactive add one to the inactives
                    if conMetaFromConId_IN(SourceId, False)[1] == 0:
                        inactiveRefersToSources += 1
                    if "[31m" in FSNString:
                        refersToCounterDict["\033[31mSemantically remote\033[92m"] += 1
                    elif "[35m" in FSNString:
                        refersToCounterDict["\033[35mRefers to ancestor\033[92m"] += 1
                    else:
                        refersToCounterDict["Refers to child"] += 1
                    print(' ' * (len(row[0])) + '\t\t' + activeColor_IN(SourceId, True) + '\t\033[2m' + FSNString + '\033[22m')
                else:
                    print('[' + str(mycounter) + ']\t' + row[0] + "\t" + activeColor_IN(value[counter]) + "\t" + refsetPtermWithDict(myValueTermsDict, value[counter]))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(value[counter])))))
                mycounter += 1
            else:
                if row[1] == tok["PARS_STR"] and cfg("PRETTY") > 0:
                    print('[*]\t' + row[0] + "\t" + str(parsablePretty(value[row[2]])))
                elif row[1] == tok["OWL_2_STR"] and cfg("PRETTY") > 0:
                    print('[*]\t' + row[0] + "\t" + str(parsablePrettyOwl(value[row[2]])))
                else:
                    print('[*]\t' + row[0] + "\t" + str(value[row[2]]))
            counter += 1
        if showMeta:
            print(metaColor_IN(refsetId, getVersion(), value[-4]) + '(Module=' + pterm(value[-4]) + ', Active=' + str(value[-2]) + ', Time=' + value[-3] + ')\033[92m')
            print('\033[36m(Row Id=' + value[-1] + ')\033[92m')
        if len(myDescRows) > 1:
            print()
    print()
    # display REFERS TO CONCEPT results
    if refsetId == tok["REFERS_TO"]:
        key_list = list(refersToCounterDict.keys())
        for key in key_list:
            if key != "\033[31mSemantically remote\033[92m":
                print(key + ":\t" + str(refersToCounterDict[key]))
            else:
                print(key + ":\t" + str(refersToCounterDict[key]) + " (" + str(inactiveRefersToSources) + " inactive.)")
        print()
    print('[1] for ' + metaColor_IN(refsetId, getVersion()) + refsetId + '\033[92m', pterm(refsetId))
    print()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetreport.txt', 'w', encoding="UTF-8")
    f.write('Members of ' + refsetId + ' ' + pterm(refsetId) + ':\n\n')
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                f.write(row[0] + "\t" + value[counter] + "\t" + refsetPtermWithDict(myValueTermsDict, value[counter]) + '\n')
                counter += 1
            else:
                f.write(row[0] + "\t" + str(value[row[2]]) + '\n')
                counter += 1
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'testset.txt', 'w', encoding="UTF-8")
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                if TCCompare_IN(value[counter], tok["DISORDER"]):
                    f.write(value[counter] + "\t" + refsetPtermWithDict(myValueTermsDict, value[counter]) + '\n')
                counter += 1
            else:
                f.write(str(value[row[2]]) + '\n')
                counter += 1
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                f.write(value[counter] +'\n')
                counter += 1
            else:
                counter += 1
    f.close()
    if refsetId == tok["MDR"] and showMeta:
        drawMDR(myValueRows)
    return mydict

def activeMetaColor(inValue):
    if inValue == 1 or inValue == '1':
        return ', Active=' + str(inValue)
    else:
        return ', \033[31mActive=' + str(inValue) + '\033[36m'

def conMetaFromConId(idIn):
    mydict = {}
    mycounter = 1
    conMeta = []
    conMeta = conMetaFromConId_IN(idIn, True)
    print("Concept Metadata for:\n\n" + activeColor_IN(idIn) + "\t" + pterm(idIn))
    if len(conMeta) == 4:
        print('\033[36m(Module=' + conMeta[0] + ', Active=' + str(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ')\033[92m')
        print('\033[36m(Children=' + str(childCountFromId_IN(tuplify(idIn))) + ('' if (descendantCountFromConId_IN(idIn) == 1) else ', Descendants=' + str(descendantCountFromConId_IN(idIn))) + ')\033[92m')
    else:
        print('\033[36m(Module=' + conMeta[0] + ', \033[31mActive=' + str(conMeta[1]) + '\033[36m, Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ', Inac.reason=' + conMeta[4] + ')\033[92m')
    if TCCompare_IN(idIn, tok["REFSETS"]):
        myRefset = refSetTypeFromConId_IN(idIn)
        for refset in myRefset:
            refsetType = refset[0]
        memCount = refSetMemberCountFromConId_IN(idIn, refsetType)
        if memCount > 0:
            maxET = maxETForRefset_IN(idIn, refsetType)
            print('\033[36m(Member count=' + str(memCount) + ', Latest membership change=' + maxET + ')\033[92m')
    print()
    print("([1] to select concept, [v] to return)")
    print()
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    return mydict


def refSetFromConIdPlusMeta(idIn):
    mydict = {}
    mycounter = 1
    refsetType = refsetList()

    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    print("Refsets for: " + activeColor_IN(idIn) + " " + refsetPterm(idIn))
    print("([1] to select concept, [v] to return)")
    print()

    if len(refsetType) > 0:
        for rType in refsetType:
            refs = refSetFromConIdPlusMeta_IN(idIn, rType, 1)
            for ref in refs:
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(ref[0])))))
                print('[' + str(mycounter) + ']\t' + ref[0] + tabString(ref[0]) + ref[1] + '\n' + metaColor_IN(ref[0], getVersion()) + '(Time=' + ref[4] + ', Module=' + ref[3] + ')\033[92m')
    else:
        print("No refsets.")
    print()
    return mydict


def refSetFromConId(idIn):
    mydict = {}
    mycounter = 1
    refsetType = refsetList()

    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    print("Refsets for: " + activeColor_IN(idIn) + " " + refsetPterm(idIn))
    print("([1] to select concept, [v] to return)")
    print()
    tempList = []
    if len(refsetType) > 0:
        for rType in refsetType:
            refs = refSetFromConId_IN(idIn, rType)
            for ref in refs:
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(ref[0])))))
                print('[' + str(mycounter) + ']\t' + ref[0] + tabString(ref[0]) + ref[1])
        for rType in refsetType:
            refs = nonRCIDRefSetFromConId_IN(idIn, rType)
            for ref in refs:
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(ref[0])))))
                tempList.append([str(mycounter), ref[0], ref[1]])
        if len(tempList) > 0:
            print('\nNon-referencedComponentId references (details not accessible by \'o\' call):')
            for  row in tempList:
                print('[' + row[0] + ']\t' + row[1] + tabString(row[1]) + row[2])
    else:
        print("No refsets.")
    print()
    return mydict


def ancestorsFromConId(idIn, onlyDefsBool):
    mydict = {}
    mycounter = 1
    findid = 0
    # ancestors
    ancestorList = []
    definitionList = []
    ancestorList = ancestorsFromConId_IN(idIn)  # call interface neutral code
    if len(ancestorList) > 0:
        if onlyDefsBool:
            ancestorString = ' (with definitions only)'
        else:
            ancestorString = ''
        print('Ancestors' + ancestorString + ': [' + str(len(ancestorList)) + '] for', idIn, pterm(idIn))
        print()
        for ancestor in ancestorList:
            if not ancestor is None:
                hasDefString = hasDefInList(ancestor[0], cfg("SHOW_DEFS"))
                if (hasDefString != "" and onlyDefsBool) or onlyDefsBool is False:
                    if hasDefString != "":
                        definitionList.append(ancestor)
                    # CONC - relax level of concept model attributes
                    if TCCompare_IN(idIn, tok["CONMOD_ATTRIB"]):
                        print('[' + str(mycounter) + ']\t' + attribColor_IN(ancestor[0]) + tabString(ancestor[0]) + ancestor[1] + hasDefInList(ancestor[0], cfg("SHOW_DEFS")))
                    else:
                        print('[' + str(mycounter) + ']\t' + activeColor_IN(ancestor[0], True) + tabString(ancestor[0]) + ancestor[1] + hasDefInList(ancestor[0], cfg("SHOW_DEFS")))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(ancestor[0])))))
                    if ancestor[0] == idIn:
                        findid = mycounter
                    mycounter += 1
        f2 = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
        if onlyDefsBool:
            for ancestor in definitionList:
                f2.write(ancestor[0] + '\n')
        else:
            for ancestor in ancestorList:
                f2.write(ancestor[0] + '\n')
        f2.close()
        print()
        if findid == 0:
            findid = mycounter
            mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
        print('[' + str(findid) + '] to return to', idIn, pterm(idIn))
        print()
    return mydict

def deepestRels(limitInt):
    mydict = {}
    mycounter = 1
    relList, depth = deepestRels_IN(limitInt)
    print('Deepest nesting found after ' + str(limitInt if limitInt < depth else depth) + ' cycles:\n')
    objList = []
    valueList = []
    for rel in relList:
        if [rel[0]] not in objList:
            objList.append([rel[0]])
        if [rel[1]] not in valueList:
            valueList.append([rel[1]])
    for obj in objList:
        obj.append(pterm(obj[0]))
    for val in valueList:
        val.append(pterm(val[0]))
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    print('\033[4mObjects with deep definitions [' + str(len(objList)) + ']:\033[24m\n')
    for item in sorted(objList, key=lambda x: x[1].lower()):
        hasDefString = hasDefInList(item[0], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + item[1] + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        f.write(item[0] + '\n')
        mycounter += 1
    print('\n\033[4mDeeply nested values [' + str(len(valueList)) + ']:\033[24m\n')
    for item in sorted(valueList, key=lambda x: x[1].lower()):
        hasDefString = hasDefInList(item[0], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + item[1] + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        mycounter += 1
    print()
    f.close()
    return mydict

def busyCrossMaps(limitInt):
    mydict = {}
    mycounter = 1
    mapList, returnNumber = busyCrossMaps_IN(limitInt)
    print(str(returnNumber) + ' busiest cross-maps [block count in bracket]:\n')
    findList = []
    procList = []
    for mapItem in mapList:
        if mapItem not in findList and TCCompare_IN(mapItem[1], tok["CLIN_FIND"]):
            findList.append(mapItem)
        if mapItem not in procList and TCCompare_IN(mapItem[1], tok["PROCEDURE"]):
            procList.append(mapItem)
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    print('\033[4mFindings with maps [' + str(len(findList)) + ']:\033[24m\n')
    for item in findList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[1], False) + tabString(item[1]) + item[2] + " [" + str(item[0]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
        f.write(item[1] + '\n')
        mycounter += 1
    print('\n\033[4mProcedures with maps [' + str(len(procList)) + ']:\033[24m\n\n')
    for item in procList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[1], False) + tabString(item[1]) + item[2] + " [" + str(item[0]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
        f.write(item[1] + '\n')
        mycounter += 1
    for mapItem in sorted(mapList, key=lambda x: x[2].lower()):
        f.write(mapItem[1] + '\n')
    print()
    f.close()
    return mydict

def highDefRels(limitInt):
    mydict = {}
    mycounter = 1
    conList, returnNumber = highDefRels_IN(limitInt)
    print(str(returnNumber) + ' concepts with high defining relationships [number in bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[1], True) + tabString(item[1]) + item[2] + " [" + str(item[0]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
        mycounter += 1
    for item in sorted(conList, key=lambda x: x[2].lower()):
        f.write(item[1] + '\n')
    print()
    f.close()
    return mydict

def randomConcepts(limitInt):
    mydict = {}
    mycounter = 1
    conList, returnNumber = randomConcepts_IN(limitInt)
    print(str(returnNumber) + ' Random concepts [anc, desc counts bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + item[1] + " [" + str(item[2]) + ", " + str(item[3]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        mycounter += 1
    for item in conList:
        f.write(item[0] + '\n')
    print()
    f.close()
    return mydict

def highAncesCount(limitInt):
    mydict = {}
    mycounter = 1
    conList, returnNumber = highAncesCount_IN(limitInt)
    print(str(returnNumber) + ' concepts with high ancestor count [number in bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[1], True) + tabString(item[1]) + item[2] + " [" + str(item[0]) + "]" + hasDefString + ancReverse(anyDefs(ancestorsIDFromConId_IN(item[1]), True, cfg("SHOW_DEFS"))))
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
        mycounter += 1
    for item in sorted(conList, key=lambda x: x[2].lower()):
        f.write(item[1] + '\n')
    print()
    f.close()
    return mydict

def highDescCount(limitInt):
    mydict = {}
    mycounter = 1
    conList, returnNumber = highDescCount_IN(limitInt)
    print(str(returnNumber) + ' concepts with high descendant count [number in bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[1], True) + tabString(item[1]) + item[2] + " [" + str(item[0]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
        mycounter += 1
    for item in sorted(conList, key=lambda x: x[2]):
        f.write(item[1] + '\n')
    print()
    f.close()
    return mydict

def highParentCount(limitInt):
    mydict = {}
    mycounter = 1
    conList, returnNumber = highParentCount_IN(limitInt)
    print(str(returnNumber) + ' concepts with high parent count [number in bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[1], True) + tabString(item[1]) + item[2] + " [" + str(item[0]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
        mycounter += 1
    for item in sorted(conList, key=lambda x: x[2].lower()):
        f.write(item[1] + '\n')
    print()
    f.close()
    return mydict

def highChildCount(limitInt):
    mydict = {}
    mycounter = 1
    conList, returnNumber = highChildCount_IN(limitInt)
    print(str(returnNumber) + ' concepts with high child count [number in bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conList:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[1], True) + tabString(item[1]) + item[2] + " [" + str(item[0]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
        mycounter += 1
    for item in sorted(conList, key=lambda x: x[2]):
        f.write(item[1] + '\n')
    print()
    f.close()
    return mydict

def inactivationByReason(limitInt):
    mydict = {}
    mycounter = 1
    writeOutList = []
    summaryListDict, returnNumber = inactivationByReason_IN(limitInt)
    print('Components with their reasons for inactivation (max. ' + str(returnNumber) + ' per category):\n')
    key_list = sorted(list(summaryListDict.keys()), key=lambda x: x[1])
    for key in key_list:
        hasDefString = hasDefInList(key[0], cfg("SHOW_DEFS"))
        print('\033[4m[' + str(mycounter) + ']\t' + activeColor_IN(key[0], True) + tabString(key[0]) + key[1] + '\033[24m' + hasDefString + '\n')
        mydict.update(list(zip(listify(mycounter), listify(tuplify(key[0])))))
        mycounter += 1
        if summaryListDict[key] == []:
            print("No matches.")
        else:
            for item in summaryListDict[key]:
                hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
                print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + item[1] + hasDefString)
                mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
                writeOutList.append(item)
                mycounter += 1
        print()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in sorted(writeOutList, key=lambda x: x[1]):
        f.write(item[0] + '\n')
    f.close()
    print()
    return mydict

def highHistoryCount(limitInt, multiTopBool):
    mydict = {}
    textOutList = []
    mycounter = 1
    preSetToRun = historicalDestinationsCountFromConId_IN(1)
    conList = preSetToRun
    # 1. sort order to prioritise high numbers of single species history rels
    print('\033[4mConcepts with high history target counts [number in bracket]:\033[24m\n')
    conList.sort(key=lambda x: -x[1])
    for item in conList[:limitInt]:
        hasDefString = hasDefInList(item[0], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + pterm(item[0]) + " [" + str(item[1]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        append_distinct(textOutList, item[0])
        mycounter += 1
    # 2. sort order to prioritise varied species history rels
    print('\n\033[4mConcepts with varied history associations [number in bracket]:\033[24m\n')
    conList.sort(key=lambda x: (-x[2], -x[1]))
    for item in conList[:limitInt]:
        hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + pterm(item[0]) + " [" + str(item[1]) + ", " + str(item[2]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        append_distinct(textOutList, item[0])
        mycounter += 1
    if multiTopBool:
    # 3. augment and sort order to prioritise varied TLC historical targets
        TLC = tlc()
        multiTopList = []
        preSetToRun = sorted(preSetToRun, key=lambda x: x[0], reverse=True)
        histList = [item for item in preSetToRun if item[0][-1] == "0"][:limitInt * 100]
        for item in histList:
            TLList = []
            destList = historicalDestinationsFromConId_IN(item[0], 1, 1)
            for TL in TLC:
                for dest in destList:
                    if TCCompare_IN(dest[2], TL[2]) and TL[2] not in TLList:
                        TLList.append(TL[2])
            if len(TLList) > 1 and item not in multiTopList:
                multiTopList.append([*item, len(TLList)])
        if len(multiTopList) > 0:
            print('\n\033[4mConcepts with varied TLC historical targets [number in bracket]:\033[24m\n')
        multiTopList.sort(key=lambda x: -x[3])
        for item in multiTopList[:limitInt]:
            hasDefString = hasDefInList(item[1], cfg("SHOW_DEFS"))
            print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + pterm(item[0]) + " [" + str(item[3]) + "]" + hasDefString)
            mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
            append_distinct(textOutList, item[0])
            mycounter += 1
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in textOutList:
        f.write(item + '\n')
    print()
    f.close()
    return mydict

def highTextDefAncCount(limitInt):
    mydict = {}
    mycounter = 1
    conListNoDef, returnNumber = testDefs_IN(limitInt, False)
    print(str(returnNumber) + ' non-text-def concepts with high numbers of text-def ancestors [number in bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conListNoDef:
        hasDefString = hasDefInList(item[0], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + item[2] + " [" + str(item[1]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        mycounter += 1
    print()
    conListDef, returnNumber = testDefs_IN(limitInt, True)
    localDefCount = conListDef[0][0]
    ancOrLocalDefCount = conListDef[0][1]
    conListDef = conListDef[1:]
    returnNumber -= 1
    print(str(returnNumber) + ' text-def concepts with high numbers of text-def ancestors [number in bracket]:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    for item in conListDef:
        hasDefString = hasDefInList(item[0], cfg("SHOW_DEFS"))
        print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], True) + tabString(item[0]) + item[2] + " [" + str(item[1]) + "]" + hasDefString)
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        mycounter += 1
    totList = [*conListNoDef, *conListDef]
    for item in sorted(totList, key=lambda x: x[2]):
        f.write(item[0] + '\n')
    print()
    print("Number of concepts with local definition: " + localDefCount)
    print("Number of concepts with ancestor or local definition: " + ancOrLocalDefCount)
    print()
    f.close()
    return mydict

def interestingOWL(limitInt):
    mydict = {}
    mycounter = 1
    OWLDict = interestingOWL_IN()
    OWLKeyList = sorted(list(OWLDict.keys()))
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
    print('Atypical OWL axioms:\n')
    for key in OWLKeyList:
        if len(OWLDict[key][0]) == 2:
            includeCount = True
        else:
            includeCount = False
        for item in OWLDict[key]:
            if includeCount:
                item.append(pterm(item[0]))
            else:
                item.append("")
                item.append(pterm(item[0]))
        if includeCount:
            workingList = sorted(OWLDict[key], key=lambda x: (-x[1], x[2]))
        else:
            workingList = sorted(OWLDict[key], key=lambda x: (x[2]))
        if len(OWLDict[key]) > limitInt:
            moreString = " (more available)"
        else:
            moreString = ""
        print('\033[4m' + key + moreString + ':\033[24m\n')
        innercounter = 1
        for item in workingList:
            hasDefString = hasDefInList(item[0], cfg("SHOW_DEFS"))
            if includeCount:
                countString = " [" + str(item[1]) + "]"
            else:
                countString = ""
            if innercounter <= limitInt:
                print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], False) + tabString(item[0]) + item[2] + countString + hasDefString)
                mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
                f.write(item[0] + '\n')
                mycounter += 1
            innercounter += 1

        print()
    f.close()
    return mydict

def roleTargetsFromConId(idIn):
    mydict = {}
    mycounter = 1
    findid = 0
    roleTargetList = []
    roleTargetList = [[item, pterm(item)] for item in list(set(growValues(idIn, True)) - set([idIn]))]
    roleTargetList.sort(key=lambda x: x[1].lower())
    if len(roleTargetList) > 0:
        print('Role targets: [' + str(len(roleTargetList)) + '] for', idIn, pterm(idIn))
        print()
        for roleTarget in roleTargetList:
            if not roleTarget is None:
                hasValDefInListString = hasDefInList(roleTarget[0], cfg("SHOW_DEFS"))
                # CONC - relax level of concept model attributes
                if TCCompare_IN(roleTarget[0], tok["CONMOD_ATTRIB"]):
                    print('[' + str(mycounter) + ']\t' + attribColor_IN(roleTarget[0]) + tabString(roleTarget[0]) + roleTarget[1] + hasValDefInListString + anyDefs(list(set(growValues(roleTarget[0], True)) - (set([roleTarget[0]]))), (False if hasValDefInListString == "" else True), cfg("SHOW_DEFS")))
                else:
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(roleTarget[0], True) + tabString(roleTarget[0]) + roleTarget[1] + hasValDefInListString + anyDefs(list(set(growValues(roleTarget[0], True)) - (set([roleTarget[0]]))), (False if hasValDefInListString == "" else True), cfg("SHOW_DEFS")))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(roleTarget[0])))))
                if roleTarget[0] == idIn:
                    findid = mycounter
                mycounter += 1
        f2 = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
        for roleTarget in roleTargetList:
            f2.write(roleTarget[0] + '\n')
        f2.close()
        print()
        if findid == 0:
            findid = mycounter
            mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
        print('[' + str(findid) + '] to return to', idIn, pterm(idIn))
        print()
    return mydict

def testPPS(idIn):
    #PPS finder
    mydict = {}
    mycounter = 1
    # ancestors
    ancestorList = []
    ancestorList = ancestorsFromConId_IN(idIn)
    #PPSs
    PPSList = []
    PPSList = PPSFromAncestors_IN(ancestorList)  # call interface neutral code
    findid = 0
    if len(PPSList) > 0:
        print('Prox prim supertypes: [' + str(len(PPSList)) + '] for', idIn, pterm(idIn))
        print()
        for PPS in PPSList:
            if not PPS is None:
                print('[' + str(mycounter) + ']\t' + PPS[0] + tabString(PPS[0]) + PPS[1])
                mydict.update(list(zip(listify(mycounter), listify(tuplify(PPS[0])))))
                if PPS[0] == idIn:
                    findid = mycounter
                mycounter += 1
        print()
        if findid == 0:
            findid = mycounter
            mydict.update(list(zip(listify(findid), listify(tuplify(idIn)))))
        print('[' + str(findid) + '] to return to', idIn, pterm(idIn))
        print()
    return mydict


def descendantsFromConId(idIn, onlyDefsBool):
    # need preliminary code to check for descendant count
    descCount = descendantCountFromConId_IN(idIn)
    if descCount <= 200 or (descCount <= 20000 and onlyDefsBool):
        secondaction = 'y'
    else:
        print(idIn, pterm(idIn), 'has', str(descCount), 'descendants.')
        secondaction = input2_3("Continue [Y/N]:")
    mydict = {}
    mycounter = 1
    findid = 0
    if secondaction[0] == 'y':
        # descendants
        descendantList = []
        definitionList = []
        descendantList = descendantsFromConId_IN(idIn)  # call interface neutral code
        if len(descendantList) > 0:
            if onlyDefsBool:
                descendantString = ' (with definitions only)'
            else:
                descendantString = ''
            if cfg("SHOW_DEFS") < 4:
                titleTerm = pterm(idIn)
            else:
                titleTerm = fsnFromConId_IN(idIn)[1]
            print('Descendants' + descendantString + ': [' + str(len(descendantList)) + '] for', idIn, titleTerm)
            print()
            for descendant in descendantList:
                if not descendant is None:
                    hasDefString = hasDefInList(descendant[0], cfg("SHOW_DEFS"))
                    if (hasDefString != "" and onlyDefsBool) or onlyDefsBool is False:
                        if hasDefString != "":
                            definitionList.append(descendant)
                        # CONC - relax level of concept model attributes
                        if TCCompare_IN(idIn, tok["CONMOD_ATTRIB"]):
                            print('[' + str(mycounter) + ']\t' + attribColor_IN(descendant[0]) + tabString(descendant[0]) + descendant[1] + hasDefString)
                        else:
                            print('[' + str(mycounter) + ']\t' + activeColor_IN(descendant[0], True) + tabString(descendant[0]) + descendant[1] + hasDefString)
                        mydict.update(list(zip(listify(mycounter), listify(tuplify(descendant[0])))))
                        if descendant[0] == idIn:
                            findid = mycounter
                        mycounter += 1
            print()
            if findid == 0:
                findid = mycounter
                mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
            print('[' + str(findid) + '] to return to', idIn, pterm(idIn))
            print()
            f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'testset.txt', 'w', encoding="UTF-8")
            f2 = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
            descendantList.sort(key=lambda x: len(x[1]))
            descLenMult = 0
            if len(secondaction) > 1: # easter egg to limit descendant terms tested in wiki code ('yy' = terms up to 30 char, 'yyy' = 35, 'yyyy'=40...)
                descLenMult = 5 * (len(secondaction) - 1)
                descendantList.sort(key=lambda x: len(x[1]))
                descLen = [len(x[1]) for x in descendantList].index(26 + descLenMult)
                for descendant in descendantList[:descLen]:
                    f.write(descendant[0] + "\t" + descendant[1] + '\n')
                    f2.write(descendant[0] + '\n')
            else:
                if onlyDefsBool:
                    for descendant in definitionList:
                        f.write(descendant[0] + "\t" + pterm(descendant[0]) + '\n')
                        f2.write(descendant[0] + '\n')
                else:
                    for descendant in descendantList:
                        f.write(descendant[0] + "\t" + pterm(descendant[0]) + '\n')
                        f2.write(descendant[0] + '\n')
            f.close()
            f2.close()
    else:
        clearScreen()
        snapshotHeader()
        mydict = readInFocus()
    return mydict

def focusSwapWrite(inString):
    if  inString[-2] == '1':
        inString = CiDFromDiD_IN(tuplify(inString))
    #print 'focusSwapWrite', inString
    if len(inString) > 5:
        try:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'focusSwap.txt', 'w', encoding="UTF-8")
            f.write(inString)
        finally:
            f.close()
        # for wiki test of single code
        try:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'testset.txt', 'w', encoding="UTF-8")
            f.write(inString + "\t" + pterm(inString))
        finally:
            f.close()
        writeRoleAndDotFiles(inString)

def writeRoleAndDotFiles(inString):
    if  inString[-2] == '1':
        inString = CiDFromDiD_IN(tuplify(inString))
    # for roletesting
    PPSString = ""
    roleString = ""
    parentString = ""
    selfString = ""
    # roleSuper = 2 # opportunity to change the 'focus' of the role expression
    roleSuper = cfg("ROLE_SUPER")
    if inString == tok['ROOT']:
        roleSuper = 2
    if not inString == tok['ROOT']:
        # selfString
        selfString = "<< " + inString + " | " + pterm(inString) + " |"
        #PPSs
        ancestorList = []
        ancestorList = ancestorsFromConId_IN(inString, True)
        parentList = parentsFromId_IN(tuplify(inString), True)
        PPSList = []
        PPSList = PPSFromAncestors_IN(ancestorList)
        for PPS in PPSList:
            PPSString += "<< " + PPS[0] + " | " + PPS[1] + " | +\n"
        for parent in parentList:
            parentString += "<< " + parent[1] + " | " + parent[0] + " | +\n"
        #roles
        roleList = []
        roleList = groupedDefiningRolesFromConId_IN(inString)
        for roles in roleList:
            if roles[0] > 0:
                roleString += "{"
                for role in roles[1:]:
                    # CONC - write value to role.txt and pre_dot.txt files
                    if role[1] in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
                        roleString += "" + role[1] + " | " +  role[2] + " | = " + role[3] + ",\n"
                    else:
                        roleString += "" + role[1] + " | " +  role[2] + " | = << " + role[3] + " | " +  role[4] + " |,\n"
                roleString = roleString[:-2] + "},\n"
        for roles in roleList:
            if roles[0] == 0:
                for role in roles[1:]:
                    # CONC - write value to role.txt and pre_dot.txt files
                    if role[1] in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
                        roleString += "" + role[1] + " | " +  role[2] + " | = " + role[3] + ",\n"
                    else:
                        roleString += "" + role[1] + " | " +  role[2] + " | = << " + role[3] + " | " +  role[4] + " |,\n"
    else:
        PPSString += tok['ROOT'] + " | " + pterm(tok['ROOT']) + " | + "
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role.txt', 'w', encoding="UTF-8")
        if roleSuper == 0:# self as supertype
            if roleString == "":
                f.write(selfString)
            else:
                f.write(selfString + ":\n")
                f.write(roleString[:-2])
        elif roleSuper == 2: # calculated PPS as supertypes (? probs with GCIs)
            if roleString == "":
                f.write(PPSString[:-2])
            else:
                f.write(PPSString[:-2] + ":\n")
                f.write(roleString[:-2])
        else: # DNF parents as supertypes
            if roleString == "":
                f.write(parentString[:-2])
            else:
                f.write(parentString[:-2] + ":\n")
                f.write(roleString[:-2])
    finally:
        f.close()
    # if drawing picture
    # if cfg("DRAW_DIAG") != 0:
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'pre_dot.txt', 'w', encoding="UTF-8")
        # focus
        f.write(inString + " | " + pterm(inString) + " |\n")
        # operator
        conMeta = []
        conMeta = conMetaFromConId_IN(inString, False)
        if not inString == tok['ROOT']:
            if len(conMeta) > 0:
                if conMeta[3] == tok["DEFINED"]:
                    f.write("=== ")
                else:
                    f.write("<<< ")
            else:
                f.write("<<< ")
        # parents
        if roleString == "":
            f.write(parentString[:-2].replace("<< ", ""))
        # parents and roles
        else:
            f.write(parentString[:-2].replace("<< ", "") + ":\n")
            f.write(roleString[:-2].replace("<< ", ""))
    finally:
        f.close()

def resetRoleAndPreDot(firstTimeBool):
    drawPics(3)
    drawBrowseId([tok["ROOT"]], {}, True)
    dotDrawFocus("", [tok["ROOT"]], ">")
    if not firstTimeBool:
        drawPics(12)
        drawBrowseId([tok["ROOT"]], {}, True)
    focusSwapWrite(tok["ROOT"])
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role.txt', 'w', encoding="UTF-8")
        f.write(tok['ROOT'] + " | " + pterm(tok['ROOT']) + " |")
    finally:
        f.close()
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'pre_dot.txt', 'w', encoding="UTF-8")
        f.write(tok['ROOT'] + " | " + pterm(tok['ROOT']) + " |")
    finally:
        f.close()
    drawPics(0, True)

def snapHeaderWrite(inId):
    tempDict = {}
    syns = synsFromConId_IN(inId)
    LAN = "EN"
    for syn in syns:
        if syn[1][syn[1].find("000001") + 7:] == "UK drug extension":
            tempDict.update({3:syn[1][0:15] + " UK Drug"})
            LAN = "GB"
        if syn[1][syn[1].find("000001") + 7:] == "UK clinical extension":
            tempDict.update({2:syn[1][0:15] + " UK Clin, "})
            LAN = "GB"
        if syn[1][0:31] == "SNOMED Clinical Terms version: ":
            tempDict.update({1:syn[1][31:39] + " Int, "})
        # import addn
        if syn[1][22:40] == "Edición en Español":
            tempDict.update({4:syn[1][22:57]})
            LAN = "ES"
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'snapHeader.txt', 'w', encoding="UTF-8")
        if LAN == "GB":
            if len(tempDict) == 2:
                f.write(tempDict[1] + tempDict[2][:-2] + 'ical extension')
            elif len(tempDict) == 3:
                f.write(tempDict[1] + tempDict[2] + tempDict[3])
        elif LAN == "EN" and len(tempDict) == 1:
            f.write(tempDict[1][:-2] + "ernational data")
        # import addn
        elif LAN == "ES" and len(tempDict) == 1:
            f.write("SCT " + tempDict[4])
        else:
            f.write("")
    finally:
        f.close()

def snapshotHeader():
    print('\033[7mSnapshot data: ' + snapHeaderRead() + '\033[27m \n')

def snapshotMRCMHeader():
    print('\033[91;7mSnapshot data: ' + snapHeaderRead() + '\033[92;27m \n')

def snapshotRoleHeader():
    print('\033[95;7mSnapshot data: ' + snapHeaderRead() + '\033[92;27m \n')

def getIdsFromLines(inLines, returnFalseBool): # *SCT*
    if isinstance(inLines, str):
        inLines = [inLines]
    truePos = []
    falsePos = []
    for preLine in inLines:
        line = preLine.strip().replace('"', "").replace("'", "")
        if len(line) > 5:
            line = "".join([re.sub('[*/:;=,.<>^{}| ()[\]]', '\t', s) for s in line]) # replace pipes, spaces and brackets etc. with tabs (for codes in most text)
            if "\t" in line: # should catch most
                tempList = [re.sub(r"[^0-9]", r"", item) for item in line.split("\t") if len(re.sub(r"[^0-9]", r"", item)) > 5] # if tabs in original or after above substitutions
                for item in tempList:
                    # SCTID SEARCH
                    if re.search(r"([0-9]{3,15})(00|01|02|10|11|12)([0-9])", item, re.IGNORECASE): # Find ID
                        if validateVerhoeff(int(item)) and (len(item) < 21):
                            truePos.append(item.strip())
                        else:
                            falsePos.append(item.strip())
            elif len(list(set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ* :=,{}|') & set(line))) > 0: # catch absolute jumble (no scg/ecl, no spaces)
                match = []
                pattern = re.compile(r'([0-9]{3,15})(00|01|02|10|11|12)([0-9])', re.IGNORECASE)
                match = pattern.findall(line)
                for item in match:
                    if validateVerhoeff(int(item[0] + item[1] + item[2])):
                        truePos.append(item[0] + item[1] + item[2])
                    else:
                        falsePos.append(item[0] + item[1] + item[2])
            else:
                if re.search(r"([0-9]{3,15})(00|01|02|10|11|12)([0-9])", line, re.IGNORECASE): # simple list of ids, nothing else.
                    if validateVerhoeff(int(line.strip())):
                        truePos.append(line.strip())
                    else:
                        falsePos.append(line.strip())
    if returnFalseBool:
        return truePos, falsePos
    else:
        return truePos

def parsablePretty(inString):
    # break templates on cardinality
    outString = re.sub(r"(\[\[[0|1|*]\.\.[0|1|*]\]\] )", r"\n\t\1", inString)
    # break lists of concept constraints at OR clauses
    outString = re.sub(r"(OR [<<|<|\d])", r"\n\t\t\1", outString)
    # break lists of concept constraints at cardinality
    outString = re.sub(r"(\[[0|1|*]..[0|1|*]\] )", r"\n\t\1", outString)
    codeList = []
    match = []
    p = re.compile(r'([0-9]{3,15})(00|01|02|10|11|12)([0-9])', re.IGNORECASE)
    match = p.findall(outString)
    for item in match:
        if validateVerhoeff(int(item[0] + item[1] + item[2])):
            codeList.append(item[0] + item[1] + item[2])
    # colorise ids
    for item in codeList:
        outString = outString.replace(item, "\033[36m" + item + "\033[92m")
    # colorise terms (pipes must be paired)
    if outString.count("|") % 2 == 0:
        returnString = ""
        outStringList = []
        outStringList = outString.split("|")
        totalCounter = 1
        counter = 1
        for row in outStringList:
            returnString += row
            if not totalCounter == len(outStringList):
                if counter == 1:
                    # if len(getIdsFromLines(outStringList[totalCounter - 1], False)) > 0:
                    tempIdList = getIdsFromLines(outStringList[totalCounter - 1].replace("36m", "").replace("92m", ""), False)
                    if len(tempIdList) > 0:
                        tempId = tempIdList[0]
                    else:
                        tempId = ""
                    if hasDef(tempId) and (cfg("PRETTY") > 2):
                        if TCCompare_IN(tempId, tok["ATTRIB"]):
                            returnString += "|\033[33m"
                        else:
                            returnString += "|\033[31m"
                    else:
                        returnString += "|\033[95m"
                    counter = 2
                else:
                    returnString += "\033[92m|"
                    counter = 1
            totalCounter += 1
        return returnString
    else:
        return outString

def parsablePrettyOwl(inString):
    tempList = []
    startFindList = ['SubClassOf(:', \
                'EquivalentClasses(:', \
                'SubObjectPropertyOf(:', \
                'SubClassOf(ObjectIntersectionOf(:', \
                'TransitiveObjectProperty(:', \
                'ReflexiveObjectProperty(:', \
                'SubObjectPropertyOf(ObjectPropertyChain(:', \
                'ObjectIntersectionOf(:', \
                'ObjectPropertyChain(:']
    startKeepList = ['SubClassOf(', \
                'EquivalentClasses(', \
                'SubObjectPropertyOf(', \
                'SubClassOf(ObjectIntersectionOf(', \
                'TransitiveObjectProperty(', \
                'ReflexiveObjectProperty(', \
                'SubObjectPropertyOf(ObjectPropertyChain(', \
                'ObjectIntersectionOf(', \
                'ObjectPropertyChain(']
    outList = []
    codeList = []
    ptermDict = {}
    outString = inString
    outString = outString.replace("))) :", ")))\n:")
    outString = outString.replace("ObjectIntersectionOf(ObjectSomeValuesFrom", "\nObjectIntersectionOf(\nObjectSomeValuesFrom")
    outString = outString.replace(" ObjectIntersectionOf", "\nObjectIntersectionOf")
    outString = outString.replace(" ObjectSomeValuesFrom(:" + tok["RG"] , "\n ObjectSomeValuesFrom(:" + tok["RG"])
    outString = outString.replace(" ObjectSomeValuesFrom", "\nObjectSomeValuesFrom")
    outString = outString.replace(" DataHasValue", "\nDataHasValue")
    outString = outString.replace(")))", ")))\n")
    outString = outString.replace("\n\n\n", "\n\n")
    match = []
    p = re.compile(r'(:)([0-9]{3,15})(00|01|02|10|11|12)([0-9])', re.IGNORECASE)
    match = p.findall(outString)
    for item in match:
        if validateVerhoeff(int(item[1] + item[2] + item[3])):
            codeList.append(item[1] + item[2] + item[3])
    for item in codeList:
        if not item in ptermDict:
            if cfg("PRETTY") > 1:
                termColorString = "\033[95m"
                if hasDef(item) and (cfg("PRETTY") > 2):
                    if TCCompare_IN(item, tok["ATTRIB"]):
                        termColorString = "\033[33m"
                    else:
                        termColorString = "\033[31m"
                ptermDict[item] = "\033[36m" + item + "\033[92m|" + termColorString + pterm(item) + "\033[92m|"
            else:
                ptermDict[item] = "\033[36m" + item + "\033[92m"
    tempList = outString.split("\n")
    tempString = ""
    for item2 in tempList:
        tempString = item2
        outList.append(tempString)
    returnString = ""
    for item in outList:
        startFMatch = False
        for startF in startFindList:
            if startF in item:
                startFMatch = True
                break
        if startFMatch:
            item2List = []
            item2List = item.split(":")
            for item2 in item2List:
                startKMatch = False
                for startK in startKeepList:
                    if startK in item2:
                        startKMatch = True
                        break
                if startKMatch:
                    returnString += "\t\t\t" + item2 + "\n"
                else:
                    returnString += "\t\t\t:" + item2 + "\n"
        else:
            returnString += "\t\t\t" + item + "\n"
    for item1 in ptermDict:
        returnString = returnString.replace(item1, ptermDict[item1])
    return returnString[3:]

def setPretty(inInteger):
    cfgWrite("PRETTY", inInteger)
    return inInteger

def drawPics(inInteger, shutDownBool=False):
    if inInteger in ("A", "S", "O"):
        resetDiag(inInteger)
    else:
        if inInteger == 0 and shutDownBool:
            clearScreen()
            cfgWrite("DRAW_DIAG", 3)
            drawBrowseId(tok["ROOT"], {}, False)
            clearScreen()
        cfgWrite("DRAW_DIAG", inInteger)
        return inInteger

def mergeVals(inInteger):
    cfgWrite("MERGE_VALS", inInteger)
    return inInteger

def getVersion():
    versionString = snapHeaderRead().split(" ")[1]
    return versionString

def toTempList(inTempList, inNotes, inCounter, inIdent, inGroup, inList, inLen):
    inTempList.append(inNotes)
    inTempList.append(inCounter)
    inTempList.append(inIdent)
    inTempList.append(inGroup)
    inTempList.append(inList)
    inTempList.append(inLen)
    return inTempList

def clean(inString):
    outString = inString.strip()
    outString = outString.replace("{", "")
    outString = outString.replace("}", "")
    outString = outString.replace("= ", "") # shortens ===
    outString = outString.replace("==", "") # removes last bit
    outString = outString.replace("<<< ", "") # removes other operator
    return outString

def isTag(inId):
    if inId in tagList:
        return True
    else:
        return False

def splitTag(inId):
    fsn = fsnFromConId_IN(inId)[1]
    insertPoint = fsn.rfind("(")
    tag = fsn[insertPoint:][1:-1]
    return tag

def isTagDonor(inId, focusId):
    fsnTag1 = splitTag(inId)
    fsnTag2 = splitTag(focusId)
    if fsnTag1 == fsnTag2:
        return True
    else:
        return False

def fetchTag(inId):
    returnList = []
    for tagId in tagList:
        if TCCompare_IN(inId, tagId) and tagId != tok["ROOT"]:
            if isTagDonor(tagId, inId):
                returnList.append([tagId, 1])
            else:
                returnList.append([tagId, 2])
    return returnList

def compareOnTag(inId):
    tagDict = {}
    # fill comparison dictionary
    for tagId in tagList:
        fsnTag1 = splitTag(tagId)
        tagDict[fsnTag1] = tagId
    # add in product too
    fsn1 = fsnFromConId_IN(tok["PHARM_PROD"])[1]
    insertPoint1 = fsn1.rfind("(")
    fsnTag1 = fsn1[insertPoint1:][1:-1]
    tagDict[fsnTag1] = tok["PHARM_PROD"]
    # generate incoming fsnTag
    fsnTag2 = splitTag(inId)
    # compare
    if fsnTag2 in tagDict:
        return tagDict[fsnTag2]
    else:
        return inId

def readInY():
    returnList = []
    try:
        f = io.open(cfgPathInstall() + 'sets' + sep() + 'Y.txt', 'r', encoding="UTF-8")
        returnList = [item.strip() for item in f.readlines()]
    finally:
        f.close()
    return returnList

def readInZ():
    returnList = []
    try:
        f = io.open(cfgPathInstall() + 'sets' + sep() + 'Z.txt', 'r', encoding="UTF-8")
        returnList = [item.strip() for item in f.readlines()]
    finally:
        f.close()
    return returnList

def scaleFontSize(inId, fillOrFont):
    filePath = pth('BASIC_DATA') + 'rankData.txt'
    rankLines = []
    try:
        f = io.open(filePath, 'r', encoding="UTF-8")
        try:
            rankLines = f.readlines()
        finally:
            f.close()
    except IOError:
        pass
    testDict = {}
    usageColumn = 1
    if len(rankLines) > 1:
        if len(rankLines[0].split()) == 1:
            usageColumn = int(rankLines[0][0])
        for line in rankLines[1:]:
            tempList = line.strip().split("\t")
            testDict[tempList[0]] = tempList[usageColumn]
        if inId in testDict:
            if testDict[inId] != '1':
                fillOrFont = ", fontsize=" + str(int((math.sqrt(int(testDict[inId])) / 30) + 9)) + fillOrFont
                testList = readInY()
                if inId in testList:
                    fillOrFont = ", fontcolor=red" + fillOrFont
            else:
                fillOrFont = ", fontcolor=darkgreen" + fillOrFont

    return fillOrFont

def checkObjValueColor(inId, checkPPS, PPSList, focusId="", historyNodeBool=False):
    returnString1 = ""
    returnString2 = ""
    if cfg("SHOW_DEFS") > 0:
        if hasDef(inId):
            fillOrFont = ", fontcolor=blue, fillcolor="
            if isTag(inId) and cfg("SHOW_DEFS") > 4: # overrides simple hasDef+
                if focusId != "" and isTagDonor(inId, focusId):
                    fillOrFont = ", fontcolor=darkgreen, fillcolor=" # is source tag insertion point of focus
                else:
                    fillOrFont = ", fontcolor=magenta, fillcolor=" # any tag insertion point
        elif isTag(inId) and cfg("SHOW_DEFS") > 4:
            if focusId != "" and isTagDonor(inId, focusId):
                fillOrFont = ", fontcolor=darkgreen, fillcolor=" # is source tag insertion point of focus
            else:
                fillOrFont = ", fontcolor=purple, fillcolor=" # any tag insertion point
        else:
            fillOrFont = ", fillcolor="
    else:
        fillOrFont = ", fillcolor="
    if cfg("DEMO") == 3:
        fillOrFont = scaleFontSize(inId, fillOrFont)
    elif cfg("DEMO") == 2:
        testList = readInY()
        if inId in testList:
            fillOrFont = ", fontcolor=red" + fillOrFont
    # CONC - set diagram concrete class (green box)
    if inId[0] == "#" or inId[0].isalpha():
        returnString1 = "shape=box, peripheries=1, style=\"diagonals,filled\"" + fillOrFont
        returnString2 = "palegreen"
        return returnString1 + returnString2
    else:
        conMetaList = conMetaFromConId_IN(inId, False)
        checkString1 = conMetaList[1]
        checkString3 = conMetaList[3]
        if checkString1 == 1:
            if checkString3 == tok["PRIMITIVE"]:
                if (checkPPS) and (not inId == tok["ROOT"]) and (cfg("MERGE_VALS") > 2)  and (cfg("MERGE_VALS") < 6):
                    if inId in PPSList:
                        returnString1 = "shape=box, peripheries=1, style=filled" + fillOrFont
                        returnString2 = "\"#99FFFF\""
                    else:
                        returnString1 = "shape=box, peripheries=1, style=filled" + fillOrFont
                        if historyNodeBool:
                            returnString2 = "\"#D1E8FF\""
                        else:
                            returnString2 = "\"#99CCFF\""
                else:
                    returnString1 = "shape=box, peripheries=1, style=filled" + fillOrFont
                    if historyNodeBool:
                        returnString2 = "\"#D1E8FF\""
                    else:
                        returnString2 = "\"#99CCFF\""
            else:
                returnString1 = "shape=box, peripheries=2, style=filled" + fillOrFont
                if historyNodeBool:
                    returnString2 = "\"#EBEBFF\""
                else:
                    returnString2 = "\"#CCCCFF\""
            returnString2 = checkColorStringForNav(inId, returnString2)
        else:
            returnString1 = "shape=box, peripheries=1, style=filled" + fillOrFont
            returnString2 = "palegreen"
        return returnString1 + returnString2

def append_distinct(inList, inItem): # *SCT*
    if list(inItem) not in [list(item) for item in inList]:
        inList.append(inItem)
    return inList

def dotDrawFocus(inId, inSet, inBTDir): # either takes a set (from SCTRF2Role) or an id (from SCTRF2Funcs)
    moreThanOne = True
    inLines = []
    groupCounter = 0
    BN0_DN_Counter = 0
    tempLine = ""
    if (cfg("SHOW_SUPERS") == 0) or (cfg("SHOW_SUPERS") == 1):
        splineText = " splines=ortho;"
    else:
        splineText = ""
    if len(inSet) == 0: # if it's a single call from the snap code then use angled edges, otherwise these will probably fail to render.
        inSet.append(inId)
        moreThanOne = False

    groupNodes = []
    attNameList = []
    objValueList = []
    bunchNodes = []
    dotEdgeList = []
    invisEdgeList = []
    domAdjList = []
    allParents = []
    parentCounter = 0
    spineRankSameString = "{rank=same; "
    groupRankSameString = "{rank=same; "
    parentAndAttRankSameString = "{rank=same; "
    parentAndValueRankSameString = "{rank=same; "

    if cfg("DRAW_DIAG") == 4 and cfg("ADD_INVIS") == 1:
        objValueList.append("\"" + tok["ROOT"] + "_1\" [shape=point, label=\"\", shape=circle, style=invis];")

    for setMember in inSet:
        outLines = []
        inGroup = False
        lineCounter = 0
        writeRoleAndDotFiles(setMember)
        try:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'pre_dot.txt', 'r', encoding="UTF-8")
            # focus
            inLines = f.readlines()
        finally:
            f.close()

        allLinesBool = True
        for line in inLines:
            tempList = []
            if "{" in line:
                inGroup = True
                groupCounter += 1
            if not inGroup:
                if lineCounter == 0:
                    tempList = toTempList(tempList, tempLine, str(lineCounter), "FOCUS", str(groupCounter), [item.strip() for item in line.strip().split("|")], len(line.strip().split("|")))
                else:
                    if "<<<" in line:
                        tempLine = "PRIM"
                    elif "===" in line:
                        tempLine = "FD"
                    else:
                        tempLine = "NTC"
            if lineCounter != 0:
                # CONC - handle #number rows into diagram build (treat as 5 elements)
                initialTempList = [clean(item) for item in line.strip().split("|")]
                if initialTempList[0] in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
                    initialTempList[2] = initialTempList[2].replace(",", "")
                    initialTempList.append("")
                    initialTempList.append("")
                    tempList = toTempList(tempList, tempLine, str(lineCounter), inGroup, str(groupCounter), initialTempList, 5)
                else:
                    tempList = toTempList(tempList, tempLine, str(lineCounter), inGroup, str(groupCounter), [clean(item) for item in line.strip().split("|")], len(line.strip().split("|")))
            if "}" in line:
                inGroup = False
            lineCounter += 1
            tempLine = ""
            if tempList[2] == 'FOCUS': # if a focus concept
                outLines.append(tempList)
            elif (cfg("SHOW_SUPERS") == 0 and tempList[4][0] in inSet) or (cfg("SHOW_SUPERS") == 2 and tempList[4][0] in inSet): # even if showsupertypes=0 or 2, add any parent row
                outLines.append(tempList)
                append_distinct(domAdjList, (setMember, tempList[4][0]))
            elif (tempList[4][0] in limitAttributes(True)) or ("100000110" in tempList[4][0]): # if a matched attribute row, or if a pharmacy attribute!
                outLines.append(tempList)
            elif (cfg("SHOW_SUPERS") == 1 and tempList[5] == 3) or (cfg("SHOW_SUPERS") == 3 and tempList[5] == 3): # if show supertypes=1 and it's a supertype row
                outLines.append(tempList)
            else:
                allLinesBool = False

        # CONC - remove concrete double quotes for dot file
        for line in outLines:
            tempString = line[4][2].replace("\"", "")
            line[4][2] = tempString

        # if P = 2 or 3, use FSNs everywhere
        if cfg("GRAPH_ANGLE") in (2, 3):
            for line in outLines:
                if "#" not in line[4][0]:
                    if validateVerhoeff(line[4][0]):
                        line[4][1] = fsnFromConId_IN(line[4][0])[1]
                if line[-1] == 5:
                    if "#" not in line[4][2]:
                        if validateVerhoeff(line[4][2]):
                            line[4][3] = fsnFromConId_IN(line[4][2])[1]
        else:
            for line in outLines:
                if line[-1] == 5:
                    if "#" not in line[4][2]:
                        if isTag(line[4][2]) and cfg("SHOW_DEFS") > 4: # if a role value is also a tag carrier, use FSN O settings appropriate
                            line[4][3] = fsnFromConId_IN(line[4][2])[1]

        focusList = []
        definedStatus = False
        parentList = []
        soleRoleList = []
        groupedRoleList = []
        # definition level bunch node (includes debug info about diagram type)
        BN0label = "BN0" + str(BN0_DN_Counter) + "\\nA" + str(cfg("DRAW_DIAG")) + " B" + str(cfg("MERGE_VALS")) + " C" + str(cfg("SHOW_SUPERS")) + "\\nO" + str(cfg("SHOW_DEFS")) + " P" + str(cfg("GRAPH_ANGLE")) + " S" + str(cfg("ACROSS_DOWN"))
        # definition node
        DNLabel = "DN" + str(BN0_DN_Counter)
        bunchNodes.append("\"" + BN0label + "\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black];")
        spineRankSameString += "\"" + BN0label + "\" "

        for line in outLines:
            # focus
            if line[2] == "FOCUS":
                focusList.append(line)
                conMeta = []
                conMeta = conMetaFromConId_IN(line[4][0], False)
                if len(conMeta) > 0:
                    if conMeta[3] == tok["DEFINED"]:
                        definedStatus = True
            elif not line[2] and line[0] == "FD":
                definedStatus = True
                parentList.append(line)
            elif not line[2] and line[0] == "PRIM":
                parentList.append(line)
            elif int(line[3]) > 0 and not line[0] == "NTC":
                groupedRoleList.append(line)
                if not line[3] in groupNodes:
                    if not "\"GN" + line[3] + "\" [shape=circle, fontsize=1, width=0.5, label=\"\" style=filled, fillcolor=white];" in groupNodes:
                        groupNodes.append("\"GN" + line[3] + "\" [shape=circle, fontsize=1, width=0.5, label=\"\" style=filled, fillcolor=white];")
                    if not "\"BN" + line[3] + "\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black];" in bunchNodes:
                        bunchNodes.append("\"BN" + line[3] + "\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black];")
                        groupRankSameString += "\"" + "BN" + line[3] + "\" "
            elif line[0] == "NTC" and line[5] == 5:
                soleRoleList.append(line)
            elif line[0] == "NTC" and line[5] == 3:
                parentList.append(line)

        ancestorList = ancestorsFromConId_IN(setMember, True)
        PPSList = [PPS[0] for PPS in PPSFromAncestors_IN(ancestorList)]

        parentTagAndDefCestorList = []
        defCestorList = []
        tagAndDefCestorList = []
        tagCestorList = []
        if cfg("SHOW_DEFS") in (7, 8, 9, 10, 11, 12) and cfg("SHOW_SUPERS") in (1, 3):
            parentIdList = [parent[4][0] for parent in parentList]
            if cfg("SHOW_DEFS") in (8, 10, 12): # show all tags
                tagCestorList = [tagId[0] for tagId in fetchTag(setMember)]
            elif cfg("SHOW_DEFS") in (7, 9, 11): # if 9, 11 show only donor
                tagCestorList = [tagId[0] for tagId in fetchTag(setMember) if tagId[1] == 1]
            if cfg("SHOW_DEFS") in (11, 12):
                defCestorList = [anc[0] for anc in ancestorList if hasDef(anc[0])]
            tagAndDefCestorList = [*tagCestorList, *defCestorList]
            parentTagAndDefCestorList = [*parentIdList, *tagCestorList, *defCestorList]
            for item in [tagOrDef for tagOrDef in tagAndDefCestorList if tagOrDef not in inSet]:
                if hasDef(item):
                    if item in tagCestorList:
                        ptermLen = len(fsnFromConId_IN(item)[1])
                    else:
                        ptermLen = len(pterm(item))
                    defString = "\l\l" + toString(textwrap.wrap(simplify(onlyDefTermsFromConId_IN(item)[1].replace(' (*)', '')), (50 if ptermLen < 50 else ptermLen))) + "\\n\", "
                else:
                    defString = "\l\", "
                if item in tagCestorList or cfg("GRAPH_ANGLE") in (2, 3):
                    append_distinct(objValueList, "\"" + item + "\" [label=\"" + item + "\l" + simplify(fsnFromConId_IN(item)[1]) + defString + checkObjValueColor(item, True, PPSList, setMember) + "];")
                else:
                    append_distinct(objValueList, "\"" + item + "\" [label=\"" + item + "\l" + simplify(pterm(item)) + defString + checkObjValueColor(item, True, PPSList, setMember) + "];")
            for item in [tagAndDef for tagAndDef in tagAndDefCestorList if tagAndDef in inSet]:
                if cfg("GRAPH_ANGLE") in (2, 3):
                    append_distinct(objValueList, "\"" + item + "\" [label=\"" + item + "\l" + simplify(fsnFromConId_IN(item)[1]) + findDefString([item], 5, True) + checkObjValueColor(item, True, PPSList, setMember) + "];")
                else:
                    append_distinct(objValueList, "\"" + item + "\" [label=\"" + item + "\l" + simplify(pterm(item)) + findDefString([item], 5, True) + checkObjValueColor(item, True, PPSList, setMember) + "];")
            for item in parentList:
                if item[4][0] in tagCestorList:
                    item[4][1] = fsnFromConId_IN(item[4][0])[1]
            tagAdjacencyList = adjacencyBuild(setToGraph(parentTagAndDefCestorList, False, []), False)
            for item in tagAdjacencyList:
                if (item[0] != setMember) and (item[1] != tok["ROOT"]):
                    if (item[0] in parentTagAndDefCestorList) and (item[1] in parentTagAndDefCestorList):
                        if item[1] in [parent[0] for parent in parentIdsFromId_IN([item[0]])]:
                            append_distinct(dotEdgeList, "\"" + item[0] + "\"->\"" + item[1] + "\" [arrowhead=onormal];")
                        else:
                            append_distinct(dotEdgeList, "\"" + item[0] + "\"->\"" + item[1] + "\" [arrowhead=onormal, color=blue];")

        labelTerm = ""
        if cfg("GRAPH_ANGLE") in (2, 3) and "#" not in setMember:
            if validateVerhoeff(setMember):
                labelTerm = fsnFromConId_IN(setMember)[1]
            else:
                labelTerm = simplify(pterm(setMember))
            objValueList.append("\"" + setMember + "\" [label=\"" + setMember + "\l" + labelTerm + findDefString([setMember], 5, True) + checkObjValueColor(setMember, True, PPSList, setMember) + "];")
            if cfg("ADD_INVIS") == 1:
                invisEdgeList.append("\"" + setMember + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
        else:
            if setMember in tagCestorList:
                labelTerm = fsnFromConId_IN(setMember)[1]
            else:
                labelTerm = simplify(pterm(setMember))
            objValueList.append("\"" + setMember + "\" [label=\"" + setMember + "\l" + labelTerm + findDefString([setMember], 5, True) + checkObjValueColor(setMember, True, PPSList, setMember) + "];")
            if cfg("ADD_INVIS") == 1:
                invisEdgeList.append("\"" + setMember + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")

        if definedStatus:
            if checkAllAtts():
                if allLinesBool:
                    groupNodes.append("\"" + DNLabel + "\" [shape=circle, fontsize=27, margin=0, width=0, label=\"&#8801;\", labelloc=b, style=filled, fillcolor=white];")
                else:
                    groupNodes.append("\"" + DNLabel + "\" [shape=circle, fontsize=27, fontcolor=red, margin=0, width=0, label=\"&#8801;\", labelloc=b, style=filled, fillcolor=white];")
            else:
                groupNodes.append("\"" + DNLabel + "\" [shape=circle, fontsize=27, fontcolor=blue, margin=0, width=0, label=\"&#8801;\", labelloc=b, style=filled, fillcolor=white];")
        else:
            if checkAllAtts():
                if allLinesBool:
                    groupNodes.append("\"" + DNLabel + "\" [shape=circle, fontsize=27, margin=0, width=0, label=\"&#8838;\", labelloc=b, style=filled, fillcolor=white];")
                else:
                    groupNodes.append("\"" + DNLabel + "\" [shape=circle, fontsize=27, fontcolor=red, margin=0, width=0, label=\"&#8838;\", labelloc=b, style=filled, fillcolor=white];")
            else:
                groupNodes.append("\"" + DNLabel + "\" [shape=circle, fontsize=27, fontcolor=blue, margin=0, width=0, label=\"&#8838;\", labelloc=b, style=filled, fillcolor=white];")
        longParentTerm = padForParents(parentList)
        spineNum = 0
        if cfg("MERGE_VALS") == 0 or cfg("MERGE_VALS") == 3  or cfg("MERGE_VALS") == 6:
            if not moreThanOne and (cfg("ACROSS_DOWN") > 0):
                append_distinct(dotEdgeList, "\"" + setMember + "\"->\"" + DNLabel + "\" [constraint=false];")
            else:
                append_distinct(dotEdgeList, "\"" + setMember + "\"->\"" + DNLabel + "\";")
            append_distinct(dotEdgeList, "\"" + DNLabel + "\"->\"" + BN0label + "\";")
            firstParent = True
            for item in parentList:
                if cfg("MERGE_VALS") == 6 and moreThanOne:
                    for item in parentList:
                        append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + item[4][0].strip() + "\" [arrowhead=onormal];")
                        append_distinct(objValueList, "\"" + item[4][0].strip() + "\" [label=\"" + item[4][0].strip() + "\l" + simplify(item[4][1]) + findDefString(item[4], 5, True) + checkObjValueColor(item[4][0].strip(), True, PPSList, setMember) + "];")
                        if cfg("ADD_INVIS") == 1:
                            invisEdgeList.append("\"" + item[4][0].strip() + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
                        append_distinct(allParents, item[4][0].strip())
                else:
                    if firstParent and (not moreThanOne) and (cfg("ACROSS_DOWN") > 0):
                        topParentString = ", weight=50"
                    else:
                        topParentString = ""
                    if item[4][0].strip() in allParents:
                        append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + item[4][0].strip() + "_" + str(parentCounter) + "\" [arrowhead=onormal" + topParentString + "];")
                        objValueList.append("\"" + item[4][0].strip() + "_" + str(parentCounter) + "\" [label=\"" + item[4][0].strip() + "\l" + padParent(simplify(item[4][1]), longParentTerm, firstParent) + findDefString(item[4], 5, True) + checkObjValueColor(item[4][0].strip(), True, PPSList, setMember) + "];")
                        if cfg("ADD_INVIS") == 1:
                            invisEdgeList.append("\"" + item[4][0].strip() + "_" + str(parentCounter) + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
                        parentCounter += 1
                    else:
                        if (cfg("ACROSS_DOWN") > 3) and not moreThanOne:
                            if firstParent:
                                append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + item[4][0].strip() + "\" [arrowhead=onormal" + topParentString + "];")
                                if len(parentList) == 1 and (soleRoleList != [] or groupedRoleList != []):
                                    append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                                    objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0];")
                                    spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
                            else:
                                if spineNum == 1:
                                    append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                                else:
                                    append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                                append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum) + "\"->\"" + item[4][0].strip() + "\"" + southWestMarkerParent(item, parentList, soleRoleList, groupedRoleList) + " [arrowhead=onormal" + topParentString + "];")
                                objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0];")
                                spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
                            spineNum += 1
                        else:
                            append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + item[4][0].strip() + "\" [arrowhead=onormal" + topParentString + "];")
                        objValueList.append("\"" + item[4][0].strip() + "\" [label=\"" + item[4][0].strip() + "\l" + padParent(simplify(item[4][1]), longParentTerm, firstParent) + findDefString(item[4], 5, True) + checkObjValueColor(item[4][0].strip(), True, PPSList, setMember) + "];")
                        parentAndAttRankSameString += "\"" + item[4][0].strip() + "\" "
                        parentAndValueRankSameString += "\"" + item[4][0].strip() + "\" "
                        if cfg("ADD_INVIS") == 1:
                            invisEdgeList.append("\"" + item[4][0].strip() + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
                        append_distinct(allParents, item[4][0].strip())
                    firstParent = False
            attNameCounter = 0
            for item in soleRoleList:
                if (cfg("ACROSS_DOWN") > 3) and not moreThanOne:
                    append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                    append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum) + "\"->\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\"" + northWestMarker(item, soleRoleList, groupedRoleList) + ";")
                    objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0];")
                    spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
                    spineNum += 1
                else:
                    append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\";")
                append_distinct(dotEdgeList, "\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\"->\"" + item[4][2] + "_" + setMember + "_" + str(attNameCounter) + "\";")
                attNameList.append("\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\" [label=\"" + item[4][0].strip() + "\l" + simplify(item[4][1]) + findDefString(item[4], 6, True) + "shape=box" + showAttDef(item[4][0].strip(), cfg("SHOW_DEFS")) + ", peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"];")
                parentAndAttRankSameString += "\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\" "
                # CONC - send to wrap check test method for diagram label
                wrapString = wrapStringCheck(item[4][0], item[4][2][0])
                hasDefString = ("\", " if wrapString == "" else findDefString([item[4][2]], 5, True))
                objValueList.append("\"" + item[4][2] + "_" + setMember + "_" + str(attNameCounter) + "\" [label=\"" + item[4][2] + wrapString + simplify(item[4][3]) + hasDefString + checkObjValueColor(item[4][2], False, PPSList, item[4][2]) + "];")
                parentAndValueRankSameString += "\"" + item[4][2] + "_" + setMember + "_" + str(attNameCounter) + "\" "
                if cfg("ADD_INVIS") == 1:
                    invisEdgeList.append("\"" + item[4][2] + "_" + setMember + "_" + str(attNameCounter) + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
                attNameCounter += 1
            groupNumTrack = '0'
            groupedRoleList = sorted(groupedRoleList, key=lambda x: (int(x[3]), x[4][3]))
            groupPadDict = padForGroup(groupedRoleList)
            spineGroupTracker = '0'
            rowInGroup = 0
            if len(groupedRoleList) > 0:
                maxGroup = sorted(list(set([item[3] for item in groupedRoleList])), key=lambda x: int(x))[-1]
            else:
                maxGroup = "0"
            for item in groupedRoleList:
                if (cfg("ACROSS_DOWN") > 3) and not moreThanOne:
                    if spineGroupTracker != item[3]:
                        spineGroupTracker = item[3]
                        rowInGroup = 0
                        if item[3] == maxGroup:
                            append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "GN" + item[3] + "\";")
                            if int(item[3]) > 1:
                                append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + "BN" + str(int(item[3]) - 1) + "\" [style=invis];")
                        else:
                            append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                            append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum) + "\"->\"" + "GN" + item[3] + "\";")
                            objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0];")
                            spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
                        spineNum += 1
                else:
                    append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + "GN" + item[3] + "\";")
                append_distinct(dotEdgeList, "\"" + "GN" + item[3] + "\"->\"" + "BN" + item[3] + "\"" + (" [weight=50]" if (cfg("ACROSS_DOWN") > 0) else "") + ";")
                topRowString = ""
                if (item[3] != groupNumTrack) and (cfg("ACROSS_DOWN") > 0):
                    topRowString = " [weight=50]"
                    groupNumTrack = item[3]
                if (cfg("ACROSS_DOWN") > 3) and not moreThanOne:
                    if rowInGroup == 0:
                        append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"" + topRowString + ";")
                    else:
                        if rowInGroup == 1:
                            append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" [arrowhead=none];")
                        else:
                            append_distinct(dotEdgeList, "\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup - 1) + "\"->\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" [arrowhead=none];")
                        append_distinct(dotEdgeList, "\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\"->\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"" + southWestMarker(item, groupedRoleList) + topRowString + ";")
                        objValueList.append("\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" [label=\"\", shape=circle, width=0, height=0];")
                        groupRankSameString += "\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" "
                    rowInGroup += 1
                else:
                    append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"" + topRowString + ";")
                append_distinct(dotEdgeList, "\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"->\"" + item[4][2] + "_" + item[1] + "_" + item[3] + "\";")
                attNameList.append("\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\" [label=\"" + padIdOrTerm(groupNumTrack, groupPadDict, item[4][0].strip(), "") + "\l" + padIdOrTerm(groupNumTrack, groupPadDict, "", simplify(item[4][1])) + findDefString(item[4], 6, True) + "shape=box" + showAttDef(item[4][0].strip(), cfg("SHOW_DEFS")) + ", peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"];")
                parentAndAttRankSameString += "\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\" "
                # CONC - send to wrap check test method for diagram label
                wrapString = wrapStringCheck(item[4][0], item[4][2][0])
                hasDefString = ("\", " if wrapString == "" else findDefString([item[4][2]], 5, True))
                objValueList.append("\"" + item[4][2] + "_" + item[1] + "_" + item[3] + "\" [label=\"" + item[4][2] + wrapString + simplify(item[4][3]) + hasDefString + checkObjValueColor(item[4][2], False, PPSList, item[4][2]) + "];")
                parentAndValueRankSameString += "\"" + item[4][2] + "_" + item[1] + "_" + item[3] + "\" "
                if cfg("ADD_INVIS") == 1:
                    invisEdgeList.append("\"" + item[4][2] + "_" + item[1] + "_" + item[3] + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
        elif cfg("MERGE_VALS") == 1 or cfg("MERGE_VALS") == 2 or cfg("MERGE_VALS") == 4 or cfg("MERGE_VALS") == 5:
            if (not moreThanOne) and (cfg("ACROSS_DOWN") > 0):
                append_distinct(dotEdgeList, "\"" + setMember + "\"->\"" + DNLabel + "\" [constraint=false];")
            else:
                append_distinct(dotEdgeList, "\"" + setMember + "\"->\"" + DNLabel + "\";")
            append_distinct(dotEdgeList, "\"" + DNLabel + "\"->\"" + BN0label + "\";")
            firstParent = True
            for item in parentList:
                if firstParent and (not moreThanOne) and (cfg("ACROSS_DOWN") > 0):
                    topParentString = ", weight=50"
                else:
                    topParentString = ""
                append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + item[4][0].strip() + "\" [arrowhead=onormal" + topParentString + "];")
                firstParent = False
                if (cfg("SHOW_DEFS") > 8 and item[4][0].strip() not in tagAndDefCestorList) or (cfg("SHOW_DEFS") < 9):
                    append_distinct(objValueList, "\"" + item[4][0].strip() + "\" [label=\"" + item[4][0].strip() + "\l" + padParent(simplify(item[4][1]), longParentTerm, firstParent) + findDefString(item[4], 5, True) + checkObjValueColor(item[4][0].strip(), True, PPSList, setMember) + "];")
                if cfg("ADD_INVIS") == 1:
                    invisEdgeList.append("\"" + item[4][0].strip() + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
            attNameCounter = 0
            for item in soleRoleList:
                append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\";")
                append_distinct(dotEdgeList, "\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\"->\"" + item[4][2] + "\";")
                append_distinct(attNameList, "\"" + item[4][0].strip() + "_" + setMember + "_" + str(attNameCounter) + "\" [label=\"" + item[4][0].strip() + "\l" + simplify(item[4][1]) + findDefString(item[4], 6, True) + "shape=box" + showAttDef(item[4][0].strip(), cfg("SHOW_DEFS")) + ", peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"];")
                # CONC - send to wrap check test method for diagram label
                wrapString = wrapStringCheck(item[4][0], item[4][2][0])
                hasDefString = ("\", " if wrapString == "" else findDefString([item[4][2]], 5, True))
                append_distinct(objValueList, "\"" + item[4][2] + "\" [label=\"" + item[4][2] + wrapString + simplify(item[4][3]) + hasDefString + checkObjValueColor(item[4][2], True, PPSList, item[4][2]) + "];")
                if cfg("ADD_INVIS") == 1:
                    invisEdgeList.append("\"" + item[4][2] + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")
                attNameCounter += 1
            groupNumTrack = '0'
            groupedRoleList = sorted(groupedRoleList, key=lambda x: (int(x[3]), x[4][3]))
            groupPadDict = padForGroup(groupedRoleList)
            for item in groupedRoleList:
                append_distinct(dotEdgeList, "\"" + BN0label + "\"->\"" + "GN" + item[3] + "\";")
                append_distinct(dotEdgeList, "\"" + "GN" + item[3] + "\"->\"" + "BN" + item[3] + "\"" + (" [weight=50]" if (cfg("ACROSS_DOWN") > 0) else "") + ";")
                topRowString = ""
                if (item[3] != groupNumTrack) and (cfg("ACROSS_DOWN") > 0):
                    topRowString = " [weight=50]"
                    groupNumTrack = item[3]
                append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"" + topRowString + ";")
                append_distinct(dotEdgeList, "\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"->\"" + item[4][2] + "\";")
                attNameList.append("\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\" [label=\"" + padIdOrTerm(groupNumTrack, groupPadDict, item[4][0].strip(), "") + "\l" + padIdOrTerm(groupNumTrack, groupPadDict, "", simplify(item[4][1])) + findDefString(item[4], 6, True) + "shape=box" + showAttDef(item[4][0].strip(), cfg("SHOW_DEFS")) + ", peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"];")
                # CONC - send to wrap check test method for diagram label
                wrapString = wrapStringCheck(item[4][0], item[4][2][0])
                hasDefString = ("\", " if wrapString == "" else findDefString([item[4][2]], 5, True))
                append_distinct(objValueList, "\"" + item[4][2] + "\" [label=\"" + item[4][2] + wrapString + simplify(item[4][3]) + hasDefString + checkObjValueColor(item[4][2], True, PPSList, item[4][2]) + "];")
                if cfg("ADD_INVIS") == 1:
                    invisEdgeList.append("\"" + item[4][2] + "\"-> \"" + tok["ROOT"] + "_1\" [style=invis];")

        #add another layer of conditions (only on merge values, and maybe level2 instead)
        if cfg("MERGE_VALS") == 2 or cfg("MERGE_VALS") == 5:
            domAndRangeList = []
            for item in objValueList:
                if "style=invis" not in item:
                    domAndRangeList.append(item.split(" ")[0].replace("\"", ""))

        BN0_DN_Counter += 1
    if cfg("MERGE_VALS") == 2 or cfg("MERGE_VALS") == 5 or (cfg("MERGE_VALS") == 6  and moreThanOne):
        if cfg("MERGE_VALS") == 2 or cfg("MERGE_VALS") == 5:
            myadjacencyList = adjacencyBuild(setToGraph(domAndRangeList, False, []), False)
        else:
            for focusCode in inSet:
                append_distinct(allParents, focusCode)
            myadjacencyList = adjacencyBuild(setToGraph(allParents, False, []), False)
            domAndRangeList = [item for item in allParents]
        for item in myadjacencyList:
            if item[1] not in domAndRangeList:
                if cfg("ADD_INVIS") == 1:
                    append_distinct(objValueList, "\"" + item[1] + "\" [shape=point, label=\"\", shape=circle, style=invis];")
                    append_distinct(dotEdgeList, "\"" + item[0] + "\"->\"" + item[1] + "\" [style=invis];")
            else:
                if (item[0] not in inSet) and (item[1] != tok["ROOT"]):
                    append_distinct(dotEdgeList, "\"" + item[0] + "\"->\"" + item[1] + "\" [arrowhead=onormal, color=blue];")
        if (cfg("SHOW_SUPERS") == 0) or (cfg("SHOW_SUPERS") == 2):
            for item1 in inSet:
                for item2 in inSet:
                    if TCCompare_IN(item1, item2) and not(item1 == item2):
                        if ((item1, item2) not in domAdjList) and ((item1, item2) in myadjacencyList):
                            append_distinct(dotEdgeList, "\"" + item1 + "\"->\"" + item2 + "\" [arrowhead=onormal];")

    # 'late' tidy up for expanded definitions - should probably rewrite to do 'early' (i.e. prevent the rows from being added in the first place).
    objValueRemoveList = []
    if moreThanOne and (cfg("MERGE_VALS") == 0 or cfg("MERGE_VALS") == 3):
        for setItem in inSet:
            foundItemKeeper = False
            foundItemLoser = False
            for item in dotEdgeList:
                if ("->\"" + setItem in item) and not "[arrowhead=onormal" in item: # check for an object concept that is also a value (but not an IsA value)
                    itemKeeper = item
                    foundItemKeeper = True
                if (setItem + "\"->" in item): # check for an object row
                    itemLoser = item
                    foundItemLoser = True
                if foundItemKeeper and foundItemLoser:
                    # reset booleans
                    foundItemKeeper = False
                    foundItemLoser = False
                    # clean up dotEdgeList - add a new row that recasts the value node as an object node.
                    dotEdgeList.append(itemKeeper[itemKeeper.find("->") + 2:itemKeeper.find(";")] + itemLoser[itemLoser.find("->"):])
                    # dotEdgeList.remove(itemKeeper)
                    dotEdgeList.remove(itemLoser)
                    # clean up objValueList
                    for objValueItem in objValueList:
                        if "\"" + setItem + "\"" in objValueItem: # where the setItem is the code of a row in objValueList
                            isAParentToo = False
                            for row in dotEdgeList:
                                if ("->\"" + setItem + "\" [arrowhead=onormal];" in row) or ("->\"" + setItem + "\" [arrowhead=onormal, color=blue];" in row): # check we are not removing an object concept that is not also an unmodelled IsA value.
                                    isAParentToo = True
                            if not isAParentToo:
                                objValueRemoveList.append(objValueItem)
    objValueList = list(set(objValueList) - set(objValueRemoveList))
    # end build dot collections
    try:
        if moreThanOne:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'conpicSet_temp.dot', 'w', encoding="UTF-8")
        else:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'conpic_temp.dot', 'w', encoding="UTF-8")
        f.write("strict digraph G { rankdir=" + edgeDirection(inBTDir) + "; ranksep=.2;" + splineText + " node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        for item in dotEdgeList:
            f.write(item + "\n")
        if cfg("DRAW_DIAG") == 4:
            for item in sorted_cfg(invisEdgeList):
                f.write(item + "\n")
        for item in sorted_cfg(bunchNodes):
            f.write(item + "\n")
        for item in sorted_cfg(groupNodes):
            f.write(item + "\n")
        for item in sorted_cfg(attNameList):
            f.write(item + "\n")
        for item in sorted_cfg(objValueList):
            if moreThanOne and ((cfg("SHOW_SUPERS") == 1) or (cfg("SHOW_SUPERS") == 3)): # for multiple sets, highlight the member objects as pink - retain periphery numbers
                for setItem in inSet:
                    if setItem in item and not (("_" + setItem) in item):
                        item = item.replace('"#99CCFF"', 'pink')
                        item = item.replace('"#CCCCFF"', 'pink')
            f.write(item + "\n")
        if (cfg("ACROSS_DOWN") > 3) and not moreThanOne:
            f.write(spineRankSameString[:-1] + "}\n")
            f.write(groupRankSameString[:-1] + "}\n")
        if (cfg("ACROSS_DOWN") == 5) and not moreThanOne:
            f.write(parentAndAttRankSameString[:-1] + "}\n")
        if (cfg("ACROSS_DOWN") == 6) and not moreThanOne:
            f.write(parentAndValueRankSameString[:-1] + "}\n")
        f.write("}")
    finally:
        f.close()
        if moreThanOne:
            renameDiagramFile('conpicSet_temp.dot', 'conpicSet.dot')
        else:
            renameDiagramFile('conpic_temp.dot', 'conpic.dot')

def padForParents(parentList):
    longTerm = 0
    for row in parentList:
        if len(row[4][1]) > longTerm:
            longTerm = len(row[4][1])
    return longTerm

def padParent(inTerm, longTerm, firstParent):
    outTerm = ""
    if cfg("ACROSS_DOWN") == 3 and firstParent and (len(inTerm) < longTerm):
        outTerm = inTerm + "            " + (" " * (longTerm - len(inTerm)))
    else:
        outTerm = inTerm
    return outTerm

def padForGroup(groupRoleList):
    returnDict = {}
    groupNumList = list(set([item[3] for item in groupRoleList]))
    longId = 0
    longTerm = 0
    for num in groupNumList:
        for row in groupRoleList:
            if row[3] == num and len(row[4][0]) > longId:
                longId = len(row[4][0])
            if row[3] == num and len(row[4][1]) > longTerm:
                longTerm = len(row[4][1])
        if longId > longTerm:
            returnDict[num] = [longId, 0]
        else:
            returnDict[num] = [0, longTerm]
    return returnDict

def padIdOrTerm(groupNumTrack, groupPadDict, AttId, AttTerm):
    idOrTerm = ""
    if cfg("ACROSS_DOWN") in [2, 3]:
        if groupPadDict[groupNumTrack][0] == 0: # only change the term
            if AttTerm == "": # if it's not the term
                idOrTerm = AttId # return the id
            else: # modify the term
                if len(AttTerm) < groupPadDict[groupNumTrack][1]:
                    idOrTerm = AttTerm + "      " + (" " * (groupPadDict[groupNumTrack][1] - len(AttTerm)))
                else:
                    idOrTerm = AttTerm
        else: # change the id
            if AttId == "": # if its not the id
                idOrTerm = AttTerm # return the term
            else: # modify the id
                idOrTerm = AttId + (" " * (groupPadDict[groupNumTrack][0] - len(AttId)))
    else:
        idOrTerm = AttId + AttTerm
    return idOrTerm

def southWestMarkerParent(item, parentList, soleRoleList, groupedRoleList):
    outString = ""
    if (len(soleRoleList) == 0) and (len(groupedRoleList) == 0) and (len(parentList) > 1):
        index = parentList.index(item)
        if index == len(parentList) - 1:
            outString = ":sw"
    return outString

def northWestMarker(item, soleRoleList, groupedRoleList):
    outString = ""
    if (len(groupedRoleList) == 0) and (len(soleRoleList) > 1):
        index = soleRoleList.index(item)
        if index == len(soleRoleList) - 1:
            outString = ":nw"
    return outString

def southWestMarker(item, groupedRoleList):
    outString = ""
    index = groupedRoleList.index(item)
    if index == len(groupedRoleList) - 1:
        outString = ":sw"
    elif groupedRoleList[index + 1][3] > item[3]:
        outString = ":sw"
    return outString

def showAttDef(inId, showOrNot):
    if showOrNot > 0:
        if hasDef(inId):
            showAttDefString = ", fontcolor=blue"
        else:
            showAttDefString = ""
    else:
        showAttDefString = ""
    return showAttDefString

def findDefString(node, levelInt, fromFocusBool=False):
    # > 4:graphs, > 5:graphs + focus diagram o's and v's (only), >6: graphs + focus attnames (only), > 7: graphs + focus oav's
    hasDefBool = False
    if hasDef(node[0]):
        if cfg("GRAPH_ANGLE") in (2, 3):
            ptermLen = len(fsnFromConId_IN(node[0])[1])
        else:
            ptermLen = len(pterm(node[0]))
        if fromFocusBool: # from the focus script
            if cfg("SHOW_DEFS") in (6, 9, 10, 11, 12) and levelInt > 4: # att, obj & val
                hasDefBool = True
        else: # from any graph
            if cfg("SHOW_DEFS") in (5, 6, 7, 8, 9, 10, 11, 12) and levelInt > 3:
                hasDefBool = True
        if hasDefBool:
            defString = "\l\l" + toString(textwrap.wrap(simplify(onlyDefTermsFromConId_IN(node[0])[1].replace(' (*)', '')), (50 if ptermLen < 50 else ptermLen))) + "\\n\", "
        else:
            defString = "\l\", "
    else:
        defString = "\l\", "
    return defString

def resolveHistoryTargets(historyEdges, ancestorNodes, testList, level):
    # before/after setup
    historyEdgesIn = historyEdges[:]
    testListIn = testList[:]
    ancestorNodesIn = ancestorNodes[:]
    level += 1
    while level < 1000: # arbitrary end point
        for item in testList:
            historyTargets = historicalDestinationsFromConId_IN(item, 1, 1, False)
            for historyTarget in historyTargets:
                append_distinct(historyEdges, (historyTarget[1], historyTarget[2], historyTarget[0]))
                if cfg("GRAPH_ANGLE") in (2, 3):
                    historyTerm1 = fsnFromConId_IN(historyTarget[1])[1]
                    historyTerm2 = fsnFromConId_IN(historyTarget[2])[1]
                else:
                    historyTerm1 = pterm(historyTarget[1])
                    historyTerm2 = pterm(historyTarget[2])
                append_distinct(ancestorNodes, (historyTarget[1], historyTerm1))
                append_distinct(ancestorNodes, (historyTarget[2], historyTerm2))
                ancestorNodesTemp = []
                if conMetaFromConId_IN(historyTarget[2], False)[1] == 1:
                    ancestorNodesTemp = ancestorsFromConId_IN(historyTarget[2], True)
                    for ancNode in ancestorNodesTemp:
                        append_distinct(ancestorNodes, ancNode)
                else:
                    if historyTarget[2] not in testList:
                        append_distinct(testList, historyTarget[2])
                if conMetaFromConId_IN(historyTarget[1], False)[1] == 0:
                    append_distinct(testList, historyTarget[1])
                else:
                    ancestorNodesTemp = ancestorsFromConId_IN(historyTarget[1], True)
                    for ancNode in ancestorNodesTemp:
                        append_distinct(ancestorNodes, ancNode)
        # conditional end point
        if (historyEdgesIn == historyEdges) and (ancestorNodesIn == ancestorNodes) and (testListIn == testList):
            level = 1000
        # recursive call
        [historyEdges, ancestorNodes, testList, level] = resolveHistoryTargets(historyEdges, ancestorNodes, testList, level)
    # end return
    return [historyEdges, ancestorNodes, testList, level]

def singleLayerHistoryOrigins(historyEdges, ancestorNodes, inId, inOption, inLookup, movedFromBool):
    historyOrigins = historicalDestinationsFromConId_IN(inId, inOption, inLookup, movedFromBool)
    firstLayerHistoryOriginIds = [item[1] for item in historyOrigins]
    for historyOrigin in historyOrigins:
        if historyOrigin[1][-2] == "1":
            historyOrigin1 = CiDFromDiD_IN(tuplify(historyOrigin[1]))
        else:
            historyOrigin1 = historyOrigin[1]
        append_distinct(historyEdges, (historyOrigin1, historyOrigin[2], historyOrigin[0]))
        # inter-historical component relationships
        historyOriginsTemp = historicalDestinationsFromConId_IN(historyOrigin1, inOption, inLookup, movedFromBool)
        for historyOriginTemp in historyOriginsTemp:
            if (historyOriginTemp[1] in firstLayerHistoryOriginIds) and (historyOriginTemp[2] in firstLayerHistoryOriginIds):
                if historyOriginTemp[1][-2] == "1":
                    historyOrigin1Temp = CiDFromDiD_IN(tuplify(historyOriginTemp[1]))
                else:
                    historyOrigin1Temp = historyOriginTemp[1]
                append_distinct(historyEdges, (historyOrigin1Temp, historyOriginTemp[2], historyOriginTemp[0]))

        if cfg("GRAPH_ANGLE") in (2, 3):
            historyTerm1 = fsnFromConId_IN(historyOrigin1)[1]
            historyTerm2 = fsnFromConId_IN(historyOrigin[2])[1]
        else:
            historyTerm1 = pterm(historyOrigin1)
            historyTerm2 = pterm(historyOrigin[2])
        append_distinct(ancestorNodes, (historyOrigin1, historyTerm1))
        append_distinct(ancestorNodes, (historyOrigin[2], historyTerm2))
        ancestorNodesTemp = []
        if conMetaFromConId_IN(historyOrigin1, False)[1] == 1:
            ancestorNodesTemp = ancestorsFromConId_IN(historyOrigin1, True)
            for ancNode in ancestorNodesTemp:
                append_distinct(ancestorNodes, ancNode)
    return historyEdges, ancestorNodes

def salvageMissingHistory(inId, bestGuessHistoryEdge, ancestorNodes, equalitySign="="):
    # impatient attempt to calculate active target for essentially orphaned inactive
    # content based on inactive is_a relations and historical targets.
    # NOT the same as official UK hist subst. table (only uses snap) so unreliable & NOT authoritative.
    tempTargets = []
    tempTargetsBefore = []
    activeTempTargets = []
    stageCounter = 1
    tempTargets.append([stageCounter, inId])
    ET = conMetaFromConId_IN(inId, False)[2]
    while tempTargetsBefore != tempTargets:
        tempTargetsBefore = tempTargets[:]
        for tempTarget in tempTargets:
            # climb up vestige of inactive is_a relationships
            # (preferably at the same ET ("=") as the inId inactivation ET
            # or after/before (">="/"<=") if nothing found).
            # NEEDS MORE WORK!
            tempRels = inactiveIsAsFromConId_IN(tempTarget[1], ET, equalitySign)
            for tempRel in tempRels:
                if tempRel not in [item[1] for item in tempTargets]:
                    # bit arbitrary, but does favour ancestors encountered sooner.
                    stageCounter += 1
                    tempTargets.append([stageCounter, tempRel])
    # expand the ancestor target set to include historical targets (except MOVED TO)
    for target in tempTargets:
        moreTargets = historicalDestinationsFromConId_IN(target[1], 1, 1, False, False)
        for moreTarget in moreTargets:
            if (not moreTarget[0] == tok["MOVED_TO"]) and (moreTarget[2] not in [item[1] for item in tempTargets]):
                append_distinct(tempTargets, [target[0], moreTarget[2]])
    # pick out the active ones
    for target in tempTargets:
        if conMetaFromConId_IN(target[1], False)[1] == 1:
            activeTempTargets.append((target, descendantCountFromConId_IN(target[1])))
    # rank on 'stageCounter' and descendant counts
    activeTempTargets = sorted(activeTempTargets, key=lambda x: (x[0][0], x[1]))
    bestGuessHistoryTarget = ""
    if activeTempTargets != []:
        # pick the 'nearest' active one; arbitrary - ? this could be improved
        # by proper 'shadow testing' to yield better (and multiple)
        # candidate active ancestors
        if cfg("DIAG_HIST") > 3:
            # early effort to identify > 1 unshadowed ancestors
            activeTempTargets = whittleDown(activeTempTargets)
            for item in activeTempTargets:
                append_distinct(bestGuessHistoryEdge, (inId, item[0][1]))
                ancNodes = ancestorsFromConId_IN(item[0][1], True)
                for anc in ancNodes:
                    append_distinct(ancestorNodes, anc)
        else:
            # pick the 'nearest' active one;
            bestGuessHistoryTarget = activeTempTargets[0][0][1]
            # generate the edge
            bestGuessHistoryEdge.append((inId, bestGuessHistoryTarget))
            # generate ancestor cloud
            for item in ancestorsFromConId_IN(bestGuessHistoryTarget, True):
                append_distinct(ancestorNodes, item)
    else:
        # if nothing found, fall back to tag comparison
        bestGuessHistoryTarget = compareOnTag(inId)
        if bestGuessHistoryTarget != inId:
            bestGuessHistoryEdge.append((inId, bestGuessHistoryTarget))
            for item in ancestorsFromConId_IN(bestGuessHistoryTarget, True):
                append_distinct(ancestorNodes, item)
    return bestGuessHistoryEdge, ancestorNodes

def whittleDown(activeTempTargets):
    removeList = []
    for item1 in activeTempTargets:
        for item2 in activeTempTargets:
            if TCCompare_IN(item1[0][1], item2[0][1]) and not (item1[0][1] == item2[0][1]):
                append_distinct(removeList, item2)
    for item in removeList:
        activeTempTargets.remove(item)
    return activeTempTargets

def dotDrawAncestors(inId, active, inDict=0, inDir="BT"):
    if inDict != 0:
        key_list = list(inDict.keys())
        val_list = list(inDict.values())
    ancestorNodes = []
    ancestorEdges = []
    writeableNodes = []
    historyEdges = []
    historyAncestorNodes = []
    bestGuessHistoryEdge = []
    refersToBool = refersToRowsFromConIdFocusCheck(tuplify(inId))
    if cfg("LONG_SHORT") == 1:
        pathRowsList = pathCalcFromId_IN(inId)
    if active == 1:
        ancestorNodes = ancestorsFromConId_IN(inId, True)
        if cfg("DIAG_HIST") > 0 and refersToBool:
            sendToHistoryNodes = ancestorNodes[:]
            historyEdgesAndAncestorNodesList = resolveHistoryTargets(historyEdges, sendToHistoryNodes, [inId], 1)
            historyEdges = historyEdgesAndAncestorNodesList[0]
            historyAncestorNodes = [node for node in historyEdgesAndAncestorNodesList[1] if ((node not in ancestorNodes) and (node != inId))]
            ancestorNodes = [*ancestorNodes, *historyEdgesAndAncestorNodesList[1]] # needs improvement
        if cfg("DIAG_HIST") > 1 and (not inId == tok["ROOT"]):
            sendToHistoryNodes = ancestorNodes[:]
            historyEdges, backFromHistoryNodes = singleLayerHistoryOrigins(historyEdges, sendToHistoryNodes, inId, 1, 0, True)
            ancestorNodes = backFromHistoryNodes[:]
    else:
        dotDrawTreeMapIsh((tok["ROOT"],), {}, False, False, False)
        drawPics(3)
        if cfg("DIAG_HIST") > 0 and (not inId == tok["ROOT"]):
            sendToHistoryNodes = ancestorNodes[:]
            historyEdgesAndAncestorNodesList = resolveHistoryTargets(historyEdges, sendToHistoryNodes, [inId], 1)
            historyEdges = historyEdgesAndAncestorNodesList[0]
            # note: this route does not populate historyAncestorNodes
            ancestorNodes = [*ancestorNodes, *historyEdgesAndAncestorNodesList[1]] # needs improvement
            # synthetic extra history for concepts with no, or only 'moved to' history.
        if cfg("DIAG_HIST") > 2 and (not inId == tok["ROOT"]):
            if (historyEdges == [] and ancestorNodes == []) or (len(historyEdges) == 1 and historyEdges[0][2] == tok["MOVED_TO"]):
                bestGuessHistoryEdge, ancestorNodes = salvageMissingHistory(inId, bestGuessHistoryEdge, ancestorNodes)
            if (bestGuessHistoryEdge == [] and ancestorNodes == []):
                bestGuessHistoryEdge, ancestorNodes = salvageMissingHistory(inId, bestGuessHistoryEdge, ancestorNodes, ">=")
    PPSList = [PPS[0] for PPS in PPSFromAncestors_IN(ancestorNodes)]
    selfAndParentNodes = parentsFromId_IN(tuplify(inId))
    selfAndParentNodes = [node[1] for node in selfAndParentNodes]
    selfAndParentNodes.append(inId)
    for node in ancestorNodes:
        for parent in parentsFromId_IN(tuplify(node[0]), True):
            ancestorEdges.append([node, parent])
    for node in ancestorNodes:
        defString = findDefString(node, 4)
        if node in historyAncestorNodes:
            historyNodeBool = True
        else:
            historyNodeBool = False
        if cfg("DRAW_DIAG") == 8 and node[0] in selfAndParentNodes:
            sizeString = str(descendantCountFromConId_IN(node[0]))
            childSizeString = str(childCountFromId_IN((node[0],)))
            if childSizeString != "0":
                combString = "\l" + sizeString + " (" + childSizeString + ")"
            else:
                combString = ""
            writeableNodes.append("\"" + node[0] + "\" [label=\"[" + str(key_list[val_list.index(tuplify(node[0]))]) + "] " + node[0] + "\l" + simplify(node[1]) + combString + defString + "fontsize=14, " + checkObjValueColor(node[0].strip(), True, PPSList, inId, historyNodeBool) + "];\n")
        else:
            metadata = conMetaFromConId_IN(node[0], True)
            if len(metadata) == 4:
                writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + defString + checkObjValueColor(node[0].strip(), True, PPSList, inId, historyNodeBool) + "];\n")
            else:
                writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + "\l(" + metadata[-1] + ")" + defString + checkObjValueColor(node[0].strip(), True, PPSList, inId, historyNodeBool) + "];\n")
    try:
        if cfg("GRAPH_ANGLE") in (1, 3): # use angled edges
            splineString = "; splines=ortho"
        else:
            splineString = ""
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc_temp.dot', 'w', encoding="UTF-8")
        f.write("strict digraph G { rankdir=" + inDir + "; ranksep=.3" + splineString + "; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        edgeTypeDict = {0:"[arrowhead=onormal]",
                        1:"[arrowhead=onormal, color=orange, penwidth=2]",
                        2:"[arrowhead=onormal, color=red, penwidth=2]",
                        3:"[arrowhead=onormal, color=purple, penwidth=2]"}
        for edge in sorted_cfg(ancestorEdges):
            edgeType = 0
            if cfg("LONG_SHORT") == 1:
                if [edge[0][0], edge[1][1]] in pathRowsList[1]:
                    edgeType += 1
                if [edge[0][0], edge[1][1]] in pathRowsList[2]:
                    edgeType += 2
            f.write("\"" + edge[0][0] + "\"->\"" + edge[1][1] + "\" " + edgeTypeDict[edgeType] + ";\n")
        if active == 0 or refersToBool or (cfg("DIAG_HIST") > 1):
            for edge in sorted_cfg(historyEdges):
                if (edge[2] == tok["REFERS_TO"]) and (cfg("COLOR_REFERS_TO") == 1):
                    edgeColor = refersToEdgeColor(edge[1], edge[0])
                else:
                    edgeColor = "green"
                f.write("\"" + edge[0] + "\"->\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [arrowhead=none, color=" + edgeColor + "];\n")
                f.write("\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=" + edgeColor + "];\n")
                writeableNodes.append("\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=" + edgeColor + ", font=helvetica, fontsize=5, height=0, label=\"" + simplify(pterm(edge[2]).replace(" association reference set", "").upper()) + "\"];\n")
        for edge in bestGuessHistoryEdge:
            f.write("\"" + edge[0] + "\"->\"" + edge[0] + "|" + edge[1] + "\" [arrowhead=none, color=orange];\n")
            f.write("\"" + edge[0] + "|" + edge[1] + "\"->\"" + edge[1] + "\" [arrowhead=onormal, color=orange];\n")
            writeableNodes.append("\"" + edge[0] + "|" + edge[1] + "\" [style=\"rounded,filled\", fillcolor=orange, color=orange, font=helvetica, fontsize=5, height=0, label=\"SALVAGED TARGET BASED\nON SNAPSHOT DATA\n(cf. HISTORY SUBST. TABLE)\"];\n")
        for node in sorted_cfg(writeableNodes):
            f.write(node)
        if active == 0 and inId not in [node[0] for node in ancestorNodes]:
            metadata = conMetaFromConId_IN(inId, True)
            f.write("\"" + inId + "\" [label=\"" + inId + "\l" + simplify(pterm(inId)) + "\l(" + metadata[-1] + ")\l\", shape=box, peripheries=1, style=filled, fillcolor=palegreen];\n")
        if cfg("DRAW_DIAG") == 8:
            focusStringDict = { 1:"Focus", 2:"Focus and parent", 3:"Focus and parents"}
            focusChoice = 3 if len(selfAndParentNodes) > 3 else len(selfAndParentNodes)
            f.write("subgraph cluster {labelloc=t; labeljust=l; label=\"" + focusStringDict[focusChoice] + "\";  peripheries=0; style=filled; fillcolor=powderblue; fontsize=14; fontname=Helvetica;\n")
            if active == 0 or refersToBool or (cfg("DIAG_HIST") > 1):
                for edge in sorted_cfg(historyEdges):
                    if edge[0] in selfAndParentNodes and edge[1] in selfAndParentNodes:
                        selfAndParentNodes.append(edge[0] + "|" + edge[1] + "|" + edge[2])
            for item in sorted_cfg(selfAndParentNodes):
                f.write("\"" + item + "\";")
            f.write("}\n")
        f.write("}")
    finally:
        f.close()
        renameDiagramFile('contc_temp.dot', 'contc.dot')

def dotDrawDescendants(inId):
    descendantNodes = []
    descendantEdges = []
    descendantNodes = descendantsFromConId_IN(inId, True)
    for node in descendantNodes:
        for child in childrenFromId_IN(tuplify(node[0])):
            descendantEdges.append([child, node])
    try:
        if cfg("GRAPH_ANGLE") in (1, 3): # use angled edges
            splineString = "; splines=ortho"
        else:
            splineString = ""
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc_temp.dot', 'w', encoding="UTF-8")
        f.write("strict digraph G { rankdir=RL; ranksep=.3" + splineString + "; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        for edge in sorted_cfg(descendantEdges):
            f.write("\"" + edge[0][2] + "\"->\"" + edge[1][0] + "\" [arrowhead=onormal];\n")
        for node in sorted_cfg(descendantNodes):
            defString = findDefString(node, 4)
            f.write("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + defString + checkObjValueColor(node[0].strip(), False, descendantNodes, inId) + "];\n")
        f.write("}")
    finally:
        f.close()
        renameDiagramFile('contc_temp.dot', 'contc.dot')
    # reset after every descendant graph (too risky to leave as may easily select concept with many descendants)
    drawPics(3)

def dotDrawAncestorsAndDescendants(inId, inDir):
    descendantNodes = []
    descendantEdges = []
    descendantNodes = descendantsFromConId_IN(inId, True)
    for node in descendantNodes:
        for child in childrenFromId_IN(tuplify(node[0])):
            descendantEdges.append([child, node])
    ancestorNodes = []
    ancestorEdges = []
    writeableNodes = []
    historyEdges = []
    historyAncestorNodes = []
    bestGuessHistoryEdge = []
    ancestorNodes = ancestorsFromConId_IN(inId, True)
    refersToBool = refersToRowsFromConIdFocusCheck(tuplify(inId))
    if cfg("DIAG_HIST") > 0 and refersToBool:
        sendToHistoryNodes = ancestorNodes[:]
        historyEdgesAndAncestorNodesList = resolveHistoryTargets(historyEdges, sendToHistoryNodes, [inId], 1)
        historyEdges = historyEdgesAndAncestorNodesList[0]
        historyAncestorNodes = [node for node in historyEdgesAndAncestorNodesList[1] if ((node not in ancestorNodes) and (node != inId))]
        ancestorNodes = [*ancestorNodes, *historyEdgesAndAncestorNodesList[1]] # needs improvement
    if cfg("DIAG_HIST") > 1 and (not inId == tok["ROOT"]):
        sendToHistoryNodes = ancestorNodes[:]
        historyEdges, backFromHistoryNodes = singleLayerHistoryOrigins(historyEdges, sendToHistoryNodes, inId, 1, 0, True)
        historyAncestorNodes = [node for node in backFromHistoryNodes if ((node not in ancestorNodes) and (node != inId))]
        ancestorNodes = backFromHistoryNodes[:]
    if cfg("DIAG_HIST") > 2 and (not inId == tok["ROOT"]):
        # synthetic extra history for concepts with no, or only 'moved to' history.
        if (historyEdges == [] and ancestorNodes == []) or (len(historyEdges) == 1 and historyEdges[0][2] == tok["MOVED_TO"]):
            bestGuessHistoryEdge, ancestorNodes = salvageMissingHistory(inId, bestGuessHistoryEdge, ancestorNodes)
        if (bestGuessHistoryEdge == [] and ancestorNodes == []):
            bestGuessHistoryEdge, ancestorNodes = salvageMissingHistory(inId, bestGuessHistoryEdge, ancestorNodes, ">=")
    PPSList = [PPS[0] for PPS in PPSFromAncestors_IN(ancestorNodes)]
    for node in ancestorNodes:
        for parent in parentsFromId_IN(tuplify(node[0])):
            ancestorEdges.append([node, parent])
    for node in ancestorNodes:
        defString = findDefString(node, 4)
        if node in historyAncestorNodes:
            historyNodeBool = True
        else:
            historyNodeBool = False
        writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + defString + checkObjValueColor(node[0].strip(), True, PPSList, inId, historyNodeBool) + "];\n")
    try:
        if cfg("GRAPH_ANGLE") in (1, 3): # use angled edges
            splineString = "; splines=ortho"
        else:
            splineString = ""
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc_temp.dot', 'w', encoding="UTF-8")
        f.write("strict digraph G { rankdir=" + inDir + "; ranksep=.3" + splineString + "; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        for edge in sorted_cfg(descendantEdges):
            f.write("\"" + edge[0][2] + "\"->\"" + edge[1][0] + "\" [arrowhead=onormal];\n")
        for edge in sorted_cfg(ancestorEdges):
            f.write("\"" + edge[0][0] + "\"->\"" + edge[1][1] + "\" [arrowhead=onormal];\n")
        for node in sorted_cfg(descendantNodes):
            defString = findDefString(node, 4)
            f.write("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + defString + checkObjValueColor(node[0].strip(), False, descendantNodes, inId) + "];\n")
    # if refersToBool:
        for edge in sorted_cfg(historyEdges):
            if (edge[2] == tok["REFERS_TO"]) and (cfg("COLOR_REFERS_TO") == 1):
                edgeColor = refersToEdgeColor(edge[1], edge[0])
            else:
                edgeColor = "green"
            f.write("\"" + edge[0] + "\"->\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [arrowhead=none, color=" + edgeColor + "];\n")
            f.write("\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=" + edgeColor + "];\n")
            writeableNodes.append("\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=" + edgeColor + ", font=helvetica, fontsize=5, height=0, label=\"" + simplify(pterm(edge[2]).replace(" association reference set", "").upper()) + "\"];\n")
        for edge in bestGuessHistoryEdge:
            f.write("\"" + edge[0] + "\"->\"" + edge[0] + "|" + edge[1] + "\" [arrowhead=none, color=orange];\n")
            f.write("\"" + edge[0] + "|" + edge[1] + "\"->\"" + edge[1] + "\" [arrowhead=onormal, color=orange];\n")
            writeableNodes.append("\"" + edge[0] + "|" + edge[1] + "\" [style=\"rounded,filled\", fillcolor=orange, color=orange, font=helvetica, fontsize=5, height=0, label=\"SALVAGED TARGET BASED\nON SNAPSHOT DATA\n(cf. HISTORY SUBST. TABLE)\"];\n")
        for node in sorted_cfg(writeableNodes):
            f.write(node)
        f.write("}")
    finally:
        f.close()
        renameDiagramFile('contc_temp.dot', 'contc.dot')
    # reset after every descendant graph (too risky to leave as may easily select concept with many descendants)
    drawPics(3)

def edgeDirection(inBTDir):
    if inBTDir == "^":
        dirString = 'BT'
    elif inBTDir == "<" or inBTDir == "+":
        dirString = 'RL'
    elif inBTDir == "v":
        dirString = 'TB'
    elif inBTDir == ">":
        dirString = 'LR'
    return dirString

def setAttsInt(testId, incExclString, operatorString):
    testId = setAtts(testId, incExclString, operatorString)
    return testId

def showRefsetMatches(callId):
    if not isinstance(callId, tuple):
        callId = tuplify(callId)
    mydict = {}
    counter = 1
    if parentsFromId_IN(callId)[0][1] == tok["ROOT"]:
        topLevelCallBool = True
    else:
        topLevelCallBool = False
    print("\n\033[92m[" + str(counter) + "]", callId[0], pterm(callId), "membership details:\n")
    mydict.update(list(zip(listify(counter), listify(callId))))
    counter += 1
    if topLevelCallBool:
        refsetType = "simpleRefset"
    else:
        refsetType = refSetTypeFromConId_IN(callId[0])[0][0]
        print("Refset type: '" + refsetType + "'")
        print(str(refSetMemberCountFromConId_IN(callId[0], refsetType)) + " members.\n")
    leftCount, otherRows = refSetMembersCompareRefTableFromConIds_IN(callId[0], "", 1)
    if topLevelCallBool:
        print("Content in " + str(leftCount) + " refset(s):\n")
    else:
        print("Content from " + str(leftCount) + " chapter(s):\n")
    for row in otherRows:
        if topLevelCallBool:
            print("[" + str(counter) + "]\t" + proportionColor_IN(row[6]) + (" " * (6 - len(str(row[6])))) + row[1] + tabString("(" + str(row[6]) + "%) " + row[1]) + pterm(row[1]))
            mydict.update(list(zip(listify(counter), listify(tuplify(row[1])))))
        else:
            print("[" + str(counter) + "]\t" + proportionColor_IN(row[6]) + (" " * (6 - len(str(row[6])))) + row[2] + tabString("(" + str(row[6]) + "%) " + row[2]) + pterm(row[2]))
            mydict.update(list(zip(listify(counter), listify(tuplify(row[2])))))
        counter += 1
    if not topLevelCallBool:
        leftCount, otherRows = refSetMembersCompareRefTableFromConIds_IN(callId[0], "", 2)
        print("\nReferencedComponentId matches with " + str(leftCount) + " other '" + refsetType + "' refset(s):\n")
        for row in otherRows:
            print("[" + str(counter) + "]\t" + proportionColor_IN(row[6]) + (" " * (6 - len(str(row[6])))) + row[2] + tabString("(" + str(row[6]) + "%) " +row[2]) + pterm(row[2]) + " " + proportionColor_IN(row[7]))
            mydict.update(list(zip(listify(counter), listify(tuplify(row[2])))))
            counter += 1
    print()
    return mydict

def refsetIntersectionRows(leftId, rightId):
    leftOperator, rightOperator = checkOperators(leftId, rightId)
    readbackListInclude = []
    readbackListExtInclude = []
    readbackListInclude.append(leftOperator + leftId + " | " + pterm(leftId) + "| AND$$$$$" + rightOperator + rightId + " | " + pterm(rightId) + " |\n")
    readbackListExtInclude.append(leftOperator + leftId + " | " + pterm(leftId) + "| AND " + rightOperator + rightId + " | " + pterm(rightId) + " |\n")
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', 'w', encoding="UTF-8")
    for entry in readbackListInclude:
        f.write(entry)
    f.write("EXCLUDE\n")
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLsetExt.txt', 'w', encoding="UTF-8")
    for entry in readbackListExtInclude:
        f.write(entry)
    f.close()

def refsetOnlyRows(leftId, rightId, dirString):
    leftOperator, rightOperator = checkOperators(leftId, rightId)
    readbackListInclude = []
    readbackListExclude = []
    if dirString == "<":
        readbackListInclude.append(leftOperator + leftId + " | " + pterm(leftId) + " |")
        readbackListExclude.append(rightOperator + rightId + " | " + pterm(rightId) + " |")
    else:
        readbackListInclude.append(rightOperator + rightId + " | " + pterm(rightId) + " |")
        readbackListExclude.append(leftOperator + leftId + " | " + pterm(leftId) + " |")
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', 'w', encoding="UTF-8")
    for entry in readbackListInclude:
        f.write(entry + "\n")
    f.write("EXCLUDE\n")
    for entry in readbackListExclude:
        f.write(entry + "\n")
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLsetExt.txt', 'w', encoding="UTF-8")
    for entry in readbackListInclude:
        f.write("(" + entry + ")\n")
    f.write("MINUS\n")
    for entry in readbackListExclude:
        f.write("(" + entry + ")\n")
    f.close()

def checkOperators(leftId, rightId):
    if TCCompare_IN(leftId, tok["REFSETS"]):
        leftOperator = "^ "
    else:
        leftOperator = "<< "
    if TCCompare_IN(rightId, tok["REFSETS"]):
        rightOperator = "^ "
    else:
        rightOperator = "<< "
    return leftOperator, rightOperator

def returnStringToFile(returnString):
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'indexList.txt', 'w', encoding="UTF-8")
    for item in returnString:
        f.write(str(item) + "\t" + returnString[item][0] + "\n")
    f.close()

def readIndexListRev():
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'indexList.txt', 'r', encoding="UTF-8")
    lines = f.readlines()
    f.close()
    returnDict = {}
    for line in lines:
        lineTemp = line.split("\t")
        returnDict.update({lineTemp[0].strip():lineTemp[1].strip()})
    return returnDict

# CONC - check whether data contains concrete rows
def concCheck():
    if concLookUp_IN():
        cfgWrite("CONC_DATA", 1)
    else:
        cfgWrite("CONC_DATA", 0)


def readSSFiles():
    actualList = []
    structuralList = []
    pathList = []
    # actual members
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSMembers.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            actualList.append(line.strip())
    finally:
        f.close()
    # path members
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSPathMembers.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            pathList.append(line.strip())
    finally:
        f.close()
    # structural members
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSStructural.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            structuralList.append(line.strip())
    finally:
        f.close()
    return actualList, structuralList, pathList

def dotProcess(topaction):
    returnAction = ""
    dotDict = dotDictionary()
    topactionList = topaction.split(" ")
    if topaction == '.':
        cfgWrite("SHOW_INDEX_SNAP", 0)
        topaction = 'v'
    elif topaction == '..':
        cfgWrite("SHOW_INDEX_SNAP", 1)
        topaction = 'v'
    elif topactionList[0] in dotDict:
        if len(topactionList) > 1:
            topaction = dotDict[topactionList[0]][0] + topactionList[1] + dotDict[topactionList[0]][1]
        else:
            topaction = dotDict[topactionList[0]][0]
    returnAction = topaction
    return returnAction

def sorted_cfg(inList):
    if cfg("SORT_DIAG") == 3:
        return sorted(inList)
    else:
        return inList

def activeColorLocal(inId, inActive):
    if inActive == 0:
        return '\033[31m' + inId + '\033[92m'
    else:
        return inId

def latestConcepts(inAllBool, onlyConBool):
    if inAllBool:
        conceptList, descriptionList = latestConcepts_IN(getVersion(), True)
    else:
        conceptList, descriptionList = latestConcepts_IN(getVersion(), False)
    mycounter = 1
    mydict = {}
    if len(conceptList) > 0:
        print('Concepts changed [' + str(len(conceptList)) + ']\n')
        for concept in conceptList:
            if not concept is None:
                print('[' + str(mycounter) + ']\t' + activeColorLocal(concept[0], concept[2]) + tabString(concept[0]) + concept[1])
                mydict.update(list(zip(listify(mycounter), listify(tuplify(concept[0])))))
                mycounter += 1
        print()
        f2 = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
        for concept in conceptList:
            f2.write(concept[0] + '\n')
        f2.close()
    if len(descriptionList) > 0 and not onlyConBool:
        print('Descriptions changed on unmodified concepts [' + str(len(descriptionList)) + ']\n')
        checkString = ""
        for description in descriptionList:
            if not description is None:
                if (description[0] != checkString) and (checkString != ""):
                    print()
                print('[' + str(mycounter) + ']\t' + activeColor_IN(description[0])  + " / " + activeColorLocal(description[1], description[3]) + tabStringTwoId(description[0] + description[1]) + description[2])
                mydict.update(list(zip(listify(mycounter), listify(tuplify(description[0])))))
                checkString = description[0]
                mycounter += 1
        print()
    return mydict

def moduleUse(fullModuleList):
    moduleList, returnList, moduleETDict = fileAndModuleDistribution_IN(fullModuleList)
    mycounter = 1
    mydict = {}
    if len(moduleList) > 0:
        print('Modules identified:\n')
        for module in moduleList:
            if not module is None:
                print('[' + str(mycounter) + ']\t' + activeColor_IN(module[0]) + tabString(module[0]) + modColor_IN(module[0], module[1], getVersion()) + (" [" + moduleETDict[module[0]] + "]" if fullModuleList else ""))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(module[0])))))
                mycounter += 1
        print()
    if len(returnList) > 0 and fullModuleList:
        print('Files by pattern and modules:\n')

        for description in returnList:
            print('\033[4m' + description[0] + '\033[24m')
            for fileOrRefset in description[1:]:
                if fileOrRefset[0] != description[0]: # if a refset rather than a basic table, print the refsetid and name
                    print('\n[' + str(mycounter) + ']\t' + activeColor_IN(fileOrRefset[0]) + tabString(fileOrRefset[0]) + fileOrRefset[1])
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(fileOrRefset[0])))))
                else:
                    print()
                for module in fileOrRefset[2]:
                    print('\t* ' + modColor_IN(module[0], module[1], getVersion()))
                mycounter += 1
            print()
        print()
    return mydict

def dotDictionary():
    return {".root": ["a", ""],
            ".reset": ["a", ""],
            ".start": ["a", ""],
            ".search": ["c", ""],
            ".search+": ["c+", ""],
            ".search-": ["c-", ""],
            ".extend": ["d", ""],
            ".term": ["e", ""],
            ".rel": ["f", ""],
            ".relstat": ["f", "s"],
            ".termmet": ["g", ""],
            ".relmet": ["h", ""],
            ".meta": ["i", ""],
            ".bread": ["j", ""],
            ".ref": ["k", ""],
            ".refmet": ["l", ""],
            ".refmem": ["m", ""],
            ".mem": ["m", ""],
            ".refmemmet": ["n", ""],
            ".memmet": ["n", ""],
            ".refdet": ["o", ""],
            ".anc": ["q", ""],
            ".desc": ["r", ""],
            ".hist": ["s", ""],
            ".histmet": ["t", ""],
            ".help": ["u", ""],
            ".reload": ["v", ""],
            ".useful": ["w", ""],
            ".pps": ["x", ""],
            ".pretty": ["y", ""],
            ".swap": ["z", ""],
            ".data": ["z", ""],
            ".diagtype": ["A", ""],
            ".diaglay": ["B", ""],
            ".diagsups": ["C", ""],
            ".atts": ["D", ""],
            ".allatts": ["DD", ""],
            ".noatts": ["Dd", ""],
            ".equiv": ["E", ""],
            ".locale": ["F", ""],
            ".tab": ["G", ""],
            ".indent": ["G", ""],
            ".rsmatch": ["H", ""],
            ".rsintersect": ["I", ""],
            ".rsonly": ["J", "<"],
            ".rsonlyleft": ["J", "<"],
            ".rsonlyright": ["J", ">"],
            ".exit": ["zz", ""],
            ".quit": ["zz", ""]}

def readFromRoleReport(inInt):
    inLines = []
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'setreportId.txt', 'r', encoding="UTF-8")
        inLines = f.readlines()
    finally:
        f.close()
    if len(inLines) >= inInt:
        return tuplify(inLines[inInt - 1].strip())
    else:
        return tuplify(tok["ROOT"])

def growValues(inId, attBool):
    modelledValues = []
    modelledValues.append(inId)
    modelledValuesCountBeginning = 1
    modelledValuesCountEnd = 0
    while modelledValuesCountBeginning != modelledValuesCountEnd:
        modelledValuesCountBeginning = len(modelledValues)
        for item in modelledValues:
            roleList = allRolesFromConId_IN(item) # first set of roles
            for role in roleList: # work through each role...
                if not role[2] in modelledValues and not role[2][0] == "#": # prevent catching concretes
                    modelledValues.append(role[2]) # add attrib val to return collection
                if attBool and not role[0] in modelledValues:
                    modelledValues.append(role[0]) # add attrib nameto return collection
        modelledValuesCountEnd = len(modelledValues)
    return modelledValues

# @see <a href="http://en.wikipedia.org/wiki/Verhoeff_algorithm">More Info</a>
# @see <a href="http://en.wikipedia.org/wiki/Dihedral_group">Dihedral Group</a>
# @see <a href="http://mathworld.wolfram.com/DihedralGroupD5.html">Dihedral Group Order 10</a>
# @author Hermann Himmelbauer

def validateVerhoeff(number): # *SCT*
    """Validate Verhoeff checksummed number (checksum is last digit)"""
    return checksum(number) == 0

def checksum(number):
    """For a given number generates a Verhoeff digit and
    returns number + digit"""
    verhoeff_table_d = (
    (0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
    (1, 2, 3, 4, 0, 6, 7, 8, 9, 5),
    (2, 3, 4, 0, 1, 7, 8, 9, 5, 6),
    (3, 4, 0, 1, 2, 8, 9, 5, 6, 7),
    (4, 0, 1, 2, 3, 9, 5, 6, 7, 8),
    (5, 9, 8, 7, 6, 0, 4, 3, 2, 1),
    (6, 5, 9, 8, 7, 1, 0, 4, 3, 2),
    (7, 6, 5, 9, 8, 2, 1, 0, 4, 3),
    (8, 7, 6, 5, 9, 3, 2, 1, 0, 4),
    (9, 8, 7, 6, 5, 4, 3, 2, 1, 0))
    verhoeff_table_p = (
    (0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
    (1, 5, 7, 6, 2, 8, 3, 0, 9, 4),
    (5, 8, 0, 3, 7, 9, 6, 1, 4, 2),
    (8, 9, 1, 6, 0, 4, 3, 5, 2, 7),
    (9, 4, 5, 3, 1, 2, 6, 8, 7, 0),
    (4, 2, 8, 6, 5, 7, 3, 9, 0, 1),
    (2, 7, 9, 3, 8, 0, 6, 4, 1, 5),
    (7, 0, 4, 6, 9, 1, 3, 2, 5, 8))
    c = 0
    for i, item in enumerate(reversed(str(number))):
        c = verhoeff_table_d[c][verhoeff_table_p[i % 8][int(item)]]
    return c

def resetDiag(inLetter):
    cfgWrite("DRAW_DIAG", 8)
    cfgWrite("MERGE_VALS", 0)
    cfgWrite("SHOW_SUPERS", 1)
    if inLetter == "O":
        cfgWrite("SHOW_DEFS", 0)
    else:
        cfgWrite("SHOW_DEFS", 3)
    if inLetter in ("A", "O"):
        cfgWrite("GRAPH_ANGLE", 1)
    elif inLetter == "S":
        cfgWrite("GRAPH_ANGLE", 0)
    cfgWrite("DIAG_HIST", 1)
    cfgWrite("ACROSS_DOWN", 4)
    cfgWrite("LONG_SHORT", 0)
    cfgWrite("COLOR_REFERS_TO", 1)
    cfgWrite("ADD_INVIS", 0)
    cfgWrite("SORT_DIAG", 2)
    cfgWrite("CHILD_LIMIT", 250)
    clearScreen()
    snapshotHeader()

def graphAngle(inInteger):
    cfgWrite("GRAPH_ANGLE", inInteger)
    return inInteger

def domainOrRangeGen():
    f = io.open(cfgPathInstall() + 'files' + sep() + 'atts' + sep() + 'modelTableCO.txt', 'r', encoding="UTF-8")
    domainList = []
    rangeList = []
    lines = f.readlines()
    f.close()
    for line in lines:
        lineTemp = line.split("\t")
        if lineTemp[1] not in domainList:
            domainList.append(lineTemp[1])
        if lineTemp[7].strip() not in rangeList:
            rangeList.append(lineTemp[7].strip())
    return domainList, rangeList
