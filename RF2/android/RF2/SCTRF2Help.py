from __future__ import print_function, unicode_literals
import io
from SCTRF2config import cfg, cfg_MRCM, cfg_SV
from platformSpecific import cfgPathInstall, sep
from SCTRF2Concrete import concNumberSetting

specialCaseDict = {"FF|SCTMRCM": ['l', 'r', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'],
                "d|SCTMRCM": ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'x', 'y', 'z', 'A', 'B'],
                "AA|SCTRF2": ["A", "B", "C", "L", "O", "P", "R", "S", "T", "U"]}

def selectHelpFile(inString, inFolder):
    # handle case insensitive os's
    if ord(inString[0]) in range(65, 91):
        inString = inString + inString
    # handle special cases:
    if inString + "|" + inFolder in specialCaseDict:
        selectHelpFileSpecial(inString, inFolder)
    else:
        selectHelpFileSimple(inString, inFolder)

def selectHelpFileSimple(inString, inFolder):
    myList = []
    f = io.open(cfgPathInstall() + 'help' + sep() + inFolder + sep() + inString + '.txt', 'r', encoding="UTF-8")
    for line in f:
        myList.append(line)
    print(myList[0])
    if inString[0] in helpIndexDict(inFolder):
        if inFolder == 'SCTRF2SV': # using SV cfg alias
            print('\033[36mCurrent value: ' + str(cfg_SV(helpIndexDict(inFolder)[inString[0]])) + '\033[92m')
        elif inFolder == 'SCTMRCM': # using MRCM cfg alias
            print('\033[36mCurrent value: ' + str(cfg_MRCM(helpIndexDict(inFolder)[inString[0]])) + '\033[92m')
        else:
            print('\033[36mCurrent value: ' + str(cfg(helpIndexDict(inFolder)[inString[0]])) + '\033[92m')
    for item in myList[1:]:
        print(item.strip())
    print()
    f.close()

def selectHelpFileSpecial(inString, inFolder):
    myList = []
    f = io.open(cfgPathInstall() + 'help' + sep() + inFolder + sep() + inString + '.txt', 'r', encoding="UTF-8")
    for line in f:
        myList.append(line)
    print(myList[0].strip() + " - current settings:\n")
    for setting in specialCaseDict[inString + "|" + inFolder]:
        if inFolder == 'SCTRF2SV': # using SV cfg alias
            print('\033[36m[' + setting + '] ' + actionIndexDict('SCTRF2SV')[setting] + '\t' + str(cfg_SV(helpIndexDict(inFolder)[setting])) + '\033[92m')
        elif inFolder == 'SCTMRCM': # using MRCM cfg alias
            print('\033[36m[' + setting + '] ' + actionIndexDict('SCTMRCM')[setting] + '\t' + str(cfg_MRCM(helpIndexDict(inFolder)[setting])) + '\033[92m')
        else: # TODA - separate out browser from sets
            print('\033[36m[' + setting + '] ' + actionIndexDict('SCTRF2')[setting] + '\t' + str(cfg(helpIndexDict(inFolder)[setting])) + '\033[92m')
    for item in myList[1:]:
        print(item.strip())
    print()
    f.close()

def helpIndexDict(inFolder):
    helpIndexDict = {'SCTRF2':
                            {'y':'PRETTY',
                            'A':'DRAW_DIAG',
                            'B':'MERGE_VALS',
                            'C':'SHOW_SUPERS',
                            'E':'WORD_EQUIV',
                            'F':'LOCALE',
                            'G':'TAB_INDENT',
                            'K':'CONC_VALS',
                            'L':'SORT_DIAG',
                            'O':'SHOW_DEFS',
                            'P':'GRAPH_ANGLE',
                            'R':'DIAG_HIST',
                            'S':'ACROSS_DOWN',
                            'T':'LONG_SHORT',
                            'U':'ADD_INVIS'},
                    'SCTRF2R':
                            {'w':'NAV_SS',
                            'O':'DEF_LIMIT',
                            'P':'NAV_TOP',
                            'Q':"DEMO"},
                    'SCTRF2SV':
                            {'k':'SHOW_METADATA',
                            'l':'SHOW_METADATA',
                            'o':'SUB_SHOW',
                            'p':'SUB_SHOW',
                            'y':'EFFECTIVE_TIME',
                            'C':'PRETTY',
                            'F':'D3_EXTRAS',
                            'K':'SHOW_SUPERS',
                            'L':'GRAPH_ANGLE',
                            'M':'ACROSS_DOWN'},
                    'SCTMRCM':
                            {'e':'SHOW_CAPTION',
                            'f':'SHOW_MODEL',
                            'g':'ATT_SIZE',
                            'h':'DOM_RANGE_SIZE',
                            'i':'GENERATE_SVG',
                            'j':'CONSTR_OP',
                            'k':'CLASS_TOOLTIP',
                            'l':'RANK_DIRECTION',
                            'm':'SHOW_PARENT_DOMAIN',
                            'n':'D_3',
                            'o':'HIDE_KEY',
                            'p':'SHOW_KEY',
                            'q':'LOCAL_FILES',
                            'r':'ANGLED_EDGES',
                            's':'FONT_SIZE',
                            't':'WIN_HOST_FILES',
                            'x':'ONLY_ATT',
                            'y':'RUN_PRE',
                            'z':'RUN_POST',
                            'A':'ATT_POWER',
                            'B':'ATT_MULT',
                            'G':'WRITE_TAG_RESULTS',
                            'H':'APPEND_CHANGING_TAG_LIST',
                            'I':'FULL_TAG_RUN',
                            'J':'TAG_RUN_NUM',
                            'K':'TAG_RUN_TYPE',
                            'L':'TAG_BUILD_DATA',
                            'M':'TAG_INCL_OTHER_DOMS',
                            'N':'TAG_FULL_TERM',
                            'O':'TAG_RESTRICT_DOMAIN',
                            'P':'TAG_INCLUDE_ATTS',
                            'Q':'TAG_DOM_RAN_TEST'}}
    return helpIndexDict[inFolder]

def actionIndexDict(inFolder):
    indexDict = {'SCTRF2':
                        {'a': 'Browse from root',
                        'b': 'Browse from id',
                        'c': 'Search (-+ inac)',
                        'd': 'Read back search',
                        'e': '(+row) Terms  ',
                        'f': '(+row) Rel\'nships',
                        'g': '(+row) Term+meta',
                        'h': '(+row) Role+meta',
                        'i': '(+row) Con meta',
                        'j': 'Breadcrumbs   ',
                        'k': '(+row) Refsets',
                        'l': '(+row) Refs+meta',
                        'm': '(+row) RS mem',
                        'n': '(+row) RS mem+meta',
                        'o': '(+r) RS[n] detail',
                        'p': 'Read SV terminal',
                        'q': '(+row) Ancestors',
                        'r': '(+row) Descendants',
                        's': '(+row) History',
                        't': '(+row) Hist+meta',
                        'u': 'Basic help  ',
                        'v': 'Reload focus',
                        'w': 'Useful codes',
                        'x': '(+row) PPS / AVs',
                        'y': 'Pretty (0-3)  ',
                        'z': 'Rotate data  ',
                        'A': 'Pics (0-13) ',
                        'B': 'Merge vals (0-5)',
                        'C': 'Sups/orth (0-3)',
                        'D': '(+row) Spec atts',
                        'E': 'Word equiv (1/0)',
                        'F': 'Locale (+\'en\'...)',
                        'G': 'Tab indent (1/0)',
                        'H': '(+row) RS match',
                        'I': '(+row) RS int.',
                        'J': '(+row +<>) RS only',
                        'K': concNumberSetting(),
                        'L': 'Sort lists (0-5)',
                        'M': 'New concepts',
                        'N': 'Module use  ',
                        'O': 'Show defs (0-12) ',
                        'P': 'Angles/terms (0-3)',
                        'Q': '(+A) Int. queries',
                        'R': 'Diag. hist.(0-4)',
                        'S': 'Force ac/down(1/0)',
                        'T': 'Show LS paths(1/0)',
                        'U': 'Use inv edge (1/0)'},
                'SCTMRCM':
                        {'a': 'List domains ',
                        'b': 'Add/reset domain',
                        'c': 'Rem/reset domain',
                        'd': 'Generate diagram',
                        'e': 'Caption (1/0)  ',
                        'f': 'Show model (1/0)',
                        'g': 'Attrib. size (0-6)',
                        'h': 'Dom/ran size (0-5)',
                        'i': 'Gen SVG (1/0)',
                        'j': 'Constr. ops (1/0)',
                        'k': 'Tooltips (1/0)',
                        'l': 'Rank directn (1-4)',
                        'm': 'Parent dom (1/0)',
                        'n': 'Prepare - D3 (1/0)',
                        'o': 'Hide key (1/0) ',
                        'p': 'Show key (1/0) ',
                        'q': 'Local files (1/0)',
                        'r': 'Angled edges (1/0)',
                        's': '(+int) Font size',
                        't': 'Windows host (1/0)',
                        'u': 'Basic help  ',
                        'v': 'Reset all   ',
                        'w': 'Draw change ',
                        'x': 'Only atts (0-2)',
                        'y': 'Run pre-co (1/0) ',
                        'z': 'Run post-co (1/0)',
                        'A': '(+int) Att power',
                        'B': '(+int) Att mult',
                        'C': 'Test snap focus',
                        'D': '(+row) Dom constr',
                        'E': 'Blank diagram',
                        'F': 'Generate tag diag',
                        'G': 'Tag write results',
                        'H': 'Tag append list',
                        'I': 'Tag full run ',
                        'J': 'Tag run num  ',
                        'K': 'Tag run type ',
                        'L': 'Tag build data',
                        'M': 'Tag include doms',
                        'N': 'Tag full term ',
                        'O': 'Tag restrict dom',
                        'P': 'Tag include atts',
                        'Q': 'Tag test v. D&R'}}
    return indexDict[inFolder]
