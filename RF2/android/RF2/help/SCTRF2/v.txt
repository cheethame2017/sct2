[v] Reload focus

'v' on its own:

Reloads the snapshot focus concept.

If just using the Snapshot Data browser this isn't very useful, but if using the ECL & Sets browser this helps load a focus concept chosen in ECL & Sets into Snapshot.

For example, if ECL and Sets has the following active constraint:

Supertype:
[2] << [3] 71388002 Procedure
Group:
[4]  [5] 260686004 Method = [6] << [7] 129304002 Excision - action
[8]  [9] 405813007 Procedure site - Direct = [10] << [11] 15776009 Pancreatic structure

If I wanted to modify the constraint by changing the [11] 15776009 Pancreatic structure value, I would begin by selecting its index number (11).
This focuses the ECL and Sets browser, and I can navigate up and down from the selected concept, find a new value, reload the constraint ('b') and insert the new value ('i[11]').

If however I wanted rapid access to all descendants, I can only generate this in the Snapshot Data browser ('r' + index).
I can achieve this by pulling the ECL and Sets focus into the Snapshot Data browser by typing 'v'.

Then, as before, I can select a new value, return to the constraint in ECL and Sets ('b'), and inserting the new value ('i[11]').

'v' plus an index number from a query or named set report in ECL & Sets:

focuses the Snapshot browser on the index number concept.
Note: the list must be ungrouped for this to work. If it includes group headings then the index matching is prone to errors.

To exit help page:
Press '1' to load Snapshot focus...