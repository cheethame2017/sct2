[j] Breadcrumbs

Shows a list of the last 25 selected concepts (except for the root concept).
If a concept is selected whilst already on the list, the list does not change.

Active components are shown in green, inactive in red. DescriptionIds are followed by an explanatory parenthesis.

[jj] shows a longer (200) member list.

To exit help page:
Press '1' to load Snapshot focus...