[b] Browse from id

Type or paste a concept or description identifier and press enter.

The browser selects the corresponding concept as the focus. An active concept will have ancestors and descendants (the root concept only has descendants), and an inactive concept will not.

Entered DescriptionIds are held in the breadcrumb list, but for most processing purposes are converted to their corresponding conceptId.
The exceptions are reference set membership functions ([m] and [n]) which can be called on a descriptionId from its breadcrumb index position.

An invalid identifier will return nothing.

To exit help page:
Press '1' to load Snapshot focus...