[x] (+row) PPS

[xN] Shows the calculated proximal primitive supertypes for the index concept.
Note these may differ from the supertype in the stated relationships OWL file because of interactions with various features of the DL.

[x] generates a list of all the attribute names and values in the focus concept's definition. Useful with O1 setting to show attributes and values with text definitions.

To exit help page:
Press '1' to load Snapshot focus...