[T] Save navig set

Saves the active navigation set (and its diagram), either as an existing set or as a new named navigation set:

Preliminaries:

Check that you have the right navigation set current by clicking [w1] or [w3]
Check that you are in the 'show list of navigation sets' screen by clicking [R]

Then, for example, either:

[T3] saves the current navigation set into slot 3 (which already has a name)
[T my new navigation set] saves the current navigation set into a new slot and gives it the name you have chosen.

[T-3] removes navigation set number 3.

Notes:
Only use a simple character set. Spaces are replaced by underscores.

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...