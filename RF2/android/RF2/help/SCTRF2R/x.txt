[x] Breadcrumbs

Returns a list of the last 100 distinct constraints that have been run.

If a constraint is re-run it will not be added again.

A constraint identified this way can be selected by chosing [y] and the number of the constraint (e.g. 'y56' will select breadcrumb constraint [56]).

See also [y].

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...