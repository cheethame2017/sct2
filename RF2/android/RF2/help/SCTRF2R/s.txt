[s] (+A..) Tpl. probe

Selects one of a small selection of templated constraints.

See sS, sZ & sX for special cases.

sA:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
[4] << [5] 138875005 SNOMED CT Concept


sB:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
Ungrouped:
[4] << [5] 762705008 Concept model object attribute = [6] << [7] 138875005 SNOMED CT Concept

sC:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
Group:
[4] << [5] 762705008 Concept model object attribute = [6] << [7] 138875005 SNOMED CT Concept

sD:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
Group:
[4] << [5] 762705008 Concept model object attribute = [6] << [7] 138875005 SNOMED CT Concept
Group:
[8] << [9] 762705008 Concept model object attribute = [10] << [11] 138875005 SNOMED CT Concept

sE:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
Group:
[4] << [5] 762705008 Concept model object attribute = [6] << [7] 138875005 SNOMED CT Concept
Group:
[8] << [9] 762705008 Concept model object attribute = [10] << [11] 138875005 SNOMED CT Concept
Group:
[12] << [13] 762705008 Concept model object attribute = [14] << [15] 138875005 SNOMED CT Concept

sF:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
Group:
[4] << [5] 762705008 Concept model object attribute = [6] << [7] 138875005 SNOMED CT Concept
Group:
[8] << [9] 762705008 Concept model object attribute = [10] << [11] 138875005 SNOMED CT Concept
Group:
[12] << [13] 762705008 Concept model object attribute = [14] << [15] 138875005 SNOMED CT Concept
Group:
[16] << [17] 762705008 Concept model object attribute = [18] << [19] 138875005 SNOMED CT Concept

sG:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
[4] << [5] 138875005 SNOMED CT Concept
Group:
[6] << [7] 762705008 Concept model object attribute = [8] << [9] 138875005 SNOMED CT Concept

sH:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
[4] << [5] 138875005 SNOMED CT Concept
Group:
[6] << [7] 762705008 Concept model object attribute = [8] << [9] 138875005 SNOMED CT Concept
[10] << [11] 762705008 Concept model object attribute = [12] << [13] 138875005 SNOMED CT Concept

sI:
Supertype:
[2] << [3] 138875005 SNOMED CT Concept
[4] << [5] 138875005 SNOMED CT Concept
Group:
[6] << [7] 762705008 Concept model object attribute = [8] << [9] 138875005 SNOMED CT Concept
[10] << [11] 762705008 Concept model object attribute = [12] << [13] 138875005 SNOMED CT Concept
[14] << [15] 762705008 Concept model object attribute = [16] << [17] 138875005 SNOMED CT Concept

sS:
creates a clause like sA, but of the form:
Supertype:
[2] ^ [3] SET_S Local set S
[4] << [5] 71388002 Procedure [or whatever is the Snapshot focus - if a refset then the default operator is '^'].

This means that if you fill perform any import (e.g. 'g', 'h' or 't') and name S as the target, its members will be referenced in the first row.
The second row is pulled in from the current Snapshot focus set at the time of the 'sS' instruction.
Doing this can therefore (at a concept level) emulate a search filtered on a concept type (but is (with effort) more configurable).
To filter on multiple types (with AND logic) add more rows ('d4'...).
To filter on multiple types (with OR logic) add this pair to a new constraint set ('C*'), then build more, add them ('C+') and then run the constraint ('F').

sX, sY and sZ:
sX & sY: as for sC, then fills the destinationId and term with details of the Snapshot focus. The constraint can be further edited, but the default is equivalent to 'what are the role references to the Snapshot focus?'
If the Snapshot focus is a concept model attribute, it is the attribute name that is pre-populated.
By running 'q+' an attribute name-delimited report is produced.
sZ sets the value operator to '', sX sets it to '<<'.
sY: instead of assigning the value directly, this wraps up the value in a set (set 'Y'). This makes it quicker to run 'what are the role references to the Snapshot focus?' questions repeatedly:
sY + (q hY, b)* [overwrites members of Y with next layer of sourceIds]
sY + (q hX, nXYY, b)* [progressively adds to Y]
see e.g. 734679000 Thyroid hormone 

To exit help page:
Press '1' to load Snapshot focus...