[v] (0-2) Set supers

Adjusts the supertype(s) of the imported probe:

v0: uses the focus of the selected probe concept.
v1: uses distributed parents of the selected probe.
v2: uses the calculated PPS of the selected probe (occasionally this differs from the true stated PPS)

To exit help page:
Press '1' to load Snapshot focus...