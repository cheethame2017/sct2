[M] Read in file

Reads file of identifiers into a lettered set. Well-structured lists of codes can be read in, and in general unstructured text that contains ids embedded can also be processed.

Full syntax:

M + letter + full path

example

'MA/home/ed/Desktop/test.txt'

...will read identifiers detected in /home/ed/Desktop/test.txt into set A, where the content of test.txt may look like:

"129125009
363661006
49062001
363661006
123037004
49755003
129264002
..."

or

"...For example, the postcoordinated expression above can be transformed using a set of standard rules to the following "normal form expression" which enables comparison with similar concepts.

64572001 | disease | :
   246075003 | causative agent | = 47448006 | hot water |
 , 363698007 | finding site | = ( 83738005 | index finger structure | :
          272741003 | laterality | = 7771000 | left | )
 , { 116676008 | associated morphology | = 80247002 | third degree burn injury |
 , 363698007 | finding site | = 39937001 | skin structure | }..."

 or

"...Thesaurus-like features are concept–term relations such as the synonymous descriptions "Acute coryza", "Acute nasal catarrh", "Acute rhinitis", "Common cold" (as well as Spanish "resfrío común" and "rinitis infecciosa") for the concept 82272006..."

Minimal syntax: 

Simply entering 'M' will (a) look for a file ('read.txt') in the 'BASIC_DATA' path (e.g. /home/ed/Data/) and (b) put any codes found into a set with letter 'M'.
Simply entering 'M' and a set letter will look for the same default file but put it into the chosen set letter (e.g. 'MA').

Note MATags or MAtags will read the latest calculated semantic tags file into A.
Note MAModel or MAmodel will read the MRCM domain and range classes into A.

If duplicates are included these are not immediately removed, but can be by:

1. JA= (build a definition constraint)
2. F (run the constraint)
3. hA (overwrite A with distinct membership of resulting set - or could be saved to a new set, e.g. hB)

False positive identifiers (satisfying the morphology of an identifier but not apparently a member of the SCT Edition tested) will be added to set Z.

To exit help page:
Press '1' to load Snapshot focus...