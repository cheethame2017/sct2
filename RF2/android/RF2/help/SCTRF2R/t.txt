[t] (+A..) Refset > set

Puts the concepts returned after a refset member listing [m], or an ancestor/descendant listing [q/r] is run in the Snapshot Data Browser into a letter-identified set.

Selecting 'tA' in the ECL and Sets window will store the concepts from the Snapshot Data Browser list as a set that can then be referred to later as 'A'.
The letters are not significant, the same list could be stored as 'a' or 'B' or '1' using 'ta', tB' or 't1' respectively, however the list must be named with only one character (not longer).

Producing such a set allows:

- Comparison with other sets ([m], [n] and [o] support AND, OR and NOT binary logic on two such sets - +/- deawing the comparison result)
- Inclusion in constraints as focus concepts or as refinement values (using [z]).
- Inverting an object:attribute=value set to identify the values used (using [p]).
- Drawing a basic subgraph diagram of a set (using [w]).
- Later retrieving and viewing a set (using [u]).

To exit help page:
Press '1' to load Snapshot focus...