[J] (+A) Conv set->constr

Converts a code set to a constraint set.

Generate or import a set of codes and assign them to a letter.

[JA] then converts each member of the code set A to a 'single code' constraint set clause.

[JA=] processes the entire set and attempts to refactor the code set A as a set of <<, < and MINUS clauses - qualifying clauses will always have more hits than exceptions

[JA+] allows this process to run for slightly longer (generating more small compound clauses)

[JA~] Detects navigational groupers (and returns a list of these) rather than a constraint set. See O and P.
[JA@] Detects navigational groupers (and returns a list of these) rather than a constraint set (automatically includes 'disease' and 'morph abno' if relevant). See O and P.
[JA]] As for [JA~] and [JA[] As for [JA@] but cycling through a sequence of slice values (25,10,5,2) rather than set via P.

[JA#] adds an extra specification step (slower, but slight improvement in specificity of clause creation)

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...