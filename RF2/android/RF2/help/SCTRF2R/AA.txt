[A] (+A..) Focus > set

A laborious process, but allows the piecemeal modification of a letter-named set.

For example, AB will create a new 'B' code set and add to it the current Snapshot focus concept.
Then:
AB+ would add the Snapshot focus to this set (so B now has new members)
AB- would remove the Snapshot focus from this set (so B now has one fewer members)

AB* will recursively expand any defined values.

AB# and AB] are macros of:
 AB  - add snaphot focus to B
 NB* - expand all attribute values in B
 then:
 AB#: BB> - draw the resulting network with edges flowing L-R
 AB]: BB^ - draw the resulting network with edges flowing B-T

AB' and AB[ are macros of:
 AB* - recursively expand any defined values into B
 then:
 AB': BB> - draw the resulting network with edges flowing L-R
 AB[: BB^ - draw the resulting network with edges flowing B-T

A# resets conpicSet to root only.

 The resulting diagram will be influenced by snapshot settings for B,C and O (although AB] and AB# force B2, C1 (unless already C3) and O11).

Note: a slightly easier mechanism to remove set members is to create a basic Constraint Set [J + set letter] and then delete each unwanted clause with [E + number].
Rerun the Constraint Set with [F] and then assign the result back to the original set letter [h + letter].

To exit help page:
Press '1' to load Snapshot focus...