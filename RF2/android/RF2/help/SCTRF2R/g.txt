[g] (+A..) Search > set

Puts the concepts returned in a Snapshot search list into a letter-identified set.

For example, a search for 'sjogren syndr' will produce a list of terms in the Snapshot Data Browser.
Selecting 'gA' in the ECL and Sets window will store the concepts this list represents as a set that can then be referred to later as 'A'.
The letters are not significant, the same list could be stored as 'a' or 'B' or '1' using 'ga', gB' or 'g1' respectively, however the list must be named with only one character (not longer).

Producing such a set allows:

- Comparison with other sets ([m], [n] and [o] support AND, OR and NOT binary logic on two such sets - +/- deawing the comparison result)
- Inclusion in constraints as focus concepts or as refinement values (using [z]).
- Inverting an object:attribute=value set to identify the values used (using [p]).
- Drawing a basic subgraph diagram of a set (using [w]).
- Later retrieving and viewing a set (using [u]).

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...