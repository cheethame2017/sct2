[I] Save constr set

Saves the active constraint set, either as an existing constraint or as a new named constraint set:

Preliminaries:

Check that you have the right constraint set current by clicking [D]
Check that you are in the 'show list of constraint sets' screen by clicking [G]

Then, for example, either:

[I3] saves the current constraint set into slot 3 (which already has a name)
[I my new constraint set] saves the current constraint set into a new slot and gives it the name you have chosen.

[I-3] removes constraint set number 3.

Notes:
Only use a simple character set. Spaces are replaced by underscores.

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...