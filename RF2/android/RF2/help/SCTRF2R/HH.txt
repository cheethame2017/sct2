[H] Load constr set

Shows a chosen constraint set's constraint clauses.

Preliminaries:

Check that you are in the 'show list of constraint sets' screen by clicking [G]

Then:

For example, click [H3] to show constraint set 3.

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...