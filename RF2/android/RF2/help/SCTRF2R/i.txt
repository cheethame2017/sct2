[i] (+no) Change id

replaces the index id with the focus id or an item from the displayed snapshot view by index.

for example if we have the concept 66754008	| Appendix structure as the focus in the Snapshot pane:

Parents: [1]
[1]	245424001		Region of large intestine

Focus:
[2]	66754008		Appendix structure

Children: [13]
[3]	+45679000		Appendiceal lymphoid nodule
[4]	+57775008		Appendiceal mucous membrane structure
[5]	+7242000		Appendiceal muscularis propria
[6]	+22488003		Appendiceal serosa

...and a 'role row' of the form in the ECL & Sets pane:
[14]  [15] 762705008 Concept model object attribute = [16] << [17] 138875005 SNOMED CT Concept

...then entering 'i17' will change the constraint to:
[14]  [15] 762705008 Concept model object attribute = [16] << [17] 66754008	Appendix structure

The same substitution can be achieved after selecting a concept by index in the ECL & Sets pane, navigating to a new value, reloading the constraint ('b') and then entering 'i' plus the index of the value to change.

Items can also be selected by index number:

For example, selecting 7242000 Appendiceal muscularis propria above can be achieved by entering 'i17-7' in the ECL pane (that is, a 'dash' followed by the index number).
This approach is useful for inspecting/making use of membership of multiple simple reference sets.

To exit help page:
Press '1' to load Snapshot focus...