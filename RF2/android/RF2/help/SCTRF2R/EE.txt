[E] (+no) Rmv constr

With the current constraint displaying (see 'D' or after running a code set -> constraint set session  - see 'J'):

E with the index number of a constraint clause will remove that clause. For example, 'E3' will remove clause 3.

To exit help page:
Press '1' to load Snapshot focus...