[u] Basic help

Shows basic help file, the same as '--'

All other help files can be found by prefixing a case sensitive action letter with a single '-'.
For example, help about [e] Caption (1/0) can be shown by entering '-e'

To exit help page:
Press 'a' to load MRCM domain list...
