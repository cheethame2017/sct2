[h] Dom/ran size (0-5)

Sets whether domains are sized according to member count in the Pre-coordinated diagram.

With edges too:
h0 - no sizing
h1 - sizing by font enlargement (linear)
h2 - sizing by shape and font enlargement (more 'square')
h3 - not used
No edges (very distorted in browser view of svg):
h4 - sizing by shape and font enlargement
h5 - sizing by shape and font enlargement, Mondrian colour scheme.

h4/5 easiest to run with x1 set.

To exit help page:
Press 'a' to load MRCM domain list...
