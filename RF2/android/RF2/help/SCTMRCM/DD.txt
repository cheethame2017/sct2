[D] (+row) Dom constr

DN shows rows from the 723560006 MRCM domain international reference set.

For example, if the Procedure domain has index number 15, D15 will return the structured set of rows from the refset (Domain constraint, parent domain etc.)

To exit help page:
Press 'a' to load MRCM domain list...
