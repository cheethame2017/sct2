[j] Constr. ops (1/0)

Show (j1) or hide (j0) the constraint operators for domains and ranges.

In general it makes more sense to display constraint operators (<<, <), but hiding them can simplify the diagram and conceal operator differences between constraints specified as both domains and ranges.

To exit help page:
Press 'a' to load MRCM domain list...
