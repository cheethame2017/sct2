[x] Only atts (0-2)

Essentially part of a routine data build - generates a file of the attributes referenced in the concept model.

The list generated is used in the Snapshot browser diagramming and concrete domain routines.

Also supports a rebuild of the same file if becomes corrupted.

x0 and x1 unset and set the flag for the following sequence:

Build/rebuild sequence:
Enter 'v' (Reset all)
Enter 'bb' (Include all domains)
Enter 'x1' (Limit processing to 'only attributes')
Enter 'd' (Run)

x2 is a macro that runs the sequence directly.

To exit help page:
Press 'a' to load MRCM domain list...
