[A] (+row) Ref->xlsx

ANN

Generates an xlsx (in files/xlsx) report showing how membership has changed over time.

Needs the configuration settings to be [e] Simple snapshot and [g] Show active data.

A temporary screen showing the same output is shown at the end of calculations.

Key:

Alpha-ordered members in rows, dates in columns.
Empty cell = concept never in the data.
# purple: Defined member
+ blue: Primitive member
-- green: Inactive (and not member - either before or after period of ancestry)
- red: Inactive member (may be an error or deliberate)
. orange: Active elsewhere - not a member at that time.

To exit help page:
Press '1' to load Full/SV browser focus...