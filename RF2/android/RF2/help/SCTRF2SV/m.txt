[m] Latest terms

Uses terms from the latest snapshot data (for speed) rather than calculating them dynamically based on ET.

This can therefore result in 'incorrect' preferred terms being displayed.

See also [n].

To exit help page:
Press '1' to load Full/SV browser focus...