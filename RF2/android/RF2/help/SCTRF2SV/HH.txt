[H] (+row) Desc->xdot

HNN

Generates a concept descendant diagram for the index concept at the effectiveTime (output as contc.dot)

As with the snapshot data, care is needed to avoid trying to draw a uselessly large tree.

To exit help page:
Press '1' to load Full/SV browser focus...