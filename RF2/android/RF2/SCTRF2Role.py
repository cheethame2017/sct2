from __future__ import print_function, unicode_literals
import io
import copy
import textwrap
import csv
from itertools import permutations
from SCTRF2config import tokDict, cfg, cfgWrite, pth
from SCTRF2RoleHelpers import orderAndCountRoleRows_IN, starterObjectSet_IN, checkEachRow_IN, listToInString, readFileMembers
from SCTRF2Helpers import descendantsIDFromConId_IN, listify, tuplify, pterm, descendantsCleanIDFromConId_IN, TCCompare_IN, \
                        groupedRolesFromConId_IN, allRolesFromConId_IN, ancestorsFromConId_IN, PPSFromAncestors_IN, \
                        refSetFromConId_IN, parentsFromId_IN, childrenFromIdWithPlus_IN, tabString, descendantCountFromConId_IN, \
                        simplify, ancestorsIDFromConIdList_IN, ancestorsIDFromConId_IN, AND, OR, NOT, \
                        refSetMembersCompareFromConIds_IN, proportionColor_IN, descendantsFromConId_IN, \
                        refSetChaptersRefTableFromConIds_IN, activeColor_IN, conMetaFromConId_IN, historicalDestinationsFromConId_IN, \
                        fsnFromConId_IN, isActiveFromConId_IN, naturalSort, fasterRolesFromConId_IN, onlyDefTermsFromConId_IN, \
                        get_power_set, refSetMembersFromConId_IN, refSetTypeFromConId_IN
from SCTRF2Funcs import domainOrRangeGen, readInZ, toTempList, clean, readFocusValue, parsablePretty, snapshotRoleHeader, checkObjValueColor, \
                        focusSwapWrite, dotDrawFocus, append_distinct, writeRoleAndDotFiles, drawPics, dotDrawTreeMapIsh, \
                        resetRoleAndPreDot, dotDrawAncestors, sorted_cfg, growValues, hasDef, toString, \
                        splitTag, getIdsFromLines
from SCTRF2Subgraph import setToGraph, adjacencyBuild, setToGraphAugmentTest
from platformSpecific import clearScreen, cfgPathInstall, sep, removeConstraintSet, copyBackNavFiles, deleteNavFolder, saveNavFiles, \
                        roleToSetReportFile, input2_3, renameDiagramFile
# CONC HEADER
from SCTRF2Concrete import concreteTakingRoles, concValueSet

tagList = readFileMembers(cfgPathInstall() + 'files' + sep() + 'misc' + sep() + 'tagList.txt')
tok = tokDict()

def refsetListLite():
    returnList = ['simplerefset',
					'srefset']

    return returnList

# CONC - menu item in matrix
# see item [L]
def promptString(inShow):
    if inShow == 1:
        indexDict = {'a': 'Probe from Snap',
                    'b': 'Reload probe',
                    'c': '(+no) Rmv (\'+\'ovr)',
                    'd': '(+no) Add row',
                    'e': '(+no) Add ungp row',
                    'f': '(+no) Add gp row',
                    'g': '(+A..) Search > set',
                    'h': '(+A..) Result > set',
                    'i': '(+no) Change id',
                    'j': '(+no) to << ',
                    'k': '(+no) to <   ',
                    'l': '(+no) to = (\'#\'^)',
                    'm': '(+A..)x3 AND ',
                    'n': '(+A..)x3 OR ',
                    'o': '(+A..)x3 NOT',
                    'p': '(+no + A..) O->A=V',
                    'q': 'Process constraint',
                    'r': 'Read back report',
                    's': '(+A..) Tpl. probe',
                    't': '(+A..) Ref/TC > set',
                    'u': '(+A..) Read set',
                    'v': '(0-2) Set supers',
                    'w': '(+A..) Draw set',
                    'x': 'Breadcrumbs',
                    'y': '(+no) Constraint',
                    'z': '(+no + A..) Set',
                    'A': '(+A..) Focus > set',
                    'B': '(+A..) Draw defs',
                    'C': 'Constr > set(*/+/-)',
                    'D': 'Show constr set',
                    'E': '(+no) Rmv constr',
                    'F': 'Process constr set',
                    'G': 'List constr sets',
                    'H': 'Load constr set',
                    'I': 'Save constr set',
                    'J': '(+A) Conv set->constr',
                    'K': '(+A) Set v. RefSet',
                    'L': concValueSet(),
                    'M': 'Read in file',
                    'N': '(+A+/-) Expand',
                    'O': 'Only def\'d (1/0)',
                    'P': '(+no) Nav. slice',
                    'Q': 'Demo Y/N (1/0)',
                    'R': 'List navig sets',
                    'S': 'Load navig set',
                    'T': 'Save navig set',
                    'U': '(+A..) Group by mod',
                    'V': '(+A..) UpSetPlot'}
        indexDictLetters = list(indexDict)
        promptString = 'Action:\n\033[36m'
        counter = 1
        if cfg("TAB_INDENT") == 0:
            for letter in indexDictLetters:
                promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 3 == 0 else '\t')
                counter += 1
        else:
            for letter in indexDictLetters:
                promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 4 == 0 else '\t')
                counter += 1
        promptString += '\n[zz] Exit\t\t[-] (+letter) help\n\n\033[95;7mInput selection:\033[92;27m '
    else:
        promptString = '\033[95;7mInput selection:\033[92;27m '
    return promptString

def rowHeaderPrint(row): # determines each 'section' of structured constraint for display
    if row[2] == "FOCUS":
        return ["Supertype:", 0]
    else:
        if (row[3] == 0) or (row[0] == "NTC"):
            return ["Ungrouped:", 0]
        else:
            return ["Group:", row[3]]
    return "Header"

def probeGroupedRoles(idIn, predList, printBool): # determines and prints each section of structured constraint, and builds dictionary for return to SCTRF2R
    mydict = {}
    myJoinList = []
    mycounter = 1
    rowHeaderList = ["", 0]
    testHeaderList = []
    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    if printBool:
        print("[a]/[v]0-2 to import snapshot focus as probe ECL, [1] to select.\nCurrent ECL:")
        print()
    for row in predList:
        myJoinDict = {}
        myJoinDictList = []
        testHeaderList = rowHeaderPrint(row)
        if not(rowHeaderList == testHeaderList):
            if printBool:
                print(testHeaderList[0])
            rowHeaderList = testHeaderList
        if row[5] == 3:
            mycounter += 1
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[4][0])))))
            myJoinDictList.append(mycounter)
            mycounter += 1
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[4][1])))))
            myJoinDictList.append(mycounter)
            myJoinDict.update(list(zip(listify(row[1]), listify(myJoinDictList))))
            if printBool:
                print('[' + str(mycounter - 1) + ']', row[4][0], '[' + str(mycounter) + ']', row[4][1], row[4][2])
        elif row[5] == 5:
            mycounter += 1
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[4][0])))))
            myJoinDictList.append(mycounter)
            mycounter += 1
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[4][1])))))
            myJoinDictList.append(mycounter)
            mycounter += 1
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[4][3])))))
            myJoinDictList.append(mycounter)
            mycounter += 1
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[4][4])))))
            myJoinDictList.append(mycounter)
            myJoinDict.update(list(zip(listify(row[1]), listify(myJoinDictList))))
            if printBool:
                print('[' + str(mycounter - 3) + ']', row[4][0], '[' + str(mycounter - 2) + ']', row[4][1], row[4][2] + " = " + '[' + str(mycounter - 1) + ']', row[4][3], '[' + str(mycounter) + ']', row[4][4], row[4][5])
        myJoinList.append(myJoinDict)

    mydict.update(list(zip(listify(0), listify([myJoinList, predList]))))
    if printBool:
        print()
    return mydict

def splitOperator(inList):
    outList = []
    # first id (A=V, O)
    if len(inList[0].split(" ")) == 1:
        outList.append("")
        outList.append(inList[0])
    else:
        outList.append(inList[0].split(" ")[0])
        outList.append(inList[0].split(" ")[1])
    outList.append(inList[1])
    # later id (A=V)
    if len(inList) == 5:
        if len(inList[2].split(" ")) == 1:
            outList.append("")
            outList.append(inList[2])
        else:
            outList.append(inList[2].split(" ")[0])
            outList.append(inList[2].split(" ")[1])
        outList.append(inList[3])
    return outList

def orderAndCountRoleRows(inRowList): # controller for testing each row's return count; inrowList only has AND logic
    idList = []
    clearScreen()
    snapshotRoleHeader()
    writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
    print("1 of 3: Preparing sequence")
    idList = orderAndCountRoleRows_IN(inRowList)
    return idList

def processRoleTxt(inPath): # generates structured constraint from ECL persistence form
    inLines = []
    outLines = []
    inGroup = False
    groupCounter = 0
    lineCounter = 0
    tempLine = ""

    try:
        f = io.open(inPath, 'r', encoding="UTF-8")
        # focus
        inLines = f.readlines()
    finally:
        f.close()
    if len(inLines) > 0:
        for line in inLines:
            tempList = []
            if "{" in line:
                inGroup = True
                groupCounter += 1
            if not inGroup:
                if lineCounter == 0:
                    tempList = toTempList(tempList, tempLine, str(lineCounter), "FOCUS", str(groupCounter), splitOperator([item.strip() for item in line.strip().split("|")]), len(line.strip().split("|")))
                else:
                    tempLine = "NTC"
            if lineCounter != 0:
                if len(line.strip().split("|")) == 5:
                    tempList = toTempList(tempList, tempLine, str(lineCounter), inGroup, str(groupCounter), splitOperator([clean(item) for item in line.strip().split("|")]), len(line.strip().split("|")))
                else:
                    # CONC - modify/pad concrete value when reading structured constraint from file
                    initialTempList = splitOperator([clean(item) for item in line.strip().split("|")])
                    if initialTempList[1] in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
                        initialTempList.append("") # pad 'operator'
                        tempConc = line.strip().split("|")[2] # prepare conc value
                        tempConc = tempConc.replace(" = ", "")
                        tempConc = tempConc.replace("}", "")
                        tempConc = tempConc.replace(",", "")
                        initialTempList.append(tempConc) # add concrete value
                        initialTempList.append("") # pad 'term'
                        tempList = toTempList(tempList, tempLine, str(lineCounter), inGroup, str(groupCounter), initialTempList, 5)
                    else:
                        tempList = toTempList(tempList, "", str(lineCounter), "FOCUS", str(groupCounter), splitOperator([clean(item) for item in line.strip().split("|")]), len(line.strip().split("|")))
            if "}" in line:
                inGroup = False
            lineCounter += 1
            tempLine = ""
            outLines.append(tempList)
    else:
        outLines.append(['', '0', 'FOCUS', '0', ['', tok["ROOT"], pterm(tok["ROOT"])], 3]) # protects against trying to load inactive concept
    return outLines

def truthTesterGroup(inList): # tests set of true/false decisions within a particular 'group' to determine if all true or not.
    trueCounter = 0
    for PS in inList:
        if PS[1]:
            trueCounter += 1
    if trueCounter == len(inList):
        return True
    else:
        return False

def truthTesterGroup2(inList): # tests set of true/false decisions within a particular 'group' to determine if all true or not.
    trueCounter = 0
    for PS in inList:
        if PS[0][1]:
            trueCounter += 1
    if trueCounter == len(inList):
        return True
    else:
        return False

def truthTesterAll(inList): # tests set of true/false decisions to determine if all true or not.
    trueCounter = 0
    for PS in inList:
        if PS:
            trueCounter += 1
    if trueCounter == len(inList):
        return True
    else:
        return False

def testSupertypes(predicateSupertypes, candidateSuperSet, candidateId): # tests set of supertype candidates against a predicate set
    PSTestList = []
    for predicateSupertype in predicateSupertypes:
        PSTestList.append([predicateSupertype, False])
    for PS in PSTestList:
        if PS[0][0] == "^":
            # check membership
            if PS[0][1][0:4] == "SET_":
                myList = []
                myList = readFileMembers(cfgPathInstall() + 'sets' + sep() + PS[0][1][4] + '.txt')
                if candidateId in myList:
                    PS[1] = True
            else:
                refsetType = refsetListLite()
                for rType in refsetType:
                    refs = refSetFromConId_IN(candidateId, rType)
                    refIds = [r[0] for r in refs]
                    if PS[0][1] in refIds:
                        PS[1] = True
        else:
            for candidateSuper in candidateSuperSet:
                # idistinction between <<, < and = already done so only need to test subsumption now
                if (TCCompare_IN(candidateSuper[3], PS[0][1])) or (candidateId == PS[0][1]):
                    PS[1] = True
        if truthTesterGroup(PSTestList):
            return True
    return False

def testRoles(rowsInGroupToTest, candidateRoles, myGroupNoList): # tests set of role candidates against a predicate set
    allPred = []
    roleTestList = []
    for row in rowsInGroupToTest:
        if not row[0] == '-1':
            roleTestList.append([row, False])
    candidateRoleNumberList = []
    candidateRoleRowList = []
    for candidateRole in candidateRoles:
        if candidateRole[0] == "*":
            candidateRoleNumberList = list(candidateRole[1])
        else:
            candidateRoleRowList.append(candidateRole)
    groupCounter = 0
    # need to ensure that a 'group 0 group' (for old data) cannot satisfy a 'non-0 group'
    CTRoles = concreteTakingRoles()
    concVals = cfg("CONC_VALS")
    concDat = cfg("CONC_DATA")
    for groupNo in myGroupNoList:
        predToTest = [row for row in roleTestList if row[0][0] == groupNo]
        candToTest = [row for row in candidateRoleRowList if row[0] == candidateRoleNumberList[groupCounter]]
        for PTT in predToTest:
            for CTT in candToTest:
                if TCCompare_IN(CTT[1], PTT[0][1][1]):
                    if PTT[0][1][3] == "^":
                        # check membership
                        if PTT[0][1][4][0:4] == "SET_":
                            myList = []
                            myList = readFileMembers(cfgPathInstall() + 'sets' + sep() + PTT[0][1][4][4] + '.txt')
                            if CTT[3] in myList:
                                PTT[1] = True
                        else:
                            refsetType = refsetListLite()
                            for rType in refsetType:
                                refs = refSetFromConId_IN(CTT[3], rType)
                                refIds = [r[0] for r in refs]
                                if PTT[0][1][4] in refIds:
                                    PTT[1] = True
                    else:
                        # CONC - convert #number to number and then compare pred/cand values when testing query
                        if (concVals == 1 or concDat == 1) and PTT[0][1][1] in CTRoles:
                            if CTRoles[PTT[0][1][1]] == 0:
                                if "." in CTT[3]:
                                    candConc = float(CTT[3].replace("#", ""))
                                else:
                                    candConc = int(CTT[3].replace("#", ""))
                                if "." in PTT[0][1][4]:
                                    predConc = float(PTT[0][1][4].replace("#", ""))
                                else:
                                    predConc = int(PTT[0][1][4].replace("#", ""))
                            else: # string
                                candConc = CTT[3]
                                predConc = PTT[0][1][4]
                            if PTT[0][1][3] == "<":
                                if candConc <= predConc: # compare as less than or equal to
                                    PTT[1] = True
                            elif PTT[0][1][3] == ">": # compare as greater than or equal to
                                if candConc >= predConc:
                                    PTT[1] = True
                            elif PTT[0][1][3] == "": # compare as equal to
                                if candConc == predConc:
                                    PTT[1] = True
                        else:
                            # distinction between <<, < and = already done so only need to test subsumption now
                            if TCCompare_IN(CTT[3], PTT[0][1][4]):
                                PTT[1] = True
        groupCounter += 1
        if truthTesterGroup(predToTest):
            allPred.append(True)
        else:
            allPred.append(False)
    if truthTesterAll(allPred):
        return True
    else:
        return False
    print()

def testOnlySupertypes(inList): # checks whether there are only supertypes to test in a structured predicate
    focusCounter = 0
    for item in inList:
        if item[2] == "FOCUS":
            focusCounter += 1
    if focusCounter == len(inList):
        return True
    else:
        return False

def testOnlyOneRole(inList): # checks whether there are only supertypes to test in a structured predicate
    focusCounter = 0
    for item in inList:
        if item[2] == "FOCUS":
            focusCounter += 1
    if focusCounter + 1 == len(inList):
        return True
    else:
        return False

def redunCheck(rowsInGroupToTest, myGroupNoList):
    # convert rows to original processed format!
    # originally written to run later in cycle, so rowsInGroupToTest
    # list slightly different format.
    # eventually can rewrite the middle code, but for now
    # the 'swap' converters at the start and finish are simpler work-around.
    rowsToProcess = []
    swapFlag = False
    if rowsInGroupToTest[0][0] == '':
        # remove zero
        myGroupNoList.remove('0')
        swapFlag = True
        for item in rowsInGroupToTest:
            tempRow = []
            if item[2] == 'FOCUS':
                tempRow.append('-1')
            elif item[0] == 'NTC':
                tempRow.append('0')
            else:
                tempRow.append(item[3])
            tempRow.append(item[4])
            tempRow.append(item[0])
            tempRow.append(item[2])
            tempRow.append(item[5])
            rowsToProcess.append(tempRow)
    else:
        for item in rowsInGroupToTest:
            rowsToProcess.append(item)
    groupNoList = []
    outGroupNoList2 = []
    outRowsGroupToTest2 = []
    for item in myGroupNoList:
        groupNoList.append(item)
    compareList = []
    for groupNo in myGroupNoList:
        compareListBlock = []
        compareListBlock.append(groupNo)
        for row in rowsToProcess:
            compareListTemp = []
            if groupNo == row[0]:
                compareListTemp.append([row[1], False])
                compareListBlock.append(compareListTemp)
        compareList.append(compareListBlock)
    RGSequences = []
    RGSequences = list(permutations(groupNoList, 2))
    for seq in RGSequences:
        seqBool0 = False
        seqBool1 = False
        Group0 = [item for item in compareList if item[0] == seq[0]]
        Group1 = [item for item in compareList if item[0] == seq[1]]
        for item in Group0[0][1:]:
            item[0][1] = False
        for item in Group1[0][1:]:
            item[0][1] = False
        seqBool0, seqBool1 = groupCompare(Group0, Group1)
        if (seqBool0 and seqBool1) or (seqBool0 and (len(Group0[0]) < len(Group1[0]))) or (seqBool1 and (len(Group1[0]) < len(Group0[0]))):
            Group0, Group1 = groupBlend(Group0, Group1)
    survivingList = []
    for group in compareList:
        if not group[1][0][0] == ['', tok["CONMODOBJ_ATTRIB"], pterm(tok["CONMODOBJ_ATTRIB"]), '^', tok["ROOT"], pterm(tok["ROOT"])]:
            survivingList.append(group)
    outRowsGroupToTest2 = [row for row in rowsToProcess if (row[0] == '-1') or (row[0] == '0')] # add supertype and ungrouped (untested) rows
    groupCounter = 1
    for group in survivingList:
        outGroupNoList2.append(str(groupCounter))
        for row in group[1:]:
            outRowsGroupToTest2.append([str(groupCounter), row[0][0], '', True, 5])
        groupCounter += 1

    # convert back to return format!
    if swapFlag:
        swapList = []
        counter = 0
        for item in outRowsGroupToTest2:
            tempList = []
            tempList.append(item[2])
            tempList.append(str(counter))
            tempList.append(item[3])
            if item[0] == '-1':
                tempList.append('0')
            else:
                tempList.append(item[0])
            tempList.append(item[1])
            tempList.append(item[4])
            swapList.append(tempList)
            counter += 1
        # add back the zero
        outGroupNoList2.insert(0, '0')
        return swapList, outGroupNoList2
    else:
        return outRowsGroupToTest2, outGroupNoList2

def groupBalance(Group0, Group1):
    # used to pad smaller group for comparision purposes - dodgy logic.
    groupDiff = len(Group0[0]) - len(Group1[0])
    absGroupDiff = abs(groupDiff)
    if absGroupDiff != 0:
        n = 1
        while n <= absGroupDiff:
            if groupDiff > 0:
                Group1[0].append([[['<<', tok["CONMOD_ATTRIB"], pterm(tok["CONMOD_ATTRIB"]), '<<', tok["ROOT"], pterm(tok["ROOT"])], True]])
            else:
                Group0[0].append([[['<<', tok["CONMOD_ATTRIB"], pterm(tok["CONMOD_ATTRIB"]), '<<', tok["ROOT"], pterm(tok["ROOT"])], True]])
            n += 1
    return Group0, Group1

def groupBlend(Group0, Group1):
    Group0counter = 1
    for item0 in Group0[0][1:]:
        item0Attrow = ['', '0', 'FOCUS', '0', [item0[0][0][0], item0[0][0][1], item0[0][0][2]], 3]
        item0Valrow = ['', '0', 'FOCUS', '0', [item0[0][0][3], item0[0][0][4], item0[0][0][5]], 3]
        Group1counter = 1
        for item1 in Group1[0][1:]:
            item1Attrow = ['', '0', 'FOCUS', '0', [item1[0][0][0], item1[0][0][1], item1[0][0][2]], 3]
            item1Valrow = ['', '0', 'FOCUS', '0', [item1[0][0][3], item1[0][0][4], item1[0][0][5]], 3]
            # test att first
            attBool = all(item in starterObjectSet_IN(item0Attrow) for item in starterObjectSet_IN(item1Attrow))
            # if true, test val
            if attBool:
                # code to protect against returning / testing for <</< root
                if (item0[0][0][4] == tok['ROOT']) and ((item0[0][0][3] == "<<") or (item0[0][0][3] == "<")):
                    valBool = True
                elif (item0[0][0][4] == tok['ROOT']) and item0[0][0][3] == "^": # protect against absurd padding row
                    valBool = False
                elif (item1[0][0][4] == tok['ROOT']) and ((item1[0][0][3] == "<<") or (item1[0][0][3] == "<") or (item1[0][0][3] == "^")): # '^' protects against absurd padding row
                    valBool = False # misses the single true case of testing whether << root subsumes < root
                else:
                    if item0Valrow[4] == item1Valrow[4]:
                        valBool = True
                    else:
                        valBool = all(item in starterObjectSet_IN(item0Valrow) for item in starterObjectSet_IN(item1Valrow))
            else:
                valBool = False # might not be true but attBool false already
            if (attBool) and (valBool):
                # Group0[0][Group0counter][0][0] = Group1[0][Group1counter][0][0]
                Group0[0][Group1counter][0][0] = ['', tok["CONMODOBJ_ATTRIB"], pterm(tok["CONMODOBJ_ATTRIB"]), '^', tok["ROOT"], pterm(tok["ROOT"])] # absurd substitution
            Group1counter += 1
        Group0counter += 1
    return Group0, Group1

def groupCompare(inGroup0, inGroup1):
    # CONC NEEDED - redundant query clause merging - ? how to merge multiple clauses. (all >/</=, what about mixed)
    # how to merge multiple clauses. (all >/</=, what about mixed)
    Group0, Group1 = groupBalance(inGroup0, inGroup1)
    Group0counter = 1
    for item0 in Group0[0][1:]:
        item0Attrow = ['', '0', 'FOCUS', '0', [item0[0][0][0], item0[0][0][1], item0[0][0][2]], 3]
        item0Valrow = ['', '0', 'FOCUS', '0', [item0[0][0][3], item0[0][0][4], item0[0][0][5]], 3]
        Group1counter = 1
        for item1 in Group1[0][1:]:
            item1Attrow = ['', '0', 'FOCUS', '0', [item1[0][0][0], item1[0][0][1], item1[0][0][2]], 3]
            item1Valrow = ['', '0', 'FOCUS', '0', [item1[0][0][3], item1[0][0][4], item1[0][0][5]], 3]
            # test att first
            attBool = all(item in starterObjectSet_IN(item0Attrow) for item in starterObjectSet_IN(item1Attrow))
            # if true, test val
            if attBool:
                # code to protect against returning / testing for <</< root
                if (item0[0][0][4] == tok['ROOT']) and ((item0[0][0][3] == "<<") or (item0[0][0][3] == "<")):
                    valBool = True
                elif (item0[0][0][4] == tok['ROOT']) and item0[0][0][3] == "^": # protect against absurd padding row
                    valBool = False
                elif (item1[0][0][4] == tok['ROOT']) and ((item1[0][0][3] == "<<") or (item1[0][0][3] == "<") or (item1[0][0][3] == "^")): # '^' protects against absurd padding row
                    valBool = False # misses the single true case of testing whether << root subsumes < root
                else:
                    if item0Valrow[4] == item1Valrow[4]:
                        valBool = True
                    else:
                        valBool = all(item in starterObjectSet_IN(item0Valrow) for item in starterObjectSet_IN(item1Valrow))
            else:
                valBool = False # might not be true but attBool false already
            if (attBool) and (valBool):
                Group0[0][Group0counter][0][1] = True
                Group1[0][Group1counter][0][1] = True
            Group1counter += 1
        Group0counter += 1
    return truthTesterGroup2(Group0[0][1:]), truthTesterGroup2(Group1[0][1:])

def testTestList(testList, rowList, myGroupNoList, debugFlag): # tests the set of candidate codes based on ungrouped analysis against the grouped predicates
    returnList = []
    rowsInGroupToTest = []
    onlySupertypes = False
    onlySupertypes = testOnlySupertypes(rowList)
    for groupNo in myGroupNoList:
        for row in rowList:
            if row[2] == "FOCUS":
                if ['-1', row[4]] not in rowsInGroupToTest:
                    rowsInGroupToTest.append(['-1', row[4]])
            elif groupNo == row[3]:
                if row[0] == "NTC":
                    rowsInGroupToTest.append(['0', row[4]])
                else:
                    rowsInGroupToTest.append([groupNo, row[4]])
    if debugFlag:
        print('supertypes to test')
        for row in rowsInGroupToTest:
            if row[0] == '-1':
                print(row)
        print('roles to test')
        for row in rowsInGroupToTest:
            if not row[0] == '-1':
                print(row)
    candidateCounter = 0
    total = len(testList)
    predicateSupertypes = []
    # late check for redundant groups - wrong
    # rowsInGroupToTest, myGroupNoList = redunCheck(rowsInGroupToTest, myGroupNoList)
    # isolate supertype tests
    for predicate in [row[1] for row in rowsInGroupToTest if row[0] == '-1']:
        predicateSupertypes.append(predicate)
    for candidate in testList:
        if ((total - candidateCounter) % 10 == 0) or (total < 20):
            clearScreen()
            snapshotRoleHeader()
            writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
            print("3 of 3: " + '*' * int((total - candidateCounter) * (60 / total)))
        supRoleBool = False
        supBool = False
        for candidateSet in candidate[1:]:
            if not supRoleBool:
                candidateRoles = []
                roleBool = False
                candidateItem = [-1]
                for candidateItem in candidateSet:
                    # test supertypes
                    if candidateItem == -1:
                        supBool = testSupertypes(predicateSupertypes, candidateSet[1:], candidate[0])
                    elif not(candidateItem[0] == -1):
                        candidateRoles.append(candidateItem)
                if candidateItem[0] != -1:
                    roleBool = testRoles(rowsInGroupToTest, candidateRoles, myGroupNoList)
                if (roleBool and supBool) or (supBool and onlySupertypes):
                    supRoleBool = True
        returnList.append([str(supRoleBool), candidate[0], pterm(candidate[0])])
        candidateCounter += 1
    returnList = sorted(returnList, key=lambda x: x[2].lower())
    return returnList

def loadFile(targetSet, filePath):
    inLines = []
    truePos = []
    falsePos = []
    if filePath in ("tags", "Tags"):
        inLines = tagList
    elif filePath in ("model", "Model"):
        domainList, rangeList = domainOrRangeGen()
        inLines = list(set([*domainList, *rangeList]))
    else:
        if filePath == "":
            filePath = pth('BASIC_DATA') + "read.txt"
        try:
            f = io.open(filePath, 'r', encoding="UTF-8")
            for line in f:
                inLines.append(line)
        finally:
            f.close()
    try:
        f = io.open(cfgPathInstall() + 'sets' + sep() + targetSet + '.txt', 'w', encoding="UTF-8")
        truePos, falsePos = getIdsFromLines(inLines, True)
        for line in truePos:
            f.write(line + "\n")
    finally:
        f.close()
    if len(falsePos) > 0:
        try:
            f = io.open(cfgPathInstall() + 'sets' + sep() + 'Z.txt', 'w', encoding="UTF-8")
            for line in falsePos:
                f.write(line + "\n")
        finally:
            f.close()
        return True
    else:
        return False

def writeOutRawECL(inString):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', 'w', encoding="UTF-8")
        f.write(inString[0])
    finally:
        f.close()

def writeOutLines(outLines): # writes structured predicate back to ECL persistence form in text file.
    groupCounter = 0
    closedForUngrouped = False
    stillFocus = True
    firstLineAfterFocus = False
    intoUngrouped = False
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', 'w', encoding="UTF-8")
        for line in outLines:
            # focus row(s)
            if line[2] == 'FOCUS':
                if line[1] == '0':
                    f.write((line[4][0] + " " + line[4][1] + " | " + line[4][2] + " | ").strip())
                else:
                    f.write(" AND \n" + (line[4][0] + " " + line[4][1] + " | " + line[4][2] + " | ").strip())
            elif stillFocus:
                stillFocus = False
                firstLineAfterFocus = True
            # after focus row(s)
            if firstLineAfterFocus:
                firstLineAfterFocus = False
                # ungrouped role
                if int(line[3]) == groupCounter or (not line[2] and not intoUngrouped):
                    CRString = " : \n"
                    intoUngrouped = True
                # grouped role
                elif int(line[3]) > groupCounter:
                    groupCounter = int(line[3])
                    CRString = ": \n{"
                f.write((CRString + (line[4][0] + " " + line[4][1] + " | " + line[4][2] + " | =").strip() + " " + line[4][3] + " " + line[4][4] + " | " + line[4][5] + " |").strip())
            elif not stillFocus:
                # ungrouped role or member of open group
                if int(line[3]) == groupCounter and not line[0] == "NTC":
                    CRString = ",\n"
                elif int(line[3]) == 0:
                    CRString = ",\n"
                # closing grouped role, ungrouped to come
                elif int(line[3]) == groupCounter and line[0] == "NTC" and groupCounter > 0:
                    if not closedForUngrouped:
                        CRString = "},\n"
                        closedForUngrouped = True
                    else:
                        CRString = ",\n"
                # closing grouped role, another grouped to come
                elif int(line[3]) > groupCounter and groupCounter > 0:
                    groupCounter = int(line[3])
                    CRString = "},\n{"
                # opeing first group
                elif int(line[3]) > groupCounter and groupCounter == 0 and not intoUngrouped:
                    groupCounter = int(line[3])
                    CRString = ",\n{"
                f.write((CRString + (line[4][0] + " " + line[4][1] + " | " + line[4][2] + " | =").strip() + " " + line[4][3] + " " + line[4][4] + " | " + line[4][5] + " |").strip())
            if int(line[1]) + 1 == len(outLines) and not line[0] == "NTC" and not line[2] == "FOCUS":
                f.write("}")
    finally:
        f.close()

def probeFromSnap(): # reads in ECL form based on snapshot focus definition (briefly holding as structured) and writes out to temporary form
    rowList = []
    rowList = processRoleTxt(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role.txt')
    # WRITE PROBE - write to intermediate/editable table
    writeOutLines(rowList)

def probeFromTemplate(inPath): # reads in ECL from various template forms (can probably drop)
    rowList = []
    if inPath == 'S':
        focId = readFocusValue()[0]
        rowList.append(['', '0', 'FOCUS', '0', ['^', 'SET_S', 'Local set S'], 3])
        if TCCompare_IN(focId, tok['REFSETS']):
            operatorString = '^'
        else:
            operatorString = '<<'
        rowList.append(['', '1', 'FOCUS', '0', [operatorString, focId, pterm(focId)], 3])
    else:
        if inPath in ("X", "Y", "Z"): # initial setup for 'references' constraint
            rowList = processRoleTxt(cfgPathInstall() + 'files' + sep() + 'roleTemplates' + sep() + 'C.txt')
            # fill destinationId and term of row[1] with snapshot focus details
            focId = readFocusValue()[0]
            if TCCompare_IN(focId, tok["CONMOD_ATTRIB"]): # if an attribute, template attribute name
                if inPath == 'Z':
                    rowList[1][4][0] = ''
                rowList[1][4][1] = focId
                rowList[1][4][2] = pterm(focId)
            else: # if not, assume a value
                if inPath == 'Z':
                    rowList[1][4][3] = ''
                if inPath in ("X", "Z"):
                    rowList[1][4][4] = focId
                    rowList[1][4][5] = pterm(focId)
                elif inPath == "Y": # put the code into a set (Y)
                    readInOneWriteOne('files' + sep() + 'snap' + sep() + 'focusSwap.txt', "Y", "Focus to " + "Y", 0, False, True)
                    clearScreen()
                    snapshotRoleHeader()
                    rowList[1][4][3] = '^'
                    rowList[1][4][4] = "SET_Y"
                    rowList[1][4][5] = "Local set Y"
        else:
            rowList = processRoleTxt(cfgPathInstall() + 'files' + sep() + 'roleTemplates' + sep() + inPath + '.txt')
    # WRITE PROBE - write to intermediate/editable table
    writeOutLines(rowList)

def refreshConstraint(rowList): # refreshes persisted ECL form in text file after various updates
    writeOutLines(rowList)
    # READ WORKING PREDICATE - read in intermediate/editable table
    rowList = processRoleTxt(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt')

def readInConstraint(printBool): # reads in constraint as structured form and returns dictionary to SCTRF2R
    rowList = processRoleTxt(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt')
    myGroupNoList = []
    myGroupNoList = sorted(list(set([item[3] for item in rowList])))
    NTCPresent = False
    for row in rowList:
        if row[0] == 'NTC':
            NTCPresent = True
    if not NTCPresent:
        myGroupNoList.remove('0')
    myDict = probeGroupedRoles(readFocusValue()[0], rowList, printBool)
    if printBool:
        writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', False)
    return myDict

def SSreadFocusValue():
    f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSfocusSwap.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    return myId

def SSfocusSwapWrite(inString):
    if len(inString) > 5:
        try:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSfocusSwap.txt', 'w', encoding="UTF-8")
            f.write(inString)
        finally:
            f.close()
    # diagram linking for ss nav focus
    tempSetting = cfg("DRAW_DIAG")
    if (tempSetting == 13) and (cfg("NAV_SS") == 2):
        drawPics(13)
    else:
        drawPics(1)
    writeRoleAndDotFiles(inString)
    dotDrawFocus(inString, [], ">")
    dotDrawAncestors(inString, 1)
    drawPics(tempSetting)

def processConstraintSet(redunBool):
    totalInclusionResultList = []
    totalExclusionResultList = []
    totalInclusionResultIds = []
    totalExclusionResultIds = []
    totalReturnableResultList = []
    # probably impossible! ordered row list option where quitbool is true
    totalInclusionOrderedRowList = []
    totalExclusionOrderedRowList = []
    tempQuitBoolOverall = False
    myTempDict = ECLsetRead(False, "", True)
    for constraint in myTempDict:
        writeOutRawECL(tuplify(myTempDict[constraint][0][1:]))
        tempOrderedRowList = []
        tempResultList = []
        tempQuitBool = False
        tempQuitBool, tempOrderedRowList, tempResultList = processConstraintApp(redunBool, False)
        if tempQuitBool:
            tempQuitBoolOverall = True
        for row in tempOrderedRowList:
            if myTempDict[constraint][0][0] == "I":
                totalInclusionOrderedRowList.append(row)
            else:
                totalExclusionOrderedRowList.append(row)
        for row in tempResultList:
            if myTempDict[constraint][0][0] == "I":
                if row not in totalInclusionResultList:
                    totalInclusionResultList.append(row)
                    totalInclusionResultIds.append(row[1])
            else:
                if row not in totalExclusionResultList:
                    totalExclusionResultList.append(row)
                    totalExclusionResultIds.append(row[1])
     # write for diagram demo:
    if cfg("DEMO") == 1:
        writeOutList(totalInclusionResultIds, cfgPathInstall() + 'sets' + sep() + 'W.txt')
        writeOutList(totalExclusionResultIds, cfgPathInstall() + 'sets' + sep() + 'X.txt')
        includeExcludeLongSetNames()
    # finally dump out the combined result
    totalInclusionResultIds = list(NOT(set(totalInclusionResultIds), set(totalExclusionResultIds)))
    totalReturnableResultList = [row for row in totalInclusionResultList if row[1] in totalInclusionResultIds]
    totalReturnableResultList = sorted(totalReturnableResultList, key=lambda x: x[2])
    myReturnDict = {}
    myReturnDict = printProcessConstraintResult(tempQuitBoolOverall, totalInclusionOrderedRowList, myReturnDict, totalReturnableResultList, True)
    return myReturnDict

def processConstraintApp(redunBool, singleBool): # main constraint processing call controller
    writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
    if singleBool:
        ECLbreadcrumbsWrite(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt')
    mydict = {}
    rowList = []
    orderedRowList = []
    myObjectList = []
    myGroupNoList = []
    # READ WORKING PREDICATE - read in intermediate/editable table
    rowList = processRoleTxt(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt')
    myGroupNoList = sorted(list(set([item[3] for item in rowList])))
    # early check for redundant groups:
    if redunBool and not myGroupNoList == ['0']:
        rowList, myGroupNoList = redunCheck(rowList, myGroupNoList)
    NTCPresent = False
    for row in rowList:
        if row[0] == 'NTC':
            NTCPresent = True
    # if not NTCPresent:
    #     myGroupNoList.remove('0')
    # PROCESS
    # probably read back and process rowList from role.txt into role_edit.txt - hand edit role.txt for now
    # 1. prioritise smallest set
    orderedRowList = orderAndCountRoleRows(rowList)
    myGroupNoList = list(set([item[1][3] for item in orderedRowList]))
    myGroupNoList.sort(key=int)
    if not NTCPresent:
        myGroupNoList.remove('0')
    # 2. get object codes from smallest set
    myObjectList = starterObjectSet_IN(orderedRowList[0][1])
    # 3. check what remains after testing all rows (independent of RGs))
    for row in orderedRowList:
        myObjectList = checkEachRow_IN(row[1], myObjectList, listToInString(myObjectList), True)
    myDefinition = []
    testList = []
    # check if it's a simple one (only supertypes or any number of supertypes and one role:
    # myObjectList is already the answer with no further testing)
    quitBool = False
    onlySupertypes = False
    onlySupertypes = testOnlySupertypes(rowList)
    onlyOneRole = False
    onlyOneRole = testOnlyOneRole(rowList)
    if len(myGroupNoList) > 8:
        quitBool = True
    if onlySupertypes or onlyOneRole:
        clearScreen()
        snapshotRoleHeader()
        writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
        print("2 & 3 of 3: Generating list")
        resultList = [['True', item, pterm(item)] for item in myObjectList]
    else:
        itemCounter = 0
        total = len(myObjectList)
        for item in myObjectList:
            if ((total - itemCounter) % 10 == 0) or (total < 20):
                clearScreen()
                snapshotRoleHeader()
                writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
                n = int((total - itemCounter) * (60 / total))
                # print("2 of 3: [" + ('#' * n) + ('-' * (59 - n)) + ']')
                print("2 of 3: " + ('*' * n))
            testTempList = []
            RGList = []
            myDefinition = groupedRolesFromConId_IN(item)
            testTempList.append(item)
            for holderItem in myDefinition:
                if not holderItem[0] == -1:
                    RGList.append(holderItem[0])
                elif holderItem[0] == -1 or holderItem[0] == 0:
                    testTempList.append(holderItem)
            RGSequences = []
            if not onlySupertypes:
                tupleLength = len(myGroupNoList)
                # nPr variant works ok up to 8 or 9 predicate groups. Below 'if' clause gambles false negative (only
                # testing 1 pattern) against speed/memory consequences of generating full permutation list:
                # 8P8 = = 40,320, 9P9 = 362,880, 10P10 = 3,628,800, 11P11 = 39,916,800
                if len(RGList) == tupleLength and len(RGList) > 8: # if long predicate and similarly long candidate
                    RGSequences.append(RGList)
                    quitBool = True
                else:
                    RGSequences = list(permutations(RGList, tupleLength)) # even if long candiate, assumes short predicate.
            else:
                # only supertypes
                RGSequences = []
                myGroupNoList = ['0']
            for RGSequence in RGSequences:
                tempCompareContainer = []
                tempCompareContainer.append(["*", RGSequence])
                tempCompareContainer = uniteSequenceAndRoles(tempCompareContainer, myDefinition, RGSequence)
                testTempList.append(tempCompareContainer)
            testList.append(testTempList)
            itemCounter += 1
        # get results
        resultList = testTestList(testList, rowList, myGroupNoList, False)
    if cfg("SORT_DIAG") == 0 or cfg("SORT_DIAG") == 3:
        resultList = sorted(resultList, key=lambda x: x[2].lower())
    else:
        resultList = naturalSort(resultList, False, 1, 2)
    if singleBool:
        # categorise by atts
        if onlyOneRole and not redunBool and (orderedRowList[0][1][4][0] != '') and (orderedRowList[1][1][4][0] != ''):
            # allow focus constraint to be tightened beyond root
            # make sure role row passed to attList step and beyond (which may otherwise not have worked with original order)
            orderedRowList = sorted(orderedRowList, key=lambda x: -x[1][5])
            myAttList = checkEachRow_IN(orderedRowList[0][1], myObjectList, listToInString(myObjectList), False)
            myAttListWithTerms = [[item, pterm(item)] for item in myAttList]
            myAttListWithTerms = sorted(myAttListWithTerms, key=lambda x: x[1])
            mydict = printProcessConstraintResultWithAtts(quitBool, orderedRowList, mydict, resultList, False, myAttListWithTerms)
            dictToSetreportIdFile(mydict)
        else:
            mydict = printProcessConstraintResult(quitBool, orderedRowList, mydict, resultList, False)
        return mydict
    else:
        return quitBool, orderedRowList, resultList

def printProcessConstraintResult(quitBool, orderedRowList, mydict, resultList, constraintSetBool):
    mycounter = 1
    clearScreen()
    snapshotRoleHeader()
    if not constraintSetBool:
        writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
    else:
        print("Results of running constraint set (see below for details):\n")
    if quitBool and orderedRowList[0][0] == 1:
        mydict.update(list(zip(listify(mycounter), listify(tuplify(orderedRowList[0][1][4][1])))))
        print("[" + str(mycounter) + "]\t" + orderedRowList[0][1][4][1] + "\t" + orderedRowList[0][1][4][2] + " *[incompletely tested RG++]")
    if len(resultList) == 0:
        print("No matches.")
    for item in resultList:
        if item[0] == str(True):
            mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
            print("[" + str(mycounter) + "]\t" + activeColor_IN(item[1]) + tabString(item[1]) + item[2])
            mycounter += 1
    print()
    if quitBool:
        print("Constraint incompletely tested [RG++]\n")
    if constraintSetBool:
        mydict = ECLsetRead(True, "", True, mycounter, mydict)
    if not resultList is None:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'rolereportId.txt', 'w', encoding="UTF-8")
        for roleresult in resultList:
            if roleresult[0] == str(True):
                f.write(roleresult[1] + '\n')
        f.close()
        roleToSetReportFile()
    return mydict

def printProcessConstraintResultWithAtts(quitBool, orderedRowList, mydict, resultList, constraintSetBool, attList):
    mycounter = 1
    clearScreen()
    snapshotRoleHeader()
    if not constraintSetBool:
        writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
    else:
        print("Results of running constraint set (see below for details):\n")
    if quitBool and orderedRowList[0][0] == 1:
        mydict.update(list(zip(listify(mycounter), listify(tuplify(orderedRowList[0][1][4][1])))))
        print("[" + str(mycounter) + "]\t" + orderedRowList[0][1][4][1] + "\t" + orderedRowList[0][1][4][2] + " *[incompletely tested RG++]")
    if len(resultList) == 0:
        print("No matches.")
    # itemTestRow = [thing for thing in orderedRowList[0][1]]
    itemTestRow = copy.deepcopy(orderedRowList[0][1])
    itemTestRow[4][0] = ''
    for att in attList:
        innerCounter = 0
        mydict.update(list(zip(listify(mycounter), listify(tuplify(att[0])))))
        print("\033[4m[" + str(mycounter) + "]\t" + activeColor_IN(att[0]) + tabString(att[0]) + att[1] + '\033[24m')
        mycounter += 1
        print()
        itemTestRow[4][1] = att[0]
        itemTestRow[4][2] = att[1]
        for item in resultList:
            if item[0] == str(True):
                includeBool = False
                if len(checkEachRow_IN(itemTestRow, [item[1]], listToInString([item[1]]), True)) > 0:
                    includeBool = True
                if includeBool:
                    innerCounter += 1
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
                    print("[" + str(mycounter) + "]\t" + activeColor_IN(item[1]) + tabString(item[1]) + item[2])
                    mycounter += 1
        print('\n[' + str(innerCounter) + (' items]\n' if innerCounter > 1 else ' item]\n'))
    if quitBool:
        print("Constraint incompletely tested [RG++]\n")
    if constraintSetBool:
        mydict = ECLsetRead(True, "", True, mycounter, mydict)
    if not resultList is None:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'rolereportId.txt', 'w', encoding="UTF-8")
        for roleresult in resultList:
            if roleresult[0] == str(True):
                f.write(roleresult[1] + '\n')
        f.close()
    return mydict

def uniteSequenceAndRoles(tempCompareContainer, myDefinition, RGSequence): # combines RG sequence with actual roles (from processConstraintApp)
    for holderItem in myDefinition:
        RGToCheck = []
        for roleItem in holderItem:
            if not isinstance(roleItem, int):
                for RGNumber in RGSequence:
                    if RGNumber == roleItem[0]:
                        if not roleItem in RGToCheck:
                            RGToCheck.append(roleItem)
        for RGNumber in RGSequence:
            for RGItem in RGToCheck:
                if RGNumber == RGItem[0]:
                    # print RGSequence, RGNumber, RGItem
                    tempCompareContainer.append(RGItem)
    return tempCompareContainer

def readBackFile(inPath, inHeader, alphaBool, showAllMembers): # reads back and displays members of an ABC file in 'sets' folder
    if inHeader == "ECL":
        writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
    else:
        print(inHeader + "\n")
        navSS(0)
    mydict = {}
    myList = []
    mycounter = 1
    f = io.open(cfgPathInstall() + inPath, 'r', encoding="UTF-8")
    if inHeader == 'Reading back navigation set members:' or inHeader == 'Reading back navigation set structural members:':
        f2 = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetIds.txt', 'w', encoding="UTF-8")
        for line in f:
            f2.write(line.strip() + '\n')
        f2.close()
    if showAllMembers:
        f.seek(0)
        if alphaBool:
            for line in f:
                myList.append([line.strip(), pterm(line.strip()), len(pterm(line.strip()))])
            myList = sorted(myList, key=lambda x: x[2])
        else:
            for line in f:
                myList.append([line.strip(), pterm(line.strip())])
            if cfg("SORT_DIAG") == 0 or cfg("SORT_DIAG") == 3:
                myList = sorted(myList, key=lambda x: x[1].lower())
            else:
                myList = naturalSort(myList, False, 0, 1)
        for item in myList:
            print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0]) + tabString(item[0]) + item[1])
            mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
            mycounter += 1
        dictToSetreportIdFile(mydict)
    else:
        if len(inHeader) <= 15:
            print("Enter 'u" + inHeader[len(inHeader) - 1] + "' to show members.")
        else:
            print("Enter 'u" + inHeader[15] + "' to show members.")
    print()
    f.close()
    return mydict

def sampleAllFiles(): # reads back and displays opening members of all ABC file in 'sets' folder
    print("Displaying all sets:\n")
    navSS(0)
    mydict = {}
    mycounter = 1
    for fileLetter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        preList = []
        myList = []
        f = io.open(cfgPathInstall() + 'sets' + sep() + fileLetter + '.txt', 'r', encoding="UTF-8")
        preList = f.readlines()
        l = len(preList)
        for line in preList:
            if l < 100:
                myList.append([line.strip(), fsnFromConId_IN(line.strip())[1]])
            else:
                if line.strip()[-1] == '0':
                    myList.append([line.strip(), fsnFromConId_IN(line.strip())[1]])
        print("Set " + fileLetter + " [" + str(len(preList)) + "]:\n")
        myList = sorted(myList, key=lambda x: x[1].lower())
        for item in myList[:5]:
            print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0]) + tabString(item[0]) + item[1])
            mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
            mycounter += 1
        print()
        f.close()
    print("Enter 'u plus set letter to show all members.\n")
    dictToSetreportIdFile(mydict)
    return mydict

def objectsToValues(inDict, inIndex, inLetter): # reads object codes from set 'A' and generates list of values for chosen A=V row. if A chosen V's unrestricted.
    mydict = {}
    # read in file A, B, C... whatever
    myObjectIdList = {a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inLetter + '.txt')}
    # identify att id and preceding operator
    inIndexInt = int(inIndex)
    for item in inDict[0][0]:
        key = list(item.keys())[0]
        if inIndexInt in item[key]:
            rowNum = int(key)
            rowPos = item[key].index(inIndexInt)
            if rowPos == 3:
                rowPos = 4
    # find values
    if rowPos == 4: # if specifying values
        attStringTail = ' row (values constrained): \n'
        valId = inDict[0][1][rowNum][4][rowPos]
        valOp = inDict[0][1][rowNum][4][rowPos - 1]
        attId = inDict[0][1][rowNum][4][rowPos - 3]
        attOp = inDict[0][1][rowNum][4][rowPos - 4]
        myValueList = findValuesFromObjects(myObjectIdList, attOp, attId, valOp, valId)
    else: # if only specifying attribute name
        attStringTail = ' row (values unconstrained): \n'
        attId = inDict[0][1][rowNum][4][rowPos]
        attOp = inDict[0][1][rowNum][4][rowPos - 1]
        myValueList = findValuesFromObjects(myObjectIdList, attOp, attId)
    # print out values
    clearScreen()
    snapshotRoleHeader()
    writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', True)
    attString = parsablePretty(attOp + ' ' + attId + ' | ' + pterm(attId) + ' |')
    print('Values for ' + attString + attStringTail)
    myValueList = sorted(myValueList, key=lambda x: x[1].lower())
    mycounter = 1
    for item in myValueList:
        print('[' + str(mycounter) + ']\t' + item[0] + tabString(item[0]) + item[1])
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        mycounter += 1
    print()
    if not myValueList is None:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'rolereportId.txt', 'w', encoding="UTF-8")
        for value in myValueList:
            f.write(value[0] + '\n')
        f.close()
        roleToSetReportFile()
    return mydict

def findValuesFromObjects(inObjectIdList, attOp, attId, valOp='<<', valId=tok['ROOT']): # supporting value-finding code for 'objectsToValues'
    outList = []
    matchableAtts = []
    if attOp == "<<":
        matchableAtts = descendantsCleanIDFromConId_IN(attId)
    elif attOp == "<":
        matchableAtts = descendantsCleanIDFromConId_IN(attId)
        matchableAtts.remove(attId)
    elif attOp == "":
        matchableAtts.append(attId)
    itemCounter = 0
    total = len(inObjectIdList)
    concRoles = concreteTakingRoles()
    concVals = cfg("CONC_VALS")
    concData = cfg("CONC_DATA")
    isaTok = tok['IS_A']
    for item in inObjectIdList:
        roles = fasterRolesFromConId_IN(item, concRoles, concVals, concData, isaTok)
        if ((total - itemCounter) % 10 == 0) or (total < 20):
            clearScreen()
            snapshotRoleHeader()
            print("Processing values: " + '*' * int((total - itemCounter) * (60 / total)))
        if valOp == '<<' and valId == tok['ROOT']:
            for roleRow in roles:
                if roleRow[0] in matchableAtts:
                    if [roleRow[2], roleRow[3]] not in outList:
                        outList.append([roleRow[2], roleRow[3]])
        else:
            for roleRow in roles:
                if roleRow[0] in matchableAtts:
                    if valOp == '<<':
                        if TCCompare_IN(roleRow[2], valId):
                            if [roleRow[2], roleRow[3]] not in outList:
                                outList.append([roleRow[2], roleRow[3]])
                    elif valOp == '<':
                        if TCCompare_IN(roleRow[2], valId) and (roleRow[2] != valId):
                            if [roleRow[2], roleRow[3]] not in outList:
                                outList.append([roleRow[2], roleRow[3]])
                    elif valOp == '':
                        if roleRow[2] == valId:
                            if [roleRow[2], roleRow[3]] not in outList:
                                outList.append([roleRow[2], roleRow[3]])
        itemCounter += 1
    return outList

def readInTwoWriteOne(inSet1, inSet2, inSet3, inLogic, drawBool, inDirection): # reads in two files and merges them according to the logic chosen
    myDict = {}
    outHeader = ""
    outName = ""
    myMergedList = []
    # read in set 1
    myList1 = {a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet1 + '.txt')}
    # read in set 2
    myList2 = {a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet2 + '.txt')}
    # apply logic to both sets
    if inLogic == "OR":
        myMergedList = list(OR(myList1, myList2))
        outHeader = inSet1 + " OR " + inSet2 + " -> " + inSet3
        outName = inSet1 + " OR " + inSet2
    elif inLogic == "NOT":
        myMergedList = list(NOT(myList1, myList2))
        outHeader = inSet1 + " NOT " + inSet2 + " -> " + inSet3
        outName = inSet1 + " NOT " + inSet2
    else:
        myMergedList = list(AND(myList1, myList2))
        outHeader = inSet1 + " AND " + inSet2 + " -> " + inSet3
        outName = inSet1 + " AND " + inSet2
    # write out result
    if inSet3.isalpha():
        writeOutList(myMergedList, cfgPathInstall() + 'sets' + sep() + inSet3 + '.txt')
    # read back and display result
    if drawBool:
        dotDrawSets(myList1, myList2, myMergedList, inSet1, inSet2, outName, inDirection, inLogic)
    myDict = readBackFile('sets' + sep() + inSet3 + '.txt', outHeader, False, False)
    return myDict

def keyNodesAndEdges(inList1Name, inList2Name, inList3Name, inList1Term, inList2Term, inLogic):
    keyNodes = []
    keyEdges = []
    if inLogic == "AND":
        keyNodes.append("\"" + inList1Name + "\" [label=\"" + toString(textwrap.wrap(simplify(inList1Term), 25)) + "\", " + checkSetMemberColor(inList1Name.strip(), [inList1Name], [], []) + "];\n")
        keyNodes.append("\"" + inList2Name + "\" [label=\"" + toString(textwrap.wrap(simplify(inList2Term), 25)) + "\", " + checkSetMemberColor(inList2Name.strip(), [], [inList2Name], []) + "];\n")
        keyNodes.append("\"" + inList3Name + "\" [label=\"" + inList3Name + "\", " + checkSetMemberColor(inList3Name.strip(), [], [], [inList3Name]) + "];\n")
        keyEdges.append("\"" + tok['ROOT'] + "\"->\"" + inList1Name + "\" [style=invis];\n")
        keyEdges.append("\"" + tok['ROOT'] + "\"->\"" + inList2Name + "\" [style=invis];\n")
    if inLogic == "OR":
        keyNodes.append("\"" + inList3Name + "\" [label=\"" + toString(textwrap.wrap(simplify(inList1Term + " OR " + inList2Term), 25)) + "\", " + checkSetMemberColor(inList3Name.strip(), [], [], [inList3Name]) + "];\n")
    elif inLogic == "NOT":
        keyNodes.append("\"" + inList2Name + "\" [label=\"" + toString(textwrap.wrap(simplify(inList2Term), 25)) + "\", " + checkSetMemberColor(inList2Name.strip(), [], [inList2Name], []) + "];\n")
        keyNodes.append("\"" + inList3Name + "\" [label=\"" + toString(textwrap.wrap(simplify(inList1Term + " NOT " + inList2Term), 25)) + "\", " + checkSetMemberColor(inList3Name.strip(), [], [], [inList3Name]) + "];\n")
        keyEdges.append("\"" + tok['ROOT'] + "\"->\"" + inList2Name + "\" [style=invis];\n")
    keyEdges.append("\"" + tok['ROOT'] + "\"->\"" + inList3Name + "\" [style=invis];\n")
    return keyNodes, keyEdges

def checkSetMemberColor(inId, list1, list2, list3):
    # colours inspired by Paul Tol's suggestion https://personal.sron.nl/~pault/ from https://davidmathlogic.com/colorblind/
    # still not happy!
    returnString = ""
    if inId in list1:
        returnString = "shape=box, peripheries=1, style=filled, fillcolor=\"#88CCEE\"" # list 1
    if inId in list2:
        returnString = "shape=box, peripheries=1, style=filled, fillcolor=\"#DDCC77\"" # list 2
    if inId in list3:
        returnString = "shape=box, peripheries=1, style=filled, fontcolor=white, fillcolor=\"#882255\"" # list 2 (and, or or not, so can overwrite)
    return returnString

def longSetNames(inList1Name, inList2Name, nameString):
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'setNames.txt', 'r', encoding="UTF-8")
    inNameLines = f.readlines()
    f.close()
    inNameDict = {}
    for inName in inNameLines:
        tempInName = inName.split("|")
        inNameDict[tempInName[0]] = tempInName[1]
    f.close()
    outNameLines = []
    inList1Term = ""
    inList2Term = ""
    inList1TermBool = False
    inList2TermBool = False
    if nameString == "#" and len(inNameLines) == 2:
        if inList1Name in inNameDict:
            inList1Term = inNameDict[inList1Name].strip() + " (" + inList1Name + ")"
            inList1TermBool = True
            outNameLines.append(inList1Name + "|" + inNameDict[inList1Name])
        if inList2Name in inNameDict:
            inList2Term = inNameDict[inList2Name].strip() + " (" + inList2Name + ")"
            inList2TermBool = True
            outNameLines.append(inList2Name + "|" + inNameDict[inList2Name])
    if not inList1TermBool:
        tempInput = input2_3("Rename " + inList1Name + "?:").strip()
        if tempInput == "":
            inList1Term = inList1Name
        else:
            inList1Term = tempInput + " (" + inList1Name + ")"
            outNameLines.append(inList1Name + "|" + tempInput + "\n")
    if not inList2TermBool:
        tempInput = input2_3("Rename " + inList2Name + "?:").strip()
        if tempInput == "":
            inList2Term = inList2Name
        else:
            inList2Term = tempInput + " (" + inList2Name + ")"
            outNameLines.append(inList2Name + "|" + tempInput + "\n")
    if len(outNameLines) == 2:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'setNames.txt', 'w', encoding="UTF-8")
        f.write(outNameLines[0])
        f.write(outNameLines[1])
        f.close()
    return inList1Term, inList2Term

def includeExcludeLongSetNames():
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'setNames.txt', 'w', encoding="UTF-8")
    f.write("W|include\n")
    f.write("X|exclude")
    f.close()

def dotDrawSets(inList1, inList2, inList3, inList1Name, inList2Name, inList3Name, inBTDir, inLogic): # draw dot file for sets of codes  - invoked from readInTwoWriteOne
    setNodes = []
    setEdges = []
    inIdList = []
    writeableNodes = []
    postEdgeNodes = []
    inIdList.extend(inList1)
    inIdList.extend(inList2)
    inIdList.extend(inList3)
    inIdList = list(set(inIdList))
    for item in inIdList:
        setNodes.append([item, pterm(item)])
    sendList = inIdList[:]
    augmentList = [tok["MORPH_ABNO"], tok["DISORDER"]]
    descendantsOfList = readInZ()
    if cfg("DEMO") == 1:
        augmentList = [*augmentList, *descendantsOfList]
    setEdges = adjacencyBuild(setToGraph(sendList, False, setToGraphAugmentTest(sendList, augmentList)), False)
    for edge in setEdges:
        postEdgeNodes.append(edge[0])
        postEdgeNodes.append(edge[1])
    postEdgeNodes = list(set(postEdgeNodes))
    postEdgeNodes = [node for node in postEdgeNodes if node not in inIdList]
    postEdgeNodes = [*postEdgeNodes, *descendantsOfList]
    nameString = input2_3("Rename each letter set? [y, n or # to reuse last pair]:")
    if nameString in ["y", "Y", "#"]:
        inList1Term, inList2Term = longSetNames(inList1Name, inList2Name, nameString)
    else:
        inList1Term = inList1Name
        inList2Term = inList2Name
    keyNodes, keyEdges = keyNodesAndEdges(inList1Name, inList2Name, inList3Name, inList1Term, inList2Term, inLogic)
    if cfg("GRAPH_ANGLE") in (1, 3): # use angled edges
        splineString = "; splines=ortho"
    else:
        splineString = ""
    for node in postEdgeNodes:
        if node not in descendantsOfList:
            writeableNodes.append("\"" + node + "\" [label=\"" + node + "\l" + simplify(pterm(node)) + "\l\", shape=box, peripheries=2, style=filled, fillcolor=pink];\n")
        elif node in inIdList:
            writeableNodes.append("\"" + node + "\" [label=\"< " + node + "\l" + simplify(pterm(node)) + "\l\", shape=box, peripheries=1, style=filled, fillcolor=gray];\n")
        else:
            writeableNodes.append("\"" + node + "\" [label=\"< " + node + "\l" + simplify(pterm(node)) + "\l\", shape=box, peripheries=1, style=filled, fillcolor=pink];\n")
    for node in [n for n in setNodes if n[0] not in [m for m in postEdgeNodes]]:
        if hasDef(node[0]) and cfg("SHOW_DEFS") > 4:
            if cfg("GRAPH_ANGLE") in (2, 3):
                ptermLen = len(fsnFromConId_IN(node[0])[1])
            else:
                ptermLen = len(pterm(node[0]))
            defString = "\l\l" + toString(textwrap.wrap(simplify(onlyDefTermsFromConId_IN(node[0])[1].replace(' (*)', '')), (50 if ptermLen < 50 else ptermLen))) + "\\n\", "
        else:
            defString = "\l\", "
        if cfg("GRAPH_ANGLE") in (2, 3): # ...and use FSNs
            writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(fsnFromConId_IN(node[0])[1]) + defString + checkSetMemberColor(node[0].strip(), inList1, inList2, inList3) + "];\n")
        else:
            writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + defString + checkSetMemberColor(node[0].strip(), inList1, inList2, inList3) + "];\n")
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'ssview_temp.dot', 'w', encoding="UTF-8")
        dirString = edgeDirection(inBTDir)
        f.write("strict digraph G { rankdir=" + dirString + splineString + "; ranksep=.3; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        for edge in sorted_cfg(setEdges):
            f.write("\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=onormal];\n")
        for edge in sorted_cfg(keyEdges):
            f.write(edge)
        for node in sorted_cfg(writeableNodes):
            f.write(node)
        for node in sorted_cfg(keyNodes):
            f.write(node)
        f.write("}")
    finally:
        f.close()
        renameDiagramFile('ssview_temp.dot', 'ssview.dot')

def edgeDirection(inBTDir):
    if inBTDir == "^":
        dirString = 'BT'
    elif inBTDir == "<" or inBTDir == "+":
        dirString = 'RL'
    elif inBTDir == "v" or inBTDir == "V":
        dirString = 'TB'
    elif inBTDir == ">":
        dirString = 'LR'
    return dirString

def readInOneWriteOne(inSet1, inSet2, inHeader, combineRule, alphaBool, showAllBool): # moves content from one file to another unchanged.
    myDict = {}
    myList = []
    myList2 = []
    # read in set 1
    myList = readFileMembers(cfgPathInstall() + inSet1)
    if combineRule == 7: # ancestors
        for item in myList:
            tempList = ancestorsFromConId_IN(item)
            for item2 in tempList:
                if item2[0] not in myList:
                    myList.append(item2[0])
    if combineRule == 6: # descendants
        for item in myList:
            tempList = descendantsFromConId_IN(item)
            for item2 in tempList:
                if item2[0] not in myList:
                    myList.append(item2[0])
    # expand atts and or vals:
    if combineRule in (4, 5, 8, 9, 12, 13): # expand through roles to all vals
        for item in myList:
            if combineRule == 4:
                tempList = growValues(item, True)
            else:
                tempList = growValues(item, False)
            for item2 in tempList:
                if item2 not in myList:
                    myList.append(item2)
    # expand through roles to defined vals
    if combineRule in (3, 10, 11):
        myList = growValuesForDiagram(myList[0])
    if combineRule in (1, 2):
        myList2 = readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet2 + '.txt')
        if combineRule == 1:
            myList = list(OR(set(myList), set(myList2)))
        else:
            myList = list(NOT(set(myList2), set(myList)))
    if inSet2.isalpha():
        writeOutList(myList, cfgPathInstall() + 'sets' + sep() + inSet2 + '.txt')

    # invoke dotDrawDefs with specified direction:
    if combineRule in (8, 9, 12, 13):
        # invoke 'BA>' equivalent
        # force cfg conditions for defs & tags to show
        cfgList = setConfigsFullDiag(combineRule)
        if combineRule in (8, 12):
            dotDrawDefs(inSet2, '>')
        elif combineRule in (9, 13):
            dotDrawDefs(inSet2, '^')
        resetConfigsFullDiag(cfgList)
        clearScreen()
        snapshotRoleHeader()
    if combineRule in (10, 11):
        # invoke 'BA>' equivalent
        if combineRule == 10:
            dotDrawDefs(inSet2, '>')
        elif combineRule == 11:
            dotDrawDefs(inSet2, '^')
        clearScreen()
        snapshotRoleHeader()
    # read back and display result
    myDict = readBackFile('sets' + sep() + inSet2 + '.txt', inHeader, alphaBool, showAllBool)
    return myDict

def setConfigsFullDiag(combineRule):
    # specify O, C and B values
    tempSD = cfg("SHOW_DEFS") #O
    tempSS = cfg("SHOW_SUPERS") #C
    tempMV = cfg("MERGE_VALS") #B
    if combineRule in (8, 9): # AA#, AA]
        cfgWrite("SHOW_DEFS", 12)
    elif combineRule in (10, 11): # AA', AA[
        cfgWrite("SHOW_DEFS", 11)
    elif combineRule in (12, 13): # AA~, AA}
        cfgWrite("SHOW_DEFS", 7)
    if tempSS == 3:
        cfgWrite("SHOW_SUPERS", 3)
    else:
        cfgWrite("SHOW_SUPERS", 1)
    if tempMV in (0, 1):
        cfgWrite("MERGE_VALS", 2)
    else:
        cfgWrite("MERGE_VALS", 5)
    return [tempSD, tempSS, tempMV]

def resetConfigsFullDiag(cfgList):
    cfgWrite("SHOW_DEFS", cfgList[0])
    cfgWrite("SHOW_SUPERS", cfgList[1])
    cfgWrite("MERGE_VALS", cfgList[2])

def writeOutList(inList, inPath): # writes out list of codes to a file
    f = io.open(inPath, 'w', encoding="UTF-8")
    for item in inList:
        f.write(item + '\n')
    f.close()

def toDesc(inDict, inIndex, inChar): # change concept operator to '<<', '<' or ''
    inIndexInt = int(inIndex)
    # findLocation
    for item in inDict[0][0]:
        key = list(item.keys())[0]
        if inIndexInt in item[key]:
            rowNum = int(key)
            rowPos = item[key].index(inIndexInt)
            if rowPos == 2:
                rowPos = 3
    # set new value
    inDict[0][1][rowNum][4][rowPos] = inChar
    return inDict

# CONC - set concrete value number, note remove term if converting from concept template
def setConcVal(inDict, inIndex, inNumber):
    inIndexInt = int(inIndex)
    # findLocation
    for item in inDict[0][0]:
        key = list(item.keys())[0]
        if inIndexInt in item[key]:
            rowNum = int(key)
            rowPos = item[key].index(inIndexInt)
            if rowPos == 3:
                rowPos = 4
    # set new value
    if "#" in inNumber:
        inDict[0][1][rowNum][4][rowPos] = inNumber
    else:
        inDict[0][1][rowNum][4][rowPos] = inNumber
    inDict[0][1][rowNum][4][rowPos + 1] = "" # remove term if converting from concept template
    return inDict

def readIndexList(indexLookup, idString):
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'indexList.txt', 'r', encoding="UTF-8")
    lines = f.readlines()
    f.close()
    for line in lines:
        lineTemp = line.split("\t")
        if lineTemp[0] == indexLookup:
            return lineTemp[1].strip()
    return idString # if nothing matched

def changeECLCode(inDict, inIndex): # inject new value from snapshot focus into specific index point of structured predicate
    idString = readFocusValue()[0]
    if "-" in inIndex:
        inIndexTemp = inIndex.split("-")
        inIndex = inIndexTemp[0]
        indexLookup = inIndexTemp[1]
        if indexLookup != "0":
            idString = readIndexList(indexLookup, idString)
        else:
            idString = tok["ROOT"]
    inIndexInt = int(inIndex)
    for item in inDict[0][0]:
        key = list(item.keys())[0]
        if inIndexInt in item[key]:
            rowNum = int(key)
            rowPos = item[key].index(inIndexInt)
            if rowPos == 3:
                rowPos = 4
    # set new value
    inDict[0][1][rowNum][4][rowPos] = idString
    inDict[0][1][rowNum][4][rowPos + 1] = pterm(idString)
    return inDict

def changeECLCodeToSet(inDict, inIndex, inSetLetter): # inject new value from snapshot focus into specific index point of structured predicate
    inIndexInt = int(inIndex)
    idString = "SET_"
    for item in inDict[0][0]:
        key = list(item.keys())[0]
        if inIndexInt in item[key]:
            rowNum = int(key)
            rowPos = item[key].index(inIndexInt)
            if rowPos == 3:
                rowPos = 4
    # set new value
    inDict[0][1][rowNum][4][rowPos] = idString + inSetLetter
    inDict[0][1][rowNum][4][rowPos + 1] = "Local set " + inSetLetter
    return inDict

def updateInsertableRow(inRowTemplate, inRowData, groupBool, cloneBool, newGroupBool): # revise new structured role row to predicate
    if not groupBool:
        inRowTemplate[0] = 'NTC'
    inRowTemplate[1] = str(int(inRowData[1]) + 1)
    inRowTemplate[2] = groupBool
    if newGroupBool:
        inRowTemplate[3] = str(int(inRowData[3]) + 1)
    else:
        inRowTemplate[3] = inRowData[3]
    if cloneBool:
        inRowTemplate[4] = inRowData[4]
    return inRowTemplate

def updateInsertableFocus(inRowTemplate, inRowData, cloneBool): # revise new structured focus row to predicate
    inRowTemplate[1] = str(int(inRowData[1]) + 1)
    if cloneBool:
        inRowTemplate[4] = inRowData[4]
    return inRowTemplate

def addRowToECL(inDict, inIndex, groupBool, cloneBool, newGroupBool): # add a new row to the predicate
    newRole = ['', '0', True, '0', ['', tok["CONMODOBJ_ATTRIB"], pterm(tok["CONMODOBJ_ATTRIB"]), '<<', tok["ROOT"], pterm(tok["ROOT"])], 5]
    newFocus = ['', '0', 'FOCUS', '0', ['<<', tok["ROOT"], pterm(tok["ROOT"])], 3]
    inIndexInt = int(inIndex)
    for item in inDict[0][0]:
        key = list(item.keys())[0]
        if inIndexInt in item[key]:
            rowNum = int(key)
    # if not the last row
    if not(rowNum + 1 == len(inDict[0][1])):
        # shift subsequent row numbers down
        for item in inDict[0][1][rowNum + 1:]:
            item[1] = str(int(item[1]) + 1)
        # insert new row
        # check if new row is a role row or a supertype
        if inDict[0][1][rowNum][5] == 5:
            newRole = updateInsertableRow(newRole, inDict[0][1][rowNum], groupBool, cloneBool, newGroupBool)
            inDict[0][1].insert(rowNum + 1, newRole)
        else:
            newFocus = updateInsertableFocus(newFocus, inDict[0][1][rowNum], cloneBool)
            inDict[0][1].insert(rowNum + 1, newFocus)
    else:
        # just append a new row to inDict[0][1]
        # check if new row is a role row or a supertype
        if inDict[0][1][rowNum][5] == 5:
            newRole = updateInsertableRow(newRole, inDict[0][1][rowNum], groupBool, cloneBool, newGroupBool)
            inDict[0][1].append(newRole)
        else:
            if (groupBool and newGroupBool) or ((not groupBool) and not(newGroupBool)): # adds grouped role to end of list of supers
                newRole = updateInsertableRow(newRole, inDict[0][1][rowNum], groupBool, cloneBool, newGroupBool)
                inDict[0][1].append(newRole)
            else:
                newFocus = updateInsertableFocus(newFocus, inDict[0][1][rowNum], cloneBool)
                inDict[0][1].append(newFocus)
    return inDict

def removeRowFromECL(inDict, inIndex, cloneBool): # remove row from predicate
    inIndexInt = int(inIndex)
    for item in inDict[0][0]:
        key = list(item.keys())[0]
        if inIndexInt in item[key]:
            rowNum = int(key)
    goAhead = False
    # check that at least one 'FOCUS' row will remain
    if inDict[0][1][rowNum][2] == 'FOCUS':
        if focusCount(inDict[0][1]) > 1:
            goAhead = True
    else:
        goAhead = True
    if goAhead:
        if cloneBool:
            tempRow = inDict[0][1][rowNum]
            replaceableRole = ['', tok["CONMODOBJ_ATTRIB"], pterm(tok["CONMODOBJ_ATTRIB"]), '<<', tok["ROOT"], pterm(tok["ROOT"])]
            replaceableFocus = ['<<', tok["ROOT"], pterm(tok["ROOT"])]
            tempRow = inDict[0][1][rowNum]
            rowCounter = 0
            for item in inDict[0][1]:
                if (item[4] == replaceableRole and item[5] == 5) or (item[4] == replaceableFocus and item[5] == 3):
                    inDict[0][1][rowCounter][4] = tempRow[4]
                rowCounter += 1
        else:
            del inDict[0][1][rowNum]
            # shift subsequent row numbers up
            for item in inDict[0][1][rowNum:]:
                item[1] = str(int(item[1]) - 1)
                # if item[0] == "NTC":
                #     # item[3] = str(int(item[3]) + 1)
                #     item[3] = '0'
    return inDict

def focusCount(inList): # check the number of focus codes
    focusCounter = 0
    for item in inList:
        if item[2] == 'FOCUS':
            focusCounter += 1
    return focusCounter

def writeECL(inPath, Processing): # prints out ECL version of prepared constraint
    inLines = []
    try:
        f = io.open(inPath, 'r', encoding="UTF-8")
        inLines = f.readlines()
    finally:
        f.close()
    if Processing:
        print("Processing constraint:\n")
    else:
        print("Constraint:\n")
    lineCounter = 0
    andBool = False
    refinBool = False
    for line in inLines:
        if "|:" in line:
            refinBool = True
    for line in inLines:
        line = line.strip() + " "
        # CONC - convert memory representation of concrete query clause to ECL representation
        if (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1) and not line[0] == "^":
            if line[[x.isdigit() for x in line].index(True):line.find(" | ")] in concreteTakingRoles():
                line = line.replace(" |  |", "")
                line = line.replace(" = < ", " <= ")
                line = line.replace(" = > ", " >= ")
        if (lineCounter == 0) and (line[-4:] == 'AND ') and refinBool:
            print("(" + parsablePretty(line))
            andBool = True
        elif (lineCounter > 0) and andBool:
            line = (parsablePretty(line)).replace("|:", "|):")
            print(line)
        else:
            print(parsablePretty(line))
        lineCounter += 1
    print()

def ECLbreadcrumbsWrite(inPath):
    inLines = []
    try:
        f = io.open(inPath, 'r', encoding="UTF-8")
        inLines = f.readlines()
    finally:
        f.close()
    straightLine = ''
    for line in inLines:
        straightLine += line.strip() + '$$$$$'
    inString = straightLine[:-5] + '\n'
    readbackList = []
    writeToList = []
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + 'ECLbreadcrumbs.txt', "r", encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            readbackList.append(line)
        matchBool = False
        if inString in readbackList:
            matchBool = True
        if not matchBool:
            readbackList.append(inString)
        f.close()
        f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + 'ECLbreadcrumbs.txt', 'w', encoding="UTF-8")
        if matchBool:
            writeToList = readbackList
        else:
            writeToList = readbackList[1:]
        for entry in writeToList:
            f.write(entry)
    finally:
        f.close()

def ECLbreadcrumbsRead():
    mydict = {}
    mycounter = 1
    print('Recent constraints:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + 'ECLbreadcrumbs.txt', "r", encoding="UTF-8")
    for line in f:
        line = line.strip()
        # CONC - clean up concrete operators and carriage returns when reading from file.
        displayLine = parsablePretty(line.replace('$$$$$', '\n\t').replace("| = > #", "| >= #").replace("| = < #", "| <= #").replace(" |  |", ""))
        print('[' + str(mycounter) + ']\t' + displayLine + '\n')
        mydict.update(list(zip(listify(mycounter), listify(tuplify(line.strip().replace('$$$$$', '\n'))))))
        mycounter += 1
    f.close()
    print()
    return mydict

def ECLsetWrite(includeExclude, newBool):
    inLines = []
    # pick up working constraint definition for addition/substitution
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', 'r', encoding="UTF-8")
        inLines = f.readlines()
    finally:
        f.close()
    straightLine = ''
    for line in inLines:
        straightLine += line.strip() + '$$$$$'
    inString = straightLine[:-5] + '\n'
    readbackListInclude = []
    readbackListExclude = []
    try:
        if newBool:
            if includeExclude == 'include':
                readbackListInclude.append(inString)
            else:
                readbackListExclude.append(inString)
        else:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', "r", encoding="UTF-8")
            lines = f.readlines()
            includeBool = True
            for line in lines:
                if line.strip() == "EXCLUDE":
                    includeBool = False
                else:
                    if includeBool:
                        readbackListInclude.append(line)
                    else:
                        readbackListExclude.append(line)
            matchBoolInclude = False
            if inString in readbackListInclude:
                matchBoolInclude = True
            matchBoolExclude = False
            if inString in readbackListExclude:
                matchBoolExclude = True
            if includeExclude == 'include' and not matchBoolInclude:
                readbackListInclude.append(inString)
            if includeExclude == 'exclude' and not matchBoolExclude:
                readbackListExclude.append(inString)
            f.close()
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', 'w', encoding="UTF-8")
        for entry in readbackListInclude:
            f.write(entry)
        f.write("EXCLUDE\n")
        for entry in readbackListExclude:
            f.write(entry)
        f.close()
        writeECLsetExt(readbackListInclude, readbackListExclude)
    finally:
        f.close()

def ECLsetRemove(removeKey): # row number to be removed
    readbackListInclude = []
    readbackListExclude = []
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', "r", encoding="UTF-8")
        lines = f.readlines()
        includeBool = True
        indexCounter = 1
        for line in lines:
            if line.strip() == "EXCLUDE":
                includeBool = False
            else:
                if not indexCounter == removeKey:
                    if includeBool:
                        readbackListInclude.append(line)
                    else:
                        readbackListExclude.append(line)
                indexCounter += 1
        f.close()
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', 'w', encoding="UTF-8")
        for entry in readbackListInclude:
            f.write(entry)
        f.write("EXCLUDE\n")
        for entry in readbackListExclude:
            f.write(entry)
        f.close()
        writeECLsetExt(readbackListInclude, readbackListExclude)
    finally:
        f.close()

def writeECLsetExt(readbackListInclude, readbackListExclude):
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLsetExt.txt', 'w', encoding="UTF-8")
    if len(readbackListExclude) > 0:
        f.write("(")
    entryCounter = 1
    for entry in readbackListInclude:
        writeLine = entry.replace("$$$$$", " ")
        if entryCounter < len(readbackListInclude):
            f.write("(" + writeLine.strip() + ") OR \n")
        else:
            if len(readbackListInclude) == 1 and len(readbackListExclude) <= 1:
                f.write(writeLine.strip())
            else:
                f.write("(" + writeLine.strip() + ")")
        entryCounter += 1
    if len(readbackListExclude) > 0:
        f.write(") \nMINUS \n")
        if len(readbackListExclude) > 1:
            f.write("(")
        entryCounter = 1
        for entry in readbackListExclude:
            writeLine = entry.replace("$$$$$", " ")
            if entryCounter < len(readbackListExclude):
                f.write("(" + writeLine.strip() + ") OR \n")
            else:
                f.write("(" + writeLine.strip() + ")")
            entryCounter += 1
        if len(readbackListExclude) > 1:
            f.write(")")

def ECLsetRead(displayBool, inName, returnBool, mycounter=1, mydict=0):
    if mydict == 0:
        mydict = {}
    if displayBool:
        if inName == "":
            print('Constraint set:\n')
        else:
            if isinstance(inName, tuple):
                tempName = inName[0]
            else:
                tempName = inName
            if tempName[-3:] == "---":
                print('Removed constraint set \033[36m' + tempName[:-3] + '\033[92m:\n')
            else:
                print('Constraint set \033[36m' + tempName + '\033[92m:\n')
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', "r", encoding="UTF-8")
    if displayBool:
        print('Include:\n')
    leadChar = "I"
    for line in f:
        line = line.strip()
        if line == "EXCLUDE":
            if displayBool:
                print("Exclude:\n")
            leadChar = "E"
        else:
            displayLine = parsablePretty(line.replace('$$$$$', '\n\t'))
            if displayBool:
                print('[' + str(mycounter) + ']\t' + displayLine + '\n')
            if displayBool:
                leadChar = ""
            mydict.update(list(zip(listify(mycounter), listify(tuplify(leadChar + line.strip().replace('$$$$$', '\n'))))))
            mycounter += 1
    f.close()
    if displayBool:
        ECLsetExtRead()
    if returnBool:
        return mydict

def ECLsetExtRead():
    print("Combined ECL:\n")
    f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLsetExt.txt', "r", encoding="UTF-8")
    for line in f:
        line = line.strip() + " "
        print(parsablePretty(line))
    print()
    f.close()

def constrSetList():
    mydict = {}
    mycounter = 1
    print('Constraint sets:\n')
    f = io.open(cfgPathInstall() + 'files/constrSets/@index.txt', "r", encoding="UTF-8")
    for line in f:
        displayLine = line.strip()
        print('[' + str(mycounter) + ']\t' + displayLine + '\n')
        mydict.update(list(zip(listify(mycounter), listify(tuplify(line.strip())))))
        mycounter += 1
    f.close()
    print()
    return mydict

def loadOrModifyConstrSet(inName, loadBool):
    if not isinstance(inName, tuple):
        inName = tuplify(inName)
    # normalise inName to _ separated alphanumeric as used for filenames
    inNameString = "".join([x if x.isalnum() else "_" for x in inName[0].strip()])
    readbackListInclude = []
    readbackListExclude = []
    # just read one in
    if loadBool:
        try:
            f = io.open(cfgPathInstall() + 'files/constrSets/' + inNameString + '.txt', "r", encoding="UTF-8")
            lines = f.readlines()
            includeBool = True
            for line in lines:
                if line.strip() == "EXCLUDE":
                    includeBool = False
                else:
                    if includeBool:
                        readbackListInclude.append(line)
                    else:
                        readbackListExclude.append(line)
            f.close()
            f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', 'w', encoding="UTF-8")
            for entry in readbackListInclude:
                f.write(entry)
            f.write("EXCLUDE\n")
            for entry in readbackListExclude:
                f.write(entry)
            f.close()
            writeECLsetExt(readbackListInclude, readbackListExclude)
        finally:
            f.close()
    # or update or add/delete
    else:
        if inNameString[-3:] == "___":
            removeConstraintSet(cfgPathInstall() + 'files/constrSets/' + inNameString[:-3] + '.txt')
            try:
                # update @index file if needed, readback first...
                f = io.open(cfgPathInstall() + 'files/constrSets/@index.txt', "r", encoding="UTF-8")
                lines = f.readlines()
                lines.sort()
                f.close()
                # and then write out again, omitting removed line
                f = io.open(cfgPathInstall() + 'files/constrSets/@index.txt', "w", encoding="UTF-8")
                for line in lines:
                    if not inNameString[:-3] + "\n" == line:
                        f.write(line)
            finally:
                f.close()
        else:
            try:
                # read in ECLset.txt
                f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', "r", encoding="UTF-8")
                lines = f.readlines()
                includeBool = True
                for line in lines:
                    if line.strip() == "EXCLUDE":
                        includeBool = False
                    else:
                        if includeBool:
                            readbackListInclude.append(line)
                        else:
                            readbackListExclude.append(line)
                f.close()
                # write out stand-alone file
                f = io.open(cfgPathInstall() + 'files/constrSets/' + inNameString + '.txt', "w", encoding="UTF-8")
                for entry in readbackListInclude:
                    f.write(entry)
                f.write("EXCLUDE\n")
                for entry in readbackListExclude:
                    f.write(entry)
                f.close()
                # update @index file if needed, readback first...
                f = io.open(cfgPathInstall() + 'files/constrSets/@index.txt', "r", encoding="UTF-8")
                lines = f.readlines()
                if inNameString + "\n" not in lines:
                    lines.append(inNameString + "\n")
                lines.sort()
                f.close()
                # and then write out again
                f = io.open(cfgPathInstall() + 'files/constrSets/@index.txt', "w", encoding="UTF-8")
                for line in lines:
                    f.write(line)
            finally:
                f.close()

def navSetList():
    mydict = {}
    mycounter = 1
    print('Navigation sets:\n')
    f = io.open(cfgPathInstall() + 'files/SS/store/@index.txt', "r", encoding="UTF-8")
    for line in f:
        displayLine = line.strip()
        print('[' + str(mycounter) + ']\t' + displayLine + '\n')
        mydict.update(list(zip(listify(mycounter), listify(tuplify(line.strip())))))
        mycounter += 1
    f.close()
    print()
    return mydict

def loadOrModifyNavSet(inName, loadBool):
    returnString = '1'
    if not isinstance(inName, tuple):
        inName = tuplify(inName)
    # normalise inName to _ separated alphanumeric as used for filenames
    inNameString = "".join([x if x.isalnum() else "_" for x in inName[0].strip()])
    # just read one in
    if loadBool:
        copyBackNavFiles(inNameString)
        # check whether focus still valid
        returnString = checkFocusValid()
    # or update or add/delete
    else:
        if inNameString[-3:] == "___":
            # delete code for named SS/store/ folder
            deleteNavFolder(inNameString[:-3])
            try:
                # update @index file if needed, readback first...
                f = io.open(cfgPathInstall() + 'files/SS/store/@index.txt', "r", encoding="UTF-8")
                lines = f.readlines()
                lines.sort()
                f.close()
                # and then write out again, omitting removed line
                f = io.open(cfgPathInstall() + 'files/SS/store/@index.txt', "w", encoding="UTF-8")
                for line in lines:
                    if not inNameString[:-3] + "\n" == line:
                        f.write(line)
            finally:
                f.close()
        else:
            # copy out code to named SS/store/ folder
            saveNavFiles(inNameString)
            try:
                # update @index file if needed, readback first...
                f = io.open(cfgPathInstall() + 'files/SS/store/@index.txt', "r", encoding="UTF-8")
                lines = f.readlines()
                if inNameString + "\n" not in lines:
                    lines.append(inNameString + "\n")
                lines.sort()
                f.close()
                # and then write out again with new line
                f = io.open(cfgPathInstall() + 'files/SS/store/@index.txt', "w", encoding="UTF-8")
                for line in lines:
                    f.write(line)
            finally:
                f.close()
    return returnString

def checkFocusValid():
    memberList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSMembers.txt', 'r', encoding="UTF-8")
    for line in f:
        memberList.append(line.strip())
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSStructural.txt', 'r', encoding="UTF-8")
    for line in f:
        memberList.append(line.strip())
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSPathMembers.txt', 'r', encoding="UTF-8")
    for line in f:
        memberList.append(line.strip())
    f.close()
    if SSreadFocusValue() in memberList:
        return str(cfg("NAV_SS"))
    else:
        if cfg("NAV_SS") == 5:
            return '5'
        else:
            return '3'

def charCheck(inString, inType): # check whether direction for set drawing is specified
    if inString[-1] in '<>^v+#]' and len(inString) > 1:
        return inString[:-1], inString[-1]
    else:
        if inType == 0:
            return inString, "<"
        else:
            return inString, ">"

def plusCheck(inString): # check whether predicate changing instruction from SCTRF2R is trailed by a cloning '+' instruction
    if inString[-1] == '+':
        return inString[:-1], True
    else:
        return inString, False

def hashCheck(inString): # check whether predicate changing instruction from SCTRF2R is trailed by an 'add caret' '#' instruction, and others
    if inString[-1] == '#':
        return inString[:-1], True
    else:
        return inString, False

def closingSquareBCheck(inString): # check whether return character SCTRF2R is trailed by a ']' instruction
    if inString[-1] == ']':
        return inString[:-1], True
    else:
        return inString, False

def singleQuoteCheck(inString): # check whether return character SCTRF2R is trailed by a ']' instruction
    if inString[-1] == '\'':
        return inString[:-1], True
    else:
        return inString, False

def openingSquareBCheck(inString): # check whether return character SCTRF2R is trailed by a '[' instruction
    if inString[-1] == '[':
        return inString[:-1], True
    else:
        return inString, False

def tildeCheck(inString): # check whether return character SCTRF2R is trailed by a '~' instruction
    if inString[-1] == '~':
        return inString[:-1], True
    else:
        return inString, False

def closingBraceCheck(inString): # check whether return character SCTRF2R is trailed by a ']' instruction
    if inString[-1] == '}':
        return inString[:-1], True
    else:
        return inString, False

def minusCheck(inString): # check whether return character SCTRF2R is trailed by a '-' instruction
    if inString[-1] == '-':
        return inString[:-1], True
    else:
        return inString, False

def starCheck(inString): # check whether return character SCTRF2R is trailed by a '*' instruction
    if inString[-1] == '*':
        return inString[:-1], True
    else:
        return inString, False

def redunTest(inString): # check whether query process instruction from SCTRF2R is trailed by a 'don't remove redundant roles' '+' instruction
    if inString[-1] == '+':
        return False
    else:
        return True

def clauseNatureTest(inString): # check whether query process instruction from SCTRF2R is trailed by a 'don't remove redundant roles' '+' instruction
    if inString[-1] == '+':
        return "include", False
    elif inString[-1] == '-':
        return "exclude", False
    elif inString[-1] == '*':
        return "include", True
    else:
        return "include", False

def setCheck(inString): # parses 'z' input into number and letter
    return inString[:-1], inString[-1].upper()

def writeSSFiles(setEdges, inIdList, postEdgeNodes, sourceId, debugBool):
    # edges
    if debugBool:
        print('1/5: SSAdj.txt')
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSAdj.txt', 'w', encoding="UTF-8")
        for edge in setEdges:
            f.write(edge[0] + "\t" + edge[1] + "\n")
    finally:
        f.close()
    # actual members
    if debugBool:
        print('2/5: SSMembers.txt')
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSMembers.txt', 'w', encoding="UTF-8")
        for node in inIdList:
            f.write(node + "\n")
    finally:
        f.close()
    # actual member ancestors (pathMembers):
    if debugBool:
        print('3/5: SSPathMembers.txt')
    pathList = []
    allAncList = []
    pathCounter = 1
    for item1 in inIdList:
        tempList = ancestorsIDFromConId_IN(item1)
        for item2 in tempList:
            if item2 not in allAncList:
                allAncList.append(item2)
        pathCounter += 1
        if debugBool and (pathCounter % 100 == 0):
            print(pathCounter)
    pathCounter = 1
    for item2 in allAncList:
        if item2 not in postEdgeNodes:
            if item2 not in inIdList:
                if item2 not in pathList:
                    pathList.append(item2)
        pathCounter += 1
        if debugBool and (pathCounter % 100 == 0):
            print(pathCounter)
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSPathMembers.txt', 'w', encoding="UTF-8")
        for node in pathList:
            f.write(node + "\n")
    finally:
        f.close()
    # structural members
    if debugBool:
        print('4/5: SSStructural.txt')
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSStructural.txt', 'w', encoding="UTF-8")
        for node in postEdgeNodes:
            f.write(node + "\n")
    finally:
        f.close()
    # source letter
    if debugBool:
        print('5/5: SSSource.txt')
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSSource.txt', 'w', encoding="UTF-8")
        f.write(sourceId + "\n")
    finally:
        f.close()
    # write transitive closure files
    # read back
    edgeList, actualList, structuralList, pathList = readSSFiles()
    TCList = []
    ancestorDict = {}
    myList = list(set(actualList) | set(structuralList))
    for item in myList:
        ancestors = []
        if conMetaFromConId_IN(item, False)[1] == 1:
            ancestors.append(item)
            ancestors.append(upCalc(item, ancestors, edgeList))
            ancestorDict[item] = [anc for anc in ancestors if anc is not None]
    # write transitive closure file
    TCList = TCToFile(ancestorDict)
    # write transitive closure count file
    TCCountsToFile(myList, TCList, edgeList)

def TCToFile(ancestorDict):
    TCList = []
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSTC.txt', 'w', encoding="UTF-8")
        for thing in ancestorDict:
            if not thing == tok["ROOT"]:
                for thing2 in ancestorDict[thing]:
                    f.write(thing + "\t" + thing2 + "\n")
                    TCList.append([thing, thing2])
    finally:
        f.close()
    return TCList

def TCCountsToFile(myList, TCList, edgeList):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSTCCount.txt', 'w', encoding="UTF-8")
        for thing in myList:
            if not thing == tok["ROOT"]:
                f.write(thing + "\t" + str(len(filterTCList(thing, TCList))) + "\t" + str(len(filterEdgeListRev(thing, edgeList))) + "\n")
    finally:
        f.close()

def filterEdgeList(idIn, edgeList):
    returnList = []
    for row in edgeList:
        if row[0] == idIn:
            returnList.append(row[1])
    return returnList

def filterEdgeListRev(idIn, edgeList):
    returnList = []
    for row in edgeList:
        if row[1] == idIn:
            returnList.append(row[0])
    if returnList == []:
        returnList.append(idIn)
    return returnList

def filterTCList(idIn, TCList):
    returnList = []
    for row in TCList:
        if row[1] == idIn:
            returnList.append(row[0])
    return returnList

def upCalc(idIn, ancestors, edgeList):
    recordsetIn2 = filterEdgeList(idIn, edgeList)
    if not recordsetIn2:
        return ancestors
    else:
        for destinationId in recordsetIn2:
            if destinationId not in ancestors:
                ancestors.append(destinationId)
            upCalc(destinationId, ancestors, edgeList)

def checkCounter(counter, frequency, displayString):
    if counter % frequency == 0:
        clearScreen()
        snapshotRoleHeader()
        print(displayString)

def convertSet(inLetter):
    readbackListInclude = []
    readbackListExclude = []
    readbackListExcludeIds = []
    readbackListMembers = []
    ancestorInClauseList = []
    navigationalGroupersList = []
    navigationalGroupersListExcl = []
    coveredMembers = []
    descendantDict = {}
    ancestorDict = {}
    ancCompareDict = {}
    if len(inLetter) == 1: # simplest - just adds load of singleton clauses
        readbackListInclude = [mem + " | " + pterm(mem) + " |\n" for mem in readFileMembers(cfgPathInstall() + 'sets' + sep() + inLetter + '.txt')]
    else: # alternative: insert code here to refactor member set to constraint set!
        readbackListMembers = readFileMembers(cfgPathInstall() + 'sets' + sep() + inLetter[0] + '.txt')
        readbackListMembersOriginal = [mem for mem in readbackListMembers]
        # main loop
        magLev = 1
        firstTimeBool = True
        if inLetter[1] == "+":
            magThreshold = 257
        else:
            magThreshold = 65
        clauseThreshold = 1

        if cfg("DEMO") == 1:
            memberThreshold = 0
        else:
            memberThreshold = 1

        while magLev < magThreshold and len(readbackListMembers) > memberThreshold: # doubles below if no qualifying rows generated, currently 1,2,4,8
            # find ancestors
            clearScreen()
            snapshotRoleHeader()
            counter = 1
            setSize = len(readbackListMembers)
            readbackAncestors = []
            preReadbackAncestors = []
            prePreReadbackAncestors = []
            for mem in readbackListMembers:
                checkCounter(counter, 200, "Preparing ancestors: [" + str(counter) + " / " + str(setSize) + " members processed]")
                if firstTimeBool:
                    tempAncestors = ancestorsIDFromConId_IN(mem)
                    ancestorDict[mem] = [anc for anc in tempAncestors if (not anc == tok["ROOT"])]
                counter += 1
            prePreReadbackAncestors = ancestorsIDFromConIdList_IN(readbackListMembers)
            if prePreReadbackAncestors != []:
                prePreReadbackAncestors.remove(tok["ROOT"])
            prePreAncSize = len(prePreReadbackAncestors)
            counter = 1
            for anc in prePreReadbackAncestors:
                checkCounter(counter, 100, "Pruning ancestors: [" + str(counter) + " / " + str(prePreAncSize) + "]")
                addBool = True
                if not firstTimeBool:
                    # check if it's an 'outsider' (neither ancestor nor desc) of already matched clause focus
                    # and build up cache of results for subsequent faster processing
                    for ancestor in ancestorInClauseList:
                        if not ancestor + "-" + anc in ancCompareDict:
                            if TCCompare_IN(ancestor, anc):
                                addBool = False
                                ancCompareDict[ancestor + "-" + anc] = True
                                break
                            else:
                                ancCompareDict[ancestor + "-" + anc] = False
                        else:
                            addBool = not ancCompareDict[ancestor + "-" + anc]
                            if not addBool:
                                break
                if addBool:
                    preReadbackAncestors.append(anc)
                counter += 1
            if not firstTimeBool and inLetter[1] == "#": # if trailing '#' to force this (slower) approach
                preAncSize = len(preReadbackAncestors)
                counter = 1
                for anc in preReadbackAncestors:
                    checkCounter(counter, 50, "Pruning ancestors 2: [" + str(counter) + " / " + str(preAncSize) + "]")
                    addBool = True
                    # even if it's an 'outsider', check it actually brings something new
                    if not coversNewMembers(anc, readbackListMembersOriginal, coveredMembers):
                        addBool = False
                        break
                    if addBool:
                        readbackAncestors.append(anc)
                    counter += 1
            else:
                readbackAncestors = [anc for anc in preReadbackAncestors]
            ancLen = len(readbackAncestors)
            n = (setSize /(4 * magLev)) + 1
            preAnalysisList = []
            counter = 1
            for mem in readbackAncestors:
                if firstTimeBool:
                    potentialNum = descendantCountFromConId_IN(mem)
                    descendantDict[mem] = potentialNum
                else:
                    potentialNum = descendantDict[mem]
                # test each clause against original list in case of overlaps
                actualNum, actualList = subsumptionMatchesAnc(ancestorDict, readbackListMembersOriginal, mem)
                rank = actualNum / ((potentialNum - actualNum) + n)
                minusList = []
                checkCounter(counter, 100, "Processing ancs: [mag: " + str(magLev) + "], [" + str(setSize) + " mems], " + "[" + str(counter) + " / " + str(ancLen) + " ancs]")
                if rank >= clauseThreshold and (potentialNum - actualNum) > 0:
                    minusList = list(NOT(set(descendantsCleanIDFromConId_IN(mem)), set(actualList)))
                if (actualNum > 1) or (potentialNum == 1 and actualNum == 1):
                    if cfg("DEMO") == 1:
                        preAnalysisList.append([rank, potentialNum, actualNum, minusList, mem, pterm(mem)])
                    else:
                        preAnalysisList.append([rank, potentialNum, actualNum, minusList, mem])
                counter += 1
            # if any rows and top row meets clauseThreshold:
            if len(preAnalysisList) > 0:
                preAnalysisList = sorted(preAnalysisList, key=lambda x: x[0], reverse=True)
                if preAnalysisList[0][0] >= clauseThreshold:
                    # - building clauses [inclusion] and a list of exclusionIds (for later reconciliation if poss)
                    if preAnalysisList[0][4] in preAnalysisList[0][3]:
                        operatorString = "< "
                        if cfg("DEMO") == 1:
                            navigationalGroupersList.append(preAnalysisList[0][4])
                    else:
                        operatorString = "<< "
                    # create constraint row
                    readbackListInclude.append(operatorString + preAnalysisList[0][4] + " | " + pterm(preAnalysisList[0][4]) + " |\n")
                    # add focus to test list
                    ancestorInClauseList.append(preAnalysisList[0][4])
                    # cache covered members
                    actualNum, actualList = subsumptionMatchesAnc(ancestorDict, readbackListMembersOriginal, preAnalysisList[0][4])
                    for item in actualList:
                        append_distinct(coveredMembers, item)
                    # catch exclusions
                    for item in preAnalysisList[0][3]:
                        if not item == preAnalysisList[0][4]:
                            append_distinct(readbackListExcludeIds, item)
                    descToCheckList = descendantsCleanIDFromConId_IN(preAnalysisList[0][4])
                    for desc in descToCheckList:
                        if desc in readbackListMembers:
                            readbackListMembers.remove(desc)
                if len(preAnalysisList) > 1:
                    preAnalysisList = preAnalysisList[1:]
                # if no more rank > 1, up the magLev
                if preAnalysisList[0][0] < clauseThreshold:
                    magLev *= 2
                firstTimeBool = False
            else:
                readbackListMembers = []
        # tidy up
        # add the remainder as singletons
        for item in readbackListMembers:
            # check not something covered by earlier ancestor clause
            includeMeBool = True
            for ancestor in ancestorInClauseList:
                if TCCompare_IN(item, ancestor):
                    includeMeBool = False
            if includeMeBool:
                append_distinct(readbackListInclude, item + " | " + pterm(item) + " |\n")
        # grouping code
        readbackListExclude, navigationalGroupersListExcl = resolveExclusions(readbackListExcludeIds)
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'ECLset.txt', 'w', encoding="UTF-8")
        for entry in readbackListInclude:
            f.write(entry)
        f.write("EXCLUDE\n")
        for entry in readbackListExclude:
            f.write(entry)
        f.close()
        writeECLsetExt(readbackListInclude, readbackListExclude)
        if cfg("DEMO") == 1 and len(inLetter) > 1:
            # introduce 'disorder' and 'morph abno'
            tempNavList = addInMorphAndDisorder(readbackListMembersOriginal, navigationalGroupersList, False)
            for item in tempNavList:
                append_distinct(navigationalGroupersList, item)
            for item in navigationalGroupersListExcl:
                append_distinct(navigationalGroupersList, item)
            # silently write any new groupers to Z
            writeOutList(navigationalGroupersList, cfgPathInstall() + 'sets' + sep() + 'Z.txt')
    finally:
        f.close()

def identifyGroupersSequence(inLetter):
    navSliceOriginal = cfg("NAV_TOP")
    totalGroupers = []
    tempTotalGroupers = []
    for n in [25, 10, 5, 2]:
        navigationSlice(n)
        tempTotalGroupers = identifyGroupers(inLetter, False)
        for item in tempTotalGroupers:
            append_distinct(totalGroupers, [tempTotalGroupers[item][0], pterm(tempTotalGroupers[item][0])])
    navigationSlice(navSliceOriginal)
    return printSimpleResult(sorted(totalGroupers, key=lambda x: x[1]), True)

def identifyGroupers(inLetter, showBool):
    # convertSet() derivative to identify fully-defined ancestors that may be suitable navigational concepts
    readbackListMembers = []
    navigationalGroupersList = []
    ancestorInClauseList = []
    coveredMembers = []
    descendantDict = {}
    ancestorDict = {}
    ancCompareDict = {}
    # alternative: insert code here to refactor member set to constraint set!
    readbackListMembers = readFileMembers(cfgPathInstall() + 'sets' + sep() + inLetter[0] + '.txt')
    readbackListMembersOriginal = [mem for mem in readbackListMembers]
    # main loop
    magLev = 1
    firstTimeBool = True

    clauseThreshold = 0.001
    magThreshold = 4097

    if cfg("DEMO") == 1:
        memberThreshold = 0
    else:
        memberThreshold = 1

    while magLev < magThreshold and len(readbackListMembers) > memberThreshold: # doubles below if no qualifying rows generated, currently 1,2,4,8
        # find ancestors
        clearScreen()
        snapshotRoleHeader()
        counter = 1
        setSize = len(readbackListMembers)
        readbackAncestors = []
        preReadbackAncestors = []
        prePreReadbackAncestors = []
        for mem in readbackListMembers:
            checkCounter(counter, 200, "Preparing ancestors: [" + str(counter) + " / " + str(setSize) + " members processed]")
            if firstTimeBool:
                tempAncestors = ancestorsIDFromConId_IN(mem)
                ancestorDict[mem] = [anc for anc in tempAncestors if (not anc == tok["ROOT"])]
            counter += 1
        prePreReadbackAncestors = ancestorsIDFromConIdList_IN(readbackListMembers)
        if prePreReadbackAncestors != []:
            prePreReadbackAncestors.remove(tok["ROOT"])
        prePreAncSize = len(prePreReadbackAncestors)
        counter = 1
        for anc in prePreReadbackAncestors:
            checkCounter(counter, 100, "Pruning ancestors: [" + str(counter) + " / " + str(prePreAncSize) + "]")
            addBool = True
            if not firstTimeBool:
                # check if it's an 'outsider' (neither ancestor nor desc) of already matched clause focus
                # and build up cache of results for subsequent faster processing
                for ancestor in ancestorInClauseList:
                    if not ancestor + "-" + anc in ancCompareDict:
                        if TCCompare_IN(ancestor, anc):
                            addBool = False
                            ancCompareDict[ancestor + "-" + anc] = True
                            break
                        else:
                            ancCompareDict[ancestor + "-" + anc] = False
                    else:
                        addBool = not ancCompareDict[ancestor + "-" + anc]
                        if not addBool:
                            break
            if addBool:
                preReadbackAncestors.append(anc)
            counter += 1
        readbackAncestors = [anc for anc in preReadbackAncestors]
        ancLen = len(readbackAncestors)
        n = (setSize /(4 * magLev)) + 1
        preAnalysisList = []
        counter = 1
        for mem in readbackAncestors:
            if firstTimeBool:
                potentialNum = descendantCountFromConId_IN(mem)
                descendantDict[mem] = potentialNum
            else:
                potentialNum = descendantDict[mem]
            # test each clause against original list in case of overlaps
            actualNum, actualList = subsumptionMatchesAnc(ancestorDict, readbackListMembersOriginal, mem)
            rank = actualNum / ((potentialNum - actualNum) + n)
            checkCounter(counter, 100, "Processing ancs: [mag: " + str(magLev) + "], [" + str(setSize) + " mems], " + "[" + str(counter) + " / " + str(ancLen) + " ancs]")
            if actualNum > 1:
                if mem not in readbackListMembersOriginal:
                    # limit to defined or not (TODA could improve to distinguish between tlc chapter types)
                    if cfg("DEF_LIMIT") == 0:
                        preAnalysisList.append([rank, potentialNum, actualNum, mem])
                    elif conMetaFromConId_IN(mem, False)[3] == tok["DEFINED"]:
                        preAnalysisList.append([rank, potentialNum, actualNum, mem])
            counter += 1
        # if any rows, take one 'near the top' (depending upon NAV_TOP setting):
        if len(preAnalysisList) > 0:
            startPoint = int(len(preAnalysisList) * ((cfg("NAV_TOP") if (cfg("NAV_TOP") > 0 and cfg("NAV_TOP") < 100) else 1) / 100))
            if startPoint > 0:
                preAnalysisList = sorted(preAnalysisList, key=lambda x: x[2], reverse=True)[startPoint:]
                # create grouper row
                navigationalGroupersList.append([preAnalysisList[0][3], pterm(preAnalysisList[0][3])])
                # add focus to test list
                ancestorInClauseList.append(preAnalysisList[0][3])
                # cache covered members
                actualNum, actualList = subsumptionMatchesAnc(ancestorDict, readbackListMembersOriginal, preAnalysisList[0][3])
                for item in actualList:
                    append_distinct(coveredMembers, item)
                # handle members used up
                descToCheckList = descendantsCleanIDFromConId_IN(preAnalysisList[0][3])
                for desc in descToCheckList:
                    if desc in readbackListMembers:
                        readbackListMembers.remove(desc)
            if len(preAnalysisList) > 1:
                preAnalysisList = preAnalysisList[1:]
            # if no more rank > 1, up the magLev
            if (preAnalysisList[0][0] < clauseThreshold) or (startPoint == 0):
                magLev *= 2
            if len(preAnalysisList) < 2:
                magLev = magThreshold
            firstTimeBool = False
        else:
            readbackListMembers = []
    # introduce 'disorder' and 'morph abno'
    if inLetter[1] == "@" or inLetter[1] == "[":
        navigationalGroupersList = addInMorphAndDisorder(readbackListMembersOriginal, navigationalGroupersList, True)
    return printSimpleResult(sorted(navigationalGroupersList, key=lambda x: x[1]), showBool)

def addInMorphAndDisorder(readbackListMembersOriginal, navigationalGroupersList, addTermBool):
    disBool = False
    morphAbnoBool = False
    for item in readbackListMembersOriginal:
        if not disBool:
            if TCCompare_IN(item, tok["DISORDER"]):
                disBool = True
        if not morphAbnoBool:
            if TCCompare_IN(item, tok["MORPH_ABNO"]):
                morphAbnoBool = True
        if morphAbnoBool and disBool:
            break
    if disBool:
        if addTermBool:
            append_distinct(navigationalGroupersList, [tok["DISORDER"], pterm(tok["DISORDER"])])
        else:
            append_distinct(navigationalGroupersList, tok["DISORDER"])
    if morphAbnoBool:
        if addTermBool:
            append_distinct(navigationalGroupersList, [tok["MORPH_ABNO"], pterm(tok["MORPH_ABNO"])])
        else:
            append_distinct(navigationalGroupersList, tok["MORPH_ABNO"])
    return navigationalGroupersList

def printSimpleResult(resultList, showBool):
    mydict = {}
    mycounter = 1
    if showBool:
        clearScreen()
        snapshotRoleHeader()
        print("Candidate groupers:\n")
    for item in resultList:
        mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
        if showBool:
            print("[" + str(mycounter) + "]\t" + item[0] + tabString(item[0]) + item[1])
        mycounter += 1
    if showBool:
        print()
    if not resultList is None:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'rolereportId.txt', 'w', encoding="UTF-8")
        for roleresult in resultList:
            f.write(roleresult[0] + '\n')
        f.close()
        roleToSetReportFile()
    return mydict

def subsumptionMatchesAnc(inDict, inList, inId):
    total = 0
    returnList = []
    for item in inList:
        if inId in inDict[item]:
            total += 1
            returnList.append(item)
    return total, returnList

def coversNewMembers(candidateAncestor, readbackListMembersOriginal, coveredMembers):
    potentiallyCovered = [item for item in readbackListMembersOriginal if TCCompare_IN(item, candidateAncestor)]
    # false if coveredMembers contains all elements in potentiallyCovered
    result = not all(elem in coveredMembers for elem in potentiallyCovered)
    return result

def resolveExclusions(readbackListExcludeIds):
    # currently uses a slightly different approach to convertSet(). Might change so they are the same; might not.
    readbackListExclude = []
    ancestorInClauseList = []
    navigationalGroupersList = []
    descendantDict = {}
    ancestorDict = {}
    ancCompareDict = {}
    readbackListExcludeIdsOriginal = [mem for mem in readbackListExcludeIds]
    magLev = 1
    firstTimeBool = True
    while magLev < 257 and len(readbackListExcludeIds) > 0:
        # find ancestors
        readbackAncestors = []
        preReadbackAncestors = []
        clearScreen()
        snapshotRoleHeader()
        counter = 1
        setSize = len(readbackListExcludeIds)
        for mem in readbackListExcludeIds:
            checkCounter(counter, 100, "Preparing exclusion ancestors: [" + str(counter) + " / " + str(setSize) + " members processed]")
            if firstTimeBool:
                tempAncestors = ancestorsIDFromConId_IN(mem)
                ancestorDict[mem] = [anc for anc in tempAncestors if (not anc == tok["ROOT"])]
            counter += 1
        preReadbackAncestors = ancestorsIDFromConIdList_IN(readbackListExcludeIds)
        preReadbackAncestors.remove(tok["ROOT"])
        prePreAncSize = len(preReadbackAncestors)
        counter = 1
        for anc in preReadbackAncestors:
            checkCounter(counter, 100, "Pruning exclusion ancestors: [" + str(counter) + " / " + str(prePreAncSize) + "]")
            addBool = True
            if not firstTimeBool:
                # check if it's an 'outsider' (neither ancestor nor desc) of already matched clause focus
                # and build up cache of results for subsequent faster processing
                for ancestor in ancestorInClauseList:
                    if not ancestor + "-" + anc in ancCompareDict:
                        if TCCompare_IN(ancestor, anc):
                            addBool = False
                            ancCompareDict[ancestor + "-" + anc] = True
                            break
                        else:
                            ancCompareDict[ancestor + "-" + anc] = False
                    else:
                        addBool = not ancCompareDict[ancestor + "-" + anc]
                        if not addBool:
                            break
            if addBool:
                readbackAncestors.append(anc)
            counter += 1
        ancLen = len(readbackAncestors)
        n = (setSize /(4 * magLev)) + 1
        preAnalysisList = []
        counter = 1
        for mem in readbackAncestors:
            if firstTimeBool:
                potentialNum = descendantCountFromConId_IN(mem)
                descendantDict[mem] = potentialNum
            else:
                potentialNum = descendantDict[mem]
            actualNum, actualList = subsumptionMatchesAnc(ancestorDict, readbackListExcludeIdsOriginal, mem)
            rank = actualNum / ((potentialNum - actualNum) + n)
            minusList = []
            checkCounter(counter, 100, "Processing exclusions: [mag: " + str(magLev) + "], [" + str(counter) + " / " + str(ancLen) + " ancs]")
            if rank >= 1 and (potentialNum - actualNum) > 0:
                minusList = list(NOT(set(descendantsCleanIDFromConId_IN(mem)), set(actualList)))
            if (actualNum >= 1) or (potentialNum == 1 and actualNum == 1):
                preAnalysisList.append([rank, potentialNum, actualNum, minusList, mem])
            counter += 1
        preAnalysisList = sorted(preAnalysisList, key=lambda x: x[0], reverse=True)
        processableBool = False
        if len(preAnalysisList) > 0:
            if preAnalysisList[0][0] >= 1 and (preAnalysisList[0][1] - preAnalysisList[0][2] <= 1):
                # if all descendants and self
                if len(preAnalysisList[0][3]) == 0:
                    operatorString = "<< "
                    processableBool = True
                # if al  descendants (not self)
                elif len(preAnalysisList[0][3]) == 1:
                    if preAnalysisList[0][3][0] == preAnalysisList[0][4]:
                        operatorString = "< "
                        if cfg("DEMO") == 1:
                            navigationalGroupersList.append(preAnalysisList[0][4])
                        processableBool = True
                # otherwise cannot be processed
                if processableBool:
                    # add clause
                    readbackListExclude.append(operatorString + preAnalysisList[0][4] + " | " + pterm(preAnalysisList[0][4]) + " |\n")
                    # add focus to list of ancestors already used (which is then used in pruning steps above)
                    ancestorInClauseList.append(preAnalysisList[0][4])
                    # delete covered exclusions
                    descToCheckList = descendantsCleanIDFromConId_IN(preAnalysisList[0][4])
                    for desc in descToCheckList:
                        if desc in readbackListExcludeIds:
                            readbackListExcludeIds.remove(desc)
                if len(preAnalysisList) > 1:
                    preAnalysisList = preAnalysisList[1:]
            # if no more rank > 1, up the magLev
            if preAnalysisList[0][0] < 1 or (preAnalysisList[0][0] >= 1 and not processableBool):
                magLev *= 2
            firstTimeBool = False
        else:
            readbackListExcludeIds = []
    for item in readbackListExcludeIds:
        appendBool = True
        for anc in ancestorInClauseList:
            if TCCompare_IN(item, anc):
                appendBool = False
                break
        if appendBool:
            readbackListExclude.append(item + " | " + pterm(item) + " |\n")
    return readbackListExclude, navigationalGroupersList

def readSSFiles():
    edgeList = []
    actualList = []
    structuralList = []
    pathList = []
    # edges
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSAdj.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            edgeList.append([line.split("\t")[0], line.strip().split("\t")[1]])
    finally:
        f.close()
    # actual members
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSMembers.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            actualList.append(line.strip())
    finally:
        f.close()
    # path members
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSPathMembers.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            pathList.append(line.strip())
    finally:
        f.close()
    # structural members
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSStructural.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            structuralList.append(line.strip())
    finally:
        f.close()
    return edgeList, actualList, structuralList, pathList

def readSSSource():
    sourceString = ""
    # source set letter(s)
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSSource.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            sourceString = line.strip()
    finally:
        f.close()
    return list(sourceString)

def dotDrawSet(preInSet1, inBTDir): # draw dot file for set of codes (needs a 't', 'g' or 'h' call first to pull into a lettered set file)
    mydict = {}
    if len(preInSet1) == 2:
        inSet1 = preInSet1[0]
        augmentSet = [a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + preInSet1[1] + '.txt')]
    else:
        inSet1 = preInSet1
        augmentSet = tagList
    if not inSet1 in "012356":
        navSS(1)
        inIdList = [a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet1 + '.txt')]
        setNodes = []
        setEdges = []
        writeableNodes = []
        postEdgeNodes = []
        inactiveNodes = []
        inactiveEdges = []
        # included to keep check obj value color happy!
        ancestorNodes = ancestorsFromConId_IN(inIdList[0], True)
        PPSList = [PPS[0] for PPS in PPSFromAncestors_IN(ancestorNodes)]
        for item in inIdList:
            conMeta = []
            conMeta = conMetaFromConId_IN(item, False)
            if len(conMeta) == 4:
                setNodes.append([item, pterm(item)])
            else:
                inactiveNodes.append([item, pterm(item)])
        inactiveEdges = identifyInactiveEdges(setNodes, inactiveNodes)
        debugBool = (True if inBTDir == "]" else False)
        sendInIdList = inIdList[:]
        if inBTDir == "#": # cheat to include all augmentSet members - defaults to RL below
            setEdges = adjacencyBuild(setToGraph(sendInIdList, debugBool, augmentSet), debugBool)
        else:
            setEdges = adjacencyBuild(setToGraph(sendInIdList, debugBool, setToGraphAugmentTest(sendInIdList, augmentSet)), debugBool)
        for edge in setEdges:
            postEdgeNodes.append(edge[0])
            postEdgeNodes.append(edge[1])
        postEdgeNodes = list(set(postEdgeNodes))
        postEdgeNodes = [node for node in postEdgeNodes if node not in inIdList]
        writeSSFiles(setEdges, inIdList, postEdgeNodes, preInSet1, debugBool)
        if inBTDir != "]":
            if cfg("GRAPH_ANGLE") in (1, 3):
                splineString = "; splines=ortho"
            else:
                splineString = ""
            for node in postEdgeNodes:
                if hasDef(node) and cfg("SHOW_DEFS") > 4:
                    if cfg("GRAPH_ANGLE") in (2, 3):
                        ptermLen = len(fsnFromConId_IN(node)[1])
                    else:
                        ptermLen = len(pterm(node))
                    defString = "\l\l" + toString(textwrap.wrap(simplify(onlyDefTermsFromConId_IN(node)[1].replace(' (*)', '')), (50 if ptermLen < 50 else ptermLen))) + "\\n\", "
                else:
                    defString = "\l\", "
                if cfg("GRAPH_ANGLE") in (2, 3):
                    if cfg("SHOW_DEFS") > 4:
                        writeableNodes.append("\"" + node + "\" [label=\"" + node + "\l" + simplify(fsnFromConId_IN(node)[1]) + defString + checkObjValueColor(node, False, PPSList) + "];\n")
                    else:
                        writeableNodes.append("\"" + node + "\" [label=\"" + node + "\l" + simplify(fsnFromConId_IN(node)[1]) + "\l\", shape=box, peripheries=2, style=filled, fillcolor=pink];\n")
                else:
                    if cfg("SHOW_DEFS") > 4:
                        writeableNodes.append("\"" + node + "\" [label=\"" + node + "\l" + simplify(pterm(node)) + defString + checkObjValueColor(node.strip(), False, PPSList) + "];\n")
                    else:
                        writeableNodes.append("\"" + node + "\" [label=\"" + node + "\l" + simplify(pterm(node)) + "\l\", shape=box, peripheries=2, style=filled, fillcolor=pink];\n")
            for node in setNodes:
                if hasDef(node[0]) and cfg("SHOW_DEFS") > 4:
                    if cfg("GRAPH_ANGLE") in (2, 3):
                        ptermLen = len(fsnFromConId_IN(node[0])[1])
                    else:
                        ptermLen = len(pterm(node[0]))
                    defString = "\l\l" + toString(textwrap.wrap(simplify(onlyDefTermsFromConId_IN(node[0])[1].replace(' (*)', '')), (50 if ptermLen < 50 else ptermLen))) + "\\n\", "
                else:
                    defString = "\l\", "
                if cfg("GRAPH_ANGLE") in (2, 3):
                    writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(fsnFromConId_IN(node[0])[1]) + defString + checkObjValueColor(node[0].strip(), False, PPSList) + "];\n")
                else:
                    writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + defString + checkObjValueColor(node[0].strip(), False, PPSList) + "];\n")
            for node in inactiveNodes:
                if cfg("GRAPH_ANGLE") in (2, 3):
                    writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(fsnFromConId_IN(node[0])[1]) + "\l\", shape=box, peripheries=1, style=filled, fillcolor=palegreen];\n")
                else:
                    writeableNodes.append("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + simplify(node[1]) + "\l\", shape=box, peripheries=1, style=filled, fillcolor=palegreen];\n")
            try:
                f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'ssview_temp.dot', 'w', encoding="UTF-8")
                if inBTDir == "#": # default RL edge flow for all augmentSet member option.
                    inBTDir = "<"
                dirString = edgeDirection(inBTDir)
                f.write("strict digraph G { rankdir=" + dirString + splineString + "; ranksep=.3; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
                for edge in sorted_cfg(setEdges):
                    f.write("\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=onormal];\n")
                for edge in sorted_cfg(inactiveEdges):
                    f.write("\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=palegreen];\n")
                for node in sorted_cfg(writeableNodes):
                    f.write(node)
                f.write("}")
            finally:
                f.close()
                renameDiagramFile('ssview_temp.dot', 'ssview.dot')
        else:
            clearScreen()
            snapshotRoleHeader()
        if not inSet1 == "Z": # Y becomes Z in gephi build - skip this step
            mydict = SSbrowseFromId(tok["ROOT"])
            focusSwapWrite(tok["ROOT"])
        return mydict
    else:
        if inSet1 == "3":
            navSS(1)
            mydict = SSbrowseFromId(tok["ROOT"])
        else:
            if inSet1 == '6':
                swapSSView('toNormal')
                inSet1 = '1'
                resetRoleAndPreDot(False)
            if inSet1 == '5':
                swapSSView('toSet')
                inSet1 = '2'
                drawPics(13)
            # insert here
            navSS(int(inSet1))
            mydict = SSbrowseFromId(readFocusValue()[0])
        return mydict

def swapSSView(inDir):
    if inDir == 'toNormal':
        checkWords = ("\"#BD00FF\"", "\"#0078FF\"", "\"#0079FF\"")
        repWords = ("pink", "\"#99CCFF\"", "\"#CCCCFF\"")
    else:
        checkWords = ("pink", "\"#99CCFF\"", "\"#CCCCFF\"")
        repWords = ("\"#BD00FF\"", "\"#0078FF\"", "\"#0079FF\"")
    outList = []
    # read and replace
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'ssview.dot', 'r', encoding="UTF-8")
        for line in f:
            for check, rep in zip(checkWords, repWords):
                line = line.replace(check, rep)
            outList.append(line)
    finally:
        f.close()
    # writeout
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'ssview_temp.dot', 'w', encoding="UTF-8")
        for line in outList:
            f.write(line)
    finally:
        f.close()
        renameDiagramFile('ssview_temp.dot', 'ssview.dot')

def identifyInactiveEdges(setNodes, inactiveNodes):
    inactiveEdges = []
    setNodeIds = [setNode[0] for setNode in setNodes]
    # find any history targets for inactiveNodes that are in new writeableNodes
    for node in inactiveNodes:
        historyTargets = historicalDestinationsFromConId_IN(node[0], 1, 1)
        # create inactiveEdges
        if historyTargets == []:
            inactiveEdges.append([node[0], tok['ROOT']])
        else:
            for historyTarget in historyTargets:
                if historyTarget[2] in setNodeIds:
                    inactiveEdges.append([node[0], historyTarget[2]])
                else:
                    inactiveEdges.append([node[0], tok['ROOT']])
    return inactiveEdges

def dotDrawDefs(inSet1, inBTDir):
    reConfigBool = False
    if inBTDir in ("#", "]"):
        reConfigBool = True
        if inBTDir == "#":
            inBTDir = ">"
        else:
            inBTDir = "^"
    idList = [a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet1 + '.txt')]
    if reConfigBool:
        tempList1 = []
        tempList2 = []
        cfgList = setConfigsFullDiag(8)
        for item in idList:
            tempList1 = growValues(item, False)
            for item2 in tempList1:
                if item2 not in tempList2:
                    tempList2.append(item2)
        idList = tempList2[:]
    dotDrawFocus("", idList, inBTDir)
    if reConfigBool:
        resetConfigsFullDiag(cfgList)
    myDict = readBackFile('sets' + sep() + inSet1 + '.txt', 'Draw definitions for ' + inSet1, False, True)
    return myDict


def SSbrowseFromId(inId):
    # mini version of browseFromId to allow navigation around a set of codes
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    SSfocusSwapWrite(inId[0])
    mydict = {}
    mycounter = 1
    # supertypes
    myId = inId[0]
    parentList, actualList, structuralList, pathList = parentsFromId_SS(myId)
    if len(parentList) > 0:
        print('Parents: [' + str(len(parentList)) + ']')
        for listParent in parentList:
            if not listParent is None:
                if cfg("NAV_SS") == 1 or cfg("NAV_SS") == 2:
                    if listParent[1] in actualList:
                        print('[' + str(mycounter) + ']\t\033[36m' + listParent[1] + tabString(listParent[1]) + listParent[0] + "\033[92m")
                    elif listParent[1] in structuralList:
                        print('[' + str(mycounter) + ']\t\033[95m' + listParent[1] + tabString(listParent[1]) + listParent[0] + "\033[92m")
                    elif SSPathTest(listParent[1], pathList):
                        print('[' + str(mycounter) + ']\t\033[93m' + listParent[1] + tabString(listParent[1]) + listParent[0] + "\033[92m")
                    else:
                        print('[' + str(mycounter) + ']\t\033[92m' + listParent[1] + tabString(listParent[1]) + listParent[0] + "\033[92m")
                else:
                    print('[' + str(mycounter) + ']\t\033[92m' + listParent[1] + tabString(listParent[1]) + listParent[0] + "\033[92m")
                mydict.update(list(zip(listify(mycounter), listify(tuplify(listParent[1])))))
                mycounter += 1
        print()

    print('Focus:')
    if cfg("NAV_SS") == 1 or cfg("NAV_SS") == 2:
        if myId in actualList:
            print('[' + str(mycounter) + ']\t\033[36m' + activeColor_IN(myId) + tabString(myId) + '\033[1m' + SSpterm(myId, False) + '\033[22m' + "\033[92m")
            print('\t' + (' ' * len(myId)) + tabString(myId) + '\033[36;2m' + SSpterm(myId, True) + "\033[92;22m")
        elif myId in structuralList:
            print('[' + str(mycounter) + ']\t\033[95m' + activeColor_IN(myId) + tabString(myId) + '\033[1m' + SSpterm(myId, False) + '\033[22m' + "\033[92m")
            print('\t' + (' ' * len(myId)) + tabString(myId) + '\033[95;2m' + SSpterm(myId, True) + "\033[92;22m")
        elif SSPathTest(myId, pathList):
            print('[' + str(mycounter) + ']\t\033[93m' + activeColor_IN(myId) + tabString(myId) + '\033[1m' + SSpterm(myId, False) + '\033[22m' + "\033[92m")
            print('\t' + (' ' * len(myId)) + tabString(myId) + '\033[93;2m' + SSpterm(myId, True) + "\033[92;22m")
        else:
            print('[' + str(mycounter) + ']\t\033[92m' + activeColor_IN(myId) + tabString(myId) + '\033[1m' + SSpterm(myId, False) + '\033[22m' + "\033[92m")
            print('\t' + (' ' * len(myId)) + tabString(myId) + '\033[92;2m' + SSpterm(myId, True) + "\033[92;22m")
    else:
        print('[' + str(mycounter) + ']\t\033[92m' + activeColor_IN(myId) + tabString(myId) + '\033[1m' + pterm(myId) + '\033[22m' + "\033[92m")
        print('\t' + (' ' * len(myId)) + tabString(myId) + '\033[92;2m' + SSpterm(myId, True) + "\033[92;22m")
    mydict.update(list(zip(listify(mycounter), listify(tuplify(myId)))))
    mycounter += 1
    print()

    # subtypes
    childList, actualList, structuralList, pathList = childrenFromIdWithPlus_SS(myId)  # call interface neutral code
    print('Children: [' + str(len(childList)) + ']')
    for listChild in childList:
        if not listChild is None:
            if cfg("NAV_SS") == 1 or cfg("NAV_SS") == 2:
                if listChild[2] in actualList:
                    print('[' + str(mycounter) + ']\t\033[36m' + listChild[1] + listChild[2] + tabString(listChild[1] + listChild[2]) + listChild[0] + "\033[92m")
                elif listChild[2] in structuralList:
                    print('[' + str(mycounter) + ']\t\033[95m' + listChild[1] + listChild[2] + tabString(listChild[1] + listChild[2]) + listChild[0] + "\033[92m")
                elif SSPathTest(listChild[2], pathList):
                    print('[' + str(mycounter) + ']\t\033[93m' + listChild[1] + listChild[2] + tabString(listChild[1] + listChild[2]) + listChild[0] + "\033[92m")
                else:
                    print('[' + str(mycounter) + ']\t\033[92m' + listChild[1] + listChild[2] + tabString(listChild[1] + listChild[2]) + listChild[0] + "\033[92m")
            else:
                print('[' + str(mycounter) + ']\t\033[92m' + listChild[1] + listChild[2] + tabString(listChild[1] + listChild[2]) + listChild[0] + "\033[92m")
            mydict.update(list(zip(listify(mycounter), listify(tuplify(listChild[2])))))
            mycounter += 1
    print()
    if (cfg("DRAW_DIAG") == 13) and cfg("NAV_SS") == 2:
        dotDrawTreeMapIsh(inId, mydict, False, False, True)
    dictToSetreportIdFile(mydict)
    return mydict

def SSPathTest(inId, inList):
    if inId in inList:
        return True
    else:
        return False

def parentsFromId_SS(myId):
    parentList = []
    # code to interrogate SSAdj.txt to find parents
    edgeList, actualList, structuralList, pathList = readSSFiles()
    if cfg("NAV_SS") == 0 or cfg("NAV_SS") == 2:
        parentList = parentsFromId_IN(tuplify(myId))
        if len(parentList) > 0:
            if parentList[0][1] == tok["ROOT"] and cfg("NAV_SS") == 2:
                parentList = [[SSpterm(tok["ROOT"], False), tok["ROOT"]]]
        parentList.sort(key=lambda x: x[0].lower())
        return parentList, actualList, structuralList, pathList
    else:
        for edge in edgeList:
            if edge[0] == myId:
                parentList.append([SSpterm(edge[1], False), edge[1]])
        parentList.sort(key=lambda x: x[0].lower())
        return parentList, actualList, structuralList, pathList

def childrenFromIdWithPlus_SS(myId):
    childList = []
    # code to interrogate SSAdj.txt to find children (and whether they have children)
    edgeList, actualList, structuralList, pathList = readSSFiles()
    if cfg("NAV_SS") == 0 or cfg("NAV_SS") == 2:
        childList = childrenFromIdWithPlus_IN(tuplify(myId))
        # childList.sort(key=lambda x: x[0].lower())
        return childList, actualList, structuralList, pathList
    else:
        for edge in edgeList:
            if edge[1] == myId:
                # check if child has descendants
                if len([innerEdge[0] for innerEdge in edgeList if innerEdge[1] == edge[0]]) > 0:
                    childList.append([SSpterm(edge[0], False), "+", edge[0]])
                else:
                    childList.append([SSpterm(edge[0], False), " ", edge[0]])
        childList.sort(key=lambda x: x[0].lower())
        return childList, actualList, structuralList, pathList

def SSpterm(inId, isFocus):
    if isFocus:
        termString = fsnFromConId_IN(inId)[1]
    else:
        termString = pterm(inId)
    if inId == tok["ROOT"] and not isFocus:
        postFixLetters = readSSSource()
        if len(postFixLetters) == 1:
            postFix = " [Set " + postFixLetters[0] + "]"
        else:
            postFix = " [Set " + postFixLetters[0] + " with " + postFixLetters[1] + "]"
        returnString = termString + postFix
    else:
        returnString = termString
    return returnString

def navSS(inOption):
    cfgWrite("NAV_SS", inOption)

def showRefsetMatches(callId, testSections):
    mydict = {}
    counter = 1
    refsetType = 'simpleRefset'
    refsetDict = {}
    chapterDict = {}

    # calculate chapters
    print("Comparing with top level chapters...")
    chapterLeftCount, chapterRows, refsetDict = refSetMembersCompareFromConIds_IN(callId, "", refsetType, 1, {}, refsetDict, False, 0)
    # calculate letter entry for chapterDict
    tempList = []
    for row in chapterRows:
        tempList.append(row[0])

    # calculate refset matches
    if testSections == 2: # if just 'KN' or 'KN+' do the refsets too
        otherRows = []
        clearScreen()
        snapshotRoleHeader()
        print("Preparing refsets for comparison...")
        idIn2List = [item[0] for item in descendantsFromConId_IN(tok["SIMPLE_REFSETS"])]
        # calculate refset entries in chapterdict
        chapterDict = refSetChaptersRefTableFromConIds_IN(chapterDict, idIn2List)
        # add in letter entry for chapterdict
        if len(tempList) > 0:
            chapterDict[callId] = tempList
        itemCounter = 0
        total = len(idIn2List)
        leftCount = 0
        for callId2 in idIn2List:
            if ((total - itemCounter) % 10 == 0) or (total < 20):
                clearScreen()
                snapshotRoleHeader()
                n = int((total - itemCounter) * (60 / total))
                print("Compare members: " + ('*' * n))
            leftCountPre = 0
            leftCountPre, otherRowsPre, refsetDict = refSetMembersCompareFromConIds_IN(callId, callId2, refsetType, 2, chapterDict, refsetDict, False, 0)
            leftCount += leftCountPre
            if len(otherRowsPre) == 1:
                otherRows.append(otherRowsPre[0])
            itemCounter += 1
        otherRows.sort(key=lambda x: (x[3], x[6]), reverse=True)

    # final printout
    clearScreen()
    snapshotRoleHeader()
    # if 'KN-' only do the chapters
    print("Set '" + callId + "' membership details:\n")
    print("Content from " + str(chapterLeftCount) + " chapter(s):\n")
    for row in chapterRows:
        print("[" + str(counter) + "]\t" + proportionColor_IN(row[3]) + (" " * (6 - len(str(row[3])))) + row[0] + tabString("(" + str(row[3]) + "%) " + row[0]) + pterm(row[0]))
        mydict.update(list(zip(listify(counter), listify(tuplify(row[0])))))
        counter += 1
    print()

    if testSections == 2: # if just 'KN' or 'KN+' do the refsets too
        print("ReferencedComponentId matches with " + str(leftCount) + " '" + refsetType + "' refset(s):\n")
        for row in otherRows:
            print("[" + str(counter) + "]\t" + proportionColor_IN(row[3]) + (" " * (6 - len(str(row[3])))) + row[0] + tabString("(" + str(row[3]) + "%) " +row[0]) + pterm(row[0]) + " " + proportionColor_IN(row[4]))
            mydict.update(list(zip(listify(counter), listify(tuplify(row[0])))))
            counter += 1
        print()
    dictToSetreportIdFile(mydict)
    return mydict

def dictToSetreportIdFile(inDict):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'setreportId.txt', 'w', encoding="UTF-8")
        for item in inDict:
            f.write(inDict[item][0] + '\n')
    finally:
        f.close()

def growValuesForDiagram(inId):
    modelledValues = []
    modelledValues.append(inId)
    modelledValuesCountBeginning = 1
    modelledValuesCountEnd = 0
    while modelledValuesCountBeginning != modelledValuesCountEnd:
        modelledValuesCountBeginning = len(modelledValues)
        for item in modelledValues:
            roleList = allRolesFromConId_IN(item) # first set of roles
            for role in roleList: # work through each role...
                roleListInner = allRolesFromConId_IN(role[2])
                if len(roleListInner) > 0: # if any role value carries roles itself...
                    if not role[2] in modelledValues:
                        modelledValues.append(role[2]) # add to return collection
        modelledValuesCountEnd = len(modelledValues)
    return modelledValues

def navigationSlice(inInteger):
    cfgWrite("NAV_TOP", inInteger)
    return inInteger

def onlyDefined(inInteger):
    cfgWrite("DEF_LIMIT", inInteger)
    return inInteger

def demoValue(inInteger):
    cfgWrite("DEMO", inInteger)
    return inInteger

def dotProcess(topaction):
    returnAction = ""
    if topaction == '.':
        cfgWrite("SHOW_INDEX_SET", 0)
        topaction = 'b'
    elif topaction == '..':
        cfgWrite("SHOW_INDEX_SET", 1)
        topaction = 'b'
    returnAction = topaction
    return returnAction

def groupByModule(inSet):
    mydict = {}
    mycounter = 1
    # generate module list
    moduleList = descendantsFromConId_IN(tok["MODULES"])
    moduleDict = { mod[1] : [] for mod in moduleList }
    # read in set
    myList = [a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet + '.txt')]
    for item in myList:
        tempMod = isActiveFromConId_IN(item)[0]
        moduleDict[tempMod].append(item)
    print("Set " + inSet + " grouped by (concept) module:\n")
    for module in moduleList:
        tempList = []
        if len(moduleDict[module[1]]) > 0:
            for thing in moduleDict[module[1]]:
                tempList.append((thing, pterm(thing)))
        tempList.sort(key=lambda x: x[1].lower())
        if len(tempList) > 0:
            print('\033[4m[' + str(mycounter) + ']\t' + activeColor_IN(module[0]) + tabString(module[0]) + module[1] + '\033[24m')
            mydict.update(list(zip(listify(mycounter), listify(tuplify(module[0])))))
            mycounter += 1
            print()
            for member in tempList:
                print('[' + str(mycounter) + ']\t' + activeColor_IN(member[0]) + tabString(member[0]) + member[1])
                mydict.update(list(zip(listify(mycounter), listify(tuplify(member[0])))))
                mycounter += 1
            print()
    print()
    dictToSetreportIdFile(mydict)
    return mydict

def upSetKeyConvert2(inKey, inDict):
    outString = ""
    tempList = inKey.split("|")
    for n in range(1,len(inDict) + 1):
        if str(n) in tempList:
            outString += str(n)
        else:
            outString += " "
    # return '\033[4;7m' + outString + "\033[24;27m | "
    return outString + " | "

def upSetKeyConvert3(inKey, inDict):
    outString = ""
    tempList = inKey.split("|")
    for n in range(1,len(inDict) + 1):
        if str(n) in tempList:
            outString += str(n)
        else:
            outString += " "
    return outString

def upSetKeyConvert5(inKey, inList):
    outString = ""
    tempList = inKey.split("|")
    for n in inList:
        if n in tempList:
            outString += n
        else:
            outString += " "
    if len(inKey) == 1:
        return outString + " | "
    else:
        return outString

def upSetKeyConvert4(inKey):
    idxs = [i for i in range(0, len(inKey)) if inKey[i].isalnum()]
    outString = inKey[0:idxs[0]] + inKey[idxs[0]:idxs[-1]].replace(" ", "\u2013") + inKey[idxs[-1]:] + " | "
    # outString = '\033[4;7m' + inKey + "\033[24;27m | "
    return outString

def fillWithMembers(inId, inType):
    memberList = []
    if inType == 1:
        memberList = [item[0] for item in descendantsIDFromConId_IN(inId)]
    elif inType == 2:
        myRefset = refSetTypeFromConId_IN(inId)
        for refset in myRefset:
            refsetType = refset[0]
        memberList = [item[0] for item in refSetMembersFromConId_IN(inId, refsetType, 0, 0)]
    return memberList

def keyToIdAndMembers(inKey, inKeyToMemIdDict, singleKeyDict, inMemberDict):
    idList = []
    for character in inKey:
        if character in inKeyToMemIdDict:
            idList.append(inKeyToMemIdDict[character])
    if len(inKey) == 1:
        memberList = inMemberDict[singleKeyDict[inKey]]
    else:
        memberList = inMemberDict[inKey]
    outTuple = (idList, memberList)

    return outTuple

def checkOperator(code):
    if TCCompare_IN(code, tok["REFSETS"]):
        operator = "^ "
    else:
        operator = "<< "
    return operator

def writeOutUpSet(inList):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'roleTemplates' + sep() + 'QQ.txt', 'w', encoding="UTF-8")
        if len(inList) == 1 and not inList[0][0].isnumeric():
            if inList[0][0] in "^<":
                f.write(inList[0])
            else:
                f.write("^ SET_" + inList[0] + " | Local set " + inList[0] + " |")
        else:
            for code in inList[:-1]:
                f.write(checkOperator(code) + code + "| " + pterm(code) + " | +\n")
            f.write(checkOperator(inList[-1]) + inList[-1] + "| " + pterm(inList[-1]) + " |")
    finally:
        f.close()

def upSetEmulOneLetter(inSet, sortByValDir):
    if sortByValDir == "+":
        sortByVal = False
    else:
        sortByVal = True
    mydict = {}
    preMemberList = []
    memberList = []
    memberIndexDict = {}
    memberIndexDictRev = {}
    preMemberList = [(a, pterm(a)) for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet + '.txt')]
    preMemberList.sort(key=lambda x: x[1])
    memberList = [a[0] for a in preMemberList]
    counter = 1
    for member in memberList:
        memberIndexDict[member] = str(counter)
        memberIndexDictRev[str(counter)] = member
        counter += 1
    memberDict = {}
    for item in memberList:
        if TCCompare_IN(item, tok["REFSETS"]):
            memberDict[memberIndexDict[item]] = fillWithMembers(item, 2)
        else:
            memberDict[memberIndexDict[item]] = fillWithMembers(item, 1)
    comparePattern = sorted([item for item in get_power_set([memberIndexDict[member] for member in memberList]) if item != []])
    resultDictSinglePre = {}
    resultDictSingle = {}
    resultDictVals = {}
    resultDict = {}
    singleKeyDict = {}
    for pattern in comparePattern:
        d = []
        for item in pattern:
            d.append(memberDict[item])
        eachResult = list(set.intersection(*[set(x) for x in d]))
        if len(d) == 1:
            resultDictSinglePre["|".join(pattern)] = len(eachResult)
            singleKeyDict["|".join(pattern)] = upSetKeyConvert3("|".join(pattern), memberIndexDict)
        else:
            resultDict[upSetKeyConvert3("|".join(pattern), memberIndexDict)] = len(eachResult)
        resultDictVals[upSetKeyConvert3("|".join(pattern), memberIndexDict)] = eachResult
    for item in preMemberList:
        resultDictSingle[memberIndexDict[item[0]]] = resultDictSinglePre[memberIndexDict[item[0]]]
    mydict = drawUpSetEmul(sortByVal, memberList, memberIndexDict, memberIndexDictRev, resultDictSingle, resultDictVals, resultDict, singleKeyDict)
    return mydict

def drawUpSetEmul(sortByVal, memberList, memberIndexDict, memberIndexDictRev, resultDictSingle, resultDictVals, resultDict, singleKeyDict):
    mydict = {}
    # sort by values
    if sortByVal:
        resultDict = dict(sorted(resultDict.items(), key=lambda item: -item[1]))
    keyListSingle = list(resultDictSingle.keys())
    keyList = list(resultDict.keys())
    maxValue = 0
    for key in keyListSingle:
        if resultDictSingle[key] > maxValue:
            maxValue = resultDictSingle[key]
    print("UpSetPlot-style comparison between sets:")
    print()
    mycounter = 1
    for member in memberList:
        if member.isalpha():
            print('[' + str(mycounter) + ']\t' + "^ SET_" + member + " | Local set " + member + " |")
            mydict.update(list(zip(listify(mycounter), listify(tuplify(["^ SET_" + member + " | Local set " + member + " |"])))))
        else:
            print('[' + str(mycounter) + ']\t' + checkOperator(member) + member + tabString(checkOperator(member) + member) + pterm(member))
            mydict.update(list(zip(listify(mycounter), listify(tuplify([checkOperator(member) + member + " | " + pterm(member) + " |"])))))
        mycounter += 1
    print()
    # sequence for individual sets
    for key in keyListSingle:
        starMult = int((resultDictSingle[key]) * (50 / maxValue))
        print('[' + str(mycounter) + ']\t' + \
                (upSetKeyConvert5(key, memberList) if key.isalpha() else upSetKeyConvert2(key, memberIndexDict)) + '\033[4;7m' + '*' * starMult + '\033[24;27m' + \
                (" " if starMult > 0 else "") + \
                (str(resultDictSingle[key]) if resultDictSingle[key] > 0 else "") + \
                singletonTerm(resultDictSingle[key], resultDictVals[singleKeyDict[key]]))
        mydict.update(list(zip(listify(mycounter), listify(keyToIdAndMembers(key, memberIndexDictRev, singleKeyDict, resultDictVals)))))
        mycounter += 1
    print()
    # rest of list comparisons
    # sort by keys
    if not sortByVal:
        keyList = [key[0] for key in sorted([[item, item.replace(" ", "")] for item in keyList], key=lambda x: x[1])]
    for key in keyList:
        if (resultDict[key] > 0) or (not sortByVal):
            starMult = int((resultDict[key]) * (50 / maxValue))
            print('[' + str(mycounter) + ']\t' + \
                    upSetKeyConvert4(key) + '\033[4;7m' + '*' * starMult + '\033[24;27m' + \
                    (" " if starMult > 0 else "") + \
                    (str(resultDict[key]) if resultDict[key] > 0 else "") + \
                    singletonTerm(resultDict[key], resultDictVals[key]))
            mydict.update(list(zip(listify(mycounter), listify(keyToIdAndMembers(key, memberIndexDictRev, singleKeyDict, resultDictVals)))))
            mycounter += 1
    print("\n[V] plus index to see intersection values.\n")
    return mydict

def singletonTerm(countForKey, identifierForKeySet):
    if countForKey == 1:
        identifierForKey = identifierForKeySet[0]
        termString = pterm(identifierForKey)
        termStringLen = len(termString) + len(identifierForKey)
        if termStringLen <= 48:
            return " [" + identifierForKey + " " + pterm(identifierForKey) + "]"
        else:
            return " [" + identifierForKey + " " + pterm(identifierForKey)[:45 - len(identifierForKey)] + "...]"
    else:
        return ""

def swapTarget(inTarget, returnString):
    targetLetter = returnString[inTarget][0][0][6]
    outTarget =  1
    key_list = list(returnString.keys())
    for key in key_list:
        if len(returnString[key][0][0]) == 1 and returnString[key][0][0][0] == targetLetter:
            outTarget = key
            break
    return outTarget


def upSetEmulInspect(inTarget, returnString):
    mydict = {}
    writeOutUpSet(returnString[int(inTarget)][0])
    rowList = processRoleTxt(cfgPathInstall() + 'files' + sep() + 'roleTemplates' + sep() + "QQ" + '.txt')
    writeOutLines(rowList)
    # returnString = readInConstraint(True)
    writeECL(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'role_edit.txt', False)
    print("Result:\n")
    if returnString[int(inTarget)][0][0][0] == "^":
        inTarget = swapTarget(int(inTarget), returnString)
    readFromDict = True
    mycounter = 1
    if returnString[int(inTarget)][0][0][0] == "<":
        readFromDict = False
    if readFromDict:
        for member in returnString[int(inTarget)][1]:
            print(str('[' + str(mycounter) + ']\t' + member + tabString(member) + pterm(member)))
            mydict.update(list(zip(listify(mycounter), listify(tuplify(member)))))
            mycounter += 1
    else:
        memberList = descendantsFromConId_IN(returnString[int(inTarget)][0][0].split(" ")[1])
        for member in memberList:
            print(str('[' + str(mycounter) + ']\t' + member[0] + tabString(member[0]) + member[1]))
            mydict.update(list(zip(listify(mycounter), listify(tuplify(member)))))
            mycounter += 1
    print()
    if not returnString[int(inTarget)][1] is None:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'rolereportId.txt', 'w', encoding="UTF-8")
        for value in returnString[int(inTarget)][1]:
            f.write(value + '\n')
        f.close()
        roleToSetReportFile()
    return mydict

def upSetEmulMultiLetter(inSets, sortByValDir):
    if sortByValDir == "+":
        sortByVal = False
    else:
        sortByVal = True
    mydict = {}
    preMemberList = []
    memberList = []
    memberIndexDict = {}
    memberIndexDictRev = {}
    memberList = [char for char in inSets]
    memberList.sort()
    for member in memberList:
        memberIndexDict[member] = member
        memberIndexDictRev[member] = member
    memberDict = {}
    for item in memberList:
        memberDict[memberIndexDict[item]] = readFileMembers(cfgPathInstall() + 'sets' + sep() + item + '.txt')
    comparePattern = sorted([item for item in get_power_set([memberIndexDict[member] for member in memberList]) if item != []])
    resultDictSinglePre = {}
    resultDictSingle = {}
    resultDictVals = {}
    resultDict = {}
    singleKeyDict = {}
    for pattern in comparePattern:
        d = []
        for item in pattern:
            d.append(memberDict[item])
        eachResult = list(set.intersection(*[set(x) for x in d]))
        if len(d) == 1:
            resultDictSinglePre["|".join(pattern)] = len(eachResult)
            singleKeyDict["|".join(pattern)] = upSetKeyConvert5("|".join(pattern), memberList)
        else:
            resultDict[upSetKeyConvert5("|".join(pattern), memberList)] = len(eachResult)
        resultDictVals[upSetKeyConvert5("|".join(pattern), memberList)] = eachResult
    for item in memberList:
        resultDictSingle[memberIndexDict[item[0]]] = resultDictSinglePre[memberIndexDict[item[0]]]
    mydict = drawUpSetEmul(sortByVal, memberList, memberIndexDict, memberIndexDictRev, resultDictSingle, resultDictVals, resultDict, singleKeyDict)
    return mydict

#csv file
def csvFile(fileName, topLine, matrix):
    with open(fileName + '.csv', 'w') as csvfile:
        tcwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
        # print topLine
        tcwriter.writerow(topLine)
        for line in matrix:
            # print line
            tcwriter.writerow(line)

def dotDrawGephi():
    # seed set
    inSet1 = "Y"
    # output result set to disposable set Z
    inSet2 = "Z"
    idList = [a for a in readFileMembers(cfgPathInstall() + 'sets' + sep() + inSet1 + '.txt')]
    tempList1 = []
    tempList2 = []
    counter = 1
    for item in idList:
        tempList1 = growValues(item, False)
        for item2 in tempList1:
            if item2 not in tempList2:
                tempList2.append(item2)
        if counter % 50 == 0:
            print(str(counter) + " / " + str(len(idList)))
        counter += 1
    # write basic output set
    writeOutList(tempList2, cfgPathInstall() + 'sets' + sep() + inSet2 + '.txt')
    # write essential graph to SS file set
    dotDrawSet(inSet2, "]")
    # identify all role relationships
    roleDict = {}
    counter = 1
    for item in tempList2:
        if groupedRolesFromConId_IN(item)[1:] != []:
            roleDict[item] = groupedRolesFromConId_IN(item)[1:]
        if counter % 50 == 0:
            print(str(counter) + " / " + str(len(tempList2)))
        counter += 1
    # convert into set of rg nodes and o-rg rg-a-v complex edges
    rgNodeList = []
    rgAVEdgeList = []
    key_list = list(roleDict.keys())
    for key in key_list:
        for rg in roleDict[key]:
            rgNodeList.append([key + "_" + str(rg[0]), key + "_" + str(rg[0]), "", "RG", "3"])
            rgAVEdgeList.append([key, key + "_" + str(rg[0]), "."])
            for role in rg[1:]:
                rgAVEdgeList.append([key + "_" + str(rg[0]), role[3], simplify(role[2])])
    # read back Ids for nodes table [all from SSMembers and SSStructural, seeds from idList]
    tempNodeList = [*[[a, a, simplify(pterm(a)), splitTag(a), ("1" if a in idList else "0")] for a in readFileMembers(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSMembers.txt')],
                    *[[a, a, simplify(pterm(a)), splitTag(a), "2"] for a in readFileMembers(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSStructural.txt')]]
    # read back basic edgess
    basicEdgeList = [[a.split("\t")[0], a.split("\t")[1], ""] for a in readFileMembers(cfgPathInstall() + 'files' + sep() + 'SS' + sep() + 'SSAdj.txt')]
    # write ids (plus terms, types and seed set membership) plus rg nodes to Nodes.csv
    gephiNodeList = [*tempNodeList, *rgNodeList]
    csvFile(cfgPathInstall() + 'files' + sep() + 'gephi' + sep() + 'Nodes', ["Node", "Id", "Label", "Type", "Set"], gephiNodeList)
    #  write basic edges and complex edges to Edges.csv
    gephiEdgeList = [*basicEdgeList, *rgAVEdgeList]
    csvFile(cfgPathInstall() + 'files' + sep() + 'gephi' + sep() + 'Edges', ["Source", "Target", "Label"], gephiEdgeList)
    clearScreen()
    snapshotRoleHeader()
    mydict = SSbrowseFromId(tok["ROOT"])
    return mydict

def resetSetPic():
    dotDrawFocus('', [tok["ROOT"]], '>')
