<<	129125009	Procedure with explicit context	0..1	363589002	Associated procedure	<<	363787002 	Observable entity
<<	129125009	Procedure with explicit context	0..1	363589002	Associated procedure	<<	71388002 	Procedure
<<	129125009	Procedure with explicit context	1..1	408730004	Procedure context	<<	288532009 	Context values for actions
<<	129125009	Procedure with explicit context	1..1	408731000	Temporal context	<<	410510008 	Temporal context value
<<	129125009	Procedure with explicit context	0..1	408732007	Subject relationship context	<<	125676002 	Person
<<	129125009	Procedure with explicit context	0..1	363589002	Associated procedure	<<	363787002 	Observable entity
<<	129125009	Procedure with explicit context	0..1	363589002	Associated procedure	<<	71388002 	Procedure
<<	129125009	Procedure with explicit context	0..1	408730004	Procedure context	<<	288532009 	Context values for actions
<<	129125009	Procedure with explicit context	0..1	408731000	Temporal context	<<	410510008 	Temporal context value
<<	129125009	Procedure with explicit context	0..1	408732007	Subject relationship context	<<	125676002 	Person
<<	243796009	Situation with explicit context	0..1	408731000	Temporal context	<<	410510008 	Temporal context value
<<	243796009	Situation with explicit context	0..1	408732007	Subject relationship context	<<	125676002 	Person
<<	373873005	Pharmaceutical / biologic product	0..1	1142139005	Count of base of active ingredient	=	[[+int(>#0..)]] 	Integer
<<	373873005	Pharmaceutical / biologic product	0..1	1142140007	Count of active ingredient	=	[[+int(>#0..)]] 	Integer
<<	373873005	Pharmaceutical / biologic product	0..1	1142141006	Count of base and modification pair	=	[[+int(>#0..)]] 	Integer
<<	373873005	Pharmaceutical / biologic product	0..1	1148793005	Unit of presentation size quantity	=	[[+dec(>#0..)]] 	Decimal
<<	373873005	Pharmaceutical / biologic product	0..1	1149367008	Has target population	<	27821000087106 	Product target population
<<	373873005	Pharmaceutical / biologic product	0..1	320091000221107	Unit of presentation size unit	<<	767524001 	Unit of measure
<<	373873005	Pharmaceutical / biologic product	0..1	411116001	Has manufactured dose form	<<	736542009 	Pharmaceutical dose form
<<	373873005	Pharmaceutical / biologic product	0..1	763032000	Has unit of presentation	<<	732935002 	Unit of presentation
<<	373873005	Pharmaceutical / biologic product	0..*	766939001	Plays role	<<	766940004 	Role
<<	373873005	Pharmaceutical / biologic product	0..1	774158006	Has product name	<<	774167006 	Product name
<<	373873005	Pharmaceutical / biologic product	0..1	774159003	Has supplier	<<	774164004 	Supplier
<<	373873005	Pharmaceutical / biologic product	0..*	860781008	Has product characteristic	<<	362981000 	Qualifier value
<<	373873005	Pharmaceutical / biologic product	0..1	1142135004	Has presentation strength numerator value	=	[[+dec(>#0..)]] 	Decimal
<<	373873005	Pharmaceutical / biologic product	0..1	1142136003	Has presentation strength denominator value	=	[[+dec(>#0..)]] 	Decimal
<<	373873005	Pharmaceutical / biologic product	0..1	1142137007	Has concentration strength denominator value	=	[[+dec(>#0..)]] 	Decimal
<<	373873005	Pharmaceutical / biologic product	0..1	1142138002	Has concentration strength numerator value	=	[[+dec(>#0..)]] 	Decimal
<<	373873005	Pharmaceutical / biologic product	0..1	1149366004	Has ingredient qualitative strength	<	1149484003 	Ingredient qualitative strength
<<	373873005	Pharmaceutical / biologic product	0..1	127489000	Has active ingredient	<<	105590001 	Substance
<<	373873005	Pharmaceutical / biologic product	0..1	732943007	Has BoSS	<	105590001 	Substance
<<	373873005	Pharmaceutical / biologic product	0..1	732945000	Has presentation strength numerator unit	<	767524001 	Unit of measure
<<	373873005	Pharmaceutical / biologic product	0..1	732947008	Has presentation strength denominator unit	<	767524001 	Unit of measure
<<	373873005	Pharmaceutical / biologic product	0..1	733722007	Has concentration strength denominator unit	<	767524001 	Unit of measure
<<	373873005	Pharmaceutical / biologic product	0..1	733725009	Has concentration strength numerator unit	<	767524001 	Unit of measure
<<	373873005	Pharmaceutical / biologic product	0..1	762949000	Has precise active ingredient	<<	105590001 	Substance
<<	373873005	Pharmaceutical / biologic product	0..1	762951001	Has ingredient	<<	105590001 	Substance
<<	373873005	Pharmaceutical / biologic product	0..*	860779006	Has ingredient characteristic	<<	362981000 	Qualifier value
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	272379006 	Event
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	363787002 	Observable entity
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	404684003 	Clinical finding
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	416698001 	Link assertion
<<	413350009	Finding with explicit context	1..1	408729009	Finding context	<<	410514004 	Finding context value
<<	413350009	Finding with explicit context	1..1	408731000	Temporal context	<<	410510008 	Temporal context value
<<	413350009	Finding with explicit context	0..1	408732007	Subject relationship context	<<	125676002 	Person
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	272379006 	Event
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	363787002 	Observable entity
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	404684003 	Clinical finding
<<	413350009	Finding with explicit context	0..1	246090004	Associated finding	<<	416698001 	Link assertion
<<	413350009	Finding with explicit context	0..1	408729009	Finding context	<<	410514004 	Finding context value
<<	413350009	Finding with explicit context	0..1	408731000	Temporal context	<<	410510008 	Temporal context value
<<	413350009	Finding with explicit context	0..1	408732007	Subject relationship context	<<	125676002 	Person
