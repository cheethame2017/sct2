11654001 | Achilles tendinitis |
1539003 | Acquired trigger finger |
202856007 | Biceps tendinitis |
68172002 | Disorder of tendon |
957321000000107 | Flexor digitorum tenosynovitis of foot |
37785001 | Patellar tendinitis |
4106009 | Rotator cuff syndrome |
429513001 | Rupture of Achilles tendon |
239978002 | Rupture of extensor tendon of finger |
239979005 | Rupture of flexor tendon of finger |
30832001 | Rupture of patellar tendon |
262982008 | Rupture of peroneal tendon |
699312008 | Rupture of posterior tibial tendon |
6849006 | Rupture of quadriceps tendon |
415746003 | Rupture of tendon |
428883008 | Rupture of tendon of biceps |
281550008 | Rupture of tendon of foot and ankle |
240001002 | Rupture of tendon of knee region |
239977007 | Rupture of tendon of wrist and hand |
288216005 | Shoulder tendon rupture |
34840004 | Tendinitis |
262972007 | Tendon strain |
EXCLUDE
