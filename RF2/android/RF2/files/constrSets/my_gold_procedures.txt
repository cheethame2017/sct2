<< 69864007 | Restoration, gold foil |
<< 27310005 | Gold measurement |
<< 228702008 | Gold 198 therapy |
719925004 | Assessment using GFTA-2 (Goldman-Fristoe Test for Articulation 2) |
719926003 | Assessment using GFTA-3 (Goldman-Fristoe Test for Articulation 3) |
83766002 | Colloidal gold test |
392339009 | Goldenrod RAST |
389152008 | Goldmann applanation tonometry |
252807001 | Goldmann visual field |
58568008 | Goldthwaite operation for ankle stabilisation |
172280008 | Insertion of gold weight into upper eyelid |
448552007 | Interstitial brachytherapy using gold grain |
180242007 | Intramuscular gold therapy |
726605004 | Medicinal gold therapy |
34861006 | Patellar stabilisation by tendon transfer |
231625002 | Removal of gold weight from upper eyelid |
36884001 | Repair of recurrent dislocation of patella |
711548004 | Ultrasonography guided insertion of gold seed marker |
EXCLUDE
