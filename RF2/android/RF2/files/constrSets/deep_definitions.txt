81077008 | Acute rheumatic arthritis |
703119002 | Carditis due to rheumatic fever |
430777009 | Family history of rheumatic fever |
161500008 | H/O: rheumatic fever |
12561000132105 | Herpes zoster with secondary bacterial infection |
681221000119108 | History of herpes zoster |
16540531000119103 | History of ocular zoster |
230597003 | Intercostal post-herpetic neuralgia |
282742004 | Intercostal post-herpetic neuritis |
15992311000119100 | Keratitis of left eye due to herpes zoster |
15992271000119100 | Keratitis of right eye due to herpes zoster |
193190005 | Polyneuropathy in herpes zoster |
282741006 | Post-herpetic neuritis |
76462000 | Post-herpetic polyneuropathy |
403396008 | Post-herpetic scar |
17974002 | Post-herpetic trigeminal neuralgia |
2177002 | Postherpetic neuralgia |
429422002 | Rheumatic arthritis of temporomandibular joint |
14175009 | Rheumatic joint disease |
732965007 | Secondary autoimmune haemolytic anaemia co-occurrent and due to rheumatic disorder |
58769002 | Subacute rheumatic arthritis |
EXCLUDE
