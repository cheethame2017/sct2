48947008 | Abnormal tooth attrition |
47222000 | Abrasion of tooth |
385645004 | Accepted |
109674000 | Acquired absence of single tooth |
708578007 | Active caries |
39273001 | Apical periodontitis |
109442002 | Congenital absence of one tooth |
281236004 | Construct temporary dental bridge |
281235000 | Construct temporary dental crown |
80967001 | Dental caries |
256607001 | Dental die - heat hardened ceramic |
109736000 | Dental restoration failure of periodontal anatomical integrity |
256596009 | Dental solder - gold |
397869004 | Dental trauma |
57203004 | Disorder of pulp of tooth |
272307009 | Endosseous oral implant |
245639007 | Entire deciduous mandibular left canine tooth |
245638004 | Entire deciduous mandibular left first molar tooth |
245641008 | Entire deciduous mandibular left lateral incisor tooth |
245637009 | Entire deciduous mandibular left second molar tooth |
245632003 | Entire deciduous mandibular right canine tooth |
245635001 | Entire deciduous mandibular right central incisor tooth |
245631005 | Entire deciduous mandibular right first molar tooth |
245634002 | Entire deciduous mandibular right lateral incisor tooth |
245630006 | Entire deciduous mandibular right second molar tooth |
245624006 | Entire deciduous maxillary left canine tooth |
245627004 | Entire deciduous maxillary left central incisor tooth |
245623000 | Entire deciduous maxillary left first molar tooth |
245626008 | Entire deciduous maxillary left lateral incisor tooth |
245622005 | Entire deciduous maxillary left second molar tooth |
245617005 | Entire deciduous maxillary right canine tooth |
245620002 | Entire deciduous maxillary right central incisor tooth |
245616001 | Entire deciduous maxillary right first molar tooth |
245619008 | Entire deciduous maxillary right lateral incisor tooth |
245615002 | Entire deciduous maxillary right second molar tooth |
245608005 | Entire permanent mandibular left canine tooth |
245611006 | Entire permanent mandibular left central incisor tooth |
245604007 | Entire permanent mandibular left first molar tooth |
245607000 | Entire permanent mandibular left first premolar tooth |
245610007 | Entire permanent mandibular left lateral incisor tooth |
245603001 | Entire permanent mandibular left second molar tooth |
245606009 | Entire permanent mandibular left second premolar tooth |
245602006 | Entire permanent mandibular left third molar tooth |
245597004 | Entire permanent mandibular right canine tooth |
245600003 | Entire permanent mandibular right central incisor tooth |
245592005 | Entire permanent mandibular right first molar tooth |
245599001 | Entire permanent mandibular right lateral incisor tooth |
245591003 | Entire permanent mandibular right second molar tooth |
245595007 | Entire permanent mandibular right second premolar tooth |
245590002 | Entire permanent mandibular right third molar tooth |
245584001 | Entire permanent maxillary left canine tooth |
245587008 | Entire permanent maxillary left central incisor tooth |
245579007 | Entire permanent maxillary left first molar tooth |
245583007 | Entire permanent maxillary left first premolar tooth |
245586004 | Entire permanent maxillary left lateral incisor tooth |
245578004 | Entire permanent maxillary left second molar tooth |
245582002 | Entire permanent maxillary left second premolar tooth |
245577009 | Entire permanent maxillary left third molar tooth |
245572003 | Entire permanent maxillary right canine tooth |
245575001 | Entire permanent maxillary right central incisor tooth |
245568002 | Entire permanent maxillary right first molar tooth |
245571005 | Entire permanent maxillary right first premolar tooth |
245574002 | Entire permanent maxillary right lateral incisor tooth |
245567007 | Entire permanent maxillary right second molar tooth |
245570006 | Entire permanent maxillary right second premolar tooth |
245566003 | Entire permanent maxillary right third molar tooth |
82212003 | Erosion of teeth |
110338002 | Exfoliation of teeth |
234713009 | Fissure seal tooth |
234803005 | Fit veneer to tooth |
426222000 | Fitting of dental crown to tooth |
234804004 | Fixation of restoration to dental implant |
36202009 | Fracture of tooth |
26748006 | Hypomineralisation of tooth |
235104008 | Impacted tooth |
385651009 | In progress |
708584005 | Inner half of enamel |
708579004 | Inner third of dentin |
234787002 | Insertion of amalgam restoration into tooth |
10266008 | Insertion of bridge pontic |
234789004 | Insertion of composite restoration into tooth |
718056001 | Insertion of dental bridge abutment on tooth |
277554000 | Insertion of glass-ionomer restoration into tooth |
723075007 | Insertion of malleable metallic restoration into tooth |
234785005 | Insertion of malleable restoration into tooth |
733964002 | Insertion of malleable tooth coloured restoration into tooth |
234793005 | Insertion of temporary restoration in tooth |
47944004 | Malocclusion of teeth |
708580001 | Middle third of dentin |
42711005 | Necrosis of the pulp |
708577002 | Non-active caries |
708585006 | Outer half of enamel |
708581002 | Outer third of dentin |
109729001 | Overhang on tooth restoration |
398166005 | Performed |
32150000 | Periodontic procedure |
41565005 | Periodontitis |
397943006 | Planned |
718356008 | Previously initiated dental therapy completed |
733035008 | Previously initiated dental therapy not completed |
722796003 | Primary active caries |
733940003 | Primary active dental caries extending into inner half of enamel |
733943001 | Primary active dental caries extending into inner third of dentin |
733942006 | Primary active dental caries extending into middle third of dentin |
733939000 | Primary active dental caries extending into outer half of enamel |
733941004 | Primary active dental caries extending into outer third of dentin |
708576006 | Primary caries |
722798002 | Primary non-active caries |
32620007 | Pulpitis |
718084006 | Replacement of tooth with removable complete denture |
718082005 | Replacement of tooth with removable denture |
718083000 | Replacement of tooth with removable partial denture |
42937000 | Restoration, crown, single |
80764003 | Restoration, inlay |
62786001 | Restoration, inlay, composite/resin |
45330002 | Restoration, inlay, metallic |
83376002 | Restoration, inlay, porcelain/ceramic |
66569006 | Retained dental root |
54258006 | Root canal therapy, comprehensive |
722797007 | Secondary active caries |
708575005 | Secondary caries |
95254009 | Secondary dental caries |
722799005 | Secondary non-active caries |
43281008 | Structure of deciduous mandibular left canine tooth |
89552004 | Structure of deciduous mandibular left central incisor tooth |
38896004 | Structure of deciduous mandibular left first molar tooth |
14770005 | Structure of deciduous mandibular left lateral incisor tooth |
49330006 | Structure of deciduous mandibular left second molar tooth |
6062009 | Structure of deciduous mandibular right canine tooth |
67834006 | Structure of deciduous mandibular right central incisor tooth |
58646007 | Structure of deciduous mandibular right first molar tooth |
22445006 | Structure of deciduous mandibular right lateral incisor tooth |
61868007 | Structure of deciduous mandibular right second molar tooth |
73937000 | Structure of deciduous maxillary left canine tooth |
51678005 | Structure of deciduous maxillary left central incisor tooth |
45234009 | Structure of deciduous maxillary left first molar tooth |
43622005 | Structure of deciduous maxillary left lateral incisor tooth |
51943008 | Structure of deciduous maxillary left second molar tooth |
30618001 | Structure of deciduous maxillary right canine tooth |
88824007 | Structure of deciduous maxillary right central incisor tooth |
17505006 | Structure of deciduous maxillary right first molar tooth |
65624003 | Structure of deciduous maxillary right lateral incisor tooth |
27855007 | Structure of deciduous maxillary right second molar tooth |
90933009 | Structure of distal surface of tooth |
72203008 | Structure of lingual surface of tooth |
8483002 | Structure of mesial surface of tooth |
83473006 | Structure of occlusal surface of tooth |
860782001 | Structure of permanent mandibular left canine tooth |
425106001 | Structure of permanent mandibular left central incisor tooth |
866006002 | Structure of permanent mandibular left first molar tooth |
2400006 | Structure of permanent mandibular left first premolar tooth |
423331005 | Structure of permanent mandibular left lateral incisor tooth |
863898000 | Structure of permanent mandibular left second molar tooth |
24573005 | Structure of permanent mandibular left second premolar tooth |
74344005 | Structure of permanent mandibular left third molar tooth |
860785004 | Structure of permanent mandibular right canine tooth |
424575004 | Structure of permanent mandibular right central incisor tooth |
866005003 | Structure of permanent mandibular right first molar tooth |
423937004 | Structure of permanent mandibular right lateral incisor tooth |
863899008 | Structure of permanent mandibular right second molar tooth |
8873007 | Structure of permanent mandibular right second premolar tooth |
38994002 | Structure of permanent mandibular right third molar tooth |
860780009 | Structure of permanent maxillary left canine tooth |
424399000 | Structure of permanent maxillary left central incisor tooth |
865988009 | Structure of permanent maxillary left first molar tooth |
61897005 | Structure of permanent maxillary left first premolar tooth |
423185002 | Structure of permanent maxillary left lateral incisor tooth |
863901004 | Structure of permanent maxillary left second molar tooth |
23226009 | Structure of permanent maxillary left second premolar tooth |
87704003 | Structure of permanent maxillary left third molar tooth |
860767006 | Structure of permanent maxillary right canine tooth |
422653006 | Structure of permanent maxillary right central incisor tooth |
865995000 | Structure of permanent maxillary right first molar tooth |
57826002 | Structure of permanent maxillary right first premolar tooth |
424877001 | Structure of permanent maxillary right lateral incisor tooth |
863902006 | Structure of permanent maxillary right second molar tooth |
36492000 | Structure of permanent maxillary right second premolar tooth |
68085002 | Structure of permanent maxillary right third molar tooth |
62579006 | Structure of vestibular surface of tooth |
367534004 | Supernumerary tooth |
234948008 | Tooth absent |
55162003 | Tooth extraction |
278661005 | Tooth present |
EXCLUDE
