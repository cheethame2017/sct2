109750005 | Abfraction |
47222000 | Abrasion of tooth |
441935006 | Acquired absence of all teeth |
37320007 | Acquired absence of teeth |
109602002 | Acute apical abscess |
399050001 | Acute necrotising ulcerative gingivitis |
235010005 | Acute necrotising ulcerative periodontitis |
91863007 | Acute pericoronitis |
702745006 | Adhesive failure of dental restoration |
61804006 | Alveolitis of jaw |
14901003 | Ankylosis of tooth |
67289000 | Anterior open bite |
247805009 | Anxiety and fear |
426965005 | Aphthous ulcer of mouth |
711639000 | Asymptomatic irreversible pulpitis |
718052004 | Asymptomatic periapical periodontitis |
711640003 | Asymptomatic reversible pulpitis |
609403008 | At extreme risk for dental caries |
609402003 | At high risk for dental caries |
609400006 | At low risk for dental caries |
609401005 | At moderate risk for dental caries |
716363001 | At risk of fracture of tooth |
91950000 | Atrophy of edentulous alveolar ridge |
711469000 | Biologically unacceptable dental restoration |
278618007 | Broken denture clasp |
79740000 | Candidiasis of mouth |
711175004 | Caries of smooth surface of tooth |
109679005 | Central dislocation of tooth |
699777008 | Centric occlusion - Centric relation discrepancy |
716364007 | Cervical caries |
109603007 | Chronic apical abscess |
4264000 | Chronic pericoronitis |
110302009 | Clenching teeth |
702645001 | Cohesive failure of dental restoration |
699857006 | Complete denture not aesthetic |
109671008 | Complete dislocation of tooth |
109747007 | Cracked tooth |
109495004 | Crossbite |
12351004 | Crowding of teeth |
43144004 | Cyst of jaw |
699685006 | Deep occlusal groove |
60476005 | Deep overbite |
109728009 | Defective dental restoration |
699711003 | Defective mandibular complete denture |
699710002 | Defective mandibular overdenture |
699713000 | Defective mandibular removable partial denture |
699712005 | Defective maxillary complete denture |
699709007 | Defective maxillary overdenture |
699714006 | Defective maxillary removable partial denture |
714635005 | Deformity of mucogingival junction |
80967001 | Dental caries |
442551007 | Dental caries extending into dentin |
699422003 | Dental peri-implantitis |
713245008 | Dental plaque induced gingivitis |
109730006 | Dental restoration aesthetically inadequate or displeasing |
109743006 | Dental restoration can not be expected to continue to be serviceable |
109734002 | Dental restoration failure of anatomical integrity regarding adjacent teeth |
109735001 | Dental restoration failure of marginal integrity |
109736000 | Dental restoration failure of periodontal anatomical integrity |
416673002 | Dental restoration lost |
716294003 | Dental restoration occlusally inadequate |
397869004 | Dental trauma |
278631002 | Denture loose |
69254008 | Denture stomatitis |
278630001 | Denture unstable |
266415009 | Developmental abnormality of tooth size and form |
734009000 | Diastema of teeth |
210366009 | Dislocation of tooth |
405258007 | Displacement of tooth |
234972003 | Disturbance of tooth eruption or exfoliation |
702402003 | Early childhood caries |
234974002 | Early tooth exfoliation |
110321001 | Edge to edge occlusion of teeth |
373588006 | Enamel and dentine fracture |
80353004 | Enamel caries |
82212003 | Erosion of teeth |
42323001 | Eruption cyst of jaw |
53963006 | Excessive attrition of teeth |
111347003 | Exostosis of jaw |
41918006 | External resorption of tooth |
234979007 | Extrinsic staining of tooth |
278528006 | Facial swelling |
57650002 | Failure of exfoliation of primary tooth |
442437005 | Failure of osseointegration of dental implant |
235014001 | Flabby alveolar ridge |
109754001 | Fracture of crown and root of tooth |
109753007 | Fracture of crown of tooth, enamel and dentin, with pulp exposure |
109751009 | Fracture of crown of tooth, enamel only |
36202009 | Fracture of tooth |
109741008 | Fractured dental restoration |
278616006 | Fractured denture |
718066009 | Generalised moderate aggressive periodontitis |
718062006 | Generalised moderate chronic periodontitis |
718065008 | Generalised severe aggressive periodontitis |
718061004 | Generalised severe chronic periodontitis |
718064007 | Generalised slight aggressive periodontitis |
718060003 | Generalised slight chronic periodontitis |
109610001 | Gingival abscess |
18718003 | Gingival disease |
716362006 | Gingival disease co-occurrent with diabetes mellitus |
95212006 | Gingival disease due to pregnancy |
54711002 | Gingival enlargement |
235001002 | Gingival fibroepithelial polyp |
4356008 | Gingival recession |
66383009 | Gingivitis |
713879003 | Gingivitis due to dental plaque with local contributing factor |
109709002 | Horizontal alveolar bone loss |
109758003 | Horizontal fracture of cervical third of root of tooth |
699423008 | Hyperocclusion |
235104008 | Impacted tooth |
109794004 | Inadequate vestibular depth after teeth loss |
698039001 | Incomplete fracture of tooth |
699274002 | Increased overjet |
109787002 | Inflammatory fibrous hyperplasia of mouth |
703401001 | Insufficient biological width |
110313007 | Insufficient clinical crown height |
52994003 | Internal resorption of tooth |
234983007 | Intrinsic staining of tooth |
38285004 | Irregular alveolar process |
699853005 | Labial ectopic tooth |
109798001 | Lack of adequate intermaxillary vertical dimension |
707219007 | Lack of keratinized gingiva |
5639000 | Late tooth eruption |
278549007 | Leaking dental restoration |
414603003 | Leucoplakia of oral mucosa |
4776004 | Lichen planus |
703132007 | Lingual ectopic tooth |
715859008 | Localised moderate aggressive periodontitis |
715286003 | Localised moderate chronic periodontitis |
715860003 | Localised severe aggressive periodontitis |
715287007 | Localised severe chronic periodontitis |
715843008 | Localised slight aggressive periodontitis |
715285004 | Localised slight chronic periodontitis |
699828007 | Loss of occlusal relationship with complete denture |
109500001 | Malocclusion, Angle class I |
109501002 | Malocclusion, Angle class II |
109504005 | Malocclusion, Angle class III |
714249007 | Mild gingivitis |
714478005 | Moderate gingivitis |
235017008 | Mucocele of mouth |
42711005 | Necrosis of the pulp |
699683004 | Nonrestorable carious tooth |
718070001 | Normal oral mucous membrane and osseointegrated bone tissue surrounding dental implant |
711635006 | Normal periapical tissue |
714482007 | Normal periodontal tissue |
715842003 | Normal periodontium with attachment loss |
711636007 | Normal pulp of tooth |
699701005 | Obliteration of root canal of tooth due to abnormal mineralisation of tooth pulp |
109647001 | Occlusal trauma |
10017004 | Occlusal wear of teeth |
20674003 | Oral fistula |
109644008 | Overeruption of tooth |
109729001 | Overhang on tooth restoration |
64969001 | Partial congenital absence of teeth |
29553002 | Peg-shaped teeth |
716296001 | Perforation of dental restoration |
699684005 | Peri-implant mucositis |
74598008 | Periapical abscess with sinus tract |
22240003 | Pericoronitis |
83412009 | Periodontal abscess |
235006007 | Periodontal and endodontic lesion |
41565005 | Periodontitis |
718375003 | Permanent tooth normal |
711173006 | Pit and fissure caries |
213299007 | Postoperative pain |
109648006 | Primary occlusal trauma |
235002009 | Pyogenic granuloma of gingiva |
716292004 | Radiographic radiolucency of tooth |
716293009 | Radiographic radiopacity of tooth |
234976000 | Rampant dental caries |
699856002 | Removable partial denture not aesthetic |
710783007 | Removable partial denture with loss of occlusal relationship |
66569006 | Retained dental root |
716299008 | Retention of dental device |
234975001 | Root caries |
109649003 | Secondary occlusal trauma |
13468005 | Sensitive dentin |
8954007 | Sequestrum of jaw bone |
714477000 | Severe gingivitis |
274950005 | Sleep related bruxism |
61170000 | Stomatitis |
41366005 | Subgingival dental calculus |
234973008 | Submerging tooth |
8666004 | Supernumerary teeth |
367534004 | Supernumerary tooth |
18755003 | Supragingival dental calculus |
711638008 | Symptomatic irreversible pulpitis |
718053009 | Symptomatic periapical periodontitis |
711641004 | Symptomatic reversible pulpitis |
8004003 | Teething syndrome |
41888000 | Temporomandibular joint disorder |
386207004 | Temporomandibular joint-pain-dysfunction syndrome |
110483000 | Tobacco user |
67787004 | Tongue tie |
278617002 | Tooth fallen off denture |
11625007 | Torus mandibularis |
46752004 | Torus palatinus |
16958000 | Total anodontia of permanent and deciduous teeth |
50882004 | Traumatic ulcer of oral mucosa |
109710007 | Vertical alveolar bone loss |
109762009 | Vertical fracture of root of tooth |
87715008 | Xerostomia |
EXCLUDE
