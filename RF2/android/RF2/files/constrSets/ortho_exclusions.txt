735876001 | Puncture wound of toe with foreign body |
735875002 | Puncture wound of toe without foreign body |
735877005 | Open bite of toe |
735874003 | Laceration of toe with foreign body |
255180005 | Benign neoplasm of bone of foot |
187978005 | Malignant neoplasm of phalanges of foot |
95124006 | Neoplasm of uncertain behaviour of tarsal bone |
93959008 | Primary malignant neoplasm of phalanx of foot |
126597003 | Neoplasm of phalanx of foot |
94999000 | Neoplasm of uncertain behaviour of phalanx of foot |
385429006 | Osteochondroma of foot |
94486006 | Secondary malignant neoplasm of phalanx of foot |
448378004 | Sarcoma of bone of foot |
735864001 | Laceration of foot with foreign body |
735868003 | Puncture wound and foreign body of foot |
302038007 | Open fracture dislocation of interphalangeal joint of toe |
302037002 | Open fracture dislocation of metatarsophalangeal joint |
209366008 | Open fracture dislocation of single metatarsophalangeal joint |
209383004 | Open fracture subluxation of single metatarsophalangeal joint |
209367004 | Open fracture dislocation of interphalangeal joint of single toe |
209384005 | Open fracture subluxation of interphalangeal joint of single toe |
209368009 | Open fracture dislocation of multiple metatarsophalangeal joints |
209369001 | Open fracture dislocation of interphalangeal joint of multiple toes |
209385006 | Open fracture subluxation of multiple metatarsophalangeal joints |
209386007 | Open fracture subluxation of interphalangeal joint of multiple toes |
735856007 | Injury of Achilles tendon |
1075841000119102 | Calcific tendinitis of left achilles tendon |
735858008 | Laceration of Achilles tendon |
15675561000119107 | Bilateral calcific tendinitis of achilles tendons |
11857701000119100 | Rupture of left Achilles tendon |
11857341000119104 | Laceration of right Achilles tendon |
11763011000119102 | Laceration of left Achilles tendon |
15934341000119103 | Congenital bilateral short Achilles tendons |
1075821000119108 | Calcific tendinitis of right achilles tendon |
11857661000119107 | Rupture of right Achilles tendon |
735201003 | Open fracture of left calcaneus |
704251001 | Pathological fracture of foot due to neoplastic disease |
292071000119106 | Left common peroneal neuropathy |
292061000119100 | Right common peroneal neuropathy |
10660471000119109 | Diabetic ulcer of left foot due to diabetes mellitus type 2 |
87451000119102 | Heel AND/OR midfoot ulcer due to type 2 diabetes mellitus |
10661671000119102 | Diabetic ulcer of right foot due to diabetes mellitus type 2 |
108781000119105 | Neuropathic ulcer of midfoot AND/OR heel due to type 2 diabetes mellitus |
87481000119109 | Heel AND/OR midfoot ulcer due to type 1 diabetes mellitus |
201852006 | Localised, secondary osteoarthritis of the hand |
699261008 | Post infectious osteoarthritis |
25343008 | Secondary localised osteoarthrosis of pelvic region |
201847001 | Localised, secondary osteoarthritis |
7126001 | Secondary localised osteoarthrosis of multiple sites |
201849003 | Localised, secondary osteoarthritis of the shoulder region |
267890003 | Localised, secondary osteoarthritis of the pelvic region and thigh |
699262001 | Post traumatic osteoarthritis |
201855008 | Localised, secondary osteoarthritis of the ankle and/or foot |
735668000 | Bilateral traumatic amputation of legs through hip joints |
704245006 | Pathological fracture of tibia due to neoplastic disease |
704252008 | Pathological fracture of fibula due to neoplastic disease |
317741000119107 | Peroneal tendinitis of left lower limb |
15638411000119100 | Bilateral tendinitis of lower legs |
317751000119109 | Peroneal tendinitis of right lower limb |
EXCLUDE
