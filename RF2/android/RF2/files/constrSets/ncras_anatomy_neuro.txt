< 25087005 | Structure of nervous system |
< 128319008 | Intracranial structure |
<< 28661005 | Cerebrovascular system structure |
<< 89471000 | Structure of labyrinthine artery |
<< 29456007 | Structure of cortical branch of posterior cerebral artery |
< 314294008 | Entire artery of inner ear |
<< 278101009 | Accessory meningeal artery |
<< 2681003 | Structure of dorsal nerve of penis |
<< 34678004 | Structure of genitofemoral nerve genital branch |
<< 64395006 | Structure of middle meningeal artery |
<< 279730001 | Vestibular pyramid |
<< 69581009 | Structure of anterior vestibular artery |
362621004 | Entire infratemporal region |
EXCLUDE
<< 70402007 | Peripheral sensory structure |
<< 60271003 | Neural tube structure |
<< 360409004 | Primitive brain vesicle |
<< 22286001 | External carotid artery structure |
<< 711516003 | Structure of wall of carotid artery |
81881007 | Synaptic membrane |
787221004 | Entire structure of nervous system of neck |
359923002 | Nervous structure of limb |
730428008 | Entire neural groove |
72167002 | Autonomic nervous system structure |
361061002 | Region of sympathetic nervous system |
729908000 | Entire central nervous tissue |
281830000 | Structure of nervous system of head and neck |
298045000 | Entire metencephalon of fetus |
272646001 | Structure of sensory nervous system |
732080004 | Entire sensory pathway |
281247006 | Structure of cervical nervous system |
731745005 | Entire nervous system of lower limb |
787218001 | Structure of nervous system of neck |
362292005 | Entire peripheral nervous system |
69147006 | Synapse structure |
732096001 | Entire developmental brain |
731123000 | Entire sensory nervous system |
68365008 | Sympathetic nervous system structure |
60791006 | Structure of lumbar portion of sympathetic nervous system |
37222002 | Structure of notochord |
730426007 | Entire neural fold |
429921001 | Structure of peripheral part of autonomic nervous system |
361478000 | Cephalic neuropore |
362487006 | Entire cervical portion of sympathetic nervous system |
761909005 | Structure of left half of nervous system |
86034008 | Structure of cervical portion of sympathetic nervous system |
23424009 | Parasympathetic nervous system structure |
730446002 | Entire lamina terminalis |
362484004 | Entire sympathetic nervous system |
36628003 | Structure of cephalic portion of sympathetic nervous system |
362486002 | Entire cephalic portion of sympathetic nervous system |
280950006 | Nervous system pathway structure |
362496006 | Entire parasympathetic nervous system |
761910000 | Structure of right half of nervous system |
731842004 | Entire nervous of limb |
362481007 | Entire autonomic nervous system |
314386003 | Developmental brain structure |
727254006 | Entire extracellular space of nervous system |
281248001 | Nervous structure of trunk |
63111001 | Peripheral motor structure |
361461009 | Neural fold |
731751000 | Entire nervous system of head and neck |
730427003 | Entire neural crest |
309894005 | Sensory pathway structure |
731098003 | Entire nervous tissue |
730442000 | Entire caudal neuropore |
732028008 | Entire nervous system pathway |
362426000 | Entire spinal cord, roots and ganglia |
362290002 | Entire olfactory system |
362490000 | Entire lumbar portion of sympathetic nervous system |
609610009 | Structure of nerve root and/or plexus |
362489009 | Entire abdominopelvic portion of sympathetic nervous system |
20353007 | Spinal cord ansae |
731735008 | Entire nervous system of head |
361479008 | Caudal neuropore |
361463007 | Neural groove |
118976002 | Entire subdivision of nervous system |
732041007 | Entire regional nervous system |
732009001 | Entire intracranial nerve |
47166007 | Structure of olfactory system |
3058005 | Peripheral nervous system structure |
281230005 | Nervous structure of head |
731458005 | Entire nerve root and plexus |
730441007 | Entire cephalic neuropore |
278196006 | Entire nervous system |
37623003 | Olfactory sensory epithelium |
281251008 | Nervous structure of lower limb |
110657001 | All nervous tissue |
361462002 | Neural crest |
91728009 | Structure of nervous tissue |
308820002 | Entire notochord |
279390002 | Intracranial nerve structure |
281250009 | Nervous structure of upper limb |
122454008 | Embryonic nervous system structure |
2332005 | Structure of metencephalon of fetus |
731983002 | Entire peripheral motor |
726994005 | Entire olfactory sensory epithelium |
361483008 | Lamina terminalis |
362298009 | Entire synapse |
732033007 | Entire cervical nervous system |
731743003 | Entire nervous system of trunk |
731994003 | Entire embryonic nervous system |
91715002 | Spinal cord, roots and ganglia structure |
82956006 | Extracellular space of nervous system |
281829005 | Structure of regional nervous system |
731744009 | Entire nervous system of upper limb |
730165001 | Entire neuropore |
309319008 | Central nervous tissue |
360408007 | Neuropore |
731998000 | Entire intracranial structure |
244678000 | Entire posterior fossa of cranial cavity |
9775002 | Structure of left carotid sinus |
244209004 | Entire carotid bifurcation |
161452006 | Entire left internal carotid artery |
161054003 | Entire left external carotid artery |
721028001 | Structure of left carotid artery |
731555006 | Entire wall of precerebral artery |
362040006 | Entire carotid artery |
5195005 | Structure of right external carotid artery |
58379002 | Structure of left internal carotid artery |
38917008 | Structure of right internal carotid artery |
32062004 | Common carotid artery structure |
86117002 | Internal carotid artery structure |
726845006 | Entire precerebral artery |
723548005 | Structure of wall of right carotid artery |
113263003 | Left common carotid artery structure |
362043008 | Entire left common carotid artery |
161251001 | Entire right internal carotid artery |
79718009 | Structure of right carotid sinus |
160959002 | Entire right external carotid artery |
161352005 | Entire left carotid sinus |
711515004 | Structure of wall of precerebral artery |
47629002 | Structure of left external carotid artery |
11281008 | Precerebral artery |
281137000 | Entire carotid sinus |
362042003 | Entire right common carotid artery |
161150005 | Entire right carotid sinus |
65355003 | Right common carotid artery structure |
721033002 | Structure of right carotid artery |
21479005 | Structure of carotid sinus |
723629003 | Structure of wall of left carotid artery |
