<!DOCTYPE html>
<meta charset="utf-8">
<body>
<script src="lib/d3.v4.min.js"></script>
<script src="lib/viz.js" type="javascript/worker"></script>
<script src="lib/d3-graphviz.js"></script>
<div id="graph" style="text-align: center;"></div>
<script>

var dotIndex = 0;
var graphviz = d3.select("#graph").graphviz()
    .transition(function () {
        return d3.transition("main")
            .ease(d3.easeLinear)
            .delay(1000)
            .duration(3000);
    })
    .logEvents(true)
    .on("initEnd", render);

function render() {
    var dotLines = dots[dotIndex];
    var dot = dotLines.join('');
    graphviz
        .renderDot(dot)
        .on("end", function () {
            dotIndex = (dotIndex + 1) % dots.length;
            render();
        });
}

// dots variable:
// list of dot instructions for each domain/domain set
// generated with MRCMConfig.cfg.D_3=1.
// currently split up or grouped and drawn as:

// Observable entity
// Evaluation procedure
// Procedure
// Surgical procedure
// Clinical finding +/- Disorder
// Event
// Situation, PWEC, FWEC
// Administration of substance via specific route
// Pharmaceutical / biologic product,  Basic dose form, Pharmaceutical dose form +/- Medicinal product package
// Body structure, Anatomical structure, Lateralizable body structure reference set
// Specimen, Substance
// +/- Physical object