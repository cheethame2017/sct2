<!DOCTYPE html>
<meta charset="utf-8">
<style>
#play-button {
    position: absolute;
    top: 50px;
    left: 50px;
    background: #839ee7;
    padding-right: 26px;
    border-radius: 3px;
    border: none;
    color: white;
    margin: 0;
    padding: 0 12px;
    width: 120px;
    cursor: pointer;
    height: 50px;
    font-size: 14px;
    }
    #play-button:hover {
    background-color: #696969;
    }
#direction-button {
    position: absolute;
    top: 110px;
    left: 50px;
    background: #83e7b0;
    padding-right: 26px;
    border-radius: 3px;
    border: none;
    color: white;
    margin: 0;
    padding: 0 12px;
    width: 120px;
    cursor: pointer;
    height: 50px;
    font-size: 14px;
    }
    #direction-button:hover {
    background-color: #696969;
    }
</style>
<body>
<script src="lib/d3.v4.min.js"></script>
<script src="lib/viz.js" type="javascript/worker"></script>
<script src="lib/d3-graphviz.js"></script>
<div id="graph" style="text-align: center;"></div>
<div id="vis"><button id="play-button">Running</button></div>
<div id="vis"><button id="direction-button">Forwards</button></div>
<script>

var moving = true;
var direction = 1;   
var playButton = d3.select("#play-button");
var directionButton = d3.select("#direction-button");
var dotIndex = 0;
var graphviz = d3.select("#graph").graphviz()
    .transition(function () {
        return d3.transition("main")
            .ease(d3.easeLinear)
            .delay(2000)
            .duration(3000);
    })
    .logEvents(true)
    .on("initEnd", render);

playButton
    .on("click", function() {
    var button = d3.select(this);
    if (button.text() == "Running") {
    moving = false;
    button.text("Paused");
    } else {
    moving = true;
    button.text("Running");
    render();
    }
    console.log("Moving: " + moving);
})
directionButton
    .on("click", function() {
    var button = d3.select(this);
    if (button.text() == "Forwards") {
    direction = -1;
    button.text("Backwards");
    } else {
    direction = 1;
    button.text("Forwards");
    }
    console.log("Direction: " + direction);
})

function render() {
    var dotLines = dots[dotIndex];
    var dot = dotLines.join('');
    graphviz
        .renderDot(dot)
        .on("end", function () {
            dotIndex = (dotIndex + direction) % dots.length;
            if (dotIndex == -1) {
            dotIndex = dots.length - 1;
            }
            if (moving) {
            render();
            }
        });
}