Tag introduction point that may need changing or non-hierarchical approach	(8)

362837007	Entire cell
118956008	Morphologically altered structure
90332006	Paracetamol-containing product
437876006	Paracetamol-containing product in oral dose form
125676002	Person
373873005	Pharmaceutical / biologic product
260787004	Physical object
765633006	Sodium valproate 200 mg prolonged-release oral tablet

MRCM-referenced class (not a tag introduction point) related to a tag introduction point that may need changing	(7)

49062001	Device
763158003	Medicinal product
781405001	Medicinal product package
49755003	Morphologically abnormal structure
420158005	Performer of method
444018008	Person with characteristic related to subject of record
419358007	Subject of record or other provider of history

Tag introduction point (MRCM-referenced) without a text definition	(23)

736478001	Basic dose form
123037004	Body structure
404684003	Clinical finding
726711005	Disposition
736665006	Dose form administration method
736479009	Dose form intended site
736480007	Dose form release characteristic
276339004	Environment
308916002	Environment or geographical location
272379006	Event
363787002	Observable entity
410607006	Organism
736542009	Pharmaceutical dose form
78621006	Physical force
71388002	Procedure
362981000	Qualifier value
419891008	Record artefact
766940004	Role
243796009	Situation with explicit context
254291000	Staging and scales
736471007	State of matter
105590001	Substance
732935002	Unit of presentation

Tag introduction point (not directly MRCM-referenced) without a text definition	(23)

273249006	Assessment scales
246061005	Attribute
4421005	Cell structure
900000000000442005	Core metadata concept
64572001	Disease
372148003	Ethnic group
900000000000454005	Foundation metadata concept
223496003	Geographical and/or political region of the world
60134006	Life style
416698001	Link assertion
106237007	Linkage concept
370136006	Namespace concept
363743006	Navigational concept
762947003	OWL metadata concept
14679004	Occupation
415229000	Racial group
243120004	Regimes and therapies
108334009	Religion AND/OR philosophy
138875005	SNOMED CT Concept
900000000000441003	SNOMED CT Model Component
48176007	Social context
370115009	Special concept
254292007	Tumour staging

MRCM-referenced class (not a tag introduction point) without a text definition	(22)

442083009	Anatomical or acquired body structure
288532009	Context values for actions
288524001	Courses
288526004	Episodicities
410514004	Finding context value
363675004	Intents
127789004	Laboratory procedure categorised by method
282032007	Periods of life
703763000	Precondition value
272125009	Priorities
103379005	Procedural approach
719982003	Process
27821000087106	Product target population
118598001	Property
284009009	Route of administration value
272141005	Severities
182353008	Side
309795001	Surgical access values
272394005	Technique
410510008	Temporal context value
7389001	Time frame
767524001	Unit of measure

Attribute name without a text definition	(71)

260507000	Access
255234002	After
733928003	All or part of
246090004	Associated finding
116676008	Associated morphology
363589002	Associated procedure
47429007	Associated with
288556008	Before
246075003	Causative agent
263502005	Clinical course
246093002	Component
733931002	Constitutional part of
363699004	Direct device
363700003	Direct morphology
363701004	Direct substance
42752001	Due to
371881003	During
246456000	Episodicity
408729009	Finding context
419066007	Finding informer
418775008	Finding method
363698007	Finding site
736476002	Has basic dose form
736472000	Has dose form administration method
736474004	Has dose form intended site
736475003	Has dose form release characteristic
736473005	Has dose form transformation
363702006	Has focus
363703001	Has intent
363713009	Has interpretation
411116001	Has manufactured dose form
116686009	Has specimen
736518005	Has state of matter
763032000	Has unit of presentation
363710007	Indirect device
363709002	Indirect morphology
363714003	Interprets
733933004	Lateral half of
272741003	Laterality
370129005	Measurement method
260686004	Method
246454002	Occurrence
370135005	Pathological process
766939001	Plays role
260870009	Priority
408730004	Procedure context
405815000	Procedure device
405816004	Procedure morphology
363704007	Procedure site
405813007	Procedure site - Direct
405814001	Procedure site - Indirect
774081006	Proper part of
370131001	Recipient category
733930001	Regional part of
246513007	Revision status
410675002	Route of administration
246112005	Severity
118171006	Specimen procedure
118170007	Specimen source identity
118168003	Specimen source morphology
118169006	Specimen source topography
370133003	Specimen substance
408732007	Subject relationship context
424876005	Surgical approach
733932009	Systemic part of
408731000	Temporal context
370134009	Time aspect
425391005	Using access device
424226004	Using device
424244007	Using energy
424361007	Using substance

MRCM-referenced class (not a tag introduction point) without a text definition but is member of a poly-class value set	(29)

769247005	Abnormal immune process
129264002	Action
129445006	Administration - action
860574003	Bioabsorbable
263714004	Colours
133928008	Community
129265001	Evaluation - action
64100000	False
35359004	Family
260245000	Finding value
389109008	Group
441862004	Infectious process
108252007	Laboratory procedure
117364006	Narrative value
117362005	Nominal value
863965006	Nonbioabsorbable
117363000	Ordinal value
257958009	Part of multistage procedure
863968008	Partially bioabsorbable
308490002	Pathological developmental process
261424001	Primary operation
26716007	Qualitative
30766002	Quantitative
255231005	Revision - value
82280004	Smooth
129284003	Surgical action
117444000	Text value
860647008	Textured
31874001	True

MRCM-referenced class and/or tag introduction point with a text definition	(9)

736477006	Dose form transformation
386053000	Evaluation procedure
472963003	Hypersensitivity process
1149484003	Ingredient qualitative strength
781405001	Medicinal product package
774167006	Product name
123038009	Specimen
774164004	Supplier
387713003	Surgical procedure

Attribute name with a text definition	(54)

704321009	Characterizes
774160008	Contains clinical drug
1142140007	Count of active ingredient
1142141006	Count of base and modification pair
1142139005	Count of base of active ingredient
1142143009	Count of clinical drug type
704327008	Direct site
732943007	Has BoSS
1148969005	Has absorbability
127489000	Has active ingredient
1148967007	Has coating material
840560000	Has compositional material
733722007	Has concentration strength denominator unit
1142137007	Has concentration strength denominator value
733725009	Has concentration strength numerator unit
1142138002	Has concentration strength numerator value
836358009	Has device intended site
726542003	Has disposition
827081001	Has filling
860779006	Has ingredient characteristic
1149366004	Has ingredient qualitative strength
1142142004	Has pack size
774163005	Has pack size unit
762949000	Has precise active ingredient
732947008	Has presentation strength denominator unit
1142136003	Has presentation strength denominator value
732945000	Has presentation strength numerator unit
1142135004	Has presentation strength numerator value
860781008	Has product characteristic
774158006	Has product name
719722006	Has realization
774159003	Has supplier
1148968002	Has surface texture
1149367008	Has target population
718497002	Inherent location
704319004	Inheres in
738774007	Is modification of
1148965004	Is sterile
704326004	Precondition
1003735000	Process acts on
704322002	Process agent
704323007	Process duration
1003703000	Process extends to
704324001	Process output
370130000	Property
704325000	Relative to
719715003	Relative to part of
370132008	Scale type
246501002	Technique
726633004	Temporally related to
704320005	Towards
1148793005	Unit of presentation size quantity
320091000221107	Unit of presentation size unit
246514001	Units

