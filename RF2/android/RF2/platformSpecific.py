from __future__ import print_function, unicode_literals
import sys
import os
import io
import subprocess
from shutil import copyfile
import shutil

def pth_toRF2(inString):  # short name for typing convenience - setConfig really
    f = io.open(cfgPath() + dataPath() + 'SCTRF2datapaths.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = tempList[1]
    f.close()
    return returnValue

def clearScreen():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        print("\033c\033[92m")
    elif sys.platform == "win32":
        # windowsj
        os.system('cls')
        print("\033[92m")

def sep():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        return '/'
    elif sys.platform == "win32":
        return '\\'

# number of print violations in SCTRF2SVHelpers.SV_SIMcheckSimpleRefset

def cfgPath():
    cfgAbsPath = os.path.dirname(os.path.realpath(__file__))
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        return cfgAbsPath + '/files/'
    elif sys.platform == "win32":
        return cfgAbsPath + '\\files\\'


def cfgPathInstall():
    cfgAbsPath = os.path.dirname(os.path.realpath(__file__))
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        return cfgAbsPath + '/'
    elif sys.platform == "win32":
        return cfgAbsPath + '\\'


def dataPath():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        return 'lin/'
    elif sys.platform == "win32":
        return 'win\\'


def startNeoPlatform(path):
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        cwdOld = os.getcwd()
        os.chdir(path)
        # subprocess.call(["ulimit -n 40000"])
        subprocess.call(["./bin/neo4j", "start"])
        os.chdir(cwdOld)
    if sys.platform == "win32":
        cwdOld = os.getcwd()
        os.chdir(path)
        subprocess.call(["neo4j", "install-service"], shell=True)
        subprocess.call(["neo4j", "start"], shell=True)
        os.chdir(cwdOld)


def stopNeoPlatform(path):
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        cwdOld = os.getcwd()
        os.chdir(path)
        subprocess.call(["./bin/neo4j", "stop"])
        os.chdir(cwdOld)
    if sys.platform == "win32":
        cwdOld = os.getcwd()
        os.chdir(path)
        subprocess.call(["neo4j", "stop"], shell=True)
        os.chdir(cwdOld)


def headerWriterFull():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        sys.stdout.write("\x1b]2;SCT Full Data Browser\x07")
    if sys.platform == "win32":
        # windows
        os.system('title SCT Full Data Browser')

def headerWriterSnap():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        sys.stdout.write("\x1b]2;SCT Snapshot Data Browser\x07")
    if sys.platform == "win32":
        # windows
        os.system('title SCT Snapshot Data Browser')

def headerWriterSnapECL():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        sys.stdout.write("\x1b]2;SCT Snapshot ECL + Sets\x07")
    if sys.platform == "win32":
        # windows
        os.system('title SCT Snapshot ECL + Sets')

def headerWriterMRCM():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        sys.stdout.write("\x1b]2;SCT MRCM Diagram Configuration\x07")
    if sys.platform == "win32":
        # windows
        os.system('title SCT MRCM Diagram Configuration')

def progressTrackCmd(startEnd, character, coverString):
    # currently identical for linux and windows
    if len(coverString) > 80:
        startEnd = "END"
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if startEnd == 'START': #or 'END'
            coverString += " "
            sys.stdout.write(character)
            sys.stdout.flush()
        else:
            sys.stdout.write('\r' + coverString + '\r')
            sys.stdout.flush()
            coverString = ""
        return coverString
    if sys.platform == "win32":
        if startEnd == 'START': #or 'END'
            coverString += " "
            sys.stdout.write(character)
            sys.stdout.flush()
        else:
            sys.stdout.write('\r' + coverString + '\r')
            sys.stdout.flush()
            coverString = ""
        return coverString

def swapData(inSnapHeader):
    currentGuide = inSnapHeader

    ukDataBool = os.path.isdir(pth_toRF2("UK_BACKUP"))
    intDataBool = os.path.isdir(pth_toRF2("INT_BACKUP"))
    tempDataBool = os.path.isdir(pth_toRF2("TEMP_BACKUP"))
    if ukDataBool and intDataBool and tempDataBool:
        source1 = os.listdir(pth_toRF2("BASIC_DATA"))
        source1Path = pth_toRF2("BASIC_DATA")
        destination1Path = pth_toRF2("TEMP_BACKUP")

        for files in source1:
            if (files == "RF2Snap.db") or (files == "RF2Out.db"):
                shutil.move(source1Path + files, destination1Path)

        if "UK Clin" in currentGuide:
            source2 = os.listdir(pth_toRF2("INT_BACKUP"))
            source2Path = pth_toRF2("INT_BACKUP")
            destination2Path = pth_toRF2("BASIC_DATA")
            source3 = os.listdir(pth_toRF2("TEMP_BACKUP"))
            source3Path = pth_toRF2("TEMP_BACKUP")
            destination3Path = pth_toRF2("UK_BACKUP")
            destination3 = os.listdir(pth_toRF2("UK_BACKUP"))
        else:
            source2 = os.listdir(pth_toRF2("UK_BACKUP"))
            source2Path = pth_toRF2("UK_BACKUP")
            destination2Path = pth_toRF2("BASIC_DATA")
            source3 = os.listdir(pth_toRF2("TEMP_BACKUP"))
            source3Path = pth_toRF2("TEMP_BACKUP")
            destination3Path = pth_toRF2("INT_BACKUP")
            destination3 = os.listdir(pth_toRF2("INT_BACKUP"))

        # if the backup destination (d3) is empty AND there are replacement data files waiting to replace them (s2)
        if len(destination3) == 0 and len(source2) > 0:
            # move the files...
            for files in source2:
                # replacement files to basic data location (for use)
                if (files == "RF2Snap.db") or (files == "RF2Out.db"):
                    shutil.move(source2Path + files, destination2Path)
            for files in source3:
                # temporarily cached working files to INT or UK backup
                if (files == "RF2Snap.db") or (files == "RF2Out.db"):
                    shutil.move(source3Path + files, destination3Path)
        else:
            # rollback
            source1 = os.listdir(pth_toRF2("TEMP_BACKUP"))
            source1Path = pth_toRF2("TEMP_BACKUP")
            destination1Path = pth_toRF2("BASIC_DATA")

            for files in source1:
                if files.endswith(".db"):
                    shutil.move(source1Path + files, destination1Path)

def copyDescendantsD3():
    # WinHost - checks for source and target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile(cfgPathInstall() + "files/d3/descendant-cycle.html") and os.path.isfile("/media/sf_VBox/dot/d3/descendant-cycle.html"):
            recursive_overwrite(cfgPathInstall() + "files/d3", "/media/sf_VBox/dot/d3")

def copyAncestorsD3():
    # WinHost - checks for source and target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile(cfgPathInstall() + "files/d3/ancestor-cycle.html") and os.path.isfile("/media/sf_VBox/dot/d3/ancestor-cycle.html"):
            recursive_overwrite(cfgPathInstall() + "files/d3", "/media/sf_VBox/dot/d3")

def copyFocusD3():
    # WinHost - checks for source and target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile(cfgPathInstall() + "files/d3/focus-cycle.html") and os.path.isfile("/media/sf_VBox/dot/d3/focus-cycle.html"):
            recursive_overwrite(cfgPathInstall() + "files/d3", "/media/sf_VBox/dot/d3")

def copyPreModelCycleD3():
    # WinHost - checks for source and target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile(cfgPathInstall() + "files/d3/pre-model-cycle.html") and os.path.isfile("/media/sf_VBox/dot/d3/pre-model-cycle.html"):
            recursive_overwrite(cfgPathInstall() + "files/d3", "/media/sf_VBox/dot/d3")

def backupMRCMOutputs():
    # WinHost - checks for target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile("/media/sf_VBox/dot/MRCM-pre.svg"):
            copyfile("/media/sf_VBox/dot/MRCM-pre.svg", "/media/sf_VBox/dot/MRCM-pre-bak.svg")
        if os.path.isfile("/media/sf_VBox/dot/MRCM-post.svg"):
            copyfile("/media/sf_VBox/dot/MRCM-post.svg", "/media/sf_VBox/dot/MRCM-post-bak.svg")

def backupAllDomainMRCMOutputsPre():
    copyfile(cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-pre.dot", cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-pre-alldom.dot")
    copyfile(cfgPathInstall() + "files" + sep() + 'svg' + sep() + "MRCM-pre.svg", cfgPathInstall() + "files" + sep() + 'svg' + sep() + "MRCM-pre-alldom.svg")
    # WinHost - checks for target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile("/media/sf_VBox/dot/MRCM-pre.dot"):
            copyfile("/media/sf_VBox/dot/MRCM-pre.dot", "/media/sf_VBox/dot/MRCM-pre-alldom.dot")
        if os.path.isfile("/media/sf_VBox/dot/MRCM-pre.svg"):
            copyfile("/media/sf_VBox/dot/MRCM-pre.svg", "/media/sf_VBox/dot/MRCM-pre-alldom.svg")

def backupAllDomainMRCMOutputsPost():
    copyfile(cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-post.dot", cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-post-alldom.dot")
    copyfile(cfgPathInstall() + "files" + sep() + 'svg' + sep() + "MRCM-post.svg", cfgPathInstall() + "files" + sep() + 'svg' + sep() + "MRCM-post-alldom.svg")
    # WinHost - checks for target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile("/media/sf_VBox/dot/MRCM-post.dot"):
            copyfile("/media/sf_VBox/dot/MRCM-post.dot", "/media/sf_VBox/dot/MRCM-post-alldom.dot")
        if os.path.isfile("/media/sf_VBox/dot/MRCM-post.svg"):
            copyfile("/media/sf_VBox/dot/MRCM-post.svg", "/media/sf_VBox/dot/MRCM-post-alldom.svg")

def newMRCMOutputs():
    # WinHost - checks for target files
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if os.path.isfile("/media/sf_VBox/dot/MRCM-pre.dot"):
            copyfile(cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-pre.dot", "/media/sf_VBox/dot/MRCM-pre.dot")
        if os.path.isfile("/media/sf_VBox/dot/MRCM-post.dot"):
            copyfile(cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-post.dot", "/media/sf_VBox/dot/MRCM-post.dot")

def recursive_overwrite(src, dest, ignore=None):
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            os.makedirs(dest)
        files = os.listdir(src)
        if ignore is not None:
            ignored = ignore(src, files)
        else:
            ignored = set()
        for f in files:
            if f not in ignored:
                recursive_overwrite(os.path.join(src, f),
                                    os.path.join(dest, f),
                                    ignore)
    else:
        copyfile(src, dest)

def modelToSVGLocal(inInteger):
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        scriptFile = 'modelToSvg.sh'
    else:
        scriptFile = 'modelToSvg.bat'
    if inInteger == 0 or inInteger == 2:
        subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-pre.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + 'MRCM-pre.svg', 'Rendering MRCM-pre.svg locally'])
        SVGCRForWindows('MRCM-pre.svg')
    if inInteger == 1 or inInteger == 2:
        subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-post.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + 'MRCM-post.svg', 'Rendering MRCM-post.svg locally'])
        SVGCRForWindows('MRCM-post.svg')

def SVGCRForWindows(inSVG):
    # if generated in Windows, convert carriage returns
    tempList = []
    if sys.platform == "win32":
        f = io.open(cfgPathInstall() + 'files' + sep() + 'svg' + sep() + inSVG, 'r', encoding="UTF-8")
        #read file
        for line in f:
            #replace '\n' with '&#10;' in the file
            tempList.append(line.replace('\\n', '&#10;'))
        f.close()
        f = io.open(cfgPathInstall() + 'files' + sep() + 'svg' + sep() + inSVG, 'w', encoding="UTF-8")
        # write out
        for row in tempList:
            f.write(row)
        f.close()


def modelToSVGWinHost(inInteger, copyBool):
    # WinHost
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        if copyBool:
            if inInteger == 0 or inInteger == 2:
                print('Copying MRCM-pre.svg to Windows host')
                if os.path.isfile("/media/sf_VBox/dot/MRCM-pre.svg"):
                    copyfile(cfgPathInstall() + 'files' + sep() + 'svg' + sep() + 'MRCM-pre.svg', "/media/sf_VBox/dot/MRCM-pre.svg")
            if inInteger == 1 or inInteger == 2:
                print('Copying MRCM-post.svg to Windows host')
                if os.path.isfile("/media/sf_VBox/dot/MRCM-post.svg"):
                    copyfile(cfgPathInstall() + 'files' + sep() + 'svg' + sep() + 'MRCM-post.svg', "/media/sf_VBox/dot/MRCM-post.svg")
        else:
            if inInteger == 0 or inInteger == 2:
                subprocess.call([cfgPathInstall() + 'scripts/modelToSvg.sh', '/media/sf_VBox/dot/MRCM-pre.dot', '/media/sf_VBox/dot/MRCM-pre.svg', 'Rendering MRCM-pre.svg in Windows host'])
            if inInteger == 1 or inInteger == 2:
                subprocess.call([cfgPathInstall() + 'scripts/modelToSvg.sh', '/media/sf_VBox/dot/MRCM-post.dot', '/media/sf_VBox/dot/MRCM-post.svg', 'Rendering MRCM-post.svg in Windows host'])

def changeModelToSVGLocal():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        scriptFile = 'modelToSvg.sh'
    else:
        scriptFile = 'modelToSvg.bat'
    subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-change.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + 'MRCM-change.svg', 'Rendering MRCM-change.svg locally'])
    SVGCRForWindows('MRCM-change.svg')

def changeModelToSVGWinHost(copyBool):
    # WinHost
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        copyfile(cfgPathInstall() + "files/dot/MRCM-change.dot", "/media/sf_VBox/dot/MRCM-change.dot")
        if copyBool:
            print('Copying MRCM-change.svg to Windows host')
            if os.path.isfile("/media/sf_VBox/dot/MRCM-change.svg"):
                copyfile(cfgPathInstall() + 'files' + sep() + 'svg' + sep() + 'MRCM-change.svg', "/media/sf_VBox/dot/MRCM-change.svg")
        else:
            subprocess.call([cfgPathInstall() + 'scripts/modelToSvg.sh', '/media/sf_VBox/dot/MRCM-change.dot', '/media/sf_VBox/dot/MRCM-change.svg', 'Rendering MRCM-change.svg in Windows host'])

def backupOldModelFile():
    copyfile(cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-pre.dot", cfgPathInstall() + "files" + sep() + "dot" + sep() + "MRCM-pre_old.dot")

def backupNewModelFile():
    copyfile(cfgPathInstall() + "files" + sep() + 'dot' + sep() + "MRCM-pre.dot", cfgPathInstall() + "files" + sep() + "dot" + sep() + "MRCM-pre_new.dot")

def roleToSetReportFile():
    copyfile(cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'rolereportId.txt', cfgPathInstall() + 'files' + sep() + 'ecl' + sep() + 'setreportId.txt')

def setToSearchReportFile(inLimit):
    copyfile(cfgPathInstall() + 'sets' + sep() + inLimit + '.txt', cfgPathInstall() + 'files' + sep() + 'search' + sep() + 'searchreport.txt')

def copyBackNavFiles(folderName):
    fileList = ['SSAdj.txt',
                'SSMembers.txt',
                'SSPathMembers.txt',
                'SSSource.txt',
                'SSStructural.txt',
                'SSTC.txt',
                'SSTCCount.txt']
    for fileName in fileList:
        copyfile(cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName + sep() + fileName, cfgPathInstall() + "files" + sep() + 'SS' + sep() + fileName)
    copyfile(cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName + sep() + 'ssview.dot', cfgPathInstall() + "files" + sep() + 'dot' + sep() + 'ssview.dot')

def deleteNavFolder(folderName):
    if os.path.exists(cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName):
        shutil.rmtree(cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName)

def saveNavFiles(folderName):
    fileList = ['SSAdj.txt',
                'SSMembers.txt',
                'SSPathMembers.txt',
                'SSSource.txt',
                'SSStructural.txt',
                'SSTC.txt',
                'SSTCCount.txt']
    if not os.path.exists(cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName):
        os.makedirs(cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName)
    for fileName in fileList:
        copyfile(cfgPathInstall() + "files" + sep() + 'SS' + sep() + fileName, cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName + sep() + fileName)
    copyfile(cfgPathInstall() + "files" + sep() + 'dot' + sep() + 'ssview.dot', cfgPathInstall() + "files" + sep() + 'SS' + sep() + 'store' + sep() + folderName + sep() + 'ssview.dot')

def xmapToSVGLocal():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        scriptFile = 'xmapToSvg.sh'
    else:
        scriptFile = 'xmapToSvg.bat'
    subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + 'xmap.svg'])

def picsToSVGLocal():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        scriptFile = 'picsToSvgAuto.sh'
    else:
        scriptFile = 'picsToSvgAuto.bat'
    for picfile in ["conpic", "contc", "ssview", "tagChange", "tagChangeNum"]:
        subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, "-Tsvg", cfgPathInstall() + 'files' + sep() + 'dot' + sep() + picfile + '.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + picfile + '.svg'])
        subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, "-Tpng", cfgPathInstall() + 'files' + sep() + 'dot' + sep() + picfile + '.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + picfile + '.png'])
        subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, "-Tjpg", cfgPathInstall() + 'files' + sep() + 'dot' + sep() + picfile + '.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + picfile + '.jpg'])

def picsToSVGLocalLite():
    if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
        scriptFile = 'picsToSvgAuto.sh'
    else:
        scriptFile = 'picsToSvgAuto.bat'
    for picfile in ["conpic", "contc", "ssview", "patchwork"]:
        subprocess.call([cfgPathInstall() + 'scripts' + sep() + scriptFile, "-Tsvg", cfgPathInstall() + 'files' + sep() + 'dot' + sep() + picfile + '.dot', cfgPathInstall() + 'files' + sep() + 'svg' + sep() + picfile + '.svg'])

def removeConstraintSet(constraintSetPath):
    if os.path.exists(constraintSetPath):
        os.remove(constraintSetPath)

def renameDiagramFile(inFileTemp, inFile):
    if os.path.exists(cfgPathInstall() + "files" + sep() + 'dot' + sep() + inFileTemp) and os.path.exists(cfgPathInstall() + "files" + sep() + 'dot' + sep() + inFile):
        os.remove(cfgPathInstall() + "files" + sep() + 'dot' + sep() + inFile)
        os.rename(cfgPathInstall() + "files" + sep() + 'dot' + sep() + inFileTemp, cfgPathInstall() + "files" + sep() + 'dot' + sep() + inFile)
        # picsToSVGLocalLite()

def input2_3(inString):
    if (sys.version_info > (3, 0)):
        # Python 3 code:
        return (input(inString))
    else:
        # Python 2 code:
        return raw_input(inString).decode('UTF-8')
