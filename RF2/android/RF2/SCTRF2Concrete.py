from __future__ import print_function, unicode_literals
from SCTRF2config import cfg, tok

# CONC - entire module - helper methods
# entire module
# CONC - list the concrete value taking roles
def concreteTakingRoles():
    # zeros in dict indicate the attributes that take numbers; other figures used for strings
    concreteTakingRolesDict = {tok("PRES_STR_N_V_OLD"):0,
                                tok("COUNT_ACT_IN_OLD"):0,
                                tok("COUNT_B_M_P_OLD"):0,
                                tok("COUNT_B_A_I_OLD"):0,
                                tok("COUNT_C_D_T_OLD"):0,
                                tok("CONC_STR_D_V_OLD"):0,
                                tok("CONC_STR_N_V_OLD"):0,
                                tok("HAS_PACK_SIZE_OLD"):0,
                                tok("PRES_STR_D_V_OLD"):0,
                                tok("UNIT_P_S_Q_OLD"):0,
                                tok("HAS_PROD_NAME"):1,
                                tok("HAS_SUPPLIER"):1,
                                tok("COUNT_ACT_IN_NEW"):0,
                                tok("COUNT_B_M_P_NEW"):0,
                                tok("COUNT_B_A_I_NEW"):0,
                                tok("COUNT_C_D_T_NEW"):0,
                                tok("CONC_STR_D_V_NEW"):0,
                                tok("CONC_STR_N_V_NEW"):0,
                                tok("HAS_PACK_SIZE_NEW"):0,
                                tok("PRES_STR_D_V_NEW"):0,
                                tok("PRES_STR_N_V_NEW"):0,
                                tok("UNIT_P_S_Q_NEW"):0}
    return concreteTakingRolesDict

# CONC - temporary augmentation to att list with concrete international data building attList.txt
def concreteAttAdds(inList):
    concreteAttDict = {tok("COUNT_ACT_IN_NEW"):tok("COUNT_ACT_IN_OLD"),
                        tok("COUNT_B_M_P_NEW"):tok("COUNT_B_M_P_OLD"),
                        tok("COUNT_B_A_I_NEW"):tok("COUNT_B_A_I_OLD"),
                        tok("COUNT_C_D_T_NEW"):tok("COUNT_C_D_T_OLD"),
                        tok("CONC_STR_D_V_NEW"):tok("CONC_STR_D_V_OLD"),
                        tok("CONC_STR_N_V_NEW"):tok("CONC_STR_N_V_OLD"),
                        tok("HAS_PACK_SIZE_NEW"):tok("HAS_PACK_SIZE_OLD"),
                        tok("PRES_STR_D_V_NEW"):tok("PRES_STR_D_V_OLD"),
                        tok("PRES_STR_N_V_NEW"):tok("PRES_STR_N_V_OLD"),
                        tok("UNIT_P_S_Q_NEW"):tok("UNIT_P_S_Q_OLD")}
    for att in inList:
        if att in concreteAttDict:
            inList.append(concreteAttDict[att])
    return inList

# CONC - convert any text number terms to number equivalent (UK PTs)
def toNumber(inString):
    toNumberDict = {"One":"1",
                    "Two":"2",
                    "Three":"3",
                    "Four":"4",
                    "Five":"5",
                    "Six":"6",
                    "Seven":"7",
                    "Eight":"8",
                    "Nine":"9",
                    "Ten":"10",
                    "Eleven":"11",
                    "Twelve":"12"}
    if inString in toNumberDict:
        return toNumberDict[inString]
    else:
        return inString

# CONC - if concrete cfg setting = 1, colour menu matrix accordingly
def concNumberSetting():
    if cfg("CONC_VALS") == 1:
        return "\033[92mNums-concr (1/0)\033[36m"
    else:
        return "Nums-concr (1/0)"

# CONC - if concrete cfg setting = 1, colour menu matrix accordingly
def concValueSet():
    if cfg("CONC_VALS") == 1:
        return "\033[92mSet Conc (+#No)\033[36m"
    else:
        return "Set Conc (+#No)"

# CONC - if concrete value, modify string wrap for dot file
def wrapStringCheck(inId, inItem, ):
    if inId in concreteTakingRoles() and (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
        wrapString = ""
    else:
        if inItem == "#":
            wrapString = ""
        else:
            wrapString = "\l"
    return wrapString

# CONC - string, decimal & integer identifier transformations
def stringDecimalInteger(inString):
    SDIDict = {"[[+str(\"*\")]]":tok("STRINGTYPE"), "[[+dec(>#0..)]]":tok("DECIMALTYPE"), "[[+int(>#0..)]]":tok("INTEGERTYPE")}
    return SDIDict[inString]

# CONC - check for 'concrete value codes' as substituted in stringDecimalInteger() above
def concreteValues():
    # zeros in dict indicate the attributes that take numbers; other figures used for strings
    concreteValuesDict = {tok("STRINGTYPE"):1,
                        tok("DECIMALTYPE"):0,
                        tok("INTEGERTYPE"):0}
    return concreteValuesDict
