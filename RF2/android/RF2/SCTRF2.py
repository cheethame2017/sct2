#!/usr/bin/env python
from __future__ import print_function, unicode_literals
from SCTRF2Funcs import snapHeaderWrite, snapshotHeader, focusSwapWrite, \
                        browseFromId, readFocusValue, pterm, breadcrumbsWrite, breadcrumbsRead, \
                        listify, readInSwap, readInSwapSV, readBackSearch, showUseful, allTermsFromConId, \
                        allTermsFromConIdPlusMeta, showRefsetRowsForRefset, groupedRolesFromConId, \
                        historicalOriginsFromConId, historicalOriginsFromConIdPlusMeta, conMetaFromConId, \
                        showRefsetRowsFromConAndRefset, descendantsFromConId, ancestorsFromConId, testPPS, \
                        groupedRolesFromConIdPlusMeta, refSetFromConId, refSetFromConIdPlusMeta, setPretty, \
                        drawPics, mergeVals, searchOnText, setSupers, swapDataCall, dictFromId, \
                        setEquiv, setLocale, showSupers, setAttsInt, tabIndent, promptString, showRefsetMatches, \
                        charCheck, refsetIntersectionRows, refsetOnlyRows, concNumber, resetRoleAndPreDot, \
                        returnStringToFile, concCheck, plusCheck, dotProcess, sortDiagram, latestConcepts, \
                        moduleUse, readFromRoleReport, showDefs, roleTargetsFromConId, graphAngle, deepestRels, \
                        busyCrossMaps, highDefRels, highAncesCount, highDescCount, highParentCount, \
                        highChildCount, interestingOWL, highTextDefAncCount, highHistoryCount, inactivationByReason, \
                        historyDiagram, randomConcepts, setAcrossDown, setLongShort, setAddInvisibleEdges
from SCTRF2config import cfg, tokDict
from SCTRF2Help import selectHelpFile
from platformSpecific import clearScreen, headerWriterSnap, input2_3, picsToSVGLocal
#from langTest import checkForOutliers

tok = tokDict()

def browseFromRootCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in browseFromRootCall')
    callId = (tok["ROOT"],)
    snapHeaderWrite(callId[0])
    # CONC - check if data includes concrete rows
    concCheck()
    snapshotHeader()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def changeDataCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in changeDataCall')
    rootId = (tok["ROOT"],)
    callId = readFocusValue()
    swapDataCall()
    snapHeaderWrite(rootId[0])
    # CONC - check if data includes concrete rows
    concCheck()
    snapshotHeader()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def browseFromIdCall(inId='123456'):
    if not debug:
        clearScreen()
    if debug:
        print('-> in browseFromIdCall')
    snapshotHeader()
    preCallId = []
    while True:
        if inId == '123456':
            prePreCallId = input2_3('EnterId: ')
        else:
            prePreCallId = inId
        if len(prePreCallId) < 2:
            prePreCallId = tok["ROOT"]
        if not pterm(prePreCallId) == "":
            preCallId.append(prePreCallId)
            callId = tuple(preCallId)
            # if validateVerhoeff(int(callId[0])):
            breadcrumbsWrite(callId[0])
            focusSwapWrite(callId[0])
            clearScreen()
            snapshotHeader()
            returnString = browseFromId(callId)
            if debug:
                print(returnString)
            return returnString
        else:
            clearScreen()
            snapshotHeader()
            if debug:
                print(returnString)
                preCallId.append(prePreCallId)
                callId = tuple(preCallId)
                returnString = browseFromId(callId)


def browseFromIndexCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in browseFromIndexCall')
    snapshotHeader()
    callId = inString
    if isinstance(callId, str):
        callId = listify(callId.strip())
    breadcrumbsWrite(callId[0])
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString


def showBreadcrumbs(longBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in showBreadcrumbs')
    snapshotHeader()
    returnString = breadcrumbsRead(longBool)
    if debug:
        print(returnString)
    return returnString


def readInSwapCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in readInSwapCall')
    snapshotHeader()
    returnString = readInSwap()
    if debug:
        print(returnString)
    return returnString

def readInSwapCallSV():
    if not debug:
        clearScreen()
    if debug:
        print('-> in readInSwapCall')
    snapshotHeader()
    returnString = readInSwapSV()
    if debug:
        print(returnString)
    return returnString

def readBackSearchCall(inLimit):
    if not debug:
        clearScreen()
    if debug:
        print('-> in readBackSearchCall')
    snapshotHeader()
    returnString = readBackSearch(inLimit)
    if debug:
        print(returnString)
    return returnString

def showUsefulCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> showUsefulCall')
    snapshotHeader()
    returnString = showUseful()
    if debug:
        print(returnString)
    return returnString


def showTermsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowTermsCall')
    snapshotHeader()
    callId = inString
    returnString = allTermsFromConId(callId)
    if debug:
        print(returnString)
    return returnString

def selectHelpFileCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in selectHelpFileCall')
    snapshotHeader()
    selectHelpFile(inString, 'SCTRF2')
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = dictFromId(callId)
    if debug:
        print(returnString)
    return returnString

def showTermsMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowTermsMetaCall')
    snapshotHeader()
    callId = inString
    returnString = allTermsFromConIdPlusMeta(callId)
    if debug:
        print(returnString)
    return returnString


def showRefsetsMembersMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRefsetsMembersMetaCall')
    snapshotHeader()
    callId = inString
    returnString = showRefsetRowsForRefset(callId, True)
    if debug:
        print(returnString)
    return returnString


def showRefsetsMembersCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRefsetsMembersCall')
    snapshotHeader()
    callId = inString
    returnString = showRefsetRowsForRefset(callId, False)
    if debug:
        print(returnString)
    return returnString


def showRolesCall(inString, statedBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRolesCall')
    snapshotHeader()
    callId = inString
    if statedBool:
        returnString = showRefsetRowsFromConAndRefset(callId, tok["OWL_AXIOM"])
    else:
        returnString = groupedRolesFromConId(callId)
    if debug:
        print(returnString)
    return returnString


def showHistOriginCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowHistOriginCall')
    snapshotHeader()
    callId = inString
    returnString = historicalOriginsFromConId(callId)
    if debug:
        print(returnString)
    return returnString


def showHistOriginPlusMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowHistOriginPlusMetaCall')
    snapshotHeader()
    callId = inString
    returnString = historicalOriginsFromConIdPlusMeta(callId)
    if debug:
        print(returnString)
    return returnString


def showRefsetRowsCall(memberId, refsetId):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRolesCall')
    snapshotHeader()
    returnString = showRefsetRowsFromConAndRefset(memberId[0], refsetId)
    if debug:
        print(returnString)
    return returnString

def refsetIntersectionRowsCall(memberId, refsetId, dirString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inrefsetIntersectionRowsCall')
    snapshotHeader()
    if dirString == "":
        refsetIntersectionRows(memberId[0], refsetId)
    else:
        refsetOnlyRows(memberId[0], refsetId, dirString)
    returnString = showRefsetMatches(memberId[0])
    if debug:
        print(returnString)
    return returnString


def showConceptMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowConceptMetaCall')
    snapshotHeader()
    callId = inString
    returnString = conMetaFromConId(callId)
    if debug:
        print(returnString)
    return returnString


def showDescendantsCall(inString, onlyDefsBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> indescendantsCall')
    snapshotHeader()
    callId = inString
    returnString = descendantsFromConId(callId, onlyDefsBool)
    if debug:
        print(returnString)
    return returnString

def showAncestorsCall(inString, onlyDefsBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> inancestorsCall')
    snapshotHeader()
    callId = inString
    returnString = ancestorsFromConId(callId, onlyDefsBool)
    if debug:
        print(returnString)
    return returnString

def testPPSCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> intestPPSCall')
    snapshotHeader()
    callId = inString
    returnString = testPPS(callId)
    if debug:
        print(returnString)
    return returnString

def roleTargetsCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in roleTargetsCall')
    snapshotHeader()
    callId = readFocusValue()[0]
    returnString = roleTargetsFromConId(callId)
    if debug:
        print(returnString)
    return returnString

def showRolesMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRolesMetaCall')
    snapshotHeader()
    callId = inString
    returnString = groupedRolesFromConIdPlusMeta(callId)
    if debug:
        print(returnString)
    return returnString


def showRefsetsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRefsetsCall')
    snapshotHeader()
    callId = inString
    returnString = refSetFromConId(callId)
    if debug:
        print(returnString)
    return returnString

def showRefsetMatchesCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRefsetMatchesCall')
    snapshotHeader()
    callId = inString
    returnString = showRefsetMatches(callId)
    if debug:
        print(returnString)
    return returnString

def showRefsetsMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRefsetsMetaCall')
    snapshotHeader()
    callId = inString
    returnString = refSetFromConIdPlusMeta(callId)
    if debug:
        print(returnString)
    return returnString


def setPrettyCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setPretty', str(inInteger))
    snapshotHeader()
    returnString = setPretty(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def setEquivCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setEquiv', str(inInteger))
    snapshotHeader()
    returnString = setEquiv(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def tabIndentCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tabIndent', str(inInteger))
    snapshotHeader()
    returnString = tabIndent(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def acrossDownCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> acrossDown', str(inInteger))
    snapshotHeader()
    setAcrossDown(inInteger)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def setLongShortCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setLongShort', str(inInteger))
    snapshotHeader()
    setLongShort(inInteger)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def setAddInvisibleEdgesCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setAddInvisibleEdges', str(inInteger))
    snapshotHeader()
    setAddInvisibleEdges(inInteger)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def showDefsCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showDefs', str(inInteger))
    snapshotHeader()
    returnString = showDefs(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def sortDiagramCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> sortDiagram', str(inInteger))
    snapshotHeader()
    returnString = sortDiagram(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

# CONC - send set concrete request
def concNumberCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> concNumber', str(inInteger))
    snapshotHeader()
    returnString = concNumber(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def showSupersCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showSupers', str(inInteger))
    snapshotHeader()
    returnString = showSupers(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def setAttsCall(testId, incExclString, operatorString):
    if not debug:
        clearScreen()
    if debug:
        print('-> setAtts', testId, incExclString, operatorString)
    snapshotHeader()
    returnString = setAttsInt(testId[0], incExclString, operatorString)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def setGraphAngleCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setGraphAngle', str(inInteger))
    snapshotHeader()
    returnString = graphAngle(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def setLocaleCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> setLocale', inString)
    snapshotHeader()
    setLocale(inString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def drawPicCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> drawPics', str(inInteger))
    snapshotHeader()
    returnString = drawPics(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def freezePicCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> picsToSVGLocal')
    snapshotHeader()
    picsToSVGLocal()
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def mergeValsCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> mergeVals', str(inInteger))
    snapshotHeader()
    returnString = mergeVals(inInteger)
    if debug:
        print(returnString)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def historyDiagramCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> historyDiagramCall', str(inInteger))
    snapshotHeader()
    returnString = historyDiagram(inInteger)
    callId = readFocusValue()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def reloadFocusCall(withMeta, fromRoleReport = 0):
    if not debug:
        clearScreen()
    if debug:
        print('-> reloadFocus')
    snapshotHeader()
    if fromRoleReport == 0:
        callId = readFocusValue()
    else:
        callId = readFromRoleReport(fromRoleReport)
    breadcrumbsWrite(callId[0])
    focusSwapWrite(callId[0])
    if withMeta:
        returnString = browseFromId(callId, True)
    else:
        returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def latestConceptsCall(inAllBool, onlyConBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> latestConcepts')
    snapshotHeader()
    returnString = latestConcepts(inAllBool, onlyConBool)
    if debug:
        print(returnString)
    return returnString

def moduleUseCall(fullModuleList):
    if not debug:
        clearScreen()
    if debug:
        print('-> moduleUse')
    snapshotHeader()
    returnString = moduleUse(fullModuleList)
    if debug:
        print(returnString)
    return returnString

def deepNestingCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> deepNesting')
    snapshotHeader()
    returnString = deepestRels(limitInt)
    if debug:
        print(returnString)
    return returnString

def busyCrossMapsCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> busyCrossMaps')
    snapshotHeader()
    returnString = busyCrossMaps(limitInt)
    if debug:
        print(returnString)
    return returnString

def interestingOWLCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> interestingOWL')
    snapshotHeader()
    returnString = interestingOWL(limitInt)
    if debug:
        print(returnString)
    return returnString

def highDefRelsCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> highDefRels')
    snapshotHeader()
    returnString = highDefRels(limitInt)
    if debug:
        print(returnString)
    return returnString

def highAncesCountCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> highAncesCount')
    snapshotHeader()
    returnString = highAncesCount(limitInt)
    if debug:
        print(returnString)
    return returnString

def highDescCountCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> highDescCount')
    snapshotHeader()
    returnString = highDescCount(limitInt)
    if debug:
        print(returnString)
    return returnString

def highParentCountCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> highParentCount')
    snapshotHeader()
    returnString = highParentCount(limitInt)
    if debug:
        print(returnString)
    return returnString

def highChildCountCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> highChildCount')
    snapshotHeader()
    returnString = highChildCount(limitInt)
    if debug:
        print(returnString)
    return returnString

def inactivationByReasonCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> inactivationByReason')
    snapshotHeader()
    returnString = inactivationByReason(limitInt)
    if debug:
        print(returnString)
    return returnString

def randomConceptsCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> randomConcepts')
    snapshotHeader()
    returnString = randomConcepts(limitInt)
    if debug:
        print(returnString)
    return returnString

def highHistoryCountCall(limitInt, multiTopBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> highHistoryCount')
    snapshotHeader()
    returnString = highHistoryCount(limitInt, multiTopBool)
    if debug:
        print(returnString)
    return returnString

def highTextDefAncCountCall(limitInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> highTextDefAncCount')
    snapshotHeader()
    returnString = highTextDefAncCount(limitInt)
    if debug:
        print(returnString)
    return returnString

def searchOnTextCall(inOption, inLimit, directTerm='oops'):
    if not debug:
        clearScreen()
    if debug:
        print('-> in searchOnTextCall')
    snapshotHeader()
    showDefs = False
    #not fully sorted yet - need to get this term processing into functions layer
    if directTerm == 'oops':
        prePreCallTerm = input2_3('Search text: ')
        showDefs = True
    else:
        print("Search text: " + directTerm)
        prePreCallTerm = directTerm
    returnString = searchOnText(prePreCallTerm, inOption, inLimit, showDefs)
    if debug:
        print(returnString)
    return returnString

returnString = {}
debug = False
headerWriterSnap()
clearScreen()
# returnString = browseFromRootCall()
# main control code
firstTime = True
while True:
    searchDirect = False
    returnStringToFile(returnString)
    if firstTime:
        topaction = "a"
        resetRoleAndPreDot(firstTime)
        firstTime = False
        setSupers(2)
        # CONC - reset concrete to 0 at end of session
        concNumber(0)
    else:
        topaction = input2_3(promptString(cfg("SHOW_INDEX_SNAP"))).strip()
    if topaction == "":
        topaction = "v"
    if len(topaction) > 0:
        if topaction[0] == '.':
            topaction = dotProcess(topaction)
    if topaction == 'zz':
        if debug:
            print(returnString)
        showDefs(3)
        resetRoleAndPreDot(firstTime)
        clearScreen()
        setSupers(2)
        break  # breaks out of the while loop and ends session
    elif topaction == 'a':
        if debug:
            print(returnString)
            print('-> calling browseFromRootCall')
        returnString = browseFromRootCall()
    elif topaction == 'b':
        if debug:
            print(returnString)
            print('-> calling browseFromIdCall')
        returnString = browseFromIdCall()
    elif topaction == 'xx':  # toggle *most* debug outputs
        if debug:
            debug = False
            print('debug OFF')
        else:
            debug = True
            print('debug ON')
    elif topaction == 'u':
        if debug:
            print(returnString)
            print('-> calling basicHelpCall')
        returnString = selectHelpFileCall('-')
    elif topaction == 'z':
        if debug:
            print(returnString)
            print('-> calling changeDataCall')
        returnString = changeDataCall()
    elif topaction == 'p':
        if debug:
            print(returnString)
            print('-> calling readInSwapCallSV')
        returnString = readInSwapCallSV()
    elif topaction == 'w':
        if debug:
            print(returnString)
            print('-> calling showUsefulCall')
        returnString = showUsefulCall()
    elif topaction == 'v':
        if debug:
            print(returnString)
            print('-> calling reloadFocusCall')
        returnString = reloadFocusCall(False)
    elif topaction == 'x':
        if debug:
            print(returnString)
            print('-> calling roleTargetsCall')
        returnString = roleTargetsCall()
    elif topaction == 'i':
        if debug:
            print(returnString)
            print('-> calling reloadFocusWithMetaCall')
        returnString = reloadFocusCall(True)
    elif topaction == 'r':
        if debug:
            print(returnString)
            print('-> calling showDescendantsCall')
        returnString = showDescendantsCall(readFocusValue()[0], False)
    elif topaction == 'q':
        if debug:
            print(returnString)
            print('-> calling showAncestorsCallaCall')
        returnString = showAncestorsCall(readFocusValue()[0], False)
    elif topaction == 'rd':
        if debug:
            print(returnString)
            print('-> calling showDescendantsCall')
        returnString = showDescendantsCall(readFocusValue()[0], True)
    elif topaction == 'qd':
        if debug:
            print(returnString)
            print('-> calling showAncestorsCallaCall')
        returnString = showAncestorsCall(readFocusValue()[0], True)
    else:
        try:
            if debug:
                print(topaction[0], topaction[1:])
            if len(topaction) > 2 and not (topaction[0].isalpha() and topaction[1:].isdigit()) and (not topaction[2] in "#]") and not ((topaction[0] == 'F') and topaction[1:3].isalpha() and len(topaction) == 3) and not ((topaction[0] == 'D') and topaction[1:3] in ["==", "!=", "=<", "!<", "*<"]) and not ((topaction[0] == 'J') and topaction[-1] in ["<", ">"]) and not ((topaction[0] == 'f') and (topaction[-1] == 's') and (any(char.isdigit() for char in topaction))) and not ((topaction[0] in 'rq') and (topaction[-1] == 'd') and (any(char.isdigit() for char in topaction))) and not (topaction[0] == 'Q' and topaction[-1].isdigit()):
                # then introduce identifier test, for now just search
                if topaction.isdigit():
                    # if the 'long number' is in the dictionary, use it
                    if  int(topaction) in list(returnString.keys()):
                        returnString = browseFromIndexCall(returnString[int(topaction)])
                    # if not, assume it's an ID
                    else:
                        returnString = browseFromIdCall(topaction)
                else:
                    returnString = searchOnTextCall(1, 2, topaction)
                    searchDirect = True
            elif topaction[0] == 'c' and (not searchDirect):
                if debug:
                    print('c')
                if len(topaction) == 1:
                    returnString = searchOnTextCall(1, 2)
                else:
                    if topaction[1] == '#':
                        returnString = searchOnTextCall(1, 1)
                    elif topaction[1] == ']':
                        returnString = searchOnTextCall(1, 0)
                    elif topaction[1] == '-':
                        if len(topaction) == 3:
                            if topaction[2] == '#':
                                returnString = searchOnTextCall(0, 1)
                            elif topaction[2] == ']':
                                returnString = searchOnTextCall(0, 0)
                            else:
                                returnString = searchOnTextCall(0, 2)
                        else:
                            returnString = searchOnTextCall(0, 2)
                    elif topaction[1] == '+':
                        if len(topaction) == 3:
                            if topaction[2] == '#':
                                returnString = searchOnTextCall(2, 1)
                            elif topaction[2] == ']':
                                returnString = searchOnTextCall(2, 0)
                            else:
                                returnString = searchOnTextCall(2, 2)
                        else:
                            returnString = searchOnTextCall(2, 2)
                    else:
                        returnString = searchOnTextCall(1, 2)
            elif topaction[0] == 'e':
                if topaction == 'e':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('e', callThing)
                returnString = showTermsCall(callThing[0])
            elif topaction[0] == 'd':
                if debug:
                    print(returnString)
                    print('-> calling readBackSearchCall')
                if len(topaction) == 1:
                    returnString  = readBackSearchCall("First25")
                elif len(topaction) > 1 and topaction[1:] in ('j', '+', 'h', '=', '#', 'k', 'd', "f", "g"):
                    if topaction[1] == 'd': # returns complete list
                        returnString = readBackSearchCall("JustResults")
                    elif topaction[1] == "h" or topaction[1] == "=": # reduced concepts, grouped by tlc (alpha sort)
                        returnString = readBackSearchCall("ByTLCCon")
                    elif topaction[1] == "#" or topaction[1] == "k": # reduced concepts (alpha sort)
                        returnString = readBackSearchCall("ByConAlpha")
                    elif topaction[1] == "f": # switch short list to fsns (same order as terms)
                        returnString = readBackSearchCall("ByConLenShort")
                    elif topaction[1] == "g": # switch long list to fsns (same order as terms, ? useful)
                        returnString = readBackSearchCall("ByConLen")
                    else: # 'j' or '+' group all terms by tlc (len sorted)
                        returnString = readBackSearchCall("ByTLC")
                elif len(topaction) == 2 and topaction[1].isupper():
                    returnString = readBackSearchCall(topaction[1])
                else: # safe catch!
                    returnString = readBackSearchCall("First25")
            elif topaction[0] == 'g':
                if topaction == 'g':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('e', callThing)
                returnString = showTermsMetaCall(callThing[0])
            elif topaction[0] == 'f':
                statedBool = False
                if len(topaction[1:]) > 1:
                    if topaction[-1].isalpha():
                        statedBool = True
                        topaction = topaction[:-1]
                if topaction == 'f' or topaction == 'fs':
                    callThing = readFocusValue()
                    if topaction == 'fs':
                        statedBool = True
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('f', callThing)
                returnString = showRolesCall(callThing[0], statedBool)
            elif topaction[0] == 'h':
                if topaction == 'h':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('h', callThing)
                returnString = showRolesMetaCall(callThing[0])
            elif topaction[0] == 'i':
                if debug:
                    print('i', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = showConceptMetaCall(callThing[0])
            elif topaction[0] == 'j':
                if debug:
                    print(returnString)
                    print('-> calling showBreadcrumbs')
                if len(topaction) == 1:
                    returnString = showBreadcrumbs(False)
                elif topaction[1] == "j":
                    returnString = showBreadcrumbs(True)
                else:
                    returnString = showBreadcrumbs(False)
            elif topaction[0] == 'k':
                if topaction == 'k':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('k', callThing)
                returnString = showRefsetsCall(callThing[0])
            elif topaction[0] == 'H':
                if debug:
                    print('H', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetMatchesCall(callThing[0])
            elif topaction[0] == 'l':
                if topaction == 'l':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('l', callThing)
                returnString = showRefsetsMetaCall(callThing[0])
            elif topaction[0] == 'm':
                if topaction == 'm':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('m', callThing)
                returnString = showRefsetsMembersCall(callThing[0])
            elif topaction[0] == 'n':
                if topaction == 'n':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('n', callThing)
                returnString = showRefsetsMembersMetaCall(callThing[0])
            elif topaction[0] == 'q':
                if debug:
                    print('q', returnString[int(topaction[1:])])
                onlyDefsBool = False
                if topaction[-1] == 'd':
                    callThing = returnString[int(topaction[1:-1])]
                    if cfg("SHOW_DEFS") > 0:
                        onlyDefsBool = True
                else:
                    callThing = returnString[int(topaction[1:])]
                returnString = showAncestorsCall(callThing[0], onlyDefsBool)
            elif topaction[0] == 'r':
                if debug:
                    print('r', returnString[int(topaction[1:])])
                onlyDefsBool = False
                if topaction[-1] == 'd':
                    callThing = returnString[int(topaction[1:-1])]
                    if cfg("SHOW_DEFS") > 0:
                        onlyDefsBool = True
                else:
                    callThing = returnString[int(topaction[1:])]
                returnString = showDescendantsCall(callThing[0], onlyDefsBool)
            elif topaction[0] == 's':
                if topaction == 's':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('s', callThing)
                returnString = showHistOriginCall(callThing[0])
            elif topaction[0] == 't':
                if topaction == 't':
                    callThing = readFocusValue()
                else:
                    callThing = returnString[int(topaction[1:])]
                if debug:
                    print('t', callThing)
                returnString = showHistOriginPlusMetaCall(callThing[0])
            elif topaction[0] == 'v':
                if debug:
                    print('v', topaction[1:])
                returnString = reloadFocusCall(False, int(topaction[1:]))
            elif topaction[0] == 'o':
                if debug:
                    print('o', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetRowsCall(returnString[1], callThing[0])
            elif topaction[0] == 'I':
                if debug:
                    print('I', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = refsetIntersectionRowsCall(returnString[1], callThing[0], "")
            elif topaction[0] == 'J':
                if debug:
                    print('J', returnString[int(topaction[1:])])
                testId, dirString = charCheck(topaction[1:])
                callThing = returnString[int(testId)]
                returnString = refsetIntersectionRowsCall(returnString[1], callThing[0], dirString)
            elif topaction[0] == 'M':
                if debug:
                    print(returnString)
                    print('-> calling latestConceptsCall')
                inAllBool = plusCheck(topaction)
                testId = ""
                if len(topaction) > 1:
                    testId, dirString = charCheck(topaction[1:])
                if testId == "<": # only show concepts for basic module
                    returnString = latestConceptsCall(inAllBool, True)
                elif testId == ">":
                    inAllBool = True
                    returnString = latestConceptsCall(inAllBool, True)
                else:
                    returnString = latestConceptsCall(inAllBool, False)
            elif topaction[0] == 'N':
                if debug:
                    print(returnString)
                    print('-> calling moduleUseCall')
                fullModuleList = plusCheck(topaction)
                returnString = moduleUseCall(fullModuleList)
            elif topaction[0] == 'Q':
                if debug:
                    print(returnString)
                    print('-> calling deepNestingCall')
                if len(topaction) > 1:
                    if topaction[1] == "A":
                        if len(topaction) == 2:
                            limitInt = 100
                        else:
                            limitInt = int(topaction[2:])
                        returnString = deepNestingCall(limitInt)
                    elif topaction[1] == "B":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = highDefRelsCall(limitInt)
                    elif topaction[1] == "C":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = highAncesCountCall(limitInt)
                    elif topaction[1] == "D":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = highDescCountCall(limitInt)
                    elif topaction[1] == "E":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = highParentCountCall(limitInt)
                    elif topaction[1] == "F":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = highChildCountCall(limitInt)
                    elif topaction[1] == "G":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = busyCrossMapsCall(limitInt)
                    elif topaction[1] == "H":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = interestingOWLCall(limitInt)
                    elif topaction[1] == "I":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = highTextDefAncCountCall(limitInt)
                    elif topaction[1] in ("J", "K"):
                        if len(topaction) == 2:
                            limitInt = 25
                        else:
                            limitInt = int(topaction[2:])
                        if topaction[1] == "J":
                            returnString = highHistoryCountCall(limitInt, False)
                        else:
                            returnString = highHistoryCountCall(limitInt, True)
                    elif topaction[1] == "L":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = inactivationByReasonCall(limitInt)
                    elif topaction[1] == "M":
                        if len(topaction) == 2:
                            limitInt = 50
                        else:
                            limitInt = int(topaction[2:])
                        returnString = randomConceptsCall(limitInt)
                    # else - other second letters for other queries
                else:
                    returnString = deepNestingCall(100) # default
            elif topaction[0] == 'x':
                if debug:
                    print('x', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = testPPSCall(callThing[0])
            elif topaction[0] == '-':
                if debug:
                    print('- help file for', topaction[1:])
                returnString = selectHelpFileCall(topaction[1:])
            if topaction[0] == 'y':
                if debug:
                    print(returnString)
                returnString = setPrettyCall(int(topaction[1:]))
            if topaction[0] == 'A':
                if debug:
                    print(returnString)
                if topaction[1:] == "D":
                    returnString = freezePicCall()
                elif topaction[1:] in ("A", "S", "O"):
                    returnString = drawPicCall(topaction[1:])
                else:
                    returnString = drawPicCall(int(topaction[1:]))
            if topaction[0] == 'B':
                if debug:
                    print(returnString)
                returnString = mergeValsCall(int(topaction[1:]))
            if topaction[0] == 'C':
                if debug:
                    print(returnString)
                returnString = showSupersCall(int(topaction[1:]))
            if topaction[0] == 'R':
                if debug:
                    print(returnString)
                returnString = historyDiagramCall(int(topaction[1:]))
            if topaction[0] == 'D':
                if debug:
                    print('D', topaction[1:])
                if len(topaction) >= 4:
                    testId = returnString[int(topaction[3:])]
                    incExclString = topaction[1]
                    operatorString = topaction[2]
                    returnString = setAttsCall(testId, incExclString, operatorString)
                elif topaction == "DD":
                    returnString = setAttsCall((tok["CONMOD_ATTRIB"], ), "*", "<")
                elif topaction == "Dd":
                    returnString = setAttsCall((tok["CONMOD_ATTRIB"], ), "-", "<")
                else:
                    returnString = browseFromIdCall(tok["CONMOD_ATTRIB"])
            if topaction[0] == 'E':
                if debug:
                    print(returnString)
                returnString = setEquivCall(int(topaction[1:]))
            if topaction[0] == 'G':
                if debug:
                    print(returnString)
                returnString = tabIndentCall(int(topaction[1:]))
            if topaction[0] == 'S':
                if debug:
                    print(returnString)
                returnString = acrossDownCall(int(topaction[1:]))
            if topaction[0] == 'T':
                if debug:
                    print(returnString)
                returnString = setLongShortCall(int(topaction[1:]))
            if topaction[0] == 'U':
                if debug:
                    print(returnString)
                returnString = setAddInvisibleEdgesCall(int(topaction[1:]))
            if topaction[0] == 'O':
                if debug:
                    print(returnString)
                returnString = showDefsCall(int(topaction[1:]))
            if topaction[0] == 'P':
                if debug:
                    print(returnString)
                returnString = setGraphAngleCall(int(topaction[1:]))
            # CONC - pick up set concrete request
            if topaction[0] == 'K':
                if debug:
                    print(returnString)
                returnString = concNumberCall(int(topaction[1:]))
            if topaction[0] == 'L':
                if debug:
                    print(returnString)
                returnString = sortDiagramCall(int(topaction[1:]))
            if topaction[0] == 'F':
                if debug:
                    print(returnString)
                if len(topaction) == 3:
                    returnString = setLocaleCall(topaction[1:3])
            else:
                if debug:
                    print(' ', returnString[int(topaction)])
                    returnString = browseFromIndexCall(returnString[int(topaction)])
                else:
                    returnString = browseFromIndexCall(returnString[int(topaction)])
            True
        except:
            True

# pylint disable:
#         W1401,
#         W0702,
#         C0301,
#         C0302,
#         C0103,
#         C0111,
#         C1801,
#         C0330,
#         W0104,
#         W0101,
#         C0325,
#         W0150,
#         W0621
