[E] Word equiv (1/0)

Toggles the use of a small word/phrase equivalents table for text searching
Default is E1 (use word equivalents)

Examples are mostly anatomical and morphological, such as lung/pulmonary and cancer/malignant neoplasm

NOTE: as an interim this flag is overloaded to tailor inactive relationships in diagrams:

For Word Equivalence, any value > 0 behaves like E1, but also:

To exit help page:
Press '1' to load Snapshot focus...