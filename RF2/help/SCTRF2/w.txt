[w] Useful codes

Shows a pre-designed set of 'useful' codes.

In particular this provides a shortcut to reference sets, attributes, modules and metadata value sets.

To exit help page:
Press '1' to load Snapshot focus...