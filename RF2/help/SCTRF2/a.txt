[a] Browse from root

Resets to the state when the browser first opens.

Shows top-level hierarchy for selected Snapshot data (i.e. focus concept is SNOMED CT with top level concepts below).

Children with subchildren are indicated by a '+' before the identifier.
Children with > 1500 subchildren are indicated by a '#' before the identifier. These are infrequent, but result in a long load time if selected. Reports are truncated at 1500 if using complex sorting, but all can be shown if simple alpha sort order (L0) is chosen. Actual child numbers can be found by calling 'i' in the hierarchy view.

It is possible to navigate around by inputting the index number in square brackets associated with each concept shown (e.g. [14] will navigate to the concept associated with [14] - the association is display sequential so the number + concept pairing will differ depending on context).

If the focus concept has a text definition its FSN tag is shown in blue
If the focus concept has a text definition its formal definition (either immediately or recursively) the FSN tag is followed by a magenta star.
If the focus concept previously carried descriptions now redirected by REFERS TO CONCEPT relationships, followed by '[may refer to...]'.
If there is a detectable module dependency conflict, followed by '[module dependency risk]'

L0 or L1 determine sort order: L0 - alphabetical, L1 - based on descendant count (higher counts first)

See help page for 'O' settings (show or hide presence of text definitions) for further details on these.

To exit help page:
Press '1' to load Snapshot focus...