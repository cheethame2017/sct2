[D] (+row) Spec atts

Specifies attributes to be used in concept diagrams. Probably most useful for compound/multi-focus diagrams
built in the ECL & Sets tab.

Initialisation is by either simply entering "D" or
by selecting 'w' (Useful concepts) then whatever row shows as 'Concept model attribute'

Attributes that will appear in the diagram are shown in blue.
Hidden attributes are shown in yellow.
If any attribute is hidden then 'Concept model attribute' code is shown in red.

Where attributes [D] or supertypes [C] are hidden, each diagram's definition node is shown in blue.
If both UK and international data is built, there is a chance that the atts/attLimit.txt file is out of step with the working data.
If this mismatch occurs then diagrams may miss certain attributes (in particular those taking concrete values). If this happens then each diagram's definition node is shown in red, and can be remedied by re-running the steps explained in the MRCM [x] help instructions using the intended data.

To specify a single attribute:
Enter D==N where N is the index number of the attribute of interest

To specify a role hierarchy:
Enter D=<N

Both of the above are cumulative.

To specify everything but a specific attribute (i.e. hide an attribute):
Enter D!=N

To specify everything but a role hierarchy:
Enter D!<N

These are not cumulative.

Notes:
To reset everything:
Enter "DD" - all model-mentioned attributes show blue and 'Concept model attribute' code shows green.
To specify no attributes:
Enter "Dd" - all attributes show green and 'Concept model attribute' code shows red.

Used with any concept diagram drawing settings:
[A] & [B] - for configuring how diagrams are shaped
[C] - for configuring whether supertypes are shown
ECL & Sets tab:
[B] - for building a multi-focus diagram from a named set.

To exit help page:
Press '1' to load Snapshot focus...