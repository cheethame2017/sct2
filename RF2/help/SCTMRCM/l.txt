[l] Rank directn (1-4)

Specifies the dominant direction of flow of arrows/edges:

1: bottom to top,
2: top to bottom,
3: left to right (default),
4: right to left.

To exit help page:
Press 'a' to load MRCM domain list...
