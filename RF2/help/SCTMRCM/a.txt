[a] List domains

Provides an indexed list of MRCM domains.

'bN' and 'cN' allow the domains of interest to be selected, and then the diagram generated using 'd'.

This is the view when the programme first runs, and is shown after many of the configuration values are set.

To exit help page:
Press 'a' to load MRCM domain list...
