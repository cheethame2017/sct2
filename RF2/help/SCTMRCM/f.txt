[f] Show model (1/0)

Includes (f1) or excludes (f0) attribute model features from diagram.
Whilst it may be odd to exclude model features from a model diagram, excluding leaves behind the subgraph of domain and range classes which can be useful to see in isolation.

To exit help page:
Press 'a' to load MRCM domain list...
