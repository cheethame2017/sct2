[w] Draw change

Experimental.

As for [d], but generates a 'change' diagram.

Requires two sets of data to be built with different effectiveTimes.
This is achievable if data has been built against the latest UK and the latest international, since the international tends to be 6 or 12 months more recent.

To exit help page:
Press 'a' to load MRCM domain list...
