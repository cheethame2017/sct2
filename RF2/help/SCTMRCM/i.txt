[i] Gen SVG (1/0)

i1 sets value to 1
i0 sets value to 0

If set to 1 continues processing to generate svg file (viewable in web browser)
If set to 0 ends processing with output of dot file (viewable in e.g. dotx)

To exit help page:
Press 'a' to load MRCM domain list...
