[b] Add/reset domain

[bN] selects a single domain of index N (for example if Procedure had index 15, b15 would select the Procedure domain and it would show with an asterisk in the row.
[bN+] selects a domain and any subdomains of index N (for example if Procedure had index 15, then b15+ would select Procedure, Evaluation Procedure, Surgical Procedure... and each would show an asterisk).
[bN*] selects a domain and any associated domains in the D3 groupings (e.g. puts the pharmacy domains together - not all of which are hierarchically-related).

Multiple bN instructions are additive.

Selected domains (with asterisks) are then included in the generated diagram.

[bb] resets to all domains selected. There will be no asterisks in rows of the domain list (the same as if all had asterisks).

see [c].

To exit help page:
Press 'a' to load MRCM domain list...
