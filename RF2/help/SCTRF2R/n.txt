[n] (+A..)x3 OR

Applies OR logic to the first two set letters and puts the result in the third.

For example, nABC performs A OR B and puts the result in C.

If a fourth character from <>^v or V is entered, then a diagrammatic representation of the same logic (direction determined by the character) will be shown in contc.dot.

If a diagram character is entered, the users is offered the chance to name the sets (for the diagram key). The last pair of set letter/name pairs will be retained and can be reused (in whole or part) by entering '#' at the prompt.

FSNs or pterms are shown depending on the setting for Q (1/0)

To exit help page:
Press '1' to load Snapshot focus...