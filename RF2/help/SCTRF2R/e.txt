[e] (+no) Add ungp row

Adds an ungrouped 'role row' to the constraint.

A 'role row' is of the form:
[14]  [15] 762705008 Concept model object attribute = [16] << [17] 138875005 SNOMED CT Concept

Rows are added with the instruction 'e' plus the operator index number of the row above.
For example, to add a row row beneath the role row above, enter 'e14'.

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...