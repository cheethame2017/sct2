[P] (+no) Nav. slice

Experimental.

Sets the 'level' at which navigational concepts ('JX~') are sought.

A 'low' level (e.g.< 5) will pick up abstract codes (in smaller numbers)
A 'high' level (e.g. > 25) will pick up more specific codes (possibly in larger numbers)

should be > 1 to make sense.

To exit help page:
Press '1' to load Snapshot focus...