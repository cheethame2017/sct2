[C] Constr > set(*/+/-)

Allows the construction of a constraint set.

Import and/or build a constraint with a sequence of:
'a' to import (or 's' to build from a template)
'b' to reload
'c', 'd', 'e', 'f' and 'i' to add/remove clauses
'm', 'n', 'o' to modify concept operators.

To create a new constraint set:
'C*' will begin a new constraint set with the active constraint.
'C+' will add a constraint to the active constraint set.
'C-' will remove a constraint from the active constraint set.


To exit help page:
Press '1' to load Snapshot focus...