[k] (+no) to <

Adjusts a concept operator to '<' (descendants but not self).

Syntax is simply 'k' plus the number (always even) of the operator to change (e.g. 'k6' to change operator [6]).

If the corresponding value is concrete then the operator becomes <= (shows as <)

See also [j] and [l].

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...