SCT Snapshot ECL + Sets

Companion scripts for SCT Snapshot Data Browser, providing a mechanism for editing ECL-based predicates and then manipulating and processing the resulting sets.

Basic functions include:

Building and manipulating single depth (i.e. no clause nesting) ECL constraints, or sets of such constraints.
Single constraints use AND logic between clauses; sets use OR logic between constraints.

Individual constraints can be generated from importing Snapshot browser focus definitions ([a] and/or [v] actions), or from a number of basic templates ([s]).
Constraints can then be modified:
[c], [d], [e], [f] Add or remove entire rows
[j], [k], [l] modify operators.
[i], [z] and [L] change coded values (including concrete values).

Constraints are then run against the reference data by entering [q].

Sets of concepts returned can then be given temporary names ('A', 'B', 'C'...) using [h] and can then be included in subsequent constraints as if they were reference sets in place of concepts ([z]).

Sets can also be rapidly populated from a snapshot search report ([g]) or refset membership/TC result ([t]). Named sets can be read back ([u]) as can the result of constraint processing ([r]).

Set combinations: A small nunber of AND/OR/NOT shortcuts to set combination are offered ([m], [n], [o])

Constraint combination: Constraint sets can be built up, modified, stored (with names), retrieved and deleted ([C], [D], [E], [G], [H], [I]).
After processing ([F]) resulting sets can be given temporary names and manipulated as above.

Set compression/refactoring: By various [J] actions, sets of concepts can be directly converted into constraint sets (multiple include clauses), or can be converted to more complex sets of include/exclude constraints that correspond to the original set.
Since constraint sets can be saved, this allows temporary sets to be saved (as constraint sets) - although this may introduce version dependencies.
[J] actions also include some experimental variants that try to identify navigational ancestors (which can be saved and manipulated with [S] and [T])

Visualisation: [w] draws a subgraph of set members (also producing a navigable subgraph), [B] draws the combined definitions of a set of codes, and [m], [n] and [o] can produce diagrammatic representations of two set mergers/comparisons.
More complex [w] options appear as diagram variants - in particular a treemap of descendants.

[U] groups set members by module.

To exit help page:
Press '1' to load Snapshot focus, or 'b' to return to latest constraint...