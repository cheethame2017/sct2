[i] Show inactive data

Shows only inactive data for a given effectiveTime.

For inactive descendants (whether the concepts are active or not) it is effectively meaningless to indicate whether they also have children, so instead of a '+' sign a '*' indicates the uncertainty.

Focus:
[1]	138875005		SNOMED CT Concept
	         		SNOMED CT Concept (SNOMED RT+CTV3)

Children: [7]
[2]	*246061005		Attribute
[3]	*64572001		Disease
[4]	*246188002		Finding
[5]	*106237007		Linkage concept
[6]	*257495001		Organism
[7]	*24221000000103		SNOMED CT UK administrative concepts
[8]	*4075801000001102	SNOMED-CT UK Drugs Concepts


See also:
[d] Show all data
[g] Show active data
[h] Show active+inac.

To exit help page:
Press '1' to load Full/SV browser focus...