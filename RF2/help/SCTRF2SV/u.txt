[u] (+row) Descendants

'u' plus an index number gives all descendants of that concept.
Since the number may be very high, the count of descendants is offered first, and a decision is required to list or not.

see also 't'

As for hierarchical views, the rows visible can be modified by setting effectiveTime, active & inactive and snapshot type.
Metadata can be shown or hidden with [k] and [l] settings.

To exit help page:
Press '1' to load Full/SV browser focus...