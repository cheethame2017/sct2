[e] Simple snapshot

The active snapshot view for a specific effectiveTime ('y') is calculated using a series of correlated subqueries with no reference to module dependencies.

Current status is shown in the top banner.

Most of the time this works as intended, but for some effectiveTimes and for some of the data - in particular drug data where the effectiveTime is out of step with international effectiveTime - the results can be erratic.
When this happens it may be that the snapshot should be calculated using the MDR approach.

See also:

[f] MDR snapshot
[g] Show active data
[h] Show active+inac.
[i] Show inactive data
[k] Show metadata (1/2)
[l] Hide metadata
[y] Input effecTime

Notes:
It is possible to show the inactive rows for a given snapshot by selecting 'h' (both) or 'i' (just inactive).
When showing inactive rows it is helpful to show metadata too ('k' and 'l')

To exit help page:
Press '1' to load Full/SV browser focus...
