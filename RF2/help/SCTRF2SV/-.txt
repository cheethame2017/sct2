SCT Full Data Browser

Companion scripts for SCT Snapshot Data Browser, providing a similar view built from Full data, allowing arbitrary snapshot times/effective times to be set.

This allows both comparison between the Snapshot and some arbitrary time past, and the production of through time artifacts (as Excel spreadsheet outputs and as D3/dot viewable graphs).

Snapshots can be generated using 'simple' correlated subquery mechanisms, or based upon a more complex analysis of module dependencies (the latter increasingly needed to reconstruct past data constructed from data with asynchronous release cycles).

Note:

[zz/zzz] Exit - zz just shuts down the terminal; zzz also shuts down neo4j backend.

To exit help page:
Press '1' to load Full/SV browser focus...