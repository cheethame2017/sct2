[I] (+row) Foc->D3

INN

Uses the D3 framework combined with graphviz to generate an animated visualisation of the concept definition changes for the index concept.

Data sampled at 6 month intervals.
Graphs only drawn when there are changes.
Plus:
Effective time date stamps in pink nodes.

The output can be viewed as dot/focus-cycle.html in a browser.

Two top left buttons allow the animation to be paused or reversed. Experimental - pause is a bit laggy.

To exit help page:
Press '1' to load Full/SV browser focus...