[s] (+row) Ances->xlsx

sNN

Generates an xlsx (in files/xlsx) report showing how ancestry has changed over time.

Needs the configuration settings to be [e] Simple snapshot and [g] Show active data.

A temporary screen showing the same output is shown at the end of calculations.

Key:

Alpha-ordered ancestors in rows, dates in columns.
Empty cell = concept never in the data.
# purple: Defined ancestor
+ blue: Primitive ancestor
-- green: Inactive (and not ancestor - either before or after period of ancestry)
. orange: Active elsewhere - not an ancestor at that time.
Blue border - immediate parents at each effectiveTime.

Inactive concept historical targets are shown in information hover box.

To exit help page:
Press '1' to load Full/SV browser focus...