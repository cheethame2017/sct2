[q] (+row) Terms

Shows the terms for any concept identified by its index number.
For example 'q12' will show the terms for the concept with index number 12.

Terms are show as:

FSN
Preferred term
Acceptable synonyms
Definition

DescriptionId are not selectable.

As for hierarchical views, the rows visible can be modified by setting effectiveTime, active & inactive and snapshot type.
Metadata can be shown or hidden with [k] and [l] settings.

To exit help page:
Press '1' to load Full/SV browser focus...