[g] Show active data

Restricts view to active rows at a given effectiveTime.

See also:
[d] Show all data
[h] Show active+inac.
[i] Show inactive data

To exit help page:
Press '1' to load Full/SV browser focus...