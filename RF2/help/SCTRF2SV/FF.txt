[F] D3 extras (0-2)

Shows or hides extra features on D3 timeline images: 

F0 -> nothing apart from active elements shown (blue)
F1 -> show active components plus previous elements that are now inactive (green)
F2 -> show active components plus previous elements that are now inactive, plus show still actives that have moved (orange)

To exit help page:
Press '1' to load Full/SV browser focus...