# Building the database files

## Overview

The steps involved in retrieving and building the SNOMED CT data can be summarised as:

![Download and build overview](images/DataBuildOverview2.png)

Data build scripts assume Linux both in terms of paths and utilities (e.g. sed), however the functional/browser scripts have all been tested and work on Windows. It is therefore easiest to build the data on a Linux or Linux-like system (including Windows Subsystem for Linux on Windows 10). The Linux capability means that the Snapshot functional/browser scripts work on Android.

In more detail:

## Initialise

*1. Create folders:* Any folder arrangement could work, but what follows is used in the various scripts run in Linux.
1. Create a top-level folder called D, with subfolders /Out and /RF2. This is where most of the 'building' goes on.
2. Create a folder for the repository (and if you use a python virtual environment one of these for the executable build)
3. Create a folder where the data files will sit in use.

*2. Run `initialFix.py`:* With the above folders created, the repo cloned and python built (with `pip install` of `unidecode` for the **Snapshot** executable plus `python-dateutil`, `neo4j` and `xlsxwriter` for the **Full** executable), .

This script prompts you to input the folder paths you have created for the data build, SQLite data storage and Neo4j data. The executable path is automatically detected.

**As of mid 2021 I have found that the neo4j launch scripts only work if the neo4j path includes a 'NEO4J_HOME' root (for example a full path of /home/ed/Data/NEO4J_HOME/neo4j-community-4.0.3). Previously any path seemed to do.**

**Depending upon the OS used, paths are stored in _files/lin/SCTRF2datapaths.cfg_ or _files/win/SCTRF2datapaths.cfg_. As long as this file can be preserved between data builds (and more importantly between pulls of updated code from the repo - e.g. by stash/pop) the initialise step only needs to be run once.**

## Prepare import script

*1. Download data files:* Download the data files from preferred location. This may be just the international data, or may also include local or experimental extensions (noting that combining the latter may introduce confusing dependencies)

Place all the data files into your data build folder e.g. D/RF2 (these will be unzipped by `fillAndBuildInit.py`):

![Data in RF2 folder](images/DataInRF2.png)

For the new UK packaging, duplicate 'Edition' folders (from either clinical or drug data) will be unzipped but by overwriting only leave one for futher processing.

*2. Run `fillAndBuildInit.py`:* This script coordinates unzipping and preparing the data files as well as copying necessary scripts for the next step.

Note: if bundling together outdated UK pathology data with more recent UK clinical/pathology data (I know this is wrong from a dependency POV) then delete the UK Edition folder from within the pathology zip file. If you don't do this then the UK Full data will contain a huge number of duplicate edition-derived rows which will mean the deduplication stage of the full build will grind to a halt (preferably this will only be dealing with complete duplicates in the MDR).

*3. Set LAN variable :*

...and...

*4. Run `importScriptBuilder.py`:* Any database import (and analysis/processing) mechanism needs to be able to keep up with evolving reference set patterns. Here this is achieved for each release with the use of `importScriptBuilder.py`. 

Preliminary steps are:

Set the LAN variable (determines the 'LAN'guage reference set to use for term preferences) depending on the build type:
+ LAN = 1 if doing NHS data build (thus using NHS RLR)
+ LAN = 2 if doing interim International build (using en-GB LR)

![importScriptBuilder2.py header settings](images/importScriptBuilder3.png)

Run importScriptBuilder

This can be run in its native location:

![importScriptBuilder2.py run](images/run_importScriptBuilder2.png)

`importScriptBuilder.py` produces `SCTRF2Refset.py` and `importGuide.py`.

`SCTRF2Refset.py` provides methods which can handle all refset patterns detected in the data in /Out, and is called by Snapshot and Full code to process all varieties of active refset.

_Note: This saves a date-stamped copy of the previous `SCTRF2Refset.py` as the stable version may be overwritten if undertaking an interim build, e.g. to inspect international data before an official UK release.
The backup file can be removed if not needed, or used to restore the correct file once an interim build and inspection is complete:_

![replacementRefset file](images/replacementRefset.py.png)

`importScriptBuilder.py` also produces `ImportGuide.py`:

![importGuide](images/importGuide.py.png)
---

*5. Run `importGuide.py`:*  This can be run in its native location, and produces the database import script `importSnapshot.txt`, `importFull.txt` (or theoretically `importDelta.txt`) depending on your responses to the input prompts:

![importGuide prompts](images/run_importGuide.py.png)

Most commonly this will be a **Snapshot** build, but could also be **Full**. Entering Delta will 'run', but the scripts are not yet set up to append the Delta in any meaningful way.

## Build Snapshot

*1. Run `fillAndBuildOutGuidePreMove.py`*

![fillAndBuildOutGuidePreMove call](images/run_fillAndBuildOutGuidePreMove.py.png)

This creates the basic snapshot data file (`RF2Snap.db`), deduplicates any relevant rows in the UK data (if not done at source - log of results in files/dedup), creates the supporting index file (`RF2Out.db`) and fills with search index and basic transitive closure files.

*2. Move data to /Data folder*

Assuming a different location for the working database files to their initial build location, manually move the `RF2Snap.db` and `RF2Out.db` files to the new location.

*3. Run `fillAndBuildOutGuidePostMove.py`*

![fillAndBuildOutGuidePostMove call](images/run_fillAndBuildOutGuidePostMove.py.png)

This creates some closing index and cache tables (refset and top-level chapter comparison tables, concrete value lookup tables and MRCM/attribute supporting tables).

The Snapshot data is now built and in place. It should now be able to run the top-level script `SCTRF2.py`:

![SCTRF2.py from shell](images/SCTRF2.py.png)

If this works then you should see something like this (showing Snapshot browser):

![Snapshot browser landing page](images/SnapBrowser1.png)

Additionally, the program(s) can be launched from a series of scripts  - working examples held in /home/ed/Code/Repos/sct/RF2/scripts/. One of these ('sct'), for example launches the snapshot, ECL/sets Full data and MRCM browsers in a tabbed Linux mint xfce4 terminal. A set of similar example launch scripts for Windows are in the /WindowsRF2Launch/ sub folder.

## Build Full

(Note Build Full depends on the first step of Build snapshot - to create the basic Snapshot data file which is used in the neo4j build step. It is also unlikely that you would ever only perform Build Full because entry to the full data (through search or identifier input) requires the Snapshot browser.

*1. Run `fillAndBuildFullGuidePreMove.py`*

![fillAndBuildFullGuidePreMove call](images/run_fillAndBuildFullGuidePreMove.py.png)

This script generates the basic Full database file, the moduleSet file if needed, and generates the `rels.csv` and `cons.csv` files for the neo4j build.

*2. Move data to /Data folder*  

Manually move the `RF2Full.db` and `RF2ModSet.db` files to the /Data folder.

*3. Expand new Neo tree and move Neo data*

**As noted above: as of mid 2021 I have found that the neo4j launch scripts only work if the neo4j path includes a 'NEO4J_HOME' root (for example a full path of /home/ed/Data/NEO4J_HOME/neo4j-community-4.0.3). Previously any path seemed to do.**

Freshly expand a neo4j build folder (e.g. into /home/ed/Data/NEO4J_HOME/ folder). Get the latest zip file release from https://neo4j.com/download-center/  Community server tab.

![Neo folder tree](images/NeoFolderTree.png)

And put the `rels.csv` and `cons.csv` files into the /import folder of the neo4j build tree

![Rels and cons in place](images/NeoImportPrep13.png)

*4. Build Neo data:*

With 6 monthly releases this has often changed in the interval. Often syntactically still correct CYPHER code that previously worked will throw a warning or error because the query engine has changed. Frustrating but what can you do...

First, from within the /bin folder, start the neo4j session.

In Linux

+ ed@ed-VirtualBox ~/N4J/neo4j-community--N.N.N/bin $ ulimit -n 40000
+ ed@ed-VirtualBox ~/N4J/neo4j-community--N.N.N/bin $ ./neo4j start

In Windows

+ C:\n4j\neo4j-community-N.N.N-windows\neo4j-community-N.N.N\bin>neo4j start

![Start neo4j](images/NeoImportPrep14.png)

Ever-changing neo4j versions result in different behaviour at this point. Recently Linux ran well, giving this feedback:

![Start neo4j feedback](images/NeoImportPrep15.png)

However recently this was prefaced by:

_WARNING! You are using an unsupported Java runtime._ 

+ _Please use Oracle(R) Java(TM) 8, OpenJDK(TM) or IBM J9 to run Neo4j._
+ _Please see https://neo4j.com/docs/ for Neo4j installation instructions._

If you install a new version of neo4j in windows and get stuck with old service (based on earlier community edition) do the following. If this is a fresh install in windows, follow the steps from 'In new path':

+ Find the 'commons daemon service' in task manager or neo4j in the task manager services tab, right click to 'open services'
+ Find and disable the neo4j service, then, in new community edition folder:

+ **To update service, stop and uninstall along old path:**
+ C:\n4j\neo4j-community-3.4.9-windows\neo4j-community-3.4.9\bin>neo4j stop
+ -> Neo4j service stopped
+ C:\n4j\neo4j-community-3.4.9-windows\neo4j-community-3.4.9\bin>neo4j uninstall-service
+ -> Neo4j service uninstalled

![neo4j Windows build](images/win-old-neo-stop.png)

+ **In new path:**
+ C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j install-service
+ -> Neo4j service installed
+ **+/-**
+ C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j update-service
+ -> Neo4j service updated

+ **Then**
+ C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j start
+ -> Neo4j service started
+ This should produce a new service of 'automatic start service type'

+ **To start service:**
+ C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j start
+ -> Neo4j service started

![neo4j Windows build](images/win-new-neo-start.png)

then, open the neo4j session in a browser (http://localhost:7474/browser/), 
_neo4j_ first time as password/username:

![neo4j login](images/NeoImportPrep17.png)

...then change to _neo_...

![neo4j update password](images/NeoImportPrep18.png)

and run the three scripts in `Import/NeoImportScriptsMinimal.txt` [note, as of neo4j 4, needs the :auto prefix for the PERIODIC COMMIT line to be accepted]:

**Import concepts**

![import concepts1](images/NeoImportPrep30.png)

![import concepts2](images/NeoImportPrep31.png)

**Index concepts**

![index concepts](images/NeoImportPrep28.png)

![index concepts](images/NeoImportPrep29.png)

**Import relationships (temperamental!)**

![import relationships](images/NeoImportPrep32.png)

![import relationships](images/NeoImportPrep33.png)

This has to be done separately in Linux and Windows (by contrast the sqlite files can just be copied). An indication of progress can be given by opening the database view on the left:

![db progress](images/NeoImportPrep34.png)

Once built, you can shut down the browser view and forget about it since the SCTRF2SV python code starts and stops the neo4j session when it runs or closes. You can, however, run arbitrary cypher queries through this interface. Copies of each ancestor/descendant query are persisted in files/cypher_sv.txt.

NeoImportScripts.txt also contains some guidance on how to detect and deal with duplicate concepts - these may be introduced if you mess around adding out of date data such as the LOINC refsets.

Consider running scripts/importAndBuildTidy.sh to remove the build scripts, the no longer needed RF2Full2.db file and tidy the data files in /Out into a zip (to keep for a bit and then delete). 

## Optionally

*1. Run `fillAndBuildTidy.py*

This removes the script files temporarily added to the data build folder and compresses the expanded data files in \Out\\ (rather than simply delete them straight away).