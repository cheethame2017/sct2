import subprocess
import os
from SCTRF2config import pth
from deduplicateFull import deduplicate
from NeoImport1Prep import NeoImport1Prep

dataBuildPath = pth('BASIC_DATA_BUILD') # location of data files during construction
dataPath = pth('BASIC_DATA') # location of data files during use

fullPath = dataBuildPath + "RF2Full.db"
modSetPath = dataBuildPath + "RF2ModSet.db"
tempFullPath = dataBuildPath + "RF2Full2.db"

def deleteDbFile(inPath):
    if os.path.isfile(inPath):
        os.remove(inPath)
    else:
        print("Note: %s not found so not deleted" % inPath)

# # fill full
subprocess.call(["sqlite3", fullPath, ".read " + dataBuildPath + "importFull.txt"])
subprocess.call(["sqlite3", fullPath, ".quit"])

# deduplicate
deduplicate(fullPath)

# generate ModuleSet database (no need if a previous one exists)
if not os.path.exists(dataPath + "RF2ModSet.db"):
    subprocess.call(["sqlite3", modSetPath, ".read " + dataBuildPath + "buildRF2ModSet.txt"])
    subprocess.call(["sqlite3", modSetPath, ".quit"])

# Neo4j build
# 1. Generate import script
NeoImport1Prep(dataBuildPath + "Out/")

# 2. fill temp file & generate rels.csv for Neo import
subprocess.call(["sqlite3", tempFullPath, ".read " + dataBuildPath + "NeoDataPrepScripts_1.txt"])
subprocess.call(["sqlite3", tempFullPath, ".quit"])

# 3. delete temp file
deleteDbFile(tempFullPath)

# 4. locate Snap file & generate cons.csv for Neo import
if os.path.exists(dataPath + "RF2Snap.db"):
    subprocess.call(["sqlite3", dataPath + "RF2Snap.db", ".read " + dataBuildPath + "NeoDataPrepScripts_2.txt"])
    subprocess.call(["sqlite3", dataPath + "RF2Snap.db", ".quit"])
else:
    subprocess.call(["sqlite3", dataBuildPath + "RF2Snap.db", ".read " + dataBuildPath + "NeoDataPrepScripts_2.txt"])
    subprocess.call(["sqlite3", dataBuildPath + "RF2Snap.db", ".quit"])
