from __future__ import print_function, unicode_literals
import sqlite3

rf1Path = "/home/ed/Data/RF1.db"

def createTempTables():
    connectionIn2 = sqlite3.connect(rf1Path)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("DROP TABLE IF EXISTS GephiConcepts;")
    cursorIn2.execute("CREATE TABLE GephiConcepts (CONCEPTID text, CONCEPTSTATUS integer, FULLYSPECIFIEDNAME text, CTV3ID text, SNOMEDID text, ISPRIMITIVE integer);")
    cursorIn2.execute("DROP TABLE IF EXISTS GephiRelationships;")
    cursorIn2.execute("CREATE TABLE GephiRelationships (RELATIONSHIPID text, CONCEPTID1 text, RELATIONSHIPTYPE text, CONCEPTID2 text, CHARACTERISTICTYPE integer, REFINABILITY integer, RELATIONSHIPGROUP integer);")
    cursorIn2.execute("DROP TABLE IF EXISTS GephiNodes;")
    cursorIn2.execute("CREATE TABLE GephiNodes (Node text, Id text, Label text, Type text);")
    cursorIn2.execute("DROP TABLE IF EXISTS GephiEdges;")
    cursorIn2.execute("CREATE TABLE GephiEdges (Source text, Target text, Label text);")
    connectionIn2.commit()

createTempTables()
