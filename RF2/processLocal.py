from __future__ import print_function, unicode_literals  # Python 2 users only
import sys
import io
import sqlite3
# import unicodedata
import nltk
from nltk.util import ngrams
from nltk.corpus import stopwords

# script for wikipedia code - performs the 'nlp' proessing steps.

# words_I_want = ['i', 'not', 'no', 's', 't', 'd', 'm', 'o', 'y', 'a', 'during', 'before', 'after', 'both', 'down', 'her', 'hers', 'all', 'by', 'from', 'out', 'under', 'me', 'be', 'as']
words_I_want = ['i', 'not', 'no', 's', 't', 'd', 'm', 'o', 'y', 'a', 'during', 'before', 'after', 'both', 'down', 'her', 'hers', 'all', 'to', 'by', 'from', 'out', 'under', 'me', 'be', 'as', 'more', 'over', 'greater', 'under', 'below', 'less']
stopwords = [w for w in nltk.corpus.stopwords.words('english') if w not in words_I_want]

porter = nltk.PorterStemmer()

def equivTester(inList, equivList):
    counter = 1
    for sentPair in inList:
        tempTermList = []
        counter += 1
        sent = sentPair[1]
        tempTermList.append(sent)
        matches = sentPair[0]
        for equiv in equivList:
            matchList = multi_find(sent.lower(), equiv[1].lower())
            if len(matchList) > 0:
                for marker in matchList:
                    matches.append((marker, equiv[0]))
        powerMatchList = []
        prePowerMatchList = get_power_set(inList[0][0])
        powerMatchList = prePowerMatchList[1:] #
        for tempterm in tempTermList:
            if len(tempTermList) < 120: # arbitrary list length in case of 'urethra' / 'urethral' cycle; needs better solution
                for items in powerMatchList:
                    items.sort(key=lambda x: x[0], reverse=True)
                    for item in items:
                        equivTermRefList = []
                        equivTermRefList = equivTerms(equivList, item[1])
                        temptempterm = tempterm
                        for term1 in equivTermRefList:
                            for term2 in equivTermRefList:
                                upTo = temptempterm[0:item[0]].lower()
                                beyond = temptempterm[item[0]:].lower()
                                if not(term1 == term2):
                                    beyond = beyond.replace(term1, term2, 1)
                                    if not((upTo + beyond) == sent):
                                        if not((upTo + beyond) in tempTermList):
                                            tempTermList.append(upTo + beyond)
    returnList = [" ".join(list(set(item.split()))) for item in tempTermList if len(sorted(item.split(), key=len)[-1]) < 50] # and limit ridiculous longest word
    return returnList

def equivTerms(inEquivs, inId):
    outList = []
    for item in inEquivs:
        if item[0] == inId:
            outList.append(item[1])
    return outList

def readInEquivs():
    readbackList = []
    returnList = []
    try:
        with io.open(equivPath, "r", encoding="utf-8") as f:
            for line in f:
                readbackList.append(line)
        f.close()
        for item in readbackList:
            row = item.split("\t")
            rowTemp = [r.strip() for r in row]
            returnList.append(rowTemp)
    finally:
        return returnList

def readInAcros():
    returnList = []
    counter = 1
    matchType = 0
    conn = sqlite3.connect(porterPath)
    cur = conn.cursor()
    cur.execute("select * from abbrevs where matchType <= ? ", ( matchType, ))
    recordset = cur.fetchall()
    for row in recordset:
        acro = sorted(set(m1.upper() for m1 in [porter.stem(m2) for m2 in nltk.word_tokenize(row[0])]))[0]
        expansion = " ".join(sorted(set(m1.upper() for m1 in [porter.stem(m2) for m2 in nltk.word_tokenize(row[1])] if (m1.lower() not in stopwords))))
        tempList = [str(counter), acro, expansion, '3']
        if tempList[1] != "" and tempList[2] != "":
            returnList.append(tempList)
        counter += 1
        if counter == 310:
            print(str(counter))
    cur.close()
    conn.close()
    return returnList

def multi_find(string, value, start=0, stop=None):
    values = []
    while True:
        found = string.find(value, start, stop)
        if found == -1:
            break
        values.append(found)
        start = found + 1
    return values

def get_power_set(s):
    power_set = [[]]
    for elem in s:
        for sub_set in power_set:
            power_set = power_set+[list(sub_set)+[elem]]
    return power_set

def newDB(dbpath):
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur.execute("CREATE TABLE aggregateResults (dateField TEXT, inputId TEXT, inputTerm TEXT, category TEXT, type TEXT, outputId TEXT, outputTerm TEXT)")
    cur.execute("CREATE TABLE allMatches (inputId TEXT, category TEXT, outputDId TEXT, outputCId TEXT, outputTerm TEXT, keyTerm TEXT, sentenceNumber TEXT)")
    cur.execute("CREATE TABLE sentences (inputId TEXT, category TEXT, sentenceNumber TEXT, sentenceText TEXT)")
    cur.execute("CREATE TABLE possNegation (inputId TEXT, category TEXT, sentenceNumber TEXT, outputId TEXT, outputTerm TEXT, sentenceTextNeg TEXT)")
    conn.commit()
    cur.close()
    conn.close()

def tempListToDB(preserveURList):
    conn = sqlite3.connect(dbPath)
    cur = conn.cursor()
    for URtempList in preserveURList:
        # print URtempList
        cur.execute("insert into allMatches values (? , ? , ? , ? , ? , ? , ?)", URtempList)
    conn.commit()
    cur.close()
    conn.close()

def aggResToDB(preserveARList):
    conn = sqlite3.connect(dbPath)
    cur = conn.cursor()
    for ARtempList in preserveARList:
        print("PRO", ARtempList)
        cur.execute("insert into aggregateResults values (? , ? , ? , ? , ? , ? , ?)", ARtempList)
    conn.commit()
    cur.close()
    conn.close()

def sentToDB(sentList):
    conn = sqlite3.connect(dbPath)
    cur = conn.cursor()
    for sentTempList in sentList:
        print("PRO", sentTempList)
        cur.execute("insert into sentences values (? , ? , ? , ?)", sentTempList)
    conn.commit()
    cur.close()
    conn.close()

def depunct(inString):
    a = ".,/=-+><^?@~{}[]'\"*():;#"
    for b in range(1, len(a)):
        inString = inString.replace(a[b], " ")
    return inString


def readInList():
    readbackList = []
    returnList = []
    # first time around; no autosuggest
    try:
        with io.open(mainPath + "testset.txt", "r", encoding="utf-8") as f:
            for line in f:
                readbackList.append(line)
    finally:
        f.close()
    for item in readbackList:
        tabpos = item.find('\t')
        returnList.append(item[0:tabpos])
    return(returnList)

# (363661006	Reason not stated concept used to pad exclusion clauses)

if len(sys.argv) != 4:
    # s = input('Wiki (w), list of terms (l), NICE document (n) or definitions/HPO (h)?')
    # t = input('Process to new db (p), append (a) or just show folderlist (j)?')
    # u = input('original (o) or new (n) code?')
    s = 'l'
    t = 'a'
    u = 'o'
else:
    s = str(sys.argv[1]) # wikipedia entry or NICE document
    t = str(sys.argv[2]) # new db, append to current, or just show folderlist content
    u = str(sys.argv[3]) # run against original datafiles/code or new ones

if u == 'o':
    # original
    mainPath = '/home/ed/wiki/files1/'
    dbPath = '/home/ed/D/RF2Wiki.db'
    porterPath = '/home/ed/D/RF2Porter.db'
    equivPath = '/home/ed/Code/Repos/sct/RF2/files/search/wordEquiv.txt'
else:
    # new
    mainPath = '/home/ed/files/wiki2/files/'
    dbPath = '/home/ed/files/wiki2/RF2Wiki.db'
    porterPath = '/home/ed/files/wiki2/RF2Porter.db'
    equivPath = '/home/ed/files/wiki2/wordEquiv.txt'

lateAcros = False

# process as for list: don't look for categories
folderList = [["out", "All", [["71388002", ["103693007", "386053000"]],
            ["103693007", ["363661006"]],
            ["386053000", ["363661006"]],
            ["105590001", ["410942007"]],
            ["410942007", ["363661006"]],
            ["373873005", ["9191801000001103"]],
            ["49062001", ["363661006"]],
            ["123037004", ["49755003"]],
            ["49755003", ["107675007"]],
            ["107675007", ["363661006"]],
            ["129264002", ["363661006"]],
            ["404684003", ["64572001"]],
            ["64572001", ["363661006"]],
            ["243796009", ["413350009", "129125009"]],
            ["413350009", ["363661006"]],
            ["129125009", ["363661006"]],
            ["272379006", ["363661006"]],
            ["410607006", ["409822003", "49872002", "414561005"]],
            ["409822003", ["363661006"]],
            ["49872002", ["363661006"]],
            ["414561005", ["363661006"]],
            ["272398008", ["363661006"]],
            ["263714004", ["363661006"]]]]]

testString = "1184	Abdominal Pain, Rectal Bleeding, Pregnant Over 20 Weeks"

identifierList = readInList()
# limitedIdentifierList = [identifier for identifier in identifierList if identifier == '127300000']  # testId
limitedIdentifierList = identifierList  # everything
if t == 'j':
    limitedIdentifierList = []  # just show folderlist interpretation

sents = []
counter = 1
total = len(limitedIdentifierList)
equivList = readInEquivs()
if lateAcros:
    acroList = readInAcros()
sents.append(testString)
testables = []
sentNumber = 1
sentList = []
for sent in sents:
    sentTempList = []
    sentTempList.append(1)
    sentTempList.append(1)
    sentTempList.append(1)
    sentTempList.append(sent.strip())
    # if isinstance(sent, str):
    #     sent = unicodedata.normalize('NFKD', sent).encode('ascii', 'ignore')
    n_gram_list = []
    powerTempList = []
    for n in range(1, 11):
        n_grams = ngrams([w for w in nltk.word_tokenize(depunct(sent)) if (w.lower() not in stopwords) and (w.isalnum())], n)
        n_gram_list = list(n_grams)
        equiv_n_gram_list = []
        equivListTemp = []
        equiv_n_gram_testable_list = []
        for n_gram in n_gram_list:
            equiv_n_gram_testable_list.append([[], " ".join(n for n in n_gram).lower()])
        for item in equiv_n_gram_testable_list:
            equivListTemp = equivTester([item], equivList)
            for item in equivListTemp:
                equiv_n_gram_list.append(tuple(nltk.word_tokenize(item)))
        toPowerTempList = []
        for n_gram in equiv_n_gram_list:
            tempTestable = []
            ports = [porter.stem(m) for m in n_gram]
            toPowerPortsList = sorted(set(m.upper() for m in ports))
            portsList = " ".join(toPowerPortsList).strip()
            tempTestable.append(portsList)
            for port in toPowerPortsList:
                toPowerTempList.append(port)
            tempTestable.append(sentNumber)
            if tempTestable not in testables:
                testables.append(tempTestable)
        if (n == 1) and (s == 'l') and ((" or " in sent) or (", " in sent) or (" and/or " in sent)) and (len(toPowerTempList) < 35): # last clause to protect against fallout from incomplete urethra/urethral equiv fallout
            powerTempList = get_power_set(toPowerTempList[:11]) # avoid runaway
            for item in powerTempList[1:]:
                testables.append([" ".join(sorted(item)), sentNumber])
    sentList.append(sentTempList)
    sentNumber += 1

    if lateAcros:
    # late acros: from expansion to contraction - proof of concept rather than useful.
        print("testables=", str(len(testables)))
        if len(testables) < 5000:
            tempTestables = []
            for testable in testables:
                for thing in acroList:
                    if (all(x in testable[0].split(" ") for x in thing[2].split(" "))):
                        testable2 = [item for item in testable[0].split(" ") if item not in thing[2].split(" ")]
                        testable2.append(thing[1])
                        if [" ".join(sorted(testable2)), testable[1]] not in tempTestables:
                            tempTestables.append([" ".join(sorted(testable2)), testable[1]])
            testables = [*testables, *tempTestables]

resultList = []
conn = sqlite3.connect(porterPath)
cur = conn.cursor()
tableList = ["porterm", "porterm2_A", "porterm2_B", "porterm2_E", "porterm2_F"]
for table in tableList:
    for testable in testables:
        cur.execute("select id, conceptId, unideterm from " + table + " where porterm = ?", (testable[0],))
        recordset = cur.fetchall()
        if len(recordset) > 0:
            if s == "l":
                print(testable[0])
            for record in recordset:
                tempList = []
                for item in record:
                    tempList.append(item)
                tempList.append(testable[0])
                tempList.append(testable[1])
                resultList.append(tempList)

uniqResultList = list(set(map(tuple, resultList)))
resultList.sort(key=lambda x: x[3])
uniqResultList.sort(key=lambda x: x[3])

print()
print(testString)
print()
for item in uniqResultList:
    print(item)


cur.close()
conn.close()
