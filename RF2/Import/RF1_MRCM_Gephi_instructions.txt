RF1 data
--------

Check location of the RF2Snap and RF2Out files and update paths accordingly), run RF1Builder.py from its original git location.

try to check that conmodules list is complete - this has been changing recently!

** make sure all the method triggers at the bottom of the script are uncommented.
this generates a temporary RF1.db file, and then builds a set of text files in /home/ed/RF1/ (make sure this exists)
these output files can be passed to windows and imported into respective access files.
processing files are in D:\PrepareFIX.
Until I fix this, these have to be run in a 'D' drive (e.g on a usb - slow or on a machine with a D partition)
or create virtual drive in windows with substr, e.g. for a folder C:\D:

Microsoft Windows [Version 10.0.19044.1586]
(c) Microsoft Corporation. All rights reserved.

C:\Users\echee>subst D: C:\D

Copy clean set of files from DATA FILES to PrepareFIX folder above.
No new linking should be needed.

Import and index all the text files.

Imports finally saved as scripts. Run each importModule/runImports() method followed by index/addController() or addController1().

** in SCTData2:
fill conceptsplus [ConceptsPlusQuery and toggle to append]
and run second index script - this indexes conceptsplus and tc (note supertypefeature is only ever 1 in tc), then runs tcsubtypebuild, then indexes this.

** in SubsetAnalysisHist.accdb:
by hand:
import subsetnames into concatnames
import allsubsets into allsubsets
append chapters to subset list [concatnames] from [concatnamesTLC] and then calculate membership [GenerateSheetModule.AddTC()]
index.
generate text file output (check target location in CreateATestfile method) [GenerateSheetModule.comparecall()]
paste into clean SubsetComparison YYYYMM.xlsm

MRCM diagrams:
--------------

run writeModelTableHelpers.py in location.
if most recent international build postdates the UK international dependent build, then run:
/home/ed/git/rf2repo/sct/RF2/scripts/MRCMChange.sh
to produce change diagram.

preliminaries:
decide which domains to include

Gephi data
----------

The RF1 build steps now produce 'GephiConcepts' and 'GephiRelationships' as text file outputs.
Whilst I should really shorten what follows, these steps aren't significant and produce the csv files that can be imported into gephi

1. check path, and then run GephiBuilderPre.py to create intermediate tables back in RF1.db
2. pull the text files back in with sqlite3 RF1.db / .read GephiPrepScripts_1.txt
3. produce the output tables with GephiBuilder.py
4. export the csv files with sqlite3 RF1.db / .read GephiPrepScripts_2.txt
5. copy across to windows machine and import into gephi (0.92 64 bit version given the size).