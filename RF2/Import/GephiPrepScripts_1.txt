.echo OFF
.separator \t
.import /home/ed/RF1/GephiConcepts.txt GephiConcepts
.import /home/ed/RF1/GephiRelationships.txt GephiRelationships
CREATE INDEX gephiconcepts_CONCEPTID_idx ON GephiConcepts (CONCEPTID);
CREATE INDEX gephirelationships_CONCEPTID1_idx ON GephiRelationships (CONCEPTID1);
CREATE INDEX gephirelationships_CONCEPTID2_idx ON GephiRelationships (CONCEPTID2);
CREATE INDEX gephirelationships_RELATIONSHIPTYPE_idx ON GephiRelationships (RELATIONSHIPTYPE);

