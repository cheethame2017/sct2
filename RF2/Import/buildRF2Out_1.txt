.echo ON
.separator \t
-- ATTACH
ATTACH DATABASE '£££££RF2Snap.db' AS RF2Snap;
-- CREATE, FILL & INDEX RELLITE
CREATE TABLE rellite(sourceId TEXT, destinationId TEXT);
INSERT INTO rellite(sourceId, destinationId) select sourceId, destinationId from relationships where typeId = '116680003' and active = 1;
CREATE INDEX rellite_sourceId_idx ON rellite (sourceId);
CREATE INDEX rellite_destinationId_idx ON rellite (destinationId);
--UPDATE DESCRIPTIONS
UPDATE RF2Snap.descriptions SET term = replace( term, '\"', '"' ) WHERE term LIKE '%\"%';
UPDATE RF2Snap.definitions SET term = replace( term, '\"', '"' ) WHERE term LIKE '%\"%';
-- CREATE, FILL & INDEX TABLE VERSION OF PREFTERM
CREATE TABLE prefterm(conId TEXT, descId TEXT, term TEXT);
INSERT INTO prefterm (conId, descId, term) SELECT conceptId, id, term FROM RF2Snap.prefterm;
CREATE INDEX prefterm_ConceptId_idx ON prefterm (conId);
CREATE INDEX prefterm_DescriptionId_idx ON prefterm (descId);
-- CREATE, FILL & INDEX TABLE VERSION OF RDSROWS
CREATE TABLE rdsrows(id TEXT, acc TEXT);
INSERT INTO rdsrows (id, acc) SELECT id, acc FROM RF2Snap.rdsrows;
CREATE INDEX rdsrows_Id_idx ON rdsrows (id);
CREATE INDEX rdsrows_acc_idx ON rdsrows (acc);
-- CREATE DESCRIPTIONSDX
CREATE TABLE Descriptionsdx(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, unideterm TEXT, caseSignificanceId TEXT);
--QUIT (DOESN'T WORK BUT USEFUL ECHO)
.quit
