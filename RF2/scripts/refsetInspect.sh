#!/bin/bash
# set path
FILEPATH=/home/ed/Desktop/
# unzip packages in FILEPATH location
ZIPFILES=${FILEPATH}*.zip
for f in ${ZIPFILES}
do
    echo
    echo "Unzipping simple refsets from ${f}..."
    unzip -j ${f} \*/Snapshot/Refset/Content/der2_Refset_\*SimpleSnapshot\*.txt -d ${FILEPATH}
done
echo
echo "Extract simplerefset file(s) to refsetId.txt."
# set filename - single full name or wildcard
FILENAMES=der2_Refset_*SimpleSnapshot*.txt
echo
TXTFILES=${FILEPATH}${FILENAMES}
# multiple files
for f in ${TXTFILES}
do
    awk 'FNR == 1 {next} {sub("\r", "", $6); 
        if ($3 == 1)
                print $6 > "/home/ed/Desktop/" $5 ".txt"}' ${f}
done
# avoids overwrite prompts if run again:
echo
for f in ${TXTFILES}
do
    echo $(mv -v ${f} ${f%.txt}.xxx)
    echo
done
