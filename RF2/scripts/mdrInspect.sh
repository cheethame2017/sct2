#!/bin/bash

echo "Extract snapshot MDR files."
# set paths could generalise paths with pwd/cwd setting
MYPATH=/home/ed/D/RF2/
ZIPFILES=${MYPATH}*.zip
for f in ${ZIPFILES}
do
    echo
    echo "Unzipping snapshot MDR from ${f}..."
    unzip -o -j ${f} \*/Snapshot/Refset/Metadata/\*ModuleDependency\*Snapshot\*.txt -d ${MYPATH}
done
echo
TXTFILES=${MYPATH}*.txt
for f in ${TXTFILES}
do
    echo "Processing ${f} snapshot MDR file..."
    awk '{print $3 "\t" $4 "\t" $7 "\t" $6 "\t" $8}' ${f}
    echo
done

echo "Copy this to http://www.webgraphviz.com/"
echo "Remove 'strict' keyword to show redundant rows in non-deduplicated appended files."
echo
echo "strict digraph MDR { rankdir=BT; node [style=filled, fillcolor=lightblue];"
for f in ${TXTFILES}
do
    awk 'FNR == 1 {next} {sub("\r", "", $8); 
        if ($3 == 1)
                print "\"" $4 "_" $7 "\" -> \"" $6 "_" $8 "\";";
        else
                print "\"" $4 "_" $7 "\" -> \"" $6 "_" $8 "\" [color=red];" }' ${f}
done
echo "}"

# avoids interaction with main extraction routine:
echo
for f in ${TXTFILES}
do
    echo $(mv -v ${f} ${f%.txt}.xxx)
    echo
done
