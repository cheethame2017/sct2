#!/bin/bash

# # declare variables
# DATA_PATH=/home/ed/D/

# declare variables
DATA_PATH=${1}

# clean up
rm ${DATA_PATH}unpackAndEscape
rm ${DATA_PATH}indexbuilder.py
rm ${DATA_PATH}buildRF2Out_1.txt
rm ${DATA_PATH}buildRF2Out_2.txt
rm ${DATA_PATH}buildRF2Out_5.txt
rm ${DATA_PATH}buildRF2ModSet.txt
rm ${DATA_PATH}NeoDataPrepScripts_1.txt
rm ${DATA_PATH}NeoDataPrepScripts_2.txt
rm ${DATA_PATH}importSnapshot.txt
rm ${DATA_PATH}testSnapshot.txt
rm ${DATA_PATH}importFull.txt
rm ${DATA_PATH}testFull.txt
if [ -e ${DATA_PATH}RF2Full2.db ]
then
    rm ${DATA_PATH}RF2Full2.db
fi

# zip up the text files for a bit
zip -rm ${DATA_PATH}Out/Out.zip ${DATA_PATH}Out/*.txt