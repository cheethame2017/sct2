#!/bin/bash

# # declare variables - SET THESE (or introduced as arguments at command prompt)
# CODE_PATH=/home/ed/git/rf2repo/sct/RF2/
# DATA_PATH=/home/ed/D/

CODE_PATH=${1}
DATA_BUILD_PATH=${2}
DATA_PATH=${3}

# unzip zipfiles in 
ZIPFILES=${DATA_BUILD_PATH}RF2/*.zip
for f in ${ZIPFILES}
do
    echo
    unzip -o -d ${DATA_BUILD_PATH}RF2/ ${f}
done

# copy files that will be needed
echo
cp ${CODE_PATH}scripts/unpackAndEscape ${DATA_BUILD_PATH}unpackAndEscape
cp ${CODE_PATH}indexbuilder.py ${DATA_BUILD_PATH}indexbuilder.py
cp ${CODE_PATH}Import/buildRF2Out_1.txt ${CODE_PATH}Import/buildRF2Out_1.tmp
sed -i "s|£££££|${DATA_BUILD_PATH}|g" ${CODE_PATH}Import/buildRF2Out_1.tmp
cp ${CODE_PATH}Import/buildRF2Out_1.tmp ${DATA_BUILD_PATH}buildRF2Out_1.txt
rm ${CODE_PATH}Import/buildRF2Out_1.tmp
cp ${CODE_PATH}Import/buildRF2Out_2.txt ${CODE_PATH}Import/buildRF2Out_2.tmp
sed -i "s|£££££|${DATA_BUILD_PATH}|g" ${CODE_PATH}Import/buildRF2Out_2.tmp
cp ${CODE_PATH}Import/buildRF2Out_2.tmp ${DATA_BUILD_PATH}buildRF2Out_2.txt
rm ${CODE_PATH}Import/buildRF2Out_2.tmp
cp ${CODE_PATH}Import/buildRF2Out_5.txt ${DATA_BUILD_PATH}buildRF2Out_5.txt
cp ${CODE_PATH}Import/buildRF2ModSet.txt ${DATA_BUILD_PATH}buildRF2ModSet.txt
cp ${CODE_PATH}Import/read.txt ${DATA_PATH}read.txt
cp ${CODE_PATH}Import/NeoDataPrepScripts_2.txt ${CODE_PATH}Import/NeoDataPrepScripts_2.tmp
sed -i "s|£££££|${DATA_BUILD_PATH}|g" ${CODE_PATH}Import/NeoDataPrepScripts_2.tmp
cp ${CODE_PATH}Import/NeoDataPrepScripts_2.tmp ${DATA_BUILD_PATH}NeoDataPrepScripts_2.txt
rm ${CODE_PATH}Import/NeoDataPrepScripts_2.tmp

# clean out folder and remove old database files
if [ -e ${DATA_BUILD_PATH}Out ]
then
    rm -r ${DATA_BUILD_PATH}Out
fi
mkdir ${DATA_BUILD_PATH}Out
if [ -e ${DATA_BUILD_PATH}RF2Snap.db ]
then
    rm ${DATA_BUILD_PATH}RF2Snap.db
fi
if [ -e ${DATA_BUILD_PATH}RF2Out.db ]
then
    rm ${DATA_BUILD_PATH}RF2Out.db
fi
if [ -e ${DATA_BUILD_PATH}RF2Full.db ]
then
    rm ${DATA_BUILD_PATH}RF2Full.db
fi

# run initial code
cd ${DATA_BUILD_PATH}
${DATA_BUILD_PATH}unpackAndEscape
