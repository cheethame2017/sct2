from __future__ import unicode_literals
import sqlite3
import io
from SCTRF2config import pth, cfg
from SCTRF2Helpers import descendantCountFromConId_IN, descendantsCleanIDFromConId_IN, refSetMemberCountFromConId_IN, refSetTypeFromConId_IN,\
                        refSetMembersFromConId_IN
from platformSpecific import cfgPathInstall, sep
# CONC HEADER
from SCTRF2Concrete import concreteTakingRoles

snapPath = pth('RF2_SNAP')
outPath = pth('RF2_OUT')

def refSetTypeFromConId(setId):
    if setId[0:4] == "SET_":
        return [["SET"]]
    else:
        return refSetTypeFromConId_IN(setId)

def refSetMemberCountFromConId(setId, refsetType):
    if setId[0:4] == "SET_":
        # code for count of sets
        myList = []
        myList = readFileMembers(cfgPathInstall() + 'sets' + sep() + setId[4] + '.txt')
        return len(myList)
    else:
        return refSetMemberCountFromConId_IN(setId, refsetType)

def refSetMembersFromConId(setId, refsetType, id1, id2):
    if setId[0:4] == "SET_":
        # code for set members
        myList = []
        myList = [[mem] for mem in readFileMembers(cfgPathInstall() + 'sets' + sep() + setId[4] + '.txt')]
        return myList
    else:
        return refSetMembersFromConId_IN(setId, refsetType, id1, id2)

def readFileMembers(inPath): # reads back members of a file
    memberList = []
    f = io.open(inPath, 'r', encoding="UTF-8")
    for line in f:
        memberList.append(line.strip())
    f.close()
    return memberList

def orderAndCountRoleRows_IN(inRowList): # inrowList only has AND logic
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    orderedList = []
    doneConstraintList = []
    for row in inRowList:
        if row[4] not in doneConstraintList:
            a = 0
            if row[2] == "FOCUS": # deal with focus row
                if row[4][0] == "<<":
                    a = descendantCountFromConId_IN(row[4][1])
                elif row[4][0] == "<":
                    a = descendantCountFromConId_IN(row[4][1]) - 1
                elif row[4][0] == "":
                    a = 1
                elif row[4][0] == "^":
                    myRefset = refSetTypeFromConId(row[4][1])
                    for refset in myRefset:
                        refsetType = refset[0]
                    a = refSetMemberCountFromConId(row[4][1], refsetType)
            else:
                # strictly should be 'select count(distinct r.sourceId)' but no real benefit for speed penalty
                # CONC - new methods for initial testing/ranking of concrete rows
                if (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1) and row[4][1] in concreteTakingRoles():
                    if concreteTakingRoles()[row[4][1]] == 0:
                        mstring = concValues(row, row[4][4].replace("#", ""))
                    else:
                        mstring = "\"" + row[4][4] + "\""
                    if row[4][0] == "<<" and (row[4][3] == ">" or row[4][3] == "<"):
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (" + mstring + ") and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and r.active=1", (row[4][1], ))
                    elif row[4][0] == "<<" and row[4][3] == "":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId = ? and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and r.active=1", (mstring, row[4][1]))
                    elif row[4][0] == "<" and (row[4][3] == ">" or row[4][3] == "<"):
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (" + mstring + ") and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and (not r.typeId = ?) \
                                            and r.active=1", (row[4][1], row[4][1]))
                    elif row[4][0] == "<" and row[4][3] == "":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId = ? and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and (not r.typeId = ?) \
                                            and r.active=1", (mstring, row[4][1], row[4][1]))
                    elif row[4][0] == "" and (row[4][3] == "<" or row[4][3] == ">"):
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (" + mstring + ") \
                                            and r.typeId = ? \
                                            and r.active=1", (row[4][1], ))
                    elif row[4][0] == "" and row[4][3] == "":
                        cursorIn2.execute("select count(typeId) from relationships r where \
                                            active=1 and destinationId = ? and typeId=?", (mstring, row[4][1]))
                else:
                    if row[4][0] == "<<" and row[4][3] == "<<":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and r.active=1", (row[4][4], row[4][1]))
                    elif row[4][0] == "<<" and row[4][3] == "<":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) \
                                            and (not r.destinationId = ?) and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and r.active=1", (row[4][4], row[4][4], row[4][1]))
                    elif row[4][0] == "<<" and row[4][3] == "":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId = ? and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and r.active=1", (row[4][4], row[4][1]))
                    elif row[4][0] == "<" and row[4][3] == "<<":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and (not r.typeId = ?) \
                                            and r.active=1", (row[4][4], row[4][1], row[4][1]))
                    elif row[4][0] == "<" and row[4][3] == "<":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                            and (not r.destinationId = ?) and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and (not r.typeId = ?) \
                                            and r.active=1", (row[4][4], row[4][4], row[4][1], row[4][1]))
                    elif row[4][0] == "<" and row[4][3] == "":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId = ? and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and (not r.typeId = ?) \
                                            and r.active=1", (row[4][4], row[4][1], row[4][1]))
                    elif row[4][0] == "" and row[4][3] == "<<":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                            r.typeId = ? \
                                            and r.active=1", (row[4][4], row[4][1]))
                    elif row[4][0] == "" and row[4][3] == "<":
                        cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) \
                                            and (not r.destinationId = ?) and \
                                            r.typeId = ? \
                                            and r.active=1", (row[4][4], row[4][4], row[4][1]))
                    elif row[4][0] == "" and row[4][3] == "":
                        cursorIn2.execute("select count(typeId) from relationships r where \
                                            active=1 and destinationId = ? and typeId=?", (row[4][4], row[4][1]))
                    # refsets
                    elif row[4][3] == "^":
                        myRefset = refSetTypeFromConId(row[4][4])
                        for refset in myRefset:
                            refsetType = refset[0]
                        tempList = [item[0] for item in refSetMembersFromConId(row[4][4], refsetType, 0, 0)]
                        RSString = listToInString(tempList)
                        if row[4][0] == "<<":
                            cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (" + RSString + ") and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and r.active=1", (row[4][1],))
                        elif row[4][0] == "<":
                            cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (" + RSString + ") and \
                                            r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                            and (not r.typeId = ?) \
                                            and r.active=1", (row[4][1], row[4][1]))
                        elif row[4][0] == "":
                            cursorIn2.execute("select count(r.sourceId) from relationships r where \
                                            r.destinationId in (" + RSString + ") and \
                                            r.typeId = ? \
                                            and r.active=1", (row[4][1],))
                recordsetIn2 = cursorIn2.fetchall()
                a = recordsetIn2[0][0]
            doneConstraintList.append(row[4])
            for item in inRowList:
                if item[4] == row[4]:
                    orderedList.append([a, item])
    cursorIn2.close()
    connectionIn2.close()
    return sorted(orderedList, key=lambda x: x[0])

def starterObjectSet_IN(row): # method only has AND logic
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    returnList = []
    if row[2] == "FOCUS": # deal with focus row
        if row[4][0] == "<<":
            tempList = descendantsCleanIDFromConId_IN(row[4][1])
            returnList = tempList
        elif row[4][0] == "<":
            tempList = descendantsCleanIDFromConId_IN(row[4][1])
            tempList.remove(row[4][1])
            returnList = tempList
        elif row[4][0] == "":
            returnList.append(row[4][1])
        elif row[4][0] == "^":
            myRefset = refSetTypeFromConId(row[4][1])
            for refset in myRefset:
                refsetType = refset[0]
            tempList = [item[0] for item in refSetMembersFromConId(row[4][1], refsetType, 0, 0)]
            returnList = tempList
    else:
        # CONC - concrete methods for returning initial candidate set for each query row
        if (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1) and row[4][1] in concreteTakingRoles():
            if concreteTakingRoles()[row[4][1]] == 0:
                mstring = concValues(row, row[4][4].replace("#", ""))
            else:
                mstring = row[4][4]
            if row[4][0] == "<<" and (row[4][3] == ">" or row[4][3] == "<"):
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (" + mstring + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][1], ))
            elif row[4][0] == "<<" and row[4][3] == "":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (mstring, row[4][1]))
            elif row[4][0] == "<" and (row[4][3] == ">" or row[4][3] == "<"):
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (" + mstring + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][1], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (mstring, row[4][1], row[4][1]))
            elif row[4][0] == "" and (row[4][3] == "<" or row[4][3] == ">"):
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (" + mstring + ") \
                                    and r.typeId = ? \
                                    and r.active=1", (row[4][1], ))
            elif row[4][0] == "" and row[4][3] == "":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    active=1 and destinationId = ? and typeId=?", (mstring, row[4][1]))
        else:
            if row[4][0] == "<<" and row[4][3] == "<<":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][4], row[4][1]))
            elif row[4][0] == "<<" and row[4][3] == "<":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) \
                                    and (not r.destinationId = ?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][4], row[4][4], row[4][1]))
            elif row[4][0] == "<<" and row[4][3] == "":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][4], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "<<":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][4], row[4][1], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "<":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    and (not r.destinationId = ?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][4], row[4][4], row[4][1], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][4], row[4][1], row[4][1]))
            elif row[4][0] == "" and row[4][3] == "<<":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    r.typeId = ? \
                                    and r.active=1", (row[4][4], row[4][1]))
            elif row[4][0] == "" and row[4][3] == "<":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) \
                                    and (not r.destinationId = ?) and \
                                    r.typeId = ? \
                                    and r.active=1", (row[4][4], row[4][4], row[4][1]))
            elif row[4][0] == "" and row[4][3] == "":
                cursorIn2.execute("select r.sourceId from relationships r where \
                                    active=1 and destinationId = ? and typeId=?", (row[4][4], row[4][1]))
            # refsets
            elif row[4][3] == "^":
                myRefset = refSetTypeFromConId(row[4][4])
                for refset in myRefset:
                    refsetType = refset[0]
                tempList = [item[0] for item in refSetMembersFromConId(row[4][4], refsetType, 0, 0)]
                RSString = listToInString(tempList)
                if row[4][0] == "<<":
                    cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (" + RSString + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][1],))
                elif row[4][0] == "<":
                    cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (" + RSString + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][1], row[4][1]))
                elif row[4][0] == "":
                    cursorIn2.execute("select r.sourceId from relationships r where \
                                    r.destinationId in (" + RSString + ") and \
                                    r.typeId = ? \
                                    and r.active=1", (row[4][1],))
        recordsetIn2 = cursorIn2.fetchall()
        returnList = list(set([item[0] for item in recordsetIn2]))
    cursorIn2.close()
    connectionIn2.close()
    return returnList

def checkEachRow_IN(row, inList, inString, onlySourceId): # method only has AND logic
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    if onlySourceId:
        returnString = 'r.sourceId'
    else:
        returnString = 'r.sourceId, r.typeId'
    returnList = []
    tempList = []
    if row[2] == "FOCUS": # deal with focus row
        if row[4][0] == "<<":
            tempList = descendantsCleanIDFromConId_IN(row[4][1])
            returnList = list(set(tempList) & set(inList))
        elif row[4][0] == "<":
            tempList = descendantsCleanIDFromConId_IN(row[4][1])
            tempList.remove(row[4][1])
            returnList = list(set(tempList) & set(inList))
        elif row[4][0] == "":
            returnList = list(set([row[4][1]]) & set(inList))
        elif row[4][0] == "^":
            myRefset = refSetTypeFromConId(row[4][1])
            for refset in myRefset:
                refsetType = refset[0]
            tempList = [item[0] for item in refSetMembersFromConId(row[4][1], refsetType, 0, 0)]
            returnList = list(set(tempList) & set(inList))
    else:
        # CONC - concrete methods for checking between query rows
        if (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1) and row[4][1] in concreteTakingRoles():
            if concreteTakingRoles()[row[4][1]] == 0:
                mstring = concValues(row, row[4][4].replace("#", ""))
            else:
                mstring = row[4][4]
            if row[4][0] == "<<" and (row[4][3] == ">" or row[4][3] == "<"):
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (" + mstring + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][1], ))
            elif row[4][0] == "<<" and row[4][3] == "":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (mstring, row[4][1]))
            elif row[4][0] == "<" and (row[4][3] == ">" or row[4][3] == "<"):
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (" + mstring + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][1], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (mstring, row[4][1], row[4][1]))
            elif row[4][0] == "" and (row[4][3] == "<" or row[4][3] == ">"):
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (" + mstring + ") \
                                    and r.typeId = ? \
                                    and r.active=1", (row[4][1], ))
            elif row[4][0] == "" and row[4][3] == "":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    active=1 and destinationId = ? and typeId=?", (mstring, row[4][1]))
        else:
            if row[4][0] == "<<" and row[4][3] == "<<":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][4], row[4][1]))
            elif row[4][0] == "<<" and row[4][3] == "<":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) \
                                    and (not r.destinationId = ?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][4], row[4][4], row[4][1]))
            elif row[4][0] == "<<" and row[4][3] == "":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][4], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "<<":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][4], row[4][1], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "<":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    and (not r.destinationId = ?) and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][4], row[4][4], row[4][1], row[4][1]))
            elif row[4][0] == "<" and row[4][3] == "":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId = ? and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][4], row[4][1], row[4][1]))
            elif row[4][0] == "" and row[4][3] == "<<":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) and \
                                    r.typeId = ? \
                                    and r.active=1", (row[4][4], row[4][1]))
            elif row[4][0] == "" and row[4][3] == "<":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    r.destinationId in (select t1.subtypeId from RF2Out.tc t1 where t1.supertypeId=?) \
                                    and (not r.destinationId = ?) and \
                                    r.typeId = ? \
                                    and r.active=1", (row[4][4], row[4][4], row[4][1]))
            elif row[4][0] == "" and row[4][3] == "":
                cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.sourceId in (" + inString + ") and \
                                    active=1 and destinationId = ? and typeId=?", (row[4][4], row[4][1]))
            # refsets
            elif row[4][3] == "^":
                myRefset = refSetTypeFromConId(row[4][4])
                for refset in myRefset:
                    refsetType = refset[0]
                tempList = [item[0] for item in refSetMembersFromConId(row[4][4], refsetType, 0, 0)]
                RSString = listToInString(tempList)
                if row[4][0] == "<<":
                    cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.destinationId in (" + RSString + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and r.active=1", (row[4][1],))
                elif row[4][0] == "<":
                    cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.destinationId in (" + RSString + ") and \
                                    r.typeId in (select t2.subtypeId from RF2Out.tc t2 where t2.supertypeId=?) \
                                    and (not r.typeId = ?) \
                                    and r.active=1", (row[4][1], row[4][1]))
                elif row[4][0] == "":
                    cursorIn2.execute("select " + returnString + " from relationships r where \
                                    r.destinationId in (" + RSString + ") and \
                                    r.typeId = ? \
                                    and r.active=1", (row[4][1],))
        recordsetIn2 = cursorIn2.fetchall()
        if onlySourceId:
            returnList = list(set([item[0] for item in recordsetIn2]) & set(inList))
        else:
            returnList = list(set([item[1] for item in recordsetIn2]))
    cursorIn2.close()
    connectionIn2.close()
    return returnList

def listToInString(inList): # generates comma-separated string of identifiers for subsequent SQL 'IN' function
    outString = ""
    for item in inList:
        outString += "'" + item + "', "
    return outString[:-2]

# CONC - data lookup (converts numbers to rel table equivalents): conc1 returns ids (i.e. spoof), conc2 returns #numbers (probable longer term solution)
def concValues(row, mstring):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    valueList = []
    # CONC - check which table to look up on (ids or values)
    if cfg("CONC_DATA") == 1:
        concTable = "conc2"
    else:
        concTable = "conc1"
    # note conc1 returns ids (i.e. spoof), conc2 returns #numbers (probable longer term solution)
    if row[4][3] == "<":
        cursorIn2.execute("select id from " + concTable + " where num <= ?", (mstring, ))
        valueList = cursorIn2.fetchall()
        mstring = ', '.join(map(str, [value[0] for value in valueList]))
        # CONC - if concrete values, return with quote delimiters around list members
        if cfg("CONC_DATA") == 1:
            mstring = "\'" + '\', \''.join(map(str, [value[0] for value in valueList])) + "\'"
        else:
            mstring = ', '.join(map(str, [value[0] for value in valueList]))
    elif row[4][3] == ">":
        cursorIn2.execute("select id from " + concTable + " where num >= ?", (mstring, ))
        valueList = cursorIn2.fetchall()
        # CONC - if concrete values, return with quote delimiters around list members
        if cfg("CONC_DATA") == 1:
            mstring = "\'" + '\', \''.join(map(str, [value[0] for value in valueList])) + "\'"
        else:
            mstring = ', '.join(map(str, [value[0] for value in valueList]))
    else:
        cursorIn2.execute("select id from " + concTable + " where num = ?", (mstring, ))
        valueList = cursorIn2.fetchall()
        # CONC - if concrete value, return with quote delimiters
        if cfg("CONC_DATA") == 1:
            mstring = "\'" + valueList[0][0] + "\'"
        else:
            mstring = valueList[0][0]
        mstring = valueList[0][0]
    return mstring
    cursorIn2.close()
    connectionIn2.close()
