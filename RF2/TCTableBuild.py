from __future__ import print_function, unicode_literals
import sqlite3
import os
import time

def deleteDbFile(inPath):
    if os.path.isfile(inPath):
        os.remove(inPath)
    else:
        print("Note: %s not found so not deleted" % inPath)


def createTCByTablesController(db1Path, db2Path, RF2Snappath, RF2Outpath, RF2Out2path, TCtarget):
    # tidy temporary files
    deleteDbFile(db1Path)
    deleteDbFile(db2Path)
    deleteDbFile(RF2Out2path)

    # create first db1
    print('Initialisation step (rellite->DB1 and new rows from first join)')
    print('->DB1', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    conn = sqlite3.connect(db1Path)
    cur = conn.cursor()
    cur.execute("CREATE TABLE localTC (subtypeId TEXT NOT NULL, supertypeId TEXT NOT NULL, PRIMARY KEY(subtypeId,supertypeId))")
    cur.execute("CREATE INDEX localTC_supertypeId_idx ON localTC (supertypeId);")
    cur.execute("ATTACH DATABASE '" + RF2Outpath + "' AS RF2Out")
    # set tableLength1
    tableLength1 = cur.execute("SELECT count(sourceId) FROM relLite;").fetchone()[0]
    # original rows
    print('->Original rows from rellite', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("INSERT INTO localTC SELECT DISTINCT * FROM RF2Out.relLite;")
    # first set of new rows
    print('->New rows', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("REPLACE INTO localTC SELECT DISTINCT r1.sourceId, r2.destinationId FROM RF2Out.relLite as r1, RF2Out.relLite as r2 WHERE r1.destinationId=r2.sourceId")
    conn.commit()
    # set tableLength2
    tableLength2 = cur.execute("SELECT count(subtypeId) FROM localTC;").fetchone()[0]
    cur.close()
    conn.close

    # main loop
    print('Main loop')
    while not (tableLength1 == tableLength2):
        print('Original:', tableLength1, 'New size:', tableLength2, 'Diff:', tableLength2-tableLength1)
        # open db2, attach db1
        print('->DB2', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        conn = sqlite3.connect(db2Path)
        cur = conn.cursor()
        cur.execute("CREATE TABLE localTC (subtypeId TEXT NOT NULL, supertypeId TEXT NOT NULL, PRIMARY KEY(subtypeId,supertypeId))")
        cur.execute("CREATE INDEX localTC_supertypeId_idx ON localTC (supertypeId);")
        cur.execute("ATTACH DATABASE '" + db1Path + "' AS db1")
        # original rows
        print('->Original rows', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        cur.execute("INSERT INTO localTC SELECT DISTINCT * FROM db1.localTC;")
        # add new rows
        print('->New rows', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        cur.execute("REPLACE INTO localTC SELECT DISTINCT t1.subtypeId, t2.supertypeId FROM db1.localTC as t1, db1.localTC as t2 WHERE t1.supertypeId=t2.subtypeId")
        conn.commit()
        # set tableLength1
        print('->SizeDB2', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        tableLength1 = cur.execute("SELECT count(supertypeId) FROM localTC;").fetchone()[0]
        cur.close()
        conn.close
        # delete the old db1
        deleteDbFile(db1Path)
        # now back the other way - create a new db1
        # open db1, attach db2
        print('->DB1', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        conn = sqlite3.connect(db1Path)
        cur = conn.cursor()
        cur.execute("CREATE TABLE localTC (subtypeId TEXT NOT NULL, supertypeId TEXT NOT NULL, PRIMARY KEY(subtypeId,supertypeId))")
        cur.execute("CREATE INDEX localTC_supertypeId_idx ON localTC (supertypeId);")
        cur.execute("ATTACH DATABASE '" + db2Path + "' AS db2")
        # original rows
        print('->Original rows', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        cur.execute("INSERT INTO localTC SELECT DISTINCT * FROM db2.localTC;")
        # add new rows
        print('->New rows', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        cur.execute("REPLACE INTO localTC SELECT DISTINCT t1.subtypeId, t2.supertypeId FROM db2.localTC as t1, db2.localTC as t2 WHERE t1.supertypeId=t2.subtypeId")
        conn.commit()
        # set tableLength2
        print('->SizeDB1', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        tableLength2 = cur.execute("SELECT count(supertypeId) FROM localTC;").fetchone()[0]
        cur.close()
        conn.close
        # delete the old db2
        deleteDbFile(db2Path)
        # if tableLength1 == tableLength2 then leave this loop

    print('Original:', tableLength1, 'New size:', tableLength2, 'Diff:', tableLength2-tableLength1)
    print('Closing step (after Diff=0)')
    print('->TC!', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    conn = sqlite3.connect(db1Path)
    cur = conn.cursor()
    if TCtarget == 1:
        cur.execute("ATTACH DATABASE '" + RF2Outpath + "' AS RF2Out")
        cur.execute("ATTACH DATABASE '" + RF2Snappath + "' AS RF2Snap")
    else:
        cur.execute("ATTACH DATABASE '" + RF2Out2path + "' AS RF2Out")
        cur.execute("ATTACH DATABASE '" + RF2Snappath + "' AS RF2Snap")
        cur.execute("CREATE TABLE RF2Out.TC (subtypeId TEXT, supertypeId TEXT)")
    # everything into official TC
    print('->All rows from localTC', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("INSERT INTO RF2Out.TC SELECT DISTINCT * FROM localTC;")
    print('->Self rows from concepts', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("INSERT INTO RF2Out.TC SELECT c.id, c.id FROM RF2Snap.Concepts as c where c.active=1;")
    print('->Index1', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("CREATE INDEX RF2Out.Tc_SubtypeId_idx ON TC (subtypeId);")
    print('->Index2', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("CREATE INDEX RF2Out.Tc_SupertypeId_idx ON TC (supertypeId);")
    conn.commit()
    # close this database
    cur.close()
    conn.close

    print('Adding subtype count table')
    print('->TC subs', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    conn = sqlite3.connect(RF2Outpath)
    cur = conn.cursor()
    cur2 = conn.cursor()
    tempList = []
    cur.execute("ATTACH DATABASE '" + RF2Snappath + "' AS RF2Snap")
    cur.execute("CREATE TABLE TCSubCount (SuperTypeId TEXT, STCount INTEGER)")
    conn.commit()
    cur2.execute('SELECT RF2Snap.Concepts.id FROM RF2Snap.Concepts WHERE RF2Snap.Concepts.active=1;')
    recordsetIn2 = cur2.fetchall()
    progress = 1
    tot = len(recordsetIn2)
    for row in recordsetIn2:
        tempList = [row[0], cur.execute("select count(subtypeId) from TC where supertypeId = ?", row).fetchone()[0]]
        cur.execute("INSERT INTO TCSubCount VALUES (?, ?)", tempList)
        if (progress % 500 == 0):
            print(progress, "/", tot)
            conn.commit()
        progress += 1
    # cleanup commit
    conn.commit()

    # index TCSubCount
    print('->Index', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("CREATE INDEX TCSubCount_SupertypeId_idx ON TCSubCount (supertypeId);")
    conn.commit()
    # close this database
    cur.close()
    cur2.close()
    conn.close

    # delete the old db1
    deleteDbFile(db1Path)
    print('->End', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))

if __name__ == '__main__':
    # defaults if run directly
    db1Path = '/home/ed/D/db1.db'
    db2Path = '/home/ed/D/db2.db'
    RF2Snappath = '/home/ed/D/RF2Snap.db'
    RF2Outpath = '/home/ed/D/RF2Out.db'
    RF2Out2path = '/home/ed/D/RF2Out2.db'

    TCtarget = 1 # set as 1 for RF2Out.db, 2 for RF2Out2.db
    createTCByTablesController(db1Path, db2Path, RF2Snappath, RF2Outpath, RF2Out2path, TCtarget)
