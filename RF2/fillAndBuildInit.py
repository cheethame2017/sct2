import subprocess
from SCTRF2config import pth
from platformSpecific import cfgPathInstall

dataBuildPath = pth('BASIC_DATA_BUILD') # location of data files during construction
dataPath = pth('BASIC_DATA') # location of data files for use
codePath = cfgPathInstall()

# run importAndBuildPrelims.sh
subprocess.call([codePath + "scripts/importAndBuildPrelims.sh", codePath, dataBuildPath, dataPath])
