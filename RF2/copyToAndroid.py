from __future__ import print_function, unicode_literals
import os
import io
from shutil import copyfile
from platformSpecific import cfgPathInstall, recursive_overwrite, sep


# copies reduced set of files/folders to 'android/RF2' folder to copy to android-based phone. Runs inside qpython 3L app.

def copyFolderToAndroidFolder(inFolderName):
    if os.path.isfile(cfgPathInstall() + "android" + sep() + 'RF2' + sep() + inFolderName + sep() + "marker.txt"):
        recursive_overwrite(cfgPathInstall() + inFolderName, cfgPathInstall() + "android" + sep() + 'RF2' + sep() + inFolderName)

def copyFileToAndroidFolder(inFileName):
    if os.path.isfile(cfgPathInstall()  + "android" + sep() + 'RF2' + sep() + inFileName):
        copyfile(cfgPathInstall() + inFileName, cfgPathInstall() + "android" + sep() + 'RF2' + sep() + inFileName)

folderList = ['files', 'help', 'sets']
fileList = ['platformSpecific.py',
            'SCTRF2.py',
            'SCTRF2config.py',
            'SCTRF2Funcs.py',
            'SCTRF2Helpers.py',
            'SCTRF2R.py',
            'SCTRF2Refset.py',
            'SCTRF2Role.py',
            'SCTRF2RoleHelpers.py',
            'SCTRF2Concrete.py',
            'SCTRF2Subgraph.py',
            'SCTRF2Help.py']

def updateSCT():
    helpersOutString = ""
    funcsOutString = ""
    f = io.open(cfgPathInstall() + sep() + 'SCTRF2Helpers.py', 'r', encoding="UTF-8")
    for line in f:
        if line[0:4] == "def ":
            tempLine = line.strip()
            helpersOutString += tempLine[0:4] + tempLine[4:tempLine.find("):") + 2] + "\n"
            helpersOutString += "    " + "return h." + tempLine[4:tempLine.find("):") + 1] + "\n\n"
    f.close()
    f = io.open(cfgPathInstall() + sep() + 'SCTRF2Funcs.py', 'r', encoding="UTF-8")
    for line in f:
        if (line[0:4] == "def ") and ("# *SCT*" in line):
            tempLine = line.strip()
            funcsOutString += tempLine[0:4] + tempLine[4:tempLine.find("):") + 2] + "\n"
            funcsOutString += "    " + "return f." + tempLine[4:tempLine.find("):") + 1] + "\n\n"
    f.close()
    headerStringLin = "import sys\nimport re\nsys.path.append('/home/ed/Code/Repos/sct/RF2/')\n\nimport SCTRF2Helpers as h\nimport SCTRF2Funcs as f\n\n"
    headerStringWin = "import sys\nimport re\nsys.path.append('C:\\\\Users\\\\echee\\\\OneDrive\\\\Documents\\\\Repos\\\\sct\\\\RF2\\\\')\n\nimport SCTRF2Helpers as h\nimport SCTRF2Funcs as f\n\n"
    f = io.open(cfgPathInstall() + sep() + 'files' + sep() + 'lin' + sep() + 'SCT.py', 'w', encoding="UTF-8")
    f.write(headerStringLin)
    f.write(helpersOutString)
    f.write(funcsOutString[:-1])
    f.close()
    f = io.open(cfgPathInstall() + sep() + 'files' + sep() + 'win' + sep() + 'SCT.py', 'w', encoding="UTF-8")
    f.write(headerStringWin)
    f.write(helpersOutString)
    f.write(funcsOutString[:-1])
    f.close()

# lazy update of SCT files with method signatures in SCTRF2Helpers.py
updateSCT()

for folder in folderList:
    copyFolderToAndroidFolder(folder)

for fileName in fileList:
    copyFileToAndroidFolder(fileName)
