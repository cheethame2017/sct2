#!/usr/bin/python
from __future__ import print_function, unicode_literals
import sys
import io
from platformSpecific import cfgPathInstall
from SCTRF2config import pth

def pthWrite(inPath, inString, inValue):
    writeToList = []
    try:
        f = io.open(inPath + 'SCTRF2datapaths.cfg', 'r+', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            tempLine = line.strip()
            if not line[0] == "#":
                tempList = tempLine.split('|')
                if tempList[0] == inString:
                    writeToList.append(tempList[0] + '|' + str(inValue))
                else:
                    writeToList.append(tempLine)
            else:
                writeToList.append(tempLine)
        #print writeToList
        f.seek(0)
        for entry in writeToList:
            f.write(entry + '\n')
    finally:
        f.close()

if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
    platform = 'files/lin/'
    sep = '/'
elif sys.platform == "win32":
    platform = 'files\\win\\'
    sep = '\\'

# input various paths
if (sys.version_info > (3, 0)):
    # Python 3 code:
    installationPath = cfgPathInstall()
    currentDataPath = pth('BASIC_DATA')
    tempDataPath = input('Input basic data path.\n' +
                        'Currently ' + currentDataPath + '\n' +
                        'To keep this enter #, or enter new path\n' +
                        '(Trailing \ for windows or / for linux): ').strip()
    if tempDataPath == '#':
        dataPath = currentDataPath
    else:
        dataPath = tempDataPath
    currentDataBuildPath = pth('BASIC_DATA_BUILD')
    tempDataBuildPath = input('\nInput data build path.\n' +
                        'Currently ' + currentDataBuildPath + '\n' +
                        'To keep this enter #, or enter new path\n' +
                        '(Trailing \ for windows or / for linux): ').strip()
    if tempDataBuildPath == '#':
        dataBuildPath = currentDataBuildPath
    else:
        dataBuildPath = tempDataBuildPath
    currentNeoPath = pth('NEO4J')
    tempNeoPath = input('\nInput Neo4j path.\n' +
                        'Currently ' + currentNeoPath + '\n' +
                        'If only doing a snapshot build or \n' +
                        'To keep this enter #. Otherwise enter a new path\n' +
                        '(trailing \ for windows, NO / for linux): ').strip()
    if tempNeoPath == '#':
        neoPath = currentNeoPath
    else:
        neoPath = tempNeoPath

    pthWrite(installationPath + platform, 'BASIC_CODE', installationPath)
    pthWrite(installationPath + platform, 'BASIC_DATA', dataPath)
    pthWrite(installationPath + platform, 'BASIC_DATA_BUILD', dataBuildPath)
    pthWrite(installationPath + platform, 'RF2_SNAP', dataPath + 'RF2Snap.db')
    pthWrite(installationPath + platform, 'RF2_OUT', dataPath + 'RF2Out.db')
    pthWrite(installationPath + platform, 'RF2_CONC', dataPath + 'RF2Conc.db')
    pthWrite(installationPath + platform, 'RF2_FULL', dataPath + 'RF2Full.db')
    pthWrite(installationPath + platform, 'RF2_UKOUT', dataPath + 'RF2UKOut.db')
    pthWrite(installationPath + platform, 'MODSET', dataPath + 'RF2ModSet.db')
    pthWrite(installationPath + platform, 'UK_BACKUP', dataPath + 'tidy_data' + sep + 'UK' + sep)
    pthWrite(installationPath + platform, 'INT_BACKUP', dataPath +'tidy_data' + sep + 'INT' + sep)
    pthWrite(installationPath + platform, 'TEMP_BACKUP', dataPath + 'tidy_data' + sep + 'TEMP' + sep)
    pthWrite(installationPath + platform, 'NEO4J', neoPath)
else:
    # Python 2 code:
    installationPath = cfgPathInstall()
    currentDataPath = pth('BASIC_DATA')
    tempDataPath = raw_input('Input basic data path.\n' +
                        'Currently ' + currentDataPath + '\n' +
                        'To keep this enter #, or enter new path\n' +
                        '(Trailing \ for windows or / for linux): ').decode('UTF-8').strip()
    if tempDataPath == '#':
        dataPath = currentDataPath
    else:
        dataPath = tempDataPath
    currentDataBuildPath = pth('BASIC_DATA_BUILD')
    tempDataBuildPath = raw_input('\nInput data build path.\n' +
                        'Currently ' + currentDataBuildPath + '\n' +
                        'To keep this enter #, or enter new path\n' +
                        '(Trailing \ for windows or / for linux): ').decode('UTF-8').strip()
    if tempDataBuildPath == '#':
        dataBuildPath = currentDataBuildPath
    else:
        dataBuildPath = tempDataBuildPath
    currentNeoPath = pth('NEO4J')
    tempNeoPath = raw_input('\nInput Neo4j path.\n' +
                        'Currently ' + currentNeoPath + '\n' +
                        'If only doing a snapshot build or \n' +
                        'To keep this enter #. Otherwise enter a new path\n' +
                        '(trailing \ for windows, NO / for linux): ').decode('UTF-8').strip()
    if tempNeoPath == '#':
        neoPath = currentNeoPath
    else:
        neoPath = tempNeoPath

    pthWrite(installationPath + platform, 'BASIC_CODE', installationPath)
    pthWrite(installationPath + platform, 'BASIC_DATA', dataPath)
    pthWrite(installationPath + platform, 'BASIC_DATA_BUILD', dataBuildPath)
    pthWrite(installationPath + platform, 'RF2_SNAP', dataPath + 'RF2Snap.db')
    pthWrite(installationPath + platform, 'RF2_OUT', dataPath + 'RF2Out.db')
    pthWrite(installationPath + platform, 'RF2_CONC', dataPath + 'RF2Conc.db')
    pthWrite(installationPath + platform, 'RF2_FULL', dataPath + 'RF2Full.db')
    pthWrite(installationPath + platform, 'RF2_UKOUT', dataPath + 'RF2UKOut.db')
    pthWrite(installationPath + platform, 'MODSET', dataPath + 'RF2ModSet.db')
    pthWrite(installationPath + platform, 'UK_BACKUP', dataPath + 'tidy_data' + sep + 'UK' + sep)
    pthWrite(installationPath + platform, 'INT_BACKUP', dataPath +'tidy_data' + sep + 'INT' + sep)
    pthWrite(installationPath + platform, 'TEMP_BACKUP', dataPath + 'tidy_data' + sep + 'TEMP' + sep)
    pthWrite(installationPath + platform, 'NEO4J', neoPath)


# windows examples:

# C:\Users\Ed\Documents\repo\sct>C:/Users/Ed/AppData/Local/Programs/Python/Python37/python3.exe c:/Users/Ed/Documents/repo/sct/RF2/initialFix.py
# codePromptString (trailing \ for windows or / for linux): C:\Users\Ed\Documents\repo\sct\RF2\
# dataPromptString (trailing \ for windows or / for linux): E:\RF2\
# neoPromptString (trailing \ for windows, NO / for linux): C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin\

# linux/android examples:

# Neo4j path
# NEO4J|/home/ed/N4J/neo4j-community-4.0.3
# Basic installation/code path
# BASIC_CODE|/storage/emulated/0/qpython/scripts3/RF2/
# Basic data path
# BASIC_DATA|/storage/emulated/0/RF2/Data/
