from __future__ import unicode_literals
import io
import os

def NeoImport1Prep(path):
    w = "Full"

    importMainItems = []

    trimpath = path[:-1]

    files = os.listdir(trimpath)

    basicFields = 'id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, '

    text_file = io.open(path[:-4] + "NeoDataPrepScripts_1.txt", "w", encoding="UTF-8")

    text_file.write('.echo ON')
    text_file.write("\n")
    text_file.write('.separator \\t')
    text_file.write("\n")

    text_file.write('--CREATE TABLE INSTRUCTIONS')
    text_file.write("\n")
    text_file.write('CREATE TABLE Relationships(' + basicFields + 'sourceId TEXT, destinationId TEXT, relationshipGroup INTEGER, typeId TEXT, characteristicTypeId TEXT, modifierId TEXT);')
    text_file.write("\n")
    text_file.write("CREATE TABLE tmpsupersede (id TEXT,effectiveTime TEXT,supersededTime TEXT);")
    text_file.write("\n")

    for fileName in files:
        if ('_Relationship_' in fileName) and (w + '_' in fileName):
            print('Relationships: ', fileName)
            importMainItems.append('.import ' + path + fileName + ' Relationships' + '\n')

    text_file.write('--MAIN IMPORTS')
    text_file.write("\n")

    for importItem in importMainItems:
        text_file.write(str(importItem))

    #basic index additions
    text_file.write('--RELATIONSHIPS INDEX ADDITION')
    text_file.write("\n")
    text_file.write('CREATE INDEX relationships_idet_idx ON relationships (id, effectiveTime);')
    text_file.write("\n")
    text_file.write('--REMOVE HEADERS')
    text_file.write("\n")
    text_file.write('DELETE FROM Relationships WHERE id == \'id\';')
    text_file.write("\n")
    text_file.write('--TMPSUPERSEDE INSERT QUERY')
    text_file.write("\n")
    text_file.write("INSERT INTO tmpsupersede SELECT r.id, r.effectiveTime, (SELECT IFNULL(MIN(r2.effectiveTime),'99991231') FROM relationships AS r2 WHERE r.id = r2.id AND r.effectiveTime < r2.effectiveTime) AS supersededTime FROM relationships AS r;")
    text_file.write("\n")
    text_file.write('--TMPSUPERSEDE INDEX ADDITION')
    text_file.write("\n")
    text_file.write("CREATE INDEX tmpsupersede_idet_idx ON tmpsupersede (id, effectiveTime);")
    text_file.write("\n")


    text_file.write("--GENERATE RELS CSV OUTPUT")
    text_file.write("\n")
    text_file.write(".echo OFF")
    text_file.write("\n")
    text_file.write(".headers ON")
    text_file.write("\n")
    text_file.write(".mode csv")
    text_file.write("\n")
    text_file.write(".output " + path[:-4] + "rels.csv")
    text_file.write("\n")
    text_file.write("SELECT r.id, r.effectiveTime, t.supersededTime, r.active, r.moduleId, r.sourceId, r.destinationId FROM relationships AS r, tmpsupersede AS t WHERE t.id = r.id AND t.effectiveTime = r.effectiveTime AND r.typeId = '116680003';")
    text_file.write("\n")


    text_file.close()

if __name__ == '__main__':
    path = '/home/ed/D/'
    NeoImport1Prep(path)
