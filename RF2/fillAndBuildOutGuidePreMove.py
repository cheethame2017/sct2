import subprocess
from SCTRF2config import pth
from deduplicateSnap import deduplicate
from indexbuilder import buildDescIndex
from TCTableBuild import createTCByTablesController

dataPath = pth('BASIC_DATA_BUILD') # location of data files during construction

snapPath = dataPath + "RF2Snap.db"
outPath = dataPath + "RF2Out.db"
db1Path = dataPath + 'db1.db'
db2Path = dataPath + 'db2.db'
RF2Out2path = dataPath + 'RF2Out2.db'

# sample line from https://stackoverflow.com/questions/2346074/execute-sqlite3-dot-commands-from-python-or-register-collation-in-command-line
# subprocess.call(["sqlite3", "xxx.db", ".mode tabs", ".import file.tsv table_name"])

# fill snap
subprocess.call(["sqlite3", snapPath, ".read " + dataPath + "importSnapshot.txt"])
subprocess.call(["sqlite3", snapPath, ".quit"])

# deduplicate
deduplicate(snapPath)

# fill out pt 1
subprocess.call(["sqlite3", outPath, ".read " + dataPath + "buildRF2Out_1.txt"])
subprocess.call(["sqlite3", outPath, ".quit"])

# fill description index
buildDescIndex(snapPath, outPath)

# fill out pt 2
subprocess.call(["sqlite3", outPath, ".read " + dataPath + "buildRF2Out_2.txt"])
subprocess.call(["sqlite3", outPath, ".quit"])

# create transitive closure
createTCByTablesController(db1Path, db2Path, snapPath, outPath, RF2Out2path, 1)

# create concrete lookups
subprocess.call(["sqlite3", outPath, ".read " + dataPath + "buildRF2Out_5.txt"])
subprocess.call(["sqlite3", outPath, ".quit"])

# now move RF2Snap.db & RF2Out.db to your preferred data location and run fillAndBuildOutGuidePostMove.py
