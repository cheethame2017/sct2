from __future__ import print_function, unicode_literals
import io
from datetime import date
from SCTRF2Helpers import pterm, listify, tuplify, activeColor_IN, tabString, TCCompare_IN, \
                        refSetDetailsFromMemIdandSetId_IN, hasDifferentTagTerm
from SCTRF2Funcs import swapDataCall, snapHeaderWrite, dotDrawTreeMapIsh, showRefsetRowsFromConAndRefset, \
                        snapshotMRCMHeader, hasDefInList
from SCTRF2Role import dictToSetreportIdFile
from writeModelTableHelpers import generateDiagram, cfg_local, getVersion, pterm_unidec, modelLimiterSelect
from SCTRF2config import cfg, tokDict
from SCTRF2Help import actionIndexDict
from platformSpecific import cfgPathInstall, sep, changeModelToSVGLocal, changeModelToSVGWinHost, modelToSVGLocal, clearScreen
from tagTest import preCompareTags

tok = tokDict()

def fillAttLimit():
    # call from fillAndBuildOutGuidePostMove.py
    resetConfig()
    setDomains((tok["ROOT"],), "*", False, False)
    limitAtt(1)
    generateDiagram()
    limitAtt(0)

def generateDiagramPre():
    generateDiagram()

def generateTagDiagram():
    # write anything out to tagresults file [build True]
    if cfg_local("WRITE_TAG_RESULTS") == 1:
        writeTagResultsBool = True
    else:
        writeTagResultsBool = False
    # write out appended list of changing top level points [build False]
    if cfg_local("APPEND_CHANGING_TAG_LIST") == 1:
        appendBool = True
    else:
        appendBool = False
    # True value reads from data; False value reads back latest pickle objects [build True]
    if cfg_local("FULL_TAG_RUN") == 1:
        fullRunBool = True
    else:
        fullRunBool = False
    # build data: False leaves file alone, True overwrites tagList.txt [build True]
    if cfg_local("TAG_BUILD_DATA") == 1:
        buildDataBool = True
    else:
        buildDataBool = False
    # include or exclude non-referenced domains and ranges from tagChangeNum.dot [build = doesn't matter]
    if cfg_local("TAG_INCL_OTHER_DOMS") == 1:
        includeOtherDomainsAndRangesBool = True
    else:
        includeOtherDomainsAndRangesBool = False
    # show or hide all the available term details [build doesn't matter]
    if cfg_local("TAG_FULL_TERM") == 1:
        fullTerm = True
    else:
        fullTerm = False
    # filter based on selected domain in MRCM window
    if cfg_local("TAG_RESTRICT_DOMAIN") == 1:
        restrictToDomainBool = True
    else:
        restrictToDomainBool = False

    returnList, maxIdList = preCompareTags(writeTagResultsBool, appendBool, fullRunBool, buildDataBool, includeOtherDomainsAndRangesBool, fullTerm, restrictToDomainBool)
    mydict = {}
    clearScreen()
    snapshotMRCMHeader()
    if not returnList == []:
        print('\033[4mTag results (Type=' + str(cfg_local("TAG_RUN_TYPE")) + (", restricted" if cfg_local("TAG_RESTRICT_DOMAIN") == 1 else "") + '):\033[24m\n')
        mycounter = 1
        key_list = sorted(list(returnList[0].keys()))
        for key in key_list:
            if key in returnList[1]:
                print(returnList[1][key] + " (" + str(len(returnList[0][key])) + ")\n")
                tempList = []
                for item in returnList[0][key]:
                    if item in maxIdList:
                        tempList.append([item, hasDifferentTagTerm((item, ), "Fake FSN (fake tag)")])
                    else:
                        tempList.append([item, pterm(item)])
                tempList = sorted(tempList, key=lambda x: x[1])
                for item in tempList:
                    print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0]) + tabString(item[0]) + item[1] + hasDefInList(item[0], cfg("SHOW_DEFS")))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(item[0])))))
                    mycounter += 1
                print()
    print(str(returnList[2][0]) + " concepts with tag named in MRCM class")
    print(str(returnList[2][1]) + " concepts with tag covered by MRCM class")
    print(str(returnList[2][2]) + " concepts with tag NOT covered by MRCM class (check K setting)")
    print(str(returnList[2][0] + returnList[2][1] + returnList[2][2]) + " total\n")

    dictToSetreportIdFile(mydict)
    return mydict

def drawChange(inDatesList, myValueRows, myValueRowsLimit):
    oldReadbackList = []
    newReadbackList = []
    compReadbackList = []
    with io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-pre_old.dot', "r", encoding="utf-8") as fold:
        for line in fold:
            oldReadbackList.append(line)
    with io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-pre_new.dot', "r", encoding="utf-8") as fnew:
        for line in fnew:
            newReadbackList.append(line)

    for newLine in newReadbackList:
        matchBool = False
        for oldLine in oldReadbackList:
            if oldLine == newLine:
                matchBool = True
        if matchBool:
            compReadbackList.append(newLine)
        else:
            compReadbackList.append([newLine, newLine])
    for line in compReadbackList:
        if len(line) == 2 and (" [arrowhead=" in line[0]) and not(" color=black, " in line[0]):
            # tempLevMatch = line[0].replace(' color=red, ',' style=bold, color="magenta", ').replace(' color=blue, ',' style=bold, color="cyan", ')
            tempLevMatch = line[0].replace(' color=red, ', ' color="magenta", ').replace(' color=blue, ', ' color="cyan", ')
            if (' style=dashed, color="magenta", ' in tempLevMatch) or (' style=dashed, color="cyan", ' in tempLevMatch):
                tempLevMatch = tempLevMatch.replace(' style=dashed, color="magenta", ', ' style="dashed,bold", color="magenta", ').replace(' style=dashed, color="cyan", ', ' style="dashed,bold", color="cyan", ')
            else:
                tempLevMatch = tempLevMatch.replace(' color="magenta", ', ' style=bold, color="magenta", ').replace(' color="cyan", ', ' style=bold, color="cyan", ')
            line[1] = tempLevMatch

    outputList = []

    # read in unchanged lines
    for line in compReadbackList[:-1]:
        if not len(line) == 2:
            outputList.append(line)

    # add section
    outputList.append("\n/* *** Addition changes *** */\n")

    # read in changed lines
    for line in compReadbackList[:-1]:
        if len(line) == 2:
            outputList.append(line[1])

    compReadbackList = []
    for oldLine in oldReadbackList:
        matchBool = False
        for newLine in newReadbackList:
            if oldLine == newLine:
                matchBool = True
        if not matchBool:
            compReadbackList.append([oldLine, oldLine])
    for line in compReadbackList:
        if len(line) == 2 and (" [arrowhead=" in line[0]) and not(" color=black, " in line[0]):
            # tempLevMatch = line[0].replace(' color=red, ',' style=bold, color="sienna", ').replace(' color=blue, ',' style=bold, color="purple", ')
            tempLevMatch = line[0].replace(' color=red, ', ' color="sienna", ').replace(' color=blue, ', ' color="purple", ')
            if (' style=dashed, color="sienna", ' in tempLevMatch) or (' style=dashed, color="purple", ' in tempLevMatch):
                tempLevMatch = tempLevMatch.replace(' style=dashed, color="sienna", ', ' style="dashed,bold", color="sienna", ').replace(' style=dashed, color="purple", ', ' style="dashed,bold", color="purple", ')
            else:
                tempLevMatch = tempLevMatch.replace(' color="sienna", ', ' style=bold, color="sienna", ').replace(' color="purple", ', ' style=bold, color="purple", ')
            line[1] = tempLevMatch

    # add section
    outputList.append("\n/* *** Removal changes *** */\n")
    # read in changed lines
    for line in compReadbackList:
        if len(line) == 2:
            if not line[1] == "}":
                outputList.append(line[1])

    # add caption
    if cfg_local("SHOW_CAPTION") == 1 and cfg_local("D_3") == 0:
        outputList.append("\n/* *** Caption *** */\n")
        today = date.today()
        dateLine = today.strftime("%B %d, %Y")
        versionString = "Between " + inDatesList[0] + " and " + inDatesList[1] + " |"
        if myValueRows == myValueRowsLimit:
            domainString = "Showing all domains|"
        else:
            preDomainString = "Showing domains:\\n"
            for item in myValueRowsLimit:
                preDomainString += item[0] + " " + pterm_unidec(item[0]) + "\\n"
            domainString = preDomainString[:-2] + "|"
        modelViewString = "Simplified diagram of SNOMED CT\\nMRCM Domain and Range constraints|Pre-coordination comparison view |"

        if cfg_local("RANK_DIRECTION") == 1 or cfg_local("RANK_DIRECTION") == 2:
            outputList.append("\"diagramLabel\" [label=\"{" + modelViewString + versionString + domainString + " International data|Drawn " + dateLine + "}\", ")
        else:
            outputList.append("\"diagramLabel\" [label=\"" + modelViewString + versionString + domainString + " International data|Drawn " + dateLine + "\", ")
        outputList.append("shape=record, fontsize=12];\n")

    # get keyLines
    keyLines = []
    outputList.append("\n/* *** Key *** */\n")
    # CONC - modify key file if concrete data
    if cfg("CONC_DATA") == 1:
        with open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'keyfileChangeConc.dot') as f:
            keyLines = f.readlines()
    else:
        with open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'keyfileChange.dot') as f:
            keyLines = f.readlines()
    for line in keyLines:
        outputList.append(line)

    # close
    outputList.append("}")

    f4 = open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-change.dot', 'w')
    for line in outputList:
        f4.write(line)
    f4.close()

    copyBool = False
    if cfg_local("LOCAL_FILES") == 1:
        changeModelToSVGLocal()
        copyBool = True
    if cfg_local("WIN_HOST_FILES") == 1:
        changeModelToSVGWinHost(copyBool)

def cfgReSet(): # resets MRCM configuration values for writeModelTableChange.py - back to 'default' values
    tempLineList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        tempLine = line
        tempLine = tempLine.replace("SHOW_KEY|0", "SHOW_KEY|1")
        tempLine = tempLine.replace("SHOW_CAPTION|0", "SHOW_CAPTION|1")
        tempLine = tempLine.replace("GENERATE_SVG|0", "GENERATE_SVG|1")
        tempLine = tempLine.replace("CLASS_TOOLTIP|0", "CLASS_TOOLTIP|1")
        tempLineList.append(tempLine)
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'w', encoding="UTF-8")
    for line in tempLineList:
        f.write(line)
    f.close()

def compareIdWithDomainsAndRanges(inId):
    f = io.open(cfgPathInstall() + 'files' + sep() + 'atts' + sep() + 'modelTableCO.txt', 'r', encoding="UTF-8")
    tempList = []
    positiveDomainSet = []
    positiveRangeDict = {}
    # domains
    for line in f:
        tempList = line.split("\t")
        if tempList[0] == "<<":
            # check subtype or self
            if TCCompare_IN(inId, tempList[1]):
                if tempList[1] not in positiveDomainSet:
                    positiveDomainSet.append(tempList[1])
        elif tempList[0] == "<":
            # check subtype
            if TCCompare_IN(inId, tempList[1]) and (not inId == tempList[1]):
                if tempList[1] not in positiveDomainSet:
                    positiveDomainSet.append(tempList[1])
        else:
            # check memberof
            if len(refSetDetailsFromMemIdandSetId_IN(tempList[1], inId)) > 0:
                if tempList[1] not in positiveDomainSet:
                    positiveDomainSet.append(tempList[1])
    # ranges
    f.seek(0)
    for line in f:
        tempList = line.split("\t")
        if tempList[6] == "<<" or tempList[6] == "=":
            # check subtype or self, or just self.
            if TCCompare_IN(inId, tempList[7].strip()):
                if tempList[7].strip() not in positiveRangeDict:
                    positiveRangeDict.update({tempList[7].strip(): [tempList[4]]})
                else:
                    if not tempList[4] in positiveRangeDict[tempList[7].strip()]:
                        positiveRangeDict[tempList[7].strip()].append(tempList[4])

        elif tempList[6] == "<":
            # check subtype
            if TCCompare_IN(inId, tempList[7].strip()) and (not inId == tempList[7].strip()):
                if tempList[7].strip() not in positiveRangeDict:
                    positiveRangeDict.update({tempList[7].strip(): [tempList[4]]})
                else:
                    if not tempList[4] in positiveRangeDict[tempList[7].strip()]:
                        positiveRangeDict[tempList[7].strip()].append(tempList[4])
        else:
            # check memberof
            if len(refSetDetailsFromMemIdandSetId_IN(tempList[7].strip(), inId)) > 0:
                if tempList[7].strip() not in positiveRangeDict:
                    positiveRangeDict.update({tempList[7].strip(): [tempList[4]]})
                else:
                    if not tempList[4] in positiveRangeDict[tempList[7].strip()]:
                        positiveRangeDict[tempList[7].strip()].append(tempList[4])
    f.close
    return positiveDomainSet, positiveRangeDict

def cfgSet(): # sets MRCM configuration values for writeModelTableChange.py - essentially simplifies diagram
    tempLineList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        tempLine = line
        tempLine = tempLine.replace("SHOW_KEY|1", "SHOW_KEY|0")
        tempLine = tempLine.replace("SHOW_CAPTION|1", "SHOW_CAPTION|0")
        tempLine = tempLine.replace("GENERATE_SVG|1", "GENERATE_SVG|0")
        tempLine = tempLine.replace("CLASS_TOOLTIP|1", "CLASS_TOOLTIP|0")
        tempLine = tempLine.replace("D_3|1", "D_3|0")
        tempLineList.append(tempLine)
    f.close()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'w', encoding="UTF-8")
    for line in tempLineList:
        f.write(line)
    f.close()

def swapDataSequence():
    swapDataCall()
    rootId = (tok['ROOT'],)
    snapHeaderWrite(rootId[0])

def cfgWrite_local(inString, inValue):
    writeToList = []
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'config' + sep() + 'MRCMconfig.cfg', 'r+', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            tempLine = line.strip()
            if not line[0] == "#":
                tempList = tempLine.split('|')
                if tempList[0] == inString:
                    writeToList.append(tempList[0] + '|' + str(inValue))
                else:
                    writeToList.append(tempLine)
            else:
                writeToList.append(tempLine)
        #print writeToList
        f.seek(0)
        for entry in writeToList:
            f.write(entry + '\n')
    finally:
        f.close()

def allDomains():
    domLimitList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # put # at start of domain rows you want to limit the diagram to
            domLimitList.append(line.strip().split('\t')[0])
        else:
            tempString = line.strip().split('\t')[0]
            domLimitList.append(tempString.split(" ")[1])
    f.close()
    return domLimitList

def limitDomainsID(returnIncluded):
    inDomainList = allDomains()
    returnAttList = []
    domLimitList = []
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # put # at start of domain rows you want to limit the diagram to
            domLimitList.append(line.strip())
    f.close()
    if returnIncluded:
        returnAttList = [row for row in inDomainList if row not in domLimitList]
        if len(returnAttList) > 0:
            return returnAttList
        else:
            return inDomainList
    else:
        return domLimitList

def domainsRead(returnDict):
    mydict = {}
    tempList = []
    mycounter = 1
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'r', encoding="UTF-8")
    for line in f:
        if line.strip()[0:2] == "# ":
            code = line.strip()[2:]
            pad = "* "
        else:
            code = line.strip()
            pad = "  "
        tempList.append([pterm(code), pad, code])
    f.close()
    tempList.sort(key=lambda x: x[0])
    if returnDict:
        print('MRCM Domains:\n')
        for row in tempList:
            print('[' + str(mycounter) + ']\t' + row[1] + activeColor_IN(row[2]) + tabString(row[2]) + pterm(row[2]))
            mydict.update(list(zip(listify(mycounter), listify(tuplify(row[2])))))
            mycounter += 1
        print()
        return mydict
    else:
        return tempList

def compareSnap(inId):
    mydict = {}
    mycounter = 1
    print("Testing:\t" + inId + "\t" + pterm(inId))
    if TCCompare_IN(inId, tok["CONMOD_ATTRIB"]):
        # attrib domain and range code
        print("\nAttribute domain:\n")
        mydict = showRefsetRowsFromConAndRefset(inId, tok["MRCM_ADIR"])
        print("Attribute range:\n")
        mydict = showRefsetRowsFromConAndRefset(inId, tok["MRCM_ARIR"], mydict, max(mydict))
        return mydict
    else:
        # domain and range code
        print()
        domSet, rangeSet = compareIdWithDomainsAndRanges(inId)
        if len(domSet) == 0:
            print("In no MRCM Domain.\n")
        else:
            if len(domSet) == 1:
                print("In MRCM Domain:\n")
            else:
                print("In MRCM Domains:\n")
            for item in domSet:
                print('[' + str(mycounter) + ']\t' + item + "\t" + pterm(item))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(item)))))
                mycounter += 1
            # print("\nFor full constraint, select domain ('N'), switch to Snap and reload focus ('v'),\nlist refsets ('kN') and view 'MRCM domain international reference set' entry ('oN')")
        if len(rangeSet) == 0:
            print("\nIn no MRCM Range.")
        else:
            if len(rangeSet) == 1:
                print("\nIn MRCM Range:")
            else:
                print("\nIn MRCM Ranges:")
            for rangeItem in rangeSet:
                print("\n"+ '[' + str(mycounter) + ']\t' + rangeItem + "\t" + pterm(rangeItem))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(rangeItem)))))
                mycounter += 1
                if len(rangeSet[rangeItem]) == 1:
                    print("\n\tFor Attribute:\n")
                else:
                    print("\n\tFor Attributes:\n")
                for attName in rangeSet[rangeItem]:
                    print("\t" + '[' + str(mycounter) + ']\t' + attName + "\t" + pterm(attName))
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(attName)))))
                    mycounter += 1
        print()
        return mydict

def showDomainConstraint(inId):
    checkList = [item[2] for item in domainsRead(False)]
    if inId in checkList:
        mydict = showRefsetRowsFromConAndRefset(inId, tok["MRCM_DIR"])
    else:
        mydict = domainsRead(True)
    return mydict

def readDoms():
    domLimitList = []
    # readback current set of domains
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'r', encoding="UTF-8")
    for line in f:
        domLimitList.append(line.strip())
    f.close()
    return domLimitList

def anyDomExclusions(domLimitList):
    total = len(domLimitList)
    hashMatches = 0
    for row in domLimitList:
        if row[0] == "#":
            hashMatches += 1
    if hashMatches > 0 and hashMatches < total:
        return True
    else:
        return False

def setDomains(testId, incExclString, descBool, groupBool):
    domsOfInterest = []
    domLimitList = []
    alreadyExcl = []
    # retrieve all domains
    domLimitList = readDoms()
    # determine domsOfInterest - with or without descendants of new entrant.
    if descBool:
        for dom in domLimitList:
            if TCCompare_IN(dom, testId) or TCCompare_IN(dom.replace("# ", ""), testId):
                domsOfInterest.append(dom.replace("# ", ""))
    elif groupBool:
        modelLimiterList = modelLimiterSelect()
        for listLine in modelLimiterList:
            if testId in listLine:
                for dom in listLine:
                    domsOfInterest.append(dom)
    else:
        domsOfInterest.append(testId)
    if anyDomExclusions(domLimitList):
        alreadyExcl = limitDomainsID(False)
    # writeback with mods
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'w', encoding="UTF-8")
    # if inclExcl is include:
    if incExclString == "=":
        for row in domLimitList:
            if row.strip().split('\t')[0] in domsOfInterest:
                f.write("# " + row + "\n")
            else:
                f.write(row + "\n")
    elif incExclString == "!":
        domsOfInterest += alreadyExcl
        for row in domLimitList:
            if (row.strip() not in domsOfInterest) and not (row[0] == "#"):
                f.write("# " + row + "\n")
            elif ' ' in row.strip():
                if (row.strip().split(' ')[1] in domsOfInterest) and (row[0] == "#"):
                    f.write(row[2:] + "\n")
                else:
                    f.write(row + "\n")
            else:
                f.write(row + "\n")
    else:
        for row in domLimitList:
            if row[0] == "#":
                f.write(row[2:] + "\n")
            else:
                f.write(row + "\n")
    f.close()
    return testId

def promptString():
    indexDict = actionIndexDict('SCTMRCM')
    indexDictLetters = list(indexDict)
    promptString = 'Action:\n\033[36m'
    counter = 1
    if cfg("TAB_INDENT") == 0:
        for letter in indexDictLetters:
            promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 3 == 0 else '\t')
            counter += 1
    else:
        for letter in indexDictLetters:
            promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 4 == 0 else '\t')
            counter += 1
    promptString += '\n\n[zz] Exit\t\t[-] (+letter) help\n\n\033[91;7mInput selection:\033[92;27m '
    return promptString

def constrOp(inInteger):
    cfgWrite_local("CONSTR_OP", inInteger)
    return inInteger

def showCard(inInteger):
    cfgWrite_local("SHOW_CARD", inInteger)
    if inInteger == 1:
        cfgWrite_local("CONSTR_OP", 1)
    return inInteger

def showKey(inInteger):
    cfgWrite_local("SHOW_KEY", inInteger)
    return inInteger

def showCaption(inInteger):
    cfgWrite_local("SHOW_CAPTION", inInteger)
    return inInteger

def showModel(inInteger):
    cfgWrite_local("SHOW_MODEL", inInteger)
    return inInteger

def attSize(inInteger):
    if inInteger == 6:
        dotDrawTreeMapIsh((tok["ROOT"],), {}, False, False, False)
        inInteger = 0
    cfgWrite_local("ATT_SIZE", inInteger)
    if inInteger > 3:
        # only run pre-coord if mondrian att size set.
        cfgWrite_local("RUN_PRE", 1)
        cfgWrite_local("RUN_POST", 0)
    return inInteger

def domRangeSize(inInteger):
    cfgWrite_local("DOM_RANGE_SIZE", inInteger)
    if inInteger > 3:
        # only run pre-coord if mondrian att size set.
        cfgWrite_local("RUN_PRE", 1)
        cfgWrite_local("RUN_POST", 0)
    return inInteger

def generateSVG(inInteger):
    cfgWrite_local("GENERATE_SVG", inInteger)
    return inInteger

def classTooltip(inInteger):
    cfgWrite_local("CLASS_TOOLTIP", inInteger)
    return inInteger

def rankDirection(inInteger):
    cfgWrite_local("RANK_DIRECTION", inInteger)
    return inInteger

def parentDomain(inInteger):
    cfgWrite_local("SHOW_PARENT_DOMAIN", inInteger)
    return inInteger

def setD3(inInteger):
    cfgWrite_local("D_3", inInteger)
    if inInteger == 1:
        cfgWrite_local("SHOW_CARD", 0)
    else:
        cfgWrite_local("SHOW_CARD", 1)
    return inInteger

def autoHideKey(inInteger):
    cfgWrite_local("HIDE_KEY", inInteger)
    return inInteger

def winHostFiles(inInteger):
    cfgWrite_local("WIN_HOST_FILES", inInteger)
    return inInteger

def localFiles(inInteger):
    cfgWrite_local("LOCAL_FILES", inInteger)
    return inInteger

def angledEdges(inInteger):
    cfgWrite_local("ANGLED_EDGES", inInteger)
    return inInteger

def fontSize(inInteger):
    cfgWrite_local("FONT_SIZE", inInteger)
    return inInteger

def multSet(inInteger, inToken):
    cfgWrite_local(inToken, inInteger)
    return inInteger

def runPre(inInteger):
    cfgWrite_local("RUN_PRE", inInteger)
    return inInteger

def runPost(inInteger):
    cfgWrite_local("RUN_POST", inInteger)
    return inInteger

def limitAtt(inInteger):
    cfgWrite_local("ONLY_ATT", inInteger)
    return inInteger

def tagWriteResults(inInteger):
    cfgWrite_local("WRITE_TAG_RESULTS", inInteger)
    return inInteger

def tagAppendChangingList(inInteger):
    cfgWrite_local("APPEND_CHANGING_TAG_LIST", inInteger)
    return inInteger

def tagFullRun(inInteger):
    cfgWrite_local("FULL_TAG_RUN", inInteger)
    if inInteger == 1:
        cfgWrite_local("TAG_BUILD_DATA", 1)
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 0)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 0)
        cfgWrite_local("TAG_INCLUDE_ATTS", 0)
    return inInteger

def tagRunNum(inInteger):
    cfgWrite_local("TAG_RUN_NUM", inInteger)
    return inInteger

def tagRunType(inInteger):
    cfgWrite_local("TAG_RUN_TYPE", inInteger)
    return inInteger

def tagBuildData(inInteger):
    cfgWrite_local("TAG_BUILD_DATA", inInteger)
    return inInteger

def tagIncludeOtherDoms(inInteger):
    cfgWrite_local("TAG_INCL_OTHER_DOMS", inInteger)
    return inInteger

def tagTestVDomAndRange(inInteger):
    cfgWrite_local("TAG_DOM_RAN_TEST", inInteger)
    return inInteger

def tagFullTerm(inInteger):
    cfgWrite_local("TAG_FULL_TERM", inInteger)
    return inInteger

def tagRestrictDomain(inInteger):
    cfgWrite_local("TAG_RESTRICT_DOMAIN", inInteger)
    return inInteger

def tagIncludeAtts(inInteger):
    cfgWrite_local("TAG_INCLUDE_ATTS", inInteger)
    return inInteger

def tagTempDomAtt(inType):
    tempDom = cfg_local("TAG_INCL_OTHER_DOMS")
    tempAtt = cfg_local("TAG_INCLUDE_ATTS")
    tempRestrict = cfg_local("TAG_RESTRICT_DOMAIN")
    if inType == "0": # restrict, no additions
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 0)
        cfgWrite_local("TAG_INCLUDE_ATTS", 0)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 1)
    if inType == "1": # restrict, show domains
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 1)
        cfgWrite_local("TAG_INCLUDE_ATTS", 0)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 1)
    if inType == "2": # restrict, show atts
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 0)
        cfgWrite_local("TAG_INCLUDE_ATTS", 1)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 1)
    if inType == "3": # restrict show both
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 1)
        cfgWrite_local("TAG_INCLUDE_ATTS", 1)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 1)
    if inType == "4": # no restrict, no additions
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 0)
        cfgWrite_local("TAG_INCLUDE_ATTS", 0)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 0)
    if inType == "5": # no restrict, show doms
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 1)
        cfgWrite_local("TAG_INCLUDE_ATTS", 0)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 0)
    if inType == "6": # no restrict, show atts
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 0)
        cfgWrite_local("TAG_INCLUDE_ATTS", 1)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 0)
    if inType == "7": # no restrict, show both
        cfgWrite_local("TAG_INCL_OTHER_DOMS", 1)
        cfgWrite_local("TAG_INCLUDE_ATTS", 1)
        cfgWrite_local("TAG_RESTRICT_DOMAIN", 0)
    return tempDom, tempAtt, tempRestrict

def tagResetDomAtt(tempDom, tempAtt, tempRestrict):
    cfgWrite_local("TAG_INCL_OTHER_DOMS", tempDom)
    cfgWrite_local("TAG_INCLUDE_ATTS", tempAtt)
    cfgWrite_local("TAG_RESTRICT_DOMAIN", tempRestrict)

def resetConfig():
    cfgWrite_local("CONSTR_OP", 1)
    cfgWrite_local("SHOW_CARD", 1)
    cfgWrite_local("SHOW_KEY", 1)
    cfgWrite_local("SHOW_CAPTION", 1)
    cfgWrite_local("SHOW_MODEL", 1)
    cfgWrite_local("ATT_SIZE", 0)
    cfgWrite_local("DOM_RANGE_SIZE", 0)
    cfgWrite_local("GENERATE_SVG", 1)
    cfgWrite_local("CLASS_TOOLTIP", 1)
    cfgWrite_local("RANK_DIRECTION", 3)
    cfgWrite_local("SHOW_PARENT_DOMAIN", 1)
    cfgWrite_local("D_3", 0)
    cfgWrite_local("HIDE_KEY", 0)
    cfgWrite_local("WIN_HOST_FILES", 1)
    cfgWrite_local("LOCAL_FILES", 1)
    cfgWrite_local("ANGLED_EDGES", 1)
    cfgWrite_local("ONLY_ATT", 0)
    cfgWrite_local("RUN_PRE", 1)
    cfgWrite_local("RUN_POST", 0)
    cfgWrite_local("FONT_SIZE", 9)
    cfgWrite_local("ATT_POWER", 5)
    cfgWrite_local("ATT_MULT", 5)
    cfgWrite_local("WRITE_TAG_RESULTS", 1)
    cfgWrite_local("APPEND_CHANGING_TAG_LIST", 0)
    cfgWrite_local("FULL_TAG_RUN", 0)
    cfgWrite_local("TAG_RUN_NUM", 0)
    cfgWrite_local("TAG_RUN_TYPE", 3)
    cfgWrite_local("TAG_BUILD_DATA", 0)
    cfgWrite_local("TAG_INCL_OTHER_DOMS", 0)
    cfgWrite_local("TAG_FULL_TERM", 1)
    cfgWrite_local("TAG_RESTRICT_DOMAIN", 0)
    cfgWrite_local("TAG_INCLUDE_ATTS", 0)

    return 1

def changeSteps():
    versionList = []
    cfgSet()
    versionList.append(getVersion(1))
    generateDiagram()
    swapDataSequence()
    versionList.append(getVersion(1))
    myValueRows, myValueRowsLimit = generateDiagram()
    swapDataSequence()
    cfgReSet()
    drawChange(sorted(versionList), myValueRows, myValueRowsLimit)

def nullDiagram():
    f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'MRCM-pre.dot', 'w', encoding="UTF-8")
    f.write("digraph RF2_MRCM { rankdir=LR; splines=ortho; node [shape=box, peripheries=1, style=filled, fontsize=9, fontname=Helvetica, tooltip=\" \", fillcolor=palegreen];\n")
    # f.write("\"Blank\" [style=invis];")
    f.write("}")
    f.close()
    modelToSVGLocal(0)
