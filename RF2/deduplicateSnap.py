from __future__ import print_function, unicode_literals
import io
import sqlite3
from platformSpecific import cfgPathInstall, sep

def findDups(inTable, snapPath):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    progress = 1
    try:
        cursorIn2.execute("select tbl.id, count(tbl.id) from " + inTable + " tbl group by tbl.id having count(tbl.id) > 1;")
        recordsetIn2 = cursorIn2.fetchall()
        if recordsetIn2:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'dedup' + sep() + 'dedupLog.txt', 'a', encoding="UTF-8")
            cursorIn = connectionIn2.cursor()
            for item in recordsetIn2:
                keepList = []
                removeList = []
                if (progress % 200 == 0):
                    print(item)
                try:
                    # and then deduplicate
                    cursorIn.execute("select * from " + inTable + " tbl where tbl.id = ? order by tbl.effectiveTime desc;", (item[0],))
                    recordsetIn = cursorIn.fetchall()
                    if (progress % 200 == 0):
                        print("keep: ", recordsetIn[0])
                    f.write(inTable + '\t' + 'keep:' + '\t' + '\t'.join(str(s) for s in recordsetIn[0]) + '\n')
                    keepList.append(recordsetIn[0])
                    for row in recordsetIn[1:]:
                        if (progress % 200 == 0):
                            print("lose: ", row)
                        f.write(inTable + '\t' + 'lose:' + '\t' + '\t'.join(str(s) for s in row) + '\n')
                    for row in recordsetIn:
                        removeList.append(row)
                    # remove all rows
                    queryString = "delete from " + inTable + " where id = ?;"
                    for row in removeList:
                        cursorIn.execute(queryString, (row[0],))
                    # add back keep row (need this approach because of absolute duplicate rows in M+DR)
                    markerString = "?, " * len(keepList[0])
                    queryString = "insert into " + inTable + " values (" + markerString[:-2] + ");"
                    for row in keepList:
                        cursorIn.execute(queryString, row)
                    if (progress % 200 == 0):
                        connectionIn2.commit()
                except:
                    continue
                progress += 1
            connectionIn2.commit()
            cursorIn.close()
            f.close()
        cursorIn2.close()
        connectionIn2.close()
    except:
        print("no id field")

def tables_in_sqlite_db(snapPath):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = [
        v[0] for v in cursorIn2.fetchall()
        if v[0] != "sqlite_sequence"
    ]
    cursorIn2.close()
    connectionIn2.close()
    return tables

def deduplicate(snapPath):
    # initialise log
    f = io.open(cfgPathInstall() + 'files' + sep() + 'dedup' + sep() + 'dedupLog.txt', 'w', encoding="UTF-8")
    f.close()
    # List tables
    tables = tables_in_sqlite_db(snapPath)
    print(tables)
    for table in tables:
        print(table)
        findDups(table, snapPath)

if __name__ == '__main__':
    # set path to snap or out db file.
    snapPath = '/home/ed/D/RF2Snap.db'
    deduplicate(snapPath)
