#!/usr/bin/python
from __future__ import print_function, unicode_literals
import sqlite3
from SCTRF2config import tok
from SCTRF2Helpers import refSetMembersCompareFromConIds_IN, descendantsIDFromConId_IN, refSetTypeFromConId_IN, \
                            refSetMemberCountFromConId_IN

def refsetCompare(outPath):
    # initialise
    refsets = descendantsIDFromConId_IN(tok("REFSETS"))
    conn = sqlite3.connect(outPath)
    cur = conn.cursor()
    progress = 1
    total = len(refsets)
    chapterDict = {}
    refsetDict = {}
    testableRefsets = []

    # do main chapters and identify list of testable refsets
    for refset in refsets:
        print(str(progress) + " / " + str(total))
        refsetType = refSetTypeFromConId_IN(refset[0])[0][0]
        if refSetMemberCountFromConId_IN(refset[0], refsetType) > 0:
            leftCount, otherRows, refsetDict = refSetMembersCompareFromConIds_IN(refset[0], "", refsetType, 1, {}, refsetDict, True, 0)
            tempList = []
            for row in otherRows:
                row.insert(1, leftCount)
                row.insert(0, refset[0])
                row.insert(0, 1)
                print(row)
                cur.execute("insert into rscompare values (?, ? , ? , ? , ? , ? , ? , ? , ? , ?)", tuple(row))
                tempList.append(row[2])
            if len(tempList) > 0:
                chapterDict[refset[0]] = tempList
                if not refset in testableRefsets:
                    testableRefsets.append(refset)
        progress += 1
        conn.commit()

    progress = 1
    total = len(testableRefsets)

    # test the testable refsets against each other.
    for refset in testableRefsets:
        print(str(progress) + " / " + str(total))
        refsetType = refSetTypeFromConId_IN(refset[0])[0][0]
        leftCount, otherRows, refsetDict = refSetMembersCompareFromConIds_IN(refset[0], "", refsetType, 2, chapterDict, refsetDict, True, 0)
        for row in otherRows:
            row.insert(1, leftCount)
            row.insert(0, refset[0])
            row.insert(0, 2)
            print(row)
            cur.execute("insert into rscompare values (?, ? , ? , ? , ? , ? , ? , ? , ? , ?)", tuple(row))
        progress += 1
        conn.commit()

    # add indices
    cur.execute("CREATE INDEX rscompare_compareType_idx ON rscompare (compareType);")
    cur.execute("CREATE INDEX rscompare_leftId_idx ON rscompare (leftId);")
    cur.execute("CREATE INDEX rscompare_rightId_idx ON rscompare (rightId);")
    conn.commit()

    cur.close()
    conn.close()

if __name__ == '__main__':
    outPath = '/home/ed/D/RF2Out.db'
    refsetCompare(outPath)
