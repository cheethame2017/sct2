#!/usr/bin/env python
from __future__ import print_function, unicode_literals
from SCTRF2Funcs import snapshotMRCMHeader, focusSwapWrite, browseFromId, listify, readFocusValue
from SCTRF2config import tokDict
from writeModelTable import domainsRead, setDomains, promptString, constrOp, showCard, showKey, \
                                showCaption, showModel, attSize, domRangeSize, generateSVG, classTooltip, rankDirection, \
                                parentDomain, setD3, autoHideKey, winHostFiles, localFiles, angledEdges, resetConfig, \
                                changeSteps, limitAtt, runPre, runPost, generateDiagramPre, fontSize, \
                                multSet, compareSnap, showDomainConstraint, nullDiagram, fillAttLimit, generateTagDiagram, \
                                tagWriteResults, tagAppendChangingList, tagFullRun, tagRunNum, tagRunType, tagBuildData, \
                                tagIncludeOtherDoms, tagFullTerm, tagRestrictDomain, tagIncludeAtts, tagTempDomAtt, tagResetDomAtt, \
                                tagTestVDomAndRange
from SCTRF2Help import selectHelpFile
from platformSpecific import clearScreen, headerWriterMRCM, input2_3

tok = tokDict()

# single letter invocation:

def selectHelpFileCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in selectHelpFileCall')
    snapshotMRCMHeader()
    selectHelpFile(inString, 'SCTMRCM')
    returnString = {}
    if debug:
        print(returnString)
    return returnString

def generateDiagramCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in generateDiagramCall')
    snapshotMRCMHeader()
    generateDiagramPre()
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def generateTagDiagramCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in generateTagDiagramCall')
    snapshotMRCMHeader()
    returnString = generateTagDiagram()
    if returnString == {}:
        returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def domainsReadCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in domainsReadCall')
    snapshotMRCMHeader()
    returnString = domainsRead(True)
    if debug:
        print(returnString)
    return returnString

def resetConfigCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> resetConfig')
    snapshotMRCMHeader()
    resetConfig()
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def changeStepsCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> changeSteps')
    snapshotMRCMHeader()
    changeSteps()
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def compareSnapCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> compareSnap')
    snapshotMRCMHeader()
    returnString = compareSnap(readFocusValue()[0])
    if debug:
        print(returnString)
    return returnString

def nullDiagramCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> nullDiagram')
    snapshotMRCMHeader()
    nullDiagram()
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

# letter plus number

def constrOpCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> constrOp', str(inInteger))
    snapshotMRCMHeader()
    returnString = constrOp(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def showCardCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showCard', str(inInteger))
    snapshotMRCMHeader()
    returnString = showCard(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def showKeyCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showKey', str(inInteger))
    snapshotMRCMHeader()
    returnString = showKey(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def showCaptionCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showCaption', str(inInteger))
    snapshotMRCMHeader()
    returnString = showCaption(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def showModelCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> showModel', str(inInteger))
    snapshotMRCMHeader()
    returnString = showModel(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def attSizeCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> attSize', str(inInteger))
    snapshotMRCMHeader()
    returnString = attSize(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def domRangeSizeCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> domRangeSize', str(inInteger))
    snapshotMRCMHeader()
    returnString = domRangeSize(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def generateSVGCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> generateSVG', str(inInteger))
    snapshotMRCMHeader()
    returnString = generateSVG(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def classTooltipCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> classTooltip', str(inInteger))
    snapshotMRCMHeader()
    returnString = classTooltip(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def rankDirectionCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> rankDirection', str(inInteger))
    snapshotMRCMHeader()
    returnString = rankDirection(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagWriteResultsCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagWriteResults', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagWriteResults(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagAppendChangingListCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagAppendChangingList', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagAppendChangingList(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagFullRunCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagFullRun', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagFullRun(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagRunNumCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagRunNum', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagRunNum(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagRunTypeCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagRunType', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagRunType(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagBuildDataCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagBuildData', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagBuildData(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagIncludeOtherDomsCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagIncludeOtherDoms', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagIncludeOtherDoms(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagTestVDomAndRangeCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagTestVDomAndRange', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagTestVDomAndRange(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString


def tagIncludeAttsCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagIncludeAtts', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagIncludeAtts(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagFullTermCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagFullTerm', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagFullTerm(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def tagRestrictDomainCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> tagRestrictDomain', str(inInteger))
    snapshotMRCMHeader()
    returnString = tagRestrictDomain(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def fontSizeCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> fontSize', str(inInteger))
    snapshotMRCMHeader()
    returnString = fontSize(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def multCall(inInteger, inToken):
    if not debug:
        clearScreen()
    if debug:
        print('-> multSet', str(inInteger))
    snapshotMRCMHeader()
    returnString = multSet(inInteger, inToken)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def parentDomainCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> parentDomain', str(inInteger))
    snapshotMRCMHeader()
    returnString = parentDomain(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def setD3Call(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setD3', str(inInteger))
    snapshotMRCMHeader()
    returnString = setD3(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def autoHideKeyCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> autoHideKey', str(inInteger))
    snapshotMRCMHeader()
    returnString = autoHideKey(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def winHostFilesCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> winHostFiles', str(inInteger))
    snapshotMRCMHeader()
    returnString = winHostFiles(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def limitAttCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> limitAtt', str(inInteger))
    snapshotMRCMHeader()
    if inInteger == 2:
        fillAttLimit()
    else:
        returnString = limitAtt(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def localFilesCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> localFiles', str(inInteger))
    snapshotMRCMHeader()
    returnString = localFiles(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def angledEdgesCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> angledEdges', str(inInteger))
    snapshotMRCMHeader()
    returnString = angledEdges(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def runPreCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> runPre', str(inInteger))
    snapshotMRCMHeader()
    returnString = runPre(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

def runPostCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> runPost', str(inInteger))
    snapshotMRCMHeader()
    returnString = runPost(inInteger)
    returnString = domainsReadCall()
    if debug:
        print(returnString)
    return returnString

# pass index identifier

def browseFromIndexCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in browseFromIndexCall')
    snapshotMRCMHeader()
    callId = inString
    if isinstance(callId, str):
        callId = listify(callId.strip())
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    if debug:
        print(returnString)
    return returnString

def showDomainConstraintCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in howDomainConstraintCall')
    snapshotMRCMHeader()
    callId = inString
    # if isinstance(callId, str):
    #     callId = listify(callId.strip())
    # focusSwapWrite(callId[0])
    returnString = showDomainConstraint(callId[0])
    if debug:
        print(returnString)
    return returnString

# pass index identifier and other pointer

def setDomsCall(testId, incExclString, descBool, groupBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> setDomains', testId, incExclString)
    snapshotMRCMHeader()
    returnString = setDomains(testId[0], incExclString, descBool, groupBool)
    if debug:
        print(returnString)
    returnString = domainsReadCall()
    return returnString

# initialise
returnString = {}
debug = False
headerWriterMRCM()
clearScreen()
# main control code
firstTime = True
while True:
    if firstTime:
        topaction = "a"
        firstTime = False
    else:
        topaction = input2_3(promptString()).strip()
    if topaction == "":
        topaction = "a"
    if topaction == 'zz':
        if debug:
            print(returnString)
        clearScreen()
        break  # breaks out of the while loop and ends session
    # toggle *most* debug outputs
    elif topaction == 'xx':
        if debug:
            debug = False
            print('debug OFF')
        else:
            debug = True
            print('debug ON')
    # single letter calls
    elif topaction == 'a':
        if debug:
            print(returnString)
            print('-> calling domainsReadCall')
        returnString = domainsReadCall()
    elif topaction == 'd':
        if debug:
            print(returnString)
            print('-> calling generateDiagramCall')
        returnString = generateDiagramCall()
    elif topaction == 'v':
        if debug:
            print(returnString)
            print('-> calling reloadFocusCall')
        returnString = resetConfigCall()
    elif topaction == 'w':
        if debug:
            print(returnString)
            print('-> calling reloadFocusCall')
        returnString = changeStepsCall()
    elif topaction == 'C':
        if debug:
            print(returnString)
            print('-> calling compareSnapCall')
        returnString = compareSnapCall()
    elif topaction == 'E':
        if debug:
            print(returnString)
            print('-> calling compareSnapCall')
        returnString = nullDiagramCall()
    elif topaction[0] == 'F':
        if debug:
            print(returnString)
            print('-> calling generateTagDiagramCall')
        if len(topaction) == 1:
            returnString = generateTagDiagramCall()
        elif len(topaction) == 2 and topaction[1] == "F":
            returnString = generateDiagramCall()
            returnString = generateTagDiagramCall()
        elif len(topaction) == 2:
            tempDom, tempAtt, tempRestrict = tagTempDomAtt(topaction[1])
            # returnString = generateDiagramCall()
            returnString = generateTagDiagramCall()
            tagResetDomAtt(tempDom, tempAtt, tempRestrict)

    else:
        try:
            if debug:
                print(topaction[0], topaction[1:])
            if topaction[0] == 'e':
                if debug:
                    print('e', int(topaction[1:]))
                returnString = showCaptionCall(int(topaction[1:]))
            elif topaction[0] == 'f':
                if debug:
                    print('f', int(topaction[1:]))
                returnString = showModelCall(int(topaction[1:]))
            elif topaction[0] == 'g':
                if debug:
                    print('g', int(topaction[1:]))
                returnString = attSizeCall(int(topaction[1:]))
            elif topaction[0] == 'h':
                if debug:
                    print('h', int(topaction[1:]))
                returnString = domRangeSizeCall(int(topaction[1:]))
            elif topaction[0] == 'i':
                if debug:
                    print('i', int(topaction[1:]))
                returnString = generateSVGCall(int(topaction[1:]))
            elif topaction[0] == 'k':
                if debug:
                    print('k', int(topaction[1:]))
                returnString = classTooltipCall(int(topaction[1:]))
            elif topaction[0] == 'l':
                if debug:
                    print('l', int(topaction[1:]))
                returnString = rankDirectionCall(int(topaction[1:]))
            elif topaction[0] == 'm':
                if debug:
                    print('m', int(topaction[1:]))
                returnString = parentDomainCall(int(topaction[1:]))
            elif topaction[0] == 'n':
                if debug:
                    print('n', int(topaction[1:]))
                returnString = setD3Call(int(topaction[1:]))
            elif topaction[0] == 'o':
                if debug:
                    print('o', int(topaction[1:]))
                returnString = autoHideKeyCall(int(topaction[1:]))
            elif topaction[0] == 's':
                if debug:
                    print('s', int(topaction[1:]))
                returnString = fontSizeCall(int(topaction[1:]))
            elif topaction[0] == 'A':
                if debug:
                    print('A', int(topaction[1:]))
                returnString = multCall(int(topaction[1:]), "ATT_POWER")
            elif topaction[0] == 'B':
                if debug:
                    print('B', int(topaction[1:]))
                returnString = multCall(int(topaction[1:]), "ATT_MULT")
            elif topaction[0] == 't':
                if debug:
                    print('t', int(topaction[1:]))
                returnString = winHostFilesCall(int(topaction[1:]))
            elif topaction[0] == 'x':
                if debug:
                    print('x', int(topaction[1:]))
                returnString = limitAttCall(int(topaction[1:]))
            elif topaction == 'u':
                if debug:
                    print(returnString)
                returnString = selectHelpFileCall('-')
            elif topaction[0] == '-':
                if debug:
                    print('- help file for', topaction[1:])
                returnString = selectHelpFileCall(topaction[1:])
            elif topaction[0] == 'q':
                if debug:
                    print('q', int(topaction[1:]))
                returnString = localFilesCall(int(topaction[1:]))
            elif topaction[0] == 'r':
                if debug:
                    print('r', int(topaction[1:]))
                returnString = angledEdgesCall(int(topaction[1:]))
            elif topaction[0] == 'y':
                if debug:
                    print('y', int(topaction[1:]))
                returnString = runPreCall(int(topaction[1:]))
            elif topaction[0] == 'z':
                if debug:
                    print('z', int(topaction[1:]))
                returnString = runPostCall(int(topaction[1:]))
            elif topaction[0] == 'j':
                if debug:
                    print(returnString)
                returnString = constrOpCall(int(topaction[1:]))
            elif topaction[0] == 's':
                if debug:
                    print(returnString)
                returnString = showCardCall(int(topaction[1:]))
            elif topaction[0] == 'p':
                if debug:
                    print(returnString)
                returnString = showKeyCall(int(topaction[1:]))
            elif topaction[0] == 'G':
                if debug:
                    print('G', int(topaction[1:]))
                returnString = tagWriteResultsCall(int(topaction[1:]))
            elif topaction[0] == 'H':
                if debug:
                    print('H', int(topaction[1:]))
                returnString = tagAppendChangingListCall(int(topaction[1:]))
            elif topaction[0] == 'I':
                if debug:
                    print('I', int(topaction[1:]))
                returnString = tagFullRunCall(int(topaction[1:]))
            elif topaction[0] == 'J':
                if debug:
                    print('J', int(topaction[1:]))
                returnString = tagRunNumCall(int(topaction[1:]))
            elif topaction[0] == 'K':
                if debug:
                    print('K', int(topaction[1:]))
                returnString = tagRunTypeCall(int(topaction[1:]))
            elif topaction[0] == 'L':
                if debug:
                    print('L', int(topaction[1:]))
                returnString = tagBuildDataCall(int(topaction[1:]))
            elif topaction[0] == 'M':
                if debug:
                    print('M', int(topaction[1:]))
                returnString = tagIncludeOtherDomsCall(int(topaction[1:]))
            elif topaction[0] == 'N':
                if debug:
                    print('N', int(topaction[1:]))
                returnString = tagFullTermCall(int(topaction[1:]))
            elif topaction[0] == 'O':
                if debug:
                    print('O', int(topaction[1:]))
                returnString = tagRestrictDomainCall(int(topaction[1:]))
            elif topaction[0] == 'P':
                if debug:
                    print('P', int(topaction[1:]))
                returnString = tagIncludeAttsCall(int(topaction[1:]))
            elif topaction[0] == 'Q':
                if debug:
                    print('Q', int(topaction[1:]))
                returnString = tagTestVDomAndRangeCall(int(topaction[1:]))
            elif topaction[0] == 'b':
                if debug:
                    print('b', topaction[1:])
                if topaction == "bb":
                    returnString = setDomsCall((tok["ROOT"],), "*", False, False)
                elif len(topaction) >= 1:
                    if topaction[-1] == "+":
                        testId = returnString[int(topaction[1:-1])]
                        descBool = True
                        groupBool = False
                    elif topaction[-1] == "*":
                        testId = returnString[int(topaction[1:-1])]
                        descBool = False
                        groupBool = True
                    else:
                        testId = returnString[int(topaction[1:])]
                        descBool = False
                        groupBool = False
                    incExclString = "="
                    returnString = setDomsCall(testId, incExclString, descBool, groupBool)
            elif topaction[0] == 'c':
                if debug:
                    print('c', topaction[1:])
                if topaction == "cc":
                    returnString = setDomsCall(tok["ROOT"], "*", False, False)
                if len(topaction) >= 1:
                    if topaction[-1] == "+":
                        testId = returnString[int(topaction[1:-1])]
                        descBool = True
                        groupBool = False
                    else:
                        testId = returnString[int(topaction[1:])]
                        descBool = False
                        groupBool = False
                    incExclString = "!"
                    returnString = setDomsCall(testId, incExclString, descBool, groupBool)
            elif topaction[0] == 'D':
                if debug:
                    print('', returnString[int(topaction[1:])])
                returnString = showDomainConstraintCall(returnString[int(topaction[1:])])
            else:
                if debug:
                    print(' ', returnString[int(topaction)])
                    returnString = browseFromIndexCall(returnString[int(topaction)])
                else:
                    returnString = browseFromIndexCall(returnString[int(topaction)])
            True
        except:
            True
