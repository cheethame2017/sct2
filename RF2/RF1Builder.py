from __future__ import print_function, unicode_literals
import io
import sqlite3
import time
import re
from shutil import copyfile
from SCTRF2config import pth, tok
from SCTRF2Funcs import tuplify
from SCTRF2Helpers import pterm, primAncestors_IN, PPSFromAncestors_IN, refSetTypeFromConId_IN, refSetMemberCountFromConId_IN, \
                        refSetFieldsFromConId_IN, refSetMembersFromConId_IN, simplify
from platformSpecific import cfgPathInstall

# needs a folder corresponding to:
# "/home/ed/Data/" (as rf1Path)
# and
# "/home/ed/RF1/" as the location of all the text files

# snapPath = "/home/ed/Code/RF2Snap.db"
snapPath = pth('RF2_SNAP')
# outPath = "/home/ed/Code/RF2Out.db"
outPath = pth('RF2_OUT')
# outPath = "/home/ed/Code/RF2Out.db"
rf1Path = "/home/ed/Data/RF1.db"

# conModules = '("900000000000207008","900000000000012004","999000011000000103")' # old
conModules = '("900000000000207008","900000000000012004","999000011000000103", "999000041000000102", "999000021000000109", "1326031000000103")' # now added uk edition and uk clinical refsets

clinRefsetModules = '("999000021000000109","705115006", "900000000000207008")'

headerDict = {"Concepts":("CONCEPTID", "CONCEPTSTATUS", "FULLYSPECIFIEDNAME", "CTV3ID", "SNOMEDID", "ISPRIMITIVE"),
                "Relationships":("RELATIONSHIPID", "CONCEPTID1", "RELATIONSHIPTYPE", "CONCEPTID2", "CHARACTERISTICTYPE", "REFINABILITY", "RELATIONSHIPGROUP"),
                "Descriptions":("DESCRIPTIONID", "DESCRIPTIONSTATUS", "CONCEPTID", "TERM", "INITIALCAPITALSTATUS", "DESCRIPTIONTYPE", "LANGUAGECODE"),
                "Subsets":("SUBSETID", "MEMBERID", "MEMBERSTATUS", "LINKEDID"),
                "SubsetNames":("SUBSETID", "SUBSETORIGINALID", "SUBSETVERSION", "SUBSETNAME", "SUBSETTYPE", "LANGUAGECODE", "REALMID", "CONTEXTID"),
                "History":("COMPONENTID", "RELEASEVERSION", "CHANGETYPE", "STATUS", "REASON"),
                "TextDefinitions":("CONCEPTID", "DEFINITION"),
                "RelLite":("CONCEPTID1", "CONCEPTID2"),
                "TC":("CONCEPTID1", "CONCEPTID2", "supertypeFeature"),
                "TC2":("CONCEPTID1", "CONCEPTID2", "supertypeFeature"),
                "DSK":("KEYWORD", "DESCRIPTIONID"),
                "DDK":("DUALKEY", "DESCRIPTIONID"),
                "CSK":("KEYWORD", "CONCEPTID"),
                "CDK":("DUALKEY", "CONCEPTID"),
                "DInLineSK":("DESCRIPTIONID", "KEYLINE"),
                "Timer":("EVENT", "TIME")}

conStatusDict = {"900000000000484002":"4", "900000000000482003":"2", "900000000000485001":"5",
                "900000000000486000":"6", "900000000000487009":"10", "723277005":"5",
                "900000000000483008":"3", "900000000000492006":"11", "1186917008":"6"}

descStatusDict = {"900000000000495008":"8", "900000000000482003":"2", "900000000000485001":"5",
                "900000000000486000":"6", "900000000000487009":"10", "723277005":"5", "723278000":"5",
                "900000000000494007":"7", "900000000000483008":"3", "900000000000492006":"11"}

isPrimitiveDict = {"900000000000074008":"1", "900000000000073002":"0"}

caseSignificanceDict = {"900000000000448009":"0", "900000000000017005":"1", "900000000000020002":"0"}

descTypeDict = {"900000000000003001":"3", "900000000000013009":"2"}

langSubTypeDict = {"900000000000549004900000000000003001":"3", "900000000000548007900000000000003001":"3", "900000000000548007900000000000013009":"1", "900000000000549004900000000000013009":"2"}

charTypeDict = {"900000000000227009":"3", "900000000000006009":"0", "900000000000011006":"0",
                "900000000000225001":"1", "900000000000010007":"0"}

historicalList = ("900000000000525002", "900000000000524003", "900000000000523009",
                "900000000000526001", "900000000000527005", "900000000000528000")

historicalRelDict = {"900000000000525002":"384598002", "900000000000524003":"370125004", "900000000000523009":"149016008",
                "900000000000526001":"370124000", "900000000000527005":"168666000", "900000000000528000":"159083000"}

historicalIsADict = {"1":"363661006", "2":"363662004", "3":"363663009",
                "4":"363660007", "5":"363664003", "6":"443559000", "10":"370126003"}

def fillTempTables():
    # CREATE TABLE ConInt (id text, active integer, term text, definitionStatusId text);
    # CREATE TABLE DescInt (id, active, conceptId, languageCode, typeId, term, caseSignificanceId, moduleId);
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath + "' as RF2Out")
    cursorIn2.execute("attach '" + rf1Path + "' as RF1")
    cursorIn2.execute("DROP TABLE IF EXISTS RF1.ConInt;")
    cursorIn2.execute("CREATE TABLE RF1.ConInt (id text, active integer, term text, definitionStatusId text);")
    cursorIn2.execute('INSERT INTO RF1.ConInt SELECT Concepts.id, Concepts.active, Descriptions.term, Concepts.definitionStatusId \
                        FROM Concepts, Descriptions, RF2Out.rdsrows \
                        WHERE Concepts.moduleId IN ' + conModules + 'AND \
                        (((Descriptions.conceptId)=Concepts.id) AND ((RF2Out.rdsrows.id)=Descriptions.id) AND ((Descriptions.typeId)="900000000000003001") AND Descriptions.active=1 AND (((RF2Out.rdsrows.acc)="900000000000548007") OR ((RF2Out.rdsrows.acc)="900000000000549004")));')
    connectionIn2.commit()
    # inactive descriptionstatuses
    cursorIn2.execute("DROP TABLE IF EXISTS RF1.DescStatus;")
    cursorIn2.execute("CREATE TABLE RF1.DescStatus (id text, status text);")
    cursorIn2.execute('INSERT INTO RF1.DescStatus SELECT cRefset.referencedComponentId, cRefset.ComponentId1 \
                        FROM cRefset \
                        WHERE (cRefset.active = 1) \
                        AND ((cRefset.refsetId)="900000000000490003");')
    connectionIn2.commit()
    # inactive conceptstatuses
    cursorIn2.execute("DROP TABLE IF EXISTS RF1.ConStatus;")
    cursorIn2.execute("CREATE TABLE RF1.ConStatus (id text, status text);")
    cursorIn2.execute('INSERT INTO RF1.ConStatus SELECT cRefset.referencedComponentId, cRefset.ComponentId1 \
                        FROM cRefset \
                        WHERE (cRefset.active = 1) \
                        AND ((cRefset.refsetId)="900000000000489007");')
    connectionIn2.commit()
    # ctv3ids
    cursorIn2.execute("DROP TABLE IF EXISTS RF1.CTV3Table;")
    cursorIn2.execute("CREATE TABLE RF1.CTV3Table (id text, CTV3 text);")
    cursorIn2.execute('INSERT INTO RF1.CTV3Table SELECT sRefset.referencedComponentId, sRefset.String1 \
                        FROM sRefset \
                        WHERE (sRefset.active = 1) \
                        AND ((sRefset.refsetId)="900000000000497000");')
    connectionIn2.commit()
    cursorIn2.close()
    connectionIn2.close()
    # reopen for indexes
    connectionIn2 = sqlite3.connect(rf1Path)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('CREATE INDEX DescStatus_Id_idx ON DescStatus (id);')
    cursorIn2.execute('CREATE INDEX DescStatus_Status_idx ON DescStatus (status);')
    cursorIn2.execute('CREATE INDEX ConStatus_Id_idx ON ConStatus (id);')
    cursorIn2.execute('CREATE INDEX ConStatus_Status_idx ON ConStatus (status);')
    cursorIn2.execute('CREATE INDEX CTV3Table_Id_idx ON CTV3Table (id);')
    cursorIn2.execute('CREATE INDEX CTV3Table_CTV3_idx ON CTV3Table (CTV3);')
    connectionIn2.commit()
    cursorIn2.close()
    connectionIn2.close()

def buildConcepts():
    # need to run fillconcepts first
    # too slow to do HASSUB calculation - can do later
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + rf1Path + "' as RF1")
    cursorIn2.execute('SELECT RF1.ConInt.id, RF1.ConInt.active, RF1.ConInt.term, RF1.ConInt.definitionStatusId \
                        FROM RF1.ConInt;')
    recordsetIn2 = cursorIn2.fetchall()
    SNOMEDID = ""
    counter = 0

    for thing in recordsetIn2:
        inactiveStatus = findInactiveConStatus(thing[0], thing[1])
        CTV3ID = FindCTV3ID(thing[0])
        if inactiveStatus in ("1", "2", "3", "4", "5", "6", "10"):
            # can refactor into another method
            tempListInactiveRels = ["", thing[0], "116680003", historicalIsADict[inactiveStatus], "0", "0", "0"]
            lineWriter("Relationships", tempListInactiveRels)
            tempListRels = [thing[0], historicalIsADict[inactiveStatus]]
            lineWriter("RelLite", tempListRels)
            tempListRels.append("1")
            # inactive TC chain:
            lineWriter("TC", tempListRels) # specific  inactive
            lineWriter("TC", [thing[0], "362955004", "1"]) # general inactive
            lineWriter("TC", [thing[0], "370115009", "1"]) # special
            lineWriter("TC", [thing[0], "138875005", "1"]) # root
            lineWriter("TC", [thing[0], thing[0], "1"]) # self

        tempListConcepts = [thing[0], inactiveStatus, thing[2], CTV3ID, SNOMEDID, isPrimitiveDict[thing[3]]]
        lineWriter("Concepts", tempListConcepts)
        if thing[1] == 1:
            tempListConcepts = [thing[0], inactiveStatus, simplify(thing[2]), CTV3ID, SNOMEDID, isPrimitiveDict[thing[3]]]
            lineWriter("GephiConcepts", tempListConcepts)
        counter += 1
        if counter % 50 == 0:
            print("Concepts", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildConceptKeys():
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + rf1Path + "' as RF1")
    cursorIn2.execute('SELECT RF1.ConInt.id FROM RF1.ConInt;')
    recordsetIn2 = cursorIn2.fetchall()
    counter = 0

    connectionIn1 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn1.cursor()

    for thing in recordsetIn2:
        # insert code to produce list of CK tokens
        cursorIn1.execute('SELECT unideterm FROM descriptionsdx WHERE conceptId=?;', (thing[0],))
        recordsetIn1 = cursorIn1.fetchall()
        if not recordsetIn1 == []:
            singleKeyList = []
            dualKeyList = []
            for termRow in recordsetIn1:
                uniDeTerm = termRow[0]
                tempSingleKeyList = []
                tempSingleKeyList = descSingleKey(re.split(' ', depunct(uniDeTerm)))
                singleKeyList.extend(tempSingleKeyList)
            singleKeyList = sorted(set(singleKeyList))
            dualKeyList = dualKey(singleKeyList)
            for key in singleKeyList:
                lineWriter("CSK", (key, thing[0]))
            for key in dualKeyList:
                lineWriter("CDK", (key, thing[0]))
        # end code to produce DK tokens
        counter += 1
        if counter % 50 == 0:
            print("ConceptKeys", str(counter))

    cursorIn2.close()
    connectionIn2.close()
    cursorIn1.close()
    connectionIn1.close()

def buildConceptsHistory():
    # greatly simplified = last date of active/inactive
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT Concepts.id, Concepts.effectiveTime, Concepts.active \
                        FROM Concepts \
                        WHERE Concepts.moduleId IN ' + conModules + ';')
    recordsetIn2 = cursorIn2.fetchall()
    counter = 0

    for thing in recordsetIn2:
        tempListConceptHistory = [thing[0], thing[1], "0", str(thing[2]), ""]
        lineWriter("History", tempListConceptHistory)
        counter += 1
        if counter % 50 == 0:
            print("Concept History", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def FindCTV3ID(inId):
    connectionIn1 = sqlite3.connect(snapPath)
    cursorIn1 = connectionIn1.cursor()
    cursorIn1.execute("attach '" + rf1Path + "' as RF1")
    cursorIn1.execute('SELECT RF1.CTV3Table.CTV3 \
                        FROM RF1.CTV3Table \
                        WHERE RF1.CTV3Table.id = ?;', (inId,))
    recordsetIn1 = cursorIn1.fetchone()
    if recordsetIn1:
        return recordsetIn1[0]
    else:
        return ""
    cursorIn1.close()
    connectionIn1.close()

def buildDescriptions():
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    # cursorIn2.execute("attach '" + rf1Path + "' as RF1")
    cursorIn2.execute('SELECT Descriptions.id, Descriptions.active, Descriptions.conceptId, Descriptions.languageCode, Descriptions.typeId, Descriptions.term, Descriptions.caseSignificanceId, Descriptions.effectiveTime \
                        FROM Descriptions, Concepts \
                        WHERE (Descriptions.conceptId=Concepts.id) \
                        AND Concepts.moduleId IN ' + conModules + ';')
                        # AND ((Descriptions.id) IN ("2618081014", "3083852010", "3526229013")) \
    recordsetIn2 = cursorIn2.fetchall()
    # connection for DK preparation
    connectionIn1 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn1.cursor()

    counter = 0
    for thing in recordsetIn2:
        inactiveStatus = findInactiveDescStatus(thing[0], thing[1])
        tempListDescriptions = [thing[0], inactiveStatus, thing[2], thing[5], caseSignificanceDict[thing[6]], descTypeDict[thing[4]], thing[3]]
        lineWriter("Descriptions", tempListDescriptions)
        tempListDescriptionHistory = [thing[0], thing[7], "0", str(thing[1]), ""]
        lineWriter("History", tempListDescriptionHistory)

        # insert code to produce list of DK tokens
        cursorIn1.execute('SELECT unideterm FROM descriptionsdx WHERE id=?;', (thing[0],))
        recordsetIn1 = cursorIn1.fetchall()
        if not recordsetIn1 == []:
            uniDeTerm = recordsetIn1[0][0]
            singleKeyList = []
            dualKeyList = []
            singleKeyList = descSingleKey(re.split(' ', depunct(uniDeTerm)))
            inLineSK = (" ".join(singleKeyList) + " |")[:255]
            dualKeyList = dualKey(singleKeyList)
            for key in singleKeyList:
                lineWriter("DSK", (key, thing[0]))
            for key in dualKeyList:
                lineWriter("DDK", (key, thing[0]))
            if inLineSK != " |":
                lineWriter("DInLineSK", (thing[0], inLineSK))
        # end code to produce DK tokens

        counter += 1
        if counter % 50 == 0:
            print("Descriptions", str(counter))

    cursorIn2.close()
    connectionIn2.close()
    cursorIn1.close()
    connectionIn1.close()

def buildLangSubset():
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath + "' as RF2Out")
    cursorIn2.execute('SELECT RF2Out.rdsrows.id , RF2Out.rdsrows.acc, Descriptions.typeId \
                        FROM RF2Out.rdsrows, Descriptions \
                        WHERE Descriptions.active = 1 \
                        AND (Descriptions.id=RF2Out.rdsrows.id);')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListLangSubset = ["", thing[0], langSubTypeDict[str(thing[1])+str(thing[2])], ""]
        # catch acceptable FSNs
        if str(thing[1])+str(thing[2]) == "900000000000549004900000000000003001":
            lineWriter("Timer", [thing[0] + ": acceptable FSN", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        lineWriter("LangSubset", tempListLangSubset)
        counter += 1
        if counter % 50 == 0:
            print("LangSubset", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildLangSubsetOld():
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT cRefset.referencedComponentId, cRefset.ComponentId1, Descriptions.typeId \
                        FROM cRefset, Descriptions \
                        WHERE cRefset.refsetId IN ("900000000000508004", "999001251000000103") \
                        AND cRefset.active = 1 \
                        AND Descriptions.active = 1 \
                        AND (Descriptions.id=cRefset.referencedComponentId);')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListLangSubset = ["", thing[0], langSubTypeDict[str(thing[1])+str(thing[2])], ""]
        # catch acceptable FSNs
        if str(thing[1])+str(thing[2]) == "900000000000549004900000000000003001":
            lineWriter("Timer", [thing[0] + ": acceptable FSN", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        lineWriter("LangSubsetOld", tempListLangSubset)
        counter += 1
        if counter % 50 == 0:
            print("LangSubsetOld", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildDefinitions():
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT Definitions.id, Definitions.conceptId, Definitions.term \
                        FROM Definitions, cRefset \
                        WHERE (((Definitions.id)=cRefset.referencedComponentId) \
                        AND Definitions.active = 1\
                        AND ((cRefset.ComponentId1)="900000000000548007") \
                        AND ((cRefset.refsetId)="900000000000508004"));')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListDefSubset = [thing[1], thing[2]]
        lineWriter("TextDefinitions", tempListDefSubset)
        counter += 1
        if counter % 50 == 0:
            print("TextDefinitions", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildRelationships():
    # need stated
    # NEED TO: eliminate rels where destinationId or typeId are in pharmacy module
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT Relationships.id, Relationships.active, Relationships.sourceId, \
                        Relationships.typeId, Relationships.destinationId, \
                        Relationships.relationshipGroup, Relationships.characteristicTypeId, Concepts.moduleId \
                        FROM Relationships, Concepts \
                        WHERE Concepts.moduleId IN ' + conModules + ' \
                        AND Relationships.moduleId IN ' + conModules + ' \
                        AND Relationships.active = 1 \
                        AND Concepts.id = Relationships.sourceId;')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListRelationships = [thing[0], thing[2], thing[3], thing[4], charTypeDict[thing[6]], "0", str(thing[5])]
        lineWriter("Relationships", tempListRelationships)
        counter += 1
        if counter % 50 == 0:
            print("Relationships", str(counter))

    cursorIn2.close()

def buildRGNodes():
    groupableDict = buildGroupableDict()
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn2.cursor()
    cursorIn2.execute('SELECT DISTINCT Relationships.sourceId, \
                        Relationships.relationshipGroup, Concepts.moduleId \
                        FROM Relationships, Concepts \
                        WHERE Concepts.moduleId IN ' + conModules + ' \
                        AND Relationships.moduleId IN ' + conModules + ' \
                        AND Relationships.active = 1 \
                        AND Concepts.id = Relationships.sourceId;')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        if thing[1] != 0:
            # write RGnode
            tempListRGNodes = [thing[0] + "_" + str(thing[1]), "0", thing[0] + "_" + str(thing[1]), "", "", "1"]
            lineWriter("GephiConcepts", tempListRGNodes)
            # write concept to RGnode relationship
            # tempListRelationships = ["", thing[0], "609096000", thing[0] + "_" + str(thing[1]), "0", "0", str(thing[1])]
            tempListRelationships = ["", thing[0], ".", thing[0] + "_" + str(thing[1]), "0", "0", str(thing[1])]
            lineWriter("GephiRelationships", tempListRelationships)
        else: #RG=0
            # look up rels table again on sourceId, relationshipGroup and active - return a set of typeIds (most likely only one)
            # if any have a groupableDict value of 1 then add a node and a source->node edge, otherwise don't do anything
            # PLACEHOLDER
            cursorIn1.execute('SELECT Relationships.typeId \
                        FROM Relationships \
                        WHERE Relationships.sourceId = ? \
                        AND Relationships.relationshipGroup = 0 \
                        AND Relationships.active = 1', (thing[0],))
            myList = cursorIn1.fetchall()
            for typeId in myList:
                if not typeId[0] == '116680003':
                    if typeId[0] in groupableDict: # by omitting the concepts join might pick up some pharmacy attributes, which aren't in the MRCM refset.
                        if groupableDict[typeId[0]] == 1:
                            # write RGnode
                            tempListRGNodes = [thing[0] + "_0", "0", thing[0] + "_0", "", "", "1"]
                            lineWriter("GephiConcepts", tempListRGNodes)
                            # write concept to RGnode relationship
                            # tempListRelationships = ["", thing[0], "609096000", typeId[0] + "_0", "0", "0", "0"]
                            tempListRelationships = ["", thing[0], ".", typeId[0] + "_0", "0", "0", "0"]
                            lineWriter("GephiRelationships", tempListRelationships)
        counter += 1
        if counter % 50 == 0:
            print("RGNodes", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildRGRels():
    groupableDict = buildGroupableDict()
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT Relationships.id, Relationships.active, Relationships.sourceId, \
                        Relationships.typeId, Relationships.destinationId, \
                        Relationships.relationshipGroup, Relationships.characteristicTypeId, Concepts.moduleId \
                        FROM Relationships, Concepts \
                        WHERE Concepts.moduleId IN ' + conModules + ' \
                        AND Relationships.moduleId IN ' + conModules + ' \
                        AND Relationships.active = 1 \
                        AND Concepts.id = Relationships.sourceId;')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        # write RGnode to target relationship
        if thing[3] == '116680003': # if an is-a, just add a direct relationship
            # tempListRelationships = [thing[0], thing[2], thing[3], thing[4], charTypeDict[thing[6]], "0", str(thing[5])]
            tempListRelationships = [thing[0], thing[2], "", thing[4], charTypeDict[thing[6]], "0", str(thing[5])]
        else:
            if groupableDict[thing[3]] == 1: # regardless of RG number, if groupable add the outgoing RGnode -> att + value row
                tempListRelationships = [thing[0], thing[2] + "_" + str(thing[5]), simplify(pterm(thing[3])), thing[4], charTypeDict[thing[6]], "0", str(thing[5])]
            else: # if not groupable, just add a direct relationship
                tempListRelationships = [thing[0], thing[2], simplify(pterm(thing[3])), thing[4], charTypeDict[thing[6]], "0", str(thing[5])]
        lineWriter("GephiRelationships", tempListRelationships)
        counter += 1
        if counter % 50 == 0:
            print("RGRels", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildGroupableDict():
    groupableDict = {}
    tempIdList = []
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    # ungroupable
    cursorIn2.execute('select distinct referencedComponentId from cissccrefset where active=1 and integer1=0;')
    tempIdList = cursorIn2.fetchall()
    for tempId in tempIdList:
        groupableDict[tempId[0]] = 0
    # groupable
    cursorIn2.execute('select distinct referencedComponentId from cissccrefset where active=1 and integer1=1;')
    tempIdList = cursorIn2.fetchall()
    for tempId in tempIdList:
        groupableDict[tempId[0]] = 1
    cursorIn2.close()
    connectionIn2.close()
    return groupableDict

def buildSimpleRefsets():
    # 999000021000000109 (Clinical), 900000000000207008 (Core), 705115006 (Tech preview), 715515008 (SCT/LOINC)
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT SimpleRefset.refsetId, SimpleRefset.referencedComponentId \
                        FROM SimpleRefset \
                        WHERE (((SimpleRefset.active)="1")) \
                        AND SimpleRefset.moduleId NOT IN ("999000021000001108","999000031000000106");')
                        # ,"999000031000000106") part of exclusion to remove huge device refset (which should contain loads of products)
                        # suspect this refset is no longer in the UK Edition refset module.
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListSimpleRefset = [thing[0], thing[1], "", ""]
        lineWriter("AllSubsets", tempListSimpleRefset)
        counter += 1
        if counter % 50 == 0:
            print("AllSubsets", str(counter))

    cursorIn2.close()
    connectionIn2.close()


def buildSimpleRefsetNames():
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath + "' as RF2Out")
    cursorIn2.execute('SELECT DISTINCT SimpleRefset.refsetId, Descriptions.term \
                        FROM SimpleRefset, Descriptions, RF2Out.rdsrows \
                        WHERE (((SimpleRefset.refsetId)=Descriptions.conceptId) \
                        AND ((SimpleRefset.active)="1") \
                        AND ((Descriptions.id)=RF2Out.rdsrows.id) \
                        AND ((Descriptions.typeId)="900000000000003001") \
                        AND ((RF2Out.rdsrows.acc)="900000000000548007")) \
                        AND SimpleRefset.moduleId NOT IN ("999000021000001108","999000031000000106") \
                        ORDER BY Descriptions.term;')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListSimpleRefsetNames = [thing[0], thing[0], "1", thing[1], "2", "en", "0", "0"]
        lineWriter("SubsetNames", tempListSimpleRefsetNames)
        counter += 1
        if counter % 50 == 0:
            print("SubsetNames", str(counter))


    cursorIn2.close()
    connectionIn2.close()

def buildRoleTable():
    # need stated
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT Relationships.id, Relationships.active, Relationships.sourceId, \
                        Relationships.typeId, Relationships.destinationId, \
                        Relationships.relationshipGroup, Relationships.characteristicTypeId, Concepts.moduleId \
                        FROM Relationships, Concepts \
                        WHERE Relationships.typeId <> "116680003" \
                        AND Relationships.active = 1 \
                        AND Concepts.moduleId IN ' + conModules + ' \
                        AND Relationships.moduleId IN ' + conModules + ' \
                        AND Concepts.id = Relationships.sourceId \
                        AND Relationships.characteristicTypeId IN ("900000000000006009","900000000000011006","900000000000010007");')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListRelationships = [thing[0], thing[2], thing[3], thing[4], charTypeDict[thing[6]], "0", str(thing[5])]
        lineWriter("RoleTable", tempListRelationships)
        counter += 1
        if counter % 50 == 0:
            print("RoleTable", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildRelLite():
    # need stated
    # NEED TO: add in the inactive concept isas
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT Relationships.sourceId, Relationships.destinationId, Concepts.moduleId \
                        FROM Relationships, Concepts \
                        WHERE Relationships.typeId = "116680003" \
                        AND Concepts.moduleId IN ' + conModules + ' \
                        AND Relationships.moduleId IN ' + conModules + ' \
                        AND Concepts.id = Relationships.sourceId \
                        AND Relationships.active = 1;')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListRels = [thing[0], thing[1]]
        lineWriter("RelLite", tempListRels)
        counter += 1
        if counter % 50 == 0:
            print("RelLite", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildTC():
    # need stated
    # NEED TO: add in the inactive concept isas
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath + "' as RF2Out")
    cursorIn2.execute('SELECT RF2Out.tc.subtypeId, RF2Out.tc.supertypeId, Csup.definitionStatusId \
                        FROM RF2Out.tc, Concepts as Csup, Concepts as Csub \
                        WHERE Csup.moduleId IN ' + conModules + ' \
                        AND Csub.moduleId IN ' + conModules + ' \
                        AND Csup.id = RF2Out.tc.supertypeId \
                        AND Csub.id = RF2Out.tc.subtypeId;')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListRels = [thing[0], thing[1], isPrimitiveDict[thing[2]]]
        lineWriter("TC", tempListRels)
        counter += 1
        if counter % 5000 == 0:
            print("TC", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def buildTC2():
    connectionIn2 = sqlite3.connect(outPath)
    connectionIn1 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn1 = connectionIn1.cursor()
    cursorIn2.execute("attach '" + rf1Path + "' as RF1")

    # primitive concepts self-PPS (from RF2Out TC where sup=sub)
    cursorIn2.execute("SELECT RF1.ConInt.id FROM RF1.ConInt WHERE RF1.ConInt.active=1 AND RF1.ConInt.definitionStatusId='900000000000074008';")
    recordsetIn2 = cursorIn2.fetchall()
    tot = len(recordsetIn2)
    counter = 0
    for thing in recordsetIn2:
        cursorIn1.execute("SELECT tc.supertypeId FROM tc WHERE tc.subtypeId=?", (thing[0],))
        recordsetIn1 = cursorIn1.fetchall()
        ancestorList = []
        for thing2 in recordsetIn1:
            ancestorList.append(tuplify(thing2[0]))
        # all primitive ancestors
        primAncestors = primAncestors_IN(ancestorList)
        for thing3 in ancestorList:
            if thing3 == thing:
                supertypeFeature = "2"
            elif thing3 in primAncestors:
                supertypeFeature = "1"
            else:
                supertypeFeature = "0"
            tempListRels = [thing[0], thing3[0], supertypeFeature]
            lineWriter("TC2", tempListRels)
        counter += 1
        if counter % 100 == 0:
            print("TC2-prim", str(counter), str(tot))
    # defined concepts
    cursorIn2.execute("SELECT RF1.ConInt.id FROM RF1.ConInt WHERE RF1.ConInt.definitionStatusId='900000000000073002';")
    recordsetIn2 = cursorIn2.fetchall()
    tot = len(recordsetIn2)
    counter = 0
    for thing in recordsetIn2:
        cursorIn1.execute("SELECT tc.supertypeId FROM tc WHERE tc.subtypeId=?", (thing[0],))
        recordsetIn1 = cursorIn1.fetchall()
        ancestorList = []
        for thing2 in recordsetIn1:
            ancestorList.append(tuplify(thing2[0]))
        # PPSs
        PPSAncestorList = PPSFromAncestors_IN(ancestorList)
        # all primitive ancestors
        primAncestors = primAncestors_IN(ancestorList)
        # remaining primitive ancestors
        primAncestorList = list(set(primAncestors) - set(PPSAncestorList))
        for thing3 in ancestorList:
            if thing3 in PPSAncestorList:
                supertypeFeature = "2"
            elif thing3 in primAncestorList:
                supertypeFeature = "1"
            else:
                supertypeFeature = "0"
            tempListRels = [thing[0], thing3[0], supertypeFeature]
            lineWriter("TC2", tempListRels)
        counter += 1
        if counter % 100 == 0:
            print("TC2-defd", str(counter), str(tot))

    cursorIn1.close()
    connectionIn1.close()
    cursorIn2.close()
    connectionIn2.close()

def buildAdditionalTable():
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT Relationships.id, Relationships.active, Relationships.sourceId, \
                        Relationships.typeId, Relationships.destinationId, \
                        Relationships.relationshipGroup, Relationships.characteristicTypeId, Concepts.moduleId \
                        FROM Relationships, Concepts \
                        WHERE Relationships.typeId <> "116680003" \
                        AND Relationships.active = 1 \
                        AND Concepts.moduleId IN ' + conModules + ' \
                        AND Relationships.moduleId IN ' + conModules + ' \
                        AND Concepts.id = Relationships.sourceId \
                        AND Relationships.characteristicTypeId = "900000000000227009";')
    recordsetIn2 = cursorIn2.fetchall()

    counter = 0
    for thing in recordsetIn2:
        tempListRelationships = [thing[0], thing[2], thing[3], thing[4], charTypeDict[thing[6]], "0", str(thing[5])]
        lineWriter("AdditionalTable", tempListRelationships)
        counter += 1
        if counter % 50 == 0:
            print("AdditionalTable", str(counter))

    cursorIn2.close()
    connectionIn2.close()

def testForSubs(inId, inStatus):
    if inStatus == 1:
        connectionIn2 = sqlite3.connect(snapPath)
        cursorIn2 = connectionIn2.cursor()
        cursorIn2.execute('SELECT Relationships.id \
                            FROM Relationships \
                            WHERE (((Relationships.active)=1) AND ((Relationships.destinationId=?) \
                            AND ((Relationships.typeId)="116680003")));', (inId,))
        recordsetIn2 = cursorIn2.fetchone()
        if recordsetIn2:
            # calculate correspondence
            return "+"
        else:
            return ""
        cursorIn2.close()
        connectionIn2.close()
    else:
        return ""

def buildHistoricalRelationships():
    # add to all relationships and to historical relationships
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    for relType in historicalList:
        # NEED TO: eliminate any pharmacy content (need modules of referencedComponentId)
        # cursorIn2.execute('SELECT crefset.referencedComponentId, crefset.ComponentId1 \
        #                     FROM crefset, Concepts \
        #                     WHERE crefset.refsetid = ? \
        #                     AND crefset.active = 1 \
        #                     AND crefset.referencedComponentId = Concepts.id \
        #                     AND Concepts.moduleId IN ' + conModules + ';', (relType,))
        # BETTER:
        cursorIn2.execute('SELECT crefset.referencedComponentId, crefset.ComponentId1 \
                            FROM crefset \
                            WHERE crefset.refsetid = ? \
                            AND crefset.active = 1 \
                            AND crefset.moduleId IN ' + conModules + ';', (relType,))
        recordsetIn2 = cursorIn2.fetchall()

        counter = 0
        for thing in recordsetIn2:
            tempListHistRelationships = ["", thing[0], historicalRelDict[relType], thing[1], "2", "0", "0"]
            lineWriter("HistoryReferenceTable", tempListHistRelationships)
            lineWriter("Relationships", tempListHistRelationships)
            counter += 1
            if counter % 50 == 0:
                print("HistoryReferenceTable", str(counter), str(relType))

    cursorIn2.close()
    connectionIn2.close()

def findInactiveConStatus(inId, inStatus):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + rf1Path + "' as RF1")
    cursorIn2.execute('SELECT RF1.ConStatus.status \
                        FROM RF1.ConStatus \
                        WHERE RF1.ConStatus.id = ?;', (inId,))
    recordsetIn2 = cursorIn2.fetchone()
    if recordsetIn2:
        # calculate correspondence
        return conStatusDict[recordsetIn2[0]]
    else:
        # return reason not stated
        if inStatus == 1:
            return "0"
        else:
            return "1"
    cursorIn2.close()
    connectionIn2.close()

def findInactiveDescStatus(inId, inStatus):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + rf1Path + "' as RF1")
    cursorIn2.execute('SELECT RF1.DescStatus.status \
                        FROM RF1.DescStatus \
                        WHERE RF1.DescStatus.id = ?;', (inId,))
    recordsetIn2 = cursorIn2.fetchone()
    if recordsetIn2:
        # calculate correspondence
        return descStatusDict[recordsetIn2[0]]
    else:
        # return reason not stated
        if inStatus == 1:
            return "0"
        else:
            return "1"
    cursorIn2.close()
    connectionIn2.close()

def createFiles():
    headerWriter("Timer", "Timer")
    headerWriter("Concepts", "Concepts")
    headerWriter("GephiConcepts", "Concepts")
    headerWriter("Descriptions", "Descriptions")
    headerWriter("Relationships", "Relationships")
    headerWriter("GephiRelationships", "Relationships")
    headerWriter("AllSubsets", "Subsets")
    headerWriter("RelLite", "RelLite")
    headerWriter("LangSubset", "Subsets")
    headerWriter("LangSubsetOld", "Subsets")
    headerWriter("RoleTable", "Relationships")
    headerWriter("AdditionalTable", "Relationships")
    headerWriter("RoleTable", "Relationships")
    headerWriter("SubsetNames", "SubsetNames")
    headerWriter("History", "History")
    headerWriter("HistoryReferenceTable", "Relationships")
    headerWriter("TextDefinitions", "TextDefinitions")
    headerWriter("TC", "TC")
    headerWriter("DSK", "DSK")
    headerWriter("DDK", "DDK")
    headerWriter("CSK", "CSK")
    headerWriter("CDK", "CDK")
    headerWriter("DInLineSK", "DInLineSK")
    # headerWriter("TC2", "TC2")

def headerWriter(fileName, filePattern):
    outputFile = io.open("/home/ed/RF1/" + fileName + ".txt", "w", encoding="UTF-8")
    outputFile.write("\t".join(headerDict[filePattern]))
    outputFile.write("\n")
    outputFile.close()

def lineWriter(fileName, lineList):
    outputFile = io.open("/home/ed/RF1/" + fileName + ".txt", "a", encoding="UTF-8")
    outputFile.write("\t".join(lineList))
    outputFile.write("\n")
    outputFile.close()

def depunct(inString):
    a = ".,/=-+><^?@~{}[]'\"*():;#"
    for b in range(0, len(a)):
        inString = inString.replace(a[b], " ")
    # # optional crunch hyphen
    # inStringHyphen = inString.replace("-", "")
    # inString = inString + " " + inStringHyphen
    return inString

def deQuote(inString):
    a = "'\""
    for b in range(0, len(a)):
        inString = inString.replace(a[b], " ")
    return inString

def descSingleKey(inList):
    RF1Stops = ['ABOUT', 'ALONGSID', 'AN', 'AND', 'ANYTHING', 'AROUND', 'AS', 'AT', 'BECAUSE', 'BEFORE',
    'BEING', 'BOTH', 'BY', 'CHRONICA', 'CONSISTS', 'COVERED', 'DOES', 'DURING', 'EVERY', 'FINDS', 'FOR', 'FROM',
    'IN', 'INSTEAD', 'INTO', 'MORE', 'MUST', 'OF', 'ON', 'ONLY', 'OR', 'PROPERLY', 'SIDE', 'SIDED', 'SOME', 'SOMETHIN',
    'SPECIFIC', 'THAN', 'THAT', 'THE', 'THINGS', 'THIS', 'THROUGHO', 'TO', 'UP', 'USING', 'USUALLY', 'WHEN', 'WHILE']
    outList = []
    for thing in inList:
        if not thing.isnumeric():
            if (len(thing) > 1) and not thing[:8] in RF1Stops:
                outList.append(thing[:8])
    outList = sorted(set(outList))
    return outList

def dualKey(inList):
    preOutList = []
    outList = []
    for thing in inList:
        if not thing.isnumeric():
            if len(thing) > 2:
                preOutList.append(thing[:3])
    preOutList = sorted(set(preOutList))
    counter = 1
    for thing1 in preOutList:
        for thing2 in preOutList[counter:]:
            outList.append(thing1 + thing2)
        counter += 1
    return outList

def conSingleKey(inList):
    RF1Stops = ['ABOUT', 'ALONGSID', 'AN', 'AND', 'ANYTHING', 'AROUND', 'AS', 'AT', 'BECAUSE', 'BEFORE',
    'BEING', 'BOTH', 'BY', 'CHRONICA', 'CONSISTS', 'COVERED', 'DOES', 'DURING', 'EVERY', 'FINDS', 'FOR', 'FROM',
    'IN', 'INSTEAD', 'INTO', 'MORE', 'MUST', 'OF', 'ON', 'ONLY', 'OR', 'PROPERLY', 'SIDE', 'SIDED', 'SOME', 'SOMETHIN',
    'SPECIFIC', 'THAN', 'THAT', 'THE', 'THINGS', 'THIS', 'THROUGHO', 'TO', 'UP', 'USING', 'USUALLY', 'WHEN', 'WHILE']
    outList = []
    for term in inList:
        for thing in term:
            if not(thing.isnumeric()):
                if (len(thing) > 1) and not(thing[:8] in RF1Stops):
                    outList.append(thing[:8])
    outList = sorted(set(outList))
    return outList

def buildModelTable(refsetId, CardOp):
    if CardOp:
        f = io.open('/home/ed/RF1/modelTableCO.txt', 'w', encoding="UTF-8")
    else:
        f = io.open('/home/ed/RF1/modelTable.txt', 'w', encoding="UTF-8")
    fList = []
    attList = []
    attDomList = []
    myRefset = refSetTypeFromConId_IN(refsetId)
    for refset in myRefset:
        refsetType = refset[0]
    myCount = refSetMemberCountFromConId_IN(refsetId, refsetType)
    myfilter = 0
    myValueRows = refSetMembersFromConId_IN(refsetId, refsetType, myfilter, myCount)
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    myRefsetType = 'c' + refSetTypeFromConId_IN(refsetId)[0][0]  # if component datatype (can't pick up which one directly)
    for value in myValueRows:
        counter = 0
        domainTemp = ""
        for row in myDescRows:
            tempRows = []
            if (myRefsetType[counter] == 'c') and (row[0] == "Referenced component"):
                domainTemp = value[counter]
            else:
                if row[1] == tok("PARS_STR") and (row[0] == "Domain template for precoordination"):
                    if CardOp:
                        tempRows = cleanForModelTableCardOp(value[row[2]], domainTemp)
                        lastAtt = ""
                        lastCard = ""
                        for row in tempRows[1:]:
                            splitRow = []
                            splitRow = row.split("\t")
                            if (len(splitRow) == 5):
                                if not(splitRow[1] == "REPLACEATT"):
                                    lastAtt = splitRow[2].split("|")[0]
                                    lastCard = splitRow[1]
                                    print(splitRow[0], splitRow[1], splitRow[2], splitRow[3], splitRow[4])
                                    fLine = "\t".join([splitRow[0], pterm(splitRow[0].strip()), splitRow[1], splitRow[2].split("|")[0], pterm(splitRow[2].split("|"[0].strip())), splitRow[3], splitRow[4].split("|")[0], pterm(splitRow[4].split("|"[0].strip()))]) + '\n'
                                    if fLine not in fList:
                                        fList.append(fLine)
                                else:
                                    print(splitRow[0], lastCard, lastAtt, splitRow[3], splitRow[4])
                                    fLine = "\t".join([splitRow[0], pterm(splitRow[0].strip()), lastCard, lastAtt, pterm(lastAtt.strip()), splitRow[3], splitRow[4].split("|")[0], pterm(splitRow[4].split("|"[0].strip()))]) + '\n'
                                    if fLine not in fList:
                                        fList.append(fLine)
                    else:
                        tempRows = cleanForModelTable(value[row[2]], domainTemp)
                        lastAtt = ""
                        for row in tempRows[1:]:
                            splitRow = []
                            splitRow = row.split("\t")
                            if len(splitRow) == 3:
                                if not(splitRow[1] == "REPLACEATT"):
                                    lastAtt = splitRow[1].split("|")[0]
                                    print("\t".join([splitRow[0], pterm(splitRow[0].strip()), splitRow[1].split("|")[0], pterm(splitRow[1].split("|"[0].strip())), splitRow[2].split("|")[0], pterm(splitRow[2].split("|"[0].strip()))]))
                                    attList.append(splitRow[1].split("|")[0].strip())
                                    attDomList.append(splitRow[0] + "|" + splitRow[1].split("|")[0].strip())
                                    fLine = "\t".join([splitRow[0], pterm(splitRow[0].strip()), splitRow[1].split("|")[0], pterm(splitRow[1].split("|"[0].strip())), splitRow[2].split("|")[0], pterm(splitRow[2].split("|"[0].strip()))]) + '\n'
                                    if fLine not in fList:
                                        fList.append(fLine)
                                else:
                                    print("\t".join([splitRow[0], pterm(splitRow[0].strip()), lastAtt, pterm(lastAtt.strip()), splitRow[2].split("|")[0], pterm(splitRow[2].split("|"[0].strip()))]))
                                    fLine = "\t".join([splitRow[0], pterm(splitRow[0].strip()), lastAtt, pterm(lastAtt.strip()), splitRow[2].split("|")[0], pterm(splitRow[2].split("|"[0].strip()))]) + '\n'
                                    if fLine not in fList:
                                        fList.append(fLine)
            counter += 1
    for line in fList:
        f.write(line)
    f.close()
    if not(CardOp):
        attList = list(set(attList))
        a = io.open('/home/ed/RF1/attTable.txt', 'w', encoding="UTF-8")
        for att in attList:
            a.write("\t".join([att, pterm(att)]) + "\n")
        a.close()
        attDomList = list(set(attDomList))
        a = io.open('/home/ed/RF1/attDomTable.txt', 'w', encoding="UTF-8")
        for att in attDomList:
            a.write("\t".join([att.split("|")[0], pterm(att.split("|")[0]), att.split("|")[1], pterm(att.split("|")[1])]) + "\n")
        a.close()


def cleanForModelTableCardOp(inString, inDomain):
    outList = []
    # break templates on cardinality
    outString = re.sub(r"(\[\[[0|1|*]\.\.[0|1|*]\]\] )", r"\nREPLACEME\t\1", inString)
    # break lists of concept constraints at OR clauses
    outString = re.sub(r"(OR [<<|<|\d])", r"\nREPLACEME\tREPLACEATT\t\1", outString)
    # break lists of concept constraints at cardinality
    outString = re.sub(r"(\[[0|1|*]..[0|1|*]\] )", r"\nREPLACEME\t\1", outString)
    # insert domain
    outString = outString.replace(r"REPLACEME", inDomain)
    # remove [[0..1]]
    outString = outString.replace(r"[[0..1]] ", "0..1\t")
    # remove [[0..*]]
    outString = outString.replace(r"[[0..*]] ", "0..*\t")
    # remove [[1..*]]
    outString = outString.replace(r"[[1..*]] ", "1..*\t")
    # remove [[1..1]]
    outString = outString.replace(r"[[1..1]] ", "1..1\t")
    # remove  = [[+id(<
    outString = outString.replace(r" = [[+id(< ", "\t<\t")
    # remove  = [[+id(<<
    outString = outString.replace(r" = [[+id(<< ", "\t<<\t")
    # remove  = [[+id(<
    outString = outString.replace(r" = [[+id(<", "\t<\t")
    # remove  = [[+id(
    outString = outString.replace(r" = [[+id(", "\t=\t")
    # remove  = )]],
    outString = outString.replace(r")]],", "")
    # remove  = )]] }
    outString = outString.replace(r")]] }", "")
    # remove  = ))]]
    outString = outString.replace(r"))]]", "")
    # remove  = |)
    outString = outString.replace(r"|)", "|")
    # remove  = OR <<
    outString = outString.replace(r"OR << ", "\t<<\t")
    # remove  = OR <<
    outString = outString.replace(r"OR < ", "\t<\t")
    # remove  = OR
    outString = outString.replace(r"OR ", "\t=\t")
    # remove [0..0] 408729009 |Finding context| = *,
    outString = outString.replace(r"[0..0] 408729009 |Finding context| = *,", "")
    # remove [0..0] 246090004 |Associated finding| = *):
    outString = outString.replace(r"[0..0] 246090004 |Associated finding| = *):", "")
    # remove [0..0] 408730004 |Procedure context| = *,
    outString = outString.replace(r"[0..0] 408730004 |Procedure context| = *,", "")
    # remove [0..0] 363589002 |Associated procedure| = *):
    outString = outString.replace(r"[0..0] 363589002 |Associated procedure| = *):", "")
    outList = outString.split('\n')
    return outList

def cleanForModelTable(inString, inDomain):
    outList = []
    # break templates on cardinality
    outString = re.sub(r"(\[\[[0|1|*]\.\.[0|1|*]\]\] )", r"\nREPLACEME\t\1", inString)
    # break lists of concept constraints at OR clauses
    outString = re.sub(r"(OR [<<|<|\d])", r"\nREPLACEME\tREPLACEATT\t\1", outString)
    # break lists of concept constraints at cardinality
    outString = re.sub(r"(\[[0|1|*]..[0|1|*]\] )", r"\nREPLACEME\t\1", outString)
    # insert domain
    outString = outString.replace(r"REPLACEME", inDomain)
    # remove [[0..1]]
    outString = outString.replace(r"[[0..1]] ", "")
    # remove [[0..*]]
    outString = outString.replace(r"[[0..*]] ", "")
    # remove [[1..*]]
    outString = outString.replace(r"[[1..*]] ", "")
    # remove [[1..1]]
    outString = outString.replace(r"[[1..1]] ", "")
    # remove  = [[+id(<
    outString = outString.replace(r" = [[+id(< ", "\t")
    # remove  = [[+id(<<
    outString = outString.replace(r" = [[+id(<< ", "\t")
    # remove  = [[+id(<
    outString = outString.replace(r" = [[+id(<", "\t")
    # remove  = [[+id(
    outString = outString.replace(r" = [[+id(", "\t")
    # remove  = )]],
    outString = outString.replace(r")]],", "")
    # remove  = )]] }
    outString = outString.replace(r")]] }", "")
    # remove  = ))]]
    outString = outString.replace(r"))]]", "")
    # remove  = |)
    outString = outString.replace(r"|)", "|")
    # remove  = OR <<
    outString = outString.replace(r"OR << ", "")
    # remove  = OR <
    outString = outString.replace(r"OR < ", "")
    # remove  = OR
    outString = outString.replace(r"OR ", "")
    # remove [0..0] 408729009 |Finding context| = *,
    outString = outString.replace(r"[0..0] 408729009 |Finding context| = *,", "")
    # remove [0..0] 246090004 |Associated finding| = *):
    outString = outString.replace(r"[0..0] 246090004 |Associated finding| = *):", "")
    # remove [0..0] 408730004 |Procedure context| = *,
    outString = outString.replace(r"[0..0] 408730004 |Procedure context| = *,", "")
    # remove [0..0] 363589002 |Associated procedure| = *):
    outString = outString.replace(r"[0..0] 363589002 |Associated procedure| = *):", "")
    outList = outString.split('\n')
    return outList

# # experiments with neo4j import
# createFiles()
# buildRGNodes()
# buildRGRels()

# then need to import the outputs of these into RF1, export the relationships to a suitable csv (with [simplified] role name terms added)
# and export the nodes unioned with *active* concepts from concepts table (with [simplified] node names) as suitable csv.

def processSteps(fullRun):
    if fullRun: # build full RF1 set of tables
        createFiles()
        lineWriter("Timer", ["Start/fillTempTables", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        fillTempTables()
        lineWriter("Timer", ["buildConcepts", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildConcepts()
        lineWriter("Timer", ["buildDescriptions", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildDescriptions() # and description key tables
        lineWriter("Timer", ["buildConceptKeys", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildConceptKeys()
        lineWriter("Timer", ["buildConceptsHistory", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildConceptsHistory()
        lineWriter("Timer", ["buildLangSubset", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildLangSubset()
        lineWriter("Timer", ["buildLangSubsetOld", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildLangSubsetOld()
        lineWriter("Timer", ["buildDefinitions", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildDefinitions()
        lineWriter("Timer", ["buildRelationships", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRelationships()
        lineWriter("Timer", ["buildRGNodes", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRGNodes()
        lineWriter("Timer", ["buildRGRels", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRGRels()
        lineWriter("Timer", ["buildRelLite", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRelLite()
        lineWriter("Timer", ["buildTC", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildTC()
        lineWriter("Timer", ["buildSimpleRefsets", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildSimpleRefsets()
        lineWriter("Timer", ["buildSimpleRefsetNames", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildSimpleRefsetNames()
        lineWriter("Timer", ["buildHistoricalRelationships", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildHistoricalRelationships()
        lineWriter("Timer", ["buildRoleTable", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRoleTable()
        lineWriter("Timer", ["buildAdditionalTable", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildAdditionalTable()
        lineWriter("Timer", ["buildModelTable", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildModelTable("723560006", True)
        buildModelTable("723560006", False)
        lineWriter("Timer", ["End", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])

        # copy files back to main path
        copyfile("/home/ed/RF1/modelTable.txt", cfgPathInstall() + "files/dot/modelTable.txt")
        copyfile("/home/ed/RF1/modelTableCO.txt", cfgPathInstall() + "files/dot/modelTableCO.txt")
    else: # sufficient tables for gephi build
        createFiles()
        lineWriter("Timer", ["Start/fillTempTables", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        fillTempTables()
        lineWriter("Timer", ["buildConcepts", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildConcepts()
        lineWriter("Timer", ["buildRelationships", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRelationships()
        lineWriter("Timer", ["buildRGNodes", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRGNodes()
        lineWriter("Timer", ["buildRGRels", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])
        buildRGRels()
        lineWriter("Timer", ["End", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())])

if __name__ == "__main__":
    fullRun = True # True - runs for whole RF1 build, False just runs for gephi build
                    # steps in /home/ed/Code/Repos/sct/RF2/Import/RF1_MRCM_Gephi_instructions.txt
    processSteps(fullRun)
