from __future__ import print_function, unicode_literals
import sqlite3

rf1Path = "/home/ed/Data/RF1.db"

def splitFSN(inFSN):
    openingTagBrace = inFSN.rfind("(")
    if openingTagBrace > 0:
        Type = inFSN[openingTagBrace + 1:-1]
        Label = inFSN[:openingTagBrace].strip()
    else:
        Type = "RG"
        Label = inFSN
    return Label, Type

def fillGephiNodes():
    connectionIn2 = sqlite3.connect(rf1Path)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT DISTINCT CONCEPTID, FULLYSPECIFIEDNAME FROM GephiConcepts;')
    recordsetIn2 = cursorIn2.fetchall()
    counter = 0

    for thing in recordsetIn2:
        if not thing[0] == 'CONCEPTID':
            Label, Type = splitFSN(thing[1])
            tempList = [thing[0], thing[0], Label, Type]
            cursorIn2.execute("INSERT INTO GephiNodes VALUES (?, ?, ?, ?)", tempList)
            counter += 1
            if counter % 50 == 0:
                print("GephiNodes", str(counter))
                connectionIn2.commit()
    connectionIn2.commit()
    cursorIn2.close()
    connectionIn2.close()

def fillGephiEdges():
    connectionIn2 = sqlite3.connect(rf1Path)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute('SELECT DISTINCT CONCEPTID1, CONCEPTID2, RELATIONSHIPTYPE FROM GephiRelationships;')
    recordsetIn2 = cursorIn2.fetchall()
    counter = 0

    for thing in recordsetIn2:
        if not thing[0] == 'CONCEPTID1':
            cursorIn2.execute("INSERT INTO GephiEdges VALUES (?, ?, ?)", thing)
            counter += 1
            if counter % 50 == 0:
                print("GephiEdges", str(counter))
                connectionIn2.commit()
    connectionIn2.commit()
    cursorIn2.close()
    connectionIn2.close()

fillGephiNodes()
fillGephiEdges()
