import sqlite3
from SCTRF2Helpers import activeColor_IN, tabString, listify, tuplify, pterm
from SCTRF2Funcs import hasDefInList
from SCTRF2config import cfg

# scans through refsetId='733073007' looking for 'interesting' OWL stated definitions

def OWLPattern(inPattern):
    OWLPatternDict = {"|SubClassOf(|": "Primitive",
                    "|EquivalentClasses(|": "Defined",
                    "|SubObjectPropertyOf(|": "Object attribute",
                    "|SubDataPropertyOf(|": "Data attribute",
                    "|SubObjectPropertyOf(|TransitiveObjectProperty(|": "Transitive",
                    "|SubObjectPropertyOf(|SubObjectPropertyOf(ObjectPropertyChain(|": "Property chain",
                    "|ReflexiveObjectProperty(|SubObjectPropertyOf(|TransitiveObjectProperty(|": "Transitive and reflexive",
                    "|EquivalentClasses(|SubClassOf(|": "Multiple necessary",
                    "|SubClassOf(|SubClassOf(ObjectIntersectionOf(|": "GCI",
                    "|EquivalentClasses(|SubClassOf(ObjectIntersectionOf(|": "Defined GCI"}
    if inPattern in OWLPatternDict:
        return OWLPatternDict[inPattern]
    else:
        return "Not pattern recognised"

def singleOWLAxioms(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, string1 as s, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r having c=1 order by c desc;")
    recordset = cur.fetchall()
    summaryDict = {}
    summaryListDict = {}
    for itemA in recordset:
        startList = []
        OWLString = ""
        OWLString = itemA[1]
        marker = OWLString.find(":")
        if OWLString[0:marker] not in startList:
            startList.append(OWLString[0:marker])
        entryString = "|"
        for entry in sorted(startList):
            entryString += entry + "|"
        if entryString not in summaryDict:
            summaryDict[entryString] = 1
        else:
            summaryDict[entryString] += 1
        if entryString not in ["|SubClassOf(|", "|EquivalentClasses(|"]:
            if OWLPattern(entryString) not in summaryListDict:
                summaryListDict[OWLPattern(entryString)] = [[itemA[0]]]
            else:
                summaryListDict[OWLPattern(entryString)].append([itemA[0]])
    cur.close
    conn.close
    return summaryListDict

def multipleOWLAxioms(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r having c > 1 order by c desc;")
    recordset = cur.fetchall()
    summaryDict = {}
    summaryListDict = {}

    IDList = [item[0] for item in recordset]
    IDString = "'" + "', '".join(IDList) + "'"
    cur.execute("select referencedComponentId, string1 from srefset where refsetId='733073007' and active = 1 and referencedComponentId IN (" + IDString + ")")
    recordset2 = cur.fetchall()

    detailDict = {}
    for itemA in recordset:
        for row in recordset2:
            if itemA[0] == row[0]:
                if itemA[0] not in detailDict:
                    detailDict[itemA[0]] = [row[1]]
                else:
                    detailDict[itemA[0]].append(row[1])
    cur.close
    conn.close

    for itemA in recordset:
        startList = []
        OWLString = ""
        for item in detailDict[itemA[0]]:
            OWLString = item
            marker = OWLString.find(":")
            if OWLString[0:marker] not in startList:
                startList.append(OWLString[0:marker])
        entryString = "|"
        for entry in sorted(startList):
            entryString += entry + "|"
        if entryString not in summaryDict:
            summaryDict[entryString] = 1
            summaryListDict[OWLPattern(entryString)] = [[itemA[0], itemA[1]]]
        else:
            summaryDict[entryString] += 1
            summaryListDict[OWLPattern(entryString)].append([itemA[0], itemA[1]])
    return summaryListDict



# define dbpath1
dbPath1 = "/home/ed/Data/RF2Snap.db"

def interestingOWL(dbPath1):
    dict1 = multipleOWLAxioms(dbPath1)
    dict2 = singleOWLAxioms(dbPath1)
    return {**dict1, **dict2}

def callInterestingOWL(dbPath1, showMax):
    mydict = {}
    mycounter = 1
    OWLDict = interestingOWL(dbPath1)
    OWLKeyList = sorted(list(OWLDict.keys()))
    for key in OWLKeyList:
        if len(OWLDict[key][0]) == 2:
            includeCount = True
        else:
            includeCount = False
        for item in OWLDict[key]:
            if includeCount:
                item.append(pterm(item[0]))
            else:
                item.append("")
                item.append(pterm(item[0]))
        if includeCount:
            workingList = sorted(OWLDict[key], key=lambda x: (-x[1], x[2]))
        else:
            workingList = sorted(OWLDict[key], key=lambda x: (x[2]))
        if len(OWLDict[key]) > showMax:
            moreString = " (more available)"
        else:
            moreString = ""
        print('\033[4m' + key + moreString + ':\033[24m\n')
        innercounter = 1
        for item in workingList:
            hasDefString = hasDefInList(item[0], cfg("SHOW_DEFS"))
            if includeCount:
                countString = " [" + str(item[1]) + "]"
            else:
                countString = ""
            if innercounter <= showMax:
                print('[' + str(mycounter) + ']\t' + activeColor_IN(item[0], False) + tabString(item[0]) + item[2] + countString + hasDefString)
                mydict.update(list(zip(listify(mycounter), listify(tuplify(item[1])))))
            mycounter += 1
            innercounter += 1
        print()
    return mydict

dict3 = callInterestingOWL(dbPath1, 50)
