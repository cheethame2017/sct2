import sqlite3
import io
import pickle
from SCTRF2Helpers import TCCompare_IN, ancestorsIDFromConId_IN, pterm
from SCTRF2Funcs import hasDef, dotDrawAncestors, drawPics
from SCTRF2Role import writeOutList
from SCTRF2Subgraph import tlc
from SCTRF2config import tok

# working paths
homePath = '/home/ed/'
repoPath = homePath + 'Code/Repos/sct/RF2/'
miscPath = repoPath + 'files/misc/'
picklePath = repoPath + 'files/pickle/'
modelTablePath = repoPath + 'files/atts/modelTableCO.txt'
writeOutPath = repoPath + 'sets/'
writeOutHasDefListPath = miscPath + 'hasDefs.txt'
dotPath = repoPath + "files/dot/tagChange.dot"
dotPathNum = repoPath + "files/dot/tagChangeNum.dot"
RF2Outpath = homePath + 'Data/RF2Out.db'

def writeOutHasDefList(hasDefList):
    mode = 'w'
    # write list of concepts with 1+ ancestor definitions
    try:
        f = io.open(writeOutHasDefListPath, mode, encoding="UTF-8")
        for item in hasDefList:
            f.write("\t".join(item) + "\n")
    finally:
        f.close()


def preTestDefs(runNum, fullRunBool, testIdentifier, withDef):
    if fullRunBool:
        testDefs(runNum, withDef)
    elif testIdentifier[0] == "-":
        readBackList = pickle.load(open(picklePath + "hasDefList.p", "rb"))
        drawPics(3) # avoids error if A=8 in snapshot browser
        testList = []
        testId = readBackList[int(testIdentifier[1:])][3]
        testList.append(testId)
        testList.append(tok("ROOT"))
        for tlconcept in tlc():
            if TCCompare_IN(testId, tlconcept[2]):
                testList.append(tlconcept[2])
        writeOutList(testList, writeOutPath + "F.txt") # write out focus to F
        dotDrawAncestors(testId, 1)
    else:
        boundList = []
        boundList = testIdentifier.split("-")
        if withDef:
            readBackDict = pickle.load(open(picklePath + "hasDefIdDictWithDef.p", "rb"))
        else:
            readBackDict = pickle.load(open(picklePath + "hasDefIdDict.p", "rb"))
        readBackDictKeys = sorted(list(readBackDict.keys()), reverse=True)
        if len(boundList) == 1:
            boundList.append(str(len(readBackDictKeys) + 1))
        elif boundList[1] == "":
            boundList[1] = str(len(readBackDictKeys) + 1)
        hasDefList = []
        chapterDict = {}
        listCounter = 0
        for readBackDictKey in readBackDictKeys[int(boundList[0]):int(boundList[1])]:
            if runNum != 0 and runNum < len(readBackDict[readBackDictKey]):
                topCounter = runNum
            else:
                topCounter = len(readBackDict[readBackDictKey])
            topCounter = len(readBackDict[readBackDictKey])
            for counter in range(0, topCounter):
                # for tlconcept in [concept for concept in tlc() if (concept[2] != tok("BODY_STRUC") and concept[2] != tok("PROCEDURE"))]:
                for tlconcept in tlc():
                    if TCCompare_IN(readBackDict[readBackDictKey][counter], tlconcept[2]):
                        print(str(listCounter), str(readBackDictKey), tlconcept[0], readBackDict[readBackDictKey][counter], pterm(readBackDict[readBackDictKey][counter]))
                        hasDefList.append([str(listCounter), str(readBackDictKey), tlconcept[0], readBackDict[readBackDictKey][counter], pterm(readBackDict[readBackDictKey][counter])])
                        if tlconcept[0] not in chapterDict:
                            chapterDict[tlconcept[0]] = 1
                        else:
                            chapterDict[tlconcept[0]] += 1
                        listCounter += 1
        writeOutHasDefList(hasDefList)
        pickle.dump(hasDefList, open(picklePath + "hasDefList.p", "wb"))
        for readBackDictKey in readBackDictKeys: # list number of instances of each definition match (17 2 means two instances with 17 definitions in ancestry etc.)
            print(str(readBackDictKey), len(readBackDict[readBackDictKey]))
        chapterDictKeys = sorted(list(chapterDict.keys()))
        for chapterDictKey in chapterDictKeys:
            print(chapterDictKey, str(chapterDict[chapterDictKey]))

def testDefs(runNum, withDef):
    conn = sqlite3.connect(RF2Outpath)
    cur2 = conn.cursor()
    outList = []
    progress = 0
    cur2.execute('SELECT SupertypeId FROM TCSubCount order by STCount desc;')
    recordsetIn2 = cur2.fetchall()
    if runNum == 0:
        setToRun = recordsetIn2[:]
    else:
        setToRun = recordsetIn2[:runNum]
    tot = len(setToRun)
    for item in setToRun:
        ancCounter = 0
        if withDef and hasDef(item[0]):
            ancList = ancestorsIDFromConId_IN(item[0])
            for anc in ancList:
                if hasDef(anc):
                    ancCounter += 1
        elif not withDef:
            ancList = ancestorsIDFromConId_IN(item[0])
            for anc in ancList:
                if hasDef(anc) and not(item[0] == anc):
                    ancCounter += 1
        if ancCounter > 0:
            outList.append([item[0], ancCounter])
        if (progress % 100 == 0):
            print(progress, "/", tot)
        progress += 1
    cur2.close()
    conn.close()

    outDict = {}
    outList.sort(key=lambda x: x[1], reverse=True)
    for item in outList:
        if item[1] not in outDict:
            outDict[item[1]] = [item[0]]
        else:
            outDict[item[1]].append(item[0])
    if withDef:
        pickle.dump(outDict, open(picklePath + "hasDefIdDictwithDef.p", "wb"))
    else:
        pickle.dump(outDict, open(picklePath + "hasDefIdDict.p", "wb"))
    outDictKeys = sorted(list(outDict.keys()), reverse=True)
    for outDictKey in outDictKeys:
        print(str(outDictKey), len(outDict[outDictKey]))

if __name__ == '__main__':
    # True value reads from data; False value reads back latest pickle objects
    fullRunBool = False
    # number of set to sample (0 is all)
    runNum = 0
    # pass in identifier to draw or generate list of concepts and counts:
    # "0-" = generate full list
    # "3-6" = generate list between third and fifth keys
    # "-14" = draw diagram of 14th item (index number in left-most column) in latest list (view in misc/hasDefs.txt).
    testIdentifier = "-10"
    # test only those concepts that have a definition themselves
    withDef = True
    preTestDefs(runNum, fullRunBool, testIdentifier, withDef)
