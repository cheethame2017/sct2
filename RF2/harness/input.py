import sys
import os

def clearScreen():
    if sys.platform == "linux" or sys.platform == "linux2":
        print("\033c\033[92m")
    elif sys.platform == "win32":
        # windowsj
        os.system('cls')
        print("\033[92m")

def functiona():
    if not debug:
        clearScreen()
    print("Function a")
    return {"a": 1}

def functionb():
    if not debug:
        clearScreen()
    print("Function b")
    return {"b": 2}

debug = False
returnString = {}
clearScreen()
# main control code
firstTime = True
while True:
    searchDirect = False
    if firstTime:
        topaction = "a"
        firstTime = False
    else:
        if (sys.version_info > (3, 0)):
            # Python 3 code:
            topaction = input("input something: ").strip()
        else:
            # Python 2 code:
            topaction = raw_input("input something: ").decode('UTF-8').strip()
    if topaction == 'zz':
        if debug:
            print(returnString)
        break  # breaks out of the while loop and ends session
    # toggle *most* debug outputs
    elif topaction == 'xx':
        if debug:
            debug = False
            print('debug OFF')
        else:
            debug = True
            print('debug ON')
    # single letter calls
    elif topaction == 'a':
        if debug:
            print('-> calling functiona')
        returnString = functiona()
        if debug:
            print(returnString)
    elif topaction == 'b':
        if debug:
            print('-> calling functionb')
        returnString = functionb()
        if debug:
            print(returnString)
