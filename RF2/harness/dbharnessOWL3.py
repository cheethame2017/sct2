import io
import sqlite3

# scans through refsetId='733073007' looking for 'interesting' OWL stated definitions
def tidyFiles():
    filenameList = ["primitive", "defined", "attributeSubtype", "transitive", \
                    "propertyChain", "transitiveReflexive", "multipleNecessary", "GCI"]
    for filename in filenameList:
        f = io.open('/home/ed/git/rf2repo/sct/RF2/harness/files/' + filename + '.txt', 'w', encoding='UTF-8')
        f.close()

def dbHarness1(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r order by c desc;")
    recordset = cur.fetchall()
    print(len(recordset))
    summaryDict = {}
    counterTarget = 1

    # while counterTarget > 0:
    counter = 1
    #     print str(counterTarget)
    for itemA in recordset:
        if itemA[1] > 1 and (counter % counterTarget == 0):
            # print itemA[0], str(counter)
            cur.execute("select * from srefset where refsetId='733073007' and active = 1 and referencedComponentId=?;", (itemA[0],))
            recordset2 = cur.fetchall()
            # startList = []
            startDict = {}

            for item in recordset2:
                if item[6][0:item[6].find(":")] not in startDict:
                    startDict[item[6][0:item[6].find(":")]] = 1
                else:
                    startDict[item[6][0:item[6].find(":")]] += 1
            cur.close
            conn.close
            entryString = "|"
            for entry in sorted(startDict):
                entryString += entry + "|"
            if entryString not in summaryDict:
                summaryDict[entryString] = 1
            else:
                summaryDict[entryString] += 1
            # primitive
            if entryString == "|SubClassOf(|":
                print(itemA, "|SubClassOf(|")
                filename = "primitive"
            # defined
            if entryString == "|EquivalentClasses(|":
                print(itemA, "|EquivalentClasses(|")
                filename = "defined"
            # attrib subtype
            if entryString == "|SubObjectPropertyOf(|":
                print(itemA, "|SubObjectPropertyOf(|")
                filename = "attributeSubtype"
            # transitive
            if entryString == "|SubObjectPropertyOf(|TransitiveObjectProperty(|":
                print(itemA, "|SubObjectPropertyOf(|TransitiveObjectProperty(|")
                filename = "transitive"
            # property chain
            if entryString == "|SubObjectPropertyOf(|SubObjectPropertyOf(ObjectPropertyChain(|":
                print(itemA, "|SubObjectPropertyOf(|SubObjectPropertyOf(ObjectPropertyChain(|")
                filename = "propertyChain"
            # transitive / reflexive
            if entryString == "|ReflexiveObjectProperty(|SubObjectPropertyOf(|TransitiveObjectProperty(|":
                print(itemA, "|ReflexiveObjectProperty(|SubObjectPropertyOf(|TransitiveObjectProperty(|")
                filename = "transitiveReflexive"
            # multiple necessary
            if entryString == "|EquivalentClasses(|SubClassOf(|":
                print(itemA, "|EquivalentClasses(|SubClassOf(|")
                filename = "multipleNecessary"
            # GCI
            if entryString == "|SubClassOf(|SubClassOf(ObjectIntersectionOf(|":
                print(itemA, "|SubClassOf(|SubClassOf(ObjectIntersectionOf(|")
                filename = "GCI"
            f = io.open('/home/ed/git/rf2repo/sct/RF2/harness/files/' + filename + '.txt', 'a', encoding='UTF-8')
            f.write(str(itemA[0]) + '\t' + str(itemA[1]) + '\n')
            f.close()
        counter += 1
    print()
    for item in summaryDict:
        print(item, summaryDict[item])
        # counterTarget = int(counterTarget/2)

        cur.close
        conn.close

# define dbpath1
dbPath1 = "/home/ed/Data/RF2Snap.db"

# empty files
tidyFiles()
# call harness
dbHarness1(dbPath1)