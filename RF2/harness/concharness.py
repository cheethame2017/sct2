import sqlite3
from SCTRF2Helpers import pterm
from SCTRF2Concrete import toNumber, concreteTakingRoles
from SCTRF2config import pth

# CONC - entire script

def concRows(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("SELECT * FROM relationships")
    recordset = cur.fetchall()
    numberDict1 = {}
    numberDict2 = {}
    for item in recordset:
        if item[7] in concreteTakingRoles():
            print(item[5], str(toNumber(pterm(item[5]))))
            if not item[5] in numberDict1:
                numberDict1[item[5]] = toNumber(pterm(item[5]))
            if "#" + str(toNumber(pterm(item[5]))) not in numberDict2:
                numberDict2["#" + str(toNumber(pterm(item[5])))] = toNumber(pterm(item[5]))

    cur.close
    conn.close

    return numberDict1, numberDict2

def fillConc(inTable, dbPath1, inDict):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    for item in inDict:
        if inTable == 1:
            cur.execute("insert into conc1 values (? , ?)", (item, inDict[item]))
        else:
            cur.execute("insert into conc2 values (? , ?)", (item, inDict[item]))
    conn.commit()
    cur.close
    conn.close


snapPath = pth('RF2_SNAP')
# outPath = pth('RF2_OUT')
concPath = "/home/ed/Data/RF2Conc.db"


# get all the numbers used
myDict1, myDict2 = concRows(snapPath)
print(myDict1)
print()
print(len(myDict1))
print()
for item in sorted(myDict1.items(), key=lambda x: x[1]):
    print(item[0], str(myDict1[item[0]]))

print(myDict2)
print()
print(len(myDict2))
print()
for item in sorted(myDict2):
    print(item, str(myDict2[item]))


# create the tables
# sqlite3 RF2Conc.db
# CREATE TABLE conc1(id text, num integer);
# CREATE TABLE conc2(id text, num integer);

# put numbers into the tables
fillConc(1, concPath, myDict1)
fillConc(2, concPath, myDict2)
# cur2.execute("insert into conc1 values (? , ?)", (item[0], myDict1[item[0]]))
# cur2.execute("insert into conc2 values (? , ?)", (item, myDict2[item]))

# index the table.

# experiment with using the table.
# depending on the > < = notation of a query, need to 'do the maths' on the RHS and then
# return an 'IN(...)' list of either identifiers or '#' strings from the LHS.
# this can then be fed back into the query on the relationships table
# select id, num from conc2 where num >= 100; - returns # notation.
# select id, num from conc1 where num <= 100; - returns ids.
# need method to find both and turn both into list.

# during experiment phase, will need to:
# take in # notation from query predicate/ECL
# convert to number
# look up on number (do maths)
# convert to conceptId
# return list of ids
# look up on relationships table.

# with native concretes:
# take in # notation from query predicate/ECL
# convert to number
# look up on number (do maths)
# convert to # notation again
# return list of # notation
# look up on relationships table.

# need to see how all these call out from
# def orderAndCountRoleRows_IN(inRowList):
# def starterObjectSet_IN(row):
# def checkEachRow_IN(row, inList, inString):
# in SCTRF2RoleHelpers.py

# also need to align my query creation syntax with concrete query representation.
