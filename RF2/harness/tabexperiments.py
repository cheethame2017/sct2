def tab1(inList, inPos): # returns tabVal (for a list) which is then fed into tab2 tested against each member of that list
    normalId = False
    shortId = False
    longId = False
    for item in inList:
        if len(item[inPos]) < 8:
            shortId = True
        elif len(item[inPos]) > 14:
            longId = True
        else:
            normalId = True
    if not normalId and not shortId and not longId:
        return 1 # impossible option!
    elif not normalId and not shortId and longId:
        return 2 # only long
    elif not normalId and shortId and not longId:
        return 3 # only short
    elif not normalId and shortId and longId:
        return 4 # short and long
    elif normalId and not shortId and not longId:
        return 5 # only normal
    elif normalId and not shortId and longId:
        return 6 # normal and long
    elif normalId and shortId and not longId:
        return 7 # normal and short
    elif normalId and shortId and longId:
        return 8 # normal, short and long

def tab2(tabVal, inId): # oscillating around 7/14 thresholds at the moment!
    idLen = len(inId)
    if idLen < 8: # 3, 4, 7, 8
        if tabVal == 8 or tabVal == 4:
            return "\t\t\t"
        else: # 3, 7
            return "\t\t"
    elif idLen > 14: # 2, 4, 6, 8
        return "\t"
    else: # 5, 6, 7, 8
        if tabVal == 6 or tabVal == 8:
            return "\t\t"
        else: # 5, 7
            return "\t"
