import sqlite3
from SCTRF2Helpers import pterm, descendantsFromConId_IN, descendantsIDFromConId_IN, descendantCountFromConId_IN, \
                            childrenIDFromId_IN
from SCTRF2config import pth, tok
# import sys, os
# from unidecode import unidecode

snapPath = pth('RF2_SNAP')
outPath = pth('RF2_OUT')

def attUseInChildCount(inAtt, inChildList):
    childList = ', '.join(map(str, inChildList))
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select distinct destinationId from relationships where relationships.active=1 and typeId = ? and relationships.sourceId in (" + childList + ")", (inAtt,))
    recordsetIn2 = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    return len(recordsetIn2)

myAttId = tok("CONMODOBJ_ATTRIB")
myAttList = descendantsFromConId_IN(myAttId)
myFinList = descendantsIDFromConId_IN('404684003')
myFinShortList = [fin[0] for fin in myFinList if descendantCountFromConId_IN(fin[0]) > 999]
maxList = []
for att in myAttList:
    maxNum = 0
    maxVal = []
    for fin in myFinShortList:
        childList = childrenIDFromId_IN(fin)
        tempNum = attUseInChildCount(att[0], childList)
        if tempNum > maxNum:
            maxNum = tempNum
            maxVal = [att[0], att[1], fin, pterm(fin), maxNum]
    if len(maxVal) > 0:
        maxList.append(maxVal)

for row in maxList:
    print(row)
            


