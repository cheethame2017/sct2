def refSetMembersCompareRSFromConIds_IN(idIn1, idIn2, refsetType, refsetDict, debugBool, memberReturn):
    idIn2List = []
    if not idIn2 == "":
        idIn2List.append(tuplify(idIn2))
    else:
        idIn2List = siblingRefsets(idIn1)
    comparisonList = []
    connectionIn2 = sqlite3.connect(snapPath)
    cursorInLeft = connectionIn2.cursor()
    cursorInRight = connectionIn2.cursor()
    cursorInLeft.execute("select referencedComponentId from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn1))
    recordsetInLeft = cursorInLeft.fetchall()
    recordsetInLeftCount = len(recordsetInLeft)
    counter = 1
    for idIn2 in idIn2List:
        if counter % 10 == 0 and debugBool:
            print(str(counter))
        if idIn2 in refsetDict:
            if set(refsetDict[idIn1]) & set(refsetDict[idIn2]): # if the refsets match on at least one major chapter...
                # all in right hand set
                cursorInRight.execute("select referencedComponentId from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn2))
                recordsetInRight = cursorInRight.fetchall()
                recordsetInRightCount = len(recordsetInRight)
                # only in left
                cursorInRight.execute("select s1.referencedComponentId from " + refsetType + " s1 where s1.active=1 and s1.refsetId=? EXCEPT select s2.referencedComponentId from " + refsetType + " s2 where s2.active=1 and s2.refsetId=?", (idIn1, idIn2))
                # select s1.referencedComponentId from simpleRefset s1 where s1.active=1 and s1.refsetId='REFSETID1'
                # EXCEPT
                # select s2.referencedComponentId from simpleRefset s2 where s2.active=1 and s2.refsetId='REFSETID2';
                onlyLeft = cursorInRight.fetchall()
                onlyLeftCount = len(onlyLeft)
                # only in right
                cursorInRight.execute("select s1.referencedComponentId from " + refsetType + " s1 where s1.active=1 and s1.refsetId=? EXCEPT select s2.referencedComponentId from " + refsetType + " s2 where s2.active=1 and s2.refsetId=?", (idIn2, idIn1))
                # select s1.referencedComponentId from simpleRefset s1 where s1.active=1 and s1.refsetId='REFSETID2'
                # EXCEPT
                # select s2.referencedComponentId from simpleRefset s2 where s2.active=1 and s2.refsetId='REFSETID1';
                onlyRight = cursorInRight.fetchall()
                onlyRightCount = len(onlyRight)
                # intersection
                cursorInRight.execute("select s1.referencedComponentId from " + refsetType + " s1, " + refsetType + " s2 where s1.referencedComponentId=s2.referencedComponentId and s1.active=1 and s2.active=1 and s1.refsetId=? and s2.refsetId=?", (idIn1, idIn2))
                # select s1.referencedComponentId from simpleRefset s1, simpleRefset s2 where s1.referencedComponentId=s2.referencedComponentId and
                # s1.active=1 and s2.active=1 and
                # s1.refsetId='REFSETID1' and
                # s2.refsetId='REFSETID2';
                intersection = cursorInRight.fetchall()
                intersectionCount = len(intersection)
                if intersectionCount > 0:
                    if memberReturn == 1:
                        comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, onlyLeft])
                    elif memberReturn == 2:
                        comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, intersection])
                    elif memberReturn == 2:
                        comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, onlyRight])
                    else:
                        comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount])
        counter += 1
    cursorInRight.close()
    cursorInLeft.close()
    connectionIn2.close()
    comparisonList.sort(key=lambda x: x[3], reverse=True)
    return recordsetInLeftCount, comparisonList

def refSetMembersCompareTLFromConIds_IN(idIn1, refsetType, memberReturn):
    idIn2List = []
    idIn2List = [child[2] for child in childrenFromId_IN((tok('ROOT'),))]
    comparisonList = []
    connectionIn2 = sqlite3.connect(snapPath)
    cursorInLeft = connectionIn2.cursor()
    cursorInRight = connectionIn2.cursor()
    cursorInLeft.execute("attach '" + outPath + "' as RF2Out")
    cursorInLeft.execute("select referencedComponentId from " + refsetType + " where active=1 and refsetId = ?", tuplify(idIn1))
    recordsetInLeft = cursorInLeft.fetchall()
    recordsetInLeftCount = len(recordsetInLeft)
    counter = 1
    for idIn2 in idIn2List:
        # all in right hand set
        cursorInRight.execute("select subtypeId from RF2Out.tc where supertypeId = ?", tuplify(idIn2))
        recordsetInRight = cursorInRight.fetchall()
        recordsetInRightCount = len(recordsetInRight)
        # only in left
        cursorInRight.execute("select s1.referencedComponentId as c from simpleRefset s1 where s1.active=1 and s1.refsetId=? EXCEPT select s2.subtypeId as c from RF2Out.tc s2 where s2.supertypeId=?", (idIn1, idIn2))
        # select s1.referencedComponentId as c from simpleRefset s1 where s1.active=1 and s1.refsetId='REFSETID'
        # EXCEPT
        # select s2.subtypeId as c from tc s2 where s2.supertypeId='SUPERTYPEID';
        onlyLeft = cursorInRight.fetchall()
        onlyLeftCount = len(onlyLeft)
        # only in right
        cursorInRight.execute("select s1.subtypeId as c from RF2Out.tc s1 where s1.supertypeId=? EXCEPT select s2.referencedComponentId as c from simpleRefset s2 where s2.active=1 and s2.refsetId=?", (idIn2, idIn1))
        # select s1.subtypeId as c from tc s1 where s1.supertypeId='SUPERTYPEID'
        # EXCEPT
        # select s2.referencedComponentId as c from simpleRefset s2 where s2.active=1 and s2.refsetId='REFSETID';
        onlyRight = cursorInRight.fetchall()
        onlyRightCount = len(onlyRight)
        # intersection
        cursorInRight.execute("select s1.referencedComponentId from simpleRefset s1, RF2Out.tc s2 where s1.referencedComponentId=s2.subtypeId and s1.active=1 and s1.refsetId=? and s2.supertypeId=?", (idIn1, idIn2))
        # select s1.referencedComponentId from simpleRefset s1, tc s2 where
        # s1.referencedComponentId=s2.subtypeId and
        # s1.active=1 and
        # s1.refsetId='REFSETID' and
        # s2.supertypeId='SUPERTYPEID';
        intersection = cursorInRight.fetchall()
        intersectionCount = len(intersection)
        if intersectionCount > 0:
            if memberReturn == 1:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, onlyLeft])
            elif memberReturn == 2:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, intersection])
            elif memberReturn == 2:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount, onlyRight])
            else:
                comparisonList.append([idIn2, onlyLeftCount, intersectionCount, round((intersectionCount / recordsetInLeftCount) * 100, 2), round((intersectionCount / recordsetInRightCount) * 100, 2), onlyRightCount, recordsetInRightCount])
        counter += 1
    cursorInRight.close()
    cursorInLeft.close()
    connectionIn2.close()
    comparisonList.sort(key=lambda x: x[3], reverse=True)
    return recordsetInLeftCount, comparisonList