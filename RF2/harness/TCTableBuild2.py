from __future__ import print_function, unicode_literals
import sqlite3
import time

def createTCSubCount():
    RF2Snappath = '/home/ed/Data/RF2Snap.db'
    RF2Outpath = '/home/ed/Data/RF2Out.db'

    print('Adding subtype count table')
    print('->TC subs!', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    conn = sqlite3.connect(RF2Outpath)
    cur = conn.cursor()
    cur2 = conn.cursor()
    tempList = []
    cur.execute("ATTACH DATABASE '" + RF2Snappath + "' AS RF2Snap")
    cur.execute("CREATE TABLE TCSubCount (SuperTypeId TEXT, STCount INTEGER)")
    conn.commit()
    cur2.execute('SELECT RF2Snap.Concepts.id FROM RF2Snap.Concepts WHERE RF2Snap.Concepts.active=1;')
    recordsetIn2 = cur2.fetchall()
    progress = 1
    tot = len(recordsetIn2)
    for row in recordsetIn2:
        tempList = [row[0], cur.execute("select count(subtypeId) from TC where supertypeId = ?", row).fetchone()[0]]
        cur.execute("INSERT INTO TCSubCount VALUES (?, ?)", tempList)
        if (progress % 500 == 0):
            print(progress, "/", tot)
            conn.commit()
        progress += 1
    # cleanup commit
    conn.commit()

    # index TCSubCount
    print('->Index', time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    cur.execute("CREATE INDEX TCSubCount_SupertypeId_idx ON TCSubCount (supertypeId);")
    conn.commit()
    # close this database
    cur.close()
    cur2.close()
    conn.close

createTCSubCount()
