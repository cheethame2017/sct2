from __future__ import division  # Python 2 users only
import nltk, re, pprint
from nltk.util import ngrams
from nltk.corpus import stopwords
import sys, codecs 
import unicodedata
import sqlite3
from SCTRF2Helpers import *
import time

reload(sys)  
sys.setdefaultencoding('utf8')
words_I_want = ['i', 'not', 'no', 's', 't', 'd', 'm', 'o', 'y', 'a']
stopwords = [w for w in nltk.corpus.stopwords.words('english') if w not in words_I_want]

porter = nltk.PorterStemmer()

mainPath = '/home/ed/wiki/files1/'

# folderList = [["treatment","Treatment", [["71388002", ["103693007", "386053000"]], ["410942007", ["363661006"]], ["373873005", ["9191801000001103"]]]],
#             ["management","Management", [["71388002", ["103693007", "386053000"]], ["410942007", ["363661006"]], ["373873005", ["9191801000001103"]]]],
#             ["risk","Risk factor", [["404684003", ["363661006"]]]],
#             ["sign","Sign", [["404684003", ["64572001"]]]],
#             ["complication","Complication", [["404684003", ["363661006"]]]],
#             ["diagnosis","Diagnosis", [["404684003", ["363661006"]], ["363787002", ["363661006"]], ["103693007", ["363661006"]], ["386053000", ["363661006"]]]],
#             ["cause","Cause", [["64572001", ["363661006"]], ["410607006", ["264395009"]], ["264395009", ["363661006"]], ["105590001", ["363661006"]], ["373873005", ["363661006"]]]]]

folderList = [["diagnosis","Diagnosis", [["404684003", ["363661006"]], ["363787002", ["363661006"]], ["103693007", ["363661006"]], ["386053000", ["363661006"]]]]]


def tempListToDB(URtempList):
    # print URtempList
    conn = sqlite3.connect('/home/ed/D/RF2Wiki.db')
    cur = conn.cursor()
    cur.execute("insert into allMatches values (? , ? , ? , ? , ? , ? , ?)", URtempList)
    conn.commit()
    cur.close()
    conn.close()

def aggResToDB(ARtempList):
    print ARtempList
    conn = sqlite3.connect('/home/ed/D/RF2Wiki.db')
    cur = conn.cursor()
    cur.execute("insert into aggregateResults values (? , ? , ? , ? , ? , ? , ?)", ARtempList)
    conn.commit()
    cur.close()
    conn.close()

def depunct(inString):
    a = "./=-+><^?@~{}[]"
    for b in range(1, len(a)):
        inString = inString.replace(a[b], " ")
    return inString


def readInList():
    readbackList = []
    returnList = []
    # first time around; no autosuggest
    try:
        with codecs.open(mainPath + "testset.txt", "r", encoding="utf-8-sig") as f:
            for line in f:
                readbackList.append(line)
    finally:
        f.close()
    for item in readbackList:
        tabpos = item.find('\t')
        returnList.append(item[0:tabpos].encode('ascii'))
    return(returnList)

identifierList = readInList()
# limitedIdentifierList = [identifier for identifier in identifierList if identifier[-1] == '0']
limitedIdentifierList = [identifier for identifier in identifierList if identifier == '127300000']

counter = 1
total = len(limitedIdentifierList)
for identifier in limitedIdentifierList:
    print "###################################################################\n\n\n"
    print str(counter) + " / " + str(total) + "\n\n\n"
    print "###################################################################"
    time.sleep(1)
    counter += 1
    for folder in folderList:
        try:
            with codecs.open("/home/ed/wiki/files1/" + folder[0] + "/" + identifier + ".txt", "rU",encoding='utf-8-sig', errors='ignore') as f:
                raw = f.read()
                sents = nltk.sent_tokenize(raw)
                testables = []
                #send sents to equivTester.py
                # print "f", folder[0]
                # equivTestableSents = []
                # for sent in sents:
                #     equivTestableSents.append([['0'], sent])
                # equivSents = []
                # # equivSents = equivTester(equivTestableSents, [])
                # equivTester(equivTestableSents, [], 1)
                # equivSents = publicList
                # print "publicList (from harness):", publicList
                # myList = []
                # for equivSent in equivSents:
                #     myList.append(equivSent[1])
                # equivs = []
                # equivs = sorted(list(set(myList)))
                for sent in sents:
                    if isinstance(sent, unicode):
                        sent = unicodedata.normalize('NFKD', sent).encode('ascii', 'ignore')
                    n_gram_list = []
                    n_gram_equiv_list = []
                    for n in range(1, 7):
                        n_grams = ngrams([w for w in nltk.word_tokenize(depunct(sent)) if (w.lower() not in stopwords) and (w.isalnum())],n)
                        n_gram_list = list(n_grams)
                        print "send:"
                        for item in n_gram_list:
                            print item
                        equiv_n_gram_testable_list = []
                        equiv_n_gram_testable_list2 = []
                        for n_gram in n_gram_list:
                            equiv_n_gram_testable_list.append([['0'], " ".join(n for n in n_gram)])
                        # try setting up for 'match id and matchpos' way of doing this
                        for n_gram in n_gram_list:
                            equiv_n_gram_testable_list2.append([[], " ".join(n for n in n_gram)])
                        equiv_n_gram_list = []
                        n_gram_list_plus_equivs = []
                        n_gram_list_plus_equivs_pre = []
                        n_gram_list_plus_equivs_pre_list = []
                        # print "2", equiv_n_gram_testable_list2
                        for item in equiv_n_gram_testable_list2:
                            equivTester5([item])
                        # equivTester4(equiv_n_gram_testable_list, [], 1)
                        # print n, "a. returned publicList:"
                        # equiv_n_gram_list = publicList
                        for item in equiv_n_gram_list:
                            n_gram_list_plus_equivs_pre.append(item[1])
                        n_gram_list_plus_equivs_pre_list = list(set(n_gram_list_plus_equivs_pre))
                    for item in n_gram_list_plus_equivs_pre_list:
                        n_gram_list_plus_equivs.append(nltk.word_tokenize(item))
                    print len(n_gram_list_plus_equivs)
                    for n_gram in sorted(n_gram_list_plus_equivs):
                        print(n_gram)
                        tempTestable = []
                        ports = [porter.stem(m) for m in n_gram]
                        tempTestable.append(" ".join(sorted(set(m.upper() for m in ports))).strip())
                        tempTestable.append(sent)
                        testables.append(tempTestable)
            print len(testables)
            # setTestables = []
            # setTestables = list(set(tuple(x) for x in testables))
        except IOError as ioex:
            print "no " + identifier + " " + folder[0] + " combination."