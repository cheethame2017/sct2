from SCTRF2Helpers import *
from SCTRF2Refset import *

def readInCON_ID():
    f = open('files/focusSwap.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    return myId

# this code tests whether all refsets with members have descriptors.
inId  = readInCON_ID()
testList = []
testList = descendantsFromConId_IN(inId)

for item in testList:
    refsetType = refSetTypeFromConId_IN(item[0])[0][0]
    memberCount = refSetMemberCountFromConId_IN(item[0], refsetType)
    descriptorList = refSetDescriptorFromConId_IN(item[0])
    if (memberCount > 0) and (len(descriptorList) == 0):
        print item, str(memberCount), str(len(descriptorList))
