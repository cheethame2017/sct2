from grammarFuncs import *
from grammarProcess import *

debug = True

def eclRead():
    myString = ""
    f = open('/home/ed/git/rf2main/RF2/files/role.txt', 'r')
    for line in f:
        if not(line[0] == "#"):
          myString += line.strip()
    f.close()
    return myString


eclstringProcess = eclRead()
print eclstringProcess
# pad start for operator lookback & pad end for no refinements
eclreturnList = stripTerms("(&&&:" + eclstringProcess + ")")
if debug:
  print eclreturnList[0]
eclreturnList = stripIDs(eclreturnList[0])
if debug:
  print eclreturnList[0]
eclMainDict = dictAssign(eclreturnList[0])
operatorDict = findOperators(eclreturnList[1], eclMainDict)
# print eclMainDict
# dictPrint(operatorDict, False)
# dictPrint(eclreturnList[1], False)
# doubleDictPrint(operatorDict, eclreturnList[1], False)
eclBraceDict = bracePull(eclMainDict)
# dictPrint(eclBraceDict)
if (":" in eclstringProcess):
  skeletonList = findSkeleton(eclBraceDict, eclreturnList[1])
else:
  print "just process: ", eclstringProcess[3:]
# refinementList = findRefinements(eclBraceDict, eclreturnList[1])
skeletonList.append(operatorDict)
skeletonList.append(eclreturnList[1])
if debug:
  print "sequenceList"
  for item in skeletonList[0]:
    print item
  print "markerRefToParenBrace"
  for item in skeletonList[1]:
    print item
  print "refOrBraceToMarker"
  for item in skeletonList[2]:
    print item
  print "parenToRefinement", skeletonList[3]
  print "conceptOperator", skeletonList[4]
  print "conceptId", skeletonList[5]

buildProcessSequence(skeletonList)

