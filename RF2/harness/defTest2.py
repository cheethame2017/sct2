import sqlite3
import time # time
from SCTRF2Helpers import ancestorsIDFromConId_IN, pterm
from SCTRF2Funcs import hasDef

# working paths
homePath = '/home/ed/'
repoPath = homePath + 'Code/Repos/sct/RF2/'
miscPath = repoPath + 'files/misc/'
picklePath = repoPath + 'files/pickle/'
modelTablePath = repoPath + 'files/atts/modelTableCO.txt'
writeOutPath = repoPath + 'sets/'
dotPath = repoPath + "files/dot/tagChange.dot"
dotPathNum = repoPath + "files/dot/tagChangeNum.dot"
RF2Outpath = homePath + 'Data/RF2Out.db'


def testDefs(runNum, withDef, withTimer):
    conn = sqlite3.connect(RF2Outpath)
    cur2 = conn.cursor()
    outList = []
    progress = 0
    # first pass to find and order concepts with definitions in ancestry.
    # subtypes identified this way then need cleaing up (cf. rlr etc) before returning.
    cur2.execute("SELECT t.SubtypeId as s, count(t.supertypeId) as c FROM tc as t, Descriptionsdx as d1 \
                    WHERE t.SupertypeId=d1.conceptId and \
                    d1.active=1 and \
                    d1.typeId = '900000000000550004' \
                    group by s order by c desc;")
    recordsetIn2 = cur2.fetchall()
    setToRun = recordsetIn2[:]
    if runNum == 0:
        runNum = len(setToRun)
    tot = len(setToRun)
    while len(outList) <= runNum:
        item = setToRun[progress]
        ancCounter = 0
        t0 = time.time() # time
        t1 = time.time() # time
        if withDef and hasDef(item[0]):
            ancList = ancestorsIDFromConId_IN(item[0])
            for anc in ancList:
                if hasDef(anc):
                    ancCounter += 1
            t1 = time.time() # time
        elif not withDef:
            ancList = ancestorsIDFromConId_IN(item[0])
            for anc in ancList:
                if hasDef(anc) and not(item[0] == anc):
                    ancCounter += 1
            t1 = time.time() # time
        total = t1-t0 # time
        if withTimer:
            print(total, progress) # time
        if ancCounter > 0:
            outList.append([item[0], ancCounter, pterm(item[0])])
        if (progress % 100 == 0):
            print(progress, "/", tot)
        progress += 1
    cur2.close()
    conn.close()
    outList = sorted(outList, key=lambda x: (-x[1], x[2]))
    return outList

if __name__ == '__main__':
    # number of set to sample (0 is all)
    runNum = 20
    # test only those concepts that have a definition themselves
    withDef = True
    # use timer
    withTimer = False # time
    mylistDef = testDefs(runNum, withDef, withTimer)
    withDef = False
    mylistNoDef = testDefs(runNum, withDef, withTimer)
    print(mylistDef)
    print(mylistNoDef)
    print()
