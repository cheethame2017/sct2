from SCTRF2Helpers import *
from SCTRF2Funcs import *
import sys, os, time
import sqlite3
from datetime import datetime
from dateutil.relativedelta import relativedelta

fullPath = pth('RF2_FULL')

def upByNMonths(ET, n):
    effectiveTimeTemp = ET
    effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
    sixMonthIncrement = relativedelta(months=n)
    effTimeObj += sixMonthIncrement
    return effTimeObj.strftime("%Y%m%d")

connectionIn2 = sqlite3.connect(fullPath)
cursorIn2 = connectionIn2.cursor()
ET = "20020401"
while ET < time.strftime("%Y%m%d"):
    #int
    cursorIn2.execute("select count(*) from concepts where active=1 and definitionStatusId='900000000000073002' and concepts.moduleId IN ('900000000000207008', '900000000000012004') and concepts.effectiveTime=(select max(con2.effectiveTime) from concepts con2 where con2.id = concepts.id and con2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        conTempFD = role[0]
    cursorIn2.execute("select count(*) from concepts where active=1 and concepts.moduleId IN ('900000000000207008', '900000000000012004') and concepts.effectiveTime=(select max(con2.effectiveTime) from concepts con2 where con2.id = concepts.id and con2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        conTemp = role[0]
    cursorIn2.execute("select count(*) from relationships where active=1 and relationships.moduleId IN ('900000000000207008', '900000000000012004') and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        relTemp = role[0]
    cursorIn2.execute("select count(*) from descriptions where active=1 and descriptions.moduleId IN ('900000000000207008', '900000000000012004') and descriptions.effectiveTime=(select max(des2.effectiveTime) from descriptions des2 where des2.id = descriptions.id and des2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        descTemp = role[0]
    cursorIn2.execute("select count(*) from definitions where active=1 and definitions.moduleId IN ('900000000000207008', '900000000000012004') and definitions.effectiveTime=(select max(des2.effectiveTime) from definitions des2 where des2.id = definitions.id and des2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        defTemp = role[0]
    #clin
    cursorIn2.execute("select count(*) from concepts where active=1 and concepts.moduleId IN ('999000011000000103') and concepts.effectiveTime=(select max(con2.effectiveTime) from concepts con2 where con2.id = concepts.id and con2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        clinConTemp = role[0]
    cursorIn2.execute("select count(*) from relationships where active=1 and relationships.moduleId IN ('999000011000000103') and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        clinRelTemp = role[0]
    cursorIn2.execute("select count(*) from descriptions where active=1 and descriptions.moduleId IN ('999000011000000103') and descriptions.effectiveTime=(select max(des2.effectiveTime) from descriptions des2 where des2.id = descriptions.id and des2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        clinDescTemp = role[0]
    #pharm
    cursorIn2.execute("select count(*) from concepts where active=1 and concepts.moduleId IN ('999000011000001104') and concepts.effectiveTime=(select max(con2.effectiveTime) from concepts con2 where con2.id = concepts.id and con2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        pharmConTemp = role[0]
    cursorIn2.execute("select count(*) from relationships where active=1 and relationships.moduleId IN ('999000011000001104') and relationships.effectiveTime=(select max(rel2.effectiveTime) from relationships rel2 where rel2.id = relationships.id and rel2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        pharmRelTemp = role[0]
    cursorIn2.execute("select count(*) from descriptions where active=1 and descriptions.moduleId IN ('999000011000001104') and descriptions.effectiveTime=(select max(des2.effectiveTime) from descriptions des2 where des2.id = descriptions.id and des2.effectiveTime <= ?)", (ET,))
    recordsetIn2 = cursorIn2.fetchall()
    for role in recordsetIn2:
        pharmDescTemp = role[0]
    print ET, conTemp, conTempFD, relTemp, descTemp, defTemp, clinConTemp, clinRelTemp, clinDescTemp, pharmConTemp, pharmRelTemp, pharmDescTemp
    ET = upByNMonths(ET, 6)

cursorIn2.close()
connectionIn2.close()
#  and concepts.moduleId IN ('900000000000207008', '900000000000012004')
#  ('999000011000000103', '999000011000001104')