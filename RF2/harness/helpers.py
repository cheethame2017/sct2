## general helpers
import sqlite3
from datetime import datetime
from dateutil.relativedelta import relativedelta
from SCTRF2SVconfig import *

outPath = pth('RF2_OUT')
snapPath = pth('RF2_SNAP')

def listify(inThing):
    ret = []
    ret.append(inThing)
    return ret

def tuplify(inThing):
    preReturnThing = []
    preReturnThing.append(inThing)
    returnThing = tuple(preReturnThing)
    return returnThing

## sct helpers

def pterm(id):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + snapPath + "' as RF2Snap")
    cursorIn2.execute("select term from prefterm where conId = ?", (id[0],))
    recordsetIn2 = cursorIn2.fetchone()
    if not recordsetIn2 is None:
        return recordsetIn2[0]
    else:
    # alternative lookup for old rf2 metadata not covered by RDS
        cursorIn2.execute("select term from RF2Snap.foundationmetadata where conceptId = ?", (id[0],))
        recordsetIn2 = cursorIn2.fetchone()
        if not recordsetIn2 is None:
            return recordsetIn2[0]
        else:
            return "No term found"
    cursorIn2.close()
    connectionIn2.close()

def downByNMonths(ET, n):
    effectiveTimeTemp = ET
    effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
    sixMonthIncrement = relativedelta(months=n)
    effTimeObj -= sixMonthIncrement
    return effTimeObj.strftime("%Y%m%d")

def upByNMonths(ET, n):
    effectiveTimeTemp = ET
    effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
    sixMonthIncrement = relativedelta(months=n)
    effTimeObj += sixMonthIncrement
    return effTimeObj.strftime("%Y%m%d")


def readInCON_ID():
    f = open('files/focus_sv.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    return myId


def progressTrack(startEnd, character, coverString):
    # calls on to platformSpecific - not *really* needed but code invokes sys module
    progressTrackCmd(startEnd, character, coverString)


