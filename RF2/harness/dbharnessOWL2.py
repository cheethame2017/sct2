import sqlite3

def dbHarness1(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r order by c desc;")
    recordset = cur.fetchall()

    for item in recordset:
        if item[1] > 2:
            print item

    cur.close
    conn.close

# define dbpath1
dbPath1 = "/home/ed/Data/RF2Snap.db"

# call harness
dbHarness1(dbPath1)