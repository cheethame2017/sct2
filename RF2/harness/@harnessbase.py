from SCTRF2Helpers import *
from SCTRF2Funcs import *
import sys, os
from unidecode import unidecode
import sqlite3

print('sys.argv[0] =', sys.argv[0])             
pathname = os.path.dirname(sys.argv[0])        
print('path =', pathname)
print('full path =', os.path.abspath(pathname))

testId = '446608001'
harnessDict = {}
harnessString = ""

# mystring = descendantsToXLSXCall(testId)
# harnessdict = ALLdescendantsToXLSX(testId, 0)
harnessDict = showRefsetRowsForRefset(testId, False)

print(harnessDict)
