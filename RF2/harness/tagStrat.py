import io
from SCTRF2Helpers import TCCompare_IN
from SCTRF2Subgraph import tlc
from SCTRF2config import tok

# working paths
homePath = '/home/ed/'
repoPath = homePath + 'Code/Repos/sct/RF2/'
miscPath = repoPath + 'files/misc/'
picklePath = repoPath + 'files/pickle/'
modelTablePath = repoPath + 'files/atts/modelTableCO.txt'
writeOutPath = repoPath + 'sets/'
writeOutTagResultsPath = miscPath + 'newTagList.txt'
dotPath = repoPath + "files/dot/tagChange.dot"
dotPathNum = repoPath + "files/dot/tagChangeNum.dot"
RF2Outpath = homePath + 'Data/RF2Out.db'

def preProcessNewTagFile(chunkSize, byTlcBool, maxLen):
    if byTlcBool:
        processNewTagFileByTlc(chunkSize, maxLen)
    else:
        processNewTagFile(chunkSize, maxLen)

def processNewTagFile(chunkSize, maxLen):
    lineList = []
    f = io.open(writeOutTagResultsPath, 'r', encoding="UTF-8")
    for line in f.readlines()[1:]:
        tempList = line.strip().split("\t")
        tempList.append("True" if (tempList[3] == tempList[7]) else "False")
        tempList.append(int(tempList[9]) * float(tempList[11]))
        if tempList[6] <= str(maxLen):
            lineList.append(tempList)
        else:
            lineList.append(["Null", "", "", "", "", "", "", "", "", "", "", "", "", tempList[13]])
    f.close()
    lineList = sorted(lineList, key=lambda x: x[13], reverse=True)
    chunks = [lineList[x:x+chunkSize] for x in range(0, len(lineList), chunkSize)]
    counter = 1
    chunkSummary = []
    for chunk in chunks:
        dr = format((len([item for item in chunk if item[0] == 'True']) / chunkSize) * 100, '.2f') # domain or range proportion format((fdist2[key] / len(subList)) * 100, '.2f')
        dp = format((len([item for item in chunk if item[1] == 'Defined']) / chunkSize) * 100, '.2f') # defined vs. primitive proportion
        td = format((len([item for item in chunk if item[2] == 'True']) / chunkSize) * 100, '.2f') # text definition proportion
        it = format((len([item for item in chunk if item[12] == 'True']) / chunkSize) * 100, '.2f') # is its own tag proportion
        chunkSummary.append([str(counter), str(len(chunk)), dr, dp, td, it])
        counter += 1
    print()
    print("ch\tno\tdom_r\tdefd\ttext\tis_tag")
    for row in chunkSummary:
        print("\t".join(row))

def processNewTagFileByTlc(chunkSize, maxLen):
    tlcList = [row for row in tlc() if row[2] != tok("PHARM_PROD")]
    f = io.open(writeOutTagResultsPath, 'r', encoding="UTF-8")
    grandTot = 0
    chunkDigest = []
    for tlcon in tlcList:
        lineList = []
        for line in f.readlines()[1:]:
            tempList = line.strip().split("\t")
            tempList.append("True" if (tempList[3] == tempList[7]) else "False")
            tempList.append(int(tempList[9]) * float(tempList[11]))
            if TCCompare_IN(tempList[3], tlcon[2]) and tempList[6] <= str(maxLen):
                lineList.append(tempList)
            else:
                lineList.append(["Null", "", "", "", "", "", "", "", "", "", "", "", "", tempList[13]])
        lineList = sorted(lineList, key=lambda x: x[13], reverse=True)
        chunks = [lineList[x:x+chunkSize] for x in range(0, len(lineList), chunkSize)]
        counter = 1
        chunkSummary = []
        acTot = 0
        for chunk in chunks:
            ac = len([item for item in chunk if item[0] != 'Null'])
            acCalc = (ac if ac > 0 else 1)
            dr = format((len([item for item in chunk if item[0] == 'True']) / acCalc) * 100, '.2f') # domain or range proportion format
            dp = format((len([item for item in chunk if item[1] == 'Defined']) / acCalc) * 100, '.2f') # defined vs. primitive proportion
            td = format((len([item for item in chunk if item[2] == 'True']) / acCalc) * 100, '.2f') # text definition proportion
            it = format((len([item for item in chunk if item[12] == 'True']) / acCalc) * 100, '.2f') # is its own tag proportion
            chunkSummary.append([str(counter), str(ac), dr, dp, td, it])
            counter += 1
            acTot += ac
        grandTot += acTot
        print()
        print(tlcon[2], tlcon[0], str(acTot))
        print()
        print("ch\tno\tdom_r\tdefd\ttext\tis_tag")
        for row in chunkSummary:
            print("\t".join(row))
        f.seek(0)
        chunkDigest.append([tlcon[2], tlcon[0], acTot, chunkSummary])
    print()
    for chunk in chunkDigest:
        chunk.insert(3, format((chunk[2] / grandTot) * 100, '.2f'))
    print(str(grandTot))
    f.close()

if __name__ == '__main__':
    chunkSize = 300
    byTlcBool = True
    maxLen = 3
    preProcessNewTagFile(chunkSize, byTlcBool, maxLen)
