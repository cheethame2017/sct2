from SCTRF2SVconfig import *
from SCTRF2SVHelpers import *
from SCTRF2SVFuncs import *
import time

def SV_SIMrefsetRowsfromId(inId, refsetType, activeFlag, ET):
    connectionIn2 = sqlite3.connect(fullPath)
    cursorIn2 = connectionIn2.cursor()
    if activeFlag == 1:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where active=1 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    elif activeFlag == 2:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    elif activeFlag == 3:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where referencedComponentId= ?", (str(inId),))
    else:
        cursorIn2.execute("select refsetId, effectiveTime, active, componentId1 from " + refsetType + " where active=0 and referencedComponentId= ? and " + refsetType + ".effectiveTime = (select max(sr2.effectiveTime) from " + refsetType + " sr2 where sr2.id = " + refsetType + ".id and sr2.effectiveTime <= ? )", (str(inId), ET))
    recordsetIn2 = cursorIn2.fetchall()
    refSetList = []
    tempSet = ()
    for refSet in recordsetIn2:
        tempSet = (refSet[0], pterm(tuplify(refSet[0])), refSet[1], refSet[2], refSet[3], pterm(tuplify(refSet[3])))
        refSetList.append(tempSet)
    refSetList.sort(key=lambda x: x[2])
    cursorIn2.close()
    connectionIn2.close()
    return refSetList

inId  = readInCON_ID()
mylist = SV_SIMrefsetRowsfromId(inId, "cRefset", 1, time.strftime("%Y%m%d"))
for item in mylist:
    if not(item[0]=='900000000000489007'):
        print item[2] + " " + item[1].replace(' association reference set','') + ":" + item[4] + " " + item[5]