import time
from SCTRF2SVconfig import tok
from SCTRF2SVFuncs import checkFullSweepBool, upByNMonths, snapshotHeader, xlsxFile
from SCTRF2SVHelpers import SV_SIMC1_IsA_C2_Compare, pterm, RLRParts, otherLangSet, SV_SIMterms
from SCTRF2Role import readFileMembers
from platformSpecific import clearScreen, cfgPathInstall, sep

def testTermTokens(thing, ET, tokList):
    myTerms = []
    myTerms = SV_SIMterms(thing, 1, ET, (RLRParts + otherLangSet))
    if not myTerms == []:
        matchTot = 0
        for tok in tokList:
            matchBool = False
            for termRow in myTerms:
                if tok in termRow[1][0][7] and termRow[1][0][2] == 1:
                    matchBool = True
            if matchBool:
                matchTot += 1
        if matchTot > 1:
            return True
        else:
            return False
    else:
        return False

def getTermTok():
    # currently fixed set
    return ["declin", "refus"]
    # will generate from /Data/read.txt file

def ALLcodesInSetToXLSX(inId, fullSweepBool, testTermTokBool):
    if testTermTokBool:
        tokList = getTermTok()
    CON_ID = tok("ROOT")
    setMembers = readFileMembers(cfgPathInstall() + inId)
    topLine = []
    matrix = []
    topLine.append(" ")
    topLine.append("Introduction dates of " + inId)
    ET = checkFullSweepBool(fullSweepBool)
    while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
        topLine.append(ET)
        ET = upByNMonths(ET, 6)
    u = len(setMembers)
    total = len(setMembers)
    if total > 250:
        showBool = False
    else:
        showBool = True
    for thing in setMembers:
        tempLine = []
        ET = checkFullSweepBool(fullSweepBool)
        clearScreen()
        snapshotHeader()
        if u > 1:
            print('Processing members: '+ '*' * int((u - 1) * (60 / total)))
            print(thing)
        else:
            print('Completed.')
        tempLine.append(thing)
        tempLine.append(pterm(thing))
        while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
            character = SV_SIMC1_IsA_C2_Compare(thing, CON_ID, 1, ET, "desc", showBool, True)
            if testTermTokBool:
                if testTermTokens(thing, ET, tokList):
                    tempLine.append("@")
                else:
                    tempLine.append(character)
            else:
                tempLine.append(character)
            ET = upByNMonths(ET, 6)
        if CON_ID != thing[0]:
            matrix.append(tempLine)
        u -= 1
    xlsxFile(cfgPathInstall() + 'files' + sep() + 'xlsx' + sep(), 'setmembers', CON_ID, topLine, matrix)

ALLcodesInSetToXLSX("sets" + sep() + "K" + ".txt", True, False)
