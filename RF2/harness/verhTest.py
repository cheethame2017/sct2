import sqlite3
import io
from SCTRF2Funcs import validateVerhoeff, fsnFromConId_IN

def writeOutList(outList, outPath):
    try:
        f = io.open(outPath, 'w', encoding="UTF-8")
        f.write("\"PreOrPost\"\t\"OriginalId\"\t\"OriginalFSN\"\t\"Slice\"\t\"NewId\"\t\"NewFSN\"\n")
        for item in outList:
            f.write("\"" + "\"\t\"".join(item) + "\"\n")
    finally:
        f.close()

def byNum():
    i = 1000
    while i < 99999999999999999999:
        iString = str(i)
        for n in range (0, 6):
            for partString in ["00", "01"]:
                for check in range (0, 10):
                    fsnString = ""
                    codeString = iString + partString + str(check)
                    if validateVerhoeff(codeString[n:]):
                        fsnString = fsnFromConId_IN(codeString[n:])
                        if fsnString[0] != "*" and n > 0:
                            print(str(n), codeString[n:], fsnString)
                        if fsnString[0] != "*" and n == 0:
                            print("*", codeString[n:], fsnString)
        if (i % 100 == 0):
            print(codeString)
        i += 1

def byId(RF2Snappath, outPath):
    conn = sqlite3.connect(RF2Snappath)
    cur2 = conn.cursor()
    cur2.execute('SELECT Concepts.id FROM Concepts WHERE Concepts.active=1 order by Concepts.id;')
    recordsetIn2 = cur2.fetchall()
    i = 1
    outList = []
    tot = len(recordsetIn2)
    for row in recordsetIn2:
        codeString = row[0]
        b = len(codeString) - 1
        for n in range (0, b):
            fsnString = ""
            if validateVerhoeff(codeString[n:]):
                fsnString = fsnFromConId_IN(codeString[n:])
                if fsnString[0] != "*" and n > 0:
                    fsnString2 = fsnFromConId_IN(codeString)
                    outList.append([">", codeString, fsnString2[1], codeString[:n], codeString[n:], fsnString[1]])
            if n > 0:
                if validateVerhoeff(codeString[:-n]):
                    fsnString = fsnFromConId_IN(codeString[:-n])
                    if fsnString[0] != "*" and n > 0:
                        fsnString2 = fsnFromConId_IN(codeString)
                        outList.append(["<", codeString, fsnString2[1], codeString[-n:], codeString[:-n], fsnString[1]])
        if (i % 100 == 0):
            print(str(i), "/", tot)
        i += 1

    writeOutList(outList, outPath)

    cur2.close()
    conn.close()

if __name__ == '__main__':
    outPath = "/home/ed/Code/Repos/sct/RF2/files/verhoeff.csv"
    RF2Snappath = '/home/ed/Data/RF2Snap.db'
    byId(RF2Snappath, outPath)
