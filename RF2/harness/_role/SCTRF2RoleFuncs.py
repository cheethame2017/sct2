from grammarFuncs import *
from grammarProcess import *

def eclRead():
    print
    myString = ""
    f = open( cfgPathInstall() + 'files/roleManip.txt', 'r')
    for line in f:
        if not(line[0] == "#"):
            myString += line.strip()
    f.close()
    return myString

def eclCopy():
    fin = open( cfgPathInstall() + 'files/role.txt', 'r')
    fout = open( cfgPathInstall() + 'files/roleManip.txt', 'w')
    for line in fin:
        fout.write(line)
    fin.close()
    fout.close()
    return "copied"


def eclDisplay(debug, debug2, copy):
    eclreturnList = []
    if copy:
        eclstringProcess = eclCopy()
    eclstringProcess = eclRead()
    print eclstringProcess
    print
    # pad start for operator lookback & pad end for no refinements
    eclreturnList = stripTerms("(&&&:" + eclstringProcess + ")")
    if debug2:
        print eclreturnList[1]
        print
    if debug2:
        print eclreturnList[0]
        print
    eclreturnList = stripIDs(eclreturnList[0])
    if debug2:
        print eclreturnList[0]
        print
    eclMainDict = dictAssign(eclreturnList[0])
    if debug2:
        print eclMainDict
        print
    operatorDict = findOperators(eclreturnList[1], eclMainDict)
    eclBraceDict = bracePull(eclMainDict)
    skeletonList = []
    if (":" in eclstringProcess):
        skeletonList = findSkeleton(eclBraceDict, eclreturnList[1])
        skeletonList.append(operatorDict)
        skeletonList.append(eclreturnList[1])
        if debug:
            print "sequenceList"
            for item in skeletonList[0]:
                print item
            print "markerRefToParenBrace"
            for item in skeletonList[1]:
                print item
            print "refOrBraceToMarker"
            for item in skeletonList[2]:
                print item
            print "parenToRefinements", skeletonList[3]
            print "conceptOperators", skeletonList[4]
            print "conceptIds", skeletonList[5]
            print
        # check single layer of refinements/braces - if not, say can't do for now

        # check single object concept - if not, say can't do for now - if is, assign index and show

        # check for brace(s) - where exist, assign index(es) and show

        # check for refinements - where exist assign index(es) and show
        
    else:
        print "just process: ", eclstringProcess[3:]
        print

    return eclstringProcess # actually return dict filled with indexed object, braces and refinements and debug show - 
    # ? always return what's in memory plus a 'workon' element (e.g. a brace or a refinement or an element (object, att or value))


def eclProcess():
    eclreturnList = []
    eclstringProcess = eclRead()
    # pad start for operator lookback & pad end for no refinements
    eclreturnList = stripTerms("(&&&:" + eclstringProcess + ")")
    eclreturnList = stripIDs(eclreturnList[0])
    eclMainDict = dictAssign(eclreturnList[0])
    operatorDict = findOperators(eclreturnList[1], eclMainDict)
    eclBraceDict = bracePull(eclMainDict)
    skeletonList = []
    if (":" in eclstringProcess):
        skeletonList = findSkeleton(eclBraceDict, eclreturnList[1])
    skeletonList.append(operatorDict)
    skeletonList.append(eclreturnList[1])
    buildProcessSequence(skeletonList)
    return eclstringProcess

# rework this code to move persistence form of constraint into db [( or : will be used to identify nesting - later]
def prelim(seedTime, seedModules):
    connectionIn1 = sqlite3.connect(modsetPath)

    cursorIn1 = connectionIn1.cursor()
    cursorIn2 = connectionIn1.cursor()
    cursorIn1.execute("drop table modset")
    cursorIn1.execute("create table modset (mod char(20), mod_et char(20))")
    cursorIn1.execute("ATTACH DATABASE '" + fullPath + "' AS RF2Full")
    mstring = ', '.join(map(str, seedModules))
    # with input seedModules converted to mstring
    queryString = "select distinct referencedComponentId as mod, string2 as mod_et from RF2Full.ssrefset where moduleId in (" + mstring + ") and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?) union select distinct ssrefset.moduleId as mod, effectiveTime as mod_et from ssrefset where moduleId in (" + mstring + ") and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?)"
    cursorIn2.execute(queryString, (seedTime, seedTime))
    recordsetIn2 = cursorIn2.fetchall()
    preModETWrite(seedTime, seedModules, recordsetIn2)
    for row in recordsetIn2:
        cursorIn1.execute('insert into modset values (?,?)', row)
    connectionIn1.commit()
    # needed because of broken data
    cursorIn1.execute('select mod, mod_et from modset where modset.mod_et = (select max(m2.mod_et) from modset m2 where m2.mod = modset.mod and m2.mod_et <= ?)', (seedTime,))
    recordsetIn1 = cursorIn1.fetchall()
    returnSet = recordsetIn1
    cursorIn1.close()
    cursorIn2.close()
    connectionIn1.close()
    modETWrite(seedTime, seedModules, returnSet)
    return returnSet