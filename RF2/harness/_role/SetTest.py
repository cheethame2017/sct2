from SCTRF2Helpers import *
from SCTRF2Funcs import *
import re
import random
import sqlite3

def AND(Aset, Bset):
    return Aset & Bset

def OR(Aset, Bset):
    return Aset | Bset

def NOT(Aset, Bset):
    return Aset - Bset

def listResults(header, resultset, lenBool):
    resultset = list(set(resultset))
    print "==================="
    if lenBool:
        print header, len(resultset)
    else:
        print header
    print
    for result in resultset:
        print str(result) + "\t" + pterm(tuplify(result))
    print

def Self(conId):
    selfSet = []
    selfSet.append(conId)
    return selfSet

def descendantsAndSelf(conId):
    descendantList = []
    descendantMembers = []
    descendantSet = []
    descendantList = descendantsFromConId_IN(conId)
    for listDescendant in descendantList:
        descendantMembers.append(listDescendant[0])
    descendantSet = set(descendantMembers)
    return descendantSet

def descendants(conId):
    selfSet = set(listify(conId))
    descendantList = []
    descendantMembers = []
    descendantSet = []
    descendantList = descendantsFromConId_IN(conId)
    for listDescendant in descendantList:
        descendantMembers.append(listDescendant[0])
    descendantSet = set(descendantMembers)
    return descendantSet - selfSet

def RolesFromAttAndDestId_IN(attIdSet, destIdSet):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    sourceList = []
    for attId in attIdSet:
        for destId in destIdSet:
            argumentList = []
            argumentList.append(attId)
            argumentList.append(destId)
            cursorIn2.execute("select distinct typeId, sourceId from relationships where active=1 and typeId = ? and destinationId = ?", argumentList)
            recordsetIn2 = cursorIn2.fetchall()
            for role in recordsetIn2:
                if not role[0] == '116680003':
                    sourceList.append(role[1])
    cursorIn2.close()
    connectionIn2.close()
    return set(sourceList)

def SelfTargetSelfAttFromConId_IN(targetId, attId):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    # children
    cursorIn2.execute("select relationshipGroup, typeId, sourceId from relationships where active=1 and destinationId = ?", tuplify(id))
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    tempSet = ()
    for role in recordsetIn2:
        if not role[1] == '116680003':
            tempSet = (role[0], role[1], role[2])
            preRoleList.append(tempSet)
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) == 0:
        return roleList
    else:
        roleMax = preRoleList[len(preRoleList) - 1][0]
        for roleCount in range(-1, roleMax + 1):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
        cursorIn2.close()
        connectionIn2.close()
        return roleList

def DescenSelfTargetSelfAttFromConId_IN(sourceOp, sourceId, attOp, attId, targetOp, targetId):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    argumentList = []
    argumentListStar = []
    argumentList.append(attId)
    argumentListStar.append(attId)
    argumentList.append(targetId)
    if (targetOp == "<<") or (targetOp == "<"):
        cursorIn2.execute("select relationshipGroup, sourceId, typeId, destinationId from relationships where typeId = ? and active=1 and destinationId in (select subtypeId from tc where supertypeId = ?)", argumentList)
    elif targetOp == "*":
        starString = ""
        # for item in targetId[0]:
        #     starString += ("'" + item + "', ")
        starString = ', '.join(map(str, targetId[0]))
        # print "select relationshipGroup, sourceId, typeId, destinationId from relationships where typeId = ? and active=1 and destinationId in (" + starString + ")"
        cursorIn2.execute("select relationshipGroup, sourceId, typeId, destinationId from relationships where typeId = ? and active=1 and destinationId in (" + starString + ")", argumentListStar)
    else:
        cursorIn2.execute("select relationshipGroup, sourceId, typeId, destinationId from relationships where typeId = ? and active=1 and destinationId = ?", argumentList)
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    tempSet = ()
    for role in recordsetIn2:
        if sourceOp == "<<":
            if TCCompare_local(role[1],sourceId):
                if (targetOp == "<") and not(role[3] == targetId):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
                elif (targetOp == "<<") or (targetOp == "") or (targetOp == "*"):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
        if sourceOp == "<":
            if TCCompare_local(role[1],sourceId) and not(role[1] == sourceId):
                if (targetOp == "<") and not(role[3] == targetId):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
                elif (targetOp == "<<") or (targetOp == "") or (targetOp == "*"):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
        if sourceOp == "":
            if (role[1] == sourceId):
                if (targetOp == "<") and not(role[3] == targetId):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
                elif (targetOp == "<<") or (targetOp == "") or (targetOp == "*"):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)

    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) == 0:
        return roleList
    else:
        roleMax = preRoleList[len(preRoleList) - 1][0]
        for roleCount in range(-1, roleMax + 1):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
        cursorIn2.close()
        connectionIn2.close()
        return roleList


def DescenSelfSourceSelfAttFromConId_IN(sourceOp, sourceId, attOp, attId, targetOp, targetId):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    argumentList = []
    argumentList.append(attId)
    argumentList.append(sourceId)
    if (sourceOp == "<<") or (sourceOp == "<"):
        cursorIn2.execute("select relationshipGroup, sourceId, typeId, destinationId from relationships where typeId = ? and active=1 and sourceId in (select subtypeId from tc where supertypeId = ?)", argumentList)
    else:
        cursorIn2.execute("select relationshipGroup, sourceId, typeId, destinationId from relationships where typeId = ? and active=1 and sourceId = ?", argumentList)
    recordsetIn2 = cursorIn2.fetchall()
    preRoleList = []
    tempSet = ()
    for role in recordsetIn2:
        if targetOp == "<<":
            if TCCompare_local(role[3],targetId):
                if (sourceOp == "<") and not(role[1] == sourceId):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
                elif (sourceOp == "<<") or (sourceOp == ""):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
        if targetOp == "<":
            if TCCompare_local(role[3],targetId) and not(role[3] == targetId):
                if (sourceOp == "<") and not(role[1] == sourceId):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
                elif (sourceOp == "<<") or (sourceOp == ""):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
        if targetOp == "":
            if (role[3] == targetId):
                if (sourceOp == "<") and not(role[1] == sourceId):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
                elif (sourceOp == "<<") or (sourceOp == ""):
                    tempSet = (role[0], role[1], role[2], role[3])
                    preRoleList.append(tempSet)
    preRoleList.sort(key=lambda x: x[0])
    #groups
    roleList = []
    if len(preRoleList) == 0:
        return roleList
    else:
        roleMax = preRoleList[len(preRoleList) - 1][0]
        for roleCount in range(-1, roleMax + 1):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
        cursorIn2.close()
        connectionIn2.close()
        return roleList


def SourceOrDestinationFirst(inList):
    # inList is OAV triple with operator prefixes - assumes that A operator is '', and OV operators are <<
    # could have first pass from calculateSimple(inOperator, inId) to test operators
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    sourceList = []
    argumentList = []
    argumentList.append(inList[1])
    cursorIn2.execute("select count(subtypeId) as x from tc where supertypeId = ?", argumentList)
    recordsetIn2 = cursorIn2.fetchall()
    O = recordsetIn2[0][0]
    sourceList = []
    argumentList = []
    if inList[4] == "*":
        V = len(inList[5][0])
    else:
        argumentList.append(inList[5])
        cursorIn2.execute("select count(subtypeId) as x from tc where supertypeId = ?", argumentList)
        recordsetIn2 = cursorIn2.fetchall()
        V = recordsetIn2[0][0]
    if O >= V:
        print "Process A=V first", str(O), str(V)
        roleList = []
        roleList = DescenSelfTargetSelfAttFromConId_IN(inList[0], inList[1], inList[2], inList[3], inList[4], inList[5])
        # print roleList
    else:
        print "Process O=A first", str(O), str(V)
        roleList = []
        roleList = DescenSelfSourceSelfAttFromConId_IN(inList[0], inList[1], inList[2], inList[3], inList[4], inList[5])
        # print roleList
    cursorIn2.close()
    connectionIn2.close()
    return roleList



def RolesFromAttAndDestId_IN(attIdSet, destIdSet):
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    sourceList = []
    for attId in attIdSet:
        for destId in destIdSet:
            argumentList = []
            argumentList.append(attId)
            argumentList.append(destId)
            cursorIn2.execute("select distinct typeId, sourceId from relationships where active=1 and typeId = ? and destinationId = ?", argumentList)
            recordsetIn2 = cursorIn2.fetchall()
            for role in recordsetIn2:
                if not role[0] == '116680003':
                    sourceList.append(role[1])
    cursorIn2.close()
    connectionIn2.close()
    return set(sourceList)


def ancestorsFromConId_local(id):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn2 = connectionIn2.cursor()
    ancestorList = []
    tempSet = ()
    cursorIn2.execute("select supertypeId from tc where subtypeId = ?", tuplify(id))
    recordsetIn2 = cursorIn2.fetchall()
    for ancestor in recordsetIn2:
        tempSet = (ancestor[0])
        ancestorList.append(tempSet)
    cursorIn2.close()
    connectionIn2.close()
    return ancestorList

def TCCompare_local(subId, supId):
    # connectionIn2 = sqlite3.connect(outPath)
    # cursorIn2 = connectionIn2.cursor()
    # cursorIn2.execute("select supertypeId from tc where subtypeId = ? and supertypeId = ?", (subId, supId))
    # recordsetIn2 = cursorIn2.fetchone()
    # checkBool = False
    # if recordsetIn2:
    #     checkBool = True
    ##########################
    supertypeSet = []   
    supertypeSet = ancestorsFromConId_local(subId)
    checkBool = False
    for supertype in supertypeSet:
        if supertype == supId:
            checkBool = True
    ##########################
    # cursorIn2.close()
    # connectionIn2.close()
    return checkBool


def prelim(seedTime, seedModules):
    connectionIn1 = sqlite3.connect(modsetPath)

    cursorIn1 = connectionIn1.cursor()
    cursorIn2 = connectionIn1.cursor()
    cursorIn1.execute("drop table modset")
    cursorIn1.execute("create table modset (mod char(20), mod_et char(20))")
    cursorIn1.execute("ATTACH DATABASE '" + fullPath + "' AS RF2Full")
    mstring = ', '.join(map(str, seedModules))
    # with input seedModules converted to mstring
    queryString = "select distinct referencedComponentId as mod, string2 as mod_et from RF2Full.ssrefset where moduleId in (" + mstring + ") and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?) union select distinct ssrefset.moduleId as mod, effectiveTime as mod_et from ssrefset where moduleId in (" + mstring + ") and ssrefset.effectiveTime = (select max(ss2.effectiveTime) from ssrefset ss2 where ss2.id = ssrefset.id and ss2.effectiveTime <= ?)"
    cursorIn2.execute(queryString, (seedTime, seedTime))
    recordsetIn2 = cursorIn2.fetchall()
    preModETWrite(seedTime, seedModules, recordsetIn2)
    for row in recordsetIn2:
        cursorIn1.execute('insert into modset values (?,?)', row)
    connectionIn1.commit()
    # needed because of broken data
    cursorIn1.execute('select mod, mod_et from modset where modset.mod_et = (select max(m2.mod_et) from modset m2 where m2.mod = modset.mod and m2.mod_et <= ?)', (seedTime,))
    recordsetIn1 = cursorIn1.fetchall()
    returnSet = recordsetIn1
    cursorIn1.close()
    cursorIn2.close()
    connectionIn1.close()
    modETWrite(seedTime, seedModules, returnSet)
    return returnSet


def initRoleWrite(inString):
    readbackList = []
    writeToList = []
    pyperclip.copy(inString)
    if len(inString) > 5:
        try:
            f = open('files/breadcrumbs.txt', 'r')
            lines = f.readlines()
            for line in lines:
                readbackList.append(line)
            matchBool = False
            if (readbackList[-1]).strip() == inString.strip():
                matchBool = True
            if not matchBool:
                readbackList.append(inString.strip() + '\n')
            f.close()
            f = open('files/breadcrumbs.txt', 'w')
            if matchBool:
                writeToList = readbackList
            else:
                writeToList = readbackList[1:]
            for entry in writeToList:
                f.write(entry)
        finally:
            f.close()


def roleRead():
    mydict = {}
    mycounter = 1
    f = open('files/breadcrumbs.txt', 'r')
    for line in f:
        print '[' + str(mycounter) + ']\t' + line.strip() + '\t' + pterm(tuplify(line.strip()))
        mydict.update(zip(listify(mycounter), listify(tuplify(line.strip()))))
        mycounter += 1
    f.close()
    return mydict

def readInFocus():
    f = open('files/focusSwap.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    return myId

def searchListRead():
    myList = []
    counter = 0
    f = open('files/searchreport.txt', 'r')
    for line in f:
        # if counter == 0:
        #      re.match('Search for \"(.*)\":', line.strip()).group(1)
        # else:
        if counter != 0:
            # print line.strip()[:line.strip().find(' ')]
            myList.append(line.strip()[:line.strip().find(' ')])
        counter += 1
    f.close()
    return myList

def tlc():
    tlcList = childrenFromId_IN(('138875005',))
    return tlcList

def readInCON_IDSnap():
    f = open('files/focusSwap.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    return myId 

def setToGraph(inSet):
    # for item in inSet:
    #     print item
    overList = []
    setList = []
    tlcLocal = []
    tlcLocal = tlc()
    for item in tlcLocal:
        temp = item[2]
        overList.append({temp:[(temp,)]})
    for member in inSet:
        # if True:
        # if (member[0][-1] == '0') or (member[0][-1] == '1'):
        # if (member[0][-1] == '1'):
            print member
            for item in overList:
                for key in item.keys():
                    # print member, key
                    if TCCompare_IN(member[0], key):
                        item[key].append(member)
    print '1'
    for item in overList:
        print item
    print '2'
    for item in overList:
        # print item
        # print item.keys(), len(item.values()[0])
        if len(item.values()[0]) > 1:
            setList.append(item)
    print '3'
    for item in setList:
        print item
    return setList

def adjacencyBuild(chapterList):
    adjList = []
    for item in chapterList:
        for key in item.keys():
            adjList.append({key:[]})
    print adjList
    for item1 in adjList:
        for key1 in item1.keys():
            for item2 in chapterList:
                for key2 in item2.keys():
                    if key1 == key2:
                        for item3 in item2.values():
                            for item4 in item3:
                                TCList = []
                                TCList = ancestorsIDFromConId_IN(item4[0])
                                for item5 in TCList:
                                    for item6 in item3:
                                        if item5 == item6[0] and not(item4[0] == item6[0]):
                                            print key1, item4, item6
                                            item1[key1].append(zip(item4, item6)[0])
    # all valid transitive relations...
    # print "before"
    # for item1 in adjList:
    #     for key1 in item1.keys():
    #         for item2 in item1[key1]:
    #             print key1, item2
    #         print
    # remove transitive relations...
    c1 = 0
    for item1 in adjList:
        removeList = []
        for key1 in item1.keys():
            c1 += 1
            # for each tuple in each value list
            c2 = 0
            for value1 in item1[key1]:
                c2 += 1
                # print value1
                # match source
                c3 = 0
                for sourceMatch in item1[key1]:
                    c3 += 1
                    # if there is a source match but not a destinationMatch...
                    if (sourceMatch[0] == value1[0]) and not(sourceMatch[1] == value1[1]):
                        # check to see if there is a destination match
                        c4 = 0
                        for destinationMatch in item1[key1]:
                            # if the destination code of the source match matches the source of another, and 
                            # the destination of this matches the destination of value 1...
                            c4 += 1
                            if (sourceMatch[1] == destinationMatch[0]) and (destinationMatch[1] == value1[1]):
                                # value1 is redundant and can be added to removeList
                                removeList.append(value1)
                            print c1, c2, c3, c4
        # remove rows that are in removeList
        item1[key1] = [item for item in item1[key1] if item not in removeList]
        # print item1[key1]
        item1[key1].append((key1, '138875005'))
    # print "after"
    for item1 in adjList:
        for key1 in item1.keys():
            for item2 in item1[key1]:
                print key1 + "\t" + item2[0] + "\t" + item2[1]
            print
    total = 0
    for item1 in adjList:
        for key1 in item1.keys():
            print key1 + "\t" + pterm(tuplify(key1)) + "\t" + str(len(item1[key1]))
            total += len(item1[key1])
    print "total", total


myId = readInCON_IDSnap()
if TCCompare_IN(myId, '446609009'): #check for simple refset
    myList = refSetMembersFromConId_IN(myId, 'simplerefset', 4)
else: # or
    myPreList = descendantsIDFromConId_IN(myId)
    # 50% or whatever randomise
    k=int(len(myPreList) * 0.2)
    print k
    random.shuffle(myPreList)
    myList = myPreList[0:k]
myChapterList = setToGraph(myList)
adjacencyBuild(myChapterList)


# to 'do', I would need to write the adjacency rows to an emulation of RF2Out.rellite.
# this can then be accessed from modified Help.parentsFromId_IN and Help.childrenFromIdWithPlus_IN,
# controlled through a modified Func.browseFromId


# if __name__ == "__main__":
#     mycounter = 0
#     searchMembers = []
#     searchList = []
#     # searchListRead()
#     # searchList = searchOnText_IN('cholecystectomy', 1, 0)
#     searchList = searchListRead()
#     for listSearch in searchList:
#         if not listSearch is None:
#             if mycounter == 0:
#                 mycounter += 1
#             else:
#                 searchMembers.append(listSearch)
#                 mycounter += 1
#     searchSet = set(searchMembers)

#     descendantSet = []
#     descendantSet = descendantsAndSelf(readInFocus())
    # descendantSet = descendantsAndSelf('38102005')

    # resultset = []
    # listResults('search', searchSet, True)
    # listResults('descendants', descendantSet, True)
    # resultset = AND(searchSet, descendantSet)
    # listResults('search AND descendant', resultset, True)
    # resultset = OR(searchSet, descendantSet)
    # listResults('search OR descendant', resultset, True)
    # resultset = NOT(searchSet, descendantSet)
    # listResults('search NOT descendant', resultset, True)
    # resultset = NOT(descendantSet, searchSet)
    # listResults('descendant NOT search', resultset, True)

    # roleList = []
    # roleList = RolesFromAttAndDestId_IN(Self('363704007'), Self('113305005'))
    # listResults('self_att, self_dest', roleList, True)
    # roleList = RolesFromAttAndDestId_IN(descendantsAndSelf('363704007'), descendantsAndSelf('113305005'))
    # listResults('descendant and self_att, descendant and self_dest', roleList, True)
    # roleList = RolesFromAttAndDestId_IN(descendants('363704007'), descendants('113305005'))
    # listResults('descendants_att, descendants_dest', roleList, True)
    # roleList = RolesFromAttAndDestId_IN(descendants('363704007'), descendantsAndSelf('113305005'))
    # listResults('descendants_att, descendants and self_dest', roleList, True)
    # roleList = RolesFromAttAndDestId_IN(descendantsAndSelf('363704007'), descendants('113305005'))
    # listResults('descendants and self_att, descendants_dest', roleList, True)
    # roleList = RolesFromAttAndDestId_IN(Self('363704007'), descendants('113305005'))
    # listResults('self_att, descendants_dest', roleList, True)
    # roleList = RolesFromAttAndDestId_IN(descendants('363704007'), Self('113305005'))
    # listResults('descendants_att, self_dest', roleList, True)


