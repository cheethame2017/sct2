debug = False

def dictAssign(inString): 
    returnDict = {}
    counter = 1
    for char in inString:
        returnDict.update({counter: char})
        counter += 1
    return returnDict

def bracePull(inDict):
    returnDict = {}
    for key in inDict:
        if inDict[key] in "&{}():=,":
            returnDict.update({key: inDict[key]})
    return returnDict

def stripIDs(inString):
    returnList = []
    IDDict = {}
    openIDBool = False
    closeIDBool = False
    counter = 1
    IDCounter = 1
    tempID = ""
    eclstringout = ""
    for char in inString:
        if char not in "1234567890" and openIDBool == True:
            closeIDBool = True
        if char in "123456789" and openIDBool == False:
            openIDBool = True
            IDCounter = counter
        if openIDBool == True:
            if char not in  "1234567890" and closeIDBool == True:
                tempID += char
                eclstringout += "_"
                IDDict.update({IDCounter:tempID.strip()})
                tempID = ""
                closeIDBool = False
                openIDBool = False
            else:
                tempID += char
                eclstringout += "_"
            # print char
        else:
            # print char, str(coi.get(char, 'empty')), ord(char)
            eclstringout += char
        counter += 1
    returnList.append(eclstringout)
    returnList.append(IDDict)
    return returnList

def stripTerms(inString):
    returnList = []
    termDict = {}
    openPipeBool = False
    closePipeBool = False
    counter = 1
    termCounter = 1
    tempTerm = ""
    eclstringout = ""
    for char in inString:
        if char == "|" and openPipeBool == True:
            closePipeBool = True
        if char == "|" and openPipeBool == False:
            openPipeBool = True
            termCounter = counter
        if openPipeBool == True:
            if char == "|" and closePipeBool == True:
                tempTerm += char
                eclstringout += "_"
                termDict.update({termCounter:tempTerm})
                tempTerm = ""
                closePipeBool = False
                openPipeBool = False
            else:
                tempTerm += char
                eclstringout += "_"
            # print char
        else:
            # print char, str(coi.get(char, 'empty')), ord(char)
            eclstringout += char
        counter += 1
    returnList.append(eclstringout)
    returnList.append(termDict)
    return returnList

def dictPrint(inDict, summaryBool = True):
    keylist = inDict.keys()
    keylist.sort()
    summaryString = ""
    for key in keylist:
        print "%s: %s" % (key, inDict[key])
        summaryString += str(inDict[key])
    if summaryBool:
        print summaryString

def dictExtend(growingDict, newDict):
    keylist = newDict.keys()
    keylist.sort()
    for key in keylist:
        growingDict.update({key:newDict[key]})
    return growingDict


def dictUpdateConcat(growingDict, newDict):
    keylist = newDict.keys()
    keylist.sort()
    for key1 in growingDict:
        for key2 in keylist:
            if key1 == key2:
                growingDict[key1] = newDict[key2] + " " + growingDict[key1]
    return growingDict
        

def doubleDictPrint(inDict1, inDict2, summaryBool = True):
    keylist = inDict1.keys()
    keylist.sort()
    summaryString = ""
    for key in keylist:
        print "%s: %s %s" % (key, inDict1[key], inDict2[key])
        summaryString += str(inDict1[key])
        summaryString += str(inDict2[key])
    if summaryBool:
        print summaryString

def findOperators(codeDict, mainDict):
    operatorDict = {}
    for item in codeDict:
        operator = ""
        for pos in range(4,1,-1):
            if mainDict[item-pos] == "<" or mainDict[item-pos] == "!":
                operator += mainDict[item-pos]
        operatorDict.update({item:operator})
    return operatorDict

def findSkeleton(inBraceDict, inIDDict):
    workingDict = {}
    outList = []
    refMarkerStack = []   # refinement markers (have a level, have a refinement target (may be an id, may be a pair of parens))
    braceGroupStack = []    # braceGroups (apply to a refinement marker (and thus have a level and a target), enclose refinements (equals signs))
                            #          paired with...
    refinementStack = []    # refinements (apply to a refinement marker (and thus have a level and a target), have an att, an equal and a value)
    parenStack = []        # parens (enclose a sub-refinement - thus apply to an equal), or (apply to a condis set - not covered yet)
                            #          paired with...
    ungroupedRefinements = []
    workingDict = merge_two_dicts(inBraceDict, inIDDict)
    level = 0
    refMarkerCounter = 0
    parenCounter = 0
    parenTally = 1
    braceCounter = 0
    braceTally = 1
    refinementCounter = 0
    itemCounter = 0
    levelMarker = ":"
    openParenMarker = "("
    closeParenMarker = ")"
    openBraceMarker = "{"
    closeBraceMarker = "}"
    keylist = workingDict.keys()
    keylist.sort()
    for key in keylist:
        # print str(itemCounter), keylist[itemCounter]
        # print "%s: %s" % (key, workingDict[key])
        if workingDict[key] == "(":
            parenStack.insert(0, ["P", parenCounter,parenTally, key]) # parenCounter, key, pair_key_tbd
            parenTally += 1
            parenCounter += 1
        if workingDict[key] == "{":
            braceGroupStack.insert(0, ["B", braceCounter, braceTally, key]) # parenCounter, key, pair_key_tbd
            braceTally += 1
            braceCounter += 1
        if workingDict[key] == ")":
            done = False
            for paren in parenStack:
                if len(paren) == 4 and done == False:
                    paren.extend([key])
                    done = True
                    parenCounter -= 1
        if workingDict[key] == "}":
            done = False
            for braceGroup in braceGroupStack:
                if len(braceGroup) == 4 and done == False:
                    braceGroup.extend([key])
                    done = True
                    braceCounter -= 1
        if workingDict[key] == "=":
            tempRefinement = []
            tempRefinement = ["R", key, keylist[itemCounter - 1], workingDict[keylist[itemCounter - 1]], keylist[itemCounter + 1], workingDict[keylist[itemCounter + 1]]]
            refinementStack.append(tempRefinement)
        if workingDict[key] == ":":
            tempRefinementMarker = []
            tempRefinementMarker = ["M", key, keylist[itemCounter - 1], workingDict[keylist[itemCounter - 1]], keylist[itemCounter + 1], workingDict[keylist[itemCounter + 1]]]
            refMarkerStack.append(tempRefinementMarker)
        itemCounter += 1
    parenStack.reverse()
    braceGroupStack.reverse()
    # print "refinements: ", refinementStack
    # print "refinement markers: ", refMarkerStack
    # print "paren: ", parenStack
    # print "brace: ", braceGroupStack
    # nest parentheses & braces
    nestedStack = []
    nestedStack = parenBraceNest(parenStack, braceGroupStack)
    # return A
    outList.append(nestedStack)
    # associate refinements with braces
    tempList = []
    templist = refineToBraces(refMarkerStack, refinementStack, braceGroupStack, parenStack)
    ungroupedAndParenRefinements = templist[0]
    matchedRefMarker_ParenBrace = []
    matchedRefMarker_ParenBrace = templist[1]
    # return B
    outList.append(matchedRefMarker_ParenBrace)
    # print "ungrouped refs: ", ungroupedAndParenRefinements
    # associate braces with markers
    refMarker_refBrace = []
    refMarker_brace = []
    refMarker_ref = []
    if len(parenStack) > 0:
        refMarker_brace = braceOrRefToMarkers(refMarkerStack, braceGroupStack, parenStack, nestedStack)
    else:
        refMarker_brace = braceOrRefToMarkersNoParen(refMarkerStack, braceGroupStack)
    # associate refinements with markers
    if len(parenStack) > 0:
        refMarker_ref = braceOrRefToMarkers(refMarkerStack, ungroupedAndParenRefinements, parenStack, nestedStack)
    else:
        refMarker_ref = braceOrRefToMarkersNoParen(refMarkerStack, ungroupedAndParenRefinements)
    # parens to refinements (which = sign does a parenthesis follow)
    refMarker_refBrace.extend(refMarker_brace)
    refMarker_refBrace.extend(refMarker_ref)
    # return C
    outList.append(refMarker_refBrace)
    parensToRefinement = []
    parensToRefinement = parensToRefinements(refinementStack, parenStack)
    # return D
    outList.append(parensToRefinement)
    return outList

def parensToRefinements(refinementStack, parenStack):
    parensToRefinement = []
    for paren in parenStack:
        for refinement in refinementStack:
            if refinement[4] == paren[3]:
                if debug:
                    print "D: refinement+parenthesis match: ", refinement, paren
                parensToRefinement.append([refinement, paren])
    return parensToRefinement

def braceOrRefToMarkers(refMarkerStack, refOrBraceStack, parenStack, nestedStack):
    refMarker_refBrace = []
    for refOrBrace in refOrBraceStack:
        markerMatch = []
        rangeMin = 10000
        rangeCount = 10000
        for marker in refMarkerStack:
            for paren in parenStack:
                if refOrBrace[0] == "R":
                    if (refOrBrace[1] >= marker[1]):
                        if (marker[1] < paren[4]) and (marker[1] > paren[3]) and (refOrBrace[1] < paren[4]) and (refOrBrace[1] > paren[3]) and (len(paren) == 5): # marker and ref in paren
                            rangeCount = refOrBrace[1] - marker[1]
                            if rangeCount < rangeMin:
                                markerMatch.insert(0,[marker, refOrBrace])
                                rangeMin = rangeCount
                        if ((marker[1] > paren[4]) or (marker[1] < paren[3])) and ((refOrBrace[1] > paren[4]) or (refOrBrace[1] < paren[3])) and (len(paren) == 5): # marker and ref out of paren
                            rangeCount = refOrBrace[1] - marker[1]
                            if rangeCount < rangeMin:
                                markerMatch.insert(0,[marker, refOrBrace])
                                rangeMin = rangeCount
                        else:
                            markerMatch.append([marker, refOrBrace])
                if refOrBrace[0] == "B":         
                    if (refOrBrace[3] >= marker[1]):
                        rangeCount = refOrBrace[3] - marker[1]
                        if rangeCount < rangeMin:
                            markerMatch.insert(0,[marker, refOrBrace, paren])
                            rangeMin = rangeCount
        if len(markerMatch) > 0:
            if debug:
                print "C: refmarker+ref/brace match: ", markerMatch[0][0], markerMatch[0][1]
            refMarker_refBrace.append([markerMatch[0][0], markerMatch[0][1]])
    return refMarker_refBrace

def braceOrRefToMarkersNoParen(refMarkerStack, refOrBraceStack):
    for refOrBrace in refOrBraceStack:
        markerMatch = []
        rangeMin = 10000
        rangeCount = 10000
        for marker in refMarkerStack:
            if refOrBrace[0] == "R":
                if (refOrBrace[1] >= marker[1]):
                    rangeCount = refOrBrace[1] - marker[1]
                    if rangeCount < rangeMin:
                        markerMatch.insert(0,[marker, refOrBrace])
                        rangeMin = rangeCount
            if refOrBrace[0] == "B":         
                if (refOrBrace[3] >= marker[1]):
                    rangeCount = refOrBrace[3] - marker[1]
                    if rangeCount < rangeMin:
                        markerMatch.insert(0,[marker, refOrBrace])
                        rangeMin = rangeCount
        if debug:
            print "refmarker+ref/brace match: ", markerMatch[0][0], markerMatch[0][1]

def refineToBraces(refMarkerStack, refinementStack, braceGroupStack, parenStack):
    ungroupedAndParenRefinements = []
    mergedBracesAndParens = []
    matchedRefMarker_ParenBrace = []
    returnList = []
    for item in braceGroupStack:
        mergedBracesAndParens.append(item)
    for item in parenStack:
        mergedBracesAndParens.append(item)
    refinementOrMarkerStack = []
    for item in refinementStack:
        refinementOrMarkerStack.append(item)
    for item in refMarkerStack:
        refinementOrMarkerStack.append(item)
    for refinementOrMarker in refinementOrMarkerStack:
        matched = False
        mayMatch = False
        braceList = []
        rangeMin = 10000
        for braceGroup in mergedBracesAndParens:
            # if testing a marker
            if refinementOrMarker[0] == "M":
                if (refinementOrMarker[1] > braceGroup[3]) and (refinementOrMarker[1] < braceGroup[4]):
                    if len(braceGroup) == 5:
                        matched = True
                    else:
                        mayMatch = True
                        matched = True
                    if mayMatch:
                        for nestedBraceGroup in braceGroup[5]:
                            if (refinementOrMarker[1] > nestedBraceGroup[3]) and (refinementOrMarker[1] < nestedBraceGroup[4]):
                                matched = False
                    if matched == True:
                        rangeCount = braceGroup[4] - braceGroup[3]
                        if rangeCount < rangeMin:
                            braceList.insert(0,[braceGroup, refinementOrMarker])
                            rangeMin = rangeCount
                    mayMatch = False
            # if testing a refinement
            if refinementOrMarker[0] == "R":
                if (refinementOrMarker[1] > braceGroup[3]) and (refinementOrMarker[1] < braceGroup[4]):
                    matched = True
                    rangeCount = braceGroup[4] - braceGroup[3]
                    if rangeCount < rangeMin:
                        braceList.insert(0,[braceGroup, refinementOrMarker])
                        rangeMin = rangeCount
        if matched == True:
            if braceList[0][1][3] == "&":
                if debug:
                    print "B: match ref/marker to paren/brace: ", braceList[0][0], braceList[0][1], "BASE MARKER"
                matchedRefMarker_ParenBrace.append([braceList[0][0], braceList[0][1], 1])
            else:
                if debug:
                    print "B: match ref/marker to paren/brace: ", braceList[0][0], braceList[0][1]
                matchedRefMarker_ParenBrace.append([braceList[0][0], braceList[0][1], 0])
            if braceList[0][0][0] == "P":
                ungroupedAndParenRefinements.append(braceList[0][1])
        if matched == False:
            if debug:
                print "unmatched ref: ", refinementOrMarker
            ungroupedAndParenRefinements.append(refinementOrMarker)
    returnList.append(ungroupedAndParenRefinements)
    returnList.append(matchedRefMarker_ParenBrace)
    return returnList

def parenBraceNest(parenStack, braceGroupStack):
    inList = []
    inList.extend(parenStack)
    inList.extend(braceGroupStack)
    outList = []
    # print inList
    for item1 in inList:
        qualifyingItems = []
        for item2 in inList:
            if (item2[3] > item1[3]) and (item2[4] < item1[4]):
                qualifyingItems.append(item2)
        if len(qualifyingItems) > 0:
            item1.append(qualifyingItems)
    for item in inList:
        if len(item) == 5:
            if debug:
                print "A: do order: 1", item
            outList.append(['1', item])
        else:
            if debug:
                print "A: do order:", str(len(item[5]) + 1), item
            outList.append([str(len(item[5]) + 1), item])
    return outList
    


    return outList

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z
