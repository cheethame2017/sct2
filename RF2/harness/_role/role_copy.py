from shutil import copyfile

# copies role files to top level for processing. 
# continue to edit in the _role folder
# and then delete before staging/syncing changes

role_file_list = ['grammarFuncs.py',
                'grammarProcess.py',
                'SCTRF2_role.py',
                'SCTRF2RoleFuncs.py',
                'SetTest.py']

for role_file in role_file_list:
    copyfile("/home/ed/git/rf2repo/sct/RF2/harness/_role/" + role_file, "/home/ed/git/rf2repo/sct/RF2/" + role_file)


