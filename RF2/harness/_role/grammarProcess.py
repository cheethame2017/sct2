from SetTest import *

debug = False

def buildProcessSequence(skeletonList):
    sequenceList = skeletonList[0]
    markerRefToParenBrace = skeletonList[1]
    refOrBraceToMarker = skeletonList[2]
    parenToRefinement = skeletonList[3]
    conceptOperator = skeletonList[4]
    conceptId = skeletonList[5]

    setList = []
    sequenceList.sort(key=lambda x: x[0])

    # NEEDS TO HANDLE >1 GROUP; SEES THEM IN SKELETONLIST, BUT LOST LATER
    for item in sequenceList:
        for refPB in markerRefToParenBrace:
            resultSet = []
            # if marker for this step in the sequence...
            if refPB[0] == item[1]:
                # if its a base marker entry
                if (refPB[2] == 1) and (item[0] == '1'):
                    if len(markerRefToParenBrace) == 1:
                        if debug:
                            print "i: base - calculate this row only", refPB
                        print "calculate (simple):"
                        print conceptOperator[refPB[1][4]], conceptId[refPB[1][4]]
                        calculateSimple(conceptOperator[refPB[1][4]], conceptId[refPB[1][4]])
                    else:
                        if debug:
                            print "ii: base > present results:", refPB
                        # resolve 'and' logic in setlist
                        resolve(refPB, setList)
                if (refPB[2] == 1) and not(item[0] == '1'):
                    if debug:
                        print "iii: base - list result", refPB
                    # resolve 'and' logic in setlist
                    resolve(refPB, setList)
                # else it's not a base marker entry
                else:
                    if (refPB[0][0] == "P") and (refPB[1][0] == "R"):
                        if debug:
                            print "iv-R: process this refinement:", item[0], refPB, "+ pass up results"
                        resultList = checkRefPWithMarker(refPB, refOrBraceToMarker)
                        if refPB[1][5] == "(":
                            # callout to method with setlist to pass results of paren'd refinement into [4][5] of paren currently showing as  '*', '(' (? 74-75 match e.g.) need more nesting as example
                            passUpResult = []
                            passUpResult = marryResultWithParen(setList, refPB)
                            setList.append([resultList[0], conceptOperator[resultList[0][2]], resultList[0][3], conceptOperator[refPB[1][2]], refPB[1][3], "*", passUpResult, "U"])
                        else:
                            setList.append([resultList[0], conceptOperator[resultList[0][2]], resultList[0][3], conceptOperator[refPB[1][2]], refPB[1][3], conceptOperator[refPB[1][4]], refPB[1][5], "U"])
                        if debug:
                            print "vi: ", setList
                    if (refPB[0][0] == "B") and (refPB[1][0] == "R"):
                        if debug:
                            print "iv-B: process this brace:", item[0], refPB, "+ pass up results"
                        resultList = checkRefBWithMarker(refPB, refOrBraceToMarker)
                        if refPB[1][5] == "(":
                            # callout to method with setlist to pass results of paren'd refinement into [4][5] of paren currently showing as  '*', '(' (? 74-75 match e.g.) need more nesting as example
                            passUpResult = []
                            passUpResult = marryResultWithParen(setList, refPB)
                            setList.append([resultList[0], conceptOperator[resultList[0][2]], resultList[0][3], conceptOperator[refPB[1][2]], refPB[1][3], "*", passUpResult, "G"])
                        else:
                            if not [resultList[0], conceptOperator[resultList[0][2]], resultList[0][3], conceptOperator[refPB[1][2]], refPB[1][3], conceptOperator[refPB[1][4]], refPB[1][5], "G"]:
                                setList.append([resultList[0], conceptOperator[resultList[0][2]], resultList[0][3], conceptOperator[refPB[1][2]], refPB[1][3], conceptOperator[refPB[1][4]], refPB[1][5], "G"])
                        if (refOrBraceToMarker[0][0][5] == "{") and not(refPB[1][5] == "("):
                            # callout to method with setlist to pass results of paren'd refinement into [4][5] of paren currently showing as  '*', '(' (? 74-75 match e.g.) need more nesting as example
                            # passUpResult = []
                            # passUpResult = marryResultWithParen(setList, refPB)
                            if not [resultList[0], conceptOperator[resultList[0][2]], resultList[0][3], conceptOperator[refPB[1][2]], refPB[1][3], conceptOperator[refPB[1][4]], refPB[1][5], "G"] in setList:
                                setList.append([resultList[0], conceptOperator[resultList[0][2]], resultList[0][3], conceptOperator[refPB[1][2]], refPB[1][3], conceptOperator[refPB[1][4]], refPB[1][5], "G"])
                        if debug:
                            print "vi: ", setList

def checkRefPWithMarker(refPB, refOrBraceToMarker):
    returnList = []
    valueList = []
    for item in refOrBraceToMarker:
        if refPB[1]  == item[1]:
            returnList.append(item[0])
    return returnList

def checkRefBWithMarker(refPB, refOrBraceToMarker):
    returnList = []
    valueList = []
    for item in refOrBraceToMarker:
        if refPB[0]  == item[1]:
            returnList.append(item[0])
    return returnList

def resolve(refPB, setList):
    calcList = []
    mainList = []
    for item in setList:
        if item[0][2] == refPB[1][4]:
            calcList.append([item[1], item[2], item[3], item[4], item[5], item[6], item[7]])
    print "calculate (resolve): "
    firstTime = True
    for item in calcList:
        if item[6] == "U":
            if debug:
                print item, "ungrouped"
            candidateRows = []
            candidateCons = []
            candidateRows = SourceOrDestinationFirst(item)
            for row1 in candidateRows:
                for row2 in row1:
                    if not isinstance(row2, int):
                        if firstTime:
                            mainList.append(row2[1])
                        else:
                            candidateCons.append(row2[1])
            candidateSet = set(candidateCons)
            if firstTime:
                firstTime = False
            else:
                mainList = AND(set(mainList), candidateSet)
        elif item[6] == "G":
            if debug:
                print item, "grouped"
            candidateRows = []
            candidateCons = []
            candidateRows = SourceOrDestinationFirst(item)
            for row1 in candidateRows:
                for row2 in row1:
                    if not isinstance(row2, int):
                        if firstTime:
                            if not str(row2[0])+"|"+row2[1] in mainList:
                                mainList.append(str(row2[0])+"|"+row2[1])
                        else:
                            if not str(row2[0])+"|"+row2[1] in candidateCons:
                                candidateCons.append(str(row2[0])+"|"+row2[1])
            # candidateSet = set(candidateCons)
            if firstTime:
                firstTime = False
            else:
                mainList = AND(set(mainList), set(candidateCons))
        # print mainList
        # print candidateCons
    if calcList[0][6] == "U":
        listResults('ungrouped simple refinement', mainList, True)
    else:
        groupList = []
        for item in mainList:
            groupList.append(item.split("|")[1])
        listResults('grouped simple refinement', groupList, True)
            

def marryResultWithParen(setList, refPB):
    resultList = []
    returnList = []
    candidateSet = ()
    print "in marry"
    for item in setList:
        if (item[0][2] == (refPB[1][4] + 1)) or (item[0][2] == (refPB[1][4] + 2)) or (item[0][2] == (refPB[1][4] + 3)) or (item[0][2] == (refPB[1][4] + 4)) or (item[0][2] == (refPB[1][4] + 5)): # if id immediately follows brace...
            resultList = [item[1],item[2],item[3],item[4],item[5],item[6]]
            candidateRows = []
            candidateCons = []
            candidateRows = SourceOrDestinationFirst(resultList)
            for row1 in candidateRows:
                for row2 in row1:
                    if not isinstance(row2, int):
                        candidateCons.append(row2[1])
            candidateSet = set(candidateCons)
    returnList.append(candidateSet)
    # returnList.append(resultList)
    print "calculate (and pass up):"
    if debug:
        print resultList
    return returnList

def calculateSimple(inOperator, inId):
    resultset = []
    parents = []
    parents = parentsFromId_IN(tuplify(inId))
    if (inId == tok('ROOT')) or (parents[0][1] == tok('ROOT')):
        print "No, I'm not doing that!"
    else:
        if inOperator == "<":
            resultset = descendants(inId)
            listResults('descendants', resultset, True)
        elif inOperator == "<<":
            resultset = descendantsAndSelf(inId)
            listResults('descendants and self', resultset, True)
        elif inOperator == "":
            resultset = Self(inId)
            listResults('self', resultset, True)





