import os

role_file_list = ['grammarFuncs.py',
                'grammarProcess.py',
                'SCTRF2_role.py',
                'SCTRF2RoleFuncs.py',
                'SetTest.py']

for role_file in role_file_list:
    os.remove("/home/ed/git/rf2repo/sct/RF2/" + role_file)