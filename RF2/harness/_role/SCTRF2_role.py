#!/usr/bin/env python

from SCTRF2Funcs import *
from platformSpecific import *
from SCTRF2RoleFuncs import *

def browseFromRootCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> in browseFromRootCall'
    callId = ('138875005',)
    snapHeaderWrite(callId[0])
    snapshotHeader()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString


def browseFromIdCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> in browseFromIdCall'
    preCallId = []
    while True:
        prePreCallId = (raw_input('EnterId: '))
        if len(prePreCallId) > 0:
            preCallId.append(prePreCallId)
            callId = tuple(preCallId)
            breadcrumbsWrite(callId[0])
            focusSwapWrite(callId[0])
            clearScreen()
            snapshotHeader()
            returnString = browseFromId(callId)
            return returnString
        else:
            clearScreen()
            snapshotHeader()
            returnString = browseFromId(callId)


def browseFromIndexCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> in browseFromIndexCall'
    snapshotHeader()
    callId = inString
    if isinstance(callId, str):
        callId = listify(callId.strip())
    breadcrumbsWrite(callId[0])
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString


def showTermsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowTermsCall'
    snapshotHeader()
    callId = inString
    returnString = allTermsFromConId(callId)
    if debug:
        print returnString
    return returnString


def showTermsMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowTermsMetaCall'
    snapshotHeader()
    callId = inString
    returnString = allTermsFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString

def showSelectedRoles():
    if not debug:
        clearScreen()
    if debug:
        print '-> in showSelectedRoles'
    snapshotHeader()
    returnString = eclDisplay(True, False, True)
    return returnString

def reloadShowSelectedRoles():
    if not debug:
        clearScreen()
    if debug:
        print '-> in reloadShowSelectedRoles'
    snapshotHeader()
    returnString = eclDisplay(True, False, False)
    return returnString

def processConstraintCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> in processConstraintCall'
    snapshotHeader()
    returnString = eclProcess()
    return returnString

returnString = {}
debug = False
headerWriterSnap()
clearScreen()
# returnString = browseFromRootCall()
# main control code
firstTime = True
while True:
    if firstTime:
        topaction = "a"
        firstTime = False
    else:
        topaction = raw_input('Action:\n\
[a] Probe from Snap\t[b] Reload probe\t[c] Browse from root\n\
[d] Browse from id\t[e] (+row) Terms  \t[f] Process constraint\n\
[g] (+row) Term+meta\t[h] XXXXXXXXXXX\t[i] XXXXXXXXXXX\n\
[j] XXXXXXXXXXX \t[k] XXXXXXXXXXX\t[l] XXXXXXXXXXX\n\
[m] XXXXXXXXXXX\t[n] XXXXXXXXXXX\t[o] (+r) XXXXXXXXXXX\n\
[p] XXXXXXXXXXX\t[q] XXXXXXXXXXX\t[r] XXXXXXXXXXX\n\
[s] XXXXXXXXXXX\t[t] XXXXXXXXXXX\t[u] XXXXXXXXXXX\n\
[v] XXXXXXXXXXX\t[w] XXXXXXXXXXX\t[x] XXXXXXXXXXX\n\
[zz] Exit\n\
Input selection: ').strip()
    if topaction == 'zz':
        if debug:
            print returnString
        clearScreen()
        break  # breaks out of the while loop and ends session
    elif topaction == 'a':
        if debug:
            print returnString
            print '-> calling showSelectedRoles'
        returnString = showSelectedRoles()
    elif topaction == 'b':
        if debug:
            print returnString
            print '-> calling reloadShowSelectedRoles'
        returnString = reloadShowSelectedRoles()
    elif topaction == 'c':
        if debug:
            print returnString
            print '-> calling browseFromRootCall'
        returnString = browseFromRootCall()
    elif topaction == 'd':
        if debug:
            print returnString
            print '-> calling browseFromIdCall'
        returnString = browseFromIdCall()
    elif topaction == 'f':
        if debug:
            print returnString
            print '-> calling eclProcess'
        returnString = processConstraintCall()

    elif topaction == 'xx':  # toggle *most* debug outputs
        if debug:
            debug = False
            print 'debug OFF'
        else:
            debug = True
            print 'debug ON'
#
    else:
        try:
            if topaction[0] == 'e':
                if debug:
                    print 'e', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showTermsCall(callThing[0])
            elif topaction[0] == 'g':
                if debug:
                    print 'g', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showTermsMetaCall(callThing[0])
#
            else:
                if debug:
                    print ' ', returnString[int(topaction)]
                returnString = browseFromIndexCall(returnString[int(topaction)])
            True
        except:
            True
