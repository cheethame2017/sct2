def multiLayerHistoryOrigins(historyEdges, ancestorNodes, inOption, inLookup, movedFromBool, testList, level):
    # before/after setup
    historyEdgesIn = historyEdges[:]
    testListIn = testList[:]
    ancestorNodesIn = ancestorNodes[:]
    level += 1
    while level < 1000: # arbitrary end point
        for item in testList:
            historyOrigins = historicalDestinationsFromConId_IN(item, inOption, inLookup, movedFromBool)
            for historyOrigin in historyOrigins:
                if historyOrigin[1][-2] == "1":
                    historyOrigin1 = CiDFromDiD_IN(tuplify(historyOrigin[1]))
                else:
                    historyOrigin1 = historyOrigin[1]
                append_distinct(historyEdges, (historyOrigin1, historyOrigin[2], historyOrigin[0]))
                append_distinct(ancestorNodes, (historyOrigin1, pterm(historyOrigin1)))
                append_distinct(ancestorNodes, (historyOrigin[2], pterm(historyOrigin[2])))
                ancestorNodesTemp = []
                if conMetaFromConId_IN(historyOrigin1, False)[1] == 1:
                    ancestorNodesTemp = ancestorsFromConId_IN(historyOrigin1, True)
                    for ancNode in ancestorNodesTemp:
                        append_distinct(ancestorNodes, ancNode)
                if conMetaFromConId_IN(historyOrigin1, False)[1] == 0:
                    append_distinct(testList, historyOrigin1)
        # conditional end point
        if (historyEdgesIn == historyEdges) and (ancestorNodesIn == ancestorNodes) and (testListIn == testList):
            level = 1000
        # recursive call
        [historyEdges, ancestorNodes, testList, level] = multiLayerHistoryOrigins(historyEdges, ancestorNodes, inOption, inLookup, movedFromBool, testList, level)
    # end return
    return [historyEdges, ancestorNodes, testList, level]

def dotDrawAncestors(inId, active, inDict=0, inDir="BT"):
    if inDict != 0:
        key_list = list(inDict.keys())
        val_list = list(inDict.values())
    ancestorNodes = []
    ancestorEdges = []
    writeableNodes = []
    historyTargets = []
    historyEdges = []
    historyAncestorNodes = []
    refersToBool = refersToRowsFromConIdFocusCheck(tuplify(inId))
    if active == 1 and not refersToBool:
        ancestorNodes = ancestorsFromConId_IN(inId, True)
        if cfg("WORD_EQUIV") > 2 and (not inId == tok["ROOT"]) and (not inId == "370136006"): # overload of this config
            sendToHistoryNodes = ancestorNodes[:]
            historyEdges, backFromHistoryNodes = singleLayerHistoryOrigins(historyEdges, sendToHistoryNodes, inId, 1, 0, True)
            # historyEdgesAndAncestorNodesList = multiLayerHistoryOrigins(historyEdges, sendToHistoryNodes, 1, 0, True, [inId], 1)
            historyAncestorNodes = [node for node in backFromHistoryNodes if node not in ancestorNodes]
            ancestorNodes = backFromHistoryNodes[:]
    elif active == 1 and refersToBool:
        ancestorNodes = ancestorsFromConId_IN(inId, True)
        if cfg("WORD_EQUIV") > 1: # overload of this config
            sendToHistoryNodes = ancestorNodes[:]
            historyEdgesAndAncestorNodesList = resolveHistoryTargets(historyEdges, sendToHistoryNodes, [inId], 1)
            historyEdges = historyEdgesAndAncestorNodesList[0]
            historyAncestorNodes = [node for node in historyEdgesAndAncestorNodesList[1] if node not in ancestorNodes]
            ancestorNodes = [*ancestorNodes, *historyEdgesAndAncestorNodesList[1]] # needs improvement
        if cfg("WORD_EQUIV") > 2 and (not inId == tok["ROOT"]) and (not inId == "370136006"): # overload of this config
            sendToHistoryNodes = ancestorNodes[:]
            historyEdges, backFromHistoryNodes = singleLayerHistoryOrigins(historyEdges, sendToHistoryNodes, inId, 1, 0, True)
            # historyEdgesAndAncestorNodesList = multiLayerHistoryOrigins(historyEdges, sendToHistoryNodes, 1, 0, True, [inId], 1)
            ancestorNodes = backFromHistoryNodes[:]
            ...