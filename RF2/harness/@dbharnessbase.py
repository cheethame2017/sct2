import sqlite3

def dbHarness1(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("SQL string here'")
    recordset = cur.fetchall()

    for item in recordset:
        print(item)

    cur.close
    conn.close

# define dbpath1
dbPath1 = "full database path"

# call harness
dbHarness1(dbPath1)
