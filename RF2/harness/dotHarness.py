#!/usr/local/bin/python
# -*- coding: utf-8 -*-

# identical to code 2261 ≡
# subset of code 2291 ⊑

def toTempList(inTempList, inNotes, inCounter, inIdent, inGroup, inList, inLen):
    inTempList.append(inNotes)
    inTempList.append(inCounter)
    inTempList.append(inIdent) 
    inTempList.append(inGroup)
    inTempList.append(inList)
    inTempList.append(inLen)
    return inTempList

def clean(inString):
    outString = inString.strip()
    outString = outString.replace("{","")
    outString = outString.replace("}","")
    outString = outString.replace("= ","") # shortens ===
    outString = outString.replace("==","") # removes last bit
    outString = outString.replace("<<<","") # removes last bit
    return outString

inLines = []
outLines = []
inGroup = False
groupCounter = 0
lineCounter = 0
tempLine = ""
try:
    f = open('files/pre_dot.txt', 'r')
    # focus
    inLines = f.readlines()
finally:
    f.close()

for line in inLines:
    tempList = []
    if "{" in line:
        inGroup = True
        groupCounter +=1
    if inGroup == False:
        if lineCounter == 0:
            # print str(lineCounter), "FOCUS", [clean(item) for item in line.strip().split("|")], len(line.strip().split("|"))
            tempList = toTempList(tempList, tempLine, str(lineCounter), "FOCUS", str(groupCounter), [item.strip() for item in line.strip().split("|")], len(line.strip().split("|")))
        else:
            if "<<<" in line:
                tempLine = "PRIM"
            elif "===" in line:
                tempLine = "FD"
            else:
                tempLine = "NTC"
    if not lineCounter == 0:
        # print tempLine, str(lineCounter), inGroup, str(groupCounter), [item.strip() for item in line.strip().split("|")], len(line.strip().split("|"))
        tempList = toTempList(tempList, tempLine, str(lineCounter), inGroup, str(groupCounter), [clean(item) for item in line.strip().split("|")], len(line.strip().split("|")))
    if "}" in line:
        inGroup = False
    lineCounter += 1
    tempLine = ""
    outLines.append(tempList)

focusList = []
definedStatus = False
parentList = []
soleRoleList = []
groupedRoleList = []
groupNodes = []
attNameList = []
objValueList = []
bunchNodes = []
dotEdgeList = []
invisEdgeList = []

bunchNodes.append("\"BN0\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black];")

for line in outLines:
    # focus
    if line[2] == "FOCUS":
        # print "FOCUS", line
        focusList.append(line)
    elif line[2] == False and line[0] == "FD":
        # print "FD"
        definedStatus = True
        parentList.append(line)
    elif line[2] == False and line[0] == "PRIM":
        # print "PRIM"
        parentList.append(line)
    elif int(line[3]) > 0 and not line[0] == "NTC":
        # print line[3], line
        groupedRoleList.append(line)
        if not line[3] in groupNodes:
            if not "\"GN" + line[3] + "\" [shape=circle, fontsize=1, width=0.5, label=\"\" style=filled, fillcolor=white];" in groupNodes:
                groupNodes.append("\"GN" + line[3] + "\" [shape=circle, fontsize=1, width=0.5, label=\"\" style=filled, fillcolor=white];")
            if not "\"BN" + line[3] + "\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black];" in bunchNodes:
                bunchNodes.append("\"BN" + line[3] + "\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black];")
    elif line[0] == "NTC" and line[5] == 5:
        # print "NTC", line
        soleRoleList.append(line)
    elif line[0] == "NTC" and line[5] == 3:
        parentList.append(line)

print "Focus"
for item in focusList:
    print item
    objValueList.append("\"" + item[4][0] + "\" [label=\"" + item[4][0] + "\l" + item[4][1] + "\", shape=box, peripheries=2, style=filled, fillcolor=\"#CCCCFF\"];")
    objValueList.append("\"138875005\" [label=\"138875005\lSNOMED CT Concept\", style=invis];")
    invisEdgeList.append("\"" + item[4][0] + "\"-> \"138875005\" [style=invis];")
print "Defined status"
print definedStatus
if definedStatus == True:
    groupNodes.append("\"DN\" [shape=circle, fontsize=9, width=0.5, image=\"img/IdenticalTo.png\", label=\"\", style=filled, fillcolor=white];")
else:
    groupNodes.append("\"DN\" [shape=circle, fontsize=9, width=0.5, image=\"img/SubsetOf.png\", label=\"\", style=filled, fillcolor=white];")
print "Parents"
for item in parentList:
    print item
    dotEdgeList.append("\"" + focusList[0][4][0] + "\"->\"" + "DN" + "\";")
    dotEdgeList.append("\"" + "DN" + "\"->\"" + "BN0" + "\";")
    dotEdgeList.append("\"" + "BN0" + "\"->\"" + item[4][0] + "\";")
    objValueList.append("\"" + item[4][0] + "\" [label=\"" + item[4][0] + "\l" + item[4][1] + "\", shape=box, peripheries=2, style=filled, fillcolor=\"#CCCCFF\"];")
    invisEdgeList.append("\"" + item[4][0] + "\"-> \"138875005\" [style=invis];")
print "Sole roles"
for item in soleRoleList:
    print item
    dotEdgeList.append("\"" + "BN0" + "\"->\"" + item[4][0] + "\";")
    dotEdgeList.append("\"" + item[4][0] + "\"->\"" + item[4][2] + "\";")
    attNameList.append("\"" + item[4][0] + "\" [label=\"" + item[4][0] + "\l" + item[4][1] + "\", shape=box, peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"];")
    objValueList.append("\"" + item[4][2] + "\" [label=\"" + item[4][2] + "\l" + item[4][3] + "\", shape=box, peripheries=2, style=filled, fillcolor=\"#CCCCFF\"];")
    invisEdgeList.append("\"" + item[4][2] + "\"-> \"138875005\" [style=invis];")
print "Grouped roles"
for item in groupedRoleList:
    print item
    dotEdgeList.append("\"" + "BN0" + "\"->\"" + "GN" + item[3] + "\";")
    dotEdgeList.append("\"" + "GN" + item[3] + "\"->\"" + "BN" + item[3] + "\";")
    dotEdgeList.append("\"" + "BN" + item[3] + "\"->\"" + item[4][0] + "_" + item[3] + "\";")
    dotEdgeList.append("\"" + item[4][0] + "_" + item[3] + "\"->\"" + item[4][2] + "_" + item[3] + "\";")
    attNameList.append("\"" + item[4][0] + "_" + item[3] + "\" [label=\"" + item[4][0] + "\l" + item[4][1] + "\", shape=box, peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"];")
    objValueList.append("\"" + item[4][2] + "_" + item[3] + "\" [label=\"" + item[4][2] + "\l" + item[4][3] + "\", shape=box, peripheries=2, style=filled, fillcolor=\"#CCCCFF\"];")
    invisEdgeList.append("\"" + item[4][2] + "_" + item[3] + "\"-> \"138875005\" [style=invis];")
print
for item in dotEdgeList:
    print item
for item in bunchNodes:
    print item
for item in groupNodes:
    print item
for item in attNameList:
    print item

try:
    f = open('files/conpic.dot', 'w')
    f.write("strict digraph G { rankdir=LR; ranksep=.3; splines=ortho; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
    for item in dotEdgeList:
        f.write(item + "\n")
    # for item in invisEdgeList:
    #     f.write(item + "\n")
    for item in bunchNodes:
        f.write(item + "\n")
    for item in groupNodes:
        f.write(item + "\n")
    for item in attNameList:
        f.write(item + "\n")
    for item in objValueList:
        f.write(item + "\n")
    f.write("}")
finally:
    f.close()

