import sqlite3
from SCTRF2Helpers import parentIdsFromId_IN, fsnFromConId_IN, descendantCountFromConId_IN
from SCTRF2Role import writeOutList
from SCTRF2config import tok
from platformSpecific import cfgPathInstall, sep

def joinMaxIdToCount(count, tag, idMatchList):
    for item in idMatchList:
        if descendantCountFromConId_IN(item) == count:
            fsn = fsnFromConId_IN(item)[1]
            insertPoint1 = fsn.rfind("(")
            fsnTag = fsn[insertPoint1:][1:-1]
            if fsnTag == tag:
                return item

def compareTags(RF2Outpath):
    conn = sqlite3.connect(RF2Outpath)
    cur2 = conn.cursor()
    tempList = []
    # no drugs or physical objects
    cur2.execute("SELECT t1.SupertypeId as A, t1.STCount as B FROM TCSubCount t1 \
                EXCEPT \
                SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                WHERE t2.supertypeId IN ('" + tok("PHARM_PROD") + "', '" + tok("PHYS_OBJ") + "') \
                and t2.SubtypeId=t3.supertypeId order by B desc;")
    recordsetIn2 = cur2.fetchall()
    setToRun = [[item[0]] for item in recordsetIn2[:]]
    progress = 1
    changeList = []
    topIdList = []
    idMatchList = []
    tagCountDict = {}
    tagItemDict = {}
    topIdDict = {}
    topNumDict = {}
    tagCache = {}
    descCountCache = {}
    topNumCounterDict = {}
    topNumCounterDict['SNOMED RT+CTV3'] = [1, tok("ROOT")]
    tot = len(setToRun)
    for row in setToRun:
        tempList = parentIdsFromId_IN(row)
        for item in tempList:
            if item[0] not in tagCache:
                fsn1 = fsnFromConId_IN(item[0])[1]
                insertPoint1 = fsn1.rfind("(")
                fsnTag1 = fsn1[insertPoint1:][1:-1]
                tagCache[item[0]] = fsnTag1
            else:
                fsnTag1 = tagCache[item[0]]
            if row[0] not in tagCache:
                fsn2 = fsnFromConId_IN(row[0])[1]
                insertPoint2 = fsn2.rfind("(")
                fsnTag2 = fsn2[insertPoint2:][1:-1]
                tagCache[row[0]] = fsnTag2
            else:
                fsnTag2 = tagCache[row[0]]
            if fsnTag2 not in tagCountDict:
                tagCountDict[fsnTag2] = 1
                tagItemDict[fsnTag2] = [row[0]]
            elif row[0] not in tagItemDict[fsnTag2]:
                tagItemDict[fsnTag2].append(row[0])
                tagCountDict[fsnTag2] += 1
            if fsnTag1 != fsnTag2: # if a tag handoff...
                if [fsnTag1, fsnTag2] not in changeList: # track any tag changes for dot file.
                    changeList.append([fsnTag1, fsnTag2])
                if item[0] not in descCountCache: # cache descendant counts
                    desc = descendantCountFromConId_IN(item[0]) # descendant count of parent
                    descCountCache[item[0]] = desc
                else:
                    desc = descCountCache[item[0]]
                if fsnTag1 + "|" + fsnTag2 not in topIdDict: # if not in id, add as list
                    topIdDict[fsnTag1 + "|" + fsnTag2] = [[item[0], row[0]]]
                    topIdList.append([fsnTag1, fsnTag2, topIdDict[fsnTag1 + "|" + fsnTag2]])
                    print("add    :", row, str(desc), fsnTag1, fsnTag2)
                    idMatchList.append(row[0])
                if fsnTag1 + "|" + fsnTag2 not in topNumDict: # if not in num, add as num
                    topNumDict[fsnTag1 + "|" + fsnTag2] = desc
                elif desc > topNumDict[fsnTag1 + "|" + fsnTag2]: # if new bigger num, replace number and name
                    topNumDict[fsnTag1 + "|" + fsnTag2] = desc # replace number
                    topIdDict[fsnTag1 + "|" + fsnTag2] = [[item[0], row[0]]] # replace name and id list in dict
                    topIdList = [topIdrow for topIdrow in topIdList if ([topIdrow[0], topIdrow[1]] != [fsnTag1, fsnTag2])]
                    topIdList.append([fsnTag1, fsnTag2, topIdDict[fsnTag1 + "|" + fsnTag2]])
                    idMatchList.append(row[0])
                    print("replace:", row, str(desc), fsnTag1, fsnTag2)
                elif (desc == topNumDict[fsnTag1 + "|" + fsnTag2]) and ([item[0], row[0]] not in topIdDict[fsnTag1 + "|" + fsnTag2]): # if same sized biggest, add to list
                    topIdDict[fsnTag1 + "|" + fsnTag2].append([item[0], row[0]])
                    print("extend :", row, str(desc), fsnTag1, fsnTag2)

                desc2 = descendantCountFromConId_IN(row[0]) # descendant count of child
                if (fsnTag2 not in topNumDict) or (desc2 > topNumDict[fsnTag2]): # add individual tag counts and track occurrences of that number or replace
                    topNumDict[fsnTag2] = desc2
                    topNumCounterDict[fsnTag2] = [1, [row[0]]]
                elif desc2 == topNumDict[fsnTag2] and (row[0] not in topNumCounterDict[fsnTag2][1]): # increment
                    topNumCounterDict[fsnTag2][0] += 1
                    topNumCounterDict[fsnTag2][1].append(row[0])

        if (progress % 500 == 0):
            print(progress, "/", tot)
        progress += 1

    maxIdList = []
    maxIdDict = {}
    childNumDict = {}
    topIdDictKeys = sorted(list(topIdDict.keys()))
    topNumDictKeys = sorted(list([key for key in topNumDict if "|" not in key]))
    childKeys = sorted(list(set([key.split("|")[1] for key in topIdDictKeys])))

    for topNumDictKey in topNumDictKeys: # fish out child concept highest descendant counts
        for childKey in childKeys:
            if childKey == topNumDictKey:
                if childKey not in childNumDict:
                    childNumDict[childKey] = topNumDict[topNumDictKey]
                elif topNumDict[topNumDictKey] > childNumDict[childKey]:
                    childNumDict[childKey] = topNumDict[topNumDictKey]

    print()
    maxIdList.append(tok("ROOT"))
    maxIdDict['SNOMED RT+CTV3'] = tok("ROOT")
    for childKey in childKeys:
        maxId = joinMaxIdToCount(childNumDict[childKey], childKey, idMatchList)
        maxIdList.append(maxId)

    writeOutList(maxIdList, cfgPathInstall() + 'files' + sep() + 'misc' + sep() + 'tagList.txt') # write out maximum 'trimmed best match' concepts to tagList

    cur2.close()
    conn.close()

if __name__ == '__main__':
    # working paths
    homePath = '/home/ed/'
    repoPath = homePath + 'Code/Repos/sct/RF2/'
    miscPath = repoPath + 'files/misc/'
    RF2Outpath = homePath + 'Data/RF2Out.db'
    compareTags(RF2Outpath)
