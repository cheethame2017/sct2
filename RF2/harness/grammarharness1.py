from grammarFuncs import *
from grammarProcess import *

eclstring1 = """< 404684003 |clinical finding|:
    { 363698007 |finding site| = << 39057004 |pulmonary valve structure|,
      116676008 |associated morphology| = << 415582006 |stenosis|},
    { 363698007 |finding site| = << 53085002 |right ventricular structure|, 
      116676008 |associated morphology| = << 56246009 |hypertrophy|}"""
eclstring3 =  """< 404684003 |clinical finding|:
47429007 |associated with| = (< 404684003 |clinical finding|: 
116676008 |associated morphology| = << 55641003 |infarct|)"""
eclstring4 = """243796009 |situation with explicit context|: 
{ 408730004 |procedure context| = 385658003 |done|, 
  408731000 |temporal context| = 410512000 |current or specified|,
  408732007 |subject relationship context| = 410604004 |subject of record|, 
  363589002 |associated procedure| = 
( 397956004 |prosthetic arthroplasty of the hip|: 
363704007 |procedure site| = (24136001 |hip joint structure|: 
272741003 |laterality| = 7771000 |left|) 
{ 363699004 |direct device| = 304120007 |total hip replacement prosthesis|, 
260686004 |method| = 257867005 |insertion - action|})}"""
eclstring2 =  """< 404684003 |clinical finding|:
47429007 |associated with| = (< 404684003 |clinical finding|:
    { 363698007 |finding site| = << 39057004 |pulmonary valve structure|,
      116676008 |associated morphology| = << 415582006 |stenosis|},
    { 363698007 |finding site| = << 53085002 |right ventricular structure|, 
      116676008 |associated morphology| = << 56246009 |hypertrophy|})"""
eclstring12= """404684003 |clinical finding|"""
eclstring13= """<< 404684003 |clinical finding|"""
eclstring5 = """< 404684003 |clinical finding|"""
eclstring6 = """< 404684003 |clinical finding|:
      363698007 |finding site| = << 43899006 |Structure of popliteal artery|"""
eclstring7 = """< 404684003 |clinical finding|:
      363698007 |finding site| = << 43899006 |Structure of popliteal artery|,
      116676008 | Associated morphology | = << 107658001 |Mechanical abnormality|"""
eclstring8 = """< 404684003 |clinical finding|:
    { 363698007 |finding site| = << 43899006 |Structure of popliteal artery|,
      116676008 | Associated morphology | = << 107658001 |Mechanical abnormality|}"""
eclstring9 = """< 404684003 |clinical finding|:
      363698007 |finding site| = << (43899006 |Structure of popliteal artery|: 
        272741003 |laterality| = 7771000 |left|),
      116676008 | Associated morphology | = << 107658001 |Mechanical abnormality|"""
eclstring10 = """< 404684003 |clinical finding|:
    { 363698007 |finding site| = << (43899006 |Structure of popliteal artery|: 
        272741003 |laterality| = 7771000 |left|),
      116676008 | Associated morphology | = << 107658001 |Mechanical abnormality|}"""
eclstring11 = """< 404684003 |clinical finding|:
      363698007 |finding site| = << (43899006 |Structure of popliteal artery|: 
        272741003 |laterality| = 7771000 |left|)"""
eclstring14 = """< 404684003 |clinical finding|:
    { 363698007 |finding site| = << (43899006 |Structure of popliteal artery|: 
        272741003 |laterality| = 7771000 |left|),
      116676008 | Associated morphology | = << 107658001 |Mechanical abnormality|},
    { 363698007 |finding site| = << (43899006 |Structure of popliteal artery|: 
        272741003 |laterality| = 7771000 |left|),
      116676008 | Associated morphology | = << 107658001 |Mechanical abnormality|}"""
eclstring15 = """< 243796009 |situation with explicit context|: 
      408730004 |procedure context| = 385658003 |done|, 
      363589002 |associated procedure| = ( 397956004 |prosthetic arthroplasty of the hip|: 
      363704007 |procedure site| = (24136001 |hip joint structure|: 
      272741003 |laterality| = 7771000 |left|))"""
eclstring16 = """< 243796009 |situation with explicit context|:    
      363704007 |procedure site| = (24136001 |hip joint structure|: 
      272741003 |laterality| = 7772000 |right|)"""
eclstring17 = """< 14304000 |Disorder of thyroid gland|:
      255234002 |After| = << (64572001 |Disease|:
      370135005 Pathological process = << ( 308489006 |Pathological process|: 
      272741003 |laterality| = 7772000 |right|))"""
eclstring100 = "({}((({}))))"
coi = {'O':0,'R':1,'<':2,'=':3,':':4,',':5,'{':6,'}':7,'(':8,')':9}


eclStringList = [eclstring12, eclstring13, eclstring5, eclstring6, eclstring7, eclstring8, eclstring9,
                eclstring11, eclstring17]

debug = True
useList = False

def eclRead():
    myString = ""
    f = open('/home/ed/git/rf2main/RF2/files/role.txt', 'r')
    # f = open('/home/ed/git/rf2main/RF2/harness/constraint.txt', 'r')
    for line in f:
        if not(line[0] == "#"):
          myString += line.strip()
    f.close()
    return myString

if useList:
  print "list"
  for item in eclStringList:
    print
    print
    eclstringProcess = item
    # pad start for operator lookback
    eclstringProcess = "(&&&:" + eclstringProcess + ")"
    # pad end for no refinements
    print eclstringProcess
    eclreturnList = stripTerms(eclstringProcess)
    print eclreturnList[0]
    eclreturnList = stripIDs(eclreturnList[0])
    print eclreturnList[0]
    eclMainDict = dictAssign(eclreturnList[0])
    operatorDict = findOperators(eclreturnList[1], eclMainDict)
    # print eclMainDict
    # dictPrint(operatorDict, False)
    # dictPrint(eclreturnList[1], False)
    # doubleDictPrint(operatorDict, eclreturnList[1], False)
    eclBraceDict = bracePull(eclMainDict)
    # dictPrint(eclBraceDict)
    if (":" in eclstringProcess):
      skeletonList = findSkeleton(eclBraceDict, eclreturnList[1])
    else:
      print "just process: ", eclstringProcess[3:]
    # refinementList = findRefinements(eclBraceDict, eclreturnList[1])
    skeletonList.append(operatorDict)
    skeletonList.append(eclreturnList[1])
    if debug:
      print "\nABCD:"
      for item in skeletonList:
        print item
        print
    buildProcessSequence(skeletonList)
else:
  print "individual"
  eclstringProcess = eclRead()
  print eclstringProcess
  # eclstringProcess = eclstring8
  # pad start for operator lookback
  eclstringProcess = "(&&&:" + eclstringProcess + ")"
  # pad end for no refinements
  # print eclstringProcess
  eclreturnList = stripTerms(eclstringProcess)
  # print eclreturnList[0]
  eclreturnList = stripIDs(eclreturnList[0])
  # print eclreturnList[0]
  eclMainDict = dictAssign(eclreturnList[0])
  operatorDict = findOperators(eclreturnList[1], eclMainDict)
  # print eclMainDict
  # dictPrint(operatorDict, False)
  # dictPrint(eclreturnList[1], False)
  # doubleDictPrint(operatorDict, eclreturnList[1], False)
  eclBraceDict = bracePull(eclMainDict)
  # dictPrint(eclBraceDict)
  if (":" in eclstringProcess):
    skeletonList = findSkeleton(eclBraceDict, eclreturnList[1])
  else:
    print "just process: ", eclstringProcess[3:]
  # refinementList = findRefinements(eclBraceDict, eclreturnList[1])
  skeletonList.append(operatorDict)
  skeletonList.append(eclreturnList[1])
  if debug:
    print "\nABCD:"
    for item in skeletonList:
      print item
      print

  buildProcessSequence(skeletonList)

