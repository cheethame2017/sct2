from SCTRF2Helpers import *
from SCTRF2Funcs import *
import sys, os
from unidecode import unidecode
import sqlite3


def showRefsetMatches(callId):
    mydict = {}
    counter = 1
    print("\n\033[92m[" + str(counter) + "]", callId[0], pterm(callId), "membership details:\n")
    mydict.update(list(zip(listify(counter), listify(callId))))
    counter += 1
    refsetType = refSetTypeFromConId_IN(callId[0])[0][0]
    leftCount, otherRows = refSetMembersCompareRefTableFromConIds_IN(callId[0], "", refsetType, 1, {}, True, 0)
    print("Content from " + str(leftCount), "chapter(s):\n")
    for row in otherRows:
        print("[" + str(counter) + "]\t(" + str(row[6]) + "%)" + (" " * (6 - len(str(row[6])))) + row[2] + tabString("(" + str(row[6]) + "%) " +row[2]) + pterm(row[2]))
        mydict.update(list(zip(listify(counter), listify(tuplify(row[2])))))
        counter += 1
    leftCount, otherRows = refSetMembersCompareRefTableFromConIds_IN(callId[0], "", refsetType, 2, {}, True, 0)
    print("\nReferencedComponentId matches with " + str(leftCount) + " other '" + refsetType + "' refset(s):\n")
    for row in otherRows:
        print("[" + str(counter) + "]\t" + proportionColor_IN(row[6]) + (" " * (6 - len(str(row[6])))) + row[2] + tabString("(" + str(row[6]) + "%) " +row[2]) + pterm(row[2]) + " " + proportionColor_IN(row[7]))
        mydict.update(list(zip(listify(counter), listify(tuplify(row[2])))))
        counter += 1
    return mydict


extDict = {}
callId = readFocusValue()
extDict = showRefsetMatches(callId)

print()
print(extDict)

