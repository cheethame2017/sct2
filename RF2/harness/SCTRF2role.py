#!/usr/bin/env python

from SCTRF2RoleFuncs import *
from SCTRF2Funcs import *
from platformSpecific import *
#from langTest import checkForOutliers

# test change

def browseFromRootCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> in browseFromRootCall'
    callId = ('138875005',)
    snapHeaderWrite(callId[0])
    snapshotHeader()
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString

def showSelectedRoles():
    if not debug:
        clearScreen()
    if debug:
        print '-> in showSelectedRoles'
    clearScreen()
    snapshotHeader()
    returnString = eclDisplay(True, False, True)
    return returnString

def reloadShowSelectedRoles():
    if not debug:
        clearScreen()
    if debug:
        print '-> in showSelectedRoles'
    clearScreen()
    snapshotHeader()
    returnString = eclDisplay(True, False, False)
    return returnString

def processConstraintCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> in showSelectedRoles'
    clearScreen()
    snapshotHeader()
    returnString = eclProcess()
    return returnString

def browseFromIndexCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> in browseFromIndexCall'
    snapshotHeader()
    callId = inString
    if isinstance(callId, str):
        callId = listify(callId.strip())
    breadcrumbsWrite(callId[0])
    focusSwapWrite(callId[0])
    returnString = browseFromId(callId)
    return returnString


def showBreadcrumbs():
    if not debug:
        clearScreen()
    if debug:
        print '-> in showBreadcrumbs'
    returnString = breadcrumbsRead()
    if debug:
        print returnString
    return returnString


def readInSwapCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> in readInSwapCall'
    snapshotHeader()
    returnString = readInSwap()
    if debug:
        print returnString
    return returnString

def readInSwapCallSV():
    if not debug:
        clearScreen()
    if debug:
        print '-> in readInSwapCall'
    snapshotHeader()
    returnString = readInSwapSV()
    if debug:
        print returnString
    return returnString

def readBackSearchCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> in readBackSearch'
    returnString = readBackSearch()
    if debug:
        print returnString
    return returnString

def showUsefulCall():
    if not debug:
        clearScreen()
    if debug:
        print '-> showUsefulCall'
    snapshotHeader()
    returnString = showUseful()
    if debug:
        print returnString
    return returnString


def showTermsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowTermsCall'
    snapshotHeader()
    callId = inString
    returnString = allTermsFromConId(callId)
    if debug:
        print returnString
    return returnString


def showTermsMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowTermsMetaCall'
    snapshotHeader()
    callId = inString
    returnString = allTermsFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString


def showRefsetsMembersMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowRefsetsMembersMetaCall'
    snapshotHeader()
    callId = inString
    returnString = showRefsetRowsForRefset(callId, True)
    if debug:
        print returnString
    return returnString


def showRefsetsMembersCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowRefsetsMembersCall'
    snapshotHeader()
    callId = inString
    returnString = showRefsetRowsForRefset(callId, False)
    if debug:
        print returnString
    return returnString


def showRolesCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowRolesCall'
    snapshotHeader()
    callId = inString
    returnString = groupedRolesFromConId(callId)
    if debug:
        print returnString
    return returnString


def showHistOriginCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowHistOriginCall'
    snapshotHeader()
    callId = inString
    returnString = historicalOriginsFromConId(callId)
    if debug:
        print returnString
    return returnString


def showHistOriginPlusMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowHistOriginPlusMetaCall'
    snapshotHeader()
    callId = inString
    returnString = historicalOriginsFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString


def showRefsetRowsCall(memberId, refsetId):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowRolesCall'
    snapshotHeader()
    returnString = showRefsetRowsFromConAndRefset(memberId[0], refsetId)
    if debug:
        print returnString
    return returnString


def showConceptMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowConceptMetaCall'
    snapshotHeader()
    callId = inString
    returnString = conMetaFromConId(callId)
    if debug:
        print returnString
    return returnString


def showDescendantsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> indescendantsCall'
    snapshotHeader()
    callId = inString
    returnString = descendantsFromConId(callId)
    if debug:
        print returnString
    return returnString


def showAncestorsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inancestorsCall'
    snapshotHeader()
    callId = inString
    returnString = ancestorsFromConId(callId)
    if debug:
        print returnString
    return returnString

def testMethodCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> intestMethodCall'
    snapshotHeader()
    callId = inString
    returnString = testMethod(callId)
    if debug:
        print returnString
    return returnString


def showRolesMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowRolesMetaCall'
    snapshotHeader()
    callId = inString
    returnString = groupedRolesFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString


def showRefsetsCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowRefsetsCall'
    snapshotHeader()
    callId = inString
    returnString = refSetFromConId(callId)
    if debug:
        print returnString
    return returnString


def showRefsetsMetaCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print '-> inshowRefsetsMetaCall'
    snapshotHeader()
    callId = inString
    returnString = refSetFromConIdPlusMeta(callId)
    if debug:
        print returnString
    return returnString


def searchOnTextCall(inOption, inLimit):
    if not debug:
        clearScreen()
    if debug:
        print '-> in searchOnTextCall'
    #not fully sorted yet - need to get this term processing into functions layer
    prePreCallTerm = (raw_input('Search text: '))
    returnString = searchOnText(prePreCallTerm, inOption, inLimit)
    return returnString


def checkForOutliersCall(inString):
    if debug:
        print '-> incheckForOutliersCall'
    clearScreen()
    print inString
    if inString == '':
        testId = (raw_input('EnterId: '))
        returnString = checkForOutliers(testId, 1)
    else:
        returnString = checkForOutliers(inString[0], 1)
    return returnString

returnString = {}
debug = False
headerWriterSnap()
clearScreen()
# returnString = browseFromRootCall()
# main control code
firstTime = True
while True:
    if firstTime:
        topaction = "a"
        firstTime = False
    else:
        topaction = raw_input('Action:\n\
[a] Probe from Snap\t[b] Reload probe\t[c] Process constraint\n\
[d] Process constraint\t[e] (+row) Terms  \t[f] (+row) Relationships\n\
[g] (+row) Term+meta\t[h] (+row) Role+meta\t[i] (+row) Con meta\n\
[j] Breadcrumbs   \t[k] (+row) Refsets\t[l] (+row) Refs+meta\n\
[m] (+row) RS mem\t[n] (+row) RS mem+meta\t[o] (+r) RS[n] detail\n\
[p]*List manip...\t[q] (+row) Ancestors\t[r] (+row) Descendants\n\
[s] (+row) History\t[t] (+row) Hist+meta\t[u] Read from GUI\n\
[v] Read SV terminal\t[w] Useful codes\t[x] Read back search\n\
[Z] (+row) Exp\n\
(or enter rownumber of list, or check for outliers - d+rownumber)\n\
[zz] Exit\n\
Input selection: ').strip()
    if topaction == 'zz':
        if debug:
            print returnString
        clearScreen()
        break  # breaks out of the while loop and ends session
    elif topaction == 'a':
        if debug:
            print returnString
            print '-> calling showSelectedRoles'
        returnString = showSelectedRoles()
    elif topaction == 'b':
        if debug:
            print returnString
            print '-> calling reloadShowSelectedRoles'
        returnString = reloadShowSelectedRoles()
##    elif topaction == 'c':
##        if debug:
##            print returnString
##            print '-> calling searchOnTextCall'
##        returnString = searchOnTextCall(1)
    elif topaction == 'j':
        if debug:
            print returnString
            print '-> calling showBreadcrumbs'
        returnString = showBreadcrumbs()
    elif topaction == 'xx':  # toggle *most* debug outputs
        if debug:
            debug = False
            print 'debug OFF'
        else:
            debug = True
            print 'debug ON'
    elif topaction == 'd':
        if debug:
            print returnString
            print '-> calling processConstraintCall'
        returnString = processConstraintCall()
    elif topaction == 'u':
        if debug:
            print returnString
            print '-> calling readInSwapCall'
        returnString = readInSwapCall()
    elif topaction == 'v':
        if debug:
            print returnString
            print '-> calling readInSwapCallSV'
        returnString = readInSwapCallSV()
    elif topaction == 'w':
        if debug:
            print returnString
            print '-> calling showUsefulCall'
        returnString = showUsefulCall()
    elif topaction == 'x':
        if debug:
            print returnString
            print '-> calling readBackSearchCall'
        returnString = readBackSearchCall()
    else:
        try:
            if debug:
                print topaction[0], topaction[1:]
            if topaction[0] == 'd':
                if debug:
                    print 'd', returnString[int(topaction[1:])]
                returnString = checkForOutliersCall(returnString[int(topaction[1:])])
            elif topaction[0] == 'c':
                if debug:
                    print 'c'
                if len(topaction) == 1:
                    returnString = searchOnTextCall(1, 0)
                else:
                    if topaction[1] == '#':
                        returnString = searchOnTextCall(1, 1)
                    elif topaction[1] == ']':
                        returnString = searchOnTextCall(1, 2)
                    elif topaction[1] == '-':
                        if len(topaction) == 3:
                            if topaction[2] == '#':
                                returnString = searchOnTextCall(0, 1)
                            elif topaction[2] == ']':
                                returnString = searchOnTextCall(0, 2)
                            else:
                                returnString = searchOnTextCall(0, 0)
                        else:
                            returnString = searchOnTextCall(0, 0)
                    elif topaction[1] == '+':
                        if len(topaction) == 3:
                            if topaction[2] == '#':
                                returnString = searchOnTextCall(2, 1)
                            elif topaction[2] == ']':
                                returnString = searchOnTextCall(2, 2)
                            else:
                                returnString = searchOnTextCall(2, 0)
                        else:
                            returnString = searchOnTextCall(2, 0)
                    else:
                        returnString = searchOnTextCall(1, 0)
            elif topaction[0] == 'e':
                if debug:
                    print 'e', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showTermsCall(callThing[0])
            elif topaction[0] == 'g':
                if debug:
                    print 'g', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showTermsMetaCall(callThing[0])
            elif topaction[0] == 'f':
                if debug:
                    print 'f', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRolesCall(callThing[0])
            elif topaction[0] == 'h':
                if debug:
                    print 'h', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRolesMetaCall(callThing[0])
            elif topaction[0] == 'i':
                if debug:
                    print 'i', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showConceptMetaCall(callThing[0])
            elif topaction[0] == 'k':
                if debug:
                    print 'k', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetsCall(callThing[0])
            elif topaction[0] == 'l':
                if debug:
                    print 'l', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetsMetaCall(callThing[0])
            elif topaction[0] == 'm':
                if debug:
                    print 'm', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetsMembersCall(callThing[0])
            elif topaction[0] == 'n':
                if debug:
                    print 'm', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetsMembersMetaCall(callThing[0])
            elif topaction[0] == 'q':
                if debug:
                    print 'q', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showAncestorsCall(callThing[0])
            elif topaction[0] == 'r':
                if debug:
                    print 'r', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showDescendantsCall(callThing[0])
            elif topaction[0] == 's':
                if debug:
                    print 's', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showHistOriginCall(callThing[0])
            elif topaction[0] == 't':
                if debug:
                    print 't', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showHistOriginPlusMetaCall(callThing[0])
            elif topaction[0] == 'o':
                if debug:
                    print 'o', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = showRefsetRowsCall(returnString[1], callThing[0])
            elif topaction[0] == 'Z':
                if debug:
                    print 'Z', returnString[int(topaction[1:])]
                callThing = returnString[int(topaction[1:])]
                returnString = testMethodCall(callThing[0])
            else:
                if debug:
                    print ' ', returnString[int(topaction)]
                returnString = browseFromIndexCall(returnString[int(topaction)])
            True
        except:
            True


##if __name__ == '__main__':
##	print 'TCCall is being run by itself'
##else:
##	print 'TCCall being imported from another module'
