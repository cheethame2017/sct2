from neo4j.v1 import GraphDatabase

def neoNodesFromPath(tx, inString):
    myNodes = []
    myNodeSet = []
    for record in tx.run(inString):
        for node in record["path"].nodes:
            myNodes.append(node.items()[0][1])
    myNodeSet = set(myNodes)
    return myNodeSet

def neoQuery(CON_ID, ET, queryType, CON_ID2="138875005"):  # queryType 1=desc, 2=ances, 3 = compare
    uri = "bolt://localhost:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neo"))
    resultNodes = []
    with driver.session() as session:
        if queryType == 1:
            print "Descendants " + ET + "\n"
            cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con)-[*]->(sup:con {cid:'" + CON_ID + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
        elif queryType == 2:
            print "Ancestors " + ET + "\n"
            cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + CON_ID2 + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
        else:
            print "Compare " + ET + "\n"
            cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + CON_ID2 + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
        resultNodes = session.read_transaction(neoNodesFromPath, cypherString)
    if queryType == 3:
        if len(resultNodes) > 0:
            print "True"
        else:
            print "False"
    else:
        for node in resultNodes:
            print node, node
        print
        print len(resultNodes)

neoQuery("80146002", "20180131", 1)
neoQuery("80146002", "20180131", 2)
neoQuery("80146002", "20180131", 3, "34853001")
neoQuery("80146002", "20180131", 3, "74400008")