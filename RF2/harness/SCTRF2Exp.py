from SCTRF2Helpers import *
from SCTRF2SVconfig import *

def expBuilder(expression, newItem, location):
    return expression

def expConBuilder(expressionCon, newItem, location):
    return expressionCon

def expTester(expressionCon, expressionSet, substrate, passFail):
    return resultSet #passes, fails

def expSaver(expression, name):
    return expression

def expConSaver(expressionCon, name):
    return expressionCon