from SCTRF2config import tok
from SCTRF2Helpers import childrenFromId_IN, descendantsIDFromConId_IN, pathCalcFromId_IN, TCCompare_IN

def tlc():
    tlcList = childrenFromId_IN((tok('ROOT'),))
    return tlcList

tlc = tlc()
for tl in tlc:
    tl.append(0)
    tl.append(0)
    tl.append(0)
    tl.append(0)
    tl.append(0)
allConcepts = [conId[0] for conId in descendantsIDFromConId_IN(tok("ROOT")) if conId[0][-1] == "0"]
for concept in allConcepts:
    myList, myLP, mySP = pathCalcFromId_IN(concept)
    for tl in tlc:
        if TCCompare_IN(concept, tl[2]):
            tl[3] += 1
            tl[4] += myLP
            tl[5] += mySP
            tl[6] = tl[4] / tl[3]
            tl[7] = tl[5] / tl[3]
    print()
    print()
    for tl in tlc:
        print(tl)
