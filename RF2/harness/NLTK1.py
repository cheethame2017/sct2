from __future__ import division  # Python 2 users only
import nltk, re, pprint
from nltk import word_tokenize
from urllib import urlopen
from bs4 import BeautifulSoup
from SCTRF2Helpers import *
from nltk.collocations import *


# run in NLTK virtualenvironment


url = "https://en.wikipedia.org/wiki/Mitral_insufficiency"
response = urlopen(url)
# raw = response.read().decode('utf8')
raw = BeautifulSoup(response, "html.parser")

# kill all script and style elements
for script in raw(["script", "style"]):
    script.extract()

text = raw.get_text()

# break into lines and remove leading and trailing space on each
lines = (line.strip() for line in text.splitlines())
# break multi-headlines into a line each
chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
# drop blank lines
text = '\n'.join(chunk for chunk in chunks if chunk)

# print(text.encode('utf-8'))

# print text[:1000]
# tokens = word_tokenize(text)

bigram_measures = nltk.collocations.BigramAssocMeasures()
trigram_measures = nltk.collocations.TrigramAssocMeasures()
tokens = nltk.wordpunct_tokenize(text)
finder = TrigramCollocationFinder.from_words(tokens)
scored = finder.score_ngrams(trigram_measures.raw_freq)
print sorted(finder.nbest(trigram_measures.raw_freq, 20))
# print sorted(bigram for bigram, score in scored)[1:100]

print type(tokens)
print len(tokens)
print tokens[:100]
text = nltk.Text(tokens)
text.concordance('absent')
text.collocations()


# matches = searchOnText_IN()
print text.tokens[1:100]