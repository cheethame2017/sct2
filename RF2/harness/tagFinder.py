import sqlite3
from SCTRF2config import pth
from SCTRF2Helpers import fsnFromConId_IN, descendantCountFromConId_IN

def dbHarness1(dbPath1):
    codeList = []
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select id from concepts where active=1")
    recordset = cur.fetchall()

    for item in recordset:
        codeList.append(item)

    cur.close
    conn.close
    return codeList

# define dbpath1
snapPath = pth('RF2_SNAP')
outPath = pth('RF2_OUT')
dbPath1 = snapPath

# call harness
codeList = dbHarness1(dbPath1)
tagDict = {}
for item in codeList:
    fsn = fsnFromConId_IN(item[0])[1]
    start = fsn.rfind("(")
    tag = fsn[start:]
    if tag not in tagDict:
        tagDict[tag] = [item[0], fsn, descendantCountFromConId_IN(item[0])]
    else:
        if descendantCountFromConId_IN(item[0]) > tagDict[tag][2]:
            tagDict[tag] = [item[0], fsn, descendantCountFromConId_IN(item[0])]

for item in tagDict:
    print(item, tagDict[item][0], tagDict[item][1], str(tagDict[item][2]))
