import sqlite3

def dbHarness1(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("Select id, term from descriptions where active=1")
    recordset = cur.fetchall()

    charList = []
    charIdList = []
    for item in recordset:
        print(item[1])
        for char in item[1]:
            if not char in charList:
                charList.append(char)
            if ord(char) > 127:
                charIdList.append([char, item[0], item[1]])

    cur.close
    conn.close

    for item in sorted(charList):
        print(item, repr(item), ord(item))
    print()
    for item in sorted(charIdList):
        print(item)

# define dbpath1
dbPath1 = "/home/ed/Data/RF2Snap.db"

# call harness
dbHarness1(dbPath1)
