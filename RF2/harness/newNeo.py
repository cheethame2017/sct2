from neo4j.v1 import GraphDatabase

def neoNodesFromPath(tx, inString):
    myNodes = []
    myNodeSet = []
    for record in tx.run(inString):
        for node in record["path"].nodes:
            myNodes.append(node.items()[0][1])
    myNodeSet = set(myNodes)
    return myNodeSet

def neoQuery(CON_ID, ET, queryType, CON_ID2="138875005"):  # queryType 1=desc, 2=ances, 3 = compare
    uri = "bolt://localhost:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neo"))
    resultNodes = []
    with driver.session() as session:
        if queryType == 1:
            print "Descendants " + ET + "\n"
            cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con)-[*]->(sup:con {cid:'" + CON_ID + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
        elif queryType == 2:
            print "Ancestors " + ET + "\n"
            cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + CON_ID2 + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
        else:
            print "Compare " + ET + "\n"
            cypherString = "WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + CON_ID2 + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path"
        resultNodes = session.read_transaction(neoNodesFromPath, cypherString)
    if queryType == 3:
        if len(resultNodes) > 0:
            print "True"
        else:
            print "False"
    else:
        for node in resultNodes:
            print node, node
        print
        print len(resultNodes)

neoQuery("80146002", "20180131", 1)
neoQuery("80146002", "20180131", 2)
neoQuery("80146002", "20180131", 3, "34853001")
neoQuery("80146002", "20180131", 3, "74400008")

# backup of old py2neo code

def SV_MDRchildrenFromConIdPlusMetaGraph_IN(inId, activeFlag, effTimeSet):
    graph = Graph(password="neo")
    pathList = []
    relList = []
    nodeList = []
    activePathNodeGroups = []
    activePathNodes = []
    preRoleList = []
    coverString = ""
    for row in effTimeSet:
        # children
        coverString = progressTrack('START',"*",coverString)
        if activeFlag == 0:
            queryString = cypherWrite("WITH " + row[1] + " AS maxdate MATCH path=()-[]->(con {cid:'" + inId + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.mod='" + row[0] + "' and r.status = '0')) RETURN path")
        elif activeFlag == 2:
            queryString = cypherWrite("WITH " + row[1] + " AS maxdate MATCH path=()-[]->(con {cid:'" + inId + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.mod='" + row[0] + "')) RETURN path")
        else:
            # queryString = cypherWrite("WITH " + row[1] + " AS maxdate MATCH path=()-[]->(con {cid:'" + inId + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.mod='" + row[0] + "' and r.status = '1')) RETURN path")
            queryString = cypherWrite("MATCH path=()-[]->(con {cid:'" + inId + "'}) WHERE ALL(r IN relationships(path) WHERE (r.status = '1')) RETURN path")
        mydata = graph.data(queryString)
        nodeSet = ()
        for item in mydata:
            if (item['path'].relationships()[0].properties['et'] <= int(row[1])) and (item['path'].relationships()[0].properties['st'] > int(row[1])) and (item['path'].relationships()[0].properties['mod'] == row[0]):
                node = item['path'].nodes()[0]
                nodeSet = (node['cid'],)
                nodeList.append(nodeSet)
                pathList.append([item['path'].relationships()[0].properties, nodeSet])
    tempSet = ()
    for role in pathList:
        coverString = progressTrack('START',"+", coverString)
        if cfg('SHOW_METADATA') == 0:
            tempSet = (-1, tok('IS_A'), tok('IS_A'), role[1][0], SV_pterm(tuplify(str(role[1][0]))), role[0]['mod'], role[0]['et'], tok('DEF_REL'), tok('SOME'), int(role[0]['status']), role[0]['mod'])
        else:
            tempSet = (-1, tok('IS_A'), pterm(tuplify(tok('IS_A'))), role[1][0], SV_pterm(tuplify(str(role[1][0]))), pterm(tuplify(role[0]['mod'])), role[0]['et'], pterm(tuplify(tok('DEF_REL'))), pterm(tuplify(tok('SOME'))), role[0]['status'], role[0]['mod'])
        preRoleList.append(tempSet)
    coverString = progressTrack('END',"", coverString)
    preRoleList.sort(key=lambda x: (x[0], x[4].lower()))
    #groups
    roleList = []
    if len(preRoleList) > 0:
        roleMax = preRoleList[len(preRoleList) - 1][0]
        for roleCount in range(-1, roleMax + 1):
            roleGroup = []
            for row in preRoleList:
                if row[0] == roleCount:
                    roleGroup.append(row)
            if len(roleGroup) > 0:
                roleGroup.insert(0, roleCount)
                roleList.append(roleGroup)
    if (cfg("SUB_SHOW") == 1) and (cfg("QUERY_TYPE") != 1):  # suspicious as to why this works - ? should use standard efftime format but currently doesn't' ok now!
        roleListOut = []
        for roleSet in roleList:
            if not roleSet is None:
                for role in roleSet:
                    matchBool = False
                    coverString = progressTrack('START',".", coverString)
                    if (not isinstance(role, int)) and (role[0] == -1):
                        for row in effTimeSet:  # swap execute lines and indent below to # marker
                            queryString = cypherWrite("MATCH path=()-[]->(con {cid:'" + role[3] + "'}) RETURN path")
                            mydata = graph.data(queryString)
                            if activeFlag == 0:
                                for item in mydata:
                                    if (item['path'].relationships()[0].properties['et'] <= row[1]) and (item['path'].relationships()[0].properties['st'] > row[1]) and (item['path'].relationships()[0].properties['mod'] == row[0]) and (item['path'].relationships()[0].properties['status'] == '0'):
                                        matchBool = True  # marker
                            elif activeFlag == 2:
                                for item in mydata:
                                    if (item['path'].relationships()[0].properties['et'] <= row[1]) and (item['path'].relationships()[0].properties['st'] > row[1]) and (item['path'].relationships()[0].properties['mod'] == row[0]):
                                        matchBool = True  # marker
                            else:
                                for item in mydata:
                                    if (item['path'].relationships()[0].properties['et'] <= row[1]) and (item['path'].relationships()[0].properties['st'] > row[1]) and (item['path'].relationships()[0].properties['mod'] == row[0]) and (item['path'].relationships()[0].properties['status'] == '1'):
                                        matchBool = True  # marker
                            # if len(mydata) > 0:
                            #     matchBool = True  # marker
                        if role[9] == 1:
                            if not matchBool:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "| "
                            else:
                                tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|+"
                        else:
                            tempstring = str(role[0]) + "|" + str(role[1]) + "|" + str(role[2]) + "|" + str(role[3]) + "|" + (role[4]).encode('UTF-8') + "|" + str(role[5]) + "|" + str(role[6]) + "|" + str(role[7]) + "|" + str(role[8]) + "|" + str(role[9]) + "|*"
                        roleListOut.append(tempstring.split("|"))
            coverString = progressTrack('END',"", coverString)
    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
        return roleList
    else:
        return roleListOut


def SV_SIMancestorsFromConId_IN(inId):
    ancestorList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    graph = Graph(password="neo")
    ET = str(cfg("EFFECTIVE_TIME"))
    CON_ID = inId
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + tok('ROOT') + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], SV_pterm(tuplify(str(node['cid']))))
        ancestorList.append(tempSet)
    ancestorList.sort(key=lambda x: x[1].lower())
    return ancestorList



def SV_SIMFindAncestorsForConIdAndET(CON_ID, ET):
    graph = Graph(password="neo")
    ancestorList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + CON_ID + "'})-[*]->(sup:con {cid:'" + tok('ROOT') + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], pterm(tuplify(str(node['cid']))))
        ancestorList.append(tempSet)
    return ancestorList


def SV_SIMdescendantsFromConId_IN(inId):
    descendantList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    graph = Graph(password="neo")
    ET = str(cfg("EFFECTIVE_TIME"))
    CON_ID = inId
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con)-[*]->(sup:con {cid:'" + CON_ID + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], SV_pterm(tuplify(str(node['cid']))))
        descendantList.append(tempSet)
    descendantList.sort(key=lambda x: x[1].lower())
    return descendantList


def SV_SIMFindDescendantsForConIdAndET(CON_ID, ET):
    graph = Graph(password="neo")
    descendantList = []
    pathList = []
    activePathNodeGroups = []
    activePathNodes = []
    tempSet = ()
    queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con)-[*]->(sup:con {cid:'" + CON_ID + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
    mydata = graph.data(queryString)
    for item in mydata:
        pathList.append(item['path'])
    for path in pathList:
        activePathNodeGroups.append(path.nodes())
    for nodeGroup in activePathNodeGroups:
        for node in nodeGroup:
            activePathNodes.append(node)
    distinctActivePathNodeSet = set(activePathNodes)
    distinctActivePathNodes = list(distinctActivePathNodeSet)
    for node in distinctActivePathNodes:
        tempSet = (node['cid'], pterm(tuplify(str(node['cid']))))
        descendantList.append(tempSet)
    return descendantList


def SV_SIMC1_IsA_C2_Compare(C1, C2, testCon, ET):
    # graph = Graph(password="neo")
    pathBool = False
    # queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + C1 + "'})-[*]->(sup:con {cid:'" + C2 + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path LIMIT 1")
    # mydata = graph.data(queryString)
    # for item in mydata:
    #     pathList.append(item['path'])
    pathBool = neoQuery(C1, ET, 3, C2)
    if pathBool:
    # if len(pathList) > 0:
        return '+'
    else:
        if testCon == 1:  # if it's C1 which might have moved, changed status or not been there at all...'
            if len(SV_SIMconMetaFromConIdSimple_IN(C1, ET)) == 1:
                if SV_SIMconMetaFromConIdSimple_IN(C1, ET)[0][1] == 1:
                    return '.'
                else:
                    return '--'
            else:
                return ' '
        else:  # or C2...
            if len(SV_SIMconMetaFromConIdSimple_IN(C2, ET)) == 1:
                if SV_SIMconMetaFromConIdSimple_IN(C2, ET)[0][1] == 1:
                    return '.'
                else:
                    return '--'
            else:
                return ' '

def SV_SIMcheckIsARefset(SUB_ID):
    # seed ET
    clearScreen()  # print violation
    ET = "20020131"
    SUP_ID = tok('REFSET')
    # graph = Graph(password="neo")
    # print violation
    print 'Checking ' + SUB_ID, pterm(tuplify(SUB_ID)) + ' as a subtype of ' + SUP_ID, pterm(tuplify(SUP_ID)) + '...\n'  # print violation
    while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
        # print violation
        print ET
        pathBool = False
        # pathList = []
        # queryString = cypherWrite("WITH " + ET + " AS maxdate MATCH path=(sub:con {cid:'" + SUB_ID + "'})-[*]->(sup:con {cid:'" + SUP_ID + "'}) WHERE ALL(r IN relationships(path) WHERE (r.et <= maxdate and r.st > maxdate and r.status = '1')) RETURN path")
        # mydata = graph.data(queryString)
        # for item in mydata:
        #     pathList.append(item['path'])
        pathBool = neoQuery(SUB_ID, ET, 3, SUP_ID)
        if pathBool:
        # if len(pathList) > 0:
            # print violation
            print "yes, it's a refset!'"
            return True
        ET = upByNMonths(ET, 6)
    # print violation
    print "no, it's never been a refset!'"
    return False

