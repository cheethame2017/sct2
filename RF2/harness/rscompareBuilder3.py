#!/usr/bin/python
from __future__ import print_function, unicode_literals
from SCTRF2Helpers import refSetMembersCompareFromConIds_IN

# initialise
testset = "A"
refsetType = 'simpleRefset'
refsetDict = {}
testableRefsets = []

# do main chapters and identify list of testable refsets
leftCount, otherRows = refSetMembersCompareFromConIds_IN(testset, "", refsetType, 1, {}, True, 0)
tempList = []
for row in otherRows:
    print(row)
    tempList.append(row[0])
if len(tempList) > 0:
    refsetDict[testset] = tempList

# test the testable refsets against each other.
leftCount, otherRows = refSetMembersCompareFromConIds_IN(testset, "", refsetType, 2, refsetDict, True, 0)
for row in otherRows:
    print(row)
