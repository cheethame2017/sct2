import sqlite3
import io
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk import ngrams, FreqDist
from SCTRF2Funcs import descendantsFromConId_IN, pterm, hasDef, fetchTag
from SCTRF2Helpers import isActiveFromConId_IN, fsnFromConId_IN
from SCTRF2config import tok

# working paths
homePath = '/home/ed/'
repoPath = homePath + 'Code/Repos/sct/RF2/'
miscPath = repoPath + 'files/misc/'
picklePath = repoPath + 'files/pickle/'
modelTablePath = repoPath + 'files/atts/modelTableCO.txt'
writeOutPath = repoPath + 'sets/'
writeOutTagResultsPath = miscPath + 'newTagList.txt'
dotPath = repoPath + "files/dot/tagChange.dot"
dotPathNum = repoPath + "files/dot/tagChangeNum.dot"
RF2Outpath = homePath + 'Data/RF2Out.db'

def domainOrRangeGen():
    f = io.open(modelTablePath, 'r', encoding="UTF-8")
    domainList = []
    rangeList = []
    lines = f.readlines()
    f.close()
    for line in lines:
        lineTemp = line.split("\t")
        if lineTemp[1] not in domainList:
            domainList.append(lineTemp[1])
        if lineTemp[7].strip() not in rangeList:
            rangeList.append(lineTemp[7].strip())
    return domainList, rangeList

def findTags(runNum, runType, newFile):
    conn = sqlite3.connect(RF2Outpath)
    cur2 = conn.cursor()
    # all
    if runType == 1:
        cur2.execute('SELECT SupertypeId, STCount FROM TCSubCount order by STCount desc;')
    # no drugs
    elif runType == 2:
        cur2.execute("SELECT t1.SupertypeId as A, t1.STCount as B FROM TCSubCount t1 \
                    EXCEPT \
                    SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                    WHERE t2.supertypeId IN ('" + tok("PHARM_PROD") +"') \
                    and t2.SubtypeId=t3.supertypeId order by B desc;")
    # only QV's
    elif runType == 3:
        cur2.execute("SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                    WHERE t2.supertypeId IN ('" + tok("QUALIFIER") + "') \
                    and t2.SubtypeId=t3.supertypeId order by B desc;")
    # fixed value
    elif runType == 4:
        cur2.execute("SELECT SupertypeId, STCount FROM TCSubCount WHERE SupertypeId in ('273249006');")
    recordsetIn2 = cur2.fetchall()
    if runNum == 0:
        setToRun = [item for item in recordsetIn2[:]]
    else:
        setToRun = [item for item in recordsetIn2[:runNum]]
    progress = 1
    subList = []
    tot = len(setToRun)
    domainList, rangeList = domainOrRangeGen()
    domainOrRangeList = [*domainList, *rangeList]
    f = io.open(writeOutTagResultsPath, 'a', encoding="UTF-8")
    if newFile:
        f.write('DomainOrRange' + '\t' + 'DefPrim' + '\t' + 'TextDef' + '\t' + 'Id' + '\t' + 'Term' + '\t' + 'TestTerm' + '\t' + 'TermLen' + '\t' + 'TagcestorId' + '\t' + 'Tagcestor' + '\t' + 'DescCount' + '\t' + 'Matches' + '\t' + 'Percent' + '\n')
    for row in setToRun:
        if row[1] < 120000 and row[1] > 30:
            rowTerm = pterm(row[0]).lower()
            rowTermTokList = [item for item in word_tokenize(rowTerm) if ((item.isalnum()) or ("-" in item) or ("/" in item))]
            rowTermTokListLen = len(rowTermTokList)
            if rowTermTokListLen < 4:
                tupleRowTermTokList = tuple(rowTermTokList)
                subList = descendantsFromConId_IN(row[0], True)
                # terms as sentences
                preText2 = ". ".join([sub[1].lower() for sub in subList])
                tokenized_sents = [word_tokenize(sent) for sent in sent_tokenize(preText2)]
                gramList2 = []
                for sent in tokenized_sents:
                    tempList = list(ngrams(sent, rowTermTokListLen))
                    tempList = [ng for ng in tempList if sum([((item.isalnum()) or ("-" in item) or ("/" in item)) for item in ng]) == len(ng)]
                    for ng in tempList:
                        gramList2.append(ng)
                fdist2 = FreqDist(gramList2)
                candidates =  fdist2.keys()
                if tupleRowTermTokList in candidates:
                    key = tupleRowTermTokList
                    if (fdist2[key] > 1):
                        if row[0] in domainOrRangeList:
                            domainOrRangeString = "True"
                        else:
                            domainOrRangeString = "False"
                        activeString = isActiveFromConId_IN(row[0])[2]
                        hasDefString = str(hasDef(row[0]))
                        tagCestorId = [tagId[0] for tagId in fetchTag(row[0]) if tagId[1] == 1][0]
                        tagCestor = fsnFromConId_IN(tagCestorId)[1]
                        print(domainOrRangeString, activeString, hasDefString, row[0], rowTerm, str(len(subList)), " ".join(key), str(len(key)), fdist2[key], format((fdist2[key] / len(subList)) * 100, '.2f'), tagCestorId, tagCestor)
                        f.write(domainOrRangeString + '\t' + activeString + '\t' + hasDefString + '\t' + row[0] + '\t' + rowTerm  + '\t' + " ".join(key) + '\t' + str(len(key)) + '\t' + tagCestorId + '\t' + tagCestor + '\t' + str(len(subList)) + '\t' + str(fdist2[key]) + '\t' + format((fdist2[key] / len(subList)) * 100, '.2f') + '\n')
        if (progress % 5000 == 0):
            print(progress, "/", tot)
        progress += 1
    f.close()
    cur2.close()
    conn.close()

if __name__ == '__main__':
    runNum = 0
    runType = 4
    newFile = False
    findTags(runNum, runType, newFile)
