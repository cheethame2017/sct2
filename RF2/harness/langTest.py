import sqlite3
import codecs
import random
import re
from nltk.classify import PositiveNaiveBayesClassifier
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import stopwords
from nltk.util import bigrams
from nltk.util import trigrams
from nltk import FreqDist
from helpers import *

stops = set(stopwords.words("english"))
nonuniqueIds = []
uniqueIds = []
wl = []


def features(sentence):
    word_list = sentence.lower().split()
    words = [word for word in word_list if word not in stops]
    return dict(('%s' % w, True) for w in words)


def featuresu(sentence):
    word_list = sentence.lower().split()
    words = [word for word in word_list if word not in stops]
    return dict(('%s' % w, True) for w in words)


def features2(sentence):
    word_list = sentence.lower().split()
    bg = bigrams(word_list)
    words = list(bg)
    return dict((('%s %s' % w, True) for w in words))


def features3(sentence):
    word_list = sentence.lower().split()
    tg = trigrams(word_list)
    words = list(tg)
    return dict((('%s %s %s' % w, True) for w in words))


def listbuilder(rowofdata, mydict, mycounter):
    print "[" + str(mycounter) + "] " + rowofdata[0] + "\t" + rowofdata[1].replace('\n', '')
    mydict.update(zip(listify(mycounter), listify(listify(rowofdata[0]))))
    nonuniqueIds.append(rowofdata[0])
    return mydict

def listbuilder_old(rowofdata):
    print rowofdata[0] + "\t" + rowofdata[1].replace('\n', '')
    nonuniqueIds.append(rowofdata[0])

def tctest(cidsub, cidsuper):
    conn3 = sqlite3.connect('RF2Out.db')
    cur3 = conn3.cursor()
    sqlstring = "SELECT subtypeid, supertypeid from tc where subtypeid = '" + cidsub + "' and supertypeid = '" + cidsuper + "'"
    cur3.execute(sqlstring)
    recset3 = cur3.fetchall()
    if len(recset3) == 1:
        return True
    else:
        return False
    cur3.close()
    conn3.close()

def wordlistbuilder(word_list, wl):
    wl += word_list
##    print len(word_list)
##    print len(wl)


def checkForOutliers(cidString, inInt):
    if inInt == 1:
        conn = sqlite3.connect('RF2Out.db')
        cur = conn.cursor()
        cur.execute("attach 'RF2Snap.db' as RF2Snap")
        # write/read connection for temporary sqlite files
        conn2 = sqlite3.connect('classify.db')
        cur2 = conn2.cursor()

        # get the focus concept
        ##sqlstring = 'SELECT conceptid FROM FocusConcept'
        ##cur.execute(sqlstring)
        ##recset = cur.fetchall()
        ##for (conceptid) in recset:
        ##    cidString = conceptid
        ##cur.close()
        ##cidString = raw_input('InputId: ')
        cur = conn.cursor()

        mydict = {}
        mycounter = 1

        # get the descendant preferred terms
        sqlstring = "SELECT prefterm.term FROM (tc INNER JOIN RF2Snap.descriptions ON tc.SubtypeId = RF2Snap.descriptions.conceptid) INNER JOIN prefterm ON RF2Snap.descriptions.id = prefterm.descid WHERE tc.SupertypeId = ?"
        a = str(cidString)
        #a = a[3:-3]
        afile = 'f' + a

        try:
            db = input('Old DB table (1 discard [default], 2 preserve)')
        except SyntaxError:
            db = 1

        if db == 1:
            try:
                cur2.execute("DROP TABLE '" + afile + "'")
            except:
                pass
            cur2.execute("CREATE TABLE '" + afile + "' (CID varchar(20))")


        cur.execute(sqlstring, [a])
        recset = cur.fetchall()
        print len(recset)
        lenrecset =  len(recset)

        # write descendant preferred terms to file
        myfile = codecs.open('test_refset.txt', 'w', encoding='utf-8')
        for (term) in recset:
            b = str(term)
            b = b[3:-3]
            myfile.write(str(b) +"\r\n")

        myfile.close()

        # process using nltk classifier code
        try:
            c = input('Stringency (9 high, 1 low, 3 [default])')
        except SyntaxError:
            c = 3
        dTest = False

        if c < 1:
            d = float(0.000000001)
        elif c == 8:
            d = float(0.00000001)
        elif c == 7:
            d = float(0.0000001)
        elif c == 6:
            d = float(0.000001)
        elif c == 5:
            d = float(0.00001)
        elif c == 4:
            d = float(0.0001)
        elif c == 3:
            d = float(0.001)
        elif c == 2:
            d = float(0.01)
        elif c == 1:
            d = float(0.1)
        elif c > 8:
            dTest = True

        try:
            s = input('Sets? (1 Findings, 2 + Situations [default], 3 = Procedures, 4 = Disorders)')
        except SyntaxError:
            s = 2

        try:
            t = input('Unigrams, bigrams or trigrams? (1 [default], 2 or 3)')
        except SyntaxError:
            t = 1

        file = open('test_refset.txt')
        line = file.readline()
        refCount = 0
        reference_sentences = []
        totalstring = ""
        tempstring = ""
        while line:
             reference_sentences.append(line.replace('\n', ''))
             tempstring = line.split()
        ##     print tempstring
             wordlistbuilder(tempstring, wl)
             line = file.readline()
             tempstring = ""
             refCount += 1

        file.close()

        file = open('test_varset.txt')
        line = file.readline()
        linecounter = 0
        pre_various_sentences = []
        ##while linecounter < lenrecset:
        ##     pre_various_sentences.append(line.replace('\n', ''))
        ##     line = file.readline()
        ##     linecounter += 0.3

        while line:
             pre_various_sentences.append(line.replace('\n', ''))
             line = file.readline()

        random.shuffle(pre_various_sentences)
        various_sentences = []
        various_sentences = pre_various_sentences[:(lenrecset * 2)]


        file.close()

        if dTest:
            d = float(refCount)/1200000


        if t == 1:
            positive_featuresets = list(map(features, reference_sentences))
            unlabeled_featuresets = list(map(featuresu, various_sentences))
        elif t == 2:
            positive_featuresets = list(map(features2, reference_sentences))
            unlabeled_featuresets = list(map(features2, various_sentences))
        elif t == 3:
            positive_featuresets = list(map(features3, reference_sentences))
            unlabeled_featuresets = list(map(features3, various_sentences))

        ##print wl
        ##print len(wl)

        classifierFin = PositiveNaiveBayesClassifier.train(positive_featuresets, unlabeled_featuresets, d)
        classifierDis = PositiveNaiveBayesClassifier.train(positive_featuresets, unlabeled_featuresets, d/10)
        ##classifierFin = NaiveBayesClassifier.train(positive_featuresets)

        if s == 1 or s == 2:
            file = open('test_testfinset.txt')
            allrow = file.readline()
            bothrow = allrow.split('\t')
            line = bothrow[1]
            counter = 0
            print "################### Findings ###################"
            while line:
                if t == 1:
                    if classifierFin.classify(features(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            # listbuilder_old(bothrow)
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 2:
                    if classifierFin.classify(features2(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 3:
                    if classifierFin.classify(features3(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]

            file.close()
            print counter


        if s == 2:
            file = open('test_testsitset.txt')
            allrow = file.readline()
            bothrow = allrow.split('\t')
            line = bothrow[1]
            counter = 0
            print "################### Situations ###################"
            while line:
                if t == 1:
                    if classifierFin.classify(features(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 2:
                    if classifierFin.classify(features2(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 3:
                    if classifierFin.classify(features3(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]

            file.close()
            print counter

        if s == 3:
            file = open('test_testprocset.txt')
            allrow = file.readline()
            bothrow = allrow.split('\t')
            line = bothrow[1]
            counter = 0
            print "################### Procedures ###################"
            while line:
                if t == 1:
                    if classifierFin.classify(features(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 2:
                    if classifierFin.classify(features2(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 3:
                    if classifierFin.classify(features3(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]

            file.close()
            print counter

        if s == 2 or s == 4:
            file = open('test_testdisset.txt')
            allrow = file.readline()
            bothrow = allrow.split('\t')
            line = bothrow[1]
            counter = 0
            print "################### Disorders ###################"
            while line:
                if t == 1:
                    if classifierDis.classify(features(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 2:
                    if classifierDis.classify(features2(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
                elif t == 3:
                    if classifierDis.classify(features3(line)):
                        if tctest(bothrow[0], a) == False:
                            counter += 1
                            mydict.update(listbuilder(bothrow, mydict, mycounter))
                            mycounter += 1
                    allrow = file.readline()
                    bothrow = allrow.split('\t')
                    if len(bothrow) == 1: break
                    line = bothrow[1]
            file.close()
            print counter

        classifierFin.show_most_informative_features(30)

        uniqueIds = set(nonuniqueIds)
        for cid in uniqueIds:
            cur2.execute("INSERT INTO '" + afile + "' VALUES ('" + cid + "')")
        conn2.commit()

        tempstring = ""
        cur2.execute("select CID, COUNT(CID) as CountId from '" + afile + "' GROUP BY CID having (COUNT(CID) > 1) ORDER BY COUNT(CID) DESC")
        rs2 = cur2.fetchall()
        for (CID, CountId) in rs2:
        ##    print "CID: %s" % CID,
        ##    print "CountId: %s" % CountId
            if CountId == t:
                tempstring += "'" + CID + "', "

        tempstring = tempstring[:-2]
        print '##############################'

        cur2.close()
        conn2.close()

        if len(tempstring) > 4:
            cur.execute("select ConceptId, FullySpecifiedName from Concepts where ConceptId In (" + tempstring + ") ORDER BY FullySpecifiedName")
            recset = cur.fetchall()
            cur.close()

            for (ConceptId, FullySpecifiedName) in recset:
                print ConceptId + "\t" + FullySpecifiedName


        conn.close()
        while True:
            whatNow = (raw_input('Retest this Id [Y/N]: '))
            if whatNow == 'Y' or whatNow == 'y':
                checkForOutliers(cidString, 1)
            elif whatNow == 'N' or whatNow == 'n':
##                checkForOutliers(cidString, 0)
                return mydict
            else:
                return mydict
    else:
        return ""

