import sqlite3

def dbHarness1(dbPath1):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select * from srefset where refsetId='733073007' and active = 1;")
    recordset = cur.fetchall()
    startList = []
    startDict = {}

    for item in recordset:
        if item[6][0:item[6].find(":")] not in startList:
            startList.append(item[6][0:item[6].find(":")])
            print startList
        if item[6][0:item[6].find(":")] not in startDict:
            startDict[item[6][0:item[6].find(":")]] = 1
        else:
            startDict[item[6][0:item[6].find(":")]] += 1
    for entry in startDict:
        print entry, startDict[entry]

    cur.close
    conn.close
    for item in startList:
        print item

# define dbpath1
dbPath1 = "/home/ed/Data/RF2Snap.db"

# call harness
dbHarness1(dbPath1)