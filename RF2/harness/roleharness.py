from SCTRF2Helpers import *
from SCTRF2Funcs import *
import sys, os
from unidecode import unidecode
import sqlite3

def attsFromConId_IN(id):
    attList = []
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    cursorIn2.execute("select * from cissccrefset where refsetId='723561005' and active=1")
    recordsetIn2 = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    for item in recordsetIn2:
        if TCCompare_IN(id,item[6]):
            attList.append(item)
    return attList

def rangeFromAttId_IN(id):
    rangeList = []
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath +"' as RF2Out")
    cursorIn2.execute("select * from ssccrefset where refsetId='723562003' and active=1 and referencedComponentId=?", tuplify(id))
    recordsetIn2 = cursorIn2.fetchall()
    cursorIn2.close()
    connectionIn2.close()
    mainString = recordsetIn2[0][6]
    while not(mainString.find(" OR ")  == -1):
        rangeList.append(mainString[0:mainString.find(" OR ")])
        mainString = mainString[mainString.find(" OR ") + 4:]
    rangeList.append(mainString)
    return rangeList

def readFocus():
    mydict = {}
    f = open('files/focusSwap.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    # mydict = browseFromId(tuplify(myId))
    return myId

# print('sys.argv[0] =', sys.argv[0])             
# pathname = os.path.dirname(sys.argv[0])        
# print('path =', pathname)
# print('full path =', os.path.abspath(pathname))

def returnModel(testId):
    mydict = {}
    mycounter = 1
    myAttList = attsFromConId_IN(testId)
    myRangeList = []
    for attItem in myAttList:
        mydict.update(zip(listify(mycounter), listify(tuplify("$" + attItem[5] + "|" + pterm(tuplify(attItem[5]))))))
        mycounter += 1
        # print 'Grouped:', attItem[7], 'Card:', attItem[8], 'In group card:', attItem[9]
        mydict.update(zip(listify(mycounter), listify(tuplify(attItem[7]))))
        mycounter += 1
        mydict.update(zip(listify(mycounter), listify(tuplify(attItem[8]))))
        mycounter += 1
        mydict.update(zip(listify(mycounter), listify(tuplify(attItem[9]))))
        mycounter += 1
        # print attItem[5], pterm(tuplify(attItem[5]))
        myRangeList = rangeFromAttId_IN(attItem[5])
        for item in myRangeList:
#             print item
            mydict.update(zip(listify(mycounter), listify(tuplify(item))))
            mycounter += 1
    return mydict

testId = readFocus()
print testId, pterm(tuplify(testId))
returnDict = returnModel(testId)
myIntCounter = 0
for item in returnDict:
    if isinstance(returnDict[item][0],basestring):
        if returnDict[item][0][0] == "$":
            myIntCounter = 0
            print
            print "[" + str(item) + "]", returnDict[item][0][1:]
            myIntCounter += 1
        else:
            if myIntCounter > 0 and myIntCounter < 3:
                print "[" + str(item) + "]", returnDict[item][0], "\t",
            else:
                print "[" + str(item) + "]", returnDict[item][0]
            myIntCounter += 1
    else:
        if myIntCounter > 0 and myIntCounter < 3:
            print "[" + str(item) + "]", returnDict[item][0], "\t",
        else:
            print "[" + str(item) + "]", returnDict[item][0]
        myIntCounter += 1

