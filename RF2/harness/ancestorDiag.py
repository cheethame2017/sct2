from SCTRF2Helpers import *
from SCTRF2Funcs import *
import sys, os
from unidecode import unidecode
import sqlite3



def dotDrawAncestors(inId):
    ancestorNodes = []
    ancestorEdges = []
    ancestorNodes = ancestorsFromConId_IN(inId)
    for node in ancestorNodes:
        for parent in parentsFromId_IN(tuplify(node[0])):
            ancestorEdges.append([node, parent])


    for edge in ancestorEdges:
        print "\"" + edge[0][0] + "\"->\"" + edge[1][1] + "\" [arrowhead=onormal];"
    for node in ancestorNodes:
        print "\"" + node[0] + "\" [label=\"" + node[0] + "\l" + node[1] + "\", " + checkObjValueColor(node[0].strip()) + "];"
    
    try:
        f = open('files/contc.dot', 'w')
        f.write("strict digraph G { rankdir=BT; ranksep=.3; splines=ortho; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        for edge in ancestorEdges:
            f.write("\"" + edge[0][0] + "\"->\"" + edge[1][1] + "\" [arrowhead=onormal];\n")
        for node in ancestorNodes:
            f.write("\"" + node[0] + "\" [label=\"" + node[0] + "\l" + node[1] + "\", " + checkObjValueColor(node[0].strip()) + "];\n")
        f.write("}")
    finally:
        f.close()


dotDrawAncestors('731368004')
