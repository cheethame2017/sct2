import io
import sqlite3

# scans through refsetId='733073007' looking for 'interesting' OWL stated definitions

def OWLPattern(inPattern):
    OWLPatternDict = {"|SubClassOf(|": "Primitive",
                    "|EquivalentClasses(|": "Defined",
                    "|SubObjectPropertyOf(|": "Object attribute",
                    "|SubDataPropertyOf(|": "Data attribute",
                    "|SubObjectPropertyOf(|TransitiveObjectProperty(|": "Transitive",
                    "|SubObjectPropertyOf(|SubObjectPropertyOf(ObjectPropertyChain(|": "Property chain",
                    "|ReflexiveObjectProperty(|SubObjectPropertyOf(|TransitiveObjectProperty(|": "Transitive and reflexive",
                    "|EquivalentClasses(|SubClassOf(|": "Multiple necessary",
                    "|SubClassOf(|SubClassOf(ObjectIntersectionOf(|": "GCI",
                    "|EquivalentClasses(|SubClassOf(ObjectIntersectionOf(|": "Defined GCI"}
    if inPattern in OWLPatternDict:
        return OWLPatternDict[inPattern]
    else:
        return "Not pattern recognised"

def tidyFiles():
    filenameList = ["primitive", "defined", "transitive", \
                    "objAttributeSubtype", "dataAttributeSubtype", "exception", \
                    "propertyChain", "transitiveReflexive", "multipleNecessary", "GCI", "GCIdefined"]
    for filename in filenameList:
        f = io.open('/home/ed/Code/Repos/sct/RF2/files/misc/' + filename + '.txt', 'w', encoding='UTF-8')
        f.close()

def dbHarness2(dbPath1, toFile):
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, string1 as s, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r having c=1 order by c desc;")
    recordset = cur.fetchall()
    tot = len(recordset)
    summaryDict = {}
    summaryListDict = {}
    counter = 1
    for itemA in recordset:
        startList = []
        OWLString = ""
        OWLString = itemA[1]
        marker = OWLString.find(":")
        if OWLString[0:marker] not in startList:
            startList.append(OWLString[0:marker])
        entryString = "|"
        for entry in sorted(startList):
            entryString += entry + "|"
        if entryString not in summaryDict:
            summaryDict[entryString] = 1
        else:
            summaryDict[entryString] += 1
        if entryString not in ["|SubClassOf(|", "|EquivalentClasses(|"]:
            if entryString not in summaryListDict:
                summaryListDict[OWLPattern(entryString)] = [itemA[0]]
            else:
                summaryListDict[OWLPattern(entryString)].append(itemA[0])
        # primitive
        if entryString == "|SubClassOf(|":
            filename = "primitive"
        # defined
        elif entryString == "|EquivalentClasses(|":
            filename = "defined"
        # obj attrib subtype
        elif entryString == "|SubObjectPropertyOf(|":
            filename = "objAttributeSubtype"
        # data attrib subtype
        elif entryString == "|SubDataPropertyOf(|":
            filename = "dataAttributeSubtype"
        # transitive
        elif entryString == "|SubObjectPropertyOf(|TransitiveObjectProperty(|":
            filename = "transitive"
        # property chain
        elif entryString == "|SubObjectPropertyOf(|SubObjectPropertyOf(ObjectPropertyChain(|":
            filename = "propertyChain"
        # transitive / reflexive
        elif entryString == "|ReflexiveObjectProperty(|SubObjectPropertyOf(|TransitiveObjectProperty(|":
            filename = "transitiveReflexive"
        # multiple necessary
        elif entryString == "|EquivalentClasses(|SubClassOf(|":
            filename = "multipleNecessary"
        # GCI
        elif entryString == "|SubClassOf(|SubClassOf(ObjectIntersectionOf(|":
            filename = "GCI"
        #  defined GCI
        elif entryString == "|EquivalentClasses(|SubClassOf(ObjectIntersectionOf(|":
            filename = "GCIdefined"
        else:
            filename = "exception"
        if toFile:
            f = io.open('/home/ed/Code/Repos/sct/RF2/files/misc/' + filename + '.txt', 'a', encoding='UTF-8')
            f.write(str(itemA[0]) + '\t' + str(itemA[1]) + "\t" + entryString + '\n')
            f.close()
        if (counter % 20000 == 0):
            print(str(counter), "/", str(tot))
        counter += 1
    print()
    for item in summaryDict:
        print(item, summaryDict[item])
        # counterTarget = int(counterTarget/2)

    cur.close
    conn.close

def dbHarness1(dbPath1, toFile):
    if toFile:
        # empty files
        tidyFiles()
    conn = sqlite3.connect(dbPath1)
    cur = conn.cursor()
    cur.execute("select referencedComponentId as r, count(referencedComponentId) as c from srefset where refsetId='733073007' and active = 1 \
        group by r having c > 1 order by c desc;")
    recordset = cur.fetchall()
    print(len(recordset))
    summaryDict = {}
    summaryListDict = {}

    IDList = [item[0] for item in recordset]
    IDString = "'" + "', '".join(IDList) + "'"
    cur.execute("select referencedComponentId, string1 from srefset where refsetId='733073007' and active = 1 and referencedComponentId IN (" + IDString + ")")
    recordset2 = cur.fetchall()

    detailDict = {}
    for itemA in recordset:
        for row in recordset2:
            if itemA[0] == row[0]:
                if itemA[0] not in detailDict:
                    detailDict[itemA[0]] = [row[1]]
                else:
                    detailDict[itemA[0]].append(row[1])
    cur.close
    conn.close

    counter = 1
    for itemA in recordset:
        startList = []
        OWLString = ""
        for item in detailDict[itemA[0]]:
            OWLString = item
            marker = OWLString.find(":")
            if OWLString[0:marker] not in startList:
                startList.append(OWLString[0:marker])
        entryString = "|"
        for entry in sorted(startList):
            entryString += entry + "|"
        if entryString not in summaryDict:
            summaryDict[entryString] = 1
            summaryListDict[OWLPattern(entryString)] = [[itemA[0], itemA[1]]]
        else:
            summaryDict[entryString] += 1
            summaryListDict[OWLPattern(entryString)].append([itemA[0], itemA[1]])
        # obj attrib subtype
        if entryString == "|SubObjectPropertyOf(|":
            filename = "objAttributeSubtype"
        # data attrib subtype
        elif entryString == "|SubDataPropertyOf(|":
            filename = "dataAttributeSubtype"
        # transitive
        elif entryString == "|SubObjectPropertyOf(|TransitiveObjectProperty(|":
            filename = "transitive"
        # property chain
        elif entryString == "|SubObjectPropertyOf(|SubObjectPropertyOf(ObjectPropertyChain(|":
            filename = "propertyChain"
        # transitive / reflexive
        elif entryString == "|ReflexiveObjectProperty(|SubObjectPropertyOf(|TransitiveObjectProperty(|":
            filename = "transitiveReflexive"
        # multiple necessary
        elif entryString == "|EquivalentClasses(|SubClassOf(|":
            filename = "multipleNecessary"
        # GCI
        elif entryString == "|SubClassOf(|SubClassOf(ObjectIntersectionOf(|":
            filename = "GCI"
        #  defined GCI
        elif entryString == "|EquivalentClasses(|SubClassOf(ObjectIntersectionOf(|":
            filename = "GCIdefined"
        else:
            filename = "exception"
        if (counter % 50 == 0):
            print(itemA, filename, entryString)
        if toFile:
            f = io.open('/home/ed/Code/Repos/sct/RF2/files/misc/' + filename + '.txt', 'a', encoding='UTF-8')
            f.write(str(itemA[0]) + '\t' + str(itemA[1]) + "\t" + entryString + '\n')
            f.close()
        counter += 1
    print()
    for item in summaryDict:
        print(item, summaryDict[item])

# define dbpath1
dbPath1 = "/home/ed/Data/RF2Snap.db"

writeToFile = False
# call harnesses
dbHarness1(dbPath1, writeToFile)
dbHarness2(dbPath1, writeToFile)
