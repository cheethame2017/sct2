from SCTRF2Funcs import swapDataCall, snapHeaderWrite
from SCTRF2config import tok

# invoked from MRCMChange.sh batch file to rotate data during comparison run.

def swapDataSequence():
    swapDataCall()
    rootId = (tok('ROOT'),)
    snapHeaderWrite(rootId[0])

if __name__ == '__main__':
    swapDataSequence()
