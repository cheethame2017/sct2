from __future__ import unicode_literals
import io
from platformSpecific import cfgPath, cfgPathInstall, changeModelToSVGLocal, changeModelToSVGWinHost
# CONC - header
from SCTRF2config import cfg

# shows what has changed in MRCM after loading latest international data.

# 1. set scope in files/modelLimit.txt

# 2. check that oldDate is set in writeModelTable.py is set to the 8 figure older date (almost certainly the UK one) - 'z' the snapshot browser to toggle between the two.

# 3. all steps now run using /scripts/MRCMChange.sh

# set configuration values with writeModelTableChangeConfigSet.py
# run writeModelTable twice, once with 'old' data, once with 'new'
# then run this. svg rendered file produced in windows share folder
# reset configuration values with writeModelTableChangeConfigReSet.py

def cfg_local(inString):  # short name for typing convenience - setConfig really
    f = io.open(cfgPath() + 'MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        if not line[0] == "#":  # don't process comments
            tempLine = line.strip()
            tempList = tempLine.split('|')
            if tempList[0] == inString:
                returnValue = int(tempList[1])
    f.close()
    return returnValue

def drawChange():
    oldReadbackList = []
    newReadbackList = []
    compReadbackList = []
    with io.open(cfgPathInstall() + "files/modelTableAtts2_old.dot", "r", encoding="utf-8") as fold:
        for line in fold:
            oldReadbackList.append(line)
    with io.open(cfgPathInstall() + "files/modelTableAtts2_new.dot", "r", encoding="utf-8") as fnew:
        for line in fnew:
            newReadbackList.append(line)

    for newLine in newReadbackList:
        matchBool = False
        for oldLine in oldReadbackList:
            if oldLine == newLine:
                matchBool = True
        if matchBool:
            compReadbackList.append(newLine)
        else:
            compReadbackList.append([newLine, newLine])
    for line in compReadbackList:
        if len(line) == 2 and (" [arrowhead=" in line[0]) and not(" color=black, " in line[0]):
            # tempLevMatch = line[0].replace(' color=red, ',' style=bold, color="magenta", ').replace(' color=blue, ',' style=bold, color="cyan", ')
            tempLevMatch = line[0].replace(' color=red, ', ' color="magenta", ').replace(' color=blue, ', ' color="cyan", ')
            if (' style=dashed, color="magenta", ' in tempLevMatch) or (' style=dashed, color="cyan", ' in tempLevMatch):
                tempLevMatch = tempLevMatch.replace(' style=dashed, color="magenta", ', ' style="dashed,bold", color="magenta", ').replace(' style=dashed, color="cyan", ', ' style="dashed,bold", color="cyan", ')
            else:
                tempLevMatch = tempLevMatch.replace(' color="magenta", ', ' style=bold, color="magenta", ').replace(' color="cyan", ', ' style=bold, color="cyan", ')
            line[1] = tempLevMatch

    outputList = []

    # read in unchanged lines
    for line in compReadbackList[:-1]:
        if not len(line) == 2:
            outputList.append(line)

    # add section
    outputList.append("\n/* *** Addition changes *** */\n")

    # read in changed lines
    for line in compReadbackList[:-1]:
        if len(line) == 2:
            outputList.append(line[1])

    compReadbackList = []
    for oldLine in oldReadbackList:
        matchBool = False
        for newLine in newReadbackList:
            if oldLine == newLine:
                matchBool = True
        if not matchBool:
            compReadbackList.append([oldLine, oldLine])
    for line in compReadbackList:
        if len(line) == 2 and (" [arrowhead=" in line[0]) and not(" color=black, " in line[0]):
            # tempLevMatch = line[0].replace(' color=red, ',' style=bold, color="sienna", ').replace(' color=blue, ',' style=bold, color="purple", ')
            tempLevMatch = line[0].replace(' color=red, ', ' color="sienna", ').replace(' color=blue, ', ' color="purple", ')
            if (' style=dashed, color="sienna", ' in tempLevMatch) or (' style=dashed, color="purple", ' in tempLevMatch):
                tempLevMatch = tempLevMatch.replace(' style=dashed, color="sienna", ', ' style="dashed,bold", color="sienna", ').replace(' style=dashed, color="purple", ', ' style="dashed,bold", color="purple", ')
            else:
                tempLevMatch = tempLevMatch.replace(' color="sienna", ', ' style=bold, color="sienna", ').replace(' color="purple", ', ' style=bold, color="purple", ')
            line[1] = tempLevMatch

    # add section
    outputList.append("\n/* *** Removal changes *** */\n")
    # read in changed lines
    for line in compReadbackList:
        if len(line) == 2:
            if not line[1] == "}":
                outputList.append(line[1])

    # get keyLines
    keyLines = []
    outputList.append("\n/* *** Key *** */\n")
    # CONC - modify key file if concrete data
    if cfg("CONC_DATA") == 1:
        with open(cfgPathInstall() + 'files/keyfileChangeConc.dot') as f:
            keyLines = f.readlines()
    else:
        with open(cfgPathInstall() + 'files/keyfileChange.dot') as f:
            keyLines = f.readlines()
    for line in keyLines:
        outputList.append(line)

    # close
    outputList.append("}")

    f4 = open(cfgPathInstall() + 'files/modelTableAttsChange.dot', 'w')
    for line in outputList:
        f4.write(line)
    f4.close()

    if cfg_local("WIN_HOST_FILES") == 1:
        changeModelToSVGWinHost()

    if cfg_local("LOCAL_FILES") == 1:
        changeModelToSVGLocal()

if __name__ == '__main__':
    drawChange()
