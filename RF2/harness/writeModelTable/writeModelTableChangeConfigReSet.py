from __future__ import unicode_literals
import io
from platformSpecific import cfgPathInstall


# resets MRCM configuration values for writeModelTableChange.py - back to 'default' values

def cfgReSet():
    tempLineList = []
    f = io.open(cfgPathInstall() + 'files/MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        tempLine = line
        tempLine = tempLine.replace("SHOW_KEY|0", "SHOW_KEY|1")
        tempLine = tempLine.replace("SHOW_CAPTION|0", "SHOW_CAPTION|1")
        tempLine = tempLine.replace("GENERATE_SVG|0", "GENERATE_SVG|1")
        tempLine = tempLine.replace("CLASS_TOOLTIP|0", "CLASS_TOOLTIP|1")
        tempLineList.append(tempLine)
    f.close()
    f = io.open(cfgPathInstall() + 'files/MRCMconfig.cfg', 'w', encoding="UTF-8")
    for line in tempLineList:
        f.write(line)
    f.close()

if __name__ == '__main__':
    cfgReSet()
