from __future__ import unicode_literals
import io
from platformSpecific import cfgPathInstall

# sets MRCM configuration values for writeModelTableChange.py - essentially simplifies diagram

def cfgSet():
    tempLineList = []
    f = io.open(cfgPathInstall() + 'files/MRCMconfig.cfg', 'r', encoding="UTF-8")
    for line in f:
        tempLine = line
        tempLine = tempLine.replace("SHOW_KEY|1", "SHOW_KEY|0")
        tempLine = tempLine.replace("SHOW_CAPTION|1", "SHOW_CAPTION|0")
        tempLine = tempLine.replace("GENERATE_SVG|1", "GENERATE_SVG|0")
        tempLine = tempLine.replace("CLASS_TOOLTIP|1", "CLASS_TOOLTIP|0")
        tempLine = tempLine.replace("D_3|1", "D_3|0")
        tempLineList.append(tempLine)
    f.close()
    f = io.open(cfgPathInstall() + 'files/MRCMconfig.cfg', 'w', encoding="UTF-8")
    for line in tempLineList:
        f.write(line)
    f.close()

if __name__ == '__main__':
    cfgSet()
