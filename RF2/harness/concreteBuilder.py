#!/usr/bin/python
from __future__ import print_function, unicode_literals
import sqlite3
import xlsxwriter
from SCTRF2config import tok, pth
from SCTRF2Helpers import refSetMembersCompareFromConIds_IN, descendantsIDFromConId_IN, refSetTypeFromConId_IN, \
                            refSetMemberCountFromConId_IN, pterm, TCCompare_IN

outPath = pth('RF2_OUT')
snapPath = pth('RF2_SNAP')
# outPath = pth('/home/ed/D/RF2Out.db')

def xlsxFile(fileName, CON_ID, topLine, matrix):
    xbook = xlsxwriter.Workbook(fileName + '.xlsx')
    xsheet = xbook.add_worksheet(CON_ID)
    xsheet.write_row(0, 0, topLine)
    n = 1
    for line in matrix:
        xsheet.write_row(n, 0, line)
        n += 1
    xbook.close()

def refsetCompareToXLSX():
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn2 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("select distinct rightId from rscompare where compareType = 1")
    recordset1 = cursorIn1.fetchall()
    cursorIn2.execute("select distinct leftId from rscompare")
    recordset2 = cursorIn2.fetchall()
    recordset1 = [[item, pterm(item), 1] for item in recordset1]
    recordset2 = [[item, pterm(item), 2] for item in recordset2 if TCCompare_IN(item[0], tok("SIMPLE_REFSETS"))]
    # recordset2 = [[item, pterm(item), 2] for item in recordset2]
    recordset1.sort(key=lambda x: x[1])
    recordset2.sort(key=lambda x: x[1])
    recordset3 = recordset1 + recordset2
    # generate topline
    sizeDict = {}
    topLine = []
    topLine.append("")
    topLine.append("")
    topLine.append("")
    for item in recordset3:
        if item[2] == 1:
            cursorIn3.execute("select distinct rightCount from rscompare where rightId = ?", item[0])
        else:
            cursorIn3.execute("select distinct leftCount from rscompare where leftId = ?", item[0])
        topLine.append(item[0][0])
        if item[2] == 1:
            topLine.append("." + item[1])
        else:
            topLine.append(item[1])
        tempNum = cursorIn3.fetchone()[0]
        topLine.append(tempNum)
        topLine.append("")
        sizeDict[item[0][0]] = tempNum
    # generate matrix
    matrix = []
    for itemLeft in recordset3:
        print(itemLeft)
        tempLine = []
        tempLine.append(itemLeft[0][0])
        if itemLeft[2] == 1:
            tempLine.append("." + itemLeft[1])
        else:
            tempLine.append(itemLeft[1])
        tempLine.append(sizeDict[itemLeft[0][0]])
        for itemRight in recordset3:
            if itemLeft == itemRight:
                tempLine.append("SELF")
                tempLine.append("")
                tempLine.append("")
                tempLine.append("")
            else:
                cursorIn3.execute("select leftProportion, rightProportion from rscompare where leftId = ? and rightId = ?", (itemLeft[0][0], itemRight[0][0]))
                recordset4 = cursorIn3.fetchall()
                if len(recordset4) > 0:
                    tempLine.append(100 - recordset4[0][0])
                    tempLine.append(recordset4[0][0])
                    tempLine.append(recordset4[0][1])
                    tempLine.append(100 - recordset4[0][1])
                else:
                    tempLine.append("")
                    tempLine.append("")
                    tempLine.append("")
                    tempLine.append("")
        matrix.append(tempLine)
    print(str(len(topLine)))
    print(str(len(matrix)))
    topLine[0] = len(topLine)
    topLine[1] = len(matrix) + 1

    xlsxFile('/home/ed/git/rf2repo/sct/RF2/files/refsetComparison', "refsetComparison", topLine, matrix)

def addTable():
    # CREATE TABLE ConInt (id text, active integer, term text, definitionStatusId text);
    # CREATE TABLE DescInt (id, active, conceptId, languageCode, typeId, term, caseSignificanceId, moduleId);
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("attach '" + outPath + "' as RF2Out")
    cursorIn2.execute("attach '" + rf1Path + "' as RF1")
    cursorIn2.execute("DROP TABLE IF EXISTS RF1.ConInt;")
    cursorIn2.execute("CREATE TABLE RF1.ConInt (id text, active integer, term text, definitionStatusId text);")
    conn.commit()
    cur.close()
    conn.close()

def addIndices():
    conn = sqlite3.connect(outPath)
    cur = conn.cursor()
    cur.execute("CREATE INDEX rscompare_compareType_idx ON rscompare (compareType);")
    cur.execute("CREATE INDEX rscompare_leftId_idx ON rscompare (leftId);")
    cur.execute("CREATE INDEX rscompare_rightId_idx ON rscompare (rightId);")
    conn.commit()
    cur.close()
    conn.close()

# initialise

# two sets of code:
# principle - try to keep them isomorphic, but differ where need to.
# actual work will be on snapshot - need to figure how to do this for full data.

# 1. fill tables with *current data*
# numberIndex table with conceptId vs. number (based on number equiv of pt (or transform for uk))


# 2. fill tables with *new data*
# import and load new concreterelationships file
# squeeze concrete relationships into relationships file
# numberIndex table with #number vs. number (should figure how to handle operator too)


