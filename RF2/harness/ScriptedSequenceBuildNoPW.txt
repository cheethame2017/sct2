0. Download files
-----------------
from TRUD - TRUD details
and/or from https://mlds.ihtsdotools.org/#/landing - JIRA details

1. unpack and flatten (for 1 and 2, see note about running unpackAndEscape in D - then goto 3)
---------------------

[steps 1 and 2 can now be run by creating folder tree below (*) and then running unpackAndEscape in D]

(*): create a top-level folder called D, with subfolders Out and RF2. 

note data files should include Nursing, GP, LOINC and Odontogram production packages.
unzip all the data files into RF2, then, running unpackAndEscape in D:

from https://mlds.ihtsdotools.org/#/landing - JIRA details

ed@ed-VirtualBox ~/D $ find ./RF2 -iname '*.txt' -exec cp \{\} ./Out/ \; ##copy
or
ed@ed-VirtualBox ~/D $ find ./RF2 -iname '*.txt' -exec mv \{\} ./Out/ \; ##move

2. escape descriptions
-------------------
ed@ed-VirtualBox ~/D $ cd Out
ed@ed-VirtualBox ~/D/Out $ sed "s_\"_\\\\\"_g" *Description_Snapshot*.txt > sct2_Description_Snapshot_out.txt
ed@ed-VirtualBox rm -i *Description_Snapshot-en*

repeating for full and delta:

sed "s_\"_\\\\\"_g" *Description_Snapshot*.txt > sct2_Description_Snapshot_out.txt
sed "s_\"_\\\\\"_g" *Description_Full*.txt > sct2_Description_Full_out.txt
sed "s_\"_\\\\\"_g" *Description_Delta*.txt > sct2_Description_Delta_out.txt

ed@ed-VirtualBox ~/D/Out $ rm -i *Description_Snapshot-en*
rm: remove regular file 'sct2_Description_Snapshot-en-GB_GB1000000_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Description_Snapshot-en-GB_GB1000001_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Description_Snapshot-en_INT_YYYYMMDD.txt'? y
ed@ed-VirtualBox ~/D/Out $ rm -i *Description_Full-en*
rm: remove regular file 'sct2_Description_Full-en-GB_GB1000000_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Description_Full-en-GB_GB1000001_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Description_Full-en_INT_YYYYMMDD.txt'? y
ed@ed-VirtualBox ~/D/Out $ rm -i *Description_Delta-en*
rm: remove regular file 'sct2_Description_Delta-en-GB_GB1000000_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Description_Delta-en-GB_GB1000001_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Description_Delta-en_INT_YYYYMMDD.txt'? y
ed@ed-VirtualBox ~/D/Out $ 

repeating for full and delta:

sed "s_\"_\\\\\"_g" *Definition_Snapshot*.txt > sct2_Definition_Snapshot_out.txt
sed "s_\"_\\\\\"_g" *Definition_Full*.txt > sct2_Definition_Full_out.txt
sed "s_\"_\\\\\"_g" *Definition_Delta*.txt > sct2_Definition_Delta_out.txt

ed@ed-VirtualBox ~/D/Out $ rm -i *Definition_Snapshot-en*
rm: remove regular file 'sct2_Definition_Snapshot-en-GB_GB1000000_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Definition_Snapshot-en-GB_GB1000001_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Definition_Snapshot-en_INT_YYYYMMDD.txt'? y
ed@ed-VirtualBox ~/D/Out $ rm -i *Definition_Full-en*
rm: remove regular file 'sct2_Definition_Full-en-GB_GB1000000_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Definition_Full-en-GB_GB1000001_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Definition_Full-en_INT_YYYYMMDD.txt'? y
ed@ed-VirtualBox ~/D/Out $ rm -i *Definition_Delta-en*
rm: remove regular file 'sct2_Definition_Delta-en-GB_GB1000000_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Definition_Delta-en-GB_GB1000001_YYYYMMDD.txt'? y
rm: remove regular file 'sct2_Definition_Delta-en_INT_YYYYMMDD.txt'? y
ed@ed-VirtualBox ~/D/Out $

3 preliminary run refset descriptor import (see 3.1 for alternative - simpler - approach):
-------------------------------------------

**if interim build, save elsewhere a copy of working SCTRF2Refset.py - this may be overwritten by interim build**

check python headers files:
path = '/home/ed/D/Out/'
trimpath = '/home/ed/D/Out'

set LAN = in header of importScriptBuilder2.py # 1 if doing NHS RLR build, 2, if doin interim International build

python RF2Import_RDR_prelim.py  [run in git folder - as SNAPSHOT]

[note - ? causes problems for importing OLD refsets where the pattern is not used in most recent snapshot.

this produces importRefSetDescriptorSnapshotYYYYMMDD.txt

copy importRefSetDescriptorSnapshotYYYYMMDD.txt to D, then run

sqlite3 RF2RDR.db
sqlite> .read importRefSetDescriptorFullYYYYMMDD.txt

using RF2RDR.db, run importScriptBuilder.py [run in git folder]

this produces SCTRF2Refset.py which is called by snapshot and full code to process all varieties of active refset.

this also produces ImportGuide.py, which when run in turn produces:

importSnapshotYYYYMMDD.txt or importFullYYYYMMDD.txt or importDeltaYYYYMMDD.txt

...plus, produces
testSnapshotYYYYMMDD.txt or testimportFullYYYYMMDD.txt or testDeltaYYYYMMDD.txt
which can indicate the possibility of appending rows from a delta file (numbers, whether new refset patterns(schema) 
or concepts/descriptions(index)/relationships(index) would be needed).

3.1 run importScriptBuilder2.py (avoids problems with new datatypes not yet included in importScriptBuilder.patternCharacter(inId)\characterDict)
-------------------------------------------------------------------------------------------------------------------------------------------------
**if interim build, save elsewhere a copy of working SCTRF2Refset.py - this may be overwritten by interim build**

check python headers files:
path = '/home/ed/D/Out/'
trimpath = '/home/ed/D/Out'

set LAN = in header of importScriptBuilder2.py # 1 if doing NHS RLR build, 2, if doin interim International build

problem with RF2Import_RDR_prelim.py is that it is too clever - builds rdr file and then distills this back to the c, ci stuff. However... this requires knowledge in code of all datatypes, which may change between releases. Actually easier to use the filenames, which is what importScriptBuilder2.py does [bypassing RF2Import_RDR_prelim.py and simply producing a text file with the c's and i's in). this file also allows me to build a different preferred term view if building UK or international data. need to set the variable LAN in the header to 1 (NHS RDR) or 2 (international). still need to tokenise the id's.

importScriptBuilder2.py produces SCTRF2Refset.py which is called by snapshot and full code to process all varieties of active refset.

importScriptBuilder2.py also produces ImportGuide.py.

3.2 run ImportGuide.py 
----------------------

This produces:

importSnapshotYYYYMMDD.txt or importFullYYYYMMDD.txt or importDeltaYYYYMMDD.txt

...plus, produces
testSnapshotYYYYMMDD.txt or testimportFullYYYYMMDD.txt or testDeltaYYYYMMDD.txt
which can indicate the possibility of appending rows from a delta file (numbers, whether new refset patterns(schema) 
or whether concepts/descriptions(index)/relationships(index) would be needed).

4 copy to the root data folder the following files and then run as prompted from 5 onwards:
-------------------------------------------------------------------------------------------

(consider running two terminal windows (opened in ed@ed-VirtualBox ~/D $) - one for the Sqlite3 instructions and one for the python instructions.

TCAncesCall.py
TCmethod.py
Indexbuilder2.py

buildRF2Out_1.txt
buildRF2Out_2.txt
buildRF2Out_3.txt

and then as they are produced (in 3.2):

importSnapshotYYYYMMDD.txt
importFullYYYYMMDD.txt

5. run import file
------------------
sqlite3 RF2Snap.db
sqlite> .read importSnapshotYYYYMMDD.txt

if:
sqlite3 RF2Full.db
sqlite> .read importFullYYYYMMDD.txt
then stop.
:endif

if rf2snap:

6. create RF2Out.db, CREATE, FILL & INDEX RELLITE, UPDATE DESCRIPTIONS, CREATE, 
--------------------------------------------------------------------------------
FILL & INDEX PREFTERM, CREATE, FILL & INDEX RDSROWS, CREATE DESCRIPTIONSDX
--------------------------------------------------------------------------
.quit
sqlite3 RF2Out.db
sqlite> .read buildRF2Out_1.txt

7. fills descriptionsdx and tc
------------------------------
a. fill descriptiondx:

run Indexbuilder2.py

b. index descriptiondx and create table for tc:

.quit
sqlite3 RF2Out.db
sqlite> .read buildRF2Out_2.txt

** can stop here for quick inspection of snapshot data **
or can proceed to...

c. fill tc:

run TCAncesCall.py

8. add tc indices
--------------------------------
sqlite3 RF2Out.db
sqlite> .read buildRF2Out_3.txt

:endif

9. for neo4j:
--------------

you can add a line in /etc/default/neo4j:
NEO4J_ULIMIT_NOFILE=60000
to set the ulimit setting (60000 open files) for the service.

neo password

note= check paths for .import steps:

run python NeoImport1Prep.py [in git]

this produces NeoDataPrepScripts_1FullYYYYMMDD.txt (with full set of relationship files)

copy this to D and then...

sqlite3 RF2Full2.db
sqlite> .read NeoDataPrepScripts_1FullYYYYMMDD.txt

which gives us rels.csv

sqlite3 RF2Snap.db
sqlite> .read NeoDataPrepScripts_2.txt

which gives us cons.csv

these are then put into the import folder of a freshly expanded neo4j build (N4jx folder).

get the latest release from https://neo4j.com/download-center/ community server tab.

run:
in Linux
ed@ed-VirtualBox ~/N4J/neo4j-community-3.3.4/bin $ ulimit -n 40000
ed@ed-VirtualBox ~/N4J/neo4j-community-3.3.4/bin $ ./neo4j start

Oct 2019 in Linux still getting this complaint despite using 3.5.9, but seems to run:
------------------------------------------------------------------------------
WARNING! You are using an unsupported Java runtime. 
* Please use Oracle(R) Java(TM) 8, OpenJDK(TM) or IBM J9 to run Neo4j.
* Please see https://neo4j.com/docs/ for Neo4j installation instructions.
Active database: graph.db
Directories in use:
  home:         /home/ed/N4Jx/neo4j-community-3.5.9
  config:       /home/ed/N4Jx/neo4j-community-3.5.9/conf
  logs:         /home/ed/N4Jx/neo4j-community-3.5.9/logs
  plugins:      /home/ed/N4Jx/neo4j-community-3.5.9/plugins
  import:       /home/ed/N4Jx/neo4j-community-3.5.9/import
  data:         /home/ed/N4Jx/neo4j-community-3.5.9/data
  certificates: /home/ed/N4Jx/neo4j-community-3.5.9/certificates
  run:          /home/ed/N4Jx/neo4j-community-3.5.9/run
Starting Neo4j.
Started neo4j (pid 4208). It is available at http://localhost:7474/
There may be a short delay until the server is ready.
See /home/ed/N4Jx/neo4j-community-3.5.9/logs/neo4j.log for current status
------------------------------------------------------------------------------

and in Windows:
C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j start

if get stuck with old service (based on earlier community edition):

Find the 'commons daemon service' in task manager or neo4j in the task manager services tab,
right click to 'open services'; find and disable the neo4j service, then:
in new community edition folder:
to update service:
stop and uninstall along old path:
C:\n4j\neo4j-community-3.4.9-windows\neo4j-community-3.4.9\bin>neo4j stop
Neo4j service stopped
C:\n4j\neo4j-community-3.4.9-windows\neo4j-community-3.4.9\bin>neo4j uninstall-service
Neo4j service uninstalled
In new path:
C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j install-service
Neo4j service installed
+/- C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j update-service
Neo4j service updated
then
C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j start
Neo4j service started
(this should produce a new service of automatic start service type)
to start service:
C:\n4j\neo4j-community-3.5.7-windows\neo4j-community-3.5.7\bin>neo4j start
Neo4j service started

then, open the neo4j session in a browser (http://localhost:7474/browser/), 
neo4j first time as password, then change to neo
and run the two scripts in NeoImportScripts.txt

11. Build UKETOut.db file:
--------------------------

RF2BuildUKETOutput.py

which produces:
buildUKETFull20171028.txt
addIndexesUKETFull20171028.txt

then sqlite3 RF2UKOut.db
.read buildUKETFull20171028.txt

then run python UKETBuild.py

then then sqlite3 RF2UKOut.db
.read addIndexesUKETFull20171028.txt

12. move all data files to Data folder
--------------------------------------

13. RF1 
-------

with these files in the data folder, run RF1Builder.py in git.
** make sure all the method triggers at the bottom of the script are uncommented.
this generates a temporary RF1.db file, and then builds a set of text files in /home/ed/RF1/
these can be passed to windows and imported into respective access files.
processing files are in D:\PrepareFIX. Copy clean set of files from DATA FILES to PrepareFIX folder above.
No new linking should be needed.

Import and index all the text files.


TC can now be imported into an intercept of the TCGen code.
** In TCGen:
run the following in ControlModule:

Public Sub createTCwithSupertypeFeatureFromPython1()
    '1. check locations for these two (top and bottom of code in each case) then run:
    Call CreateTCOutFilePython
    '2. fill TCOut file in NewDB by importing TC.txt file from python RF1 build
End Sub

Public Sub createTCwithSupertypeFeatureFromPython2()
    '3. add indices to tcout
    Call indexAddTCOut
End Sub

Public Sub createTCwithSupertypeFeatureFromPython3()
    '4. check TCFullyDefined() is pointing at the right table (Concepts for the whole thing)
    '5. populate with defined concepts programmatically
    Call TCFullyDefined
    '6. populate with primitive concepts using SQL (comment out if doing sample)
    Call AppendPrimitives
    '7. add indices to tc
    Call indexAddTC
    '8. fill tcsubtypecount
    Call TCSubtypeBuild
    '9. index it
    Call indexAddTCSubCount
End Sub

** in SCTData2:
fill conceptsplus [ConceptsPlusQuery and toggle to append]

** in SubsetAnalysisHist.accdb:
import subsetnames into concatnames
import allsubsets into allsubsets
append chapters to subset list [concatnames] from [concatnamesTLC] and then calculate membership [GenerateSheetModule.AddTC()]
generate text file output (check target location in CreateATestfile method) [GenerateSheetModule.comparecall()]
paste into clean SubsetComparison YYYYMM.xlsm

added: run writeModelTable.py - this produces modelTable.txt (plus other things) which can be used to overwrite modelTable in SCTData2.mdb









