import csv
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from SCTRF2SVHelpers import *
import time

#csv file
def csvFile(fileName, CON_ID, topLine, matrix):
    with open(fileName + '.csv', 'wb') as csvfile:
        tcwriter = csv.writer(csvfile, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # print topLine
        tcwriter.writerow(topLine)
        for line in matrix:
            # print line
            tcwriter.writerow(line)

# xls file
def xlsxFile(fileName, CON_ID, topLine, matrix):
    xbook = xlsxwriter.Workbook(fileName + '.xlsx')
    xsheet = xbook.add_worksheet(CON_ID)

    xformat1 = xbook.add_format()
    xformat1.set_rotation(90)
    xsheet.write_row(0, 0, topLine, xformat1)
    xformat2 = xbook.add_format()
    xformat2.set_pattern(1)
    xformat2.set_bg_color('#cccccc')
    xformat3 = xbook.add_format()
    xformat3.set_pattern(1)
    xformat3.set_bg_color('#ccccff')
    xformat4 = xbook.add_format()
    xformat4.set_pattern(1)
    xformat4.set_bg_color('#ddffcc')

    for n in range(2, len(topLine)):
        xsheet.set_column(n, n, 2)

    n = 1
    for line in matrix:
        xsheet.write_row(n, 0, line)
        if line[-1] == "--":
            commentString = ""
            xsheet.write(n, 0, line[0], xformat2)
            historyStatus = SV_SIMconMetaFromConId_IN(line[0], time.strftime("%Y%m%d"))
            commentString += historyStatus[0][4]
            xsheet.write_comment(xl_rowcol_to_cell(n, 0), commentString, {'width': 60, 'height': 10})
        for m in range(1, len(line)):
            if line[m] == "--":
                xsheet.write(n, m, line[m], xformat2)
            if line[m] == "+":
                xsheet.write(n, m, line[m], xformat4)
            elif line[m] == ".":
                xsheet.write(n, m, line[m], xformat3)
        n += 1

    xsheet.set_column(0, 0, 15)
    xsheet.set_column(1, 1, 40)
    xsheet.set_row(0, 75)

    xbook.close()