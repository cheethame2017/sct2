import sqlite3
from SCTRF2config import *
from SCTRF2Refset import *
from SCTRF2Helpers import *
import re

snapPath = pth('RF2_SNAP')

def parsablePretty(inString):
    # break templates on cardinality
    outString = re.sub(r"(\[\[[0|1|*]\.\.[0|1|*]\]\] )",r"\n\t\1",inString)
    # break lists of concept constraints at OR clauses
    outString = re.sub(r"(OR [<<|<|\d])",r"\n\t\t\1",outString)
    # break lists of concept constraints at cardinality
    outString = re.sub(r"(\[[0|1|*]..[0|1|*]\] )",r"\n\t\1",outString)
    return outString

def parsablePretty2(inString):
    tempList = []
    outList = []
    codeList = []
    ptermDict = {}
    outString = inString
    outString = re.sub(r"(\(:)",r"\n\t\1",inString)
    outString = re.sub(r"(\(ObjectSomeValuesFrom)",r"\n\t\1",outString)
    # outString = re.sub(r"(\) ObjectSomeValuesFrom)",r"\n\t\1",outString)
    codeList = re.findall(r"\b\d{6,20}\b", outString)
    for item in codeList:
        if not item in ptermDict:
            ptermDict[item] = item + "|" + pterm(item) + "|"
    tempList = outString.split("\n")
    tempString = ""
    for item2 in tempList:
        tempString = item2
        for item1 in ptermDict:
            tempString = tempString.replace(item1, ptermDict[item1])
        outList.append(tempString)
    returnString = ""
    for item in outList:
        returnString += item + "\n"
    # return ptermList
    return returnString

# 733073007 OWL axiom reference set

# 762103008

def owltester():
    connectionIn2 = sqlite3.connect(snapPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select * from srefset where active=1 and refsetId ='733073007' and referencedcomponentid like '86299006'")
    # cursorIn2.execute("select * from srefset where active=1 and refsetId ='762103008'")
    recordsetIn2 = cursorIn2.fetchall()
    for item in recordsetIn2:
        print item[5]
        print parsablePretty2(item[6])
    cursorIn2.close()
    connectionIn2.close()

owltester()