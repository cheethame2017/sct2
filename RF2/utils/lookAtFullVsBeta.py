from __future__ import print_function, unicode_literals
import io
import sqlite3
from platformSpecific import cfgPathInstall

originalPath = '/home/ed/Data/actual/RF2Snap.db'
betaPath = '/home/ed/Data/RF2Snap.db'

# basically too slow to run as code

def comparessRefsets():
    connectionIn2 = sqlite3.connect(originalPath)
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select * from simpleRefset;")
    recordsetIn2 = cursorIn2.fetchall()
    connectionIn = sqlite3.connect(betaPath)
    cursorIn = connectionIn.cursor()
    cursorIn.execute("select * from simpleRefset;")
    recordsetIn = cursorIn.fetchall()
    f = io.open(cfgPathInstall() + 'files/noMatchRows.txt', 'a', encoding="UTF-8")
    # one way
    counter = 1
    for originalItem in recordsetIn2:
        print(str(counter))
        matchBool = False
        for betaItem in recordsetIn:
            if originalItem == betaItem:
                matchBool = True
        if not matchBool:
            print("noMatch: ", recordsetIn2[0])
            f.write('simpleRefset origNotBeta:' + '\t'.join(str(s) for s in recordsetIn2[0]) + '\n')
        counter += 1
    # and the other
    counter = 1
    for betaItem in recordsetIn:
        print(str(counter))
        matchBool = False
        for originalItem in recordsetIn2:
            if originalItem == betaItem:
                matchBool = True
        if not matchBool:
            print("noMatch: ", recordsetIn[0])
            f.write('simpleRefset betaNotOrig:' + '\t'.join(str(s) for s in recordsetIn[0]) + '\n')
        counter += 1
    f.close()
    cursorIn.close()
    connectionIn.close()
    cursorIn2.close()
    connectionIn2.close()

# better option to run in sql; sample code below:

# select 'in a but not b', a.* 
# from simpleRefset as a left join Beta.simpleRefset as b on a.id = b.id
# where b.id is null
# union all
# select'in b but not a', b.* 
# from Beta.simpleRefset as b left join simpleRefset as a on a.id = b.id
# where a.id is null;

# https://stackoverflow.com/questions/25296309/sqlite-find-the-differences-between-two-tables


comparessRefsets()
