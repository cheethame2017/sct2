import os
import sqlite3


w = raw_input("Input Build Type (Full, Delta, Snapshot): ")
d = raw_input("Input Build Date YYYYMMDD: ")

importItems = []
importMainItems = []

path = '/home/ed/D/Out/'
trimpath = '/home/ed/D/Out'

files = os.listdir(trimpath)                                  # open database

basicFields = 'id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, '

text_file = open("importRefSetDescriptor" + w + d + ".txt", "w")

text_file.write('.echo ON')
text_file.write("\n")
text_file.write('.separator \\t')
text_file.write("\n")

text_file.write('--CREATE TABLE INSTRUCTIONS')
text_file.write("\n")
text_file.write('CREATE TABLE cciRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, componentId1 TEXT, componentId2 TEXT, integer1 INTEGER);')
text_file.write("\n")

for file in files:

    if ('Refset_' in file):
        if (( w + '_' in file) or ( w + '-en_' in file) or ( w + '-en-GB_' in file)):                  # filter for full refsets
            RSType = file[file.find('_'):file.find('Refset')]       # filter file type after first
                                                                    # open each file
            #fileContent = open('C:\\Python27\\Files\\RF2\\' + file, "r")
            # fileLines = fileContent.readlines()                     # break into lines
            if RSType == "_cci":
                print 'ComponentComponentInteger: ', file #, len(fileLines)
                importItems.append('.import ' + path + file + ' cciRefset' + '\n')
            else:
                print 'Pass: ', file #, len(fileLines)

text_file.write('--REFSET IMPORTS')
text_file.write("\n")

for importItem in importItems:
    text_file.write(str(importItem))

# declare fields
tableNames = ['cciRefset']

indexFields = ['id',
               'active'] # should have another for full build (effectiveTime)

fullIndexFields = ['id',
               'active',
               'effectiveTime',
               'moduleId'] # should have another for full build (effectiveTime)

#basic index additions
text_file.write('--BASIC INDEX ADDITIONS')
text_file.write("\n")

for tableName in tableNames:
    if w == 'Full':
        for indexField in fullIndexFields:
            text_file.write('CREATE INDEX ' + tableName + '_' + indexField + '_idx ON ' + tableName + ' (' + indexField + ');')
            text_file.write("\n")
    else:
        for indexField in indexFields:
            text_file.write('CREATE INDEX ' + tableName + '_' + indexField + '_idx ON ' + tableName + ' (' + indexField + ');')
            text_file.write("\n")
    if ('Refset' in tableName):
        text_file.write('CREATE INDEX ' + tableName + '_referencedComponentId_idx ON ' + tableName + ' (referencedComponentId);')
        text_file.write("\n")
        text_file.write('CREATE INDEX ' + tableName + '_refsetId_idx ON ' + tableName + ' (refsetId);')
        text_file.write("\n")


# tidy up header rows
text_file.write('--REMOVE HEADERS')
text_file.write("\n")

for tableName in tableNames:
    text_file.write('DELETE FROM ' + tableName + ' WHERE id == \'id\';')
    text_file.write("\n")

text_file.close()
