from __future__ import print_function, unicode_literals
import sqlite3
import sys

# assist file for wikipedia code - adds preferred terms into usablecodes table when substituted

def updateUsableCodes(dbpath):
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur2 = conn.cursor()
    cur.execute("select * from usableCodesReplacement where newOutputTerm <> '';")
    recordset = cur.fetchall()
    for row in recordset:
        newTerm = row[9]
        print(newTerm)
        cur2.execute("select * from usableCodes where section=? and rank=? and type=? and inputId=? and outputId=?", (row[0], row[1], row[2], row[3], row[8]))
        recordset2 = cur2.fetchall()
        for row2 in recordset2:
            if newTermCheck(newTerm):
                cur2.execute("delete from usableCodes where section=? and rank=? and type=? and inputId=? and outputId=?", (row2[0], row2[1], row2[2], row2[3], row2[8]))
                newTerm += ' NEW TERM INFRINGEMENT'
                cur.execute("update usableCodesReplacement set newOutputTerm=? where oldSection=? and rank=? and type=? and inputId=? and newOutputId = ?", (newTerm, row[0], row[1], row[2], row[3], row[8]))
                conn.commit()
    cur.close()
    cur2.close()
    conn.close()

def newTermCheck(inTerm):
    termStringInfringement = False
    compareStringList = ['pregnan', 'puerper', 'neonat', 'newborn', 'child', 'fetal', 'fetus', 'Pregnan', 'Puerper', 'Neonat', 'Newborn', 'Child', 'Fetal', 'Fetus']
    for item in compareStringList:
        if item in inTerm:
            termStringInfringement = True
    return termStringInfringement

if (sys.version_info > (3, 0)):
    p3 = True
else:
    p3 = False

if p3:
    if len(sys.argv) != 2:
        u = input('original (o) or new (n) code?')
    else:
        u = str(sys.argv[1])
else:
    if len(sys.argv) != 2:
        u = unicode(raw_input('original (o) or new (n) code?'))
    else:
        u = str(sys.argv[1])

if u == 'o':
    # original
    wikiPath = '/home/ed/D/RF2Wiki2.db'
else:
    # new
    wikiPath = '/home/ed/files/wiki2/RF2Wiki.db'

updateUsableCodes(wikiPath)
