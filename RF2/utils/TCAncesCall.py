from __future__ import print_function, unicode_literals
import sqlite3
from TCmethod import upCalc

# setup connections
connectionIn1 = sqlite3.connect('RF2Snap.db')
cursorIn1 = connectionIn1.cursor()
connectionOut = sqlite3.connect('RF2Out.db')
cursorOut = connectionOut.cursor()
####################
#cursorIn1.execute("select id from concepts where id = '275490009' and active = 1")
#cursorIn1.execute("select id from concepts where id = '60600009' and active = 1")
#cursorIn1.execute("select id from concepts where id like '%1000001100' and active = 1")
#cursorIn1.execute("select id from concepts where id like '%100' and id not like '%100000110%' and active = 1")
cursorIn1.execute("select id from concepts where active = 1")
####################
## initialise
recordsetIn1 = cursorIn1.fetchall()
tot = len(recordsetIn1)
print(tot)
progress = 1
####################
## initial call
for idIn in recordsetIn1:
    ancestors = []
    ancestors.append(idIn)
    ancestors.append(upCalc(idIn, ancestors))
####################
## console monitor if set test
    #if (progress % 400 == 0):
        #print "id: %s" % id
        #print 'ancestors: ', len(ancestors) - 1
        #print progress, "/", tot
####################
## console feedback for single test
##    print "id: %s" % id
##    print ancestors
##    print 'ancestors: ', len(ancestors) - 1
####################
#write (each result) to database
    for supertype in ancestors:
        if not supertype is None:
            cursorOut.execute('INSERT INTO TC VALUES (?,?)', (str(idIn[0]), str(supertype[0])))
    ##regular commit
    #connectionOut.commit()
    ##intermittent commit (? quicker)
    if (progress % 500 == 0):
        print("id: %s" % id)
        print('ancestors: ', len(ancestors) - 1)
        print(progress, "/", tot)
        connectionOut.commit()
    progress += 1
####################
# cleanup commit (needed for intermittent)
connectionOut.commit()

cursorIn1.close()
connectionIn1.close()
cursorOut.close()
connectionOut.close()


if __name__ == '__main__':
    print('TCCall is being run by itself')
else:
    print('TCCall being imported from another module')
