from SCTRF2Helpers import *
from SCTRF2Funcs import *
import re
import random
import sqlite3


def readInCON_IDSnap():
    f = open('files/focusSwap.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    return myId 


def tlc():
    tlcList = childrenFromId_IN(('138875005',))
    return tlcList


def setToGraph(inSet):
    # for item in inSet:
    #     print item
    overList = []
    setList = []
    tlcLocal = []
    tlcLocal = tlc()
    for item in tlcLocal:
        temp = item[2]
        overList.append({temp:[(temp,)]})
    for member in inSet:
        # if True:
        # if (member[0][-1] == '0') or (member[0][-1] == '1'):
        # if (member[0][-1] == '1'):
            print member
            for item in overList:
                for key in item.keys():
                    # print member, key
                    if TCCompare_IN(member[0], key):
                        item[key].append(member)
    print '1'
    for item in overList:
        print item
    print '2'
    for item in overList:
        # print item
        # print item.keys(), len(item.values()[0])
        if len(item.values()[0]) > 1:
            setList.append(item)
    print '3'
    for item in setList:
        print item
    return setList


def adjacencyBuild(chapterList):
    adjList = []
    for item in chapterList:
        for key in item.keys():
            adjList.append({key:[]})
    print adjList
    for item1 in adjList:
        for key1 in item1.keys():
            for item2 in chapterList:
                for key2 in item2.keys():
                    if key1 == key2:
                        for item3 in item2.values():
                            for item4 in item3:
                                TCList = []
                                TCList = ancestorsIDFromConId_IN(item4[0])
                                for item5 in TCList:
                                    for item6 in item3:
                                        if item5 == item6[0] and not(item4[0] == item6[0]):
                                            print key1, item4, item6
                                            item1[key1].append(zip(item4, item6)[0])
    # all valid transitive relations...
    # print "before"
    # for item1 in adjList:
    #     for key1 in item1.keys():
    #         for item2 in item1[key1]:
    #             print key1, item2
    #         print
    # remove transitive relations...
    c1 = 0
    for item1 in adjList:
        removeList = []
        for key1 in item1.keys():
            c1 += 1
            # for each tuple in each value list
            c2 = 0
            for value1 in item1[key1]:
                c2 += 1
                # print value1
                # match source
                c3 = 0
                for sourceMatch in item1[key1]:
                    c3 += 1
                    # if there is a source match but not a destinationMatch...
                    if (sourceMatch[0] == value1[0]) and not(sourceMatch[1] == value1[1]):
                        # check to see if there is a destination match
                        c4 = 0
                        for destinationMatch in item1[key1]:
                            # if the destination code of the source match matches the source of another, and 
                            # the destination of this matches the destination of value 1...
                            c4 += 1
                            if (sourceMatch[1] == destinationMatch[0]) and (destinationMatch[1] == value1[1]):
                                # value1 is redundant and can be added to removeList
                                removeList.append(value1)
                            print c1, c2, c3, c4
        # remove rows that are in removeList
        item1[key1] = [item for item in item1[key1] if item not in removeList]
        # print item1[key1]
        item1[key1].append((key1, '138875005'))
    # print "after"
    for item1 in adjList:
        for key1 in item1.keys():
            for item2 in item1[key1]:
                print key1 + "\t" + item2[0] + "\t" + item2[1]
            print
    total = 0
    for item1 in adjList:
        for key1 in item1.keys():
            print key1 + "\t" + pterm(tuplify(key1)) + "\t" + str(len(item1[key1]))
            total += len(item1[key1])
    print "total", total


myId = readInCON_IDSnap()
if TCCompare_IN(myId, '446609009'): #check for simple refset
    myList = refSetMembersFromConId_IN(myId, 'simplerefset', 4)
else: # or
    myPreList = descendantsIDFromConId_IN(myId)
    # 50% or whatever randomise
    k=int(len(myPreList) * 0.5)
    print k
    random.shuffle(myPreList)
    myList = myPreList[0:k]
myChapterList = setToGraph(myList)
adjacencyBuild(myChapterList)