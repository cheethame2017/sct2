#!/usr/bin/python
from __future__ import print_function, unicode_literals
import io

def filterEdgeList(idIn, edgeList):
    returnList = []
    for row in edgeList:
        if row[0] == idIn:
            returnList.append(row[1])
    return returnList

def filterTCList(idIn, TCList):
    returnList = []
    for row in TCList:
        if row[1] == idIn:
            returnList.append(row[0])
    return returnList

def upCalc(idIn, ancestors, edgeList):
    recordsetIn2 = filterEdgeList(idIn, edgeList)
    # if len(recordsetIn2) == 0:
    if not recordsetIn2:
        return ancestors
    else:
        for destinationId in recordsetIn2:
            if destinationId not in ancestors:
                ancestors.append(destinationId)
            upCalc(destinationId, ancestors, edgeList)

def readSSFiles():
    edgeList = []
    actualList = []
    structuralList = []
    pathList = []
    # edges
    try:
        f = io.open('/home/ed/git/rf2repo/sct/RF2/' + 'files' + '/' + 'SS' + '/' + 'SSAdj.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            edgeList.append([line.split("\t")[0], line.strip().split("\t")[1]])
    finally:
        f.close()
    # actual members
    try:
        f = io.open('/home/ed/git/rf2repo/sct/RF2/' + 'files' + '/' + 'SS' + '/' + 'SSMembers.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            actualList.append(line.strip())
    finally:
        f.close()
    # path members
    try:
        f = io.open('/home/ed/git/rf2repo/sct/RF2/' + 'files' + '/' + 'SS' + '/' + 'SSPathMembers.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            pathList.append(line.strip())
    finally:
        f.close()
    # structural members
    try:
        f = io.open('/home/ed/git/rf2repo/sct/RF2/' + 'files' + '/' + 'SS' + '/' + 'SSStructural.txt', 'r', encoding="UTF-8")
        lines = f.readlines()
        for line in lines:
            structuralList.append(line.strip())
    finally:
        f.close()
    return edgeList, actualList, structuralList, pathList

def TCToFile(ancestorDict):
    TCList = []
    try:
        f = io.open('/home/ed/git/rf2repo/sct/RF2/utils/SSTC.txt', 'w', encoding="UTF-8")
        for thing in ancestorDict:
            if not thing == '138875005':
                for thing2 in ancestorDict[thing]:
                    f.write(thing + "\t" + thing2 + "\n")
                    TCList.append([thing, thing2])
    finally:
        f.close()
    return TCList

def TCCountsToFile(myList, TCList, edgeList):
    try:
        f = io.open('/home/ed/git/rf2repo/sct/RF2/utils/SSTCCount.txt', 'w', encoding="UTF-8")
        for thing in myList:
            if not thing == '138875005':
                f.write(thing + "\t" + str(len(filterTCList(thing, TCList))) + "\t" + str(len(filterEdgeList(thing, edgeList))) + "\n")
    finally:
        f.close()

edgeList, actualList, structuralList, pathList = readSSFiles()

TCList = []
ancestorDict = {}
myList = list(set(actualList) | set(structuralList))
for item in myList:
    ancestors = []
    ancestors.append(item)
    ancestors.append(upCalc(item, ancestors, edgeList))
    ancestorDict[item] = [anc for anc in ancestors if anc is not None]
counter = 0
# display
for thing in ancestorDict:
    print(str(counter), thing, ancestorDict[thing])
    counter += 1
# write to file
TCList = TCToFile(ancestorDict)
TCCountsToFile(myList, TCList, edgeList)
