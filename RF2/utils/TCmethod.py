#!/usr/bin/python
from __future__ import print_function, unicode_literals
import sqlite3

def upCalc(idIn, ancestors):
    connectionIn2 = sqlite3.connect('RF2Out.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select destinationId from rellite where sourceId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchall()
    #if len(recordsetIn2) == 0:
    if not recordsetIn2:
        return ancestors
    else:
        for destinationId in recordsetIn2:
            if not destinationId in ancestors:
                ancestors.append(destinationId)
            upCalc(destinationId, ancestors)
    cursorIn2.close()
    connectionIn2.close()

def downCalc(idIn, descendants):
    connectionIn2 = sqlite3.connect('RF2Out.db')
    cursorIn2 = connectionIn2.cursor()
    cursorIn2.execute("select sourceId from rellite where destintionId = ?", (idIn[0],))
    recordsetIn2 = cursorIn2.fetchall()
    #if len(recordsetIn2) == 0:
    if not recordsetIn2:
        return descendants
    else:
        for sourceId in recordsetIn2:
            if not sourceId in descendants:
                descendants.append(sourceId)
            downCalc(sourceId, descendants)
    cursorIn2.close()
    connectionIn2.close()

if __name__ == '__main__':
    print('TCMethod is being run by itself')
else:
    print('TCMethod is being imported from another module')
