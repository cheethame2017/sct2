#!/usr/bin/python
from __future__ import print_function, unicode_literals
import sqlite3
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from SCTRF2config import tok
from SCTRF2Helpers import refSetMembersCompareFromConIds_IN, descendantsIDFromConId_IN, refSetTypeFromConId_IN, \
                            refSetMemberCountFromConId_IN, pterm, TCCompare_IN

def rowAndColToCell(topLine, matrix):
    returnString = "D2:"
    col = len(topLine)
    row = len(matrix)
    returnString += xl_rowcol_to_cell(row, col)
    return returnString, col

def xlsxFile(fileName, CON_ID, topLine, matrix):
    xbook = xlsxwriter.Workbook(fileName + '.xlsx')
    cell_format1 = xbook.add_format({'font_size': 8}) # small fonts for matrix
    cell_format2 = xbook.add_format()
    cell_format2.set_rotation(90) # rotate headers
    cell_format3 = xbook.add_format()
    cell_format3.set_rotation(0) # derotate corner
    xsheet = xbook.add_worksheet(CON_ID)
    xsheet.write_row(0, 0, topLine, cell_format2)
    rangeName, colMax = rowAndColToCell(topLine, matrix)
    xsheet.conditional_format(rangeName, {'type': '3_color_scale'}) # conditional format matrix colours
    xsheet.set_row(0, 200) # top row height
    xsheet.set_column(0, 0, 15, cell_format3) # first col width
    xsheet.set_column(1, 1, 30, cell_format3) # second col width
    xsheet.set_column(3, colMax, 2, cell_format1) # matrix width and set font
    n = 1
    for line in matrix:
        xsheet.write_row(n, 0, line)
        n += 1
    xbook.close()

def refsetCompareToXLSX(outPath, codePath):
    connectionIn2 = sqlite3.connect(outPath)
    cursorIn1 = connectionIn2.cursor()
    cursorIn2 = connectionIn2.cursor()
    cursorIn3 = connectionIn2.cursor()
    cursorIn1.execute("select distinct rightId from rscompare where compareType = 1")
    recordset1 = cursorIn1.fetchall()
    cursorIn2.execute("select distinct leftId from rscompare")
    recordset2 = cursorIn2.fetchall()
    recordset1 = [[item, pterm(item), 1] for item in recordset1]
    recordset2 = [[item, pterm(item), 2] for item in recordset2 if TCCompare_IN(item[0], tok("SIMPLE_REFSETS"))]
    recordset1.sort(key=lambda x: x[1].lower())
    recordset2.sort(key=lambda x: x[1].lower())
    recordset3 = recordset1 + recordset2
    # generate topline
    sizeDict = {}
    topLine = []
    topLine.append("")
    topLine.append("")
    topLine.append("")
    for item in recordset3:
        if item[2] == 1:
            cursorIn3.execute("select distinct rightCount from rscompare where rightId = ?", item[0])
        else:
            cursorIn3.execute("select distinct leftCount from rscompare where leftId = ?", item[0])
        topLine.append(item[0][0])
        if item[2] == 1:
            topLine.append("." + item[1])
        else:
            topLine.append(item[1])
        tempNum = cursorIn3.fetchone()[0]
        topLine.append(tempNum)
        topLine.append("")
        sizeDict[item[0][0]] = tempNum
    # generate matrix
    matrix = []
    for itemLeft in recordset3:
        print(itemLeft)
        tempLine = []
        tempLine.append(itemLeft[0][0])
        if itemLeft[2] == 1:
            tempLine.append("." + itemLeft[1])
        else:
            tempLine.append(itemLeft[1])
        tempLine.append(sizeDict[itemLeft[0][0]])
        for itemRight in recordset3:
            if itemLeft == itemRight:
                tempLine.append("SELF")
                tempLine.append("")
                tempLine.append("")
                tempLine.append("")
            else:
                cursorIn3.execute("select leftProportion, rightProportion from rscompare where leftId = ? and rightId = ?", (itemLeft[0][0], itemRight[0][0]))
                recordset4 = cursorIn3.fetchall()
                if len(recordset4) > 0:
                    tempLine.append(100 - recordset4[0][0])
                    tempLine.append(recordset4[0][0])
                    tempLine.append(recordset4[0][1])
                    tempLine.append(100 - recordset4[0][1])
                else:
                    tempLine.append("")
                    tempLine.append("")
                    tempLine.append("")
                    tempLine.append("")
        matrix.append(tempLine)
    print(str(len(topLine)))
    print(str(len(matrix)))
    topLine[0] = len(topLine)
    topLine[1] = len(matrix) + 1

    xlsxFile(codePath + 'files/xlsx/refsetComparison', "refsetComparison", topLine, matrix)

def refsetCompare(outPath, codePath):
    # initialise
    refsets = descendantsIDFromConId_IN(tok("REFSETS"))
    conn = sqlite3.connect(outPath)
    cur = conn.cursor()
    progress = 1
    total = len(refsets)
    chapterDict = {}
    refsetDict = {}
    testableRefsets = []

    # do main chapters and identify list of testable refsets
    for refset in refsets:
        print(str(progress) + " / " + str(total))
        refsetType = refSetTypeFromConId_IN(refset[0])[0][0]
        if refSetMemberCountFromConId_IN(refset[0], refsetType) > 0:
            leftCount, otherRows, refsetDict = refSetMembersCompareFromConIds_IN(refset[0], "", refsetType, 1, {}, refsetDict, True, 0)
            tempList = []
            for row in otherRows:
                row.insert(1, leftCount)
                row.insert(0, refset[0])
                row.insert(0, 1)
                print(row)
                cur.execute("insert into rscompare values (?, ? , ? , ? , ? , ? , ? , ? , ? , ?)", tuple(row))
                tempList.append(row[2])
            if len(tempList) > 0:
                chapterDict[refset[0]] = tempList
                if not refset in testableRefsets:
                    testableRefsets.append(refset)
        progress += 1
        conn.commit()

    progress = 1
    total = len(testableRefsets)

    # test the testable refsets against each other.
    for refset in testableRefsets:
        print(str(progress) + " / " + str(total))
        refsetType = refSetTypeFromConId_IN(refset[0])[0][0]
        leftCount, otherRows, refsetDict = refSetMembersCompareFromConIds_IN(refset[0], "", refsetType, 2, chapterDict, refsetDict, True, 0)
        for row in otherRows:
            row.insert(1, leftCount)
            row.insert(0, refset[0])
            row.insert(0, 2)
            print(row)
            cur.execute("insert into rscompare values (?, ? , ? , ? , ? , ? , ? , ? , ? , ?)", tuple(row))
        progress += 1
        conn.commit()

    # add indices
    cur.execute("CREATE INDEX rscompare_compareType_idx ON rscompare (compareType);")
    cur.execute("CREATE INDEX rscompare_leftId_idx ON rscompare (leftId);")
    cur.execute("CREATE INDEX rscompare_rightId_idx ON rscompare (rightId);")
    conn.commit()

    cur.close()
    conn.close()

    # write summary to XLSX file
    refsetCompareToXLSX(outPath, codePath)

if __name__ == '__main__':
    # outPath = pth('RF2_OUT')
    codePath = "/home/ed/git/rf2repo/sct/RF2/"
    outPath = '/home/ed/D/RF2Out.db'
    refsetCompare(outPath, codePath)
