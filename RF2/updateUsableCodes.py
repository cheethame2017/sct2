from __future__ import print_function, unicode_literals
import sqlite3
import sys
from SCTRF2Helpers import pterm

# assist file for wikipedia code - adds preferred terms into usablecodes table when substituted

def updateUsableCodes(dbpath):
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur.execute("select * from usableCodes where outputTerm='';")
    recordset = cur.fetchall()
    for row in recordset:
        newTerm = pterm(row[5])
        print("UPUC", newTerm)
        if newTermCheck(newTerm):
            cur.execute("update usableCodes set outputTerm=? where outputId = ?", (newTerm, row[5]))
        else:
            cur.execute("delete from usableCodes where section=? and rank=? and type=? and inputId=? and outputId=?", (row[0], row[1], row[2], row[3], row[5]))
        conn.commit()
    cur.execute("select * from usableCodesReplacement where newOutputId <>'' and newOutputTerm='';")
    recordset = cur.fetchall()
    for row in recordset:
        newTerm = pterm(row[8])
        print("UPUC", newTerm)
        if newTermCheck(newTerm):
            newTerm += ' NEW TERM INFRINGEMENT'
        cur.execute("update usableCodesReplacement set newOutputTerm=? where newOutputId = ?", (newTerm, row[8]))
        conn.commit()
    cur.close()
    conn.close()

def newTermCheck(inTerm):
    termStringInfringement = False
    compareStringList = ['pregnan', 'puerper', 'neonat', 'newborn', 'child', 'fetal', 'fetus', 'Pregnan', 'Puerper', 'Neonat', 'Newborn', 'Child', 'Fetal', 'Fetus']
    for item in compareStringList:
        if item in inTerm:
            termStringInfringement = True
    return termStringInfringement

if (sys.version_info > (3, 0)):
    p3 = True
else:
    p3 = False

if p3:
    if len(sys.argv) != 2:
        u = input('original (o) or new (n) code?')
    else:
        u = str(sys.argv[1])
else:
    if len(sys.argv) != 2:
        u = raw_input('original (o) or new (n) code?').decode('UTF-8')
    else:
        u = str(sys.argv[1])

if u == 'o':
    # original
    wikiPath = '/home/ed/D/RF2Wiki.db'
else:
    # new
    wikiPath = '/home/ed/files/wiki2/RF2Wiki.db'

updateUsableCodes(wikiPath)
