--
-- File generated with SQLiteStudio v3.3.3 on Thu Sep 30 08:23:22 2021
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: cciRefset
CREATE TABLE cciRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT, ComponentId2 TEXT, Integer1 INTEGER);

-- Table: ciRefset
CREATE TABLE ciRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT, Integer1 INTEGER);

-- Table: cissccRefset
CREATE TABLE cissccRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT, Integer1 INTEGER, String1 TEXT, String2 TEXT, ComponentId2 TEXT, ComponentId3 TEXT);

-- Table: Concepts
CREATE TABLE Concepts(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, definitionStatusId TEXT);

-- Table: cRefset
CREATE TABLE cRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT);

-- Table: Definitions
CREATE TABLE Definitions(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT);

-- Table: Descriptions
CREATE TABLE Descriptions(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT);

-- Table: iisssccRefset
CREATE TABLE iisssccRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, Integer1 INTEGER, Integer2 INTEGER, String1 TEXT, String2 TEXT, String3 TEXT, ComponentId1 TEXT, ComponentId2 TEXT);

-- Table: iisssciRefset
CREATE TABLE iisssciRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, Integer1 INTEGER, Integer2 INTEGER, String1 TEXT, String2 TEXT, String3 TEXT, ComponentId1 TEXT, Integer3 INTEGER);

-- Table: iissscRefset
CREATE TABLE iissscRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, Integer1 INTEGER, Integer2 INTEGER, String1 TEXT, String2 TEXT, String3 TEXT, ComponentId1 TEXT);

-- Table: Relationships
CREATE TABLE Relationships(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, sourceId TEXT, destinationId TEXT, relationshipGroup INTEGER, typeId TEXT, characteristicTypeId TEXT, modifierId TEXT);

-- Table: scccRefset
CREATE TABLE scccRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, String1 TEXT, ComponentId1 TEXT, ComponentId2 TEXT, ComponentId3 TEXT);

-- Table: SimpleRefset
CREATE TABLE SimpleRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT);

-- Table: sRefset
CREATE TABLE sRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, String1 TEXT);

-- Table: sscccRefset
CREATE TABLE sscccRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT, ComponentId1 TEXT, ComponentId2 TEXT, ComponentId3 TEXT);

-- Table: ssccRefset
CREATE TABLE ssccRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT, ComponentId1 TEXT, ComponentId2 TEXT);

-- Table: ssRefset
CREATE TABLE ssRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT);

-- Table: sssssssRefset
CREATE TABLE sssssssRefset(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT, String3 TEXT, String4 TEXT, String5 TEXT, String6 TEXT, String7 TEXT);

-- Index: cciRefset_active_idx
CREATE INDEX cciRefset_active_idx ON cciRefset (active);

-- Index: cciRefset_id_idx
CREATE INDEX cciRefset_id_idx ON cciRefset (id);

-- Index: cciRefset_referencedComponentId_idx
CREATE INDEX cciRefset_referencedComponentId_idx ON cciRefset (referencedComponentId);

-- Index: cciRefset_refsetId_idx
CREATE INDEX cciRefset_refsetId_idx ON cciRefset (refsetId);

-- Index: ciRefset_active_idx
CREATE INDEX ciRefset_active_idx ON ciRefset (active);

-- Index: ciRefset_id_idx
CREATE INDEX ciRefset_id_idx ON ciRefset (id);

-- Index: ciRefset_referencedComponentId_idx
CREATE INDEX ciRefset_referencedComponentId_idx ON ciRefset (referencedComponentId);

-- Index: ciRefset_refsetId_idx
CREATE INDEX ciRefset_refsetId_idx ON ciRefset (refsetId);

-- Index: cissccRefset_active_idx
CREATE INDEX cissccRefset_active_idx ON cissccRefset (active);

-- Index: cissccRefset_id_idx
CREATE INDEX cissccRefset_id_idx ON cissccRefset (id);

-- Index: cissccRefset_referencedComponentId_idx
CREATE INDEX cissccRefset_referencedComponentId_idx ON cissccRefset (referencedComponentId);

-- Index: cissccRefset_refsetId_idx
CREATE INDEX cissccRefset_refsetId_idx ON cissccRefset (refsetId);

-- Index: Concepts_active_idx
CREATE INDEX Concepts_active_idx ON Concepts (active);

-- Index: Concepts_id_idx
CREATE INDEX Concepts_id_idx ON Concepts (id);

-- Index: concepts_moduleId_idx
CREATE INDEX concepts_moduleId_idx ON concepts (moduleId);

-- Index: cRefset_active_idx
CREATE INDEX cRefset_active_idx ON cRefset (active);

-- Index: crefset_componentId1_idx
CREATE INDEX crefset_componentId1_idx ON crefset (componentId1);

-- Index: cRefset_id_idx
CREATE INDEX cRefset_id_idx ON cRefset (id);

-- Index: cRefset_referencedComponentId_idx
CREATE INDEX cRefset_referencedComponentId_idx ON cRefset (referencedComponentId);

-- Index: cRefset_refsetId_idx
CREATE INDEX cRefset_refsetId_idx ON cRefset (refsetId);

-- Index: Definitions_active_idx
CREATE INDEX Definitions_active_idx ON Definitions (active);

-- Index: Definitions_conceptId_idx
CREATE INDEX Definitions_conceptId_idx ON Definitions (conceptId);

-- Index: Definitions_id_idx
CREATE INDEX Definitions_id_idx ON Definitions (id);

-- Index: Descriptions_active_idx
CREATE INDEX Descriptions_active_idx ON Descriptions (active);

-- Index: Descriptions_conceptId_idx
CREATE INDEX Descriptions_conceptId_idx ON Descriptions (conceptId);

-- Index: Descriptions_id_idx
CREATE INDEX Descriptions_id_idx ON Descriptions (id);

-- Index: descriptions_moduleId_idx
CREATE INDEX descriptions_moduleId_idx ON descriptions (moduleId);

-- Index: Descriptions_term_idx
CREATE INDEX Descriptions_term_idx ON Descriptions (term);

-- Index: iisssccRefset_active_idx
CREATE INDEX iisssccRefset_active_idx ON iisssccRefset (active);

-- Index: iisssccRefset_id_idx
CREATE INDEX iisssccRefset_id_idx ON iisssccRefset (id);

-- Index: iisssccRefset_referencedComponentId_idx
CREATE INDEX iisssccRefset_referencedComponentId_idx ON iisssccRefset (referencedComponentId);

-- Index: iisssccRefset_refsetId_idx
CREATE INDEX iisssccRefset_refsetId_idx ON iisssccRefset (refsetId);

-- Index: iisssciRefset_active_idx
CREATE INDEX iisssciRefset_active_idx ON iisssciRefset (active);

-- Index: iisssciRefset_id_idx
CREATE INDEX iisssciRefset_id_idx ON iisssciRefset (id);

-- Index: iisssciRefset_referencedComponentId_idx
CREATE INDEX iisssciRefset_referencedComponentId_idx ON iisssciRefset (referencedComponentId);

-- Index: iisssciRefset_refsetId_idx
CREATE INDEX iisssciRefset_refsetId_idx ON iisssciRefset (refsetId);

-- Index: iissscRefset_active_idx
CREATE INDEX iissscRefset_active_idx ON iissscRefset (active);

-- Index: iissscRefset_id_idx
CREATE INDEX iissscRefset_id_idx ON iissscRefset (id);

-- Index: iissscRefset_referencedComponentId_idx
CREATE INDEX iissscRefset_referencedComponentId_idx ON iissscRefset (referencedComponentId);

-- Index: iissscRefset_refsetId_idx
CREATE INDEX iissscRefset_refsetId_idx ON iissscRefset (refsetId);

-- Index: Relationships_active_idx
CREATE INDEX Relationships_active_idx ON Relationships (active);

-- Index: Relationships_CharacteristicTypeId_idx
CREATE INDEX Relationships_CharacteristicTypeId_idx ON Relationships (characteristicTypeId);

-- Index: Relationships_DestinationId_idx
CREATE INDEX Relationships_DestinationId_idx ON Relationships (destinationId);

-- Index: Relationships_id_idx
CREATE INDEX Relationships_id_idx ON Relationships (id);

-- Index: relationships_moduleId_idx
CREATE INDEX relationships_moduleId_idx ON relationships (moduleId);

-- Index: Relationships_SourceId_idx
CREATE INDEX Relationships_SourceId_idx ON Relationships (sourceId);

-- Index: Relationships_typeId_idx
CREATE INDEX Relationships_typeId_idx ON Relationships (typeId);

-- Index: scccRefset_active_idx
CREATE INDEX scccRefset_active_idx ON scccRefset (active);

-- Index: scccRefset_id_idx
CREATE INDEX scccRefset_id_idx ON scccRefset (id);

-- Index: scccRefset_referencedComponentId_idx
CREATE INDEX scccRefset_referencedComponentId_idx ON scccRefset (referencedComponentId);

-- Index: scccRefset_refsetId_idx
CREATE INDEX scccRefset_refsetId_idx ON scccRefset (refsetId);

-- Index: SimpleRefset_active_idx
CREATE INDEX SimpleRefset_active_idx ON SimpleRefset (active);

-- Index: SimpleRefset_id_idx
CREATE INDEX SimpleRefset_id_idx ON SimpleRefset (id);

-- Index: SimpleRefset_referencedComponentId_idx
CREATE INDEX SimpleRefset_referencedComponentId_idx ON SimpleRefset (referencedComponentId);

-- Index: SimpleRefset_refsetId_idx
CREATE INDEX SimpleRefset_refsetId_idx ON SimpleRefset (refsetId);

-- Index: sRefset_active_idx
CREATE INDEX sRefset_active_idx ON sRefset (active);

-- Index: sRefset_id_idx
CREATE INDEX sRefset_id_idx ON sRefset (id);

-- Index: sRefset_referencedComponentId_idx
CREATE INDEX sRefset_referencedComponentId_idx ON sRefset (referencedComponentId);

-- Index: sRefset_refsetId_idx
CREATE INDEX sRefset_refsetId_idx ON sRefset (refsetId);

-- Index: sscccRefset_active_idx
CREATE INDEX sscccRefset_active_idx ON sscccRefset (active);

-- Index: sscccRefset_id_idx
CREATE INDEX sscccRefset_id_idx ON sscccRefset (id);

-- Index: sscccRefset_referencedComponentId_idx
CREATE INDEX sscccRefset_referencedComponentId_idx ON sscccRefset (referencedComponentId);

-- Index: sscccRefset_refsetId_idx
CREATE INDEX sscccRefset_refsetId_idx ON sscccRefset (refsetId);

-- Index: ssccRefset_active_idx
CREATE INDEX ssccRefset_active_idx ON ssccRefset (active);

-- Index: ssccRefset_id_idx
CREATE INDEX ssccRefset_id_idx ON ssccRefset (id);

-- Index: ssccRefset_referencedComponentId_idx
CREATE INDEX ssccRefset_referencedComponentId_idx ON ssccRefset (referencedComponentId);

-- Index: ssccRefset_refsetId_idx
CREATE INDEX ssccRefset_refsetId_idx ON ssccRefset (refsetId);

-- Index: ssRefset_active_idx
CREATE INDEX ssRefset_active_idx ON ssRefset (active);

-- Index: ssRefset_id_idx
CREATE INDEX ssRefset_id_idx ON ssRefset (id);

-- Index: ssRefset_referencedComponentId_idx
CREATE INDEX ssRefset_referencedComponentId_idx ON ssRefset (referencedComponentId);

-- Index: ssRefset_refsetId_idx
CREATE INDEX ssRefset_refsetId_idx ON ssRefset (refsetId);

-- Index: sssssssRefset_active_idx
CREATE INDEX sssssssRefset_active_idx ON sssssssRefset (active);

-- Index: sssssssRefset_id_idx
CREATE INDEX sssssssRefset_id_idx ON sssssssRefset (id);

-- Index: sssssssRefset_referencedComponentId_idx
CREATE INDEX sssssssRefset_referencedComponentId_idx ON sssssssRefset (referencedComponentId);

-- Index: sssssssRefset_refsetId_idx
CREATE INDEX sssssssRefset_refsetId_idx ON sssssssRefset (refsetId);

-- View: foundationmetadata
CREATE VIEW "foundationmetadata" AS select conceptId, term from descriptions where term like "% metadata concept)%" and active = 1 order by term;

-- View: prefTerm
CREATE VIEW "prefTerm" AS select descriptions.conceptId, descriptions.id, descriptions.term from descriptions, rdsrows where descriptions.id = rdsrows.id and descriptions.typeId = (select conceptId from foundationmetadata where term like "%synonym%") and rdsrows.acc = (select conceptId from foundationmetadata where term like "%preferred%");

-- View: rdscodes
CREATE VIEW "rdscodes" AS select conceptId, term from foundationMetadata where conceptId IN ("999001261000000100", "999000691000001104");

-- View: rdsrows
CREATE VIEW "rdsrows" AS select referencedComponentId as id, componentId1 as acc from crefset where refsetId in (select conceptId from rdscodes) and active = 1;

-- View: refsetModules
CREATE VIEW "refsetModules" AS select distinct 'Concepts' as filetype, 'Concepts' as refsetId, moduleId from Concepts UNION select distinct 'Descriptions' as filetype, 'Descriptions' as refsetId, moduleId from Descriptions UNION select distinct 'Definitions' as filetype, 'Definitions' as refsetId, moduleId from Definitions UNION select distinct 'Relationships' as filetype, 'Relationships' as refsetId, moduleId from Relationships UNION select distinct 'simplerefset' as filetype, refsetId, moduleId from simplerefset UNION select distinct 'crefset' as filetype, refsetId, moduleId from crefset UNION select distinct 'ccirefset' as filetype, refsetId, moduleId from ccirefset UNION select distinct 'cirefset' as filetype, refsetId, moduleId from cirefset UNION select distinct 'cissccrefset' as filetype, refsetId, moduleId from cissccrefset UNION select distinct 'iissscrefset' as filetype, refsetId, moduleId from iissscrefset UNION select distinct 'iisssccrefset' as filetype, refsetId, moduleId from iisssccrefset UNION select distinct 'iissscirefset' as filetype, refsetId, moduleId from iissscirefset UNION select distinct 'srefset' as filetype, refsetId, moduleId from srefset UNION select distinct 'scccrefset' as filetype, refsetId, moduleId from scccrefset UNION select distinct 'ssrefset' as filetype, refsetId, moduleId from ssrefset UNION select distinct 'ssccrefset' as filetype, refsetId, moduleId from ssccrefset UNION select distinct 'sscccrefset' as filetype, refsetId, moduleId from sscccrefset UNION select distinct 'sssssssrefset' as filetype, refsetId, moduleId from sssssssrefset;

-- View: refsets
CREATE VIEW "refsets" AS select * from foundationmetadata where term like "%reference set (%";

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
