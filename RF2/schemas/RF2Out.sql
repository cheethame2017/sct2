--
-- File generated with SQLiteStudio v3.3.3 on Thu Sep 30 08:22:00 2021
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: conc1
CREATE TABLE conc1(id text, num integer);

-- Table: conc2
CREATE TABLE conc2(id text, num integer);

-- Table: Descriptionsdx
CREATE TABLE Descriptionsdx(id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, unideterm TEXT, caseSignificanceId TEXT);

-- Table: prefterm
CREATE TABLE prefterm(conId TEXT, descId TEXT, term TEXT);

-- Table: rdsrows
CREATE TABLE rdsrows(id TEXT, acc TEXT);

-- Table: refsetModules
CREATE TABLE refsetModules(filetype TEXT, refsetId TEXT , moduleId TEXT);

-- Table: rellite
CREATE TABLE rellite(sourceId TEXT, destinationId TEXT);

-- Table: rscompare
CREATE TABLE rscompare(compareType INTEGER, leftId TEXT, rightId TEXT, leftCount INTEGER, onlyLeftCount INTEGER, intersectionCount INTEGER, leftProportion REAL, rightProportion REAL, onlyRightCount INTEGER, rightCount INTEGER);

-- Table: tc
CREATE TABLE tc(subtypeId TEXT, supertypeId TEXT);

-- Table: TCSubCount
CREATE TABLE TCSubCount (SuperTypeId TEXT, STCount INTEGER);

-- Table: unideterms
CREATE VIRTUAL TABLE unideterms USING fts4(id, conceptId, term, unideterm, typeId, rdstype);

-- Table: unideterms_content
CREATE TABLE 'unideterms_content'(docid INTEGER PRIMARY KEY, 'c0id', 'c1conceptId', 'c2term', 'c3unideterm', 'c4typeId', 'c5rdstype');

-- Table: unideterms_docsize
CREATE TABLE 'unideterms_docsize'(docid INTEGER PRIMARY KEY, size BLOB);

-- Table: unideterms_segdir
CREATE TABLE 'unideterms_segdir'(level INTEGER,idx INTEGER,start_block INTEGER,leaves_end_block INTEGER,end_block INTEGER,root BLOB,PRIMARY KEY(level, idx));

-- Table: unideterms_segments
CREATE TABLE 'unideterms_segments'(blockid INTEGER PRIMARY KEY, block BLOB);

-- Table: unideterms_stat
CREATE TABLE 'unideterms_stat'(id INTEGER PRIMARY KEY, value BLOB);

-- Index: descriptionsdx_active_idx
CREATE INDEX descriptionsdx_active_idx ON descriptionsdx (active);

-- Index: descriptionsdx_conceptId_idx
CREATE INDEX descriptionsdx_conceptId_idx ON descriptionsdx (conceptId);

-- Index: descriptionsdx_id_idx
CREATE INDEX descriptionsdx_id_idx ON descriptionsdx (id);

-- Index: prefterm_ConceptId_idx
CREATE INDEX prefterm_ConceptId_idx ON prefterm (conId);

-- Index: prefterm_DescriptionId_idx
CREATE INDEX prefterm_DescriptionId_idx ON prefterm (descId);

-- Index: rdsrows_acc_idx
CREATE INDEX rdsrows_acc_idx ON rdsrows (acc);

-- Index: rdsrows_Id_idx
CREATE INDEX rdsrows_Id_idx ON rdsrows (id);

-- Index: refsetModules_refsetId_idx
CREATE INDEX refsetModules_refsetId_idx ON refsetModules (refsetId);

-- Index: rellite_destinationId_idx
CREATE INDEX rellite_destinationId_idx ON rellite (destinationId);

-- Index: rellite_sourceId_idx
CREATE INDEX rellite_sourceId_idx ON rellite (sourceId);

-- Index: rscompare_compareType_idx
CREATE INDEX rscompare_compareType_idx ON rscompare (compareType);

-- Index: rscompare_leftId_idx
CREATE INDEX rscompare_leftId_idx ON rscompare (leftId);

-- Index: rscompare_rightId_idx
CREATE INDEX rscompare_rightId_idx ON rscompare (rightId);

-- Index: Tc_SubtypeId_idx
CREATE INDEX Tc_SubtypeId_idx ON TC (subtypeId);

-- Index: Tc_SupertypeId_idx
CREATE INDEX Tc_SupertypeId_idx ON TC (supertypeId);

-- Index: TCSubCount_SupertypeId_idx
CREATE INDEX TCSubCount_SupertypeId_idx ON TCSubCount (supertypeId);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
