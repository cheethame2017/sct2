-- SCTRF2Snap.db
-- high defining relationship count
select sourceId, count(sourceId) from relationships where active=1 and typeId<>'116680003' group by sourceId order by count(sourceId) desc;
-- high parent relationship count
select sourceId, count(sourceId) from relationships where active=1 and typeId='116680003' group by sourceId order by count(sourceId) desc;
-- high child relationship count
select destinationId, count(destinationId) from relationships where active=1 and typeId='116680003' group by destinationId order by count(destinationId) desc;
-- active descriptions not in rlr
select descriptions.id from descriptions where descriptions.active=1 and descriptions.id not in (select rdsrows.id from rdsrows);
-- busy cross map diagrams
SELECT * FROM iisssciRefset WHERE active=1 AND Integer1 >= 2 AND Integer2 >= 2 ORDER BY Integer3 DESC;
-- long chain of relationships (may vary so need lengthening/shortening to get best results for a given release)
select r1.sourceId, r5.destinationId 
from relationships r1, relationships r2, relationships r3, relationships r4, relationships r5 
where r1.active=1 and r1.typeId<>'116680003' 
and r2.active=1 and r2.typeId<>'116680003' 
and r3.active=1 and r3.typeId<>'116680003' 
and r4.active=1 and r4.typeId<>'116680003' 
and r5.active=1 and r5.typeId<>'116680003' 
and r1.destinationId = r2.sourceId 
and r2.destinationId = r3.sourceId 
and r3.destinationId = r4.sourceId 
and r4.destinationId = r5.sourceId;

-- SCTRF2Out.db
-- high descendant count
select supertypeId, count(supertypeId) from tc group by supertypeId order by count(supertypeId) desc;
-- high ancestor count
select subtypeId, count(subtypeId) from tc group by subtypeId order by count(subtypeId) desc;
-- product is a medicinal product
select sourceId, destinationId, d1.term, d2.term from rellite, rdsrows as r1, rdsrows as r2, Descriptionsdx as d1, Descriptionsdx as d2 
where d1.conceptId=sourceId and d1.id=r1.id and d2.conceptId=destinationId and d2.id=r2.id 
and d1.typeId='900000000000003001' and d2.typeId='900000000000003001' and r1.acc='900000000000548007' and r2.acc='900000000000548007' 
and d1.term like '%(product)' and d2.term like '%(medicinal product)';
-- physical object is a product
select sourceId, destinationId, d1.term, d2.term from rellite, rdsrows as r1, rdsrows as r2, Descriptionsdx as d1, Descriptionsdx as d2 
where d1.conceptId=sourceId and d1.id=r1.id and d2.conceptId=destinationId and d2.id=r2.id 
and d1.typeId='900000000000003001' and d2.typeId='900000000000003001' and r1.acc='900000000000548007' and r2.acc='900000000000548007' 
and d1.term like '%(physical object)' and d2.term like '%(product)';
-- any children of clinical drug
select sourceId, destinationId, d1.term, d2.term from rellite, rdsrows as r1, rdsrows as r2, Descriptionsdx as d1, Descriptionsdx as d2 
where d1.conceptId=sourceId and d1.id=r1.id and d2.conceptId=destinationId and d2.id=r2.id 
and d1.typeId='900000000000003001' and d2.typeId='900000000000003001' and r1.acc='900000000000548007' and r2.acc='900000000000548007' 
and d2.term like '%(clinical drug)';
-- fsn ends with ... tag (+/- UK clinical module - note this will not detect -> UK content unless the descriptions have been remodulated).
select d1.conceptId, d1.term from Descriptionsdx as d1, rdsrows as r1, TCSubCount as t1 
where d1.id=r1.id and d1.conceptId = t1.supertypeId
and r1.acc='900000000000548007' and d1.moduleId = '999000011000000103'
and d1.active=1 and d1.typeId='900000000000003001' 
and d1.term like '%(...)';