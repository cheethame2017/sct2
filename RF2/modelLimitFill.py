#!/usr/bin/python
from __future__ import print_function, unicode_literals
import io
import sqlite3
from SCTRF2Helpers import parentIdsFromId_IN, descendantCountFromConId_IN
from SCTRF2Funcs import splitTag
from SCTRF2Role import writeOutList
from SCTRF2config import tok
from platformSpecific import cfgPathInstall, sep

# fills modelLimit.txt - specifies domains for model drawing

def fillModelLimit(snapPath):
    conn = sqlite3.connect(snapPath)
    cur = conn.cursor()
    cur.execute("select referencedComponentId, string1 from sssssssRefset where active=1")
    recordsetIn1 = cur.fetchall()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimit.txt', 'w', encoding="UTF-8")
    f2 = io.open(cfgPathInstall() + 'files' + sep() + 'mrcm' + sep() + 'modelLimitID.txt', 'w', encoding="UTF-8")
    f.write("Generated from domain constraint field (active and inactive rows) of MRCM domain international reference set\n")
    f.write("select string1 from sssssssRefset;\n")
    f.write("Guide: prefix each row to be included with '#' [none prefixed=all shown]\n")
    for item in recordsetIn1:
        f.write(item[1] + '\n')
        f2.write(item[0] + '\n')
    f.close()
    f2.close()
    cur.close()
    conn.close()

def joinMaxIdToCount(count, tag, idMatchList):
    for item in idMatchList:
        if descendantCountFromConId_IN(item) == count:
            fsnTag = splitTag(item)
            if fsnTag == tag:
                return item

def buildTags(RF2Outpath):
    conn = sqlite3.connect(RF2Outpath)
    cur2 = conn.cursor()
    tempList = []
    # no drugs or physical objects
    cur2.execute("SELECT t1.SupertypeId as A, t1.STCount as B FROM TCSubCount t1 \
                EXCEPT \
                SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                WHERE t2.supertypeId IN ('" + tok("PHARM_PROD") + "', '" + tok("PHYS_OBJ") + "') \
                and t2.SubtypeId=t3.supertypeId order by B desc;")
    recordsetIn2 = cur2.fetchall()
    setToRun = [[item[0]] for item in recordsetIn2[:]]
    progress = 1
    changeList = []
    topIdList = []
    idMatchList = []
    tagCountDict = {}
    tagItemDict = {}
    topIdDict = {}
    topNumDict = {}
    tagCache = {}
    descCountCache = {}
    topNumCounterDict = {}
    topNumCounterDict['SNOMED RT+CTV3'] = [1, tok("ROOT")]
    tot = len(setToRun)
    for row in setToRun:
        tempList = parentIdsFromId_IN(row)
        for item in tempList:
            if item[0] not in tagCache:
                fsnTag1 = splitTag(item[0])
                tagCache[item[0]] = fsnTag1
            else:
                fsnTag1 = tagCache[item[0]]
            if row[0] not in tagCache:
                fsnTag2 = splitTag(row[0])
                tagCache[row[0]] = fsnTag2
            else:
                fsnTag2 = tagCache[row[0]]
            if fsnTag2 not in tagCountDict:
                tagCountDict[fsnTag2] = 1
                tagItemDict[fsnTag2] = [row[0]]
            elif row[0] not in tagItemDict[fsnTag2]:
                tagItemDict[fsnTag2].append(row[0])
                tagCountDict[fsnTag2] += 1
            if fsnTag1 != fsnTag2: # if a tag handoff...
                if [fsnTag1, fsnTag2] not in changeList: # track any tag changes for dot file.
                    changeList.append([fsnTag1, fsnTag2])
                if item[0] not in descCountCache: # cache descendant counts
                    desc = descendantCountFromConId_IN(item[0]) # descendant count of parent
                    descCountCache[item[0]] = desc
                else:
                    desc = descCountCache[item[0]]
                if fsnTag1 + "|" + fsnTag2 not in topIdDict: # if not in id, add as list
                    topIdDict[fsnTag1 + "|" + fsnTag2] = [[item[0], row[0]]]
                    topIdList.append([fsnTag1, fsnTag2, topIdDict[fsnTag1 + "|" + fsnTag2]])
                    print("add    :", row, str(desc), fsnTag1, fsnTag2)
                    idMatchList.append(row[0])
                if fsnTag1 + "|" + fsnTag2 not in topNumDict: # if not in num, add as num
                    topNumDict[fsnTag1 + "|" + fsnTag2] = desc
                elif desc > topNumDict[fsnTag1 + "|" + fsnTag2]: # if new bigger num, replace number and name
                    topNumDict[fsnTag1 + "|" + fsnTag2] = desc # replace number
                    topIdDict[fsnTag1 + "|" + fsnTag2] = [[item[0], row[0]]] # replace name and id list in dict
                    topIdList = [topIdrow for topIdrow in topIdList if ([topIdrow[0], topIdrow[1]] != [fsnTag1, fsnTag2])]
                    topIdList.append([fsnTag1, fsnTag2, topIdDict[fsnTag1 + "|" + fsnTag2]])
                    idMatchList.append(row[0])
                    print("replace:", row, str(desc), fsnTag1, fsnTag2)
                elif (desc == topNumDict[fsnTag1 + "|" + fsnTag2]) and ([item[0], row[0]] not in topIdDict[fsnTag1 + "|" + fsnTag2]): # if same sized biggest, add to list
                    topIdDict[fsnTag1 + "|" + fsnTag2].append([item[0], row[0]])
                    print("extend :", row, str(desc), fsnTag1, fsnTag2)

                desc2 = descendantCountFromConId_IN(row[0]) # descendant count of child
                if (fsnTag2 not in topNumDict) or (desc2 > topNumDict[fsnTag2]): # add individual tag counts and track occurrences of that number or replace
                    topNumDict[fsnTag2] = desc2
                    topNumCounterDict[fsnTag2] = [1, [row[0]]]
                elif desc2 == topNumDict[fsnTag2] and (row[0] not in topNumCounterDict[fsnTag2][1]): # increment
                    topNumCounterDict[fsnTag2][0] += 1
                    topNumCounterDict[fsnTag2][1].append(row[0])

        if (progress % 500 == 0):
            print(progress, "/", tot)
        progress += 1

    maxIdList = []
    maxIdDict = {}
    childNumDict = {}
    topIdDictKeys = sorted(list(topIdDict.keys()))
    topNumDictKeys = sorted(list([key for key in topNumDict if "|" not in key]))
    childKeys = sorted(list(set([key.split("|")[1] for key in topIdDictKeys])))

    for topNumDictKey in topNumDictKeys: # fish out child concept highest descendant counts
        for childKey in childKeys:
            if childKey == topNumDictKey:
                if childKey not in childNumDict:
                    childNumDict[childKey] = topNumDict[topNumDictKey]
                elif topNumDict[topNumDictKey] > childNumDict[childKey]:
                    childNumDict[childKey] = topNumDict[topNumDictKey]

    print()
    maxIdList.append(tok("ROOT"))
    maxIdDict['SNOMED RT+CTV3'] = tok("ROOT")
    for childKey in childKeys:
        maxId = joinMaxIdToCount(childNumDict[childKey], childKey, idMatchList)
        maxIdList.append(maxId)

    writeOutList(maxIdList, cfgPathInstall() + 'files' + sep() + 'misc' + sep() + 'tagList.txt') # write out maximum 'trimmed best match' concepts to tagList

    cur2.close()
    conn.close()

def buildTagsSimple(RF2Outpath):
    returnList = []
    tagText = ""
    tagTextList = []
    tempDict = {}
    conn = sqlite3.connect(RF2Outpath)
    cur2 = conn.cursor()
    # # all
    # cur2.execute('SELECT SupertypeId FROM TCSubCount order by STCount desc;')
    # no drugs or physical objects
    cur2.execute("SELECT t1.SupertypeId as A, t1.STCount as B FROM TCSubCount t1 \
                EXCEPT \
                SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                WHERE t2.supertypeId IN ('" + tok("PHARM_PROD") + "', '" + tok("PHYS_OBJ") + "') \
                and t2.SubtypeId=t3.supertypeId order by B desc;")
    recordsetIn2 = cur2.fetchall()
    setToRun = [item[0] for item in recordsetIn2[:]]
    tot = len(setToRun)
    cur2.close()
    conn.close()
    counter = 1
    for item in setToRun:
        fsnTag = splitTag(item)
        if fsnTag not in tempDict:
            tempDict[fsnTag] = item
            returnList.append(item)
        tagText += tempDict[fsnTag] + " " + fsnTag + "|"
        counter += 1
        if (counter % 500 == 0):
            print(str(counter), "/", str(tot), "/", str(len(returnList)))

    writeOutList(returnList, cfgPathInstall() + 'files' + sep() + 'misc' + sep() + 'tagList.txt') # write out simple tag concepts to tagList

    tagTextList.append(tagText)
    writeOutList(tagTextList, cfgPathInstall() + 'files' + sep() + 'misc' + sep() + 'tagTextList.txt') # write out all tags to tagTextList (input to wordcloud app)

if __name__ == '__main__':
    snapPath = '/home/ed/Data/RF2Snap.db'
    fillModelLimit(snapPath)
