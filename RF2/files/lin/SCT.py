import sys
import re
sys.path.append('/home/ed/Code/Repos/sct/RF2/')

import SCTRF2Helpers as h
import SCTRF2Funcs as f

def listify(inThing):
    return h.listify(inThing)

def tuplify(inThing):
    return h.tuplify(inThing)

def simplify(inTerm):
    return h.simplify(inTerm)

def AND(Aset, Bset):
    return h.AND(Aset, Bset)

def OR(Aset, Bset):
    return h.OR(Aset, Bset)

def NOT(Aset, Bset):
    return h.NOT(Aset, Bset)

def refsetPterm(idIn):
    return h.refsetPterm(idIn)

def pterm(inId):
    return h.pterm(inId)

def ptermDid(idIn):
    return h.ptermDid(idIn)

def ptermCid(idIn):
    return h.ptermCid(idIn)

def fsnFallback(idIn):
    return h.fsnFallback(idIn)

def parentsFromId_IN(idIn, FromDotCodeBool=False):
    return h.parentsFromId_IN(idIn, FromDotCodeBool=False)

def parentIdsFromId_IN(idIn):
    return h.parentIdsFromId_IN(idIn)

def parentsFromIdPlusMeta_IN(inId, activeStatus):
    return h.parentsFromIdPlusMeta_IN(inId, activeStatus)

def childrenFromIdWithPlus_IN(idIn, limit=False):
    return h.childrenFromIdWithPlus_IN(idIn, limit=False)

def naturalSortTerm(inTerm, toNumberDict, toNumberDictKeys, p=re.compile("\d*\.?\d+")):
    return h.naturalSortTerm(inTerm, toNumberDict, toNumberDictKeys, p=re.compile("\d*\.?\d+"))

def naturalSort(inList, generateTermBool, idPos=0, termPos=0):
    return h.naturalSort(inList, generateTermBool, idPos=0, termPos=0)

def zfill_local(inString):
    return h.zfill_local(inString)

def numberDict():
    return h.numberDict()

def childrenFromId_IN(idIn):
    return h.childrenFromId_IN(idIn)

def childrenIDFromId_IN(idIn):
    return h.childrenIDFromId_IN(idIn)

def childCountFromId_IN(idIn):
    return h.childCountFromId_IN(idIn)

def attUseCount(inAtt):
    return h.attUseCount(inAtt)

def attUseInDomCount(inAtt, inDomList):
    return h.attUseInDomCount(inAtt, inDomList)

def allTermsFromConId_IN(idIn):
    return h.allTermsFromConId_IN(idIn)

def hasDifferentTagTerm(idIn, focusFSN):
    return h.hasDifferentTagTerm(idIn, focusFSN)

def onlyDefTermsFromConId_IN(idIn):
    return h.onlyDefTermsFromConId_IN(idIn)

def inactiveTermsFromConIdPlusMeta_IN(idIn):
    return h.inactiveTermsFromConIdPlusMeta_IN(idIn)

def otherTermsFromConIdPlusMeta_IN(idIn, allRDSTermIds):
    return h.otherTermsFromConIdPlusMeta_IN(idIn, allRDSTermIds)

def allTermsFromConIdPlusMeta_IN(idIn, inVersion):
    return h.allTermsFromConIdPlusMeta_IN(idIn, inVersion)

def ptFromConId_IN(idIn):
    return h.ptFromConId_IN(idIn)

def fsnFromConId_IN(idIn):
    return h.fsnFromConId_IN(idIn)

def synsFromConId_IN(idIn):
    return h.synsFromConId_IN(idIn)

def defnFromConId_IN(idIn):
    return h.defnFromConId_IN(idIn)

def ptFromConIdPlusMeta_IN(idIn, inVersion):
    return h.ptFromConIdPlusMeta_IN(idIn, inVersion)

def fsnFromConIdPlusMeta_IN(idIn, inVersion):
    return h.fsnFromConIdPlusMeta_IN(idIn, inVersion)

def synsFromConIdPlusMeta_IN(idIn, inVersion):
    return h.synsFromConIdPlusMeta_IN(idIn, inVersion)

def defnFromConIdPlusMeta_IN(idIn, inVersion):
    return h.defnFromConIdPlusMeta_IN(idIn, inVersion)

def allRolesFromConId_IN(idIn):
    return h.allRolesFromConId_IN(idIn)

def inactiveIsAsFromConId_IN(idIn, ET, equalitySign):
    return h.inactiveIsAsFromConId_IN(idIn, ET, equalitySign)

def fasterRolesFromConId_IN(idIn, concRoles, concVals, concData, isaTok):
    return h.fasterRolesFromConId_IN(idIn, concRoles, concVals, concData, isaTok)

def groupedRolesFromConId_IN(idIn):
    return h.groupedRolesFromConId_IN(idIn)

def groupedDefiningRolesFromConId_IN(idIn):
    return h.groupedDefiningRolesFromConId_IN(idIn)

def rolesAsTargetFromConId_IN(idIn):
    return h.rolesAsTargetFromConId_IN(idIn)

def groupedRolesFromConIdPlusMeta_IN(idIn, activeFlag):
    return h.groupedRolesFromConIdPlusMeta_IN(idIn, activeFlag)

def childrenFromConIdPlusMeta_IN(idIn, activeFlag, invRolesFlag):
    return h.childrenFromConIdPlusMeta_IN(idIn, activeFlag, invRolesFlag)

def charEquivTester(inTerm):
    return h.charEquivTester(inTerm)

def wordEquivTester(inTerm):
    return h.wordEquivTester(inTerm)

def searchOnText_IN(initialTerm, inOption, inLimit, showDefs):
    return h.searchOnText_IN(initialTerm, inOption, inLimit, showDefs)

def inStart(inString1, inString2):
    return h.inStart(inString1, inString2)

def conMetaFromConId_IN(idIn, ptermBool):
    return h.conMetaFromConId_IN(idIn, ptermBool)

def isActiveFromConId_IN(idIn):
    return h.isActiveFromConId_IN(idIn)

def desMetaFromDesId_IN(idIn):
    return h.desMetaFromDesId_IN(idIn)

def conModCheckFromConId_IN(idIn):
    return h.conModCheckFromConId_IN(idIn)

def conModuleFromConId_IN(idIn):
    return h.conModuleFromConId_IN(idIn)

def conDefStatusFromConId_IN(idIn):
    return h.conDefStatusFromConId_IN(idIn)

def refSetFromConId_IN(idIn, refsetType):
    return h.refSetFromConId_IN(idIn, refsetType)

def checkConIdVsRefset_IN(idIn, refsetId):
    return h.checkConIdVsRefset_IN(idIn, refsetId)

def nonRCIDRefSetFromConId_IN(idIn, refsetType):
    return h.nonRCIDRefSetFromConId_IN(idIn, refsetType)

def getDescriptorField(fullFieldDict, field, refSet):
    return h.getDescriptorField(fullFieldDict, field, refSet)

def refSetFromConIdPlusMeta_IN(idIn, refsetType, inOption):
    return h.refSetFromConIdPlusMeta_IN(idIn, refsetType, inOption)

def refSetMembersFromConIdPlusMeta_IN(idIn, refsetType, myfilter, mycount):
    return h.refSetMembersFromConIdPlusMeta_IN(idIn, refsetType, myfilter, mycount)

def refSetMembersFromConId_IN(idIn, refsetType, myfilter, mycount):
    return h.refSetMembersFromConId_IN(idIn, refsetType, myfilter, mycount)

def maxETForRefset_IN(idIn, refsetType):
    return h.maxETForRefset_IN(idIn, refsetType)

def readFileMembers(inPath):
    return h.readFileMembers(inPath)

def refSetMembersCompareFromConIds_IN(idIn1, idIn2, refsetType, topLevelOrRefsets, chapterDict, refsetDict, debugBool, memberReturn):
    return h.refSetMembersCompareFromConIds_IN(idIn1, idIn2, refsetType, topLevelOrRefsets, chapterDict, refsetDict, debugBool, memberReturn)

def refSetMembersCompareRefTableFromConIds_IN(idIn1, idIn2, topLevelOrRefsets):
    return h.refSetMembersCompareRefTableFromConIds_IN(idIn1, idIn2, topLevelOrRefsets)

def refSetChaptersRefTableFromConIds_IN(refsetDict, idIn2List):
    return h.refSetChaptersRefTableFromConIds_IN(refsetDict, idIn2List)

def siblingRefsets(idIn):
    return h.siblingRefsets(idIn)

def refSetDetailsFromMemIdandSetIdPlusMeta_IN(setId, memberId, inOption):
    return h.refSetDetailsFromMemIdandSetIdPlusMeta_IN(setId, memberId, inOption)

def refSetDetailsFromMemIdandSetId_IN(setId, memberId):
    return h.refSetDetailsFromMemIdandSetId_IN(setId, memberId)

def refSetMemberCountFromConId_IN(idIn, refsetType):
    return h.refSetMemberCountFromConId_IN(idIn, refsetType)

def refSetTypeFromConId_IN(idIn):
    return h.refSetTypeFromConId_IN(idIn)

def findModuleMaxET(moduleList, fileTypeList):
    return h.findModuleMaxET(moduleList, fileTypeList)

def fileAndModuleDistribution_IN(fullModuleList):
    return h.fileAndModuleDistribution_IN(fullModuleList)

def activeColor_IN(inId, focusOrAncBool=False):
    return h.activeColor_IN(inId, focusOrAncBool=False)

def attribColor_IN(inId):
    return h.attribColor_IN(inId)

def activeColorOnTerm_IN(inId, inMarker):
    return h.activeColorOnTerm_IN(inId, inMarker)

def proportionColor_IN(inNo):
    return h.proportionColor_IN(inNo)

def modColor_IN(inId, inTerm, inVersion):
    return h.modColor_IN(inId, inTerm, inVersion)

def metaColor_IN(inId, inVersion, inRefsetRowModuleId='987654321'):
    return h.metaColor_IN(inId, inVersion, inRefsetRowModuleId='987654321')

def latestConcepts_IN(inVersion, inAllBool):
    return h.latestConcepts_IN(inVersion, inAllBool)

def refSetDescriptorFromConId_IN(idIn):
    return h.refSetDescriptorFromConId_IN(idIn)

def refSetFieldsFromConId_IN(idIn):
    return h.refSetFieldsFromConId_IN(idIn)

def descendantsFromConId_IN(idIn, FromDotCodeBool=False):
    return h.descendantsFromConId_IN(idIn, FromDotCodeBool=False)

def descendantsIDFromConId_IN(idIn):
    return h.descendantsIDFromConId_IN(idIn)

def descendantsCleanIDFromConId_IN(idIn):
    return h.descendantsCleanIDFromConId_IN(idIn)

def descendantCountFromConId_IN(idIn):
    return h.descendantCountFromConId_IN(idIn)

def ancestorsFromConId_IN(idIn, FromDotCodeBool=False):
    return h.ancestorsFromConId_IN(idIn, FromDotCodeBool=False)

def ancestorsIDFromConId_IN(idIn):
    return h.ancestorsIDFromConId_IN(idIn)

def ancestorsIDFromConIdList_IN(idInList):
    return h.ancestorsIDFromConIdList_IN(idInList)

def TCCompare_IN(subId, supId):
    return h.TCCompare_IN(subId, supId)

def PPSFromAncestors_IN(ancestorList):
    return h.PPSFromAncestors_IN(ancestorList)

def primAncestors_IN(ancestorList):
    return h.primAncestors_IN(ancestorList)

def primAncestorsFromConId_IN(idIn):
    return h.primAncestorsFromConId_IN(idIn)

def ancestorCountFromConId_IN(idIn):
    return h.ancestorCountFromConId_IN(idIn)

def historicalOriginsFromConId_IN(inId, inOption):
    return h.historicalOriginsFromConId_IN(inId, inOption)

def historicalDestinationsFromConId_IN(inId, inOption, inLookup, movedFromBool=True, refersToRowsBool=True):
    return h.historicalDestinationsFromConId_IN(inId, inOption, inLookup, movedFromBool=True, refersToRowsBool=True)

def historicalDestinationsCountFromConId_IN(inOption):
    return h.historicalDestinationsCountFromConId_IN(inOption)

def refersToRowsFromConId_IN(inId, inOption):
    return h.refersToRowsFromConId_IN(inId, inOption)

def CiDFromDiD_IN(idIn):
    return h.CiDFromDiD_IN(idIn)

def dTypeTermFromId(inId):
    return h.dTypeTermFromId(inId)

def equivTester(inList, equivList):
    return h.equivTester(inList, equivList)

def equivTerms(inEquivs, inId):
    return h.equivTerms(inEquivs, inId)

def readInWordEquivs():
    return h.readInWordEquivs()

def readInCharEquivs(langCode):
    return h.readInCharEquivs(langCode)

def multi_find(string, value, start=0, stop=None):
    return h.multi_find(string, value, start=0, stop=None)

def get_power_set(s):
    return h.get_power_set(s)

def prelim(seedModules):
    return h.prelim(seedModules)

def latestET(seedModules):
    return h.latestET(seedModules)

def deepestRels_IN(limitInt):
    return h.deepestRels_IN(limitInt)

def busyCrossMaps_IN(limitInt):
    return h.busyCrossMaps_IN(limitInt)

def reasonList():
    return h.reasonList()

def inactivationByReason_IN(limitInt):
    return h.inactivationByReason_IN(limitInt)

def highDefRels_IN(limitInt):
    return h.highDefRels_IN(limitInt)

def randomConcepts_IN(limitInt):
    return h.randomConcepts_IN(limitInt)

def highAncesCount_IN(limitInt):
    return h.highAncesCount_IN(limitInt)

def highDescCount_IN(limitInt):
    return h.highDescCount_IN(limitInt)

def highParentCount_IN(limitInt):
    return h.highParentCount_IN(limitInt)

def highChildCount_IN(limitInt):
    return h.highChildCount_IN(limitInt)

def limitAttributes(returnIncluded):
    return h.limitAttributes(returnIncluded)

def allAttributes():
    return h.allAttributes()

def tabCfg():
    return h.tabCfg()

def tabString(inString):
    return h.tabString(inString)

def tabStringTwoId(inString):
    return h.tabStringTwoId(inString)

def checkAllAtts():
    return h.checkAllAtts()

def readAtts():
    return h.readAtts()

def anyAttExclusions(attLimitList):
    return h.anyAttExclusions(attLimitList)

def setAtts(testId, incExclString, operatorString):
    return h.setAtts(testId, incExclString, operatorString)

def concLookUp_IN():
    return h.concLookUp_IN()

def pathCalcFromId_IN(inId):
    return h.pathCalcFromId_IN(inId)

def pathRowsFromPath(inPath):
    return h.pathRowsFromPath(inPath)

def OWLPattern(inPattern):
    return h.OWLPattern(inPattern)

def singleOWLAxioms():
    return h.singleOWLAxioms()

def multipleOWLAxioms():
    return h.multipleOWLAxioms()

def interestingOWL_IN():
    return h.interestingOWL_IN()

def hasDef_IN(idIn):
    return h.hasDef_IN(idIn)

def testDefs_IN(runNum, withDef):
    return h.testDefs_IN(runNum, withDef)

def readFocusValue():
    return f.readFocusValue()

def getIdsFromLines(inLines, returnFalseBool):
    return f.getIdsFromLines(inLines, returnFalseBool)

def append_distinct(inList, inItem):
    return f.append_distinct(inList, inItem)

def validateVerhoeff(number):
    return f.validateVerhoeff(number)
