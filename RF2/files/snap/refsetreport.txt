Members of 999002771000000107 Dementia diagnosis simple reference set:

Referenced component	26929004	Alzheimer's disease
Referenced component	836301008	Amnestic mild cognitive disorder
Referenced component	191464005	Arteriosclerotic dementia with delirium
Referenced component	191466007	Arteriosclerotic dementia with depression
Referenced component	191465006	Arteriosclerotic dementia with paranoia
Referenced component	279982005	Cerebral degeneration presenting primarily with dementia
Referenced component	191475009	Chronic alcoholic brain syndrome
Referenced component	840452004	Classical sporadic Creutzfeldt-Jakob disease
Referenced component	792004	Creutzfeldt-Jakob disease
Referenced component	52448006	Dementia
Referenced component	281004	Dementia associated with alcoholism
Referenced component	191519005	Dementia associated with another disease
Referenced component	425390006	Dementia associated with Parkinson's Disease
Referenced component	713844000	Dementia co-occurrent with human immunodeficiency virus infection
Referenced component	429458009	Dementia due to Creutzfeldt Jakob disease
Referenced component	442344002	Dementia due to Huntington chorea
Referenced component	21921000119103	Dementia due to Pick's disease
Referenced component	278857002	Dementia of frontal lobe type
Referenced component	80098002	Diffuse Lewy body disease
Referenced component	42769004	Diffuse Lewy body disease with spongiform cortical change
Referenced component	191493005	Drug-induced dementia
Referenced component	230265002	Familial Alzheimer's disease of early onset
Referenced component	230267005	Familial Alzheimer's disease of late onset
Referenced component	230269008	Focal Alzheimer's disease
Referenced component	230273006	Frontotemporal degeneration
Referenced component	230270009	Frontotemporal dementia
Referenced component	32875003	Inhalant-induced persisting dementia
Referenced component	86188000	Kuru
Referenced component	135811000119107	Lewy body dementia with behavioural disturbance
Referenced component	386805003	Mild cognitive disorder
Referenced component	230287006	Mixed cortical and subcortical vascular dementia
Referenced component	79341000119107	Mixed dementia
Referenced component	56267009	Multi-infarct dementia
Referenced component	230266001	Non-familial Alzheimer's disease of early onset
Referenced component	230268000	Non-familial Alzheimer's disease of late onset
Referenced component	231438001	Presbyophrenic psychosis
Referenced component	12348006	Presenile dementia
Referenced component	421023003	Presenile dementia associated with AIDS
Referenced component	713488003	Presenile dementia co-occurrent with human immunodeficiency virus infection
Referenced component	191452002	Presenile dementia with delirium
Referenced component	31081000119101	Presenile dementia with delusions
Referenced component	191455000	Presenile dementia with depression
Referenced component	191454001	Presenile dementia with paranoia
Referenced component	1089501000000102	Presenile dementia with psychosis
Referenced component	22381000119105	Primary degenerative dementia
Referenced component	416780008	Primary degenerative dementia of the Alzheimer type, presenile onset
Referenced component	6475002	Primary degenerative dementia of the Alzheimer type, presenile onset, uncomplicated
Referenced component	65096006	Primary degenerative dementia of the Alzheimer type, presenile onset, with delirium
Referenced component	10532003	Primary degenerative dementia of the Alzheimer type, presenile onset, with depression
Referenced component	416975007	Primary degenerative dementia of the Alzheimer type, senile onset
Referenced component	66108005	Primary degenerative dementia of the Alzheimer type, senile onset, uncomplicated
Referenced component	230280008	Progressive aphasia in Alzheimer's disease
Referenced component	111480006	Psychoactive substance-induced organic dementia
Referenced component	59651006	Sedative, hypnotic AND/OR anxiolytic-induced persisting dementia
Referenced component	230288001	Semantic dementia
Referenced component	268612007	Senile and presenile organic psychotic conditions
Referenced component	45864009	Senile degeneration of brain
Referenced component	15662003	Senile dementia
Referenced component	312991009	Senile dementia of the Lewy body type
Referenced component	191461002	Senile dementia with delirium
Referenced component	191459006	Senile dementia with depression
Referenced component	191457008	Senile dementia with depressive or paranoid features
Referenced component	191458003	Senile dementia with paranoia
Referenced component	713060000	Sporadic Creutzfeldt-Jakob disease
Referenced component	90099008	Subcortical leucoencephalopathy
Referenced component	230286002	Subcortical vascular dementia
Referenced component	191463004	Uncomplicated arteriosclerotic dementia
Referenced component	191451009	Uncomplicated presenile dementia
Referenced component	191449005	Uncomplicated senile dementia
Referenced component	429998004	Vascular dementia
Referenced component	230285003	Vascular dementia of acute onset
