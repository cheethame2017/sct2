<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 118598001 | Property |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 263714004 | Colours |}
<< 363787002 | Observable entity |:$$$$${370132008 | Scale type | = << 117362005 | Nominal value |}
<< 363787002 | Observable entity |:$$$$${370130000 | Property | = << 118598001 | Property |}
<< 363787002 | Observable entity |:$$$$${370130000 | Property | = << 30001000004102 | Histologic feature (property) |}
<< 871509002 | Allergy to dextropropoxyphene and/or paracetamol |:$$$$${719722006 | Has realization | = << 472964009 | Allergic process |,$$$$$246075003 | Causative agent | = << 387517004 | Paracetamol |}
^ SET_S | Local set S | AND$$$$$<< 609328004 | Allergic disposition |
<< 870727009 | Allergy to folic acid and/or iron compound |:$$$$${246075003 | Causative agent | = << 105590001 | Substance |,$$$$$719722006 | Has realization | = << 472964009 | Allergic process |}
<< 84114007 | Heart failure |:$$$$${363698007 | Finding site | = << 80891009 | Heart structure |}
<< 84114007 | Heart failure |:$$$$${363698007 | Finding site | = << 21814001 | Cardiac ventricular structure |}
^ SET_S | Local set S | AND$$$$$<< 64572001 | Disease |
^ SET_S | Local set S | AND$$$$$<< 84114007 | Heart failure |
<< 84114007 | Heart failure |:$$$$${363698007 | Finding site | = << 17401000 | Cardiac valve structure |}
^ SET_S | Local set S |:$$$$${363698007 | Finding site | = << 21814001 | Cardiac ventricular structure |}
<< 84114007 | Heart failure |:$$$$${363698007 | Finding site | = << 53085002 | Right cardiac ventricular structure |},$$$$${363698007 | Finding site | = << 87878005 | Left cardiac ventricular structure |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 897682000 | Structure of artery within stomach |}
^ SET_S | Local set S | AND$$$$$<< 71388002 | Procedure |
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 360215002 | Pharmaceutical excipient |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 363704007 | Procedure site | = << 272625005 | Entire body organ |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 370131001 | Recipient category | = << 138875005 | SNOMED CT Concept |}
<< 64572001 | Disease |:$$$$${116676008 | Associated morphology | = << 49755003 | Morphologically abnormal structure |,$$$$$363698007 | Finding site | = << 113262008 | Thoracic aorta structure |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | =  23685000 | Rheumatic heart disease |}
<< 118698009 | Procedure on abdomen |:$$$$${260686004 | Method | = << 129357001 | Closure - action |,$$$$$405813007 | Procedure site - Direct | = << 1010585000 | Structure of abdominopelvic wall |}
<< 118698009 | Procedure on abdomen |:$$$$${260686004 | Method | = << 129357001 | Closure - action |,$$$$$405813007 | Procedure site - Direct | = << 818983003 | Abdomen |}
<< 363787002 | Observable entity |
<< 414237002 | Feature of entity |
<< 278844005 | General clinical state |
<< 160476009 | Social / personal history observable |
<< 260787004 | Physical object |
<< 78621006 | Physical force |
<< 419891008 | Record artefact |
<< 123038009 | Specimen |
<< 246464006 | Function |
<< 363788007 | Clinical history/examination observable |
<< 138875005 | SNOMED CT Concept |:$$$$$<< 116680003 | Is a | = << 138875005 | SNOMED CT Concept |,$$$$$<< 116680003 | Is a | = << 138875005 | SNOMED CT Concept |
<< 766918008 | Microbial agent-containing product |
<< 732935002 | Unit of presentation (unit of presentation) |
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 58405006 | Keloid |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 68431000052104 | Enzyme-linked immunosorbent assay technique |}
^ SET_A | Local set A | AND$$$$$^ SET_B | Local set B | AND$$$$$^ SET_C | Local set C |
<< 195967001 | Asthma (disorder) |:$$$$${363698007 | Finding site | = << 89187006 | Airway structure |}
^ SET_M | Local set M | AND$$$$$^ SET_N | Local set N | AND$$$$$^ SET_O | Local set O |
<< 4421005 | Cell structure (cell structure) | AND$$$$$<< 118956008 | Morphologically altered structure |
^ SET_M | Local set M |
<< 118698009 | Procedure on abdomen |:$$$$${363704007 | Procedure site | = << 113276009 | Intestinal structure |}
<< 118831003 | Procedure on intestine |
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 371246006 | Green colour |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 719722006 | Has realization | = << 138875005 | SNOMED CT Concept |}
<< 64572001 | Disease |:$$$$${116676008 | Associated morphology | = << 86049000 | Neoplasm, malignant (primary) |}
<< 64572001 | Disease |:$$$$${116676008 | Associated morphology | = << 14799000 | Neoplasm, metastatic |},$$$$${116676008 | Associated morphology | = << 86049000 | Neoplasm, malignant (primary) |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 7010000 | Carcinomatosis |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 51576004 | Structure of multiple topographic sites |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | =  51576004 | Structure of multiple topographic sites |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | =  363346000 | Malignant neoplastic disease |}
<< 363787002 | Observable entity |:$$$$${<< 762705008 | Concept model object attribute | = << 367651003 | Malignant Neoplasm (Morphology) |}
<< 363787002 | Observable entity |:$$$$${704321009 | Characterizes | = << 1505281000004101 | Direct local invasion |}
<< 363787002 | Observable entity |:$$$$${704319004 | Inheres in | = << 108369006 | Neoplasm |}
^ SET_N | Local set N | AND$$$$$<< 395531003 | Neoplasm observable |
<< 395531003 | Neoplasm observable |
<< 363787002 | Observable entity |:$$$$${<< 762705008 | Concept model object attribute | = << 108369006 | Neoplasm |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 108369006 | Neoplasm |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | =  108369006 | Neoplasm |}
<< 363787002 | Observable entity |:$$$$${<< 762705008 | Concept model object attribute | =  108369006 | Neoplasm |}
<< 363787002 | Observable entity |:$$$$${<< 762705008 | Concept model object attribute | = < 108369006 | Neoplasm |}
<< 442083009 | Anatomical or acquired body structure | AND$$$$$<< 118956008 | Morphologically altered structure |
<< 138875005 | SNOMED CT Concept |:$$$$${363698007 | Finding site | = << 118956008 | Morphologically altered structure |}
<< 138875005 | SNOMED CT Concept |:$$$$${116676008 | Associated morphology | = ^ SET_R | Local set R |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | =  275322007 | Scar |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | =  12402003 | Scar |}
<< 404684003 | Clinical finding |:$$$$${<< 762705008 | Concept model object attribute | =  12402003 | Scar |}
<< 404684003 | Clinical finding |:$$$$${<< 762705008 | Concept model object attribute | =  272726003 | Gastrostomy |}
<< 404684003 | Clinical finding |:$$$$${<< 762705008 | Concept model object attribute | =  245849007 | Post-surgical anatomy |}
<< 404684003 | Clinical finding |:$$$$${<< 762705008 | Concept model object attribute | = << 245849007 | Post-surgical anatomy |}
<< 404684003 | Clinical finding |:$$$$${<< 363698007 | Finding site | = << 245849007 | Post-surgical anatomy |}
<< 404684003 | Clinical finding |:$$$$${<< 116676008 | Associated morphology | = << 245849007 | Post-surgical anatomy |}
<< 442083009 | Anatomical or acquired body structure | AND$$$$$<< 4421005 | Cell structure |
<< 442083009 | Anatomical or acquired body structure | AND$$$$$<< 4421005 | Cell structure | AND$$$$$<< 118956008 | Morphologically altered structure |
<< 280115004 | Acquired body structure | AND$$$$$<< 91723000 | Anatomical structure |
<< 91723000 | Anatomical structure |
<< 118956008 | Morphologically altered structure |
<< 4421005 | Cell structure |
<< 362837007 | Entire cell |
<< 123037004 | Body structure |
^ 991411000000109 | Emergency care diagnosis simple reference set |
<< 235856003 | Disease of liver | AND$$$$$<< 81308009 | Disorder of brain | AND$$$$$<< 90708001 | Kidney disease |
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 1990001000004102 | Near complete |}
<< 138875005 | SNOMED CT Concept |:$$$$${<< 762705008 | Concept model object attribute | = << 758637006 | Anatomic location (property) |}
<< 413815006 | Chest imaging | AND$$$$$<< 441987005 | Imaging of abdomen | AND$$$$$<< 363141001 | Imaging of neck | AND$$$$$<< 715959009 | Imaging of pelvis |
<< 413815006 | Chest imaging | AND$$$$$<< 441987005 | Imaging of abdomen | AND$$$$$<< 715959009 | Imaging of pelvis |
<< 3855007 | Disorder of pancreas | AND$$$$$<< 991411000000109 | Emergency care diagnosis simple reference set | AND$$$$$<< 999001741000000107 | Gastroenterology outpatient diagnosis simple reference set |
<< 3855007 | Disorder of pancreas | AND$$$$$^ 991411000000109 | Emergency care diagnosis simple reference set | AND$$$$$^ 999001741000000107 | Gastroenterology outpatient diagnosis simple reference set |
<< 3855007 | Disorder of pancreas | AND$$$$$^ 991411000000109 | Emergency care diagnosis simple reference set |
<< 52182008 | Disorder of duodenum | AND$$$$$<< 3855007 | Disorder of pancreas |
<< 71388002 | Procedure |:$$$$${260686004 | Method | = << 360037004 | Imaging - action |,$$$$$405813007 | Procedure site - Direct | = << 816094009 | Cross-sectional thorax |}
<< 441987005 | Imaging of abdomen |:$$$$${260686004 | Method | = << 360037004 | Imaging - action |,$$$$$405813007 | Procedure site - Direct | = << 818982008 | Abdominopelvic cross-sectional segment |}
<< 71388002 | Procedure |:$$$$${260686004 | Method | = << 360037004 | Imaging - action |,$$$$$405813007 | Procedure site - Direct | = << 69536005 | Head structure |}
<< 71388002 | Procedure |:$$$$${405813007 | Procedure site - Direct | = << 45048000 | Neck structure |,$$$$$260686004 | Method | = << 360037004 | Imaging - action |}
<< 71388002 | Procedure |:$$$$${260686004 | Method | = << 360037004 | Imaging - action |,$$$$$405813007 | Procedure site - Direct | = << 816092008 | Cross-sectional pelvis |}
^ SET_K | Local set K |
<< 235856003 | Disease of liver | AND$$$$$<< 76069003 | Disorder of bone |
