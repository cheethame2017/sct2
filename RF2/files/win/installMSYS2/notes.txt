https://github.com/jrfonseca/xdot.py

pacman -Ss pip
An easy_install replacement for installing pypi python packages (mingw-w64)
mingw64/mingw-w64-x86_64-python-pip

pacman -Ss graphviz
Graph Visualization Software (mingw-w64)
mingw64/mingw-w64-x86_64-graphviz

pacman -S mingw64/mingw-w64-x86_64-python-pip mingw64/mingw-w64-x86_64-graphviz

pip install xdot

C:\msys64\mingw64.exe python3 -m xdot C:\Users\Ed\Documents\repo\sct\RF2\files\conpic.dot
C:\msys64\mingw64.exe python3 -m xdot C:\Users\Ed\Documents\repo\sct\RF2\files\contc.dot