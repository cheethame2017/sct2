1153430004	2	4564290013	Partial deletion of chromosome 14q	chromosome 14
37506004	2	2912992019	Chromosome 4q deletion syndrome	Chromosome 4
449819002	2	2912647011	Chromosome 3p deletion syndrome	Chromosome 3
719909009	0	4570721014	Chromosome Xq28 trisomy (disorder)	Chromosome X
719909009	2	4570722019	Chromosome Xq28 trisomy	Chromosome X
699254009	0	2983673017	Microdeletion of chromosome 15q13.3 (disorder)	chromosome 15
699254009	2	3015079019	Microdeletion of chromosome 15q13.3	chromosome 15
699305004	2	2983579010	Chromosome 1q21.1 deletion syndrome	Chromosome 1
699305004	2	3015049014	Microdeletion of chromosome 1q21.1	chromosome 1
699306003	0	2983672010	Chromosome 1p36 deletion syndrome (disorder)	Chromosome 1
699306003	2	2983669015	Chromosome 1p36 deletion syndrome	Chromosome 1
699307007	0	2983684012	Chromosome 16p11.2 deletion syndrome (disorder)	Chromosome 16
699307007	1	2983646018	Chromosome 16p11.2 deletion syndrome	Chromosome 16
699308002	0	2983696017	Microdeletion of chromosome 15q24 (disorder)	chromosome 15
699308002	2	2983648017	Interstitial deletion of chromosome 15q24	chromosome 15
699308002	2	3023268018	Microdeletion of chromosome 15q24	chromosome 15
699311001	2	2983751019	Chromosome 22q11.2 microduplication syndrome	Chromosome 22
699311001	2	2983761014	Chromosome 22q11.2 duplication syndrome	Chromosome 22
702346005	0	2995937010	Chromosome 11p11.2 deletion syndrome (disorder)	Chromosome 11
702346005	2	2995170018	Chromosome 11p11.2 deletion syndrome	Chromosome 11
702357000	0	2995538014	Chromosome 2q37 deletion syndrome (disorder)	Chromosome 2
702357000	1	2995103015	Chromosome 2q37 deletion syndrome	Chromosome 2
715215007	0	3301886014	Chromosome 11p13 deletion syndrome (disorder)	Chromosome 11
715215007	2	3301887017	Chromosome 11p13 deletion syndrome	Chromosome 11
717973004	0	3310742010	Chromosome 3q29 duplication syndrome (disorder)	Chromosome 3
717973004	2	3310745012	Chromosome 3q29 duplication syndrome	Chromosome 3
718615003	3	3312953018	Encompasses heterozygous overlapping microdeletions on chromosome 8q21.11 resulting in intellectual disability, facial dysmorphism comprising a round face, ptosis, short philtrum, Cupid's bow and prominent low-set ears, nasal speech and mild finger and toe anomalies. The prevalence is unknown but 8q21.11 microdeletion syndrome is rare. Microdeletions appear de novo or are inherited from affected parents in an autosomal dominant manner.	chromosome 8
718881004	0	3314293013	Chromosome Xq27.3q28 duplication syndrome (disorder)	Chromosome X
718881004	1	3314294019	Chromosome Xq27.3q28 duplication syndrome	Chromosome X
719808002	0	3317982011	Chromosome Xp11.3 microdeletion syndrome (disorder)	Chromosome X
719808002	1	3317983018	Chromosome Xp11.3 microdeletion syndrome	Chromosome X
720749004	3	3321968018	A degenerative corneal disorder characterised by the association of congenital hereditary endothelial dystrophy with progressive postlingual sensorineural hearing loss. The ocular manifestations include diffuse bilateral corneal oedema occurring with severe corneal clouding, blurred vision, visual loss and nystagmus. Caused by mutations in the SLC4A11 gene located at the CHED2 locus on chromosome 20p13p12.	chromosome 20
724282009	3	3433591014	An inherited condition consisting of hypoparathyroidism, sensorineural deafness and renal disease. The exact prevalence is unknown, but the disease is considered to be very rare. Patients may present at any age with hypocalcaemia, tetany, or afebrile convulsions. Hearing loss is usually bilateral and may range from mild to profound impairment. Renal disease manifestations include nephrotic syndrome, cystic kidney, renal dysplasia, hypoplasia or aplasia, pelvicalyceal deformity, vesicoureteral reflux, chronic renal failure, haematuria, proteinuria and renal scarring. The defect in the majority of cases was mapped to chromosome 10p (10pter-p13 region or 10p14-p15.1). Haploinsufficiency (deletions) of zinc-finger transcription factor GATA3, or mutations in the GATA3 gene appear to be the underlying cause of this syndrome.	chromosome 10
726733007	0	3452288011	Chromosome Xp22.3 microdeletion syndrome (disorder)	Chromosome X
726733007	1	3452289015	Chromosome Xp22.3 microdeletion syndrome	Chromosome X
764435003	2	3649826017	Chromosome 17q12 duplication syndrome	Chromosome 17
764440006	2	3649844013	Chromosome 19p13.13 deletion syndrome	Chromosome 19
764739008	0	3654946017	Proximal chromosome 18q deletion syndrome (disorder)	chromosome 18
764739008	2	3654947014	Proximal chromosome 18q deletion syndrome	chromosome 18
765171002	0	3657321015	Distal chromosome 18q deletion syndrome (disorder)	chromosome 18
765171002	1	3657320019	Distal chromosome 18q deletion syndrome	chromosome 18
789187001	2	3787696019	Chromosome Xq26 microduplication syndrome	Chromosome X
880093002	2	4008406018	Chromosome 17q11.2 deletion syndrome	Chromosome 17
