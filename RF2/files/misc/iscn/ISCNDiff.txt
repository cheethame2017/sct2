702417004	0	2995820019	Supernumerary der(22)t(11;22) syndrome (disorder)	der(22	t(11
702417004	2	2995222019	Supernumerary der(22)t(11;22) syndrome	der(22	t(11
702417004	2	2995830011	Supernumerary der(22) syndrome	der(22
715983001	3	3304319017	A rare chromosomal anomaly from a variable part of chromosome 8. The phenotype of mosaic or non-mosaic supernumerary r(8)/mar(8) ranges from almost normal to variable degrees of minor abnormalities, and growth and mental retardation overlapping with the well-known mosaic trisomy 8 syndrome.	r(8
