import sqlite3
import re
import io
from SCTRF2Helpers import allTermsFromConId_IN, pterm
from SCTRF2Funcs import readFocusValue
from SCTRF2config import tok, cfg_MRCM as cfg_local
import nltk
from nltk.corpus import stopwords

porter = nltk.PorterStemmer()
stopwords = nltk.corpus.stopwords.words('english')

# working paths
homePath = '/home/ed/'
RF2Outpath = homePath + 'Data/RF2Out.db'
repoPath = homePath + 'Code/Repos/sct/RF2/'
miscPath = repoPath + 'files/misc/iscn/'
dotPath = repoPath + 'files/dot/'
ISCNPathLoose = miscPath + 'ISCNLoose.txt'
ISCNPathTight = miscPath + 'ISCNTight.txt'
ISCNPathTightId = miscPath + 'ISCNTightId.txt'
ISCNPathDiff = miscPath + 'ISCNDiff.txt'
ISCNPathISCN = miscPath + 'ISCN.txt'
ISCNPathRemains = miscPath + 'ISCNRemains.txt'
ChrPathLoose = miscPath + 'ChrLoose.txt'
ChrPathTight = miscPath + 'ChrTight.txt'
ChrPathTightId = miscPath + 'ChrTightId.txt'
ChrPathDiff = miscPath + 'ChrDiff.txt'
ChrPathRemains = miscPath + 'ChrRemains.txt'
MatchListTightPath = miscPath + 'MatchListTight.txt'
MatchListLoosePath = miscPath + 'MatchListLoose.txt'
MatchListAllPath = miscPath + 'MatchListAll.txt'
MatchListIdPath = miscPath + 'MatchListId.txt'
MatchListDotPath = dotPath + 'iscn_auto.dot'
classListPath = miscPath + 'classList.txt'
singletonListPath = miscPath + 'singletonList.txt'

# filewriter
def writeOutISCNResults(outList, ISCNPath, splitBool=True):
    mode = 'w'
    try:
        f = io.open(ISCNPath, mode, encoding="UTF-8")
        for item in outList:
            if splitBool:
                item = [str(thing) if isinstance(thing, int) else thing for thing in item]
                f.write("\t".join(item) + "\n")
            else:
                f.write(item + "\n")
    finally:
        f.close()

# late tidy A
def stringCleanA(inString):
    outString = inString.replace("chromosome 4", "chromosome #")
    outString = outString.replace("chromosome chromosome #", "chromosome #")
    outString = outString.replace("monosomy monosomy #", "monosomy #")
    outString = outString.replace("trisomy trisomy #", "trisomy #")
    outString = outString.replace("monosomy # monosomy", "monosomy #")
    outString = outString.replace("trisomy # trisomy", "trisomy #")
    outString = outString.replace("the (the)", "(the)").replace("#ter", "#")
    return outString

# late tidy B
def stringCleanB(inString):
    outString = inString.replace("p#ds - ", "").replace("# (the) ", "# ")
    return outString

# late tidy C - only applied to singletonList
def stringCleanC(inString):
    # long short arm detection (special case of chromosome matches)
    if ("long arm" in inString):
        outString = inString.replace("long", "(the) short|long").replace("#", "chromosome #").lower()
    elif ("short arm" in inString):
        outString = inString.replace("short", "(the) short|long").replace("#", "chromosome #").lower()
    else:
        outString = inString
    return outString

# rudimentary modification method for singletonList - not as 'honest' as the match-generated list (results in forms that may not be in the data)
def modifySingletonList(singletonList):
    # for each 'of #' add 'of chromosome #' variant (& reverse)
    for item in singletonList:
        if 'of #' in item:
            singletonList.append(item.replace('of #', 'of chromosome #'))
    for item in singletonList:
        if 'of chromosome #' in item:
            tempString = item.replace('of chromosome #', 'of #')
            if tempString not in singletonList:
                singletonList.append(tempString)
    for item in singletonList:
        if 'chromosome #' in item:
            tempString = item.replace('chromosome #', '#')
            if tempString not in singletonList:
                singletonList.append(tempString)
    singletonList = sorted(singletonList)
    return singletonList

# early removal of high likelihood false positives
def checkRedFlagWords(inTerm, inType):
    if inType == "iscn":
        # if not(" due " in inTerm) and not(" with " in inTerm) and not("omplement " in inTerm):
        if not(" due " in inTerm) and not("omplement " in inTerm):
            return True
        else:
            return False
    elif inType == "chr":
        if not(" due " in inTerm) and not(" with " in inTerm) and not(" in " in inTerm) and not(" linked to " in inTerm):
            return True
        else:
            return False

def tidyTempRemains(tempRemains, st):
    if ("chromosome " in tempRemains.lower()) and ("chromosome " in st.lower()):
        return tempRemains.replace(st, "chromosome #").strip()
    elif ("monosomy " in tempRemains.lower()) and ("monosomy " in st.lower()):
        return tempRemains.replace(st, "monosomy #").strip()
    elif ("trisomy " in tempRemains.lower()) and ("trisomy " in st.lower()):
        return tempRemains.replace(st, "trisomy #").strip()
    else:
        return tempRemains.replace(st, "#").strip()

# main method
def detectISCN(showSingletons, showSingletonSimilars):
    # runNum and runType set in MRCM browser
    # number of set to sample (0 is all) [build 0] change=0
    runNum = cfg_local("TAG_RUN_NUM")
    # hacky reuse of MRCM/tag code...
    # run type: 1=all, 2=no drugs/phys obj, 3=drugs phys obj only [build 2]
    runType = cfg_local("TAG_RUN_TYPE")
    conn = sqlite3.connect(RF2Outpath)
    cur2 = conn.cursor()
    iscnListLoose = []
    iscnListTight = []
    iscnListDiff = []
    iscnRemainsList = []
    chrListLoose = []
    chrListTight = []
    chrListDiff = []
    chrRemainsList = []
    iscnList = []
    # all
    if runType == 1:
        cur2.execute('SELECT SupertypeId FROM TCSubCount order by STCount desc;')
    # no drugs or physical objects
    elif runType == 2:
        cur2.execute("SELECT t1.SupertypeId as A, t1.STCount as B FROM TCSubCount t1 \
                    EXCEPT \
                    SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                    WHERE t2.supertypeId IN ('" + tok("PHARM_PROD") + "', '" + tok("PHYS_OBJ") + "') \
                    and t2.SubtypeId=t3.supertypeId order by B;")
    # no drugs
    elif runType == 3:
        cur2.execute("SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                    WHERE t2.supertypeId IN ('" + readFocusValue()[0] + "') \
                    and t2.SubtypeId=t3.supertypeId order by B desc;")
    # only drugs and physical objects
    elif runType == 4:
        cur2.execute("SELECT t2.SubtypeId as A, t3.STCount as B FROM tc t2, TCSubCount t3 \
                    WHERE t2.supertypeId IN ('" + "66091009" + "') \
                    and t2.SubtypeId=t3.supertypeId order by B desc;")
                    # 66091009 - congenital disease
                    # 726733007
                    # 64572001 - disease
                    # 129154003 - haem neopl.
    # could use active concepts, however this derivative can easily be ordered by number counts.
    # Concepts alternative: cur2.execute('SELECT Concepts.id FROMs Concepts WHERE Concepts.active=1 order by Concepts.id;') in theory, but (as with 'order by STCount asc' variant above, will
    # likely run into key errors for several lookups.
    recordsetIn2 = cur2.fetchall()
    if runNum == 0:
        setToRun = [[item[0]] for item in recordsetIn2[:]]
    else:
        setToRun = [[item[0]] for item in recordsetIn2[:runNum]]

    print(str(len(setToRun)))
    for item in setToRun:
        terms = allTermsFromConId_IN(item[0])
        counter = 0
        for dtype in terms:
            for term in dtype:
                g = re.findall(r"\b((t|r|inv|del|der|dic|dup|fra|ins|inv|tri|trp)\([0-9]{1,2}(;[0-9]{1,2}){0,1}\)\([pq][0-9]{1,2}(\.[0-9]{1,2}){0,1}(;){0,1}[pq][0-9]{1,2}(\.[0-9]{1,2}){0,1}\)|([0-9]{1,2}|[XY])[pq][0-9]{0,2}(\.){0,1}(?:[0-9]){0,2}((\-){0,1}[pq](ter|(?:[0-9]){0,2}(\.){0,1}([0-9]{0,2}))){0,1})", term[1])
                if g:
                    # Successful match
                    print(item[0], str(counter), term)
                    glist = [item[0] for item in g]
                    if (counter < 3):
                        for match in glist:
                            if ([match, item[0], pterm(item[0]), ""] not in iscnList):
                                iscnList.append([match, item[0], pterm(item[0]), ""])
                    else:
                        for match in glist:
                            if match[-1] == ".":
                                match = match[:-1]
                            if ([match, item[0], pterm(item[0]), "D"] not in iscnList):
                                iscnList.append([match, item[0], pterm(item[0]), "D"])
                    if checkRedFlagWords(term[1], "iscn") or counter == 3:
                        iscnListTight.append([*[item[0], counter, term[0], term[1]], *glist])

                        if counter in (1, 2):
                            tempRemains = term[1]
                            for st in glist:
                                tempRemains = tidyTempRemains(tempRemains, st)
                            if tempRemains not in iscnRemainsList:
                                iscnRemainsList.append(tempRemains)
            for term in dtype:
                g = re.findall(r"\b((t|r|inv|del|der|dic|dup|fra|ins|inv|tri|trp)\([0-9]{1,2}|([0-9]{1,2}|[XY])[pq])", term[1])
                if g:
                    # Successful match
                    print(item[0], str(counter), term)
                    glist = [item[0] for item in g]
                    if checkRedFlagWords(term[1], "iscn") or counter == 3:
                        iscnListLoose.append([*[item[0], counter, term[0], term[1]], *glist])
            for term in dtype:
                g = re.findall(r"\b(((C|c)hromosome |(T|t)risomy |(M|m)onosomy )([0-9]{1,2}|[XY]))\b", term[1])
                # g = re.findall(r"\b(((C|c)hromosome )([0-9]{1,2}|[XY]))\b", term[1])
                if g:
                    # Successful match
                    print(item[0], str(counter), term)
                    glist = [item[0] for item in g]
                    if checkRedFlagWords(term[1], "chr") or counter == 3:
                        chrListTight.append([*[item[0], counter, term[0], term[1]], *glist])
                        if counter in (1, 2):
                            tempRemains = term[1]
                            for st in glist:
                                tempRemains = tidyTempRemains(tempRemains, st)
                            if tempRemains not in chrRemainsList:
                                chrRemainsList.append(tempRemains)
            for term in dtype:
                g = re.findall(r"\b(((C|c)hromosome |(T|t)risomy |(M|m)onosomy )([0-9]{1,2}|[XY]))", term[1])
                # g = re.findall(r"\b(((C|c)hromosome )([0-9]{1,2}|[XY]))", term[1])
                if g:
                    # Successful match
                    print(item[0], str(counter), term)
                    glist = [item[0] for item in g]
                    if checkRedFlagWords(term[1], "chr") or counter == 3:
                        chrListLoose.append([*[item[0], counter, term[0], term[1]], *glist])
            counter += 1

    iscnListDiff = [item for item in iscnListLoose if item[:2] not in [iteminner[:2] for iteminner in iscnListTight]]
    chrListDiff = [item for item in chrListLoose if item[:2] not in [iteminner[:2] for iteminner in chrListTight]]

    # text file cacheing of results to this point:
    writeOutISCNResults(iscnListLoose, ISCNPathLoose) # iscn loose matches
    writeOutISCNResults(iscnListTight, ISCNPathTight) # iscn tight matches
    writeOutISCNResults(sorted(iscnList), ISCNPathISCN) # iscn appearances
    writeOutISCNResults(list(set([item[0] for item in iscnListTight])), ISCNPathTightId, False) # just conceptIds from iscn tight
    writeOutISCNResults(iscnListDiff, ISCNPathDiff)
    writeOutISCNResults(sorted(iscnRemainsList), ISCNPathRemains, False) # iscn # patterns
    writeOutISCNResults(chrListLoose, ChrPathLoose) # chromosome loose matches
    writeOutISCNResults(chrListTight, ChrPathTight) # chromosome tight matches
    writeOutISCNResults(list(set([item[0] for item in chrListTight])), ChrPathTightId, False) # just conceptIds from chromosome tight
    writeOutISCNResults(chrListDiff, ChrPathDiff)
    writeOutISCNResults(sorted(chrRemainsList), ChrPathRemains, False) # chromosome # patterns


    # look for instances of patterns appearing as synonyms
    matchListTight = []
    # iscntight vs. iscntight
    for iscnitem1 in iscnListTight:
        for iscnitem2 in iscnListTight:
            if iscnitem1[0] == iscnitem2[0] and (iscnitem1[1] < 3) and (iscnitem2[1] < 3) and (iscnitem1[1] > 0) and (iscnitem2[1] > 0):
                if not(" due " in iscnitem1[3]) and not(" with " in iscnitem1[3]) and not("omplement " in iscnitem1[3]) and not(" due " in iscnitem2[3]) and not(" with " in iscnitem2[3]) and not("omplement " in iscnitem2[3]):
                    if ([*iscnitem1, *iscnitem2] not in matchListTight) and ([*iscnitem2, *iscnitem1] not in matchListTight) and (iscnitem1 != iscnitem2):
                        matchListTight.append([*iscnitem1, *iscnitem2])
    # iscntight vs. chrtight
    for iscnitem in iscnListTight:
        for chritem in chrListTight:
            if iscnitem[0] == chritem[0] and (iscnitem[1] < 3) and (chritem[1] < 3):
                if [*iscnitem, *chritem] not in matchListTight:
                    matchListTight.append([*iscnitem, *chritem])
    writeOutISCNResults(matchListTight, MatchListTightPath)

    matchListLoose = []
    # iscntight vs. chrloose
    for iscnitem in iscnListTight:
        for chritem in chrListLoose:
            if iscnitem[0] == chritem[0] and (iscnitem[1] < 3) and (chritem[1] < 3) and (iscnitem[2] != chritem[2]):
                if ([*iscnitem, *chritem] not in matchListLoose) and ([*iscnitem, *chritem] not in matchListTight):
                    fsnbool = False
                    if iscnitem[1] == 0:
                        tempString = iscnitem[3].replace(" (disorder)", "")
                        if tempString == chritem[3]:
                            fsnbool = True
                    if chritem[1] == 0:
                        tempString = chritem[3].replace(" (disorder)", "")
                        if tempString == iscnitem[3]:
                            fsnbool = True
                    if not fsnbool:
                        matchListLoose.append([*iscnitem, *chritem])
    writeOutISCNResults(matchListLoose, MatchListLoosePath)
    matchListAll = [*matchListLoose, *matchListTight]
    # prepare matchListAll for dot file
    matchListDotPre = []
    for row in matchListAll:
        # long short arm detection (special case of chromosome matches)
        if ("long arm" in row[3]):
            tempList = [row[3].lower().replace(row[4], "chromosome #").replace("long", "(the) short|long").lower(), row[8].lower().replace(row[9], "#p|q").lower(), row[0], row[2], row[7]]
        elif ("short arm" in row[3]):
            tempList = [row[3].lower().replace(row[4], "chromosome #").replace("short", "(the) short|long").lower(), row[8].lower().replace(row[9], "#p|q").lower(), row[0], row[2], row[7]]
        elif ("long arm" in row[8]):
            tempList = [row[8].replace(row[9], "chromosome #").replace("long", "(the) short|long").lower(), row[3].lower().replace(row[4], "#p|q").lower(), row[0], row[2], row[7]]
        elif ("short arm" in row[8]):
            tempList = [row[8].lower().replace(row[9], "chromosome #").replace("short", "(the) short|long").lower(), row[3].lower().replace(row[4], "#p|q").lower(), row[0], row[2], row[7]]
        else:
            if ("chromosome " in row[4].lower()):
                leftTemp = row[3].lower().replace(row[4].lower(), "chromosome #").lower()
            elif ("monosomy " in row[4].lower()):
                leftTemp = row[3].lower().replace(row[4].lower(), "monosomy #").lower()
            elif ("trisomy " in row[4].lower()):
                leftTemp = row[3].lower().replace(row[4].lower(), "trisomy #").lower()
            else:
                leftTemp = row[3].lower().replace(row[4].lower(), "#").lower()
            if ("chromosome " in row[9].lower()):
                rightTemp = row[8].lower().replace(row[9].lower(), "chromosome #").lower()
            elif ("monosomy " in row[9].lower()):
                rightTemp = row[8].lower().replace(row[9].lower(), "monosomy #").lower()
            elif ("trisomy " in row[9].lower()):
                rightTemp = row[8].lower().replace(row[9].lower(), "trisomy #").lower()
            else:
                rightTemp = row[8].lower().replace(row[9].lower(), "#").lower()

            tempList = [leftTemp, rightTemp, row[0], row[2], row[7]]
        if tempList[0:2] not in [item[0:1] for item in matchListDotPre] and (" (disorder)" not in tempList[0]) and (" (disorder)" not in tempList[1]):
            matchListDotPre.append(tempList)
    matchListDotClean = []
    classList = []
    # clean up a number of # followed by iscn string remainders (from loose detection)
    for row in matchListDotPre:
        tempRow = []
        string0 = ""
        if "#p" in row[0] and not "#p|q" in row[0]:
            startMarker = row[0].find("#p")
            endMarker = row[0][startMarker:].find(" ")
            if endMarker == -1:
                string1 = row[0][:startMarker] + "#"
            else:
                string1 = row[0][:startMarker] + "#" + row[0][startMarker + endMarker:]
        elif "#q" in row[0]:
            startMarker = row[0].find("#q")
            endMarker = row[0][startMarker:].find(" ")
            if endMarker == -1:
                string1 = row[0][:startMarker] + "#"
            else:
                string1 = row[0][:startMarker] + "#" + row[0][startMarker + endMarker:]
        else:
            string0 = row[0]
        string1 = ""
        if "#p" in row[1] and not "#p|q" in row[1]:
            startMarker = row[1].find("#p")
            endMarker = row[1][startMarker:].find(" ")
            if endMarker == -1:
                string1 = row[1][:startMarker] + "#"
            else:
                string1 = row[1][:startMarker] + "#" + row[1][startMarker + endMarker:]
        elif "#q" in row[1]:
            startMarker = row[1].find("#q")
            endMarker = row[1][startMarker:].find(" ")
            if endMarker == -1:
                string1 = row[1][:startMarker] + "#"
            else:
                string1 = row[1][:startMarker] + "#" + row[1][startMarker + endMarker:]
        else:
            string1 = row[1]
        string0 = stringCleanB(stringCleanA(string0))
        string1 = stringCleanB(stringCleanA(string1))
        if string0 not in classList:
            classList.append(string0)
        if string1 not in classList:
            classList.append(string1)
        tempRow.append(string0)
        tempRow.append(string1)
        tempRow.append(row[2])
        tempRow.append(row[3])
        tempRow.append(row[4])
        matchListDotClean.append(tempRow)
    # # identify similar word order variants in classlist
    preSimilarList = []
    similarList = []
    for cl in classList:
        temptermTokens = []
        temptermTokens = [porter.stem(w) for w in nltk.word_tokenize(cl.lower().strip()) if (w.lower() not in stopwords)]
        tempString = " ".join(sorted(set(temptermTokens)))
        preSimilarList.append([cl, tempString])
    for ts1 in preSimilarList:
        for ts2 in preSimilarList:
            if ts1[1] == ts2[1]:
                if ([ts1[0], ts2[0]] not in similarList) and ([ts2[0], ts1[0]] not in similarList) and not(ts1[0] == ts2[0]):
                    similarList.append([ts1[0], ts2[0]])

    singletonListPre = sorted([[stringCleanB(stringCleanA(stringCleanC(item.lower()))), item.lower()] for item in [*iscnRemainsList, *chrRemainsList] if stringCleanB(stringCleanA(stringCleanC(item.lower()))) not in classList])

    writeOutISCNResults(sorted(list(set([item[0] for item in [*matchListLoose, *matchListTight]]))), MatchListIdPath, False)

    singletonList = []
    for item in singletonListPre:
        if item[0] not in singletonList:
            singletonList.append(item[0])

    # call to rudimentary modification method for singletonList
    singletonList = modifySingletonList(singletonList)

    writeOutISCNResults(singletonList, singletonListPath, False)

    # identify similar word order variants in singletons AND classlist
    preSimilarList = []
    singletonMergeSimilarList = []
    for cl in [*singletonList, *classList]:
        temptermTokens = []
        temptermTokens = [porter.stem(w) for w in nltk.word_tokenize(cl.lower().strip()) if (w.lower() not in stopwords)]
        tempString = " ".join(sorted(set(temptermTokens)))
        preSimilarList.append([cl, tempString])
    for ts1 in preSimilarList:
        for ts2 in preSimilarList:
            if ts1[1] == ts2[1]:
                if ([ts1[0], ts2[0]] not in singletonMergeSimilarList) and ([ts2[0], ts1[0]] not in singletonMergeSimilarList) and not(ts1[0] == ts2[0]):
                    singletonMergeSimilarList.append([ts1[0], ts2[0]])

    # identify similar word order variants in singletons only
    preSimilarList = []
    singletonSimilarList = []
    for cl in singletonList:
        temptermTokens = []
        temptermTokens = [porter.stem(w) for w in nltk.word_tokenize(cl.lower().strip()) if (w.lower() not in stopwords)]
        tempString = " ".join(sorted(set(temptermTokens)))
        preSimilarList.append([cl, tempString])
    for ts1 in preSimilarList:
        for ts2 in preSimilarList:
            if ts1[1] == ts2[1]:
                if ([ts1[0], ts2[0]] not in singletonSimilarList) and ([ts2[0], ts1[0]] not in singletonSimilarList) and not(ts1[0] == ts2[0]):
                    singletonSimilarList.append([ts1[0], ts2[0]])

    singletonDotList = []
    singletonDotList.append("subgraph cluster_0 { style=filled; color=\"#ffffff\"; fontsize=9; fontname=Helvetica;")
    counter1 = 0
    counter2 = 0
    if len(singletonList) == 1:
        singletonDotList.append("\"" + singletonList[0] + "\";")
    else:
        for item in singletonList[:-1]:
            if counter2 < 4:
                singletonDotList.append("\"" + singletonList[counter1] + "\" -- \"" + singletonList[counter1 + 1] + "\" [style=invis];")
                counter1 += 1
                counter2 += 1
            else:
                counter1 += 1
                counter2 = 0
        if counter1 == len(singletonList) - 1:
            singletonDotList.append("\"" + singletonList[-1] + "\";")

    singletonDotList.append("}")

    dotClassList = []
    for testlist in [classList, singletonList]:
        for cl in testlist:
            if "monosomy" in cl:
                if moreThanOne("monosomy", cl):
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=red];")
                else:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=yellow];")
            elif "disomy" in cl:
                if moreThanOne("disomy", cl):
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=red];")
                else:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=green];")
            elif "duplication" in cl:
                if moreThanOne("duplication", cl):
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=red];")
                elif "micro" in cl:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=blue, fontcolor=white, color=red];")
                else:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=blue, fontcolor=white];")
            elif "triplication" in cl:
                if moreThanOne("triplication", cl):
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=red];")
                elif "micro" in cl:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=pink, color=red];")
                else:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=pink];")
            elif "trisomy" in cl:
                if moreThanOne("trisomy", cl):
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=red];")
                else:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=indigo, fontcolor=white];")
            elif "tetrasomy" in cl:
                if moreThanOne("tetrasomy", cl):
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=red];")
                else:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=violet];")
            elif "deletion" in cl:
                if moreThanOne("deletion", cl):
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=red];")
                elif "micro" in cl:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=orange, color=red];")
                else:
                    dotClassList.append("\"" + cl + "\"" + " [fillcolor=orange];")
            else:
                dotClassList.append("\"" + cl + "\"" + " [fillcolor=white];")

    # initialise dot file
    matchListDot = []
    matchListDot.append(["graph G { graph [rankdir=LR, overlap=false, ranksep=0.8, splines=true] node [shape=box, fontsize=11, fontname=Helvetica, style=filled, fillcolor=white]; bgcolor=lightgray;"])
    # fill dot file
    matchListDot.append(["/* *** matches subgraph *** */"])
    for row in matchListDotClean:
        if ("\"" + row[0] + "\" -- \"" + row[1] + "\";" not in [item[0] for item in matchListDot]) and ("\"" + row[1] + "\" -- \"" + row[0] + "\";" not in [item[0] for item in matchListDot]):
            matchListDot.append(["\"" + row[0] + "\" -- \"" + row[1] + "\";", "//", row[2], row[3], row[4]])
    matchListDot.append(["/* *** similarity edges *** */"])
    for row in similarList:
        matchListDot.append(["\"" + row[0] + "\" -- \"" + row[1] + "\" [style=dashed, color=blue];"])
    if showSingletonSimilars and showSingletons:
        matchListDot.append(["/* *** additional similarity edges *** */"])
        for row in singletonSimilarList:
            matchListDot.append(["\"" + row[0] + "\" -- \"" + row[1] + "\" [style=dashed, color=green];"])
        remainderList = [row for row in singletonMergeSimilarList if (row not in singletonSimilarList) and (row not in similarList)]
        for row in remainderList:
            matchListDot.append(["\"" + row[0] + "\" -- \"" + row[1] + "\" [style=dashed, color=red];"])
    if showSingletons:
        matchListDot.append(["/* *** singleton subgraph *** */"])
        for row in singletonDotList:
            matchListDot.append([row])
    matchListDot.append(["/* *** " + str(len(dotClassList)) + " classes *** */"])
    for cl in sorted(dotClassList):
        matchListDot.append([cl])
    matchListDot.append(["}"])
    writeOutISCNResults(matchListDot, MatchListDotPath)
    writeOutISCNResults(sorted(classList), classListPath, False)

def moreThanOne(inString, inTerm):
    for matchToken in [item for item in ["monosomy", "disomy", "duplication", "triplication", "trisomy", "tetrasomy", "deletion"] if item != inString]:
        if matchToken in inTerm:
            return True
    return False

if __name__ == "__main__":
    # configs
    showSingletons = True # show or hide singletons
    showSingletonSimilars = True # show or hide singleton-participating similarity relations
    detectISCN(showSingletons, showSingletonSimilars)
