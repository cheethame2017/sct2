from __future__ import print_function, unicode_literals
import io
import time
import re
import sys
import textwrap
from datetime import datetime
from dateutil.relativedelta import relativedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from SCTRF2SVHelpers import tuplify, listify, pterm, SV_SIMparentsFromIdPlusMeta_IN, SV_MDRparentsFromIdPlusMeta_IN, effTimeSet, SV_pterm, \
                            SV_SIMchildrenFromIdPlusMeta_IN, SV_MDRchildrenFromIdPlusMeta_IN, SV_ALLconMetaFromConId_IN, SV_SIMconMetaFromConId_IN, \
                            SV_MDRconMetaFromConId_IN, SV_SIMterms, RLRParts, otherLangSet, SV_SIMrefSetDetailsFromMemIdandSetIdPlusMeta_IN, \
                            SV_MDRterms, SV_SIMrolesFromIdPlusMeta_IN, SV_MDRrolesFromIdPlusMeta_IN, refSetTypeFromConId_IN, \
                            SV_SIMrefSetMemberCountFromConId_IN, SV_SIMrefSetMembersFromConIdPlusMeta_IN, refsetPterm, SV_SIMancestorsFromConId_IN2, \
                            SV_SIMdescendantsFromConId_IN2, SV_SIMFindAncestorsForConIdAndET2, upByNMonths, \
                            SV_SIMdescendantsFromConIdWithET_IN, SV_SIMcheckIsARefset, SV_SIMC1_IsA_C2_Compare, SV_SIMcheckRefsetMemberCount, \
                            SV_SIMancestorsFromConIdWithET_IN2, checkEarliestReleaseSimpleRefset, downByNMonths, FindAllMembersOfRefset, \
                            SV_SIMCheckConceptAgainstRefset, SV_SIMconMetaFromConIdSimple_IN, SV_SIMrefSetFromConIdPlusMeta_IN, \
                            SV_MDRrefSetFromConIdPlusMeta_IN, startNeoPlatformCall, stopNeoPlatformCall, SV_SIMHistoricalRefsetRowsfromId, \
                            SV_MDRrefSetMemberCountFromConId_IN, SV_MDRrefSetMembersFromConIdPlusMeta_IN, \
                            SV_SIMactiveColor_IN, SV_fsn, SV_SIMdesMetaFromDesId_IN
from SCTRF2Helpers import CiDFromDiD_IN, tabString, simplify, tabCfg, limitAttributes, checkAllAtts, refSetFieldsFromConId_IN
from SCTRF2Funcs import drawMDR, sorted_cfg, validateVerhoeff, southWestMarker, northWestMarker, southWestMarkerParent, padForParents, padForGroup
from SCTRF2Role import readFileMembers
from SCTRF2Refset import refsetList
from SCTRF2SVconfig import cfgWrite, tok, cfg
from platformSpecific import cfgPathInstall, clearScreen, copyDescendantsD3, copyAncestorsD3, copyFocusD3, sep, input2_3


def snapshotHeader():
    if cfg("QUERY_TYPE") == 1:
        queryTypeString = 'Showing all data'
    elif cfg("QUERY_TYPE") == 2:
        queryTypeString = 'Showing data for ' + str(cfg("EFFECTIVE_TIME")) + ', simple snapshot'
    else:
        queryTypeString = 'Showing data for ' + str(cfg("EFFECTIVE_TIME")) + ', MDR snapshot'
    if cfg('SHOW_TYPE') == 1:
        activeTypeString = ', active rows only'
    elif cfg('SHOW_TYPE') == 2:
        activeTypeString = ', active and inactive rows'
    else:
        activeTypeString = ', inactive rows only'
    if cfg("PTERM_TYPE") == 0 or cfg("QUERY_TYPE") == 1:
        pTermTypeString = ', latest snapshot PTs'
    else:
        pTermTypeString = ', state valid PTs'
    print("\033[36;7m" + queryTypeString + activeTypeString + pTermTypeString + '\033[92;27m \n')

def promptString():
    indexDict = {'a': 'Browse from root',
                'b': 'Import terminal',
                'c': 'Snapshot crumbs',
                'd': 'Show all data',
                'e': 'Simple snapshot',
                'f': 'MDR snapshot',
                'g': 'Show active data',
                'h': 'Show active+inac.',
                'i': 'Show inactive data',
                'j': 'Breadcrumbs   ',
                'k': 'Show metadata (1/2)',
                'l': 'Hide metadata',
                'm': 'Latest terms',
                'n': 'Snapshot terms',
                'o': 'Indicate subtypes',
                'p': 'Hide subtypes',
                'q': '(+row) Terms',
                'r': '(+row) Roles',
                's': '(+row) Ances->xlsx',
                't': '(+row) Ancestors',
                'u': '(+row) Descendants',
                'v': 'Reload focus',
                'w': '(+row) Desc->xlsx',
                'x': '(+row) Refsets',
                'y': 'Input effecTime',
                'z': '(+row) Ref members',
                'A': '(+row) Ref->xlsx',
                'B': '(+r) RS[n] detail',
                'C': 'Pretty (0/1/2)',
                'D': '(+row) Ances->D3',
                'E': '(+row) Ances->xdot',
                'F': 'D3 extras (0-2)',
                'G': '(+row) Desc->D3',
                'H': '(+row) Desc->xdot',
                'I': '(+row) Foc->D3',
                'J': '(+row) Foc->xdot',
                'K': 'Sups/orth (0-3)',
                'L': 'Angles (0/1)  ',
                'M': 'Force ac/down (1/0)',
                'N': '(+set) Mem->xlsx'}
    indexDictLetters = list(indexDict)
    promptString = 'Action:\n\033[36m'
    counter = 1
    if tabCfg() == 0:
        for letter in indexDictLetters:
            promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 3 == 0 else '\t')
            counter += 1
    else:
        for letter in indexDictLetters:
            promptString += '[' + letter + '] ' + indexDict[letter] + ('\n' if counter % 4 == 0 else '\t')
            counter += 1
    promptString += '\n\n[zz/zzz] Exit\t\t[-] (+letter) help\n\n\033[36;7mInput selection:\033[92;27m '
    return promptString

def browseFromId(inId):
    mydict = {}
    mycounter = 1
    # supertypes
    parentList = []
    if  inId[0][-2] == '1':
        myId = tuplify(CiDFromDiD_IN(inId))
    else:
        myId = inId
    # parentList = parentsFromId_IN(myId)  # call interface neutral code
    if cfg("QUERY_TYPE") == 1:
        parentList = SV_SIMparentsFromIdPlusMeta_IN(myId, 3, cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 2:
        parentList = SV_SIMparentsFromIdPlusMeta_IN(myId, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
    elif cfg("QUERY_TYPE") == 3:
        parentList = SV_MDRparentsFromIdPlusMeta_IN(myId, cfg('SHOW_TYPE'), effTimeSet())
    if len(parentList) > 0:
        print('Parents: [' + str(len(parentList)) + ']')
        for listParent in parentList:
            if not listParent is None:
                if cfg("QUERY_TYPE") == 3:
                    print('[' + str(mycounter) + ']\t' + listParent[3] + tabString(listParent[3]) + listParent[4])
                else:
                    print('[' + str(mycounter) + ']\t' + SV_SIMactiveColor_IN(listParent[3], cfg("EFFECTIVE_TIME"), True) + tabString(listParent[3]) + listParent[4])
                if cfg('SHOW_METADATA') > 0:
                    if cfg('SHOW_METADATA') == 2:
                        browseFromIdConMeta(tuplify(listParent[3]), str(mycounter), 0)
                    print('\033[36m(Rel:Module=' + listParent[5] + activeMetaColor(listParent[9]) + ', Time=' + listParent[6]+ ')\033[92m')
                    if cfg('SHOW_METADATA') == 2:
                        print('\033[36m(Rel Id=' + listParent[10] + ')\033[92m')
                mydict.update(list(zip(listify(mycounter), listify(tuplify(listParent[3])))))
                mycounter += 1
        print()
    # focus
    print('Focus:')
    if cfg("QUERY_TYPE") == 3:
        print('[' + str(mycounter) + ']\t' + myId[0] + tabString(myId[0]) + '\033[1m' + SV_pterm(myId) + '\033[22m')
        print('\t' + (' ' * len(myId[0])) + tabString(myId[0]) + '\033[2m' + SV_fsn(myId) + '\033[22m')
    else:
        print('[' + str(mycounter) + ']\t' + SV_SIMactiveColor_IN(myId[0], cfg("EFFECTIVE_TIME"), True) + tabString(myId[0]) + '\033[1m' + SV_pterm(myId) + '\033[22m')
        print('\t' + (' ' * len(myId[0])) + tabString(myId[0]) + '\033[2m' + SV_fsn(myId) + '\033[22m')
    browseFromIdConMeta(myId, str(mycounter), 0)
    mydict.update(list(zip(listify(mycounter), listify(myId))))
    mycounter += 1
    print()

    # subtypes
    childList = []
    # childList = childrenFromIdWithPlus_IN(myId)  # call interface neutral code
    if cfg("QUERY_TYPE") == 1:
        childList = SV_SIMchildrenFromIdPlusMeta_IN(myId, 3, cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 2:
        childList = SV_SIMchildrenFromIdPlusMeta_IN(myId, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 3:
        childList = SV_MDRchildrenFromIdPlusMeta_IN(myId, cfg('SHOW_TYPE'), effTimeSet())
    print('Children: [' + str(len(childList)) + ']')
    for listChild in childList:
        if not listChild is None:
            if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
                if cfg("QUERY_TYPE") == 3:
                    print('[' + str(mycounter) + ']\t' + " " + listChild[3] + tabString(listChild[3] + " ") + listChild[4])
                else:
                    print('[' + str(mycounter) + ']\t' + " " + SV_SIMactiveColor_IN(listChild[3], cfg("EFFECTIVE_TIME"), True) + tabString(listChild[3] + " ") + listChild[4])
            else:
                if cfg("QUERY_TYPE") == 3:
                    print('[' + str(mycounter) + ']\t' + listChild[10] + listChild[3] + tabString(listChild[10] + listChild[3]) + listChild[4])
                else:
                    print('[' + str(mycounter) + ']\t' + listChild[10] + SV_SIMactiveColor_IN(listChild[3], cfg("EFFECTIVE_TIME"), True) + tabString(listChild[10] + listChild[3]) + listChild[4])
            if cfg('SHOW_METADATA') > 0:
                if cfg('SHOW_METADATA') == 2:
                    browseFromIdConMeta(tuplify(listChild[3]), str(mycounter), 0)
                print('\033[36m(Rel:Module=' + listChild[5] + activeMetaColor(listChild[9]) + ', Time=' + listChild[6] + ')\033[92m')
                if cfg('SHOW_METADATA') == 2:
                    if (cfg("SUB_SHOW") == 0) or (cfg("QUERY_TYPE") == 1):
                        print('\033[36m(Rel Id=' + listChild[10] + ')\033[92m')
                    else:
                        print('\033[36m(Rel Id=' + listChild[11] + ')\033[92m')
            mydict.update(list(zip(listify(mycounter), listify(tuplify(listChild[3])))))
            mycounter += 1

    # housekeeping
    print()
    return mydict


def dictFromId(inId):
    if not isinstance(inId, tuple):
        inId = tuplify(inId)
    mydict = {}
    mycounter = 1
    if  inId[0][-2] == '1':
        myId = tuplify(CiDFromDiD_IN(inId))
    else:
        myId = inId
    mydict.update(list(zip(listify(mycounter), listify(myId))))
    return mydict


def browseFromIdConMeta(myId, counterNum, callType):
    preConMeta = []
    if cfg("QUERY_TYPE") == 1:
        preConMeta = SV_ALLconMetaFromConId_IN(myId[0])
    elif cfg("QUERY_TYPE") == 2:
        preConMeta = SV_SIMconMetaFromConId_IN(myId[0], cfg("EFFECTIVE_TIME"))
    else:
        preConMeta = SV_MDRconMetaFromConId_IN(myId[0], effTimeSet())
    someConMeta = False
    for conMeta in preConMeta:
        someConMeta = True
        # print type(preConMeta), len(preConMeta), len(preConMeta[0]), preConMeta, type(conMeta)
        if cfg('SHOW_METADATA') > 0:
            if callType == 0:
                if len(conMeta) == 4:
                    print('\033[36m(Con:Module=' + conMeta[0] + activeMetaColor(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ')\033[92m')
                elif len(conMeta) == 5:
                    print('\033[36m(Con:Module=' + conMeta[0] + activeMetaColor(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ', Inac.reason=' + conMeta[4] + ')\033[92m')
            else:
                if len(conMeta) == 4:
                    print('\033[36m(Con['+ counterNum + ']:Module=' + conMeta[0] + activeMetaColor(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ')\033[92m')
                elif len(conMeta) == 5:
                    print('\033[36m(Con['+ counterNum + ']:Module=' + conMeta[0] + activeMetaColor(conMeta[1]) + ', Time=' + conMeta[2] + ', Defn=' + conMeta[3] + ', Inac.reason=' + conMeta[4] + ')\033[92m')
    if not someConMeta:
        print('(Con:No evidence of concept at this effectiveTime)')

def activeMetaColor(inValue):
    if inValue == 1 or inValue == '1':
        return ', Active=' + str(inValue)
    else:
        return ', \033[31mActive=' + str(inValue) + '\033[36m'

def activeAcceptabilityColor(inAccepValue, inActiveValue):
    if (inAccepValue == 'Preferred') and (inActiveValue == 1 or inActiveValue == '1'):
        return ', \033[95mAcc=' + inAccepValue + '\033[36m'
    else:
        return ', Acc=' + inAccepValue

def descIdActiveMetaColor(inId, inValue): # status at ET already known so simply color id as well (whether metadata shown or not)
    if inValue == 1 or inValue == '1':
        return inId
    else:
        return '\033[31m' + inId + '\033[92m'

def allTermsFromConIdPlusMeta(identifier):
    mydict = {}
    mycounter = 1
    effectiveTime = cfg("EFFECTIVE_TIME") #read in from SVconfig file.
    if cfg("QUERY_TYPE") == 1:
        myTerms = SV_SIMterms(identifier, 3, effectiveTime, (RLRParts + otherLangSet))
    if cfg("QUERY_TYPE") == 2:
        myTerms = SV_SIMterms(identifier, cfg("SHOW_TYPE"), effectiveTime, (RLRParts + otherLangSet))
    elif cfg("QUERY_TYPE") == 3:
        myTerms = SV_MDRterms(identifier, cfg("SHOW_TYPE"), effTimeSet(), (RLRParts + otherLangSet))

    print("Terms for:", SV_SIMactiveColor_IN(identifier, effectiveTime), SV_pterm(identifier))
    print("([1] to return to selected concept)")
    print()
    if len(myTerms) == 0: # if no terms returned (should only happen with inactive only as show)
        if cfg("SHOW_TYPE") == 0:
            print("No inactive descriptions for this concept\n") # if calling terms with inactive only as show type
        else:
            print("No descriptions found for this concept\n") # shouldn't happen
    else:
        # Fully specified name
        print("FSNs:")
        for rows in myTerms:
            if rows[1][0][6] == tok('FSN'):
                for row in rows[1]:
                    print(descIdActiveMetaColor(row[0], row[2]) + tabString(row[0]) + row[7])
                    if cfg('SHOW_METADATA') > 0:
                        if row[2] == 0:
                            print('\033[36m(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + activeMetaColor(row[2]) + ', Inac.reason=' + SV_SIMdesMetaFromDesId_IN(row[0], effectiveTime) + ')\033[92m')
                        else:
                            print('\033[36m(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + activeMetaColor(row[2]) + ')\033[92m')
                if cfg('SHOW_METADATA') > 0:
                    for rsEntry in rows[2]:
                        print('\033[36m(Ref=' + SV_pterm(rsEntry[4]) + activeAcceptabilityColor(SV_pterm(rsEntry[6]), rsEntry[2]) + ', Ref.Mod=' + SV_pterm(rsEntry[3]) + ', Time=' + str(rsEntry[1]) + activeMetaColor(rsEntry[2]) + ')\033[92m')
                    print()
        print("\nSynonyms:")
        # Synonym
        for rows in myTerms:
            if rows[1][0][6] == tok('SYNONYM'):
                for row in rows[1]:
                    print(descIdActiveMetaColor(row[0], row[2]) + tabString(row[0]) + row[7])
                    if cfg('SHOW_METADATA') > 0:
                        if row[2] == 0:
                            print('\033[36m(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + activeMetaColor(row[2]) + ', Inac.reason=' + SV_SIMdesMetaFromDesId_IN(row[0], effectiveTime) + ')\033[92m')
                        else:
                            print('\033[36m(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + activeMetaColor(row[2]) + ')\033[92m')
                if cfg('SHOW_METADATA') > 0:
                    for rsEntry in rows[2]:
                        print('\033[36m(Ref=' + SV_pterm(rsEntry[4]) + activeAcceptabilityColor(SV_pterm(rsEntry[6]), rsEntry[2]) + ', Ref.Mod=' + SV_pterm(rsEntry[3]) + ', Time=' + str(rsEntry[1]) + activeMetaColor(rsEntry[2]) + ')\033[92m')
                    print()
        # Definition
        for rows in myTerms:
            if rows[1][0][6] == tok('DEFINIT'):
                print("\nDefinitions:")
                for row in rows[1]:
                    print(descIdActiveMetaColor(row[0], row[2]) + tabString(row[0]) + row[7])
                    if cfg('SHOW_METADATA') > 0:
                        if row[2] == 0:
                            print('\033[36m(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + activeMetaColor(row[2]) + ', Inac.reason=' + SV_SIMdesMetaFromDesId_IN(row[0], effectiveTime) + ')\033[92m')
                        else:
                            print('\033[36m(Des:Module=' + SV_pterm(row[3]) + ', ICS=' + SV_pterm(row[8]) + ', Time=' + row[1] + activeMetaColor(row[2]) + ')\033[92m')
                if cfg('SHOW_METADATA') > 0:
                    for rsEntry in rows[2]:
                        print('\033[36m(Ref=' + SV_pterm(rsEntry[4]) + activeAcceptabilityColor(SV_pterm(rsEntry[6]), rsEntry[2]) + ', Ref.Mod=' + SV_pterm(rsEntry[3]) + ', Time=' + str(rsEntry[1]) + activeMetaColor(rsEntry[2]) + ')\033[92m')
        print()
    mydict.update(list(zip(listify(mycounter), listify(tuplify(identifier)))))
    return mydict


def allTermsFromConId(identifier):
    mydict = {}
    mycounter = 1
    effectiveTime = cfg("EFFECTIVE_TIME") #read in from SVconfig file.

    if cfg("QUERY_TYPE") == 1:
        myTerms = SV_SIMterms(identifier, 3, effectiveTime, ((RLRParts + otherLangSet)))
    if cfg("QUERY_TYPE") == 2:
        myTerms = SV_SIMterms(identifier, cfg("SHOW_TYPE"), effectiveTime, ((RLRParts + otherLangSet)))
    elif cfg("QUERY_TYPE") == 3:
        myTerms = SV_MDRterms(identifier, cfg("SHOW_TYPE"), effTimeSet(), ((RLRParts + otherLangSet)))

    print("Terms for:", identifier, SV_pterm(identifier))
    print("([1] to return to selected concept)")
    print()
    # Fully specified name
    print("FSNs:")
    for rows in myTerms:
        if rows[1][0][6] == tok('FSN'):
            for row in rows[1]:
                print(row[0] + tabString(row[0]) + row[7])
    print()
    print("\nSynonyms:")
    # Synonym
    for rows in myTerms:
        if rows[1][0][6] == tok('SYNONYM'):
            for row in rows[1]:
                print(row[0] + tabString(row[0]) + row[7])
    print()
    # Definition
    for rows in myTerms:
        if rows[1][0][6] == tok('DEFINIT'):
            print("\nDefinitions:")
            for row in rows[1]:
                print(row[0] + tabString(row[0]) + row[7])
    print()
    mydict.update(list(zip(listify(mycounter), listify(tuplify(identifier)))))
    return mydict


def groupedRolesFromConIdPlusMeta(inId):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(inId)))))
    print("Roles for:", inId, pterm(inId))
    print("([1] to return to selected concept)")
    print()
    effectiveTime = cfg("EFFECTIVE_TIME") #read in from SVconfig file.
    if cfg("QUERY_TYPE") == 1:
        myroles = SV_SIMrolesFromIdPlusMeta_IN(inId, 3, effectiveTime)
    if cfg("QUERY_TYPE") == 2:
        myroles = SV_SIMrolesFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), effectiveTime)
    elif cfg("QUERY_TYPE") == 3:
        myroles = SV_MDRrolesFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), effTimeSet())
    for roleGroup in myroles:
        for role in roleGroup:
            if isinstance(role, int):
                if role == -1:
                    print("Parents:")
                elif role == 0:
                    print("Ungrouped:")
                else:
                    print("Group:")
            else:
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(role[1])))))
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(role[3])))))
                print('[' + str(mycounter - 1) + ']', role[1], role[2] + " = " + '[' + str(mycounter) + ']', role[3], role[4])
                if cfg('SHOW_METADATA') > 0:
                    if cfg('SHOW_METADATA') == 2:
                        browseFromIdConMeta(tuplify(role[1]), str(mycounter - 1), 1)
                        browseFromIdConMeta(tuplify(role[3]), str(mycounter), 1)
                    print('\033[36m(Rel:Module=' + role[5] + activeMetaColor(role[9]) + ', Time=' + role[6] + ', Type=' + role[7] + ', Logic=' + role[8] + ')\033[92m')
                    if cfg('SHOW_METADATA') == 2:
                        print('\033[36m(Rel Id=' + role[10] + ("" if role[0] == -1 else ", RG No=" + str(role[0])) + ')\033[92m')
        print()
    writePreDotFile(tuplify(inId), effectiveTime)
    print("Definition:\n")
    f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'pre_dot_sv.txt', 'r', encoding="UTF-8")
    for line in f:
        print(parsablePretty(line.strip()))
    f.close()
    print()
    return mydict


def breadcrumbsWrite(inString):
    if pterm(inString) != "" and inString != tok("ROOT"):
        for fileName in ['breadcrumbs_sv.txt', 'breadcrumbs_svLong.txt']:
            readbackList = []
            writeToList = []
            try:
                f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + fileName, "r", encoding="UTF-8")
                lines = f.readlines()
                for line in lines:
                    readbackList.append(line)
                matchBool = False
                if inString.strip() + '\n' in readbackList:
                    matchBool = True
                if not matchBool:
                    readbackList.append(inString.strip() + '\n')
                f.close()
                f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + fileName, 'w', encoding="UTF-8")
                if matchBool:
                    writeToList = readbackList
                else:
                    writeToList = readbackList[1:]
                for entry in writeToList:
                    f.write(entry)
            finally:
                f.close()


def breadcrumbsRead(inType, longBool):
    mydict = {}
    mycounter = 1
    breadList = []
    if longBool:
        addnText = 'Long'
    else:
        addnText = ''
    if inType == 'Full':
        print('Recent SV codes:\n')
        f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + 'breadcrumbs_sv' + addnText + '.txt', 'r', encoding="UTF-8")
    else:
        print('Recent Snap codes:\n')
        f = io.open(cfgPathInstall() + 'files' + sep() + 'bread' + sep() + 'breadcrumbs' + addnText + '.txt', 'r', encoding="UTF-8")
    for line in f:
        tempLine = [line.strip(), refsetPterm(line.strip())]
        if tempLine not in breadList:
            breadList.append(tempLine)
    if longBool:
        breadList.sort(key=lambda x: x[1])
    for line in breadList:
        print('[' + str(mycounter) + ']\t' + SV_SIMactiveColor_IN(line[0], cfg("EFFECTIVE_TIME"), True) + tabString(line[0]) + line[1])
        mydict.update(list(zip(listify(mycounter), listify(tuplify(line[0])))))
        mycounter += 1
    f.close()
    print()
    return mydict


def focusWrite(inString):
    if len(inString) > 5:
        try:
            f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'focus_sv.txt', 'w', encoding="UTF-8")
            f.write(inString)
        finally:
            f.close()


def readInFromTerminal():
    mydict = {}
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'focusSwap.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    focusWrite(myId)
    mydict = browseFromId(tuplify(myId))
    return mydict


def readInFromFocus():
    mydict = {}
    f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'focus_sv.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    mydict = browseFromId(tuplify(myId))
    return mydict


def readFocusValue():
    f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'focus_sv.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    return tuplify(myId)


def readInFromWindow():
    mydict = {}
    f = io.open(cfgPathInstall() + 'files/focusSwap_mw.txt', 'r', encoding="UTF-8")
    for line in f:
        myId = line.strip()
    f.close()
    focusWrite(myId)
    mydict = browseFromId(tuplify(myId))
    return mydict


def showRefsetRowsFromConAndRefset(memberId, refsetId):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(memberId)))))
    print('Rows for', SV_SIMactiveColor_IN(memberId, cfg("EFFECTIVE_TIME")), refsetPterm(memberId), 'in', refsetId, pterm(refsetId))
    print()
    if cfg("QUERY_TYPE") == 1:
        myValueRows = SV_SIMrefSetDetailsFromMemIdandSetIdPlusMeta_IN(refsetId, memberId, 3, cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 2:
        myValueRows = SV_SIMrefSetDetailsFromMemIdandSetIdPlusMeta_IN(refsetId, memberId, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 3:
        myValueRows = SV_SIMrefSetDetailsFromMemIdandSetIdPlusMeta_IN(refsetId, memberId, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
        # myValueRows = SV_MDRrefSetDetailsFromMemIdandSetIdPlusMeta_IN(refsetId, memberId,  cfg('SHOW_TYPE'), effTimeSet(inId, 'SVshowRefsetRowsForRefset'))
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    for value in myValueRows:
        for row in myDescRows:
            if (row[1] == tok('CONTYPE')) or (row[1] == tok('COMPTYPE')):  # if componentId datatype
                print('[' + str(mycounter) + ']\t' + row[0] + "\t" + SV_SIMactiveColor_IN(value[row[2]], cfg("EFFECTIVE_TIME")) + "\t" + refsetPterm(value[row[2]]))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(value[row[2]])))))
                mycounter += 1
            elif row[1] == tok("PARS_STR") and cfg("PRETTY") > 0:
                print('[*]\t' + row[0] + "\t" + str(parsablePretty(value[row[2]])))
            elif row[1] == tok('OWL_2_STR') and cfg("PRETTY") > 0:
                print('[*]\t' + row[0] + "\t" + str(parsablePrettyOwl(value[row[2]])))
            else:
                print('[*]\t' + row[0] + "\t" + str(value[row[2]]))
        if cfg('SHOW_METADATA') > 0:
            print('\033[36m(Module=' + pterm(value[-4]) + activeMetaColor(value[-2]) + ', Time=' + value[-3] + ')\033[92m')
            if cfg('SHOW_METADATA') > 1:
                print('\033[36m(Row Id=' + value[-1] + ')\033[92m')
        print()
    print('[1] for', SV_SIMactiveColor_IN(memberId, cfg("EFFECTIVE_TIME")), refsetPterm(memberId))
    print()
    return mydict


def refsetPtermWithDict(inDict, inKey):
    if inKey in inDict:
        return inDict[inKey]
    else:
        return refsetPterm(inKey)


def showRefsetRowsForRefset(refsetId):
    mydict = {}
    mycounter = 1
    mydict.update(list(zip(listify(mycounter), listify(tuplify(refsetId)))))
    mycounter += 1
    print('Rows for', refsetId, pterm(refsetId))
    print()
    myRefset = refSetTypeFromConId_IN(refsetId)
    for refset in myRefset:
        refsetType = refset[0]
    if cfg("QUERY_TYPE") == 1:
        myCount = SV_SIMrefSetMemberCountFromConId_IN(refsetId, refsetType, 3, cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 2:
        myCount = SV_SIMrefSetMemberCountFromConId_IN(refsetId, refsetType, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 3:
        myCount = SV_MDRrefSetMemberCountFromConId_IN(refsetId, refsetType, cfg('SHOW_TYPE'), effTimeSet())
    print("refset member count= ", str(myCount))
    secondaction = input2_3('Sample [Y/N]:')
    if secondaction == 'y':
        if myCount < 300:
            print()
            myfilter = 0
        elif myCount < 9000:
            print("approx 1/10 sample:")
            print()
            myfilter = 1
        elif myCount < 90000:
            print("approx 1/100 sample:")
            print()
            myfilter = 2
        elif myCount < 1000000:
            print("approx 1/10000 sample:")
            print()
            myfilter = 3
        else:
            print("too many members to list")
            print()
            return mydict
    else:
        if myCount < 6000:
            print()
            myfilter = 0
        else:
            thirddaction = input('Really? [Y/N]:')
            if thirddaction == 'y':
                myfilter = 0
            else:
                print("sorry, too many members to list unfiltered")
                print()
                return mydict
    if cfg("QUERY_TYPE") == 1:
        myValueRows = SV_SIMrefSetMembersFromConIdPlusMeta_IN(refsetId, refsetType, myfilter, myCount, 3, cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 2:
        myValueRows = SV_SIMrefSetMembersFromConIdPlusMeta_IN(refsetId, refsetType, myfilter, myCount, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
    if cfg("QUERY_TYPE") == 3:
        myValueRows = SV_MDRrefSetMembersFromConIdPlusMeta_IN(refsetId, refsetType, myfilter, myCount, cfg('SHOW_TYPE'), effTimeSet())
    # find terms for myValueRows and sort memberIds on them
    myValueTermsDict = {}
    for value in myValueRows:
        myValueTermsDict[value[0]] = refsetPterm(value[0])
    sortOrderIds = [x[0] for x in sorted(myValueTermsDict.items(), key=lambda x: x[1].lower())]
    if (sys.version_info > (3, 0)):
        # Python 3 code:
        myValueRows.sort(key=lambda i: sortOrderIds.index(i[0]))
    myDescRows = refSetFieldsFromConId_IN(refsetId)
    myRefsetType = 'c' + refSetTypeFromConId_IN(refsetId)[0][0]  # if component datatype (can't pick up which one directly)
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                print('[' + str(mycounter) + ']\t' + row[0] + "\t" + SV_SIMactiveColor_IN(value[counter], cfg("EFFECTIVE_TIME")) + "\t" + refsetPtermWithDict(myValueTermsDict, value[counter]))
                mydict.update(list(zip(listify(mycounter), listify(tuplify(value[counter])))))
                mycounter += 1
            else:
                if row[1] == tok('PARS_STR') and cfg("PRETTY") > 0:
                    print('[*]\t' + row[0] + "\t" + str(parsablePretty(value[row[2]])))
                elif row[1] == tok('OWL_2_STR') and cfg("PRETTY") > 0:
                    print('[*]\t' + row[0] + "\t" + str(parsablePrettyOwl(value[row[2]])))
                else:
                    print('[*]\t' + row[0] + "\t" + str(value[row[2]]))
            counter += 1
        if cfg('SHOW_METADATA') > 0:
            print('\033[36m(Module=' + pterm(value[-4]) + activeMetaColor(value[-2]) + ', Time=' + value[-3] + ')\033[92m')
            if cfg('SHOW_METADATA') > 1:
                print('\033[36m(Row Id=' + value[-1] + ')\033[92m')
        if len(myDescRows) > 1:
            print()
    print()
    print('[1] for', refsetId, pterm(refsetId))
    print()
    f = io.open(cfgPathInstall() + 'files' + sep() + 'snap' + sep() + 'refsetreport.txt', 'w', encoding="UTF-8")
    f.write('Members of ' + refsetId + ' ' + pterm(refsetId) + ':\n\n')
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                if value[counter] in myValueTermsDict:
                    f.write(row[0] + "\t" + value[counter] + "\t" + myValueTermsDict[value[counter]] + '\n')
                else:
                    f.write(row[0] + "\t" + value[counter] + "\t" + refsetPterm(value[counter]) + '\n')
                counter += 1
            else:
                f.write(row[0] + "\t" + str(value[row[2]]) + '\n')
                counter += 1
    f.close()
    if refsetId == tok("MDR"):
        drawMDR(myValueRows, cfg("EFFECTIVE_TIME"))
    return mydict


def ancestorsFromConId(inId):
    mydict = {}
    mycounter = 1
    # ancestors
    ancestorList = []
    ancestorList = SV_SIMancestorsFromConId_IN2(inId)  # call interface neutral code
    inactiveAncestorList = []
    allAncestorList = []
    if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
        allAncestorList = ALLancestorsFromConIdToET(inId, 1)
        inactiveAncestorList = list(set(allAncestorList).difference(set(ancestorList)))
        clearScreen()
        snapshotHeader()
    if len(ancestorList) > 0:
        print('Active ancestors: [' + str(len(ancestorList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)')
        print()
        for ancestor in ancestorList:
            if not ancestor is None:
                if (cfg('SHOW_TYPE') == 1) or (cfg('SHOW_TYPE') == 2):
                    print('[' + str(mycounter) + ']\t' + SV_SIMactiveColor_IN(ancestor[0], cfg("EFFECTIVE_TIME"), True) + tabString(ancestor[0]) + ancestor[1])
                mydict.update(list(zip(listify(mycounter), listify(tuplify(ancestor[0])))))
                if ancestor[0] == inId:
                    findid = mycounter
                mycounter += 1
        print()
        if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
            print('Former ancestors: [' + str(len(inactiveAncestorList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)')
            print()
            for ancestor in inactiveAncestorList:
                if not ancestor is None:
                    print('[' + str(mycounter) + ']\t' + SV_SIMactiveColor_IN(ancestor[0], cfg("EFFECTIVE_TIME"), True) + tabString(ancestor[0]) + ancestor[1])
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(ancestor[0])))))
                    mycounter += 1
            print()
        print('[' + str(findid) + '] to return to', inId, SV_pterm(inId))
        print()
    else:
        clearScreen()
        mydict = browseFromId(tuplify(id))
    return mydict

def descendantsFromConId(inId):
    mydict = {}
    mycounter = 1
    descendantList = []
    descendantList = SV_SIMdescendantsFromConId_IN2(inId)
    inactiveDescendantList = []
    allDescendantList = []
    if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
        allDescendantList = ALLdescendantsFromConIdToET(inId, 1, True)
        inactiveDescendantList = list(set(allDescendantList).difference(set(descendantList)))
        inactiveDescendantList = sorted(inactiveDescendantList, key=lambda x: x[1].lower())
    clearScreen()
    snapshotHeader()
    if len(descendantList) > 0:
        print('Active descendants: [' + str(len(descendantList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)')
        print()
        for descendant in descendantList:
            if not descendant is None:
                if (cfg('SHOW_TYPE') == 1) or (cfg('SHOW_TYPE') == 2):
                    print('[' + str(mycounter) + ']\t' + SV_SIMactiveColor_IN(descendant[0], cfg("EFFECTIVE_TIME"), True) + tabString(descendant[0]) + descendant[1])
                mydict.update(list(zip(listify(mycounter), listify(tuplify(descendant[0])))))
                if descendant[0] == inId:
                    findid = mycounter
                mycounter += 1
        print()
        if (cfg('SHOW_TYPE') == 0) or (cfg('SHOW_TYPE') == 2):
            print('Former descendants: [' + str(len(inactiveDescendantList)) + '] for', inId, SV_pterm(inId) + ' (simple snapshot)')
            print()
            for descendant in inactiveDescendantList:
                if not descendant is None:
                    print('[' + str(mycounter) + ']\t' + SV_SIMactiveColor_IN(descendant[0], cfg("EFFECTIVE_TIME"), True) + tabString(descendant[0]) + descendant[1])
                    mydict.update(list(zip(listify(mycounter), listify(tuplify(descendant[0])))))
                    mycounter += 1
            print()
        print('[' + str(findid) + '] to return to', inId, SV_pterm(inId))
        print()
    else:
        clearScreen()
        mydict = browseFromId(tuplify(inId))
            # mydict.update(zip(listify(mycounter), listify(tuplify(id))))
    return mydict


def setEffectiveTime():
    while True:
        effectiveTime = input('Date YYYYMMDD:')
        if len(effectiveTime) == 2 and not(effectiveTime == 'uu' or effectiveTime == 'dd') and effectiveTime.isdigit():  # should do with regex
            effectiveTime = "20" + effectiveTime + "0131"
        if len(effectiveTime) == 8 and not(effectiveTime == 'uuuuuuuu' or effectiveTime == 'dddddddd'):  # should do with regex
        # if re.match("[^0-9]([0-9]{8})[^0-9]", effectiveTime):
            cfgWrite("EFFECTIVE_TIME", effectiveTime)
            try:
                f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'effectiveTime.txt', 'w', encoding="UTF-8")
                f.write(effectiveTime)
            finally:
                f.close()
            return effectiveTime
        else:
            True
        if effectiveTime == 'now':
            effectiveTime = time.strftime("%Y%m%d")
            cfgWrite("EFFECTIVE_TIME", effectiveTime)
            try:
                f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'effectiveTime.txt', 'w', encoding="UTF-8")
                f.write(str(effectiveTime))
            finally:
                f.close()
            return effectiveTime
        if len(effectiveTime) > 0:
            if effectiveTime[0] == 'u':
                effectiveTimeTemp = str(cfg('EFFECTIVE_TIME'))
                effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
                sixMonthIncrement = relativedelta(months=(6 * len(effectiveTime)))
                effTimeObj += sixMonthIncrement
                effectiveTime = effTimeObj.strftime("%Y%m%d")
                cfgWrite("EFFECTIVE_TIME", effectiveTime)
                try:
                    f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'effectiveTime.txt', 'w', encoding="UTF-8")
                    f.write(effectiveTime)
                finally:
                    f.close()
                return effectiveTime
            if effectiveTime[0] == 'd':
                effectiveTimeTemp = str(cfg('EFFECTIVE_TIME'))
                effTimeObj = datetime.strptime(effectiveTimeTemp, "%Y%m%d")
                sixMonthIncrement = relativedelta(months=(6 * len(effectiveTime)))
                effTimeObj -= sixMonthIncrement
                effectiveTime = effTimeObj.strftime("%Y%m%d")
                cfgWrite("EFFECTIVE_TIME", effectiveTime)
                try:
                    f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'effectiveTime.txt', 'w', encoding="UTF-8")
                    f.write(effectiveTime)
                finally:
                    f.close()
                return effectiveTime
        else:
            True


def ALLancestorsFromConIdToET(inId, latestOrSnap): #latestOrSnap: 0=latest date, 1=snapshotdate
    allAncestorsEver = []
    ET = "20020131"
    CON_ID = inId
    u = 1
    if latestOrSnap == 1:
        endET = str(cfg("EFFECTIVE_TIME"))
    else:
        endET = str(time.strftime("%Y%m%d"))
    # identify all ancestors up to endET
    while ET <= endET:
        clearScreen()
        snapshotHeader()
        print('Calculating ancestors: '+ '*' * u)
        print(ET[:4])
        ancestorList = SV_SIMFindAncestorsForConIdAndET2(CON_ID, ET)
        for node in ancestorList:
            allAncestorsEver.append(node)
        ET = upByNMonths(ET, 6)
        u += 1
    uniqueAllAncestorsEver = list(set(allAncestorsEver))
    uniqueAllAncestorsEver.sort(key=lambda x: x[1].lower())
    return uniqueAllAncestorsEver


def ALLdescendantsFromConIdToET(inId, latestOrSnap, fullSweepBool): #latestOrSnap: 0=latest date, 1=snapshotdate
    allDescendantsEver = []
    ET = checkFullSweepBool(fullSweepBool)
    CON_ID = inId
    u = 1
    if latestOrSnap == 1:
        endET = str(cfg("EFFECTIVE_TIME"))
    else:
        endET = str(time.strftime("%Y%m%d"))
    # identify all ancestors up to endET
    totalList = []
    while ET <= endET:
        clearScreen()
        snapshotHeader()
        print('Calculating descendants: '+ '*' * u)
        if len(totalList) > 0:
            print("\nDate: \tCount:")
            for row in totalList:
                print(row[0] + '\t' + row[1])
        descendantList = SV_SIMdescendantsFromConIdWithET_IN(CON_ID, ET)
        totalList.append([ET[:6], str(len(descendantList))])
        for node in descendantList:
            allDescendantsEver.append(node)
        ET = upByNMonths(ET, 6)
        u += 1
    uniqueAllDescendantsEver = list(set(allDescendantsEver))
    uniqueAllDescendantsEver.sort(key=lambda x: x[1].lower())
    return uniqueAllDescendantsEver

def checkFullSweepBool(inBool):
    if inBool:
        return "20020131"
    else:
        return downByNMonths(time.strftime("%Y%m%d"), 24)

def ALLdescendantsToXLSX(inId, RefSetFlag, fullSweepBool, proceedBool):
    if inId == tok('SIMPLEREFSET') or SV_SIMcheckIsARefset(inId, True):
        RefSetFlag = 1
    else:
        RefSetFlag = 0
    mydict = {}
    CON_ID = inId
    uniqueAllDescendantsEver = ALLdescendantsFromConIdToET(CON_ID, 0, fullSweepBool)
    if proceedBool:
        topLine = []
        matrix = []
        topLine.append(" ")
        topLine.append("Descendants of " + CON_ID + '\n' + toString(textwrap.wrap(pterm(CON_ID), 50)))
        ET = checkFullSweepBool(fullSweepBool)
        while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
            topLine.append(ET)
            ET = upByNMonths(ET, 6)
        u = len(uniqueAllDescendantsEver)
        total = len(uniqueAllDescendantsEver)
        if total > 250:
            showBool = False
        else:
            showBool = True
        for thing in uniqueAllDescendantsEver:
            tempLine = []
            ET = checkFullSweepBool(fullSweepBool)
            clearScreen()
            snapshotHeader()
            if u > 1:
                print('Processing descendants: '+ '*' * int((u - 1) * (60 / total)))
                print(thing[0], thing[1])
            else:
                print('Completed.')
            tempLine.append(thing[0])
            tempLine.append(thing[1])
            while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
                character = SV_SIMC1_IsA_C2_Compare(thing[0], CON_ID, 1, ET, "desc", showBool, True)
                ############
                if RefSetFlag == 1:
                    if character == '+' or character == "#":
                        rsType = refSetTypeFromConId_IN(thing[0])[0][0]
                        myMemberCount = SV_SIMcheckRefsetMemberCount(thing[0], rsType, ET)
                        if myMemberCount > 0:
                            tempLine.append(str(myMemberCount))
                        else:
                            tempLine.append(character)
                    else:
                        tempLine.append(character)
                ############
                else:
                    tempLine.append(character)
                ET = upByNMonths(ET, 6)
            if CON_ID != thing[0]:
                matrix.append(tempLine)
            u -= 1
        xlsxFile(cfgPathInstall() + 'files' + sep() + 'xlsx' + sep(), 'descendants', CON_ID, topLine, matrix)
    else:
        secondaction = input2_3('\nPress any key to continue...')
        if secondaction:
            pass
        clearScreen()
        snapshotHeader()
    mydict = browseFromId(tuplify(inId))
    return mydict


def ALLancestorsToXLSX(inId):
    mydict = {}
    parentDict = {}
    CON_ID = inId
    uniqueAllAncestorsEver = ALLancestorsFromConIdToET(CON_ID, 0)
    topLine = []
    matrix = []
    topLine.append(" ")
    topLine.append("Ancestors of " + CON_ID + '\n' + toString(textwrap.wrap(pterm(CON_ID), 50)))
    ET = "20020131"
    while ET <= time.strftime("%Y%m%d"):
        topLine.append(ET)
        ET = upByNMonths(ET, 6)
    u = len(uniqueAllAncestorsEver)
    total = len(uniqueAllAncestorsEver)
    if total > 250:
        showBool = False
    else:
        showBool = True
    for thing in uniqueAllAncestorsEver:
        tempLine = []
        ET = "20020131"
        clearScreen()
        snapshotHeader()
        if u > 1:
            print('Processing ancestors: '+ '*' * int((u - 1) * (60 / total)))
            print(thing[0], thing[1])
        else:
            print('Completed.')
        tempLine.append(thing[0])
        tempLine.append(thing[1])
        while ET <= time.strftime("%Y%m%d"):
            character = SV_SIMC1_IsA_C2_Compare(CON_ID, thing[0], 2, ET, "ances", showBool, True)
            if character in "=~":
                if thing[0] not in parentDict:
                    parentDict[thing[0]] = [ET]
                else:
                    parentDict[thing[0]].append(ET)
            tempLine.append(character)
            ET = upByNMonths(ET, 6)
        if (CON_ID != thing[0]) and (thing[0] != tok("ROOT")):
            matrix.append(tempLine)
        u -= 1
    xlsxFile(cfgPathInstall() + 'files' + sep() + 'xlsx' + sep(), 'ancestors', CON_ID, topLine, matrix)
    mydict = browseFromId(tuplify(inId))
    return mydict


def ALLdescendantsToD3(inId):
    if not inId == tok('ROOT'):
        # generate rows
        ET = "20020131"
        writeLines = []
        descendantEdgesOld = []
        writeableIdsOld = []
        writeableIdIds = []
        differenceNodes = []
        otherwiseActiveNodes = []
        otherwiseActiveIds = []
        inactiveNodes = []
        inactiveIds = []
        dateList = []
        dateTableList = []
        uniqueAllDescendantsEver = ALLdescendantsFromConIdToET(inId, 0, True)
        clearScreen()
        snapshotHeader()
        print("Processing descendants")
        dateIndex = 0
        tableCounter = 0
        while ET <= time.strftime("%Y%m%d"):
            if ET[4:] == "0131":
                print(ET[:4])
            inactiveEdges = []
            inactiveEdgesIn = []
            inactiveEdgesOut = []
            otherwiseActiveEdges = []
            # calculate active descendants and active ediges
            descendantEdges, writeableNodes, writeableIds, historyEdges = dotDrawDescendants(inId, "D3", ET)
            writeableIdIds = [node[0] for node in writeableIds]
            # find old nodes not in new writeableNodes -> difsferenceNodes
            differenceNodes = [node for node in writeableIdsOld if node not in writeableIds]
            # find inactive nodes in difference nodes -> inactiveNodes
            # otherwise -> prevStill active nodes
            for node in differenceNodes:
                checkStatus = SV_SIMconMetaFromConIdSimple_IN(node[0], ET, False)
                # if inactive, add to inactiveNodes
                if checkStatus[0][1] == 0:
                    inactiveIds.append(node)
                else:
                    otherwiseActiveIds.append(node)
            # check whether focus is active (for reanimated focuses)
            tempConMeta = SV_SIMconMetaFromConIdSimple_IN(inId, ET, False)
            if tempConMeta != []:
                if tempConMeta[0][1] == 0:
                    append_distinct(inactiveIds, (inId, pterm(inId)))
            # check wether any prev still actives are now inactive; if so, move to inactives
            removeList = []
            for otherwiseActiveId in otherwiseActiveIds:
                checkStatus = SV_SIMconMetaFromConIdSimple_IN(otherwiseActiveId[0], ET, False)
                # if inactive, add to inactiveNodes and remove from otherwiseActive
                if checkStatus[0][1] == 0:
                    inactiveIds.append(otherwiseActiveId)
                    # addNodeForD3(inactiveNodes, otherwiseActiveId, "shape=box, peripheries=1, style=filled, fillcolor=palegreen", ET)
                    removeList.append(otherwiseActiveId)
            otherwiseActiveIds = removeMembers(otherwiseActiveIds, removeList)
            # check whether any prev still actives are now descendants; if so remove from otherwiseActive
            removeList = []
            for otherwiseActiveId in otherwiseActiveIds:
                if otherwiseActiveId in writeableIds:
                    removeList.append(otherwiseActiveId)
            otherwiseActiveIds = removeMembers(otherwiseActiveIds, removeList)
            # check wiether any inactives are now active (and descendants again); if so remove from inactives
            removeList = []
            for inactiveId in inactiveIds:
                if inactiveId in writeableIds:
                    removeList.append(inactiveId)
            inactiveIds = removeMembers(inactiveIds, removeList)
            # process uniqueAllAncestorsEver (looking for sometime ancestors that are active at thi ET)
            otherwiseActiveIds = trimAllRelativesEver(uniqueAllDescendantsEver, otherwiseActiveIds, writeableIdIds, ET)
            # create renderable nodes from ids for otherwiseActives
            otherwiseActiveNodes = []
            for otherwiseActiveId in otherwiseActiveIds:
                if SV_SIMC1_IsA_C2_Compare(otherwiseActiveId[0], tok("NAV_CON"), 1, ET, "ances", False, False) == '.':
                    otherwiseActiveNodes = addNodeForD3(otherwiseActiveNodes, otherwiseActiveId, "shape=box, peripheries=1, style=filled, fillcolor=lightsalmon", ET)
                else:
                    otherwiseActiveNodes = addNodeForD3(otherwiseActiveNodes, otherwiseActiveId, "shape=box, peripheries=1, style=filled, fillcolor=lightsalmon, fontcolor=white", ET)
            # create renderable nodes from ids for inactives
            inactiveNodes = []
            for inactiveId in inactiveIds:
                inactiveNodes = addNodeForD3(inactiveNodes, inactiveId, "shape=box, peripheries=1, style=filled, fillcolor=palegreen", ET)
            # find any history targets for inactiveNodes that are in new writeableNodes
            for nodeId in inactiveIds:
                historyTargets = SV_SIMHistoricalRefsetRowsfromId(nodeId[0], "cRefset", 1, 1, ET)
                # create inactiveEdges
                if historyTargets == []:
                    inactiveEdges.append([nodeId[0], tok('ROOT')])
                else:
                    for historyTarget in historyTargets:
                        if historyTarget[4] in writeableIdIds:
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0], historyTarget[4]])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
                        elif historyTarget[4] in [otherwiseActiveId[0] for otherwiseActiveId in otherwiseActiveIds]:
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0], historyTarget[4]])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
                        elif historyTarget[4] in [inactiveId[0] for inactiveId in inactiveIds]:
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0], historyTarget[4]])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
                        else:
                            # inactiveEdges.append([nodeId[0], tok('ROOT')])
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + tok('ROOT') + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + tok('ROOT') + "|" + historyTarget[0], tok('ROOT')])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + tok('ROOT') + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
            # create otherwiseActive edges
            # 'horizontal'
            # for nodeId in otherwiseActiveIds:
                # otherwiseActiveEdges.append([nodeId[0], tok('ROOT')])
            # 'vertical'
            if not otherwiseActiveIds == []:
                otherwiseActiveIds.sort(key=lambda x: x[1])
                otherwiseActiveEdges.append([otherwiseActiveIds[0][0], tok('ROOT')])
                for i in range(0, len(otherwiseActiveIds) - 1):
                    if i < 2:
                        otherwiseActiveEdges.append([otherwiseActiveIds[i + 1][0], otherwiseActiveIds[i][0]])
                    else:
                        otherwiseActiveEdges.append([otherwiseActiveIds[i + 1][0], otherwiseActiveIds[i - 1][0]])
                        otherwiseActiveEdges.append([otherwiseActiveIds[i + 1][0], otherwiseActiveIds[i - 2][0]])
            # print(str(ET))
            if ((descendantEdges != []) and (writeableNodes != [])) and (descendantEdges != descendantEdgesOld):
                dateList.append([dateIndex, tableCounter])
                dateIndex += 1
                writeableIdsOld = writeableIds
                descendantEdgesOld = descendantEdges
                writeLines.append("\t[\n")
                if cfg("GRAPH_ANGLE") == 1: # use angled edges
                    splineString = "; splines=ortho"
                else:
                    splineString = ""
                writeLines.append("'strict digraph G { rankdir=RL; ranksep=.3" + splineString + "; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white]',\n")
                # write date node
                writeLines.append("'\"" + inId + "\"->\"" + tok('ROOT') + "\" [arrowhead=onormal, style=dotted]',\n")
                writeLines.append("'\"" + tok('ROOT') + "\"->\"" + ET + "\" [style=invis]',\n")
                # write live edges
                for edge in sorted_cfg(descendantEdges):
                    writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=onormal]',\n")
                if cfg('D3_EXTRAS') > 0:
                    # write historical edges
                    for edge in sorted_cfg(inactiveEdges):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=green]',\n")
                    for edge in sorted_cfg(inactiveEdgesIn):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=none, color=green]',\n")
                    for edge in sorted_cfg(inactiveEdgesOut):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=green]',\n")
                    for edge in sorted_cfg(historyEdges):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [arrowhead=none, color=green]',\n")
                        writeLines.append("'\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\"->\"" + edge[1] + "\" [arrowhead=onormal, color=green]',\n")
                        writeableNodes.append("'\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + simplify(pterm(edge[2]).replace(" association reference set", "").upper()) + "\"]',\n")
                if cfg('D3_EXTRAS') > 1:
                    # write previous descendant active edges
                    for edge in sorted_cfg(otherwiseActiveEdges):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [style=invis]',\n")
                # attach date node
                writeLines.append("'\"" + ET + "\" [label=\"" + ET + "\", shape=box, fontsize=11, peripheries=2, style=filled, fillcolor=pink]',\n")
                writeLines.append("'\"" + tok('ROOT') + "\" [label=\"" + tok('ROOT') + "\\n" + pterm(tok('ROOT')) + "\", shape=box, peripheries=1, style=filled, fillcolor=\"#99CCFF\"]',\n")
                # write active descendant nodes
                for node in sorted_cfg(writeableNodes):
                    writeLines.append(node)
                if cfg('D3_EXTRAS') > 0:
                    # write historical nodes
                    for node in sorted_cfg(inactiveNodes):
                        writeLines.append(node)
                if cfg('D3_EXTRAS') > 1:
                    # write previous descendant active nodes
                    for node in sorted_cfg(otherwiseActiveNodes):
                        writeLines.append(node)
                writeLines.append("'}'")
                writeLines.append("\n\t],\n")
            tableCounter += 1
            dateTableList.append([tableCounter, ET])
            ET = upByNMonths(ET, 6)
        # write out rows
        with io.open(cfgPathInstall() + "files/d3/descendant-cycle.html", "w", encoding="utf-8") as fout:
            # first part of header
            readbackList = []
            with io.open(cfgPathInstall() + "files/d3/anc_desc-opening_pt1.txt", "r", encoding="utf-8") as fheader:
                for line in fheader:
                    readbackList.append(line)
            for line in readbackList:
                fout.write(line)
            # date table
            for line in dateTableListToHTML(dateTableList):
                fout.write(line)
            # second part of header
            readbackList = []
            with io.open(cfgPathInstall() + "files/d3/anc_desc-opening_pt2.txt", "r", encoding="utf-8") as fheader:
                for line in fheader:
                    readbackList.append(line)
            for line in readbackList:
                fout.write(line)
            # used date sequence rows
            for line in dateListToJSVar(dateList):
                fout.write(line)
            # main data rows
            fout.write("\n\nvar dots = [\n")
            for line in writeLines:
                fout.write(line)
            fout.write("];\n</script>\n</body>\n")
            clearScreen()
        # if running on linux vbox client and want to write to host shared folder
        if cfg("WIN_HOST_FILES") == 1:
            copyDescendantsD3()

def ALLancestorsToD3(inId):
    if not inId == tok('ROOT'):
        # generate rows
        ET = "20020131"
        writeLines = []
        ancestorEdgesOld = []
        writeableIdsOld = []
        writeableIdIds = []
        differenceNodes = []
        otherwiseActiveNodes = []
        otherwiseActiveIds = []
        inactiveNodes = []
        inactiveIds = []
        dateList = []
        dateTableList = []
        uniqueAllAncestorsEver = ALLancestorsFromConIdToET(inId, 0)
        clearScreen()
        snapshotHeader()
        print("Processing ancestors")
        dateIndex = 0
        tableCounter = 0
        while ET <= time.strftime("%Y%m%d"):
            if ET[4:] == "0131":
                print(ET[:4])
            inactiveEdges = []
            inactiveEdgesIn = []
            inactiveEdgesOut = []
            otherwiseActiveEdges = []
            # calculate active ancestors and active ediges
            ancestorEdges, writeableNodes, writeableIds, historyEdges = dotDrawAncestors(inId, "D3", ET)
            writeableIdIds = [node[0] for node in writeableIds]
            # find old nodes not in new writeableNodes -> differenceNodes
            differenceNodes = [node for node in writeableIdsOld if node not in writeableIds]
            # find inactive nodes in difference nodes -> inactiveNodes
            # otherwise -> prevStill active nodes
            for node in differenceNodes:
                checkStatus = SV_SIMconMetaFromConIdSimple_IN(node[0], ET, False)
                # if inactive, add to inactiveNodes
                if checkStatus[0][1] == 0:
                    inactiveIds.append(node)
                else:
                    otherwiseActiveIds.append(node)
            # check whether focus is active (for reanimated focuses)
            tempConMeta = SV_SIMconMetaFromConIdSimple_IN(inId, ET, False)
            if tempConMeta != []:
                if tempConMeta[0][1] == 0:
                    append_distinct(inactiveIds, (inId, pterm(inId)))
            # check wether any prev still actives are now inactive; if so, move to inactives
            removeList = []
            for otherwiseActiveId in otherwiseActiveIds:
                checkStatus = SV_SIMconMetaFromConIdSimple_IN(otherwiseActiveId[0], ET, False)
                # if inactive, add to inactiveNodes and remove from otherwiseActive
                if checkStatus[0][1] == 0:
                    inactiveIds.append(otherwiseActiveId)
                    # addNodeForD3(inactiveNodes, otherwiseActiveId, "shape=box, peripheries=1, style=filled, fillcolor=palegreen", ET)
                    removeList.append(otherwiseActiveId)
            otherwiseActiveIds = removeMembers(otherwiseActiveIds, removeList)
            # check whether any prev still actives are now ancestors; if so remove from otherwiseActive
            removeList = []
            for otherwiseActiveId in otherwiseActiveIds:
                if otherwiseActiveId in writeableIds:
                    removeList.append(otherwiseActiveId)
            otherwiseActiveIds = removeMembers(otherwiseActiveIds, removeList)
            # check wiether any inactives are now active (and ancestors again); if so remove from inactives
            removeList = []
            for inactiveId in inactiveIds:
                if inactiveId in writeableIds:
                    removeList.append(inactiveId)
            inactiveIds = removeMembers(inactiveIds, removeList)
            # process uniqueAllAncestorsEver (looking for sometime ancestors that are active at thi ET)
            otherwiseActiveIds = trimAllRelativesEver(uniqueAllAncestorsEver, otherwiseActiveIds, writeableIdIds, ET)
            # create renderable nodes from ids for otherwiseActives
            otherwiseActiveNodes = []
            for otherwiseActiveId in otherwiseActiveIds:
                if SV_SIMC1_IsA_C2_Compare(otherwiseActiveId[0], tok("NAV_CON"), 1, ET, "ances", False, False) == '.':
                    otherwiseActiveNodes = addNodeForD3(otherwiseActiveNodes, otherwiseActiveId, "shape=box, peripheries=1, style=filled, fillcolor=lightsalmon", ET)
                else:
                    otherwiseActiveNodes = addNodeForD3(otherwiseActiveNodes, otherwiseActiveId, "shape=box, peripheries=1, style=filled, fillcolor=lightsalmon, fontcolor=white", ET)
            # create renderable nodes from ids for inactives
            inactiveNodes = []
            for inactiveId in inactiveIds:
                inactiveNodes = addNodeForD3(inactiveNodes, inactiveId, "shape=box, peripheries=1, style=filled, fillcolor=palegreen", ET)
            # find any history targets for inactiveNodes that are in new writeableNodes
            for nodeId in inactiveIds:
                historyTargets = SV_SIMHistoricalRefsetRowsfromId(nodeId[0], "cRefset", 1, 1, ET)
                # create inactiveEdges
                if historyTargets == []:
                    inactiveEdges.append([nodeId[0], tok('ROOT')])
                else:
                    for historyTarget in historyTargets:
                        if historyTarget[4] in writeableIdIds:
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0], historyTarget[4]])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
                        elif historyTarget[4] in [otherwiseActiveId[0] for otherwiseActiveId in otherwiseActiveIds]:
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0], historyTarget[4]])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
                        elif historyTarget[4] in [inactiveId[0] for inactiveId in inactiveIds]:
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0], historyTarget[4]])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + historyTarget[4] + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
                        else:
                            inactiveEdgesIn.append([nodeId[0], nodeId[0] + "|" + tok('ROOT') + "|" + historyTarget[0]])
                            inactiveEdgesOut.append([nodeId[0] + "|" + tok('ROOT') + "|" + historyTarget[0], tok('ROOT')])
                            writeableNodes.append("'\"" + nodeId[0] + "|" + tok('ROOT') + "|" + historyTarget[0] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + historyTarget[1].replace(" association reference set", "").upper() + "\"]',\n")
            # create otherwiseActive edges
            # 'horizontal'
            # for nodeId in otherwiseActiveIds:
                # otherwiseActiveEdges.append([nodeId[0], tok('ROOT')])
            # 'vertical'
            if not otherwiseActiveIds == []:
                otherwiseActiveIds.sort(key=lambda x: x[1])
                otherwiseActiveEdges.append([otherwiseActiveIds[0][0], tok('ROOT')])
                for i in range(0, len(otherwiseActiveIds) - 1):
                    if i < 2:
                        otherwiseActiveEdges.append([otherwiseActiveIds[i + 1][0], otherwiseActiveIds[i][0]])
                    else:
                        otherwiseActiveEdges.append([otherwiseActiveIds[i + 1][0], otherwiseActiveIds[i - 1][0]])
                        otherwiseActiveEdges.append([otherwiseActiveIds[i + 1][0], otherwiseActiveIds[i - 2][0]])
            if ((ancestorEdges != []) and (writeableNodes != [])) and (ancestorEdges != ancestorEdgesOld):
                dateList.append([dateIndex, tableCounter])
                dateIndex += 1
                writeableIdsOld = writeableIds
                ancestorEdgesOld = ancestorEdges
                writeLines.append("\t[\n")
                if cfg("GRAPH_ANGLE") == 1: # use angled edges
                    splineString = "; splines=ortho"
                else:
                    splineString = ""
                writeLines.append("'strict digraph G { rankdir=BT; ranksep=.3" + splineString + "; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white]',\n")
                # write date node
                writeLines.append("'\"" + tok('ROOT') + "\"->\"" + ET + "\" [style=invis]',\n")
                # write live edges
                for edge in sorted_cfg(ancestorEdges):
                    writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=onormal]',\n")
                if cfg('D3_EXTRAS') > 0:
                    # write historical edges
                    for edge in sorted_cfg(inactiveEdges):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=green]',\n")
                    for edge in sorted_cfg(inactiveEdgesIn):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=none, color=green]',\n")
                    for edge in sorted_cfg(inactiveEdgesOut):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=green]',\n")
                    for edge in sorted_cfg(historyEdges):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [arrowhead=none, color=green]',\n")
                        writeLines.append("'\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\"->\"" + edge[1] + "\" [arrowhead=onormal, color=green]',\n")
                        writeableNodes.append("'\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + simplify(pterm(edge[2]).replace(" association reference set", "").upper()) + "\"]',\n")
                if cfg('D3_EXTRAS') > 1:
                    # write previous ancestor active edges
                    for edge in sorted_cfg(otherwiseActiveEdges):
                        writeLines.append("'\"" + edge[0] + "\"->\"" + edge[1] + "\" [style=invis]',\n")
                # attach date node
                writeLines.append("'\"" + ET + "\" [label=\"" + ET + "\", shape=box, fontsize=11, peripheries=2, style=filled, fillcolor=pink]',\n")
                # write active ancestor nodes
                for node in sorted_cfg(writeableNodes):
                    writeLines.append(node)
                if cfg('D3_EXTRAS') > 0:
                    # write historical nodes
                    for node in sorted_cfg(inactiveNodes):
                        writeLines.append(node)
                if cfg('D3_EXTRAS') > 1:
                    # write previous ancestor active nodes
                    for node in sorted_cfg(otherwiseActiveNodes):
                        writeLines.append(node)
                writeLines.append("'}'")
                writeLines.append("\n\t],\n")
            tableCounter += 1
            dateTableList.append([tableCounter, ET])
            ET = upByNMonths(ET, 6)
        # write out rows
        with io.open(cfgPathInstall() + "files/d3/ancestor-cycle.html", "w", encoding="utf-8") as fout:
            # first part of header
            readbackList = []
            with io.open(cfgPathInstall() + "files/d3/anc_desc-opening_pt1.txt", "r", encoding="utf-8") as fheader:
                for line in fheader:
                    readbackList.append(line)
            for line in readbackList:
                fout.write(line)
            # date table
            for line in dateTableListToHTML(dateTableList):
                fout.write(line)
            # second part of header
            readbackList = []
            with io.open(cfgPathInstall() + "files/d3/anc_desc-opening_pt2.txt", "r", encoding="utf-8") as fheader:
                for line in fheader:
                    readbackList.append(line)
            for line in readbackList:
                fout.write(line)
            # used date sequence rows
            for line in dateListToJSVar(dateList):
                fout.write(line)
            # main data rows
            fout.write("\n\nvar dots = [\n")
            for line in writeLines:
                fout.write(line)
            fout.write("];\n</script>\n</body>\n")
            clearScreen()
        # if running on linux vbox client and want to write to host shared folder
        if cfg("WIN_HOST_FILES") == 1:
            copyAncestorsD3()

def dateListToJSVar(inList):
    outList = []
    outList.append('\nvar dates = {\n')
    for line in inList[:-1]:
        outList.append('\t' + str(line[0]) + ': ' + str(line[1]) + ',\n')
    outList.append('\t' + str(inList[-1][0]) + ': ' + str(inList[-1][1]) + '};\n\n')
    return outList

def dateTableListToHTML(inList):
    outList = []
    outList.append('\n<table class="date-table"><tbody>\n')
    for line in inList[:-1]: # omit last line
        outList.append('<tr id="' + str(line[0]) + '"><td>' + line[1][:4] + ' ' + line[1][4:6] + ' ' + line[1][6:] + '</td></tr>\n')
    outList.append('</tbody></table>\n')
    return  outList

def trimAllRelativesEver(uniqueAllRelativesEver, otherwiseActiveIds, writeableIdIds, ET):
    for relative in uniqueAllRelativesEver:
        checkStatus = SV_SIMconMetaFromConIdSimple_IN(relative[0], ET, False)
        if len(checkStatus) > 0:
            if checkStatus[0][1] == 1:
                if (relative[0] not in writeableIdIds) and (relative not in otherwiseActiveIds):
                    otherwiseActiveIds.append(relative)
    return otherwiseActiveIds


def ALLfocusToD3(inId):
    if not inId == tok('ROOT'):
        # fill dots sections
        ET = "20020131"
        tempEffTime = cfg("EFFECTIVE_TIME")
        writeLines = []
        dotEdgeList = []
        bunchNodes = []
        groupNodes = []
        attNameList = []
        objValueList = []
        dotEdgeListOld = []
        dotEdgeListOld.append("dummy1")
        dotEdgeListOld.append("dummy2")
        dateList = []
        dateTableList = []
        clearScreen()
        snapshotHeader()
        print("Processing focus definitions")
        dateIndex = 0
        tableCounter = 0
        while ET <= time.strftime("%Y%m%d"):
            if ET[4:] == "0131":
                print(ET[:4])
            if cfg("QUERY_TYPE") == 1:
                conMeta = SV_ALLconMetaFromConId_IN(inId)
            elif cfg("QUERY_TYPE") == 2:
                conMeta = SV_SIMconMetaFromConId_IN(inId, ET)
            else:
                cfgWrite("EFFECTIVE_TIME", ET)
                conMeta = SV_MDRconMetaFromConId_IN(inId, effTimeSet())
            if len(conMeta) > 0:
                if conMeta[0][1] == 1:
                    writePreDotFile(tuplify(inId), ET)
                    dotEdgeList, bunchNodes, groupNodes, attNameList, objValueList = dotDrawFocus("D3", ET)
                    if dotEdgeList[1:] != dotEdgeListOld[1:]:
                        dateList.append([dateIndex, tableCounter])
                        dateIndex += 1
                        dotEdgeListOld = dotEdgeList
                        writeLines.append("\t[\n")
                        if (cfg("SHOW_SUPERS") == 2 or cfg("SHOW_SUPERS") == 3):
                            writeLines.append("'strict digraph G { rankdir=LR; ranksep=.3; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white]',\n")
                        else:
                            writeLines.append("'strict digraph G { rankdir=LR; ranksep=.3; splines=ortho; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white]',\n")
                        for item in dotEdgeList:
                            writeLines.append("'" + item.replace("\l", "\\n") + "',\n")
                        for item in  sorted(bunchNodes):
                            writeLines.append("'" + item.replace("\l", "\\n") + "',\n")
                        for item in sorted_cfg(groupNodes):
                            writeLines.append("'" + item.replace("\l", "\n") + "',\n")
                        for item in sorted_cfg(attNameList):
                            writeLines.append("'" + item.replace("\l", "\\n") + "',\n")
                        for item in sorted_cfg(objValueList):
                            writeLines.append("'" + item.replace("\l", "\\n") + "',\n")
                        writeLines.append("'}'")
                        writeLines.append("\n\t],\n")
            tableCounter += 1
            dateTableList.append([tableCounter, ET])
            ET = upByNMonths(ET, 6)
        with io.open(cfgPathInstall() + "files/d3/focus-cycle.html", "w", encoding="utf-8") as fout:
            readbackList = []
            with io.open(cfgPathInstall() + "files/d3/anc_desc-opening_pt1.txt", "r", encoding="utf-8") as fheader:
                for line in fheader:
                    readbackList.append(line)
            for line in readbackList:
                fout.write(line)
            # date table
            for line in dateTableListToHTML(dateTableList):
                fout.write(line)
            # second part of header
            readbackList = []
            with io.open(cfgPathInstall() + "files/d3/anc_desc-opening_pt2.txt", "r", encoding="utf-8") as fheader:
                for line in fheader:
                    readbackList.append(line)
            for line in readbackList:
                fout.write(line)
            # used date sequence rows
            for line in dateListToJSVar(dateList):
                fout.write(line)
            # write opening 'var dots = [' to output file
            fout.write("\n\nvar dots = [\n")
            for line in writeLines:
                fout.write(line)
            fout.write("];\n</script>\n</body>\n")
        cfgWrite("EFFECTIVE_TIME", tempEffTime)
        clearScreen()
        snapshotHeader()
        # if running on linux vbox client and want to write to host shared folder
        if cfg("WIN_HOST_FILES") == 1:
            copyFocusD3()

def removeMembers(inList, inRemoveList):
    for item in inRemoveList:
        inList.remove(item)
    return inList


def dotDrawAncestorsCall(inId):
    ancestorEdges, writeableNodes, historyEdges = dotDrawAncestors(inId, "xdot", cfg("EFFECTIVE_TIME"))
    if SV_SIMconMetaFromConIdSimple_IN(inId, cfg("EFFECTIVE_TIME"), False)[0][1] == 0:
        writeableNodes = addNodeForxdot(writeableNodes, [inId, pterm(inId)], "shape=box, peripheries=1, style=filled, fillcolor=palegreen", cfg("EFFECTIVE_TIME"))
    try:
        if cfg("GRAPH_ANGLE") == 1: # use angled edges
            splineString = "; splines=ortho"
        else:
            splineString = ""
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc.dot', 'w', encoding="UTF-8")
        f.write("strict digraph G { rankdir=BT; ranksep=.3" + splineString + "; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        f.write("\"" + tok('ROOT') + "\"->\"" + str(cfg("EFFECTIVE_TIME")) + "\" [style=invis];\n")
        for edge in sorted_cfg(ancestorEdges):
            f.write("\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=onormal];\n")
        for edge in sorted_cfg(historyEdges):
            f.write("\"" + edge[0] + "\"->\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [arrowhead=none, color=palegreen];\n")
            f.write("\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\"->\"" + edge[1] + "\" [arrowhead=onormal, color=palegreen];\n")
            writeableNodes.append("\"" + edge[0] + "|" + edge[1] + "|" + edge[2] + "\" [style=\"rounded,filled\", fillcolor=palegreen, color=palegreen, font=helvetica, fontsize=5, height=0, label=\"" + simplify(pterm(edge[2]).replace(" association reference set", "").upper()) + "\"];\n")
        f.write("\"" + str(cfg("EFFECTIVE_TIME")) + "\" [label=\"" + str(cfg("EFFECTIVE_TIME")) + "\", shape=box, peripheries=2, style=filled, fillcolor=pink];\n")
        for node in sorted_cfg(writeableNodes):
            f.write(node)
        f.write("}")
    finally:
        f.close()


def dotDrawDescendantsCall(inId):
    descendantEdges, writeableNodes, historyEdges = dotDrawDescendants(inId, "xdot", cfg("EFFECTIVE_TIME"))
    if SV_SIMconMetaFromConIdSimple_IN(inId, cfg("EFFECTIVE_TIME"), False)[0][1] == 0:
        writeableNodes = addNodeForxdot(writeableNodes, [inId, pterm(inId)], "shape=box, peripheries=1, style=filled, fillcolor=palegreen", cfg("EFFECTIVE_TIME"))
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'contc.dot', 'w', encoding="UTF-8")
        if cfg("GRAPH_ANGLE") == 1: # use angled edges
            splineString = "; splines=ortho"
        else:
            splineString = ""
        f.write("strict digraph G { rankdir=RL; ranksep=.3" + splineString + "; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        f.write("\"" + inId + "\"->\"" + tok('ROOT') + "\" [arrowhead=onormal, style=dotted];\n")
        f.write("\"" + tok('ROOT') + "\"->\"" + str(cfg("EFFECTIVE_TIME")) + "\" [style=invis];\n")
        for edge in sorted_cfg(descendantEdges):
            f.write("\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=onormal];\n")
        for edge in sorted_cfg(historyEdges):
            f.write("\"" + edge[0] + "\"->\"" + edge[1] + "\" [arrowhead=normal, color=palegreen];\n")
        f.write("\"" + str(cfg("EFFECTIVE_TIME")) + "\" [label=\"" + str(cfg("EFFECTIVE_TIME")) + "\", shape=box, peripheries=2, style=filled, fillcolor=pink];\n")
        f.write("\"" + tok('ROOT') + "\" [label=\"" + tok('ROOT') + "\\n" + pterm(tok('ROOT')) + "\", shape=box, peripheries=1, style=filled, fillcolor=\"#99CCFF\"];\n")
        for node in sorted_cfg(writeableNodes):
            f.write(node)
        f.write("}")
    finally:
        f.close()

def resolveHistoryTargets(historyEdges, ancestorNodes, testList, level, inET):
    # before/after setup
    historyEdgesIn = historyEdges[:]
    testListIn = testList[:]
    ancestorNodesIn = ancestorNodes[:]
    level += 1
    while level < 1000: # arbitrary end point
        for item in testList:
            historyTargets = SV_SIMHistoricalRefsetRowsfromId(item, "cRefset", 1, 1, inET)
            for historyTarget in historyTargets:
                append_distinct(historyEdges, (historyTarget[6], historyTarget[4], historyTarget[0]))
                append_distinct(ancestorNodes, (historyTarget[4], pterm(historyTarget[4])))
                append_distinct(ancestorNodes, (historyTarget[6], pterm(historyTarget[6])))
                ancestorNodesTemp = []
                conMetaList = SV_SIMconMetaFromConIdSimple_IN(historyTarget[4], inET, False)
                if conMetaList != []:
                    if conMetaList[0][1] == 1:
                        ancestorNodesTemp = SV_SIMancestorsFromConIdWithET_IN2(historyTarget[4], inET)
                        for ancNode in ancestorNodesTemp:
                            append_distinct(ancestorNodes, ancNode)
                else:
                    if historyTarget[4] not in testList:
                        append_distinct(testList, historyTarget[4])
                if SV_SIMconMetaFromConIdSimple_IN(historyTarget[6], inET, False)[0][1] == 1:
                    append_distinct(testList, historyTarget[6])
        # conditional end point
        if (historyEdgesIn == historyEdges) and (ancestorNodesIn == ancestorNodes) and (testListIn == testList):
            level = 1000
        # recursive call
        [historyEdges, ancestorNodes, testList, level, inET] = resolveHistoryTargets(historyEdges, ancestorNodes, testList, level, inET)
    # end return
    return [historyEdges, ancestorNodes, testList, level, inET]

def dotDrawAncestors(inId, inType, inET=time.strftime("%Y%m%d")):
    ancestorNodes = []
    ancestorEdges = []
    writeableNodes = []
    writeableIds = []
    historyEdges = []
    historyEdgesAndAncestorNodesList = []
    if SV_SIMconMetaFromConIdSimple_IN(inId, inET, False) == []:
        if inType == "D3":
            return [], [], [], []
        else:
            return [], [], []
    else:
        if SV_SIMconMetaFromConIdSimple_IN(inId, inET, False)[0][1] == 1:
            ancestorNodes = SV_SIMancestorsFromConIdWithET_IN2(inId, inET)
        sendToHistoryNodes = ancestorNodes[:]
        historyEdgesAndAncestorNodesList = resolveHistoryTargets(historyEdges, sendToHistoryNodes, [inId], 1, inET)
        historyEdges = historyEdgesAndAncestorNodesList[0]
        ancestorNodes = [*ancestorNodes, *historyEdgesAndAncestorNodesList[1]] # needs improvement
        for node in ancestorNodes:
            for parent in SV_SIMparentsFromIdPlusMeta_IN(tuplify(node[0]), cfg("SHOW_TYPE"), inET):
                ancestorEdges.append([node[0], parent[3]])
        for node in ancestorNodes:
            if inType == "D3":
                writeableNodes = addNodeForD3(writeableNodes, node, "TEST", inET)
                writeableIds.append(node)
            else:
                addNodeForxdot(writeableNodes, node, "TEST", inET)
        if inType == "D3":
            return ancestorEdges, writeableNodes, writeableIds, historyEdges
        else:
            return ancestorEdges, writeableNodes, historyEdges


def dotDrawDescendants(inId, inType, inET=time.strftime("%Y%m%d")):
    descendantNodes = []
    descendantEdges = []
    writeableNodes = []
    writeableIds = []
    historyTargets = []
    tempDescendantNodes = []
    historyEdges = []
    if SV_SIMconMetaFromConIdSimple_IN(inId, inET, False) == []:
        if inType == "D3":
            return [], [], [], []
        else:
            return [], [], []
    else:
        if SV_SIMconMetaFromConIdSimple_IN(inId, inET, False)[0][1] == 1:
            descendantNodes = SV_SIMdescendantsFromConIdWithET_IN(inId, inET)
        else:
            historyTargets = [item[4] for item in SV_SIMHistoricalRefsetRowsfromId(inId, "cRefset", 1, 1, inET)]
            for item in historyTargets:
                historyEdges.append([inId, item])
                tempDescendantNodes = SV_SIMdescendantsFromConIdWithET_IN(item, inET)
                for descendant in tempDescendantNodes:
                    if descendant not in descendantNodes:
                        descendantNodes.append(descendant)
        for node in descendantNodes:
            for child in SV_SIMchildrenFromIdPlusMeta_IN(tuplify(node[0]), cfg("SHOW_TYPE"), inET):
                descendantEdges.append([child[3], node[0]])
        for node in descendantNodes:
            if inType == "D3":
                writeableNodes = addNodeForD3(writeableNodes, node, "TEST", inET)
                writeableIds.append(node)
            else:
                addNodeForxdot(writeableNodes, node, "TEST", inET)
        if inType == "D3":
            return descendantEdges, writeableNodes, writeableIds, historyEdges
        else:
            return descendantEdges, writeableNodes, historyEdges

def addNodeForD3(inNodeList, inNode, inBoxDetails="TEST", inET=time.strftime("%Y%m%d")):
    if inBoxDetails == "TEST":
        inBoxDetails = checkObjValueColor(inNode[0].strip(), inET)
    inNodeList.append("'\"" + inNode[0] + "\" [label=\"" + inNode[0] + "\\n" + simplify(inNode[1]) + "\", " + inBoxDetails + "]',\n")
    inNodeList = list(set(inNodeList))
    return inNodeList


def addNodeForxdot(inNodeList, inNode, inBoxDetails="TEST", inET=time.strftime("%Y%m%d")):
    if inBoxDetails == "TEST":
        inBoxDetails = checkObjValueColor(inNode[0].strip(), inET)
    inNodeList.append("\"" + inNode[0] + "\" [label=\"" + inNode[0] + "\\n" + simplify(inNode[1]) + "\", " + inBoxDetails + "];\n")
    inNodeList = list(set(inNodeList))
    return inNodeList


def checkObjValueColor(inId, inET=time.strftime("%Y%m%d")):
    returnString = ""
    metaList = []
    metaList = SV_SIMconMetaFromConIdSimple_IN(inId, inET, False)[0]
    checkString3 = metaList[3]
    checkString1 = metaList[1]
    if checkString1 == 1:
        if checkString3 == tok("PRIMITIVE"):
            returnString = "shape=box, peripheries=1, style=filled, fillcolor=\"#99CCFF\""
        else:
            returnString = "shape=box, peripheries=2, style=filled, fillcolor=\"#CCCCFF\""
    else:
        returnString = "shape=box, peripheries=1, style=filled, fillcolor=palegreen"
    return returnString


def ALLmembersToXLSX(inId, fullSweepBool):
    mydict = {}
    ET = "20020131"
    CON_ID = inId
    if SV_SIMcheckIsARefset(CON_ID, True):
        memberList = []
        # find earliest release of refset
        if fullSweepBool:
            print('Checking earliest release...')
        else:
            print('Checking recent prior releases...')
        rsType = refSetTypeFromConId_IN(CON_ID)[0][0]
        min_ETSet = checkEarliestReleaseSimpleRefset(CON_ID, rsType)
        min_ET = min_ETSet[0]
        if min_ET is None:
            print("However, no members, ever...")
        else:
            # if there are any members...
            if fullSweepBool:
                ET_early = downByNMonths(min_ET, 6)
            else:
                ET_early = downByNMonths(time.strftime("%Y%m%d"), 24)
            print(ET_early[:4])
            print('Checking for all members since this time...')
            # ... now find all members of refset
            memberList = FindAllMembersOfRefset(CON_ID, rsType)
            memberCount = len(memberList)
            print(str(memberCount), 'members in total.')
            secondaction = input2_3('Continue [Y/N]:')
            if secondaction == 'y':
                memberList.sort(key=lambda x: x[1])
                # test each member against time and produce matrix
                topLine = []
                tempLine = []
                matrix = []
                firstTime = True
                topLine.append(" ")
                topLine.append("Members of " + CON_ID + '\n' + toString(textwrap.wrap(pterm(CON_ID), 50)))
                u = len(memberList)
                total = len(memberList)
                for thing in memberList:
                    ET = ET_early
                    clearScreen()
                    snapshotHeader()
                    if u > 1:
                        print('Processing members: '+ '*' * int((u - 1) * (60 / total)))
                        print(thing[0][0], thing[1])
                    else:
                        clearScreen()
                    tempLine = []
                    tempLine.append(thing[0][0])
                    tempLine.append(thing[1])
                    while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
                        if firstTime:
                            topLine.append(ET)
                        # check each member at each ET
                        mydata = SV_SIMCheckConceptAgainstRefset(CON_ID, str(thing[0][0]), rsType, ET)
                        if mydata: # is or was a member
                            if (mydata[0][1] == '1') or (mydata[0][1] == 1): # is a member
                                memberString = ""
                                metaList = []
                                metaList = SV_SIMconMetaFromConIdSimple_IN(str(thing[0][0]), ET, False)[0]
                                checkStringPrim = metaList[3]
                                checkStringAct = metaList[1]
                                if checkStringPrim == tok("PRIMITIVE") and checkStringAct == 1: # active primitive member
                                    memberString = "+"
                                elif checkStringPrim == tok("DEFINED") and checkStringAct == 1: # active defined member
                                    memberString = "#"
                                else: # inactive member
                                    memberString = "-"
                                tempLine.append(memberString)
                            else: # was a member
                                if len(SV_SIMconMetaFromConIdSimple_IN(str(thing[0][0]), ET, False)) == 1:
                                    if SV_SIMconMetaFromConIdSimple_IN(str(thing[0][0]), ET, False)[0][1] == 1:
                                        tempLine.append('.') # active, was a member
                                    else:
                                        tempLine.append('--') # inactive, was a member
                        else: # not yet a member
                            if len(SV_SIMconMetaFromConIdSimple_IN(str(thing[0][0]), ET, False)) == 1:
                                if SV_SIMconMetaFromConIdSimple_IN(str(thing[0][0]), ET, False)[0][1] == 1:
                                    tempLine.append('.') # active, not yet a member
                                else:
                                    tempLine.append('--') # inactive, not yet a member
                            else:
                                tempLine.append(' ') # doesn't exist yet.
                        ET = upByNMonths(ET, 6)
                    matrix.append(tempLine)
                    firstTime = False
                    u -= 1

                xlsxFile(cfgPathInstall() + 'files' + sep() + 'xlsx' + sep(), 'members', CON_ID, topLine, matrix)

    clearScreen()
    snapshotHeader()
    mydict = browseFromId(tuplify(inId))
    return mydict

def testTermTokens(thing, ET, tokList):
    myTerms = []
    myTerms = SV_SIMterms(thing, 1, ET, (RLRParts + otherLangSet))
    if not myTerms == []:
        matchTot = 0
        for tok in tokList:
            matchBool = False
            for termRow in myTerms:
                if tok in termRow[1][0][7] and termRow[1][0][2] == 1:
                    matchBool = True
            if matchBool:
                matchTot += 1
        if matchTot > 1:
            return True
        else:
            return False
    else:
        return False

def getTermTok():
    # currently fixed set
    return ["declin", "refus"]
    # will generate from /Data/read.txt file

def ALLcodesInSetToXLSX(inId, fullSweepBool, testTermTokBool):
    if testTermTokBool:
        tokList = getTermTok()
    CON_ID = tok("ROOT")
    setMembers = readFileMembers(cfgPathInstall() + "sets" + sep() + inId + ".txt")
    topLine = []
    matrix = []
    topLine.append(" ")
    topLine.append("Introduction dates of " + inId)
    ET = checkFullSweepBool(fullSweepBool)
    while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
        topLine.append(ET)
        ET = upByNMonths(ET, 6)
    u = len(setMembers)
    total = len(setMembers)
    if total > 250:
        showBool = False
    else:
        showBool = True
    for thing in setMembers:
        tempLine = []
        ET = checkFullSweepBool(fullSweepBool)
        clearScreen()
        snapshotHeader()
        if u > 1:
            print('Processing members: '+ '*' * int((u - 1) * (60 / total)))
            print(thing)
        else:
            print('Completed.')
        tempLine.append(thing)
        tempLine.append(pterm(thing))
        while ET < upByNMonths(time.strftime("%Y%m%d"), 6):
            character = SV_SIMC1_IsA_C2_Compare(thing, CON_ID, 1, ET, "desc", showBool, True)
            if testTermTokBool:
                if testTermTokens(thing, ET, tokList):
                    tempLine.append("@")
                else:
                    tempLine.append(character)
            else:
                tempLine.append(character)
            ET = upByNMonths(ET, 6)
        if CON_ID != thing[0]:
            matrix.append(tempLine)
        u -= 1
    xlsxFile(cfgPathInstall() + 'files' + sep() + 'xlsx' + sep(), 'setmembers', CON_ID, topLine, matrix)

def toString(inList):
    outString = ""
    if len(inList) == 1:
        outString =  inList[0]
    else:
        for item in inList[0:-1]:
            outString += item + "\n"
        outString += inList[-1]
    return outString

def refSetFromConIdPlusMeta(idIn):
    mydict = {}
    mycounter = 1
    refsetType = refsetList()

    mydict.update(list(zip(listify(mycounter), listify(tuplify(idIn)))))
    print("Refsets for:", SV_SIMactiveColor_IN(idIn, cfg("EFFECTIVE_TIME")), refsetPterm(idIn))
    print("([1] to return to selected concept)")
    print()

    if len(refsetType) > 0:
        for rType in refsetType:
            if cfg("QUERY_TYPE") == 1:
                refs = SV_SIMrefSetFromConIdPlusMeta_IN(idIn, rType, 3, cfg("EFFECTIVE_TIME"))
            if cfg("QUERY_TYPE") == 2:
                refs = SV_SIMrefSetFromConIdPlusMeta_IN(idIn, rType, cfg("SHOW_TYPE"), cfg("EFFECTIVE_TIME"))
            if cfg("QUERY_TYPE") == 3:
                refs = SV_MDRrefSetFromConIdPlusMeta_IN(idIn, rType, cfg('SHOW_TYPE'), effTimeSet())
            for ref in refs:
                mycounter += 1
                mydict.update(list(zip(listify(mycounter), listify(tuplify(ref[0])))))
                print('[' + str(mycounter) + ']\t' + ref[0] + tabString(ref[0]) + ref[1])
                if cfg('SHOW_METADATA') > 0:
                    print('\033[36m(Ref:Module=' + ref[3] + activeMetaColor(ref[5]) + ', Time=' + ref[4] + ')\033[92m')
    else:
        print("No refsets.")
    print()
    return mydict


def setQueryType(inInteger):
    cfgWrite("QUERY_TYPE", inInteger)
    if inInteger == 3:
        cfgWrite("SUB_SHOW", 0)
        cfgWrite("PTERM_TYPE", 0)
    if inInteger == 2 or inInteger == 1:
        cfgWrite("SUB_SHOW", 1)
    return inInteger


def setShowType(inInteger):
    cfgWrite("SHOW_TYPE", inInteger)
    return inInteger


def setShowMetadata(inInteger):
    cfgWrite("SHOW_METADATA", inInteger)
    return inInteger


def setD3Extras(inInteger):
    cfgWrite("D3_EXTRAS", inInteger)
    return inInteger


def setPretty(inInteger):
    cfgWrite("PRETTY", inInteger)
    return inInteger


def setPtermControl(inInteger):
    cfgWrite("PTERM_TYPE", inInteger)
    return inInteger


def setSubShow(inInteger):
    cfgWrite("SUB_SHOW", inInteger)
    return inInteger

def showSupers(inInteger):
    cfgWrite("SHOW_SUPERS", inInteger)
    return inInteger

def graphAngle(inInteger):
    cfgWrite("GRAPH_ANGLE", inInteger)
    return inInteger

def setAcrossDown(inString):
    cfgWrite("ACROSS_DOWN", inString)

def startNeoCall():
    startNeoPlatformCall()


def stopNeoCall():
    stopNeoPlatformCall()

# xls file
def xlsxFile(pathName, fileName, CON_ID, topLine, matrix):
    xbook = xlsxwriter.Workbook(pathName + fileName + '.xlsx')
    xsheet = xbook.add_worksheet(CON_ID)

    xformat1 = xbook.add_format()
    xformat1.set_rotation(90)
    xformat1.set_bg_color('#ffc0cb')
    xsheet.write_row(0, 0, topLine, xformat1)
    xformat6 = xbook.add_format()
    xformat6.set_rotation(0)
    xformat6.set_align('center')
    xformat6.set_align('vcenter')
    xformat6.set_bg_color('#ffc0cb')
    xsheet.write(0, 1, topLine[1], xformat6)
    xformat2 = xbook.add_format()
    xformat2.set_pattern(1)
    xformat2.set_bg_color('#a5fca5')
    xformat3 = xbook.add_format()
    xformat3.set_pattern(1)
    xformat3.set_bg_color('#f8a68b')
    xformat4 = xbook.add_format()
    xformat4.set_pattern(1)
    xformat4.set_bg_color('#99ccff')
    xformat5 = xbook.add_format()
    xformat5.set_pattern(1)
    xformat5.set_bg_color('#ccccff')
    xformat7 = xbook.add_format()
    xformat7.set_pattern(1)
    xformat7.set_bg_color('#ff0000')
    xformat8 = xbook.add_format()
    xformat8.set_pattern(1)
    xformat8.set_bg_color('#99ccff')
    xformat8.set_border(2)
    xformat8.set_border_color('#0000ff')
    xformat9 = xbook.add_format()
    xformat9.set_pattern(1)
    xformat9.set_bg_color('#ccccff')
    xformat9.set_border(2)
    xformat9.set_border_color('#0000ff')

    for n in range(2, len(topLine)):
        xsheet.set_column(n, n, 2)

    n = 1
    u = len(matrix)
    total = len(matrix)
    tempMatrix = []
    for line in matrix:
        xsheet.write_row(n, 0, line)
        historyTargets = []
        if line[-1] == "--":
            clearScreen()
            snapshotHeader()
            if u > 1:
                print('Processing inactive rows: '+ '*' * int((u - 1) * (60 / total)))
                print(line[0], line[1])
            else:
                clearScreen()
            commentString = ""
            xsheet.write(n, 0, line[0], xformat2)
            historyStatus = SV_SIMconMetaFromConId_IN(line[0], time.strftime("%Y%m%d"))
            commentString += historyStatus[0][4]
            commentString += "\n"
            historyTargets = SV_SIMHistoricalRefsetRowsfromId(line[0], "cRefset", 1, 1, time.strftime("%Y%m%d"))
            for item in historyTargets:
                if not(item[0] == tok('INAC_CON')):
                    commentString += item[2] + " " + item[1].replace(' association reference set', '') + ":" + item[4] + " " + item[5] + "\n"
            xsheet.write_comment(xl_rowcol_to_cell(n, 0), commentString)
        tempString = " " + SV_SIMactiveColor_IN(line[0], time.strftime("%Y%m%d")) + tabStringLocal(line[0], 6, 15) + "|" + line[1][:20] + tabStringLocal(line[1][:20], 6, 15) + "|"
        for m in range(2, len(line)):
            if line[m] == " ":
                tempString += " "
            elif line[m] == "--":
                xsheet.write(n, m, line[m], xformat2)
                tempString += "\033[30;42m-\033[49;92m"
            elif line[m] == "+":
                xsheet.write(n, m, line[m], xformat4)
                tempString += "\033[30;44m+\033[49;92m"
            elif line[m] == "=":
                xsheet.write(n, m, "+", xformat8)
                tempString += "\033[30;44m+\033[49;92m"
            elif line[m] == "-":
                xsheet.write(n, m, line[m], xformat7)
                tempString += "\033[30;41m-\033[49;92m"
            elif line[m] == "#":
                xsheet.write(n, m, line[m], xformat5)
                tempString += "\033[30;45m#\033[49;92m"
            elif line[m] == "~":
                xsheet.write(n, m, "#", xformat9)
                tempString += "\033[30;45m#\033[49;92m"
            elif line[m] == ".":
                xsheet.write(n, m, line[m], xformat3)
                tempString += "\033[30;43m.\033[49;92m"
            else:
                tempString += "\033[30;44m+\033[49;92m" # line to handle/degrade refset membercounts
        tempMatrix.append(tempString + "|")
        n += 1
        u -= 1

    xsheet.set_column(0, 0, 15)
    xsheet.set_column(1, 1, 40)
    xsheet.set_row(0, 75)
    xbook.close()

    clearScreen()
    snapshotHeader()
    print("Preview showing " + fileName + " of " + CON_ID + " " + pterm(CON_ID))
    print("Details in files" + sep() + "xlsx" + sep() + fileName + ".xlsx\n")
    if len(topLine) > 10:
        timeBar = " ID\t\t\t|Term\t\t\t|" + topLine[2][:4] + (" " * (len(topLine) - 10)) + topLine[-1][:4] + "|"
    else:
        timeBar = " ID\t\t\t|Term\t\t\t|" + topLine[2][2:4] + (" " * (len(topLine) - 6)) + topLine[-1][2:4] + "|"
    print(timeBar)
    for line in tempMatrix:
        print(line)
    secondaction = input2_3('\nPress any key to continue...')
    if secondaction:
        pass
    clearScreen()
    snapshotHeader()

def tabStringLocal(inString, inValLower, inValUpper):
    if len(inString) <= inValLower:
        return "\t\t\t"
    elif len(inString) >= inValUpper:
        return "\t"
    else:
        return "\t\t"

def parsablePretty(inString):
    # break templates on cardinality
    outString = re.sub(r"(\[\[[0|1|*]\.\.[0|1|*]\]\] )", r"\n\t\1", inString)
    # break lists of concept constraints at OR clauses
    outString = re.sub(r"(OR [<<|<|\d])", r"\n\t\t\1", outString)
    # break lists of concept constraints at cardinality
    outString = re.sub(r"(\[[0|1|*]..[0|1|*]\] )", r"\n\t\1", outString)
    codeList = []
    codeList = re.findall(r"\b\d{6,20}\b", outString)
    # colorise ids
    for item in codeList:
        outString = outString.replace(item, "\033[36m" + item + "\033[92m")
    # colorise terms (pipes must be paired)
    if outString.count("|") % 2 == 0:
        returnString = ""
        outStringList = []
        outStringList = outString.split("|")
        totalCounter = 1
        counter = 1
        for row in outStringList:
            returnString += row
            if not totalCounter == len(outStringList):
                if counter == 1:
                    returnString += "|\033[95m"
                    counter = 2
                else:
                    returnString += "\033[92m|"
                    counter = 1
            totalCounter += 1
        return returnString
    else:
        return outString


def parsablePrettyOwl(inString):
    tempList = []
    startFindList = ['SubClassOf(:', \
                'EquivalentClasses(:', \
                'SubObjectPropertyOf(:', \
                'SubClassOf(ObjectIntersectionOf(:', \
                'TransitiveObjectProperty(:', \
                'ReflexiveObjectProperty(:', \
                'SubObjectPropertyOf(ObjectPropertyChain(:', \
                'ObjectIntersectionOf(:', \
                'ObjectPropertyChain(:']
    startKeepList = ['SubClassOf(', \
                'EquivalentClasses(', \
                'SubObjectPropertyOf(', \
                'SubClassOf(ObjectIntersectionOf(', \
                'TransitiveObjectProperty(', \
                'ReflexiveObjectProperty(', \
                'SubObjectPropertyOf(ObjectPropertyChain(', \
                'ObjectIntersectionOf(', \
                'ObjectPropertyChain(']
    outList = []
    codeList = []
    ptermDict = {}
    outString = inString
    outString = outString.replace("))) :", ")))\n:")
    outString = outString.replace("ObjectIntersectionOf(ObjectSomeValuesFrom", "\nObjectIntersectionOf(\nObjectSomeValuesFrom")
    outString = outString.replace(" ObjectIntersectionOf", "\nObjectIntersectionOf")
    outString = outString.replace(" ObjectSomeValuesFrom(:" + tok("RG") , "\n ObjectSomeValuesFrom(:" + tok("RG"))
    outString = outString.replace(" ObjectSomeValuesFrom", "\nObjectSomeValuesFrom")
    outString = outString.replace(" DataHasValue", "\nDataHasValue")
    outString = outString.replace(")))", ")))\n")
    outString = outString.replace("\n\n\n", "\n\n")
    match = []
    p = re.compile(r'(:)([0-9]{3,15})(00|01|02|10|11|12)([0-9])', re.IGNORECASE)
    match = p.findall(outString)
    for item in match:
        if validateVerhoeff(int(item[1] + item[2] + item[3])):
            codeList.append(item[1] + item[2] + item[3])
    for item in codeList:
        if not item in ptermDict:
            if cfg("PRETTY") == 2:
                ptermDict[item] = "\033[36m" + item + "\033[92m|\033[95m" + pterm(item) + "\033[92m|"
            else:
                ptermDict[item] = "\033[36m" + item + "\033[92m"
    tempList = outString.split("\n")
    tempString = ""
    for item2 in tempList:
        tempString = item2
        outList.append(tempString)
    returnString = ""
    for item in outList:
        startFMatch = False
        for startF in startFindList:
            if startF in item:
                startFMatch = True
        if startFMatch:
            item2List = []
            item2List = item.split(":")
            for item2 in item2List:
                startKMatch = False
                for startK in startKeepList:
                    if startK in item2:
                        startKMatch = True
                if startKMatch:
                    returnString += "\t\t\t" + item2 + "\n"
                else:
                    returnString += "\t\t\t:" + item2 + "\n"
        else:
            returnString += "\t\t\t" + item + "\n"
    for item1 in ptermDict:
        returnString = returnString.replace(item1, ptermDict[item1])
    return returnString[3:]

def writePreDotFile(inString, inET):
    roleString = ""
    parentString = ""
    inId = inString[0]
    if not inId == tok('ROOT'):
        parentList = []
        # parentList = parentsFromId_IN(tuplify(inString))
        if cfg("QUERY_TYPE") == 1:
            parentList = SV_SIMparentsFromIdPlusMeta_IN(tuplify(inId), 3, inET)
        elif cfg("QUERY_TYPE") == 2:
            parentList = SV_SIMparentsFromIdPlusMeta_IN(tuplify(inId), cfg("SHOW_TYPE"), inET)
        elif cfg("QUERY_TYPE") == 3:
            parentList = SV_MDRparentsFromIdPlusMeta_IN(tuplify(inId), cfg('SHOW_TYPE'), effTimeSet())
        for parent in parentList:
            parentString += parent[3] + " | " + parent[4] + " | +\n"
        #roles
        roleList = []
        # roleList = groupedDefiningRolesFromConId_IN(inString)
        if cfg("QUERY_TYPE") == 1:
            roleList = SV_SIMrolesFromIdPlusMeta_IN(inId, 3, inET)
        elif cfg("QUERY_TYPE") == 2:
            roleList = SV_SIMrolesFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), inET)
        elif cfg("QUERY_TYPE") == 3:
            roleList = SV_MDRrolesFromIdPlusMeta_IN(inId, cfg("SHOW_TYPE"), effTimeSet())
        for roles in roleList:
            if roles[0] > 0:
                roleString += "{"
                for role in roles[1:]:
                    roleString += "" + role[1] + " | " +  role[2] + " | = " + role[3] + " | " +  role[4] + " |,\n"
                roleString = roleString[:-2] + "},\n"
        for roles in roleList:
            if roles[0] == 0:
                for role in roles[1:]:
                    roleString += "" + role[1] + " | " +  role[2] + " | = " + role[3] + " | " +  role[4] + " |,\n"
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'pre_dot_sv.txt', 'w', encoding="UTF-8")
        # focus
        f.write(inId + " | " + pterm(inId) + " |\n")
        # operator
        conMeta = []
        if cfg("QUERY_TYPE") == 1:
            conMeta = SV_ALLconMetaFromConId_IN(inId)
        elif cfg("QUERY_TYPE") == 2:
            conMeta = SV_SIMconMetaFromConId_IN(inId, inET)
        else:
            conMeta = SV_MDRconMetaFromConId_IN(inId, effTimeSet())
        if not inId == tok('ROOT'):
            if len(conMeta) > 0:
                if conMeta[0][3] == "Defined":
                    f.write("=== ")
                else:
                    f.write("<<< ")
            else:
                f.write("<<< ")
        # parents
        if roleString == "":
            f.write(parentString[:-2])
        # parents and roles
        else:
            f.write(parentString[:-2] + ":\n")
            f.write(roleString[:-2])
    finally:
        f.close()

def dotDrawFocusCall(inId):
    writePreDotFile(tuplify(inId), cfg("EFFECTIVE_TIME"))
    dotDrawFocus("xdot", cfg("EFFECTIVE_TIME"))

def dotDrawFocus(inType, inET=time.strftime("%Y%m%d")):
    inLines = []
    outLines = []
    inGroup = False
    groupCounter = 0
    lineCounter = 0
    tempLine = ""
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'full' + sep() + 'pre_dot_sv.txt', 'r', encoding="UTF-8")
        # focus
        inLines = f.readlines()
    finally:
        f.close()

    for line in inLines:
        tempList = []
        if "{" in line:
            inGroup = True
            groupCounter += 1
        if not inGroup:
            if lineCounter == 0:
                tempList = toTempList(tempList, tempLine, str(lineCounter), "FOCUS", str(groupCounter), [item.strip() for item in line.strip().split("|")], len(line.strip().split("|")))
            else:
                if "<<<" in line:
                    tempLine = "PRIM"
                elif "===" in line:
                    tempLine = "FD"
                else:
                    tempLine = "NTC"
        if lineCounter != 0:
            tempList = toTempList(tempList, tempLine, str(lineCounter), inGroup, str(groupCounter), [clean(item) for item in line.strip().split("|")], len(line.strip().split("|")))
        if "}" in line:
            inGroup = False
        lineCounter += 1
        tempLine = ""
        if tempList[2] == 'FOCUS': # if a focus concept
            outLines.append(tempList)
        elif (tempList[4][0] in limitAttributes(True)) or ("100000110" in tempList[4][0]): # if a matched attribute row, or if a pharmacy attribute!
            outLines.append(tempList)
        elif (cfg("SHOW_SUPERS") == 1 and tempList[5] == 3) or (cfg("SHOW_SUPERS") == 3 and tempList[5] == 3): # if show supertypes=1 and it's a supertype row
            outLines.append(tempList)

    focusList = []
    definedStatus = False
    parentList = []
    soleRoleList = []
    groupedRoleList = []
    groupNodes = []
    attNameList = []
    objValueList = []
    bunchNodes = []
    dotEdgeList = []
    # invisEdgeList = []

    spineRankSameString = "{rank=same; "
    groupRankSameString = "{rank=same; "
    parentAndAttRankSameString = "{rank=same; "
    parentAndValueRankSameString = "{rank=same; "

    if inType == "D3":
        cl = ""
    else:
        cl = ";"
    bunchNodes.append("\"BN0\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black]" + cl)
    spineRankSameString += "\"BN0\" "
    for line in outLines:
        # focus
        if line[2] == "FOCUS":
            focusList.append(line)
        elif not line[2] and line[0] == "FD":
            definedStatus = True
            parentList.append(line)
        elif not line[2] and line[0] == "PRIM":
            parentList.append(line)
        elif int(line[3]) > 0 and not line[0] == "NTC":
            groupedRoleList.append(line)
            if not line[3] in groupNodes:
                if not "\"GN" + line[3] + "\" [shape=circle, fontsize=1, width=0.5, label=\"\" style=filled, fillcolor=white]" + cl in groupNodes:
                    groupNodes.append("\"GN" + line[3] + "\" [shape=circle, fontsize=1, width=0.5, label=\"\" style=filled, fillcolor=white]" + cl)
                if not "\"BN" + line[3] + "\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black]" + cl in bunchNodes:
                    bunchNodes.append("\"BN" + line[3] + "\" [shape=circle, fontsize=0, width=0, style=filled, fillcolor=black]" + cl)
                    groupRankSameString += "\"" + "BN" + line[3] + "\" "
        elif line[0] == "NTC" and line[5] == 5:
            soleRoleList.append(line)
        elif line[0] == "NTC" and line[5] == 3:
            parentList.append(line)

    for item in focusList:
        objValueList.append("\"" + item[4][0].strip() + "\" [label=\"" + item[4][0].strip() + "\l" + simplify(item[4][1]) + "\l\", " + checkObjValueColor(item[4][0].strip(), inET) + "]" + cl)
        objValueList.append("\"" + str(inET) + "\" [label=\"" + str(inET) + "\", shape=box, peripheries=2, style=filled, fillcolor=pink]" + cl)
    if definedStatus:
        if checkAllAtts():
            groupNodes.append("\"DN\" [shape=circle, fontsize=27, margin=0, width=0, label=\"&#8801;\", labelloc=c, style=filled, fillcolor=white]" + cl)
        else:
            groupNodes.append("\"DN\" [shape=circle, fontsize=27, fontcolor=blue, margin=0, width=0, label=\"&#8838;\", labelloc=c, style=filled, fillcolor=white]" + cl)
    else:
        if checkAllAtts():
            groupNodes.append("\"DN\" [shape=circle, fontsize=27, margin=0, width=0, label=\"&#8838;\", labelloc=c, style=filled, fillcolor=white]" + cl)
        else:
            groupNodes.append("\"DN\" [shape=circle, fontsize=27, fontcolor=blue, margin=0, width=0, label=\"&#8838;\", labelloc=c, style=filled, fillcolor=white]" + cl)
    longParentTerm = padForParents(parentList)
    spineNum = 0
    if cfg("ACROSS_DOWN") > 0:
        dotEdgeList.append("\"" + str(inET) + "\"->\"" + focusList[0][4][0] + "\" [style=invis, constraint=false]" + cl)
        dotEdgeList.append("\"" + focusList[0][4][0] + "\"->\"" + "DN" + "\" [constraint=false]" + cl)
    else:
        dotEdgeList.append("\"" + str(inET) + "\"->\"" + focusList[0][4][0] + "\" [style=invis]" + cl)
        dotEdgeList.append("\"" + focusList[0][4][0] + "\"->\"" + "DN" + "\"" + cl)
    dotEdgeList.append("\"" + "DN" + "\"->\"" + "BN0" + "\"" + cl)
    firstParent = True
    for item in parentList:
        if firstParent and (cfg("ACROSS_DOWN") > 0):
            topParentString = ", weight=50]"
        else:
            topParentString = "]"
        if cfg("ACROSS_DOWN") > 3:
            if firstParent:
                append_distinct(dotEdgeList, "\"" + "BN0" + "\"->\"" + item[4][0].strip() + "\" [arrowhead=onormal" + topParentString + cl)
                if len(parentList) == 1 and (soleRoleList != [] or groupedRoleList != []):
                    append_distinct(dotEdgeList, "\"" + "BN0" + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                    objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0];")
                    spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
            else:
                if spineNum == 1:
                    append_distinct(dotEdgeList, "\"" + "BN0" + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                else:
                    append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none];")
                append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum) + "\"->\"" + item[4][0].strip() + "\"" + southWestMarkerParent(item, parentList, soleRoleList, groupedRoleList) + " [arrowhead=onormal" + topParentString + cl)
                objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0];")
                spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
            spineNum += 1
        else:
            dotEdgeList.append("\"" + "BN0" + "\"->\"" + item[4][0].strip() + "\" [arrowhead=onormal" + topParentString + cl)
        objValueList.append("\"" + item[4][0].strip() + "\" [label=\"" + item[4][0].strip() + "\l" + padParent(simplify(item[4][1]), longParentTerm, firstParent) + "\l\", " + checkObjValueColor(item[4][0].strip(), inET) + "]" + cl)
        parentAndAttRankSameString += "\"" + item[4][0].strip() + "\" "
        parentAndValueRankSameString += "\"" + item[4][0].strip() + "\" "
        firstParent = False
    for item in soleRoleList:

        if (cfg("ACROSS_DOWN") > 3):
            append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none]" + cl)
            append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum) + "\"->\"" + item[4][0].strip() + "_" + item[1] + "\"" + northWestMarker(item, soleRoleList, groupedRoleList) + cl)
            objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0]" + cl)
            spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
            spineNum += 1
        else:
            append_distinct(dotEdgeList, "\"" + "BN0" + "\"->\"" + item[4][0].strip() + "_" + item[1] + "\"" + cl)
        append_distinct(dotEdgeList, "\"" + item[4][0].strip() + "_" + item[1] + "\"->\"" + item[4][2] + "_" + item[1] + "\"" + cl)
        attNameList.append("\"" + item[4][0].strip() + "_" + item[1] + "\" [label=\"" + item[4][0].strip() + "\l" + simplify(item[4][1]) + "\l\", shape=box, peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"]" + cl)
        parentAndAttRankSameString += "\"" + item[4][0].strip() + "_" + item[1] + "\" "
        objValueList.append("\"" + item[4][2] + "_" + item[1] + "\" [label=\"" + item[4][2] + "\l" + simplify(item[4][3]) + "\l\", " + checkObjValueColor(item[4][2], inET) + "]" + cl)
        parentAndValueRankSameString += "\"" + item[4][2] + "_" + item[1] + "\" "
    groupNumTrack = '0'
    groupedRoleList = sorted(groupedRoleList, key=lambda x: (int(x[3]), x[4][3]))
    groupPadDict = padForGroup(groupedRoleList)
    spineGroupTracker = '0'
    if len(groupedRoleList) > 0:
        maxGroup = sorted(list(set([item[3] for item in groupedRoleList])), key=lambda x: int(x))[-1]
    else:
        maxGroup = "0"
    for item in groupedRoleList:
        if cfg("ACROSS_DOWN") > 3:
            if spineGroupTracker != item[3]:
                spineGroupTracker = item[3]
                rowInGroup = 0
                if item[3] == maxGroup:
                    append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "GN" + item[3] + "\"" + cl)
                    if int(item[3]) > 1:
                        append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + "BN" + str(int(item[3]) - 1) + "\" [style=invis]" + cl)
                else:
                    append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum - 1) + "\"->\"" + "SP_"+ str(spineNum) + "\" [arrowhead=none]" + cl)
                    append_distinct(dotEdgeList, "\"" + "SP_"+ str(spineNum) + "\"->\"" + "GN" + item[3] + "\"" + cl)
                    objValueList.append("\"" + "SP_"+ str(spineNum) + "\" [label=\"\", shape=circle, width=0, height=0]" + cl)
                    spineRankSameString += "\"" + "SP_"+ str(spineNum) + "\" "
                spineNum += 1
        else:
            append_distinct(dotEdgeList, "\"" + "BN0" + "\"->\"" + "GN" + item[3] + "\"" + cl)
        append_distinct(dotEdgeList, "\"" + "GN" + item[3] + "\"->\"" + "BN" + item[3] + "\"" + (" [weight=50]" if (cfg("ACROSS_DOWN") > 0) else "") + cl)
        topRowString = ""
        if (item[3] != groupNumTrack) and (cfg("ACROSS_DOWN") > 0):
            topRowString = " [weight=50]"
            groupNumTrack = item[3]
        if cfg("ACROSS_DOWN") > 3:
            if rowInGroup == 0:
                append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"" + topRowString + cl)
            else:
                if rowInGroup == 1:
                    append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" [arrowhead=none]" + cl)
                else:
                    append_distinct(dotEdgeList, "\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup - 1) + "\"->\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" [arrowhead=none]" + cl)
                append_distinct(dotEdgeList, "\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\"->\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"" + southWestMarker(item, groupedRoleList) + topRowString + cl)
                objValueList.append("\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" [label=\"\", shape=circle, width=0, height=0]" + cl)
                groupRankSameString += "\"" + "GSP_"+ spineGroupTracker + "_" + str(rowInGroup) + "\" "
            rowInGroup += 1
        else:
            append_distinct(dotEdgeList, "\"" + "BN" + item[3] + "\"->\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"" + topRowString + cl)
        append_distinct(dotEdgeList, "\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\"->\"" + item[4][2] + "_" + item[1] + "_" + item[3] + "\"" + cl)
        attNameList.append("\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\" [label=\"" + padIdOrTerm(groupNumTrack, groupPadDict, item[4][0].strip(), "") + "\l" + padIdOrTerm(groupNumTrack, groupPadDict, "", simplify(item[4][1])) + "\l\", shape=box, peripheries=2, style=\"rounded,filled\", fillcolor=\"#FFFFCC\"]" + cl)
        objValueList.append("\"" + item[4][2] + "_" + item[1] + "_" + item[3] + "\" [label=\"" + item[4][2] + "\l" + simplify(item[4][3]) + "\l\", " + checkObjValueColor(item[4][2], inET) + "]" + cl)
        parentAndAttRankSameString += "\"" + item[4][0].strip() + "_" + item[1] + "_" + item[3] + "\" "
        parentAndValueRankSameString += "\"" + item[4][2] + "_" + item[1] + "_" + item[3] + "\" "

    # overload objValueList with spineRankList(s)
    if cfg("ACROSS_DOWN") > 3:
        objValueList.append(spineRankSameString[:-1] + "}" + cl)
        objValueList.append(groupRankSameString[:-1] + "}" + cl)
    if cfg("ACROSS_DOWN") == 5:
        objValueList.append(parentAndAttRankSameString[:-1] + "}" + cl)
    if cfg("ACROSS_DOWN") == 6:
        objValueList.append(parentAndValueRankSameString[:-1] + "}" + cl)
    if inType == "D3":
        return dotEdgeList, bunchNodes, groupNodes, attNameList, objValueList
    else:
        focusToXdot(dotEdgeList, bunchNodes, groupNodes, attNameList, objValueList)


def focusToXdot(dotEdgeList, bunchNodes, groupNodes, attNameList, objValueList):
    try:
        f = io.open(cfgPathInstall() + 'files' + sep() + 'dot' + sep() + 'conpic.dot', 'w', encoding="UTF-8")
        if (cfg("SHOW_SUPERS") == 2 or cfg("SHOW_SUPERS") == 3):
            f.write("strict digraph G { rankdir=LR; ranksep=.3; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        else:
            f.write("strict digraph G { rankdir=LR; ranksep=.3; splines=ortho; node [shape=box, fontsize=9, fontname=Helvetica, style=filled, fillcolor=white];\n")
        for item in dotEdgeList:
            f.write(item + "\n")
        for item in sorted_cfg(bunchNodes):
            f.write(item + "\n")
        for item in sorted_cfg(groupNodes):
            f.write(item + "\n")
        for item in sorted_cfg(attNameList):
            f.write(item + "\n")
        for item in sorted_cfg(objValueList):
            f.write(item + "\n")
        f.write("}")
    finally:
        f.close()

def padParent(inTerm, longTerm, firstParent):
    outTerm = ""
    if cfg("ACROSS_DOWN") == 3 and firstParent and (len(inTerm) < longTerm):
        outTerm = inTerm + "            " + (" " * (longTerm - len(inTerm)))
    else:
        outTerm = inTerm
    return outTerm

def padIdOrTerm(groupNumTrack, groupPadDict, AttId, AttTerm):
    idOrTerm = ""
    if cfg("ACROSS_DOWN") in [2, 3]:
        if groupPadDict[groupNumTrack][0] == 0: # only change the term
            if AttTerm == "": # if it's not the term
                idOrTerm = AttId # return the id
            else: # modify the term
                if len(AttTerm) < groupPadDict[groupNumTrack][1]:
                    idOrTerm = AttTerm + "      " + (" " * (groupPadDict[groupNumTrack][1] - len(AttTerm)))
                else:
                    idOrTerm = AttTerm
        else: # change the id
            if AttId == "": # if its not the id
                idOrTerm = AttTerm # return the term
            else: # modify the id
                idOrTerm = AttId + (" " * (groupPadDict[groupNumTrack][0] - len(AttId)))
    else:
        idOrTerm = AttId + AttTerm
    return idOrTerm

def clean(inString):
    outString = inString.strip()
    outString = outString.replace("{", "")
    outString = outString.replace("}", "")
    outString = outString.replace("= ", "") # shortens ===
    outString = outString.replace("==", "") # removes last bit
    outString = outString.replace("<<<", "") # removes other operator
    return outString

def toTempList(inTempList, inNotes, inCounter, inIdent, inGroup, inList, inLen):
    inTempList.append(inNotes)
    inTempList.append(inCounter)
    inTempList.append(inIdent)
    inTempList.append(inGroup)
    inTempList.append(inList)
    inTempList.append(inLen)
    return inTempList

def append_distinct(inList, inItem):
    if list(inItem) not in [list(item) for item in inList]:
        inList.append(inItem)
    return inList
