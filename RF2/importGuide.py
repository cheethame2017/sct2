from __future__ import print_function, unicode_literals
import io
import os
import sys

if (sys.version_info > (3, 0)):
    # Python 3 code:
    w = input("Input Build Type (Full, Delta, Snapshot): ")
else:
    # Python 2 code:
    w = raw_input("Input Build Type (Full, Delta, Snapshot): ").decode('UTF-8')

importItems = []
importMainItems = []

path = '/home/ed/D/Out/'
trimpath = path[:-1]

files = os.listdir(trimpath)

basicFields = 'id TEXT, effectiveTime TEXT, active INTEGER, moduleId TEXT, '

text_file = io.open(path[:-4] + "import" + w + ".txt", "w", encoding="UTF-8")

text_file.write('.echo ON')
text_file.write("\n")
text_file.write('.separator \\t')
text_file.write("\n")

text_file.write('--CREATE TABLE INSTRUCTIONS')
text_file.write("\n")
text_file.write('CREATE TABLE Concepts(' + basicFields + 'definitionStatusId TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE Relationships(' + basicFields + 'sourceId TEXT, destinationId TEXT, relationshipGroup INTEGER, typeId TEXT, characteristicTypeId TEXT, modifierId TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE Descriptions(' + basicFields + 'conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE Definitions(' + basicFields + 'conceptId TEXT, languageCode TEXT, typeId TEXT, term TEXT, caseSignificanceId TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE SimpleRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE cRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE cciRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT, ComponentId2 TEXT, Integer1 INTEGER);')
text_file.write("\n")
text_file.write('CREATE TABLE ciRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT, Integer1 INTEGER);')
text_file.write("\n")
text_file.write('CREATE TABLE cissccRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, ComponentId1 TEXT, Integer1 INTEGER, String1 TEXT, String2 TEXT, ComponentId2 TEXT, ComponentId3 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE iissscRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, Integer1 INTEGER, Integer2 INTEGER, String1 TEXT, String2 TEXT, String3 TEXT, ComponentId1 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE iisssccRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, Integer1 INTEGER, Integer2 INTEGER, String1 TEXT, String2 TEXT, String3 TEXT, ComponentId1 TEXT, ComponentId2 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE iisssciRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, Integer1 INTEGER, Integer2 INTEGER, String1 TEXT, String2 TEXT, String3 TEXT, ComponentId1 TEXT, Integer3 INTEGER);')
text_file.write("\n")
text_file.write('CREATE TABLE sRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, String1 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE scccRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, String1 TEXT, ComponentId1 TEXT, ComponentId2 TEXT, ComponentId3 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE ssRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE ssccRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT, ComponentId1 TEXT, ComponentId2 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE sscccRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT, ComponentId1 TEXT, ComponentId2 TEXT, ComponentId3 TEXT);')
text_file.write("\n")
text_file.write('CREATE TABLE sssssssRefset(' + basicFields + 'refsetId TEXT, referencedComponentId TEXT, String1 TEXT, String2 TEXT, String3 TEXT, String4 TEXT, String5 TEXT, String6 TEXT, String7 TEXT);')
text_file.write("\n")

text_file.write('--MAIN TABLE IMPORTS')
text_file.write("\n")
for fileName in files:
    if ('_Concept_' in fileName) and (w + '_' in fileName):
        print('Concepts: ', fileName)
        importMainItems.append('.import ' + path + fileName + ' Concepts' + '\n')
    if ('_Relationship_' in fileName) and (w + '_' in fileName):
        print('Relationships: ', fileName)
        importMainItems.append('.import ' + path + fileName + ' Relationships' + '\n')
    if ('_Description_' in fileName) and (w + '_' in fileName):
        print('Descriptions: ', fileName)
        importMainItems.append('.import ' + path + fileName + ' Descriptions' + '\n')
    if ('_Definition_' in fileName) and (w + '_' in fileName):
        print('Definitions: ', fileName)
        importMainItems.append('.import ' + path + fileName + ' Definitions' + '\n')
    if ('Refset_' in fileName):
        if ((w + '_' in fileName) or (w + '-en_' in fileName) or (w + '-es_' in fileName) or (w + '-en-GB_' in fileName)):
            RSType = fileName[fileName.find('_'):fileName.find('Refset')]
            if RSType == '_':
                print('Simple: ', fileName)
                importItems.append('.import ' + path + fileName + ' SimpleRefset' + '\n')
            elif RSType == '_c':
                print('Component: ', fileName)
                importItems.append('.import ' + path + fileName + ' cRefset' + '\n')
            elif RSType == '_cci':
                print('ComponentComponentInteger: ', fileName)
                importItems.append('.import ' + path + fileName + ' cciRefset' + '\n')
            elif RSType == '_ci':
                print('ComponentInteger: ', fileName)
                importItems.append('.import ' + path + fileName + ' ciRefset' + '\n')
            elif RSType == '_cisscc':
                print('ComponentIntegerStringStringComponentComponent: ', fileName)
                importItems.append('.import ' + path + fileName + ' cissccRefset' + '\n')
            elif RSType == '_iisssc':
                print('IntegerIntegerStringStringStringComponent: ', fileName)
                importItems.append('.import ' + path + fileName + ' iissscRefset' + '\n')
            elif RSType == '_iissscc':
                print('IntegerIntegerStringStringStringComponentComponent: ', fileName)
                importItems.append('.import ' + path + fileName + ' iisssccRefset' + '\n')
            elif RSType == '_iisssci':
                print('IntegerIntegerStringStringStringComponentInteger: ', fileName)
                importItems.append('.import ' + path + fileName + ' iisssciRefset' + '\n')
            elif RSType == '_s':
                print('String: ', fileName)
                importItems.append('.import ' + path + fileName + ' sRefset' + '\n')
            elif RSType == '_sccc':
                print('StringComponentComponentComponent: ', fileName)
                importItems.append('.import ' + path + fileName + ' scccRefset' + '\n')
            elif RSType == '_ss':
                print('StringString: ', fileName)
                importItems.append('.import ' + path + fileName + ' ssRefset' + '\n')
            elif RSType == '_sscc':
                print('StringStringComponentComponent: ', fileName)
                importItems.append('.import ' + path + fileName + ' ssccRefset' + '\n')
            elif RSType == '_ssccc':
                print('StringStringComponentComponentComponent: ', fileName)
                importItems.append('.import ' + path + fileName + ' sscccRefset' + '\n')
            elif RSType == '_sssssss':
                print('StringStringStringStringStringStringString: ', fileName)
                importItems.append('.import ' + path + fileName + ' sssssssRefset' + '\n')
            else:
                print('Dunno: ', fileName)
        elif ((w == 'Snapshot') and ('NHSRDS' in fileName)):
            print('Component [NHSRDS snapshot exception]: ', fileName)
            importItems.append('.import ' + path + fileName + ' cRefset' + '\n')
        elif (not ('Full' in fileName) and not ('Delta' in fileName) and not ('Snapshot' in fileName)):
            print('NOT FOUND: ', fileName)

for importItem in importMainItems:
    text_file.write(str(importItem))

text_file.write('--REFSET IMPORTS')
text_file.write("\n")

for importItem in importItems:
    text_file.write(str(importItem))

tableNames = ['Concepts',
            'Relationships',
            'Descriptions',
            'Definitions',
            'SimpleRefset',
            'cRefset',
            'cciRefset',
            'ciRefset',
            'cissccRefset',
            'iissscRefset',
            'iisssccRefset',
            'iisssciRefset',
            'sRefset',
            'scccRefset',
            'ssRefset',
            'ssccRefset',
            'sscccRefset',
            'sssssssRefset',]

indexFields = ['id',
                'active']

text_file.write('--BASIC INDEX ADDITIONS')
text_file.write("\n")

for tableName in tableNames:
    for indexField in indexFields:
        text_file.write('CREATE INDEX ' + tableName + '_' + indexField + '_idx ON ' + tableName + ' (' + indexField + ');')
        text_file.write("\n")
    if ('Refset' in tableName):
        text_file.write('CREATE INDEX ' + tableName + '_referencedComponentId_idx ON ' + tableName + ' (referencedComponentId);')
        text_file.write("\n")
        text_file.write('CREATE INDEX ' + tableName + '_refsetId_idx ON ' + tableName + ' (refsetId);')
        text_file.write("\n")

#table-specific index additions (this will grow)
text_file.write('--TABLE-SPECIFIC INDEX ADDITIONS')
text_file.write("\n")
text_file.write('CREATE INDEX Descriptions_conceptId_idx ON Descriptions (conceptId);')
text_file.write("\n")
text_file.write('CREATE INDEX Definitions_conceptId_idx ON Definitions (conceptId);')
text_file.write("\n")
text_file.write('CREATE INDEX Descriptions_term_idx ON Descriptions (term);')
text_file.write("\n")
text_file.write('CREATE INDEX Relationships_SourceId_idx ON Relationships (sourceId);')
text_file.write("\n")
text_file.write('CREATE INDEX Relationships_DestinationId_idx ON Relationships (destinationId);')
text_file.write("\n")
text_file.write('CREATE INDEX Relationships_CharacteristicTypeId_idx ON Relationships (characteristicTypeId);')
text_file.write("\n")
text_file.write('CREATE INDEX Relationships_typeId_idx ON Relationships (typeId);')
text_file.write("\n")
text_file.write('CREATE INDEX crefset_componentId1_idx ON crefset (componentId1);')
text_file.write("\n")
text_file.write('CREATE INDEX concepts_moduleId_idx ON concepts (moduleId);')
text_file.write("\n")
text_file.write('CREATE INDEX descriptions_moduleId_idx ON descriptions (moduleId);')
text_file.write("\n")
text_file.write('CREATE INDEX relationships_moduleId_idx ON relationships (moduleId);')
text_file.write("\n")
#build time views (this will grow)
text_file.write('--VIEWS')
text_file.write("\n")
#1
text_file.write("CREATE VIEW \"rdscodes\" AS select id as CID from concepts where id IN (\"999001261000000100\", \"999000691000001104\") and active = 1;")
text_file.write("\n")
#2 - depends on 1
text_file.write("CREATE VIEW \"rdsrows\" AS select referencedComponentId as id, componentId1 as acc from crefset where refsetId in (select CID from rdscodes) and active = 1;")
text_file.write("\n")
#3 - depends on 2
text_file.write("CREATE VIEW \"prefTerm\" AS select descriptions.conceptId, descriptions.id, descriptions.term from descriptions, rdsrows where descriptions.id = rdsrows.id and descriptions.typeId = \"900000000000013009\" and rdsrows.acc = \"900000000000548007\" and descriptions.active = 1;")
text_file.write("\n")
#6 ->
text_file.write("CREATE VIEW \"refsetModules\" AS \
select distinct 'Concepts' as filetype, 'Concepts' as refsetId, moduleId from Concepts where active=1 UNION \
select distinct 'Descriptions' as filetype, 'Descriptions' as refsetId, moduleId from Descriptions where active=1 UNION \
select distinct 'Definitions' as filetype, 'Definitions' as refsetId, moduleId from Definitions where active=1 UNION \
select distinct 'Relationships' as filetype, 'Relationships' as refsetId, moduleId from Relationships where active=1 UNION \
select distinct 'simplerefset' as filetype, refsetId, moduleId from simplerefset where active=1 UNION \
select distinct 'crefset' as filetype, refsetId, moduleId from crefset where active=1 UNION \
select distinct 'ccirefset' as filetype, refsetId, moduleId from ccirefset where active=1 UNION \
select distinct 'cirefset' as filetype, refsetId, moduleId from cirefset where active=1 UNION \
select distinct 'cissccrefset' as filetype, refsetId, moduleId from cissccrefset where active=1 UNION \
select distinct 'iissscrefset' as filetype, refsetId, moduleId from iissscrefset where active=1 UNION \
select distinct 'iisssccrefset' as filetype, refsetId, moduleId from iisssccrefset where active=1 UNION \
select distinct 'iissscirefset' as filetype, refsetId, moduleId from iissscirefset where active=1 UNION \
select distinct 'srefset' as filetype, refsetId, moduleId from srefset where active=1 UNION \
select distinct 'scccrefset' as filetype, refsetId, moduleId from scccrefset where active=1 UNION \
select distinct 'ssrefset' as filetype, refsetId, moduleId from ssrefset where active=1 UNION \
select distinct 'ssccrefset' as filetype, refsetId, moduleId from ssccrefset where active=1 UNION \
select distinct 'sscccrefset' as filetype, refsetId, moduleId from sscccrefset where active=1 UNION \
select distinct 'sssssssrefset' as filetype, refsetId, moduleId from sssssssrefset where active=1;")
text_file.write("\n")

# tidy up header rows
text_file.write('--REMOVE HEADERS')
text_file.write("\n")

for tableName in tableNames:
    text_file.write('DELETE FROM ' + tableName + ' WHERE id == \'id\';')
    text_file.write("\n")
text_file.write('--DE-ESCAPE TERMS')
text_file.write("\n")
text_file.write('UPDATE descriptions SET term = replace( term, \'\\"\', \'"\' ) WHERE term LIKE \'%\\"%\';')
text_file.write("\n")
text_file.write('UPDATE definitions SET term = replace( term, \'\\"\', \'"\' ) WHERE term LIKE \'%\\"%\';')
text_file.write("\n")
text_file.write('UPDATE relationships SET destinationId = replace( destinationId, \'\\"\', \'"\' ) WHERE destinationId LIKE \'%\\"%\';')
text_file.write("\n")

text_file.close()
text_file = io.open(path[:-4] + "test" + w + ".txt", "w", encoding="UTF-8")

text_file.write('.echo ON')
text_file.write("\n")
text_file.write('.separator \\t')
text_file.write("\n")

text_file.write('--READ TABLE INSTRUCTIONS')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM Concepts;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM Relationships;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM Descriptions;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM Definitions;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM SimpleRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM cRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM cciRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM ciRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM cissccRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM iissscRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM iisssccRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM iisssciRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM sRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM scccRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM ssRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM ssccRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM sscccRefset;')
text_file.write("\n")
text_file.write('SELECT COUNT(*) FROM sssssssRefset;')
text_file.write("\n")

text_file.close()
