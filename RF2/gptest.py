import math
import io
import sys
from SCTRF2Helpers import descendantsCleanIDFromConId_IN
from SCTRF2Funcs import readFocusValue
from SCTRF2Role import writeOutList
from SCTRF2config import pth
from platformSpecific import cfgPathInstall, sep


def readInGP():
    testDict = {}
    filePath = pth('BASIC_DATA') + 'rankData.txt'
    try:
        f = io.open(filePath, 'r', encoding="UTF-8")
        rankLines = f.readlines()
    finally:
        f.close()
    for line in rankLines[1:]:
        tempList = line.strip().split("\t")
        testDict[tempList[0]] = [tempList[2], tempList[3], tempList[4]]
    return testDict

def testGP(numberToResult, onlyInDict, dictDensity):
    GPRankList = []
    outList = []
    GPDict = readInGP()
    # key_list = list(GPDict.keys())
    key_list = descendantsCleanIDFromConId_IN(readFocusValue()[0])
    for key in key_list:
        density = 0
        if dictDensity != 3:
            descList = descendantsCleanIDFromConId_IN(key)
            tempNum = 0
            matchCounter = 0
            for desc in descList:
                if desc in GPDict:
                    matchCounter += 1
                    tempNum += math.log(int(GPDict[desc][0]))
            if dictDensity == 1:
                density = (tempNum / (len(descList) + 1)) * math.log(matchCounter + 1)
            elif dictDensity == 2:
                density = (matchCounter / (len(descList) + 1)) * math.log(matchCounter + 1)
            GPRankList.append([key, density, matchCounter, len(descList)])
        else:
            if key in GPDict:
                density = int(GPDict[key][0])
            GPRankList.append([key, density])
    GPRankList.sort(key=lambda x: x[1], reverse=True)
    for item in GPRankList[:numberToResult]:
        if item[0] in GPDict and onlyInDict:
        #     pass
        #     print(item[0], pterm(item[0]), item[1], item[2], item[3], "*")
            print(item[0])
            outList.append(item[0])
        else:
            print(item[0])
            outList.append(item[0])
        # else:
        #     print(item[0], pterm(item[0]), item[1], item[2], item[3])
        # print(item[0])
        if dictDensity == 3:
            writeOutList(outList, cfgPathInstall() + 'sets' + sep() + 'M.txt')
        else:
            writeOutList(outList, cfgPathInstall() + 'sets' + sep() + 'N.txt')

if __name__ == "__main__":
    # numberToResult = 200
    # onlyInDict = True
    # dictDensity = 3

    if len(sys.argv) != 4:
        if (sys.version_info > (3, 0)):
            s = input('Number to result?')
            t = input('Only in Dict (y/n)?')
            u = input('Dict Density (1, 2, 3)?')
    else:
        s = str(sys.argv[1])
        t = str(sys.argv[2])
        u = str(sys.argv[3])

    numberToResult = int(s)
    if t == "1":
        onlyInDict = True
    else:
        onlyInDict = False
    dictDensity = int(u)

    # (sct) ed@ed-VirtualBox:~/Code/Repos/sct/RF2$ python gptest.py 200 1 3
    testGP(numberToResult, onlyInDict, dictDensity)