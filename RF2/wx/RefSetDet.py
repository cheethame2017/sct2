#Boa:Frame:Frame2

import wx
import wx.grid
import SCTRF2FrameFuncsMeta
import SCTRF2mw
from SCTRF2config import *
from SCTRF2Helpers import *

def create(parent):
    return Frame2(parent)

[wxID_FRAME2, wxID_FRAME2ENTRYDETAILSGRID, wxID_FRAME2FOCUSGRID, 
 wxID_FRAME2GENPURPOSEGAUGE, wxID_FRAME2PANEL1, wxID_FRAME2REFSETSGRID, 
 wxID_FRAME2STATUSBAR1, 
] = [wx.NewId() for _init_ctrls in range(7)]

class Frame2(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_FRAME2, name=u'Frame2', parent=prnt,
              pos=wx.Point(511, 252), size=wx.Size(1170, 563),
              style=wx.DEFAULT_FRAME_STYLE, title=u'RefSet Entry Details')
        self.SetClientSize(wx.Size(1170, 563))
        self.Bind(wx.EVT_ACTIVATE, self.OnFrame2Activate)
        self.Bind(wx.EVT_CLOSE, self.OnFrame2Close)

        self.statusBar1 = wx.StatusBar(id=wxID_FRAME2STATUSBAR1,
              name='statusBar1', parent=self, style=0)
        self.SetStatusBar(self.statusBar1)

        self.panel1 = wx.Panel(id=wxID_FRAME2PANEL1, name='panel1', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(1170, 538),
              style=wx.TAB_TRAVERSAL)

        self.refsetsGrid = wx.grid.Grid(id=wxID_FRAME2REFSETSGRID,
              name=u'RefsetsGrid', parent=self.panel1, pos=wx.Point(10, 90),
              size=wx.Size(1150, 152), style=0)
        self.refsetsGrid.SetColLabelSize(20)
        self.refsetsGrid.SetColMinimalAcceptableWidth(1)
        self.refsetsGrid.SetRowLabelSize(0)
        self.refsetsGrid.SetDefaultRowSize(20)
        self.refsetsGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnrefsetsGridCellLeftClick)

        self.genPurposeGauge = wx.Gauge(id=wxID_FRAME2GENPURPOSEGAUGE,
              name=u'genPurposeGauge', parent=self.panel1, pos=wx.Point(10, 10),
              range=100, size=wx.Size(1150, 8), style=wx.GA_HORIZONTAL)

        self.FocusGrid = wx.grid.Grid(id=wxID_FRAME2FOCUSGRID,
              name=u'FocusGrid', parent=self.panel1, pos=wx.Point(10, 20),
              size=wx.Size(1150, 56), style=0)
        self.FocusGrid.SetRowLabelSize(1)
        self.FocusGrid.SetColLabelSize(20)
        self.FocusGrid.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Sans'))
        self.FocusGrid.SetDefaultRowSize(20)
        self.FocusGrid.SetMinSize(wx.Size(200, 20))
        self.FocusGrid.SetDefaultCellOverflow(True)
        self.FocusGrid.SetColMinimalAcceptableWidth(0)
        self.FocusGrid.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Sans'))

        self.EntryDetailsGrid = wx.grid.Grid(id=wxID_FRAME2ENTRYDETAILSGRID,
              name=u'EntryDetailsGrid', parent=self.panel1, pos=wx.Point(10,
              250), size=wx.Size(1144, 300), style=0)
        self.EntryDetailsGrid.SetRowLabelSize(1)
        self.EntryDetailsGrid.SetColLabelSize(20)
        self.EntryDetailsGrid.SetDefaultRowSize(20)
        self.EntryDetailsGrid.SetMinSize(wx.Size(200, 20))
        self.EntryDetailsGrid.SetDefaultCellOverflow(True)
        self.EntryDetailsGrid.SetColMinimalAcceptableWidth(0)

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnFrame2Activate(self, event):
        SCTRF2FrameFuncsMeta.OnFrame2Activate(self, event)
        
    def OnrefsetsGridCellLeftClick(self, event):
        row = event.GetRow()
        column = 0
        if self.refsetsGrid.GetCellBackgroundColour(row,column) == wx.WHITE:
            value = self.refsetsGrid.GetCellValue(row, column)
            if not value == "":
                SCTRF2FrameFuncsMeta.OnGrid2Click(self, event, tuplify(value))

    def OnFrame2Close(self, event):
        print "closing refsets window"
        SCTRF2mw.resetRefsetRunning(self)
        # self.Destroy()
        self.Hide()


if __name__ == '__main__':
    app = wx.PySimpleApp()
    frame = create(None)
    frame.Show()

    # app.MainLoop()
