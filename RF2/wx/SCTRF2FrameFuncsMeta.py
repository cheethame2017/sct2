from SCTRF2Helpers import *
from SCTRF2config import *
import wx


def OnFrame2Activate(self, event):
    # callId = ('138875005',)
    callId = readInSwap()

    #focus
    self.FocusGrid.CreateGrid(1,7)
    self.FocusGrid.SetColLabelValue(0,"Module")
    self.FocusGrid.SetColLabelValue(1,"Ef.Time")
    self.FocusGrid.SetColLabelValue(2,"Active")
    self.FocusGrid.SetColLabelValue(3,"Defn.")
    self.FocusGrid.SetColLabelValue(4,"Inac. Reason")
    self.FocusGrid.SetColLabelValue(5,"ID")
    self.FocusGrid.SetColLabelValue(6,"Term")
    self.FocusGrid.SetColSize(0,150)
    self.FocusGrid.SetColSize(1,80)
    self.FocusGrid.SetColSize(2,60)
    self.FocusGrid.SetColSize(3,80)
    self.FocusGrid.SetColSize(4,0)
    self.FocusGrid.SetColSize(5,150)
    self.FocusGrid.SetColSize(6,700)
    self.FocusGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    fillFocusGrid(self, event, callId)

    # refsets
    allRefs = []
    refsetType = ['simplerefset',
                'srefset',
                'ssrefset',
                'crefset',
                'cirefset',
                'icrefset',
                'ccirefset',
                'iisssccrefset',
                'iissscirefset',
                'iissscrefset']
    for rType in refsetType:
        refs = refSetFromConIdPlusMeta_IN(callId[0], rType, cfg('REFSETS_TYPE'))
        for ref in refs:
            allRefs.append(ref)
    counter = len(allRefs) * 2
    self.refsetsGrid.CreateGrid(counter,3)
    self.refsetsGrid.SetColLabelValue(0,"ID or Mod")
    self.refsetsGrid.SetColLabelValue(1,"Active")
    self.refsetsGrid.SetColLabelValue(2,"Term or ET")
    self.refsetsGrid.SetColSize(0,150)
    self.refsetsGrid.SetColSize(1,1)
    self.refsetsGrid.SetColSize(2,1000)
    self.refsetsGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    # basic refsets
    fillRefsetsGrid(self, event, callId)

    # entry details
    # no need to determine rowNumber - empty to start
    self.EntryDetailsGrid.CreateGrid(0,4)
    self.EntryDetailsGrid.SetColLabelValue(0,"Field or Mod")
    self.EntryDetailsGrid.SetColLabelValue(1,"ID or Value")
    self.EntryDetailsGrid.SetColLabelValue(2,"Active")
    self.EntryDetailsGrid.SetColLabelValue(3,"Term or ET")
    self.EntryDetailsGrid.SetColSize(0,150)
    self.EntryDetailsGrid.SetColSize(1,150)
    self.EntryDetailsGrid.SetColSize(2,1)
    self.EntryDetailsGrid.SetColSize(3,1000)
    self.EntryDetailsGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)


def OnFrameActivate(self, event):
    callId = ('138875005',)
    #supertypes - initialise grid
    self.ParentGrid.CreateGrid(0,7)
    self.ParentGrid.SetColLabelValue(0,"Module")
    self.ParentGrid.SetColLabelValue(1,"Ef.Time")
    self.ParentGrid.SetColLabelValue(2,"Type")
    self.ParentGrid.SetColLabelValue(3,"Logic")
    self.ParentGrid.SetColLabelValue(4,"Active")
    self.ParentGrid.SetColLabelValue(5,"ID")
    self.ParentGrid.SetColLabelValue(6,"Term")
    self.ParentGrid.SetColSize(0,150)
    self.ParentGrid.SetColSize(1,80)
    self.ParentGrid.SetColSize(2,160)
    self.ParentGrid.SetColSize(3,70)
    if cfg('ROLE_DISPLAY_TYPE') == 0 or cfg('ROLE_DISPLAY_TYPE') == 2:
        self.ParentGrid.SetColSize(4,60)
    else:
        self.ParentGrid.SetColSize(4,0)
    self.ParentGrid.SetColSize(5,150)
    self.ParentGrid.SetColSize(6,435)
    self.ParentGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    self.ParentGrid.Refresh()

    #roles - initialise grid
    self.RoleGrid.CreateGrid(0,11)
    self.RoleGrid.SetColLabelValue(0," ")
    self.RoleGrid.SetColLabelValue(1,"Module")
    self.RoleGrid.SetColLabelValue(2,"Ef.Time")
    self.RoleGrid.SetColLabelValue(3,"Type")
    self.RoleGrid.SetColLabelValue(4,"Logic")
    self.RoleGrid.SetColLabelValue(5,"Active")
    self.RoleGrid.SetColLabelValue(6,"Att ID")
    self.RoleGrid.SetColLabelValue(7,"Att Term")
    self.RoleGrid.SetColLabelValue(8," ")
    self.RoleGrid.SetColLabelValue(9,"Val ID")
    self.RoleGrid.SetColLabelValue(10,"Val Term")
    self.RoleGrid.SetColSize(0,0)
    self.RoleGrid.SetColSize(1,150)
    self.RoleGrid.SetColSize(2,80)
    self.RoleGrid.SetColSize(3,160)
    self.RoleGrid.SetColSize(4,60)
    if cfg('ROLE_DISPLAY_TYPE') == 0 or cfg('ROLE_DISPLAY_TYPE') == 2:
        self.RoleGrid.SetColSize(5,60)
    else:
        self.RoleGrid.SetColSize(5,0)
    self.RoleGrid.SetColSize(6,140)
    self.RoleGrid.SetColSize(7,310)
    self.RoleGrid.SetColSize(8,20)
    self.RoleGrid.SetColSize(9,140)
    self.RoleGrid.SetColSize(10,435)
    self.RoleGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    self.RoleGrid.Refresh()

    # search initialise grid
    self.SearchReportGrid.CreateGrid(0,4)
    self.SearchReportGrid.SetColLabelValue(0," ")
    self.SearchReportGrid.SetColLabelValue(1," ")
    self.SearchReportGrid.SetColLabelValue(2,"ID")
    self.SearchReportGrid.SetColLabelValue(3,"Term")
    self.SearchReportGrid.SetColSize(0,0)
    self.SearchReportGrid.SetColSize(1,0)
    self.SearchReportGrid.SetColSize(2,140)
    self.SearchReportGrid.SetColSize(3,315)
    self.SearchReportGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    self.SearchReportGrid.Refresh()

    #focus
    self.FocusGrid.CreateGrid(1,7)
    self.FocusGrid.SetColLabelValue(0,"Module")
    self.FocusGrid.SetColLabelValue(1,"Ef.Time")
    self.FocusGrid.SetColLabelValue(2,"Active")
    self.FocusGrid.SetColLabelValue(3,"Defn.")
    self.FocusGrid.SetColLabelValue(4,"Inac. Reason")
    self.FocusGrid.SetColLabelValue(5,"ID")
    self.FocusGrid.SetColLabelValue(6,"Term")
    self.FocusGrid.SetColSize(0,150)
    self.FocusGrid.SetColSize(1,80)
    self.FocusGrid.SetColSize(2,60)
    self.FocusGrid.SetColSize(3,80)
    self.FocusGrid.SetColSize(4,0)
    self.FocusGrid.SetColSize(5,150)
    self.FocusGrid.SetColSize(6,500)
    self.FocusGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    #focus
    fillFocusGrid(self, event, callId)

    #children
    childList = []
    childList = childrenFromIdWithPlus_IN(callId)  # call interface neutral code
    num = len(childList)
    # Give the Grid the right number of rows and three columns.
    # Set the titles for the columns and size them.
    self.ChildGrid.CreateGrid(num,4)
    self.ChildGrid.SetColLabelValue(0," ")
    self.ChildGrid.SetColLabelValue(1," ")
    self.ChildGrid.SetColLabelValue(2,"ID")
    self.ChildGrid.SetColLabelValue(3,"Term")
    self.ChildGrid.SetColSize(0,0)
    self.ChildGrid.SetColSize(1,20)
    self.ChildGrid.SetColSize(2,150)
    self.ChildGrid.SetColSize(3,400)
    self.ChildGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    #children
    fillChildGrid(self, event, callId)


    # terms
    termNum = 2
    synList = []
    synList = synsFromConIdPlusMeta_IN(callId[0])
    termNum += len(synList)
    try:
        defList = defnFromConIdPlusMeta_IN(callId[0])
        termNum += len(defList)
    except:
        zz = 0
    self.DescriptionGrid.CreateGrid(termNum,9)
    self.DescriptionGrid.SetColLabelValue(0,"Des. Mod.")
    self.DescriptionGrid.SetColLabelValue(1,"Des. ET")
    self.DescriptionGrid.SetColLabelValue(2,"ICS")
    self.DescriptionGrid.SetColLabelValue(3,"Active")
    self.DescriptionGrid.SetColLabelValue(4,"Ref. Mod.")
    self.DescriptionGrid.SetColLabelValue(5,"Ref. ET")
    self.DescriptionGrid.SetColLabelValue(6,"Type")
    self.DescriptionGrid.SetColLabelValue(7,"ID")
    self.DescriptionGrid.SetColLabelValue(8,"Term")
    self.DescriptionGrid.SetColSize(0,150)
    self.DescriptionGrid.SetColSize(1,80)
    self.DescriptionGrid.SetColSize(2,220)
    if cfg('TERM_DISPLAY_TYPE') == 0 or cfg('TERM_DISPLAY_TYPE') == 2:
        self.DescriptionGrid.SetColSize(3,60)
    else:
        self.DescriptionGrid.SetColSize(3,0)
    self.DescriptionGrid.SetColSize(4,130)
    self.DescriptionGrid.SetColSize(5,80)
    self.DescriptionGrid.SetColSize(6,145)
    self.DescriptionGrid.SetColSize(7,150)
    self.DescriptionGrid.SetColSize(8,600)
    self.DescriptionGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    fillDescriptionGrid(self, event, callId)

    fillRoleGrid(self, event, callId)

    # historical origins
    if cfg('HISTORICAL_ORIGINS') ==1:
        a = historicalOriginsFromConId_IN(callId[0], cfg('HISTORICAL_ORIGINS_TYPE'))
        counter = 0
        if len(a) > 0:
            tempId2 = '1'
            for row in a:
                tempId = row[0]
                if tempId != tempId2:
                    counter += 1
                counter +=2
                tempId2 = tempId
        self.historicalOriginsGrid.CreateGrid(counter,3)
    else:
        self.historicalOriginsGrid.CreateGrid(0,3)
    self.historicalOriginsGrid.SetColLabelValue(0,"ID or Mod")
    self.historicalOriginsGrid.SetColLabelValue(1,"Active")
    self.historicalOriginsGrid.SetColLabelValue(2,"Term or ET")
    self.historicalOriginsGrid.SetColSize(0,150)
    self.historicalOriginsGrid.SetColSize(1,1)
    self.historicalOriginsGrid.SetColSize(2,600)
    self.historicalOriginsGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)


    # refsets
    allRefs = []
    refsetType = ['simplerefset',
                'srefset',
                'ssrefset',
                'crefset',
                'cirefset',
                'icrefset',
                'ccirefset',
                'iisssccrefset',
                'iissscirefset',
                'iissscrefset']
    for rType in refsetType:
        refs = refSetFromConIdPlusMeta_IN(callId[0], rType, cfg('REFSETS_TYPE'))
        for ref in refs:
            allRefs.append(ref)
    counter = len(allRefs) * 2
    self.refsetsGrid.CreateGrid(counter,3)
    self.refsetsGrid.SetColLabelValue(0,"ID or Mod")
    self.refsetsGrid.SetColLabelValue(1,"Active")
    self.refsetsGrid.SetColLabelValue(2,"Term or ET")
    self.refsetsGrid.SetColSize(0,150)
    self.refsetsGrid.SetColSize(1,1)
    self.refsetsGrid.SetColSize(2,600)
    self.refsetsGrid.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
    # basic refsets
    fillRefsetsGrid(self, event, callId)

    if cfg('HISTORICAL_ORIGINS') == 1:
        fillHistoricalOriginsGrid(self, event, callId)
    else:
        fillHistoricalOriginsGrid(self, event, '370137002') # uk namespace - need better dummy


    # set focus back to search
    self.SearchText.SetFocus()


def OnGrid2Click(self, event, callId):
    memberId = readInSwap()
    refsetId = callId
    fillRefSetEntriesGrid(self, memberId, refsetId)

def OnGridClick(self, event, callId):
    # update swap file
    focusSwap_mwWrite(callId)
    #supertypes
    fillParentGrid(self, event, callId)
    #focus
    fillFocusGrid(self, event, callId)
    #children
    fillChildGrid(self, event, callId)
    # descriptions
    fillDescriptionGrid(self, event, callId)
    # roles
    fillRoleGrid(self, event, callId)
    # historical origins
    if cfg('HISTORICAL_ORIGINS') == 1:
        fillHistoricalOriginsGrid(self, event, callId)
    else:
        fillHistoricalOriginsGrid(self, event, '370137002') # uk namespace - need better dummy

    # basic refsets
    fillRefsetsGrid(self, event, callId)
    # update breadcrumbs
    if not callId[0] == '138875005': #except if root
        breadcrumbs_mwWrite(callId)
    # refresh searchlist with breadcrumbs (temporary)
    if cfg('BREADCRUMB_TYPE') == 1:
        breadcrumbs_mwRead(self)


def fillParentGrid(self, event, callId):
    parentList = []
    parentList = parentsFromIdPlusMeta_IN(callId, cfg('ROLE_DISPLAY_TYPE'))  # call interface neutral code
    parentNum = len(parentList)
    termLen = 0
    for listParent in parentList:
        if not listParent is None:
            if len(listParent[4]) > termLen:
                termLen = len(listParent[4])
    current = self.ParentGrid.GetNumberRows()
    if parentNum < current:
        self.ParentGrid.DeleteRows(0, current-parentNum, True)
    if current < parentNum:
        self.ParentGrid.AppendRows(parentNum-current)
    if termLen > 60:
        self.ParentGrid.SetColSize(6,termLen * 7.4)
    else:
        self.ParentGrid.SetColSize(6,435)
    i = 0
    for listParent in parentList:
        if not listParent is None:
            self.ParentGrid.SetCellBackgroundColour(i,0, wx.LIGHT_GREY)
            self.ParentGrid.SetCellBackgroundColour(i,1, wx.LIGHT_GREY)
            self.ParentGrid.SetCellBackgroundColour(i,2, wx.LIGHT_GREY)
            self.ParentGrid.SetCellBackgroundColour(i,3, wx.LIGHT_GREY)
            self.ParentGrid.SetCellBackgroundColour(i,4, wx.LIGHT_GREY)
            if cfg('ROLE_DISPLAY_TYPE') == 0 or cfg('ROLE_DISPLAY_TYPE') == 2:
                self.ParentGrid.SetColSize(4,60)
            else:
                self.ParentGrid.SetColSize(4,0)
            self.ParentGrid.SetCellTextColour(i,0, checkModForGridByTerm(listParent[5]))
            self.ParentGrid.SetCellValue(i,0,str(modSimple(listParent[5].encode("utf-8"))))
            self.ParentGrid.SetCellValue(i,1,str(listParent[6]))
            self.ParentGrid.SetCellValue(i,2,str(listParent[7]))
            self.ParentGrid.SetCellValue(i,3,str(listParent[8]))
            self.ParentGrid.SetCellValue(i,4,str(listParent[9]))
            self.ParentGrid.SetCellFont(i,5, wx.Font(10, wx.SWISS,
                checkStatusForGrid(listParent[3]), wx.NORMAL, False, u'Sans'))
            self.ParentGrid.SetCellTextColour(i,5, checkDefForGrid(listParent[3]))
            self.ParentGrid.SetCellValue(i, 5,str(listParent[3]))
            self.ParentGrid.SetCellFont(i,6, wx.Font(10, wx.SWISS,
                checkStatusForGrid(listParent[3]), wx.NORMAL, False, u'Sans'))
            self.ParentGrid.SetCellTextColour(i,6, checkModForGrid(listParent[3]))
            self.ParentGrid.SetCellValue(i,6,str(listParent[4].encode("utf-8")))
            i = i + 1
    self.ParentGrid.Refresh()


def fillFocusGrid(self, event, callId):
    termLen = len(fsnFromConId_IN(callId[0])[1])
    if termLen > 70:
        self.FocusGrid.SetColSize(6,termLen * 7.4)
    else:
        self.FocusGrid.SetColSize(6,500)
    conMeta = []
    conMeta = conMetaFromConId_IN(callId[0])
    self.FocusGrid.SetCellBackgroundColour(0,0, wx.LIGHT_GREY)
    self.FocusGrid.SetCellBackgroundColour(0,1, wx.LIGHT_GREY)
    self.FocusGrid.SetCellBackgroundColour(0,2, wx.LIGHT_GREY)
    self.FocusGrid.SetCellBackgroundColour(0,3, wx.LIGHT_GREY)
    self.FocusGrid.SetCellBackgroundColour(0,4, wx.LIGHT_GREY)
    self.FocusGrid.SetCellTextColour(0,0, checkModForGridByTerm(conMeta[0]))
    self.FocusGrid.SetCellValue(0,0, str(modSimple(conMeta[0].encode("utf-8"))))
    self.FocusGrid.SetCellValue(0,1, conMeta[2])
    self.FocusGrid.SetCellValue(0,2, str(conMeta[1]))
    self.FocusGrid.SetCellValue(0,3, conMeta[3])
    if len(conMeta) == 4:
        self.FocusGrid.SetColSize(4,1)
    else:
        self.FocusGrid.SetColSize(4, 100 + (len(conMeta[4])))
        self.FocusGrid.SetCellValue(0,4, conMeta[4])
    self.FocusGrid.SetCellFont(0,5, wx.Font(10, wx.SWISS,
                checkStatusForGrid(callId[0]), wx.NORMAL, False, u'Sans'))
    self.FocusGrid.SetCellTextColour(0,5, checkDefForGrid(callId[0]))
    self.FocusGrid.SetCellValue(0,5,str(callId[0]))
    self.FocusGrid.SetCellFont(0,6, wx.Font(10, wx.SWISS,
                checkStatusForGrid(callId[0]), wx.NORMAL, False, u'Sans'))
    self.FocusGrid.SetCellTextColour(0,6, checkModForGrid(callId[0]))
    self.FocusGrid.SetCellValue(0,6,str(fsnFromConId_IN(callId[0])[1].encode("utf-8")))
    self.FocusGrid.Refresh()

def fillChildGrid(self, event, callId):
    childList = []
    childList = childrenFromIdWithPlus_IN(callId)  # call interface neutral code
    childNum = len(childList)
    generalGaugeInit(self, childNum)
    termLen = 0
    i = 0
    for listChild in childList:
        generalGaugeUpdate(self, i)
        if not listChild is None:
            if len(listChild[0]) > termLen:
                termLen = len(listChild[0])
        i += 1
    generalGaugeReset(self, 0)
    current = self.ChildGrid.GetNumberRows()
    if childNum < current:
        self.ChildGrid.DeleteRows(0, current-childNum, True)
    if current < childNum:
        self.ChildGrid.AppendRows(childNum-current)
    if termLen > 60:
        self.ChildGrid.SetColSize(3,termLen * 7.4)
    else:
        self.ChildGrid.SetColSize(3,415)
    generalGaugeInit(self, childNum)
    i = 0
    for listChild in childList:
        if not listChild is None:
            self.ChildGrid.SetCellValue(i,0,str(i))
            self.ChildGrid.SetCellValue(i,1,str(listChild[1]))
            self.ChildGrid.SetCellTextColour(i,2, checkDefForGrid(listChild[2]))
            self.ChildGrid.SetCellValue(i,2,str(listChild[2]))
            self.ChildGrid.SetCellFont(i,3, wx.Font(10, wx.SWISS,
                checkStatusForGrid(listChild[2]), wx.NORMAL, False, u'Sans'))
            self.ChildGrid.SetCellTextColour(i,3, checkModForGrid(listChild[2]))
            self.ChildGrid.SetCellValue(i,3,str(listChild[0].encode("utf-8")))
            i = i + 1
            generalGaugeUpdate(self, i)

    generalGaugeReset(self, 0)
    self.ChildGrid.Refresh()


def fillDescriptionGrid(self, event, callId):
    allterms = []
    if (cfg('TERM_DISPLAY_TYPE') == 1) or (cfg('TERM_DISPLAY_TYPE') == 2):
        termNum = 2
        myterm = fsnFromConIdPlusMeta_IN(callId[0])
        allterms.append(myterm[1])
        myterm = ptFromConIdPlusMeta_IN(callId[0])
        allterms.append(myterm[1])
        myterm = synsFromConIdPlusMeta_IN(callId[0])
        if len(myterm) > 0:
            for term in myterm:
                allterms.append(term[1])
                termNum += 1
        try:
            myterm = defnFromConIdPlusMeta_IN(callId[0])
            if len(myterm) > 0:
                allterms.append(myterm[1])
                termNum += 1
        except:
            zz = 0
    # insert code here to find the inactive terms [part 1]
    if (cfg('TERM_DISPLAY_TYPE') == 2) or (cfg('TERM_DISPLAY_TYPE') == 0):
        if cfg('TERM_DISPLAY_TYPE') == 0:
            termNum = 0
        inactiveTerms = []
        inactiveTerms = inactiveTermsFromConIdPlusMeta_IN(callId[0])
        if len(inactiveTerms) > 0:
            for term in inactiveTerms:
                allterms.append(term[1])
                termNum += 1
    termLen = 0
    for term in allterms:
        if len(term) > termLen:
            termLen = len(term)
    current = self.DescriptionGrid.GetNumberRows()
    if termNum < current:
        self.DescriptionGrid.DeleteRows(0, current-termNum, True)
    if current < termNum:
        self.DescriptionGrid.AppendRows(termNum-current)
    if termLen > 60:
        self.DescriptionGrid.SetColSize(8,termLen * 7.4)
    else:
        self.DescriptionGrid.SetColSize(8,600)
    # self.DescriptionGrid.CreateGrid(termNum,8)
    allRows = []
    if (cfg('TERM_DISPLAY_TYPE') == 1) or (cfg('TERM_DISPLAY_TYPE') == 2):
        myterm = fsnFromConIdPlusMeta_IN(callId[0])
        allRows.append(tuplify("Preferred FSN"))
        newterm = ()
        newterm = myterm + tuplify(1)
        allRows.append(newterm)
        myterm = ptFromConIdPlusMeta_IN(callId[0])
        allRows.append(tuplify("Preferred synonym"))
        newterm = ()
        newterm = myterm + tuplify(1)
        allRows.append(newterm)
        myterm = synsFromConIdPlusMeta_IN(callId[0])
        if len(myterm) > 0:
            # myterm.sort(key=lambda x: x[1])
            for term in myterm:
                allRows.append(tuplify("Acceptable synonym"))
                newterm = ()
                newterm = term + tuplify(1)
                allRows.append(newterm)
                termNum += 1
        try:
            myterm = defnFromConIdPlusMeta_IN(callId[0])
            if len(myterm) > 0:
                allRows.append(tuplify("Definition"))
                newterm = ()
                newterm = myterm + tuplify(1)
                allRows.append(newterm)
                termNum += 1
        except:
            zz = 0
    if (cfg('TERM_DISPLAY_TYPE') == 2) or (cfg('TERM_DISPLAY_TYPE') == 0):
        myterm = inactiveTermsFromConIdPlusMeta_IN(callId[0])
        if len(myterm) > 0:
            # myterm.sort(key=lambda x: x[1])
            for term in myterm:
                allRows.append(tuplify(dTypeTermFromId(term[2])))
                tempTerm = [term[0],term[1],pterm(tuplify(term[3])),term[4],pterm(tuplify(term[5])),"","", term[6]]
                allRows.append(tempTerm)
                termNum += 1
    i = 0
    generalGaugeInit(self, len(allRows))
    for row in allRows:
        if len(row) == 1:
            self.DescriptionGrid.SetCellValue(i,6,row[0])
        else:
            self.DescriptionGrid.SetCellBackgroundColour(i,0, wx.LIGHT_GREY)
            self.DescriptionGrid.SetCellBackgroundColour(i,1, wx.LIGHT_GREY)
            self.DescriptionGrid.SetCellBackgroundColour(i,2, wx.LIGHT_GREY)
            self.DescriptionGrid.SetCellBackgroundColour(i,3, wx.LIGHT_GREY)
            self.DescriptionGrid.SetCellBackgroundColour(i,4, wx.LIGHT_GREY)
            self.DescriptionGrid.SetCellBackgroundColour(i,5, wx.LIGHT_GREY)
            if cfg('TERM_DISPLAY_TYPE') == 0 or cfg('TERM_DISPLAY_TYPE') == 2:
                self.DescriptionGrid.SetColSize(3,60)
            else:
                self.DescriptionGrid.SetColSize(3,0)
            self.DescriptionGrid.SetCellTextColour(i,0, checkDescModForGrid(row[2]))
            self.DescriptionGrid.SetCellValue(i,0,str(modSimple(row[2].encode("utf-8"))))
            self.DescriptionGrid.SetCellValue(i,1,row[3])
            self.DescriptionGrid.SetCellValue(i,2,row[4])
            self.DescriptionGrid.SetCellValue(i,3,str(row[7]))
            self.DescriptionGrid.SetCellTextColour(i,4, checkRefsModForGrid(row[5]))
            self.DescriptionGrid.SetCellValue(i,4,str(modSimple(row[5].encode("utf-8"))))
            self.DescriptionGrid.SetCellValue(i,5,row[6])
            self.DescriptionGrid.SetCellValue(i,7,row[0])
            self.DescriptionGrid.SetCellTextColour(i,8, checkDescModForGrid(row[2]))
            self.DescriptionGrid.SetCellValue(i,8,row[1].encode("utf-8"))
            # self.DescriptionGrid.SetCellValue(i,7, unicode(row[1], "utf-8"))
            i += 1
            generalGaugeUpdate(self, i)

    generalGaugeReset(self, 0)
    self.DescriptionGrid.Refresh()


def fillRoleGrid(self, event, callId):
    roleList = []
    roleList = groupedRolesFromConIdPlusMeta_IN(callId[0],cfg('ROLE_DISPLAY_TYPE'))
    childList = []
    childList = childrenFromConIdPlusMeta_IN(callId[0],cfg('ROLE_DISPLAY_TYPE'), cfg('INVERSE_ROLES'))
    roleList = roleList + childList
    #print roleList
    roleNum = 0
    for roleGroup in roleList:
        for role in roleGroup:
            # if only showing active rows
            if cfg('ROLE_DISPLAY_TYPE') == 1:
                if isinstance(role, int):
                    if role > -1:
                        roleNum += 1
                else:
                    if not role[1] == "116680003":
                        roleNum += 1
            # if showing inactive rows
            if cfg('ROLE_DISPLAY_TYPE') == 0 or cfg('ROLE_DISPLAY_TYPE') == 2:
                if isinstance(role, int):
                    if role == -2 or role > -1:
                        roleNum += 1
                elif role[0] > 0:
                    roleNum += 1
                elif role[0] == 0 and not role[1] == "116680003":
                    roleNum += 1
                else:
                    if role[0] == -2:
                        roleNum += 1
    current = self.RoleGrid.GetNumberRows()
    # print 'current=' + str(current), 'roleNum=' + str(roleNum)
    if roleNum < current:
        self.RoleGrid.DeleteRows(0, current - roleNum, True)
    if current < roleNum:
        self.RoleGrid.AppendRows(roleNum - current)
    i = 0
    generalGaugeInit(self, roleNum)
    for roleGroup in roleList:
        for role in roleGroup:
            if isinstance(role, int):
                for n in range(2, 11):
                    self.RoleGrid.SetCellValue(i, n, "")
                for n in range(1, 6):
                    self.RoleGrid.SetCellBackgroundColour(i, n, wx.WHITE)
                if role == -2:
                    self.RoleGrid.SetCellValue(i,0,str(i))
                    if cfg('INVERSE_ROLES') == 0:
                        self.RoleGrid.SetCellValue(i,1,"Children:")
                    else:
                        self.RoleGrid.SetCellValue(i,1,"Children & Inv. roles:")
                    i += 1
                if role == 0:
                    self.RoleGrid.SetCellValue(i,0,str(i))
                    self.RoleGrid.SetCellValue(i,1,"Ungrouped:")
                    i += 1
                elif role > 0:
                    self.RoleGrid.SetCellValue(i,0,str(i))
                    self.RoleGrid.SetCellValue(i,1,"Group:")
                    i += 1
            else:
                if not role[1] == "116680003" or (role[1] == "116680003" and role[0] == -2):
                    # print role[1], role[2], role[3], role[4]
                    self.RoleGrid.SetCellBackgroundColour(i,1, wx.LIGHT_GREY)
                    self.RoleGrid.SetCellBackgroundColour(i,2, wx.LIGHT_GREY)
                    self.RoleGrid.SetCellBackgroundColour(i,3, wx.LIGHT_GREY)
                    self.RoleGrid.SetCellBackgroundColour(i,4, wx.LIGHT_GREY)
                    self.RoleGrid.SetCellBackgroundColour(i,5, wx.LIGHT_GREY)
                    if cfg('ROLE_DISPLAY_TYPE') == 0 or cfg('ROLE_DISPLAY_TYPE') == 2:
                        self.RoleGrid.SetColSize(5,60)
                    else:
                        self.RoleGrid.SetColSize(5,0)
                    self.RoleGrid.SetCellTextColour(i,1, checkModForGridByTerm(role[5]))
                    self.RoleGrid.SetCellValue(i,1,modSimple(role[5].encode("utf-8")))
                    self.RoleGrid.SetCellValue(i,2,role[6])
                    self.RoleGrid.SetCellValue(i,3,role[7])
                    self.RoleGrid.SetCellValue(i,4,role[8])
                    self.RoleGrid.SetCellValue(i,5,str(role[9]))
                    self.RoleGrid.SetCellValue(i,6,role[1])
                    self.RoleGrid.SetCellFont(i,7, wx.Font(10, wx.SWISS,
                    checkStatusForGrid(role[1]), wx.NORMAL, False, u'Sans'))
                    self.RoleGrid.SetCellTextColour(i,7, checkModForGrid(role[1]))
                    self.RoleGrid.SetCellValue(i,7,role[2].encode("utf-8"))
                    self.RoleGrid.SetCellValue(i,8,"=")
                    self.RoleGrid.SetCellTextColour(i,9, checkDefForGrid(role[3]))
                    self.RoleGrid.SetCellValue(i,9,role[3])
                    self.RoleGrid.SetCellFont(i,10, wx.Font(10, wx.SWISS,
                    checkStatusForGrid(role[3]), wx.NORMAL, False, u'Sans'))
                    self.RoleGrid.SetCellTextColour(i,10, checkModForGrid(role[3]))
                    self.RoleGrid.SetCellValue(i,10,role[4].encode("utf-8"))
                    i += 1
                    generalGaugeUpdate(self, i)

    if cfg('HISTORICAL_ORIGINS') == 1:
        generalGaugeReset(self, 0)
    else:
        generalGaugeReset(self, 1)
    self.RoleGrid.Refresh()

def searchOnText(self, searchText, inOption):
    searchList = []
    generalGaugeInit(self, 1)
    searchList = searchOnText_IN(searchText, inOption)
    if cfg('SEARCH_ORDER') == 2:
        searchList.sort(key=lambda x: x[0])
    fillSearchGrid(self, searchList)

def fillSearchGrid(self, searchList):
    # SearchReportGrid
    searchNum = len(searchList) - 1
    generalGaugeInit(self, searchNum)
    termLen = len(searchList[searchNum][0])
    current = self.SearchReportGrid.GetNumberRows()
    if searchNum < current:
        self.SearchReportGrid.DeleteRows(0, current-searchNum, True)
    if current < searchNum:
        self.SearchReportGrid.AppendRows(searchNum-current)
    self.SearchReportGrid.SetColLabelValue(0," ")
    self.SearchReportGrid.SetColLabelValue(1," ")
    self.SearchReportGrid.SetColLabelValue(2,"ID")
    self.SearchReportGrid.SetColLabelValue(3,"Term")
    self.SearchReportGrid.SetColSize(0,0)
    self.SearchReportGrid.SetColSize(1,0)
    self.SearchReportGrid.SetColSize(2,200)
    if termLen > 60:
        self.SearchReportGrid.SetColSize(3,termLen * 7.4)
    else:
        self.SearchReportGrid.SetColSize(3,600)

    i = -1
    for listItem in searchList:
        if not listItem is None:
            if i > -1:
                self.SearchReportGrid.SetCellValue(i,0,str(i))
                self.SearchReportGrid.SetCellValue(i,1,str(""))
                normalItalicStatus = wx.ITALIC
                normalItalicStatus = checkStatusForGrid(listItem[1])
                self.SearchReportGrid.SetCellFont(i,2, wx.Font(10, wx.SWISS,
                normalItalicStatus, wx.NORMAL, False, u'Sans'))
                self.SearchReportGrid.SetCellTextColour(i,2, checkDefForGrid(listItem[1]))
                self.SearchReportGrid.SetCellValue(i,2,str(listItem[1]))
                self.SearchReportGrid.SetCellFont(i,3, wx.Font(10, wx.SWISS,
                normalItalicStatus, wx.NORMAL, False, u'Sans'))
                self.SearchReportGrid.SetCellTextColour(i,3, checkModForGrid(listItem[1]))
                if not listItem[0].find(" (*)") == -1:
                    self.SearchReportGrid.SetCellValue(i,3,str(listItem[0][0:listItem[0].find("(*)")].encode("utf-8")))
                else:
                    self.SearchReportGrid.SetCellValue(i,3,str(listItem[0].encode("utf-8")))
            i = i + 1
            generalGaugeUpdate(self, i)

    generalGaugeReset(self, 1)
    self.SearchReportGrid.Refresh()


def fillHistoricalOriginsGrid(self, event, callId):
    # print 'Historical origins for', callId, pterm(callId)
    a = historicalOriginsFromConId_IN(callId[0], cfg('HISTORICAL_ORIGINS_TYPE'))
    # calculate rows etc.
    counter = 0
    if len(a) > 0:
        generalGaugeInit(self, len(a))
        tempId2 = '1'
        for row in a:
            # print row
            tempId = row[0]
            if tempId != tempId2:
                counter += 1
            counter +=2
            tempId2 = tempId
    current = self.historicalOriginsGrid.GetNumberRows()
    if counter < current:
        self.historicalOriginsGrid.DeleteRows(0, current-counter, True)
    if current < counter:
        self.historicalOriginsGrid.AppendRows(counter-current)
    # fill the rows
    if len(a) > 0:
        i = 0
        tempId2 = '1'
        if (cfg('HISTORICAL_ORIGINS_TYPE') == 0) or (cfg('HISTORICAL_ORIGINS_TYPE') == 2):
            self.historicalOriginsGrid.SetColSize(1,60)
        else:
            self.historicalOriginsGrid.SetColSize(1,1)
        for row in a:
            tempId = row[0]
            if tempId != tempId2:
                # historical header
                # print row[0] + '\t' + pterm(tuplify(row[0]))
                self.historicalOriginsGrid.SetCellBackgroundColour(i,0, wx.WHITE)
                self.historicalOriginsGrid.SetCellBackgroundColour(i,1, wx.WHITE)
                self.historicalOriginsGrid.SetCellBackgroundColour(i,2, wx.WHITE)
                self.historicalOriginsGrid.SetCellFont(i,2, wx.Font(10, wx.SWISS,
                wx.NORMAL, wx.BOLD, False, u'Sans'))
                # self.historicalOriginsGrid.SetCellValue(i,1,str(row[0]))
                self.historicalOriginsGrid.SetCellValue(i,0,"")
                self.historicalOriginsGrid.SetCellValue(i,2,str(assocRefsetSimple(pterm(tuplify(row[0])))))
                i += 1
            # print row[1] + '\t' + pterm(tuplify(row[1]))
            # historical row
            self.historicalOriginsGrid.SetCellBackgroundColour(i,0, wx.WHITE)
            self.historicalOriginsGrid.SetCellBackgroundColour(i,1, wx.WHITE)
            self.historicalOriginsGrid.SetCellBackgroundColour(i,2, wx.WHITE)
            # statusNormalItalic = checkStatusForGrid(row[1])
            if row[6] == 0:
                statusNormalItalic = wx.ITALIC
            else:
                statusNormalItalic = wx.NORMAL
            self.historicalOriginsGrid.SetCellFont(i,0, wx.Font(10, wx.SWISS,
                    statusNormalItalic, wx.NORMAL, False, u'Sans'))
            self.historicalOriginsGrid.SetCellFont(i,2, wx.Font(10, wx.SWISS,
                    statusNormalItalic, wx.NORMAL, False, u'Sans'))
            modTextColour = checkModForGrid(row[1])
            self.historicalOriginsGrid.SetCellTextColour(i,2, modTextColour)
            self.historicalOriginsGrid.SetCellValue(i,0,row[1])
            self.historicalOriginsGrid.SetCellValue(i,2,pterm(tuplify(row[1])).encode("utf-8"))
            i += 1
            # print row[3], pterm(tuplify(row[3])), row[4]
            # historical metadata
            self.historicalOriginsGrid.SetCellBackgroundColour(i,0, wx.LIGHT_GREY)
            self.historicalOriginsGrid.SetCellBackgroundColour(i,1, wx.LIGHT_GREY)
            self.historicalOriginsGrid.SetCellBackgroundColour(i,2, wx.LIGHT_GREY)
            self.historicalOriginsGrid.SetCellTextColour(i,0, modTextColour)
            self.historicalOriginsGrid.SetCellValue(i,0,modSimple(pterm(tuplify(row[3]))))
            self.historicalOriginsGrid.SetCellValue(i,1,str(row[5]))
            self.historicalOriginsGrid.SetCellValue(i,2,row[4])
            i += 1
            tempId2 = tempId
            generalGaugeUpdate(self, i)

    generalGaugeReset(self, 1)
    self.historicalOriginsGrid.Refresh()


def fillRefsetsGrid(self, event, callId):
    allRefs = []
    refsetType = ['simplerefset',
                'srefset',
                'ssrefset',
                'crefset',
                'cirefset',
                'icrefset',
                'ccirefset',
                'iisssccrefset',
                'iissscirefset',
                'iissscrefset']

    # print "Refsets for:", callId, pterm(callId)

    for rType in refsetType:
        refs = refSetFromConIdPlusMeta_IN(callId[0], rType, cfg('REFSETS_TYPE'))
        for ref in refs:
            allRefs.append(ref)
            # print ref[0] + '\t' + ref[1] + '\n' + ref[3], ref[4]
    counter = len(allRefs) * 2
    current = self.refsetsGrid.GetNumberRows()
    if counter < current:
        self.refsetsGrid.DeleteRows(0, current-counter, True)
    if current < counter:
        self.refsetsGrid.AppendRows(counter-current)
    # fill the rows
    if len(allRefs) > 0:
        i = 0
        if (cfg('REFSETS_TYPE') == 0) or (cfg('REFSETS_TYPE') == 2):
            self.refsetsGrid.SetColSize(1,60)
        else:
            self.refsetsGrid.SetColSize(1,1)
        for row in allRefs:
            # print row[1] + '\t' + pterm(tuplify(row[1]))
            # historical row
            self.refsetsGrid.SetCellBackgroundColour(i,0, wx.WHITE)
            self.refsetsGrid.SetCellBackgroundColour(i,1, wx.WHITE)
            self.refsetsGrid.SetCellBackgroundColour(i,2, wx.WHITE)
            # statusNormalItalic = checkStatusForGrid(row[1])
            if conMetaFromConId_IN(row[0])[1] == 0:
                statusNormalItalic = wx.ITALIC
            else:
                statusNormalItalic = wx.NORMAL
            self.refsetsGrid.SetCellFont(i,0, wx.Font(10, wx.SWISS,
                    statusNormalItalic, wx.NORMAL, False, u'Sans'))
            self.refsetsGrid.SetCellFont(i,2, wx.Font(10, wx.SWISS,
                    statusNormalItalic, wx.NORMAL, False, u'Sans'))
            modTextColour = checkModForGrid(row[0])
            self.refsetsGrid.SetCellTextColour(i,2, modTextColour)
            self.refsetsGrid.SetCellValue(i,0,row[0])
            self.refsetsGrid.SetCellValue(i,2,pterm(tuplify(row[0])).encode("utf-8"))
            i += 1

            self.refsetsGrid.SetCellBackgroundColour(i,0, wx.LIGHT_GREY)
            self.refsetsGrid.SetCellBackgroundColour(i,1, wx.LIGHT_GREY)
            self.refsetsGrid.SetCellBackgroundColour(i,2, wx.LIGHT_GREY)
            self.refsetsGrid.SetCellTextColour(i,0, checkModForGridByTerm(row[3]))
            self.refsetsGrid.SetCellValue(i,0,modSimple(row[3]))
            self.refsetsGrid.SetCellValue(i,1,str(row[5]))
            self.refsetsGrid.SetCellValue(i,2,row[4])
            i += 1
            generalGaugeUpdate(self, i)

    generalGaugeReset(self, 1)
    self.refsetsGrid.Refresh()


def fillRefSetEntriesGrid(self, memberId, refsetId):
    # print 'Rows for ', memberId[0], pterm(memberId), 'in', refsetId[0], ptFromConId_IN(refsetId[0])[1]
    # print
    myValueRows = refSetDetailsFromMemIdandSetIdPlusMeta_IN(refsetId[0], memberId[0], cfg('REFSETS_TYPE'))
    myDescRows = refSetFieldsFromConId_IN(refsetId[0])
    myRefsetType = 'c' + refSetTypeFromConId_IN(refsetId[0])[0][0]  # if component datatype (can't pick up which one directly)
    if (cfg('REFSETS_TYPE') == 0) or (cfg('REFSETS_TYPE') == 2):
        self.EntryDetailsGrid.SetColSize(2,60)
    else:
        self.EntryDetailsGrid.SetColSize(2,1)
    self.EntryDetailsGrid.Refresh()
    counter = len(myValueRows) * (len(myDescRows) + 1)
    generalGaugeInit(self, counter)
    current = self.EntryDetailsGrid.GetNumberRows()
    if counter < current:
        self.EntryDetailsGrid.DeleteRows(0, current-counter, True)
    if current < counter:
        self.EntryDetailsGrid.AppendRows(counter-current)
    self.EntryDetailsGrid.SetDefaultCellTextColour(wx.BLACK)
    i = 0
    for value in myValueRows:
        counter = 0
        for row in myDescRows:
            self.EntryDetailsGrid.SetCellBackgroundColour(i,0, wx.WHITE)
            self.EntryDetailsGrid.SetCellBackgroundColour(i,1, wx.WHITE)
            self.EntryDetailsGrid.SetCellBackgroundColour(i,2, wx.WHITE)
            self.EntryDetailsGrid.SetCellBackgroundColour(i,3, wx.WHITE)
            if myRefsetType[counter] == 'c':  # if component datatype (can't pick up which one directly)
                # print row[0] + '\t' + value[counter + 3]  + '\t' + pterm(tuplify(value[counter + 3]))
                self.EntryDetailsGrid.SetCellValue(i,0,row[0])
                self.EntryDetailsGrid.SetCellTextColour(i,1, checkDefForGrid(value[counter + 3]))
                self.EntryDetailsGrid.SetCellValue(i,1,value[counter + 3])
                self.EntryDetailsGrid.SetCellValue(i,2,"")
                modTextColour = checkModForGrid(value[counter + 3])
                self.EntryDetailsGrid.SetCellTextColour(i,3, modTextColour)
                if conMetaFromConId_IN(value[counter + 3])[1] == 0:
                    statusNormalItalic = wx.ITALIC
                else:
                    statusNormalItalic = wx.NORMAL
                self.EntryDetailsGrid.SetCellFont(i,3, wx.Font(10, wx.SWISS,
                    statusNormalItalic, wx.NORMAL, False, u'Sans'))
                self.EntryDetailsGrid.SetCellValue(i,3,pterm(tuplify(value[counter + 3])).encode("utf-8"))
            else:
                # print row[0] + '\t' + str(value[counter + 3])
                self.EntryDetailsGrid.SetCellValue(i,0,row[0])
                self.EntryDetailsGrid.SetCellValue(i,1,str(value[counter + 3]))
                self.EntryDetailsGrid.SetCellValue(i,2,"")
                self.EntryDetailsGrid.SetCellValue(i,3,"")
            i += 1
            counter += 1
        self.EntryDetailsGrid.SetCellBackgroundColour(i,0, wx.LIGHT_GREY)
        self.EntryDetailsGrid.SetCellBackgroundColour(i,1, wx.LIGHT_GREY)
        self.EntryDetailsGrid.SetCellBackgroundColour(i,2, wx.LIGHT_GREY)
        self.EntryDetailsGrid.SetCellBackgroundColour(i,3, wx.LIGHT_GREY)
        self.EntryDetailsGrid.SetCellTextColour(i,0, checkModForGridByTerm(pterm(tuplify(value[0]))))
        self.EntryDetailsGrid.SetCellValue(i,0,modSimple(pterm(tuplify(value[0]))))
        self.EntryDetailsGrid.SetCellValue(i,2,str(value[2]))
        self.EntryDetailsGrid.SetCellValue(i,3,str(value[1]))
        # print pterm(tuplify(value[0])), value[0], value[1], value[2]
        i += 1
        generalGaugeUpdate(self, i)

    generalGaugeReset(self, 1)
    self.EntryDetailsGrid.Refresh()


def checkDefForGrid(inId):
    if conDefStatusFromConId_IN(inId) == 'Primitive':
        return wx.WHITE
    else:
        return wx.BLUE


def checkModForGrid(inId):
    if conModuleFromConId_IN(inId) == 'SNOMED CT United Kingdom clinical extension module':
        return wx.Colour(160,15,55)
    elif conModuleFromConId_IN(inId) == 'SNOMED CT United Kingdom drug extension module':
        return wx.Colour(25,135,45)
    elif conModuleFromConId_IN(inId) == 'SNOMED CT to ICD-10 rule-based mapping module':
        return wx.Colour(235,115,50)
    elif conModuleFromConId_IN(inId) == 'SNOMED CT United Kingdom Edition reference set module':
        return wx.Colour(165,85,165)
    elif conModuleFromConId_IN(inId) == 'SNOMED CT United Kingdom clinical extension reference set module':
        return wx.Colour(40,125,210)
    elif conModuleFromConId_IN(inId) == 'SNOMED CT United Kingdom drug extension reference set module':
        return wx.Colour(25,135,45)
    elif conModuleFromConId_IN(inId) == 'SNOMED CT model component':
        return wx.Colour(185,15,165)
    else:
        return wx.WHITE


def checkModForGridByTerm(inString):
    if inString == 'SNOMED CT United Kingdom clinical extension module':
        return wx.Colour(160,15,55)
    elif inString == 'SNOMED CT United Kingdom drug extension module':
        return wx.Colour(25,135,45)
    elif inString == 'SNOMED CT to ICD-10 rule-based mapping module':
        return wx.Colour(235,115,50)
    elif inString == 'SNOMED CT United Kingdom Edition reference set module':
        return wx.Colour(165,85,165)
    elif inString == 'SNOMED CT United Kingdom clinical extension reference set module':
        return wx.Colour(40,125,210)
    elif inString == 'SNOMED CT United Kingdom drug extension reference set module':
        return wx.Colour(25,135,45)
    elif inString == 'SNOMED CT model component':
        return wx.Colour(185,15,165)
    else:
        return wx.WHITE


def checkRefsModForGrid(inString):
    if inString == 'SNOMED CT United Kingdom clinical extension reference set module':
        return wx.Colour(160,15,55)
    elif inString == 'SNOMED CT United Kingdom drug extension reference set module':
        return wx.Colour(25,135,45)
    elif inString == 'SNOMED CT model component':
        return wx.Colour(185,15,165)
    else:
        return wx.WHITE

def checkDescModForGrid(inString):
    if inString == 'SNOMED CT United Kingdom clinical extension module':
        return wx.Colour(160,15,55)
    elif inString == 'SNOMED CT United Kingdom drug extension module':
        return wx.Colour(25,135,45)
    elif inString == 'SNOMED CT model component':
        return wx.Colour(185,15,165)
    else:
        return wx.WHITE


def modSimple(inString):
    if inString == 'SNOMED CT United Kingdom clinical extension module':
        return 'UK clinical extension'
    elif inString == 'SNOMED CT United Kingdom drug extension module':
        return 'UK drug extension'
    elif inString == 'SNOMED CT United Kingdom clinical extension reference set module':
        return 'UK clinical refset'
    elif inString == 'SNOMED CT United Kingdom drug extension reference set module':
        return 'UK drug refset'
    elif inString == 'SNOMED CT United Kingdom Edition reference set module':
        return 'UK edition refset'
    elif inString == 'SNOMED CT to ICD-10 rule-based mapping module':
        return 'ICD-10 rule-based'
    elif inString == 'SNOMED CT model component':
        return 'SNOMED CT model'
    else:
        return inString


def assocRefsetSimple(inString):
    outString = inString[0:inString.find(" association reference set")]
    return outString


def checkStatusForGrid(inId):
    if conMetaFromConId_IN(inId)[1] == 1:
        return wx.NORMAL
    else:
        return wx.ITALIC


def breadcrumbs_mwWrite(inString):
    readbackList = []
    writeToList = []
    if len(inString[0]) > 5:
        try:
            f = open('files/breadcrumbs_mw.txt', 'r')
            lines = f.readlines()
            for line in lines:
                readbackList.append(line)
            matchBool = False
            if (readbackList[-1]).strip() == inString[0].strip():
                matchBool = True
            if not matchBool:
                readbackList.append(inString[0].strip() + '\n')
            f.close()
            f = open('files/breadcrumbs_mw.txt', 'w')
            if matchBool:
                writeToList = readbackList
            else:
                writeToList = readbackList[1:]
            for entry in writeToList:
                f.write(entry)
        finally:
            f.close()


def focusSwap_mwWrite(inString):
    if len(inString[0]) > 5:
        try:
            f = open('files/focusSwap_mw.txt', 'w')
            f.write(inString[0])
        finally:
            f.close()


def breadcrumbs_mwRead(self):
    mycounter = 1
    breadcrumbList = []
    f = open('files/breadcrumbs_mw.txt', 'r')
    for line in f:
        tempLine = ()
        # print '[' + str(mycounter) + ']\t' + line.strip() + '\t' + pterm(tuplify(line.strip()))
        tempLine = (pterm(tuplify(line.strip())),line.strip())
        breadcrumbList.append(tempLine)
        mycounter += 1
    f.close()
    # print breadcrumbList
    fillSearchGrid(self, breadcrumbList)


def generalGaugeInit(self, total):
    # self.genPurposeGauge.SetBackgroundColour(wx.Colour(160,15,55)) # red
    self.genPurposeGauge.SetBackgroundColour(wx.Colour(25,135,45)) # green
    self.genPurposeGauge.SetRange(total)


def generalGaugeUpdate(self, i):
    self.genPurposeGauge.SetBackgroundColour(wx.Colour(196, 194, 189))
    self.genPurposeGauge.SetValue(i)
    wx.Yield()


def generalGaugeReset(self, finalFlag):
    if finalFlag == 1:
        self.genPurposeGauge.SetBackgroundColour(wx.Colour(196, 194, 189))
    else:
        self.genPurposeGauge.SetBackgroundColour(wx.Colour(25,135,45)) # green
    self.genPurposeGauge.SetRange(0)
    self.genPurposeGauge.SetValue(0)
    wx.Yield()

def readInSwap():
    f = open('files/focusSwap_mw.txt', 'r')
    for line in f:
        myId = line.strip()
    f.close()
    return tuplify(myId)

