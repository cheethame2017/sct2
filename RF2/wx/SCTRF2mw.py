#Boa:Frame:Frame1

import wx
import wx.lib.buttons
import wx.grid
import SCTRF2FrameFuncsMeta
import RefSetDet
from SCTRF2config import *
from SCTRF2Helpers import *

running = 0
refsetrunning = 0

def resetRefsetRunning(self):
    global refsetrunning
    print refsetrunning
    refsetrunning = 0
    print refsetrunning

def create(parent):
    return Frame1(parent)

[wxID_FRAME1, wxID_FRAME1CHILDGRID, wxID_FRAME1DESCRIPTIONGRID, 
 wxID_FRAME1FOCUSGRID, wxID_FRAME1GENPURPOSEGAUGE, 
 wxID_FRAME1HISTORICALORIGINSGRID, wxID_FRAME1PANEL1, wxID_FRAME1PARENTGRID, 
 wxID_FRAME1REFSETMEM, wxID_FRAME1REFSETSBUTTON, wxID_FRAME1REFSETSGRID, 
 wxID_FRAME1ROLEGRID, wxID_FRAME1SEARCHBUTTON, wxID_FRAME1SEARCHREPORTGRID, 
 wxID_FRAME1SEARCHTEXT, wxID_FRAME1STATUSBAR1, 
] = [wx.NewId() for _init_ctrls in range(16)]


[wxID_FRAME1PREFERENCEMENUSEARCHTYPE0, wxID_FRAME1PREFERENCEMENUSEARCHTYPE1, 
 wxID_FRAME1PREFERENCEMENUSEARCHTYPE2, 
] = [wx.NewId() for _init_coll_searchMenu_Items in range(3)]

[wxID_FRAME1SEARCHORDERMENUSEARCHORDER1, 
 wxID_FRAME1SEARCHORDERMENUSEARCHORDER2, 
] = [wx.NewId() for _init_coll_searchOrderMenu_Items in range(2)]

[wxID_FRAME1PREFERENCEMENUITEMS0, wxID_FRAME1PREFERENCEMENUITEMS1, 
 wxID_FRAME1PREFERENCEMENUITEMS2, wxID_FRAME1PREFERENCEMENUITEMS3, 
 wxID_FRAME1PREFERENCEMENUITEMS5, wxID_FRAME1PREFERENCEMENUITEMS8, 
 wxID_FRAME1PREFERENCEMENUSHOWBREADCRUMBS, 
 wxID_FRAME1PREFERENCEMENUSHOWHISTORIGINS, 
] = [wx.NewId() for _init_coll_preferenceMenu_Items in range(8)]

[wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY0, 
 wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY1, 
 wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY2, 
] = [wx.NewId() for _init_coll_roleDisplayMenu_Items in range(3)]

[wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY0, 
 wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY1, 
 wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY2, 
] = [wx.NewId() for _init_coll_termDisplayMenu_Items in range(3)]

[wxID_FRAME1GLOBALMENUGLOBALACTIVE0, wxID_FRAME1GLOBALMENUGLOBALACTIVE1, 
 wxID_FRAME1GLOBALMENUGLOBALACTIVE2, 
] = [wx.NewId() for _init_coll_globalMenu_Items in range(3)]

[wxID_FRAME1HISTORICALORIGINSMENUHISTORIG0, 
 wxID_FRAME1HISTORICALORIGINSMENUHISTORIG1, 
 wxID_FRAME1HISTORICALORIGINSMENUHISTORIG2, 
] = [wx.NewId() for _init_coll_historicalOriginsMenu_Items in range(3)]

[wxID_FRAME1FILEMENUEXIT] = [wx.NewId() for _init_coll_fileMenu_Items in range(1)]

class Frame1(wx.Frame):
    def _init_coll_menuBar1_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.fileMenu, title=u'File')
        parent.Append(menu=self.preferenceMenu, title=u'Preferences')

    def _init_coll_historicalOriginsMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'', id=wxID_FRAME1HISTORICALORIGINSMENUHISTORIG1,
              kind=wx.ITEM_RADIO, text=u'Historical origins active')
        parent.Append(help='', id=wxID_FRAME1HISTORICALORIGINSMENUHISTORIG0,
              kind=wx.ITEM_RADIO, text=u'Historical origins inactive')
        parent.Append(help='', id=wxID_FRAME1HISTORICALORIGINSMENUHISTORIG2,
              kind=wx.ITEM_RADIO, text=u'Historical origins all')
        self.Bind(wx.EVT_MENU, self.OnHistoricalOriginsMenuHistorig2Menu,
              id=wxID_FRAME1HISTORICALORIGINSMENUHISTORIG2)
        self.Bind(wx.EVT_MENU, self.OnHistoricalOriginsMenuHistorig1Menu,
              id=wxID_FRAME1HISTORICALORIGINSMENUHISTORIG1)
        self.Bind(wx.EVT_MENU, self.OnHistoricalOriginsMenuHistorig0Menu,
              id=wxID_FRAME1HISTORICALORIGINSMENUHISTORIG0)

    def _init_coll_roleDisplayMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY1,
              kind=wx.ITEM_RADIO, text=u'Role display active only')
        parent.Append(help='', id=wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY0,
              kind=wx.ITEM_RADIO, text=u'Role display inactive only')
        parent.Append(help='', id=wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY2,
              kind=wx.ITEM_RADIO, text=u'Role display all')
        self.Bind(wx.EVT_MENU, self.OnRoleDisplayMenuRoledisplay1Menu,
              id=wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY1)
        self.Bind(wx.EVT_MENU, self.OnRoleDisplayMenuRoledisplay0Menu,
              id=wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY0)
        self.Bind(wx.EVT_MENU, self.OnRoleDisplayMenuRoledisplay2Menu,
              id=wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY2)

    def _init_coll_searchOrderMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_FRAME1SEARCHORDERMENUSEARCHORDER1,
              kind=wx.ITEM_RADIO, text=u'Length')
        parent.Append(help='', id=wxID_FRAME1SEARCHORDERMENUSEARCHORDER2,
              kind=wx.ITEM_RADIO, text=u'Alphabetical')
        self.Bind(wx.EVT_MENU, self.OnSearchOrderMenuSearchorder1Menu,
              id=wxID_FRAME1SEARCHORDERMENUSEARCHORDER1)
        self.Bind(wx.EVT_MENU, self.OnSearchOrderMenuSearchorder2Menu,
              id=wxID_FRAME1SEARCHORDERMENUSEARCHORDER2)

    def _init_coll_preferenceMenu_Items(self, parent):
        # generated method, don't edit

        parent.AppendMenu(help='', id=wxID_FRAME1PREFERENCEMENUITEMS5,
              submenu=self.globalMenu, text=u'Global status')
        parent.AppendSeparator()
        parent.AppendMenu(help='', id=wxID_FRAME1PREFERENCEMENUITEMS0,
              submenu=self.searchMenu, text=u'Setup search')
        parent.AppendMenu(help='', id=wxID_FRAME1PREFERENCEMENUITEMS1,
              submenu=self.searchOrderMenu, text=u'Search order')
        parent.AppendMenu(help='', id=wxID_FRAME1PREFERENCEMENUITEMS2,
              submenu=self.roleDisplayMenu, text=u'Role display')
        parent.AppendMenu(help='', id=wxID_FRAME1PREFERENCEMENUITEMS3,
              submenu=self.termDisplayMenu, text=u'Term display')
        parent.AppendMenu(help='', id=wxID_FRAME1PREFERENCEMENUITEMS8,
              submenu=self.historicalOriginsMenu, text=u'Historical origins')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_FRAME1PREFERENCEMENUSHOWBREADCRUMBS,
              kind=wx.ITEM_CHECK, text=u'Show breadcrumbs')
        parent.Append(help='', id=wxID_FRAME1PREFERENCEMENUSHOWHISTORIGINS,
              kind=wx.ITEM_CHECK, text=u'Show historical origins')
        self.Bind(wx.EVT_MENU, self.OnPreferenceMenuShowbreadcrumbsMenu,
              id=wxID_FRAME1PREFERENCEMENUSHOWBREADCRUMBS)
        self.Bind(wx.EVT_MENU, self.OnPreferenceMenuShowhistoriginsMenu,
              id=wxID_FRAME1PREFERENCEMENUSHOWHISTORIGINS)

    def _init_coll_termDisplayMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY1,
              kind=wx.ITEM_RADIO, text=u'Term display active only')
        parent.Append(help='', id=wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY0,
              kind=wx.ITEM_RADIO, text=u'Term display inactive only')
        parent.Append(help='', id=wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY2,
              kind=wx.ITEM_RADIO, text=u'Term display all')
        self.Bind(wx.EVT_MENU, self.OnTermDisplayMenuTermdisplay1Menu,
              id=wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY1)
        self.Bind(wx.EVT_MENU, self.OnTermDisplayMenuTermdisplay0Menu,
              id=wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY0)
        self.Bind(wx.EVT_MENU, self.OnTermDisplayMenuTermdisplay2Menu,
              id=wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY2)

    def _init_coll_fileMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_FRAME1FILEMENUEXIT, kind=wx.ITEM_NORMAL,
              text=u'Exit')
        self.Bind(wx.EVT_MENU, self.OnFileMenuItems0Menu,
              id=wxID_FRAME1FILEMENUEXIT)

    def _init_coll_globalMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_FRAME1GLOBALMENUGLOBALACTIVE1,
              kind=wx.ITEM_NORMAL, text=u'Global active only')
        parent.Append(help='', id=wxID_FRAME1GLOBALMENUGLOBALACTIVE0,
              kind=wx.ITEM_NORMAL, text=u'Global inactive only')
        parent.Append(help='', id=wxID_FRAME1GLOBALMENUGLOBALACTIVE2,
              kind=wx.ITEM_NORMAL, text=u'Global all')
        self.Bind(wx.EVT_MENU, self.OnGlobalMenuGlobalactive1Menu,
              id=wxID_FRAME1GLOBALMENUGLOBALACTIVE1)
        self.Bind(wx.EVT_MENU, self.OnGlobalMenuGlobalactive0Menu,
              id=wxID_FRAME1GLOBALMENUGLOBALACTIVE0)
        self.Bind(wx.EVT_MENU, self.OnGlobalMenuGlobalactive2Menu,
              id=wxID_FRAME1GLOBALMENUGLOBALACTIVE2)

    def _init_coll_searchMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_FRAME1PREFERENCEMENUSEARCHTYPE1,
              kind=wx.ITEM_RADIO, text=u'Search type active only')
        parent.Append(help=u'', id=wxID_FRAME1PREFERENCEMENUSEARCHTYPE0,
              kind=wx.ITEM_RADIO, text=u'Search type inactive only')
        parent.Append(help='', id=wxID_FRAME1PREFERENCEMENUSEARCHTYPE2,
              kind=wx.ITEM_RADIO, text=u'Search type all')
        self.Bind(wx.EVT_MENU, self.OnPreferenceMenuSearchtype1Menu,
              id=wxID_FRAME1PREFERENCEMENUSEARCHTYPE1)
        self.Bind(wx.EVT_MENU, self.OnPreferenceMenuSearchtype0Menu,
              id=wxID_FRAME1PREFERENCEMENUSEARCHTYPE0)
        self.Bind(wx.EVT_MENU, self.OnPreferenceMenuSearchtype2Menu,
              id=wxID_FRAME1PREFERENCEMENUSEARCHTYPE2)

    def _init_utils(self):
        # generated method, don't edit
        self.searchMenu = wx.Menu(title=u'Search')

        self.preferenceMenu = wx.Menu(title=u'Preferences')

        self.menuBar1 = wx.MenuBar()

        self.fileMenu = wx.Menu(title=u'File')

        self.searchOrderMenu = wx.Menu(title=u'Search order')

        self.roleDisplayMenu = wx.Menu(title=u'Role display type')

        self.termDisplayMenu = wx.Menu(title=u'Term display type')

        self.globalMenu = wx.Menu(title=u'Global menu')

        self.historicalOriginsMenu = wx.Menu(title=u'Historical origins')

        self._init_coll_searchMenu_Items(self.searchMenu)
        self._init_coll_preferenceMenu_Items(self.preferenceMenu)
        self._init_coll_menuBar1_Menus(self.menuBar1)
        self._init_coll_fileMenu_Items(self.fileMenu)
        self._init_coll_searchOrderMenu_Items(self.searchOrderMenu)
        self._init_coll_roleDisplayMenu_Items(self.roleDisplayMenu)
        self._init_coll_termDisplayMenu_Items(self.termDisplayMenu)
        self._init_coll_globalMenu_Items(self.globalMenu)
        self._init_coll_historicalOriginsMenu_Items(self.historicalOriginsMenu)
        
        self.setMenuOptions()

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_FRAME1, name='', parent=prnt,
              pos=wx.Point(234, 127), size=wx.Size(3166, 1654),
              style=wx.DEFAULT_FRAME_STYLE, title=u'RF2 Browser')
        self._init_utils()
        self.SetClientSize(wx.Size(2375, 1654))
        self.SetMenuBar(self.menuBar1)
        self.Bind(wx.EVT_ACTIVATE, self.OnFrame1Activate)

        self.panel1 = wx.Panel(id=wxID_FRAME1PANEL1, name='panel1', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(2375, 1550),
              style=wx.TAB_TRAVERSAL)

        self.ParentGrid = wx.grid.Grid(id=wxID_FRAME1PARENTGRID,
              name=u'ParentGrid', parent=self.panel1, pos=wx.Point(488, 40),
              size=wx.Size(1620, 240), style=0)
        self.ParentGrid.SetColLabelSize(20)
        self.ParentGrid.SetRowLabelSize(1)
        self.ParentGrid.SetColMinimalAcceptableWidth(0)
        self.ParentGrid.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL,
              False, u'Sans'))
        self.ParentGrid.SetDefaultRowSize(20)
        self.ParentGrid.SetMinSize(wx.Size(1620, 200))
        self.ParentGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnParentGridCellLeftClick)

        self.FocusGrid = wx.grid.Grid(id=wxID_FRAME1FOCUSGRID,
              name=u'FocusGrid', parent=self.panel1, pos=wx.Point(488, 168),
              size=wx.Size(1620, 112), style=0)
        self.FocusGrid.SetRowLabelSize(1)
        self.FocusGrid.SetColLabelSize(20)
        self.FocusGrid.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Sans'))
        self.FocusGrid.SetDefaultRowSize(20)
        self.FocusGrid.SetMinSize(wx.Size(300, 40))
        self.FocusGrid.SetDefaultCellOverflow(True)
        self.FocusGrid.SetColMinimalAcceptableWidth(0)
        self.FocusGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnFocusGridCellLeftClick)

        self.ChildGrid = wx.grid.Grid(id=wxID_FRAME1CHILDGRID,
              name=u'ChildGrid', parent=self.panel1, pos=wx.Point(488, 232),
              size=wx.Size(864, 624), style=0)
        self.ChildGrid.SetRowLabelSize(1)
        self.ChildGrid.SetColLabelSize(20)
        self.ChildGrid.SetColMinimalAcceptableWidth(0)
        self.ChildGrid.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL,
              False, u'Sans'))
        self.ChildGrid.SetDefaultRowSize(20)
        self.ChildGrid.EnableGridLines(True)
        self.ChildGrid.SetHelpText(u'')
        self.ChildGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnChildGridCellLeftClick)

        self.SearchReportGrid = wx.grid.Grid(id=wxID_FRAME1SEARCHREPORTGRID,
              name=u'SearchReportGrid', parent=self.panel1, pos=wx.Point(16,
              100), size=wx.Size(684, 944), style=0)
        self.SearchReportGrid.SetRowLabelSize(1)
        self.SearchReportGrid.SetColLabelSize(40)
        self.SearchReportGrid.SetColMinimalAcceptableWidth(0)
        self.SearchReportGrid.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL,
              wx.NORMAL, False, u'Sans'))
        self.SearchReportGrid.SetDefaultRowSize(38)
        self.SearchReportGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnSearchReportGridCellLeftClick)

        self.DescriptionGrid = wx.grid.Grid(id=wxID_FRAME1DESCRIPTIONGRID,
              name=u'DescriptionGrid', parent=self.panel1, pos=wx.Point(16,
              1000), size=wx.Size(2328, 200), style=0)
        self.DescriptionGrid.SetColMinimalAcceptableWidth(0)
        self.DescriptionGrid.SetColLabelSize(40)
        self.DescriptionGrid.SetRowLabelSize(1)
        self.DescriptionGrid.SetDefaultRowSize(40)

        self.RoleGrid = wx.grid.Grid(id=wxID_FRAME1ROLEGRID, name=u'RoleGrid',
              parent=self.panel1, pos=wx.Point(16, 1200), size=wx.Size(1552,
              120), style=0)
        self.RoleGrid.SetColMinimalAcceptableWidth(0)
        self.RoleGrid.SetColLabelSize(40)
        self.RoleGrid.SetRowLabelSize(1)
        self.RoleGrid.SetDefaultRowSize(40)
        self.RoleGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnRoleGridCellLeftClick)

        self.SearchText = wx.TextCtrl(id=wxID_FRAME1SEARCHTEXT,
              name=u'SearchText', parent=self.panel1, pos=wx.Point(16, 40),
              size=wx.Size(564, 50), style=0, value=u'')

        self.SearchButton = wx.lib.buttons.GenButton(id=wxID_FRAME1SEARCHBUTTON,
              label=u'Search', name=u'SearchButton', parent=self.panel1,
              pos=wx.Point(400, 40), size=wx.Size(108, 48), style=0)
        self.SearchButton.Bind(wx.EVT_BUTTON, self.OnSearchButtonButton,
              id=wxID_FRAME1SEARCHBUTTON)

        self.RefsetsButton = wx.Button(id=wxID_FRAME1REFSETSBUTTON,
              label=u'Refset entry details', name=u'RefsetsButton',
              parent=self.panel1, pos=wx.Point(1068, 4), size=wx.Size(288, 40),
              style=0)
        self.RefsetsButton.Bind(wx.EVT_BUTTON, self.OnRefsetsButtonButton,
              id=wxID_FRAME1REFSETSBUTTON)

        self.statusBar1 = wx.StatusBar(id=wxID_FRAME1STATUSBAR1,
              name='statusBar1', parent=self, style=0)
        self.SetStatusBar(self.statusBar1)

        self.historicalOriginsGrid = wx.grid.Grid(id=wxID_FRAME1HISTORICALORIGINSGRID,
              name=u'historicalOriginsGrid', parent=self.panel1,
              pos=wx.Point(1080, 232), size=wx.Size(732, 304), style=0)
        self.historicalOriginsGrid.SetColLabelSize(20)
        self.historicalOriginsGrid.SetColMinimalAcceptableWidth(1)
        self.historicalOriginsGrid.SetRowLabelSize(1)
        self.historicalOriginsGrid.SetDefaultRowSize(20)
        self.historicalOriginsGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnHistoricalOriginsGridCellLeftClick)

        self.refsetsGrid = wx.grid.Grid(id=wxID_FRAME1REFSETSGRID,
              name=u'refsetsGrid', parent=self.panel1, pos=wx.Point(1080, 392),
              size=wx.Size(732, 304), style=0)
        self.refsetsGrid.SetColLabelSize(20)
        self.refsetsGrid.SetColMinimalAcceptableWidth(1)
        self.refsetsGrid.SetRowLabelSize(0)
        self.refsetsGrid.SetDefaultRowSize(20)
        self.refsetsGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnrefsetsGridCellLeftClick)

        self.genPurposeGauge = wx.Gauge(id=wxID_FRAME1GENPURPOSEGAUGE,
              name=u'genPurposeGauge', parent=self.panel1, pos=wx.Point(16, 28),
              range=100, size=wx.Size(2328, 16), style=wx.GA_HORIZONTAL)
        self.genPurposeGauge.SetMinSize(wx.Size(440, 10))

        self.RefSetMem = wx.Button(id=wxID_FRAME1REFSETMEM,
              label=u'Refset membership', name=u'RefSetMem', parent=self.panel1,
              pos=wx.Point(1270, 4), size=wx.Size(216, 40), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnFrame1Activate(self, event):
        global running
        if running == 0:
            SCTRF2FrameFuncsMeta.OnFrameActivate(self, event)
        running = 1

    def OnChildGridCellLeftClick(self, event):
        row = event.GetRow()
        column = 2
        value = self.ChildGrid.GetCellValue(row, column)
        SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))

    def OnFocusGridCellLeftClick(self, event):
        callId = ('138875005',)
        row = 0
        column = event.GetCol()
        print column
        if column == 0:
            SCTRF2FrameFuncsMeta.OnGridClick(self, event, callId)
        elif column == 1:
            SCTRF2FrameFuncsMeta.breadcrumbs_mwRead(self)
        elif column == 5:
            value = self.FocusGrid.GetCellValue(row, column)
            SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))

    def OnParentGridCellLeftClick(self, event):
        row = event.GetRow()
        column = 5
        value = self.ParentGrid.GetCellValue(row, column)
        SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))

    def OnRoleGridCellLeftClick(self, event):
        row = event.GetRow()
        column = event.GetCol()
        if column == 6 or column == 9:
            value = self.RoleGrid.GetCellValue(row, column)
            SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))
        elif column == 7 or column == 10:
            value = self.RoleGrid.GetCellValue(row, column - 1)
            SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))


    def OnHistoricalOriginsGridCellLeftClick(self, event):
        row = event.GetRow()
        column = 0
        if self.historicalOriginsGrid.GetCellBackgroundColour(row,column) == wx.WHITE:
            value = self.historicalOriginsGrid.GetCellValue(row, column)
            if not value == "":
                SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))

    def OnrefsetsGridCellLeftClick(self, event):
        row = event.GetRow()
        column = 0
        if self.refsetsGrid.GetCellBackgroundColour(row,column) == wx.WHITE:
            value = self.refsetsGrid.GetCellValue(row, column)
            if not value == "":
                SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))

    def OnSearchReportGridCellLeftClick(self, event):
        row = event.GetRow()
        column = 2
        value = self.SearchReportGrid.GetCellValue(row, column)
        SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))

    def OnSearchButtonButton(self, event):
        value = self.SearchText.GetValue()
        SCTRF2FrameFuncsMeta.searchOnText(self, value, cfg('SEARCH_TYPE'))

    def OnRefsetsButtonButton(self, event):
        # event.Skip()
        global refsetrunning
        print "refsetrunning ", refsetrunning
        if refsetrunning == 0:
            refSetWindow = RefSetDet.create(self)
            refSetWindow.Show()
            refsetrunning = 0
        else:
            print 'window already open'
            refSetWindow = RefSetDet.create(self)
            refSetWindow.Show()

    def OnPreferenceMenuSearchtype1Menu(self, event):
        cfgWrite("SEARCH_TYPE", 1)
        self.OnSearchButtonButton(event)

    def OnPreferenceMenuSearchtype0Menu(self, event):
        cfgWrite("SEARCH_TYPE", 0)
        self.OnSearchButtonButton(event)

    def OnPreferenceMenuSearchtype2Menu(self, event):
        cfgWrite("SEARCH_TYPE", 2)
        self.OnSearchButtonButton(event)

    def OnPreferenceMenuSearchtypesubmenMenu(self, event):
        event.Skip()

    def setMenuOptions(self):
        # search_type
        print "SEARCH_TYPE", cfg("SEARCH_TYPE")
        if cfg("SEARCH_TYPE") == 0:
            self.searchMenu.Check(wxID_FRAME1PREFERENCEMENUSEARCHTYPE0,True)
        elif cfg("SEARCH_TYPE") == 1:
            self.searchMenu.Check(wxID_FRAME1PREFERENCEMENUSEARCHTYPE1,True)
        else:
            self.searchMenu.Check(wxID_FRAME1PREFERENCEMENUSEARCHTYPE2,True)
        # search_order
        print "SEARCH_ORDER", cfg("SEARCH_ORDER")
        if cfg("SEARCH_ORDER") == 1:
            self.searchOrderMenu.Check(wxID_FRAME1SEARCHORDERMENUSEARCHORDER1,True)
        elif cfg("SEARCH_ORDER") == 2:
            self.searchOrderMenu.Check(wxID_FRAME1SEARCHORDERMENUSEARCHORDER2,True)
        # role_display_type ROLE_DISPLAY_TYPE
        print "ROLE_DISPLAY_TYPE", cfg("ROLE_DISPLAY_TYPE")
        if cfg("ROLE_DISPLAY_TYPE") == 0:
            self.roleDisplayMenu.Check(wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY0,True)
        elif cfg("ROLE_DISPLAY_TYPE") == 1:
            self.roleDisplayMenu.Check(wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY1,True)
        else:
            self.roleDisplayMenu.Check(wxID_FRAME1ROLEDISPLAYMENUROLEDISPLAY2,True)
        #breadcrumb_type BREADCRUMB_TYPE
        print "BREADCRUMB_TYPE", cfg("BREADCRUMB_TYPE")
        if cfg("BREADCRUMB_TYPE") == 1:
            self.preferenceMenu.Check(wxID_FRAME1PREFERENCEMENUSHOWBREADCRUMBS,True)
        else:
            self.preferenceMenu.Check(wxID_FRAME1PREFERENCEMENUSHOWBREADCRUMBS,False)
        #breadcrumb_type HISTORICAL_ORIGINS
        print "HISTORICAL_ORIGINS", cfg("HISTORICAL_ORIGINS")
        if cfg("HISTORICAL_ORIGINS") == 1:
            self.preferenceMenu.Check(wxID_FRAME1PREFERENCEMENUSHOWHISTORIGINS,True)
        else:
            self.preferenceMenu.Check(wxID_FRAME1PREFERENCEMENUSHOWHISTORIGINS,False)
        # term_display_type TERM_DISPLAY_TYPE
        print "TERM_DISPLAY_TYPE", cfg("TERM_DISPLAY_TYPE")
        if cfg("TERM_DISPLAY_TYPE") == 0:
            self.termDisplayMenu.Check(wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY0,True)
        elif cfg("TERM_DISPLAY_TYPE") == 1:
            self.termDisplayMenu.Check(wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY1,True)
        else:
            self.termDisplayMenu.Check(wxID_FRAME1TERMDISPLAYMENUTERMDISPLAY2,True)
        # historical_origins_type HISTORICAL_ORIGINS_TYPE
        print "HISTORICAL_ORIGINS_TYPE", cfg("HISTORICAL_ORIGINS_TYPE")
        if cfg("HISTORICAL_ORIGINS_TYPE") == 0:
            self.historicalOriginsMenu.Check(wxID_FRAME1HISTORICALORIGINSMENUHISTORIG0,True)
        elif cfg("HISTORICAL_ORIGINS_TYPE") == 1:
            self.historicalOriginsMenu.Check(wxID_FRAME1HISTORICALORIGINSMENUHISTORIG1,True)
        else:
            self.historicalOriginsMenu.Check(wxID_FRAME1HISTORICALORIGINSMENUHISTORIG2,True)


    def OnSearchOrderMenuSearchorder1Menu(self, event):
        cfgWrite("SEARCH_ORDER", 1)
        self.OnSearchButtonButton(event)

    def OnSearchOrderMenuSearchorder2Menu(self, event):
        cfgWrite("SEARCH_ORDER", 2)
        self.OnSearchButtonButton(event)

    def OnRoleDisplayMenuRoledisplay1Menu(self, event):
        cfgWrite("ROLE_DISPLAY_TYPE", 1)
        self.refreshForm(event)

    def OnRoleDisplayMenuRoledisplay0Menu(self, event):
        cfgWrite("ROLE_DISPLAY_TYPE", 0)
        self.refreshForm(event)
        
    def OnRoleDisplayMenuRoledisplay2Menu(self, event):
        cfgWrite("ROLE_DISPLAY_TYPE", 2)
        self.refreshForm(event)

    def OnPreferenceMenuShowbreadcrumbsMenu(self, event):
        if cfg("BREADCRUMB_TYPE") == 1:
            cfgWrite("BREADCRUMB_TYPE", 0)
        else:
            cfgWrite("BREADCRUMB_TYPE", 1)
            
    def OnTermDisplayMenuTermdisplay1Menu(self, event):
        cfgWrite("TERM_DISPLAY_TYPE", 1)
        self.refreshForm(event)

    def OnTermDisplayMenuTermdisplay0Menu(self, event):
        cfgWrite("TERM_DISPLAY_TYPE", 0)
        self.refreshForm(event)
        
    def OnTermDisplayMenuTermdisplay2Menu(self, event):
        cfgWrite("TERM_DISPLAY_TYPE", 2)
        self.refreshForm(event)

    def OnFileMenuItems0Menu(self, event):
        self.Close()

    def OnGlobalMenuGlobalactive1Menu(self, event):
        cfgWrite("ROLE_DISPLAY_TYPE", 1)
        cfgWrite("TERM_DISPLAY_TYPE", 1)
        # cfgWrite("SEARCH_TYPE", 1)
        cfgWrite("HISTORICAL_ORIGINS_TYPE", 1)
        cfgWrite("REFSETS_TYPE", 1)
        self.refreshForm(event)
        self.setMenuOptions()

    def OnGlobalMenuGlobalactive0Menu(self, event):
        cfgWrite("ROLE_DISPLAY_TYPE", 0)
        cfgWrite("TERM_DISPLAY_TYPE", 0)
        # cfgWrite("SEARCH_TYPE", 0)
        cfgWrite("HISTORICAL_ORIGINS_TYPE", 0)
        cfgWrite("REFSETS_TYPE", 0)
        self.refreshForm(event)
        self.setMenuOptions()

    def OnGlobalMenuGlobalactive2Menu(self, event):
        cfgWrite("ROLE_DISPLAY_TYPE", 2)
        cfgWrite("TERM_DISPLAY_TYPE", 2)
        # cfgWrite("SEARCH_TYPE", 2)
        cfgWrite("HISTORICAL_ORIGINS_TYPE", 2)
        cfgWrite("REFSETS_TYPE", 2)
        self.refreshForm(event)
        self.setMenuOptions()
        
    def refreshForm(self, event):
        value = self.FocusGrid.GetCellValue(0, 5)
        SCTRF2FrameFuncsMeta.OnGridClick(self, event, tuplify(value))
        self.OnSearchButtonButton(event)

    def OnHistoricalOriginsMenuHistorig2Menu(self, event):
        cfgWrite("HISTORICAL_ORIGINS_TYPE", 2)
        self.refreshForm(event)
        
    def OnHistoricalOriginsMenuHistorig1Menu(self, event):
        cfgWrite("HISTORICAL_ORIGINS_TYPE", 1)
        self.refreshForm(event)
        
    def OnHistoricalOriginsMenuHistorig0Menu(self, event):
        cfgWrite("HISTORICAL_ORIGINS_TYPE", 0)
        self.refreshForm(event)

    def OnPreferenceMenuShowhistoriginsMenu(self, event):
        if cfg("HISTORICAL_ORIGINS") == 1:
            cfgWrite("HISTORICAL_ORIGINS", 0)
        else:
            cfgWrite("HISTORICAL_ORIGINS", 1)
        
if __name__ == '__main__':
    app = wx.PySimpleApp()
    frame = create(None)
    frame.Show()

    app.MainLoop()
