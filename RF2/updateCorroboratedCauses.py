from __future__ import print_function, unicode_literals
import sqlite3
import sys
from SCTRF2Helpers import pterm

# assist file for wikipedia code - adds preferred terms into corroboratedcauses table

def updateCorroboratedCauses(dbpath):
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur.execute("select * from corroboratedCauses;")
    recordset = cur.fetchall()
    for row in recordset:
        sourceTerm = pterm(row[8])
        typeTerm = pterm(row[10])
        destinationTerm = pterm(row[12])
        print("UPCC", sourceTerm, typeTerm, destinationTerm)
        cur.execute("update corroboratedCauses set sourceTerm = ?, typeTerm = ?, destinationTerm = ? where inputId = ? and sourceId = ? and destinationId = ?", (sourceTerm, typeTerm, destinationTerm, row[4], row[8], row[12]))
        conn.commit()
    cur.close()
    conn.close()

if (sys.version_info > (3, 0)):
    p3 = True
else:
    p3 = False

if p3:
    if len(sys.argv) != 2:
        u = input('original (o) or new (n) code?')
    else:
        u = str(sys.argv[1])
else:
    if len(sys.argv) != 2:
        u = raw_input('original (o) or new (n) code?').decode('UTF-8')
    else:
        u = str(sys.argv[1])

if u == 'o':
    # original
    wikiPath = '/home/ed/D/RF2Wiki.db'
else:
    # new
    wikiPath = '/home/ed/files/wiki2/RF2Wiki.db'

updateCorroboratedCauses(wikiPath)
