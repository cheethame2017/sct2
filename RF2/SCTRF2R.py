#!/usr/bin/env python
from __future__ import print_function, unicode_literals
from SCTRF2Funcs import snapshotRoleHeader, focusSwapWrite, browseFromId, breadcrumbsWrite, \
                        listify, setSupers, readFocusValue
from platformSpecific import clearScreen, headerWriterSnapECL, sep, input2_3
from SCTRF2config import cfg
from SCTRF2Role import processConstraintApp, probeFromSnap, readInConstraint, probeFromTemplate, toDesc, \
                        refreshConstraint, changeECLCode, readBackFile, readInOneWriteOne, readInTwoWriteOne, \
                        addRowToECL, removeRowFromECL, plusCheck, objectsToValues, dotDrawSet, ECLbreadcrumbsRead, \
                        writeOutRawECL, changeECLCodeToSet, setCheck, redunTest, charCheck, navSS, \
                        SSbrowseFromId, minusCheck, dotDrawDefs, ECLsetRead, ECLsetWrite, clauseNatureTest, \
                        processConstraintSet, ECLsetRemove, constrSetList, loadOrModifyConstrSet, convertSet, \
                        promptString, showRefsetMatches, starCheck, hashCheck, setConcVal, loadFile, \
                        identifyGroupers, navigationSlice, onlyDefined, identifyGroupersSequence, demoValue, \
                        navSetList, loadOrModifyNavSet, dotProcess, groupByModule, closingSquareBCheck, \
                        singleQuoteCheck, openingSquareBCheck, tildeCheck, closingBraceCheck, upSetEmulOneLetter, \
                        upSetEmulInspect, upSetEmulMultiLetter, sampleAllFiles, dotDrawGephi, resetSetPic
from SCTRF2Help import selectHelpFile

def browseFromIndexCall(inIndex, inString, inDict):
    if not debug:
        clearScreen()
    if debug:
        print('-> in browseFromIndexCall')
    snapshotRoleHeader()
    # if 0 is a key in the dictionary -> Id call from constraint
    # so check if it's an odd number too.
    # otherwise just navigating around so don't check for oddness.
    if ((0 in inDict) and ((inIndex % 2) != 0)) or (0 not in inDict):
        if 0 in inDict: # if it is a constraint call, force back to normal navigation.
            navSS(0)
        callId = inString
        if isinstance(callId, str):
            callId = listify(callId.strip())
        breadcrumbsWrite(callId[0])
        focusSwapWrite(callId[0])
        if cfg("NAV_SS") == 0:
            returnString = browseFromId(callId)
        else:
            returnString = SSbrowseFromId(callId)
        if debug:
            print(returnString)
        return returnString
    else:
        returnString = readInConstraint(True)
        return returnString

# ===============================

def processConstraintCall(redunBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in processConstraintCall')
    snapshotRoleHeader()
    returnString = processConstraintApp(redunBool, True)
    if debug:
        print(returnString)
    return returnString

def processConstraintSetCall(redunBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in processConstraintCall')
    snapshotRoleHeader()
    returnString = processConstraintSet(redunBool)
    if debug:
        print(returnString)
    return returnString

def readBackBreadcrumbsCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in readBackBreadcrumbsCall')
    snapshotRoleHeader()
    returnString = ECLbreadcrumbsRead()
    if debug:
        print(returnString)
    return returnString

def readBackSetCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in readBackSetCall')
    snapshotRoleHeader()
    returnString = ECLsetRead(True, "", True)
    if debug:
        print(returnString)
    return returnString

def constrSetListCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in constrSetListCall')
    snapshotRoleHeader()
    returnString = constrSetList()
    if debug:
        print(returnString)
    return returnString

def navSetListCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in constrSetListCall')
    snapshotRoleHeader()
    returnString = navSetList()
    if debug:
        print(returnString)
    return returnString

def probeFromSnapCall(resetSetPicBool=False):
    if not debug:
        clearScreen()
    if debug:
        print('-> in probeFromSnapCall')
    if resetSetPicBool:
        resetSetPic()
    snapshotRoleHeader()
    probeFromSnap()
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def probeFromTemplateCall(inPath):
    if not debug:
        clearScreen()
    if debug:
        print('-> in probeFromTemplateCall')
    snapshotRoleHeader()
    probeFromTemplate(inPath)
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def readInConstraintCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in readInConstraintCall')
    snapshotRoleHeader()
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def addConstraintCall(includeExclude, newBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in readInConstraintCall')
    snapshotRoleHeader()
    ECLsetWrite(includeExclude, newBool)
    returnString = ECLsetRead(True, "", True)
    if debug:
        print(returnString)
    return returnString

def toDescAndSelfCall(inIndex, inDict):
    if not debug:
        clearScreen()
    if debug:
        print('-> in toDescAndSelf')
    snapshotRoleHeader()
    if (int(inIndex) % 2) == 0:
        # CONC - for concrete values, set operator to greater than or equal to
        if (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
            if inDict[int(inIndex) + 1][0][0] == "#":
                returnString = toDesc(inDict, inIndex, ">")
            else:
                returnString = toDesc(inDict, inIndex, "<<")
        else:
            returnString = toDesc(inDict, inIndex, "<<")
        refreshConstraint(returnString[0][1])
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

# CONC - call set concrete value number
def setConcValCall(inIndexAndNumber, inDict):
    if not debug:
        clearScreen()
    if debug:
        print('-> in setConcValCall')
    if "#" in inIndexAndNumber:
        inIndex = inIndexAndNumber[:inIndexAndNumber.find("#")]
        inNumber = inIndexAndNumber[inIndexAndNumber.find("#"):]
        snapshotRoleHeader()
        if (int(inIndex) % 2) != 0:
            returnString = setConcVal(inDict, inIndex, inNumber)
            refreshConstraint(returnString[0][1])
        returnString = readInConstraint(True)
        if debug:
            print(returnString)
        return returnString
    else:
        return inDict

def toDescCall(inIndex, inDict):
    if not debug:
        clearScreen()
    if debug:
        print('-> in toDescCall')
    snapshotRoleHeader()
    if (int(inIndex) % 2) == 0:
        # CONC - for concrete values, set operator to less than or equal to
        if (cfg("CONC_VALS") == 1 or cfg("CONC_DATA") == 1):
            returnString = toDesc(inDict, inIndex, "<")
        else:
            returnString = toDesc(inDict, inIndex, "<")
        refreshConstraint(returnString[0][1])
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def loadOrModifyConstrSetCall(inName, inBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in loadConstrSetCall')
    snapshotRoleHeader()
    loadOrModifyConstrSet(inName, inBool)
    returnString = ECLsetRead(True, inName, True)
    if debug:
        print(returnString)
    return returnString

def loadOrModifyNavSetCall(inName, inBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in loadConstrSetCall')
    snapshotRoleHeader()
    focusSetString = loadOrModifyNavSet(inName, inBool)
    returnString = dotDrawSet(focusSetString, '<')
    if debug:
        print(returnString)
    return returnString

def loadFileCall(targetSet, filePath):
    if not debug:
        clearScreen()
    if debug:
        print('-> in loadFileCall')
    snapshotRoleHeader()
    errorBool = loadFile(targetSet, filePath)
    if errorBool:
        errorString = ' (with errors in set Z).'
    else:
        errorString = ''
    returnString = readBackFile('sets' + sep() + targetSet + '.txt', 'File loaded in ' + targetSet + errorString, False, False)
    if debug:
        print(returnString)
    return returnString

def convertSetCall(inLetter):
    if not debug:
        clearScreen()
    if debug:
        print('-> in convertSetCall')
    snapshotRoleHeader()
    if len(inLetter) > 1:
        if  inLetter[1] == "]" or inLetter[1] == "[":
            returnString = identifyGroupersSequence(inLetter)
        elif inLetter[1] == "~" or inLetter[1] == "@":
            returnString = identifyGroupers(inLetter, True)
        else:
            convertSet(inLetter)
            returnString = ECLsetRead(True, "", True)
    else:
        convertSet(inLetter)
        returnString = ECLsetRead(True, "", True)
    if debug:
        print(returnString)
    return returnString

def toSelfCall(inIndex, inDict, refBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in toSelfCall')
    snapshotRoleHeader()
    if (int(inIndex) % 2) == 0:
        if refBool:
            returnString = toDesc(inDict, inIndex, "^")
        else:
            returnString = toDesc(inDict, inIndex, "")
        refreshConstraint(returnString[0][1])
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def changeECLCodeCall(inIndex, inDict):
    if not debug:
        clearScreen()
    if debug:
        print('-> in changeECLCodeCall')
    snapshotRoleHeader()
    if "-" in inIndex:
        inIndexCheckable = inIndex.split("-")[0]
    else:
        inIndexCheckable = inIndex
    if (int(inIndexCheckable) % 2) != 0:
        returnString = changeECLCode(inDict, inIndex)
        refreshConstraint(returnString[0][1])
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def changeECLCodeToSetCall(inIndex, inDict, inSetLetter):
    if not debug:
        clearScreen()
    if debug:
        print('-> in changeECLCodeToSetCall')
    snapshotRoleHeader()
    returnString = changeECLCodeToSet(inDict, inIndex, inSetLetter)
    # automatically set preceding operator to 'member of'
    returnString = toSelfCall(str(int(inIndex) - 1), inDict, True)
    # therefore uses refresh and readIn methods from toSelfCall()
    if debug:
        print(returnString)
    return returnString

def selectConstraintCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in selectConstraintCall')
    snapshotRoleHeader()
    writeOutRawECL(inString)
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def removeConstraintCall(inInt):
    if not debug:
        clearScreen()
    if debug:
        print('-> in removeConstraintCall')
    snapshotRoleHeader()
    ECLsetRemove(inInt)
    returnString = ECLsetRead(True, "", True)
    if debug:
        print(returnString)
    return returnString

def objectsToValuesCall(inIndex, inDict, inLetter):
    if not debug:
        clearScreen()
    if debug:
        print('-> in objectsToValuesCall')
    snapshotRoleHeader()
    returnString = objectsToValues(inDict, inIndex, inLetter)
    if debug:
        print(returnString)
    return returnString

def removeRowFromECLCall(inIndex, inDict, cloneBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in removeRowFromECLCall')
    snapshotRoleHeader()
    returnString = removeRowFromECL(inDict, inIndex, cloneBool)
    refreshConstraint(returnString[0][1])
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def addRowToECLCall(inIndex, inDict, groupBool, cloneBool, newGroupBool):
    if not debug:
        clearScreen()
    if debug:
        print('-> in addRowToECLCall')
    snapshotRoleHeader()
    returnString = addRowToECL(inDict, inIndex, groupBool, cloneBool, newGroupBool)
    refreshConstraint(returnString[0][1])
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def showRefsetMatchesCall(inString):
    if plusCheck(inString)[1]:
        testSections = 2
    elif minusCheck(inString)[1]:
        testSections = 1
    else:
        testSections = 2
    if not debug:
        clearScreen()
    if debug:
        print('-> inshowRefsetMatchesCall')
    snapshotRoleHeader()
    returnString = showRefsetMatches(inString[0], testSections)
    if debug:
        print(returnString)
    return returnString

def readBackRoleReportCall(inTarget):
    if not debug:
        clearScreen()
    if debug:
        print('-> in readBackRoleReportCall')
    snapshotRoleHeader()
    if inTarget == "ECL":
        returnString = readBackFile('files' + sep() + 'ecl' + sep() + 'rolereportId.txt', 'ECL', False, True)
    elif inTarget == "ALL":
        returnString = sampleAllFiles()
    else:
        returnString = readBackFile('sets' + sep() + inTarget + '.txt', 'Reading back ' + inTarget, False, True)
    if debug:
        print(returnString)
    return returnString

def groupByModuleCall(inTarget):
    if not debug:
        clearScreen()
    if debug:
        print('-> in groupByModuleCall')
    snapshotRoleHeader()
    returnString = groupByModule(inTarget)
    if debug:
        print(returnString)
    return returnString

def dotDrawSetCall(inTarget, dirString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in dotDrawSetCall')
    snapshotRoleHeader()
    if inTarget == '4':
        returnString = readBackFile('files' + sep() + 'SS' + sep() + 'SSMembers.txt', 'Reading back navigation set members:', False, True)
    elif inTarget == '7':
        returnString = readBackFile('files' + sep() + 'SS' + sep() + 'SSStructural.txt', 'Reading back navigation set structural members:', False, True)
    else:
        returnString = dotDrawSet(inTarget, dirString)
    if debug:
        print(returnString)
    return returnString

def upSetEmulLetterCall(inTarget, dirString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in upSetEmulLetterCall')
    snapshotRoleHeader()
    if len(inTarget) == 1:
        returnString = upSetEmulOneLetter(inTarget, dirString)
    else:
        returnString = upSetEmulMultiLetter(inTarget, dirString)
    if debug:
        print(returnString)
    return returnString

def upSetEmulInspectCall(inTarget, returnString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in upSetEmulInspectCall')
    snapshotRoleHeader()
    returnString = upSetEmulInspect(inTarget, returnString)
    if debug:
        print(returnString)
    return returnString

def dotDrawDefsCall(inTarget, dirString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in dotDrawDefsCall')
    snapshotRoleHeader()
    returnString = dotDrawDefs(inTarget, dirString)
    if debug:
        print(returnString)
    return returnString

def dotDrawGephiCall():
    if not debug:
        clearScreen()
    if debug:
        print('-> in dotDrawGephiCall')
    snapshotRoleHeader()
    returnString = dotDrawGephi()
    if debug:
        print(returnString)
    return returnString

def selectHelpFileCall(inString):
    if not debug:
        clearScreen()
    if debug:
        print('-> in selectHelpFileCall')
    snapshotRoleHeader()
    selectHelpFile(inString, 'SCTRF2R')
    returnString = readInConstraint(False)
    if debug:
        print(returnString)
    return returnString

def searchToLetter(inLetter):
    if plusCheck(inLetter)[1]:
        combineSet = 1
    elif minusCheck(inLetter)[1]:
        combineSet = 2
    else:
        combineSet = 0
    if not debug:
        clearScreen()
    if debug:
        print('-> in searchToLetter')
    snapshotRoleHeader()
    returnString = readInOneWriteOne('files' + sep() + 'search' + sep() + 'searchreportId.txt', inLetter[0], "Search list to " + inLetter[0], combineSet, True, False)
    if debug:
        print(returnString)
    return returnString

def roleToLetter(inLetter):
    if plusCheck(inLetter)[1]:
        combineSet = 1
    elif minusCheck(inLetter)[1]:
        combineSet = 2
    else:
        combineSet = 0
    if not debug:
        clearScreen()
    if debug:
        print('-> in roleToLetter')
    snapshotRoleHeader()
    returnString = readInOneWriteOne('files' + sep() + 'ecl' + sep() + 'rolereportId.txt', inLetter[0], "ECL list to " + inLetter[0], combineSet, False, False)
    if debug:
        print(returnString)
    return returnString

def refsetToLetter(inLetter):
    if plusCheck(inLetter)[1]:
        combineSet = 1
    elif minusCheck(inLetter)[1]:
        combineSet = 2
    else:
        combineSet = 0
    if not debug:
        clearScreen()
    if debug:
        print('-> in refsetToLetter')
    snapshotRoleHeader()
    returnString = readInOneWriteOne('files' + sep() + 'snap' + sep() + 'refsetIds.txt', inLetter[0], "Snap list to " + inLetter[0], combineSet, False, False)
    if debug:
        print(returnString)
    return returnString

def focusToLetter(inLetter):
    if plusCheck(inLetter)[1]:
        combineSet = 1
    elif minusCheck(inLetter)[1]:
        combineSet = 2
    elif starCheck(inLetter)[1]:
        combineSet = 3
    elif hashCheck(inLetter)[1]:
        combineSet = 8
    elif closingSquareBCheck(inLetter)[1]:
        combineSet = 9
    elif singleQuoteCheck(inLetter)[1]:
        combineSet = 10
    elif openingSquareBCheck(inLetter)[1]:
        combineSet = 11
    elif tildeCheck(inLetter)[1]:
        combineSet = 12
    elif closingBraceCheck(inLetter)[1]:
        combineSet = 13
    else:
        combineSet = 0
    if not debug:
        clearScreen()
    if debug:
        print('-> in focusToLetter')
    snapshotRoleHeader()
    returnString = readInOneWriteOne('files' + sep() + 'snap' + sep() + 'focusSwap.txt', inLetter[0], "Focus to " + inLetter[0], combineSet, False, True)
    if debug:
        print(returnString)
    return returnString

def expandLetterSet(inLetter):
    if plusCheck(inLetter)[1]:
        expandSet = 7 # ancestors
    elif minusCheck(inLetter)[1]:
        expandSet = 6 # descendants
    elif starCheck(inLetter)[1]:
        expandSet = 5 # role values
    elif hashCheck(inLetter)[1]:
        expandSet = 4 # role values and att names
    else:
        expandSet = 7 # ancestors
    if not debug:
        clearScreen()
    if debug:
        print('-> in expandLetterSet')
    snapshotRoleHeader()
    returnString = readInOneWriteOne('sets' + sep() + inLetter[0] + '.txt', inLetter[0], "Expanding " + inLetter[0], expandSet, False, False)
    if debug:
        print(returnString)
    return returnString

def compareFilesAND(inLetters):
    if not debug:
        clearScreen()
    if debug:
        print('-> in readBackRoleReportCall')
    snapshotRoleHeader()
    if len(inLetters) == 4:
        returnString = readInTwoWriteOne(inLetters[0], inLetters[1], inLetters[2], "AND", True, inLetters[3])
    else:
        returnString = readInTwoWriteOne(inLetters[0], inLetters[1], inLetters[2], "AND", False, "")
    if debug:
        print(returnString)
    return returnString

def compareFilesOR(inLetters):
    if not debug:
        clearScreen()
    if debug:
        print('-> in readBackRoleReportCall')
    snapshotRoleHeader()
    if len(inLetters) == 4:
        returnString = readInTwoWriteOne(inLetters[0], inLetters[1], inLetters[2], "OR", True, inLetters[3])
    else:
        returnString = readInTwoWriteOne(inLetters[0], inLetters[1], inLetters[2], "OR", False, "")
    if debug:
        print(returnString)
    return returnString

def compareFilesNOT(inLetters):
    if not debug:
        clearScreen()
    if debug:
        print('-> in readBackRoleReportCall')
    snapshotRoleHeader()
    if len(inLetters) == 4:
        returnString = readInTwoWriteOne(inLetters[0], inLetters[1], inLetters[2], "NOT", True, inLetters[3])
    else:
        returnString = readInTwoWriteOne(inLetters[0], inLetters[1], inLetters[2], "NOT", False, "")
    if debug:
        print(returnString)
    return returnString

def setSupersCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setSupers', str(inInteger))
    snapshotRoleHeader()
    setSupers(inInteger)
    focusSwapWrite(readFocusValue()[0])
    probeFromSnap()
    # returnString = refreshConstraint(returnString)
    returnString = readInConstraint(True)
    if debug:
        print(returnString)
    return returnString

def setNavigationSliceCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> navigationSlice', str(inInteger))
    snapshotRoleHeader()
    returnString = navigationSlice(inInteger)
    if debug:
        print(returnString)
    returnString = dotDrawSetCall('0', '<')
    return returnString

def setOnlyDefinedCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> onlyDefined', str(inInteger))
    snapshotRoleHeader()
    returnString = onlyDefined(inInteger)
    if debug:
        print(returnString)
    returnString = dotDrawSetCall('0', '<')
    return returnString

def setDemoValueCall(inInteger):
    if not debug:
        clearScreen()
    if debug:
        print('-> setDemo', str(inInteger))
    snapshotRoleHeader()
    returnString = demoValue(inInteger)
    if debug:
        print(returnString)
    returnString = dotDrawSetCall('0', '<')
    return returnString

returnString = {}
debug = False
headerWriterSnapECL()
clearScreen()
# main control code
firstTime = True
while True:
    if firstTime:
        dotDrawSetCall('$', '<')
        topaction = "a"
        firstTime = False
    else:
        topaction = input2_3(promptString(cfg("SHOW_INDEX_SET"))).strip()
    if topaction == "":
        topaction = "a"
    if len(topaction) > 0:
        if topaction[0] == '.':
            topaction = dotProcess(topaction)
    if topaction == 'zz':
        if debug:
            print(returnString)
        clearScreen()
        navSS(0)
        break  # breaks out of the while loop and ends session
    elif topaction == 'a':
        if debug:
            print(returnString)
            print('-> calling probeFromSnapCall')
        returnString = probeFromSnapCall()
    elif topaction == 'b':
        if debug:
            print(returnString)
            print('-> calling readInConstraintCall')
        returnString = readInConstraintCall()
    elif topaction == 'r':
        if debug:
            print(returnString)
            print('-> calling readBackRoleReportCall')
        returnString = readBackRoleReportCall("ECL")
    elif topaction == 'x':
        if debug:
            print(returnString)
            print('-> calling readBackBreadcrumbsCall')
        returnString = readBackBreadcrumbsCall()
    elif topaction == 'D':
        if debug:
            print(returnString)
            print('-> calling readBackSetCall')
        returnString = readBackSetCall()
    elif topaction == 'G':
        if debug:
            print(returnString)
            print('-> calling constrSetListCall')
        returnString = constrSetListCall()
    elif topaction == 'R':
        if debug:
            print(returnString)
            print('-> calling constrSetListCall')
        returnString = navSetListCall()
    elif topaction == 'xx':  # toggle *most* debug outputs
        if debug:
            debug = False
            print('debug OFF')
        else:
            debug = True
            print('debug ON')
#
    else:
        try:
            if topaction[0] == 'g':
                if debug:
                    print('g', returnString[topaction[1:]])
                callThing = topaction[1:]
                if callThing.isalpha():
                    returnString = searchToLetter(callThing.upper())
            elif topaction[0] == 'A':
                if debug:
                    print('A', returnString[topaction[1:]])
                callThing = topaction[1:]
                if callThing == '#':
                    probeFromSnapCall(True)
                else:
                    returnString = focusToLetter(callThing.upper())
            elif topaction[0] == '-':
                if debug:
                    print('- help file for', topaction[1:])
                returnString = selectHelpFileCall(topaction[1:])
            elif topaction[0] == 'h':
                if debug:
                    print('h', returnString[topaction[1:]])
                callThing = topaction[1:]
                if callThing.isalpha():
                    returnString = roleToLetter(callThing.upper())
            elif topaction[0] == 't':
                if debug:
                    print('t', topaction[1:])
                callThing = topaction[1:]
                if callThing.isalpha():
                    returnString = refsetToLetter(callThing.upper())
            elif topaction[0] == 'q':
                if debug:
                    print(returnString)
                    print("'q' -> calling eclProcess")
                redunBool = redunTest(topaction)
                returnString = processConstraintCall(redunBool)
            elif topaction[0] == 'F':
                if debug:
                    print(returnString)
                    print("'F' -> calling processConstraintSetCall")
                redunBool = redunTest(topaction)
                returnString = processConstraintSetCall(redunBool)
            elif topaction[0] == 'C':
                if debug:
                    print(returnString)
                    print("'C' -> calling addConstraintCall")
                includeExclude, newBool = clauseNatureTest(topaction)
                returnString = addConstraintCall(includeExclude, newBool)
            if topaction[0] == 'O':
                if debug:
                    print(returnString)
                returnString = setOnlyDefinedCall(int(topaction[1:]))
            if topaction[0] == 'Q':
                if debug:
                    print(returnString)
                returnString = setDemoValueCall(int(topaction[1:]))
            if topaction[0] == 'P':
                if debug:
                    print(returnString)
                returnString = setNavigationSliceCall(int(topaction[1:]))
            elif topaction[0] == 'm':
                if debug:
                    print('m', topaction[1:])
                callThing = topaction[1:]
                returnString = compareFilesAND(callThing.upper())
            elif topaction[0] == 'n':
                if debug:
                    print('n', topaction[1:])
                callThing = topaction[1:]
                returnString = compareFilesOR(callThing.upper())
            elif topaction[0] == 'o':
                if debug:
                    print('o', topaction[1:])
                callThing = topaction[1:]
                returnString = compareFilesNOT(callThing.upper())
            elif topaction[0] == 'i':
                if debug:
                    print('i', returnString[int(topaction[1:])])
                returnString = changeECLCodeCall(topaction[1:], returnString)
            elif topaction[0] == 'y':
                if debug:
                    print('y', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = selectConstraintCall(callThing)
            elif topaction[0] == 'E':
                if debug:
                    print('E', topaction[1:])
                returnString = removeConstraintCall(int(topaction[1:]))
            elif topaction[0] == 'K':
                if debug:
                    print('K', topaction[1:])
                callThing = topaction[1:]
                returnString = showRefsetMatchesCall(callThing)
            elif topaction[0] == 'p':
                testId, setLetter = setCheck(topaction[1:])
                if debug:
                    print('p', returnString[int(testId)])
                returnString = objectsToValuesCall(testId, returnString, setLetter)
            elif topaction[0] == 'c':
                testId, cloneBool = plusCheck(topaction[1:])
                if debug:
                    print('c', returnString[int(topaction[1:])])
                returnString = removeRowFromECLCall(testId, returnString, cloneBool)
            elif topaction[0] == 'd':
                testId, cloneBool = plusCheck(topaction[1:])
                if debug:
                    print('d', returnString[int(testId)])
                returnString = addRowToECLCall(testId, returnString, True, cloneBool, False)
            elif topaction[0] == 'e':
                testId, cloneBool = plusCheck(topaction[1:])
                if debug:
                    print('e', returnString[int(testId)])
                returnString = addRowToECLCall(testId, returnString, False, cloneBool, False)
            elif topaction[0] == 'f':
                testId, cloneBool = plusCheck(topaction[1:])
                if debug:
                    print('f', returnString[int(testId)])
                returnString = addRowToECLCall(testId, returnString, True, cloneBool, True)
            elif topaction[0] == 'j':
                if debug:
                    print('j', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = toDescAndSelfCall(topaction[1:], returnString)
            elif topaction[0] == 'k':
                if debug:
                    print('k', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = toDescCall(topaction[1:], returnString)
            elif topaction[0] == 'l':
                testId, refBool = hashCheck(topaction[1:])
                if debug:
                    print('l', returnString[int(topaction[1:])])
                returnString = toSelfCall(testId, returnString, refBool)
            elif topaction[0] == 'H':
                if debug:
                    print('H', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = loadOrModifyConstrSetCall(callThing, True)
            elif topaction[0] == 'S':
                if debug:
                    print('S', returnString[int(topaction[1:])])
                callThing = returnString[int(topaction[1:])]
                returnString = loadOrModifyNavSetCall(callThing, True)
            elif topaction[0] == 'I':
                if debug:
                    print('I')
                if topaction[1:].isnumeric():
                    if int(topaction[1:]) in returnString:
                        callThing = returnString[int(topaction[1:])]
                elif topaction[1] == "-" and topaction[2:].isnumeric():
                    if int(topaction[2:]) in returnString:
                        callThing = (returnString[int(topaction[2:])][0] + "---",)
                else:
                    callThing = topaction[1:]
                returnString = loadOrModifyConstrSetCall(callThing, False)
            elif topaction[0] == 'T':
                if debug:
                    print('T')
                if topaction[1:].isnumeric():
                    if int(topaction[1:]) in returnString:
                        callThing = returnString[int(topaction[1:])]
                elif topaction[1] == "-" and topaction[2:].isnumeric():
                    if int(topaction[2:]) in returnString:
                        callThing = (returnString[int(topaction[2:])][0] + "---",)
                else:
                    callThing = topaction[1:]
                returnString = loadOrModifyNavSetCall(callThing, False)
            elif topaction[0] == 'M':
                if debug:
                    print('M')
                targetSet = "M"
                if len(topaction) > 1:
                    targetSet = topaction[1]
                filePath = ""
                if len(topaction) > 2:
                    filePath = topaction[2:]
                returnString = loadFileCall(targetSet, filePath)
            elif topaction[0] == 'J':
                if debug:
                    print('J')
                returnString = convertSetCall(topaction[1:])
            # CONC - pick up set concrete value number
            elif topaction[0] == 'L':
                if debug:
                    print('L', returnString[int(topaction[1:])])
                returnString = setConcValCall(topaction[1:], returnString)
            elif topaction[0] == 's':
                if debug:
                    print('s', topaction[1:])
                callThing = topaction[1:]
                returnString = probeFromTemplateCall(callThing.upper())
            elif topaction[0] == 'u':
                if debug:
                    print('u', ("ALL" if topaction == 'u' else topaction[1:]))
                if topaction == 'u':
                    callThing = "ALL"
                else:
                    callThing = topaction[1:]
                returnString = readBackRoleReportCall(callThing.upper())
            elif topaction[0] == 'U':
                if debug:
                    print('U', topaction[1:])
                callThing = topaction[1:]
                returnString = groupByModuleCall(callThing.upper())
            elif topaction[0] == 'v':
                if debug:
                    print('v', returnString)
                callThing = topaction[1:]
                returnString = setSupersCall(int(topaction[1:]))
            elif topaction[0] == 'w':
                testId, dirString = charCheck(topaction[1:], 0)
                if debug:
                    print('w', topaction[1:])
                returnString = dotDrawSetCall(testId.upper(), dirString)
            elif topaction[0] == 'V':
                testId, dirString = charCheck(topaction[1:], 0)
                if debug:
                    print('V', topaction[1:])
                if testId.isalpha():
                    returnString = upSetEmulLetterCall(testId.upper(), dirString)
                else:
                    returnString = upSetEmulInspectCall(testId, returnString)
            elif topaction[0] == 'B':
                testId, dirString = charCheck(topaction[1:], 1)
                if debug:
                    print('B', topaction[1:])
                if testId == "Y":
                    returnString = dotDrawGephiCall()
                else:
                    returnString = dotDrawDefsCall(testId.upper(), dirString)
            elif topaction[0] == 'N':
                if debug:
                    print('N', topaction[1:])
                if len(topaction[1:]) == 1:
                    sendString = topaction[1:] + "+"
                else:
                    sendString = topaction[1:]
                returnString = expandLetterSet(sendString)
            elif topaction[0] == 'z':
                testId, setLetter = setCheck(topaction[1:])
                if debug:
                    print('z', returnString[int(testId)])
                returnString = changeECLCodeToSetCall(testId, returnString, setLetter.upper())
            else:
                if debug:
                    print(' ', returnString[int(topaction)])
                returnString = browseFromIndexCall(int(topaction), returnString[int(topaction)], returnString)
            True
        except:
            True
